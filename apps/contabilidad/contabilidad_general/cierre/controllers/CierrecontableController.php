<?php

/**
 * Clase controladora para gestionar el Cierre Contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CierrecontableController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new CierreContableModel();
        parent::init();
    }

    public function cierrecontableAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadCuentasAgrupadasAction()
    {
        echo json_encode($this->model->loadDataCuentasAgrupadas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataFechaAction()
    {
        echo json_encode($this->model->loadDataFecha());
    }

    /**
     * call this model action
     */
    public function loadCfgComprobanteAction()
    {
        echo json_encode($this->model->loadCfgComprobante());
    }

    /**
     * call this model action
     */
    public function getComprobanteCierreAction()
    {
        echo json_encode($this->model->getComprobanteCierre($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadCfgCierreAction()
    {
        echo json_encode($this->model->loadCfgCierre($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function cerrarPeriodoAction()
    {
        echo json_encode($this->model->cerrarPeriodo());
    }

    /**
     * call this model action
     */
    public function cerrarEjercicioAction()
    {
        echo json_encode($this->model->cerrarEjercicio());
    }

    /**
     * call this model action
     */
    public function updateCfgCierreAction()
    {
        echo $this->model->updateCfgCierre($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteCfgCierreAction()
    {
        echo $this->model->deleteCfgCierre($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function saveComprobanteAction()
    {
        echo $this->model->saveComprobante($this->_request->getPost());
    }

}
