<?php

/**
 * Clase modelo para gestionar el Cierre Contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CierreContableModel extends ZendExt_Model
{

    private $datCierrecontable;

    public function CierreContableModel()
    {
        parent::ZendExt_Model();
        $this->datCierrecontable = new DatCierrecontable();
    }

    /**
     * Carga el arbol de Cuentas Contables agrupadas.
     * @param Array $argparams (node)
     * @return Array arbol de Cuentas Contables agrupadas
     */
    public function loadDataCuentasAgrupadas($argparams)
    {
        $ctas = new CuentasAgrupadasModel();
        return $ctas->loadDataCuentasAgrupadas($argparams);
    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $datComprobante = new DatComprobante();
        $periodo = $datComprobante->obtenerPeriodo($this->global->Estructura->idestructura, 4);
        $fecha = $datComprobante->obtenerFecha($this->global->Estructura->idestructura, 4);
        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Carga los datos asociados al Cierre contable.
     * @param Array $argparams (action)
     * @return Array datos asociados al Cierre contable
     */
    public function loadCfgCierre($argparams)
    {
        $datConfCierrecontable = new ConfCierrecontable();
        $datCierrecontable = new DatCierrecontable();
        $cierre = $datCierrecontable->getCierre($this->global->Estructura->idestructura);
        $config = $datConfCierrecontable->loadConfCierrecontable(array('concepto' => $this->getConcepto($argparams['action'])), $cierre[0]['idcierrecontable']);
        return $this->getComplementaryData($config);
    }

    /**
     * Complementa los datos asociados al Cierre contable.
     * @param Array $config
     * @return Array datos asociados al Cierre contable
     */
    public function getComplementaryData($config)
    {
        $NomGrupo = new NomGrupo();
        $NomContenido = new NomContenido();
        $NomCuenta = new NomCuenta();
        foreach ($config as &$v) {
            if ($v['tabla'] == 1) {//grupo subgrupo
                $grupo = $NomGrupo->obtenerGrupoByID($v['id']);
                $v['denominacion'] = $grupo[0]['concatgrupo'] . ' ' . $grupo[0]['denominacion'];
                $v['descripcion'] = $grupo[0]['descripcion'];
                $v['tipo'] = 'Grupo';
            } else if ($v['tabla'] == 2) {//contenido
                $content = $NomContenido->loadContenidoPorId($v['id']);
                $v['denominacion'] = $content[0]['codigo'] . ' ' . $content[0]['denominacion'];
                $v['descripcion'] = $content[0]['descripcion'];
                $v['tipo'] = 'Contenido';
            } else {//cta
                $cta = $NomCuenta->loadCuentasPorId($v['id']);
                $v['denominacion'] = $cta[0]['text'];
                $v['descripcion'] = $cta[0]['denominacion'];
                $v['tipo'] = 'Cuenta';
            }
        }
        return $config;
    }

    /**
     * Cierra el Periodo contable del subsistema.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function cerrarPeriodo()
    {
        $pendientes = $this->verificarComprobantesAsentados();
        if (count($pendientes)) {
            return array('success' => false, 'status' => 'comprobante', 'datos' => $pendientes);
        } else {
            return $this->goToClosePeriodo();
        }
    }

    /**
     * Cierra el Periodo contable.
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function goToClosePeriodo()
    {
        $precedencia = $this->verificarPrecedencia();
        if (!count($precedencia)) {
            $lastPeriodo = $this->integrator->contabilidad->ultimoPeriodoCerrado($this->global->Estructura->idestructura, 4);
            if (!$lastPeriodo) {
                $datPeriodocontable = new DatPeriodocontable();
                $periodoActual = $datPeriodocontable->getPeriodoSubsistema($this->global->Estructura->idestructura, 4);
                $nextP = $datPeriodocontable->getNextPeriodo($periodoActual[0]['idperiodo'], $periodoActual[0]['idejercicio'], $periodoActual[0]['fin']);
                $cierreModel = new DatCierreModel();
                if (count($nextP)) {
                    return $cierreModel->cerrarPeriodo(4, $nextP);
                } else {
                    return $cierreModel->ultimoDiaPeriodo(4, $periodoActual[0]['fin']);
                }
            } else {
                return array('success' => false, 'msg' => 'El &uacute;ltimo per&iacute;odo ya ha sido cerrado.');
            }
        } else {
            return array('success' => false, 'status' => 'subsistemas', 'datos' => $precedencia);
        }
    }

    /**
     * Revisa la precedencia para el cierre contable.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function verificarPrecedencia()
    {
        $subsistOpen = array();
        $nomSubsistema = new NomSubsistema();
        $datasubsist = $nomSubsistema->getSubsistema(4);
        $precedentes = $nomSubsistema->getSubsistemasPrecedentes($this->global->Estructura->idestructura, $datasubsist[0]['precedencia']);
        if (count($precedentes)) {
            foreach ($precedentes as $v) {
                if (implode('', explode('-', $datasubsist[0]['fecha'])) > implode('', explode('-', $v[0]['fecha']))) {
                    $subsistOpen[] = $v;
                }
            }
        }return $subsistOpen;
    }

    /**
     * Cierra el Ejercicio contable.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function cerrarEjercicio()
    {
        $valid = $this->validarPrecondicionesEjercicio();
        if ($valid === true) {
            $lastPeriodo = $this->integrator->contabilidad->ultimoPeriodoCerrado($this->global->Estructura->idestructura, 4);
            if ($lastPeriodo) {
                $conf = $this->datCierrecontable->getConfCierreComprobante($this->global->Estructura->idestructura);
                if (count($conf) === 3) {
                    return $this->integrator->contabilidad->cerrarEjercicio($this->global->Estructura->idestructura, 4);
                } else {
                    return array('success' => false, 'status' => 'cmpcierre', 'msg' => 'No se han generado todos los comprobantes del cierre.');
                }
            } else {
                return array('success' => false, 'msg' => 'El &uacute;ltimo per&iacute;odo no ha sido cerrado.');
            }
        } else {
            return $valid;
        }
    }

    /**
     * Revisa las precondiciones para el Cierre de Ejercicio.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function validarPrecondicionesEjercicio()
    {
        $pendientes = $this->verificarComprobantesAsentados();
        if (count($pendientes)) {
            return array('success' => false, 'status' => 'comprobante', 'datos' => $pendientes);
        } else {
            $precedencia = $this->verificarPrecedencia();
            if (!count($precedencia)) {
                return true;
            } else {
                return array('success' => false, 'status' => 'subsistemas', 'datos' => $precedencia);
            }
        }
    }

    /**
     * Guarda la configuracion asociada a los conceptos del cierre.
     * @param Array $argparams (action, criterio, data)
     * @return boolean true if ocurred, false if failure
     */
    public function updateCfgCierre($argparams)
    {
        $idcierrecontable = $this->saveCierreContable();
        return $this->saveConfCierreContable($idcierrecontable, $argparams);
    }

    /**
     * Guarda la configuracion asociada al cierre contable.
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function saveCierreContable()
    {
        $datCierrecontable = new DatCierrecontable();
        $cierre = $datCierrecontable->getCierre($this->global->Estructura->idestructura);
        if (count($cierre)) {
            return $cierre[0]['idcierrecontable'];
        } else {
            $datCierrecontable->idestructura = $this->global->Estructura->idestructura;
            return $this->insertarCierre($datCierrecontable);
        }
    }

    /**
     * Adiciona el Cierre contable.
     * @param stdClass $datCierrecontable 
     * @return boolean true if ocurred, false if failure
     */
    public function insertarCierre(DatCierrecontable $datCierrecontable)
    {
        try {
            $datCierrecontable->save();
            return $datCierrecontable->idcierrecontable;
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    /**
     * Guarda la configuracion asociada al cierre contable.
     * @param Integer $idcierrecontable
     * @param Array $argparams (action, id, type)
     * @return boolean true if ocurred, false if failure
     */
    public function saveConfCierreContable($idcierrecontable, $argparams)
    {
        $datConfCierrecontable = new ConfCierrecontable();
        if (!count($datConfCierrecontable->existConfCierrecontable($idcierrecontable, $argparams['id'], $argparams['type']))) {
            $datConfCierrecontable->id = $argparams['id'];
            $datConfCierrecontable->tabla = $argparams['type'];
            $datConfCierrecontable->concepto = $this->getConcepto($argparams['action']);
            $datConfCierrecontable->idcierrecontable = $idcierrecontable;
            return $this->insertarConfCierre($datConfCierrecontable);
        } else {
            return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgExisteCfg}";
        }
    }

    /**
     * Adiciona la configuracion del Cierre contable.
     * @param stdClass $datConfCierrecontable 
     * @return boolean true if ocurred, false if failure
     */
    public function insertarConfCierre(ConfCierrecontable $datConfCierrecontable)
    {
        try {
            $datConfCierrecontable->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgFallido}";
        }
    }

    /**
     * Obtiene el concepto de Cierre contable.
     * @param String $action 
     * @return Integer
     */
    public function getConcepto($action)
    {
        if ($action == 'config_cuenta_acreditar')
            return '1';
        else if ($action == 'config_cuenta_debitar')
            return '2';
        else if ($action == 'config_cuenta_cierre')
            return '3';
        else if ($action == 'config_cuenta_utilidades')
            return '4';
        else
            return '5';
    }

    /**
     * elimina la configuracion asociada a los conceptos del cierre.
     * @param Array $argparams (idconfcierrecontable)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteCfgCierre($argparams)
    {
        try {
            $objCfgCierre = Doctrine::getTable('ConfCierrecontable')->find($argparams['idconfcierrecontable']);
            $objCfgCierre->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
        } catch (Doctrine_Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgFallido}";
        }
    }

    /**
     * Carga los datos asociados a la configuracion de los Comprobantes de Cierre.
     * @param none
     * @return Array datos asociados a la configuracion de los Comprobantes de Cierre
     */
    public function loadCfgComprobante()
    {
        $arrCfg = array(array('id' => 0, 'denominacion' => 'Comprobante cuentas a acreditar', 'generado' => false),
            array('id' => 1, 'denominacion' => 'Comprobante cuentas a debitar', 'generado' => false),
            array('id' => 2, 'denominacion' => 'Comprobante cuentas de utilidades/p&eacute;rdidas', 'generado' => false));
        $conf = $this->datCierrecontable->getConfCierreComprobante($this->global->Estructura->idestructura);
        if (count($conf)) {
            foreach ($conf as $v) {
                foreach ($arrCfg as &$m) {
                    if ($v['tipocierre'] == $m['id']) {
                        $m['generado'] = true;
                        $m['numCmp'] = $v['numerocomprobante'];
                    }
                }
            }
        }
        return array('datos' => $arrCfg);
    }

    /**
     * Carga los datos para la confeccion de los Comprobantes de Cierre.
     * @param Array $argparams (id)
     * @return Array datos para la confeccion de los Comprobantes de Cierre
     */
    public function getComprobanteCierre($argparams)
    {
        $valid = $this->validarPrecondicionesEjercicio();
        if ($valid === true) {
            $lastPeriodo = $this->integrator->contabilidad->ultimoPeriodoCerrado($this->global->Estructura->idestructura, 4);
            if ($lastPeriodo) {
                $fechaCont = $this->loadDataFecha();
                $fecha = $fechaCont['fecha']['fecha'];
                $conceptCierre = '3';
                $concept = ($argparams['id'] == 0) ? '1' : (($argparams['id'] == 1) ? '2' : '0' );
                $datConfCierrecontable = new ConfCierrecontable();
                $datCierrecontable = new DatCierrecontable();
                $cierre = $datCierrecontable->getCierre($this->global->Estructura->idestructura);
                $configCierre = $datConfCierrecontable->loadConfCierrecontable(array('concepto' => $conceptCierre), $cierre[0]['idcierrecontable']);
                if (!count($configCierre)) {
                    return array('success' => false, 'msg' => 'no hay cta de cierre configurada');
                }
                $ctaCierre = $this->integrator->contabilidad->getSaldoCuentas(array($configCierre[0]['id']), $this->global->Estructura->idestructura, $fecha);
                if ($argparams['id'] == 0 || $argparams['id'] == 1) {//haciendo cero ingresos y gastos, la cta del cierre debe presentar saldo cero
                    $ctaCierre[0]['saldocuenta'] = 0;
                }
                if ($concept == '0' && $ctaCierre[0]['saldocuenta'] < 0) {
                    $concept = '5';
                } else if ($concept == '0' && $ctaCierre[0]['saldocuenta'] > 0) {
                    $concept = '4';
                }
                if ($concept == 0) {//caso en que no hay utilidades ni perdidas
                    return array('success' => false, 'msg' => 'no hay utilidades ni perdidas');
                } else {
                    $config = $datConfCierrecontable->loadConfCierrecontable(array('concepto' => $concept), $cierre[0]['idcierrecontable']);
                    if (!count($config)) {
                        if ($concept == 1) {
                            $msg = 'no hay ctas a acreditar configuradas';
                        } else if ($concept == 2) {
                            $msg = 'no hay ctas a debitar configuradas';
                        } else if ($concept == 4) {
                            $msg = 'no hay cta de utilidades configurada';
                        } else if ($concept == 5) {
                            $msg = 'no hay cta de perdidas configurada';
                        }
                        return array('success' => false, 'msg' => $msg);
                    } else {
                        $NomCuenta = new NomCuenta();
                        $ctas = array();
                        foreach ($config as $v) {
                            if ($v['tabla'] == '1') {
                                $c = $NomCuenta->loadCuentasHijasPorGrupo($this->global->Estructura->idestructura, array($v['id']));
                                foreach ($c as $m) {
                                    $ctas[] = $m['id'];
                                }
                            } else if ($v['tabla'] == 2) {
                                $c = $NomCuenta->loadCuentasHijasPorContenido($this->global->Estructura->idestructura, array($v['id']));
                                foreach ($c as $m) {
                                    $ctas[] = $m['id'];
                                }
                            } else if ($v['tabla'] == '3') {
                                $ctas[] = $v['id'];
                            }
                        }
                        $cuentasProc = array();
                        if (count($ctas)) {
                            $cuentasProc = $this->integrator->contabilidad->getSaldoCuentas($ctas, $this->global->Estructura->idestructura, $fecha);
                        }
                        return $this->createComprobante($ctaCierre, $cuentasProc, $concept);
                    }
                }
            } else {
                return array('success' => false, 'msg' => 'El &uacute;ltimo per&iacute;odo no ha sido cerrado.');
            }
        } else {
            return $valid;
        }
    }

    /**
     * Crea el formato para el comprobante de cierre.
     * @param Array $ctaCierre
     * @param Array $ctas
     * @param Integer $concepto
     * @return Array comprobante de cierre
     */
    public function createComprobante($ctaCierre, $ctas, $concepto)
    {
        $dataCmp = new stdClass();
        $ref = $this->generateReferenciaCmp($concepto);
        $dataCmp->referencia = $ref;
        $dataCmp->detalle = $ref;
        $fechaCont = $this->loadDataFecha();
        $dataCmp->fechaemision = $fechaCont['fecha']['fecha'];
        $dataCmp->idperiodo = $fechaCont['periodo']['idperiodo'];
        $dataCmp->idsubsistema = 4;
        $dataCmp->pases = $this->createPases($ctaCierre, $ctas, $concepto);
        $dataCmp->tipo = $concepto;
        return $dataCmp;
    }

    /**
     * Crea los pases para el comprobante de cierre.
     * @param Array $ctaCierre
     * @param Array $ctas
     * @param Integer $concepto
     * @return Array pases para el comprobante de cierre
     */
    public function createPases($ctaCierre, $ctas, $concepto)
    {
        $arrPases = array();
        if ($concepto == 1) {
            $myCtas = $this->recorrerCtas($ctas, 'cred');
            $ctaC = $ctaCierre[0];
            $ctaC['saldoContrapartida'] = $myCtas[1];
            $arrPases[] = $this->makePase($ctaC, 'deb');
            foreach ($myCtas[0] as $v) {
                $arrPases[] = $v;
            }
        } else if ($concepto == 2) {
            $myCtas = $this->recorrerCtas($ctas, 'deb');
            $ctaC = $ctaCierre[0];
            $ctaC['saldoContrapartida'] = $myCtas[1];
            foreach ($myCtas[0] as $v) {
                $arrPases[] = $v;
            }
            $arrPases[] = $this->makePase($ctaC, 'cred');
        } else if ($concepto == 4 || 5) {//utilidades o perdidas
            $ctaC = $ctaCierre[0];
            $arrPases[] = $this->makePase($ctaC, 'deb');
            $ctasProc = $ctas[0];
            $ctasProc['saldoContrapartida'] = $ctaC['saldocuenta'];
            $arrPases[] = $this->makePase($ctasProc, 'cred');
        }
        return $arrPases;
    }

    /**
     * Crea un pase para el comprobante.
     * @param Array $ctas
     * @param Integer $debcred
     * @return Array pase del comprobante
     */
    public function makePase($cta, $debcred)
    {
        $pase = new stdClass();
        $pase->idpase = 0;
        $pase->codigodocumento = "";
        $pase->idcuenta = $cta['idcuenta'];
        $pase->concatcta = $cta['concatcta'];
        $pase->denominacion = $cta['denominacion'];
        $pase->debito = ($debcred == 'deb') ? abs($cta['saldocuenta']) + abs($cta['saldoContrapartida']) : 0;
        $pase->credito = ($debcred == 'cred') ? abs($cta['saldocuenta']) + abs($cta['saldoContrapartida']) : 0;
        return $pase;
    }

    /**
     * Crea un pase para el comprobante.
     * @param Array $ctas
     * @param Integer $debcred
     * @return Array pase del comprobante
     */
    public function recorrerCtas($arrctas, $debcred)
    {
        $arrTmpPases = array();
        $totalImporte = 0;
        foreach ($arrctas as $v) {
            if (($v['saldocuenta'] !== 0) && ($v['saldocuenta'] !== '0.00')) {
                $arrTmpPases[] = $this->makePase($v, $debcred);
                $totalImporte += $v['saldocuenta'];
            }
        }
        return array($arrTmpPases, $totalImporte);
    }

    /**
     * Genera la Referencia del comprobante de cierre a partir del concepto.
     * @param Integer $concepto
     * @return String Referencia del comprobante de cierre a partir del concepto
     */
    public function generateReferenciaCmp($concepto)
    {
        if ($concepto == 1) {
            return 'Comprobante de Cierre, acreditando cuentas de gastos.';
        } else if ($concepto == 2) {
            return 'Comprobante de Cierre, debitando cuentas de ingresos.';
        } else if ($concepto == 4) {
            return 'Comprobante de Cierre, acreditando cuenta de utilidades.';
        } else if ($concepto == 5) {
            return 'Comprobante de Cierre, acreditando cuenta de p&eacute;rdidas.';
        }
    }

    /**
     * Adiciona los comprobantes del cierre.
     * @param Array $argparams (node)
     * @return boolean true if ocurred, false if failure
     */
    public function saveComprobante($argparams)
    {
        $save = $this->integrator->contabilidad->saveComprobante($argparams);
        $cad = explode("'id':", $save);
        if (count($cad) > 1) {
            $id = substr($cad[1], 0, -1);
            $this->saveConfCierreComprobante($id, $argparams['tipo']);
        }
        return $save;
    }

    /**
     * Guarda la asociacion entre el cierre contable y sus comprobantes.
     * @param Array $argparams (node)
     * @return boolean true if ocurred, false if failure
     */
    public function saveConfCierreComprobante($idcomprobante, $tipoCierre)
    {
        $tipo = ($tipoCierre == 1) ? 0 : (($tipoCierre == 2) ? 1 : 2);
        $idestructura = $this->global->Estructura->idestructura;
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("INSERT INTO mod_contabilidad.conf_cierrecomprobante (tipocierre,idestructura,idcomprobante) VALUES ($tipo,$idestructura,$idcomprobante);");
        return;
    }

    public function verificarComprobantesAsentados()
    {
        $datComprobante = new DatComprobante();
        $periodo = $datComprobante->obtenerPeriodo($this->global->Estructura->idestructura, 4);
        return $datComprobante->loadComprobantePorEstado($this->global->Estructura->idestructura, "8030,8033", $periodo[0]['idperiodo']);
    }

}
