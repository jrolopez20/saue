<?php

abstract class BaseConfCierrecontable extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.conf_cierrecontable');
        $this->hasColumn('idconfcierrecontable', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_contabilidad.sec_confcierrecontable'));
        $this->hasColumn('id', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('tabla', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('concepto', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcierrecontable', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
