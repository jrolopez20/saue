<?php

abstract class BaseDatCierrecontable extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.dat_cierrecontable');
        $this->hasColumn('idcierrecontable', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_contabilidad.sec_cierrecontable'));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
