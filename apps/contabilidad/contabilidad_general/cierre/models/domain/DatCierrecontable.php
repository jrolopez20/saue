<?php

/**
 * Clase dominio para configurar el Cierre Contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatCierrecontable extends BaseDatCierrecontable
{

    public function setUp()
    {
        parent:: setUp();
    }

    /**
     * Obtiene los datos del cierre para una estructura determinada.
     * @param Integer $idestructura
     * @return Array datos del cierre para una estructura determinada
     */
    public function getCierre($idestructura)
    {
        $query = Doctrine_Query::create();
        return $query->from('DatCierrecontable c')
                        ->where('c.idestructura = ?', $idestructura)
                        ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                        ->execute();
    }

    public function getConfCierreComprobante($idestructura)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT cc.*, dc.* FROM mod_contabilidad.conf_cierrecomprobante cc "
                        . "INNER JOIN mod_contabilidad.dat_comprobante dc ON dc.idcomprobante = cc.idcomprobante "
                        . "WHERE cc.idestructura = $idestructura;");
    }

}
