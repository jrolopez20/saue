<?php

/**
 * Clase dominio para configurar el Cierre Contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConfCierrecontable extends BaseConfCierrecontable
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('DatCierrecontable', array('local' => 'idcierrecontable', 'foreign' => 'idcierrecontable'));
    }

    /**
     * Carga la configuracion asociada al Cierre Contable.
     * @param Array $filter
     * @param Integer $idcierrecontable
     * @return Array configuracion asociada al Cierre Contable
     */
    public function loadConfCierrecontable($filter, $idcierrecontable)
    {
        extract($filter);
        $query = Doctrine_Query::create();
        return $query->from('ConfCierrecontable cc')
                        ->where('cc.idcierrecontable = ? AND cc.concepto = ?', array($idcierrecontable, $concepto))
                        ->orderBy('cc.tabla ASC')
                        ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                        ->execute();
    }

    /**
     * Busca si existe la configuracion asociada al Cierre Contable.
     * @param Integer $id
     * @param Integer $tabla
     * @return Array configuracion asociada al Cierre Contable
     */
    public function existConfCierrecontable($idcierrecontable, $id, $tabla)
    {
        $query = Doctrine_Query::create();
        return $query->from('ConfCierrecontable cc')
                        ->where('cc.idcierrecontable = ? AND cc.id = ? AND cc.tabla = ?', array($idcierrecontable, $id, $tabla))
                        ->orderBy('cc.tabla ASC')
                        ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                        ->execute();
    }

}
