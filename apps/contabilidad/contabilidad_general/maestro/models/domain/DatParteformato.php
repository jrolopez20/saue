<?php

/**
 * Clase dominio para configurar las Partes Formatos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatParteformato extends BaseDatParteformato
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatFormato', array('local' => 'idformato', 'foreign' => 'idformato'));
    }

    /**
     * Carga las Partes Formatos
     * @param Integer $idformato
     * @return Array Listado de las Parte Formatos
     */
    public function listParteFormatos($idformato)
    {
        $query = Doctrine_Query::create();
        $data_return = $query->select('p.*')
                ->from('DatParteformato p')
                ->where("p.idformato = $idformato")
                ->orderby("p.nivel")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return $data_return;
    }

    /**
     * Busca las Partes Formatos de un Formato dado (nombre, abreviatura, nivel)
     * @param Array $filter (idformato, nombre, abreviatura, nivel)
     * @return Boolean true si lo encuentra, false caso contrario
     */
    public function buscarParteFormato($filter)
    {
        extract($filter);
        $where = (!$idparteformato) ? "(p.nombre = '$nombre' OR p.abreviatura = '$abreviatura' OR p.nivel = $nivel) AND p.idformato = $idformato" :
                "(p.nombre = '$nombre' OR p.abreviatura = '$abreviatura' OR p.nivel = $nivel) AND p.idformato = $idformato AND p.idparteformato <> $idparteformato";

        $query = Doctrine_Query::create();
        $data_result = $query->select('p.*')
                ->from('DatParteformato p')
                ->where($where)
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return (count($data_result)) ? 1 : 0;
    }

    /**
     * Busca los Niveles de un Formato dado (nombre, abreviatura, nivel)
     * @param Array $filter (idformato, nombre, abreviatura, nivel)
     * @return Boolean true si lo encuentra, false caso contrario
     */
    public function buscarNivelFormato($filter)
    {
        extract($filter);
        $where = (!$idparteformato) ? "(p.nombre = '$nombre' OR p.abreviatura = '$abreviatura' OR p.nivel = $nivel) AND p.idformato = $idformato" :
                "(p.nombre = '$nombre' OR p.abreviatura = '$abreviatura' OR p.nivel = $nivel) AND p.idformato = $idformato AND p.idparteformato <> $idparteformato";

        $query = Doctrine_Query::create();
        $data_result = $query->select('p.*')
                ->from('DatParteformato p')
                ->where($where)
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return $data_result;
    }

}
