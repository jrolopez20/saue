<?php

/**
 * Clase dominio para configurar el Ejercicio Contable de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatEjerciciocontable extends BaseDatEjerciciocontable
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatPeriodocontable', array('local' => 'idejercicio', 'foreign' => 'idejercicio'));
        $this->hasMany('DatCierre', array('local' => 'idejercicio', 'foreign' => 'idejercicioactual'));
    }

    /**
     * Lista los datos de los Ejercicios Contables
     * @param Array $start, $limit, $filter
     * @return Array Listado de los Ejercicios Contables
     */
    public function loadEjercicios($start = 0, $limit = 0, $filter = 0)
    {
        $query = Doctrine_Query::create();
        $query->select('e.*')
                ->from('DatEjerciciocontable e')
                ->limit($limit)
                ->offset($start)
                ->orderby("e.nombre");

        ($filter) ? $query->addWhere("e.nombre = $filter") : '';
        $result = $query->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();
        return $result;
    }

    /**
     * Obtiene el ultimo de los Ejercicios Contables
     * @param none
     * @return stdClass Ejercicio 
     */
    public function getLastEjercicio()
    {
        $query = Doctrine_Query::create();
        $result = $query->select("e.*")
                ->from('DatEjerciciocontable e')
                ->orderby("e.fin DESC")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();
        return (count($result) === 0) ? 0 : $result[0];
    }

    /**
     * Obtiene el ultimo de los Ejercicios Contables
     * @param Integer $idejercicio
     * @return stdClass Ejercicio 
     */
    public function getEjercicioSiguiente($idejercicio)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT * FROM mod_maestro.dat_ejerciciocontable e
                                WHERE e.idejercicio > $idejercicio ORDER BY e.nombre");
    }

    /**
     * Obtiene el Ejercicio Contable por id.
     * @param Integer $idejercicio
     * @return stdClass Ejercicio 
     */
    public function getEjercicio($idejercicio)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT * FROM mod_maestro.dat_ejerciciocontable e
                                WHERE e.idejercicio = $idejercicio;");
    }

}
