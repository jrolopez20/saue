<?php

/**
 * Clase dominio para configurar la fecha de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatFecha extends BaseDatFecha
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatEstructurasubsist', array('local' => 'idestructurasubsist', 'foreign' => 'idestructurasubsist'));
    }

    /**
     * Lista las Fechas de los Subsistemas
     * @param Integer $start
     * @param Integer $limit
     * @param Array ($filter, $idestructuracomun)
     * @return Array Listado de las Fechas de los Subsistemas
     */
    public function loadFechas($start, $limit, $filter)
    {
        extract($filter);
        $where = "";
        if ($fecha) {
            $where.= "f.fecha = $fecha";
        } if ($where != '' && $idestructuracomun) {
            $where.= " AND es.idestructuracomun = $idestructuracomun";
        } else if ($idestructuracomun) {
            $where.= "es.idestructuracomun = $idestructuracomun";
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT DISTINCT f.*, s.denominacion as subsistema, s.idsubsistema "
                . "FROM mod_maestro.dat_fecha f "
                . "INNER JOIN mod_maestro.dat_estructurasubsist es ON f.idestructurasubsist = es.idestructurasubsist "
                . "INNER JOIN mod_maestro.nom_subsistema s ON es.idsubsistema = s.idsubsistema "
                . "WHERE $where ORDER BY f.fecha ASC LIMIT {$limit} OFFSET {$start};");

        return $data_return;
    }

    /**
     * Obtiene la cantidad total de Fechas
     * @param Array ($filter, $idestructuracomun)
     * @return Integer Cantidad total de Fechas
     */
    public function getTotalFechas($filter)
    {
        extract($filter);
        $query = Doctrine_Query::create();
        $query->select('f.*')
                ->from('DatFecha f')
                ->innerJoin('f.DatEstructurasubsist es')
                ->where('es.idestructuracomun=?', $idestructuracomun);

        ($fecha) ? $query->addWhere("f.fecha = $fecha") : '';
        $result = $query->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();
        return count($result);
    }

    /**
     * Obtiene los datos de la fecha.
     * @param Integer $idfecha
     * @return Integer datos de la fecha
     */
    public function getDataFecha($idfecha)
    {
        $query = Doctrine_Query::create();
        return $query->select('f.*')
                        ->from('DatFecha f')
                        ->where('f.idfecha=?', $idfecha)
                        ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                        ->execute();
    }

}
