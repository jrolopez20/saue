<?php

/**
 * Clase dominio para asociar el Modelo contable a las cuentas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConfModelocontablecuenta extends BaseConfModelocontablecuenta
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomModelocontable', array('local' => 'idmodelocontable', 'foreign' => 'idmodelocontable'));
        $this->hasOne('NomCuenta', array('local' => 'idcuenta', 'foreign' => 'idcuenta'));
    }

    /**
     * Lista las Cuentas contables asociadas al modelo.
     * @param Integer $idcuentapadre identificador del nodo padre
     * @return Array Listado de las Cuentas contables asociadas al modelo
     */
    public function loadDataPases($idmodelocontable)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $SELECT = "nc.idcuenta, (nc.concatcta ||' '|| nc.denominacion) as text, (nc.concatcta ||' '|| nc.denominacion) as qtip, "
                . "( (nc.ordender - nc.ordenizq) = 1) as leaf, nc.codigo, nc.concatcta, nc.denominacion, nc.denominacion as denomcuenta, "
                . "nc.idcuentapadre, nc.idparteformato, nc.nivel, nn.idnaturaleza, "
                . "nn.descripcion as naturaleza, pf.longitud as longitud, df.separador as separador, mcta.*";
        $WHERE = "mcta.idmodelocontable = $idmodelocontable";
        $data_return = $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta nc "
                . "INNER JOIN mod_maestro.conf_modelocontablecuenta mcta ON mcta.idcuenta = nc.idcuenta "
                //. "INNER JOIN mod_contabilidad.nom_contenido cont ON cont.idcontenido = nc.idcontenido "
                . "INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = nc.idnaturaleza "
                . "INNER JOIN mod_maestro.dat_parteformato pf ON nc.idparteformato = pf.idparteformato "
                . "INNER JOIN mod_maestro.dat_formato df ON df.idformato = pf.idformato "
                . "WHERE $WHERE "
                . "ORDER BY mcta.orden, nc.codigo ASC;");
        return $data_return;
    }

    /**
     * Verifica si existe el pase para un mismo modelo.
     * @param Integer $idmodelocontable
     * @param Integer $idcuenta
     * @return Array Listado de los Modelos Contables
     */
    public function existePase($idmodelocontable, $idcuenta)
    {
        $query = Doctrine_Query::create();
        $result = $query->from('ConfModelocontablecuenta mcta')
                ->where("mcta.idmodelocontable = $idmodelocontable AND mcta.idcuenta = $idcuenta")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return (count($result)) ? true : false;
    }

    /**
     * Verifica si la Cuenta Contable esta en uso.
     * @param Integer $idcuenta
     * @return boolean
     */
    public function findUseCta($idcuenta)
    {
        $query = Doctrine_Query::create();
        $result = $query->from('ConfModelocontablecuenta c')
                ->where("c.idcuenta = $idcuenta")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return (count($result)) ? true : false;
    }

}
