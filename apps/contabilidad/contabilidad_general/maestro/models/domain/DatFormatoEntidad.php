<?php

/**
 * Clase dominio para asociar los Formatos a la Entidad.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatFormatoEntidad extends BaseDatFormatoEntidad
{

    public function setUp()
    {
        $this->hasOne('DatFormato', array('local' => 'idformato', 'foreign' => 'idformato'));
        parent::setUp();
    }

}
