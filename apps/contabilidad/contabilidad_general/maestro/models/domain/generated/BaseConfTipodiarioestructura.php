<?php

abstract class BaseConfTipodiarioestructura extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.conf_tipodiarioestructura');
        $this->hasColumn('idtipodiarioestructura', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_idtipodiarioestructura'));
        $this->hasColumn('idestructuracomun', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idtipodiario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
