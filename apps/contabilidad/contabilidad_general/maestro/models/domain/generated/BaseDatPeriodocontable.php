<?php

abstract class BaseDatPeriodocontable extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_periodocontable');
        $this->hasColumn('idperiodo', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datperiodocontable'));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('inicio', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fin', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idejercicio', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
