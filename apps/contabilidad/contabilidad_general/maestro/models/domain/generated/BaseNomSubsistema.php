<?php

abstract class BaseNomSubsistema extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_subsistema');
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => false, 'primary' => true));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('crc', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('uri', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('cierra', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('fecha', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('precedencia', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('contabiliza', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
