<?php

abstract class BaseNomEstadosubsistema extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_estadosubsistema');
        $this->hasColumn('idestado', 'numeric', null, array('notnull' => false, 'primary' => true));
        $this->hasColumn('estado', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}
