<?php

abstract class BaseDatEjerciciocontable extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_ejerciciocontable');
        $this->hasColumn('idejercicio', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datejerciciocontable'));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('inicio', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fin', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('antecesor', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}
