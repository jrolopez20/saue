<?php

abstract class BaseDatFormatoEntidad extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_formatoentidad');
        $this->hasColumn('idformatoentidad', 'serial', 19, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_maestro.dat_formatoentidad_idformatoentidad'));
        $this->hasColumn('idformato', 'numeric', 19, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idestructuracomun', 'numeric', 19, array('notnull' => true, 'primary' => false));
    }

}
