<?php

abstract class BaseConfModelocontablecuenta extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.conf_modelocontablecuenta');
        $this->hasColumn('idmodelocontablecuenta', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.conf_modelocontablecuenta'));
        $this->hasColumn('idmodelocontable', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcuenta', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('debitocredito', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('orden', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
