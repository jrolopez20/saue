<?php

abstract class BaseDatEstructurasubsist extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_estructurasubsist');
        $this->hasColumn('idestructurasubsist', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datestructurasubsist'));
        $this->hasColumn('aperturado', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestructuracomun', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}
