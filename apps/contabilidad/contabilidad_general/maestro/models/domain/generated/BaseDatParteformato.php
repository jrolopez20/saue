<?php

abstract class BaseDatParteformato extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_parteformato');
        $this->hasColumn('idparteformato', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datparteformato'));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('abreviatura', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('nivel', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('longitud', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idformato', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => true, 'primary' => false));
    }

}
