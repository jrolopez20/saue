<?php

abstract class BaseNomModelocontable extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_modelocontable');
        $this->hasColumn('idmodelocontable', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_nommodelocontable'));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}
