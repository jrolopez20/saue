<?php

abstract class BaseNomTipodiario extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_tipodiario');
        $this->hasColumn('idtipodiario', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_idtipodiario'));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('codigo', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
