<?php

abstract class BaseDatFecha extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_fecha');
        $this->hasColumn('idfecha', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datfecha'));
        $this->hasColumn('fecha', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('incremento', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fechasist', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idestructurasubsist', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}
