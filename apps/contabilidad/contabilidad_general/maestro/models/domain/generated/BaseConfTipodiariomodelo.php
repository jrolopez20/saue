<?php

abstract class BaseConfTipodiariomodelo extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.conf_tipodiariomodelo');
        $this->hasColumn('idtipodiariomodelocontable', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_conftipodiarioconceptocontable'));
        $this->hasColumn('idtipodiario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idmodelocontable', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
