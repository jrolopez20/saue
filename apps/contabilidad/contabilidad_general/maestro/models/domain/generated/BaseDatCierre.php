<?php

abstract class BaseDatCierre extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_cierre');
        $this->hasColumn('idcierre', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datcierre'));
        $this->hasColumn('idestructurasubsist', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idperiodoactual', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idejercicioactual', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
