<?php

abstract class BaseDatFormato extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_formato');
        $this->hasColumn('idformato', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datformato'));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('separador', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('estandar', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idestructuracomun', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}
