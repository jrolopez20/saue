<?php

/**
 * Clase dominio para configurar los Tipo de Diario.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomTipodiario extends BaseNomTipodiario
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('ConfTipodiarioestructura', array('local' => 'idtipodiario', 'foreign' => 'idtipodiario'));
        $this->hasMany('ConfTipodiariomodelo', array('local' => 'idtipodiario', 'foreign' => 'idtipodiario'));
        $this->hasMany('NomSubsistema', array('local' => 'idsubsistema', 'foreign' => 'idsubsistema'));
    }

    /**
     * Lista los datos de los Tipo de Diario
     * @param Array $argparams (start, limit)
     * @return Array Listado de los Tipo de Diario
     */
    public function listarTipoDiario($argparams)
    {
        $start = ($argparams['start']) ? $argparams['start'] : 0;
        $limit = ($argparams['limit']) ? $argparams['limit'] : 1000;
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT t.*, s.denominacion as subsistema FROM mod_maestro.nom_tipodiario t "
                . "INNER JOIN mod_maestro.nom_subsistema s ON s.idsubsistema = t.idsubsistema "
                . "ORDER BY s.denominacion, t.denominacion ASC LIMIT $limit OFFSET $start;");
        return $data_return;
    }

    /**
     * Obtiene el total de los Tipo de Diario
     * @param none
     * @return Integer total de los Tipo de Diario
     */
    public function getTotalTipoDiario()
    {
        $query = Doctrine_Query::create();
        $data_return = $query->select('t.*')
                ->from('NomTipodiario t')
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return count($data_return);
    }

    /**
     * Busca los Tipo de Diario
     * @param Array $argparams
     * @return Array Listado de los Tipo de Diario
     */
    public function buscarTipoDiario($argparams)
    {
        extract($argparams);
        $query = Doctrine_Query::create();
        $query->select('t.*')
                ->from('NomTipodiario t');

        ($codigo) ? $query->addWhere("t.codigo = $codigo") : '';
        $data_return = $query->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();

        return $data_return;
    }

    /**
     * Lista los datos de los Tipo de Diario asociados al Subsistema
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Tipo de Diario
     */
    public function listarTipoDiarioSubsistema($argparams)
    {
        $start = ($argparams['start']) ? $argparams['start'] : 0;
        $limit = ($argparams['limit']) ? $argparams['limit'] : 1000;
        $query = "SELECT t.*, s.denominacion as subsistema FROM mod_maestro.nom_tipodiario t "
                . "INNER JOIN mod_maestro.nom_subsistema s ON s.idsubsistema = t.idsubsistema ";
        if ($argparams['idsubsistema']) {
            $id = $argparams['idsubsistema'];
            $query.= " WHERE t.idsubsistema = $id";
        }
        $query .= " ORDER BY s.denominacion, t.denominacion ASC LIMIT $limit OFFSET $start;";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll($query);
    }

}
