<?php

class DatCierre extends BaseDatCierre
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatEjerciciocontable', array('local' => 'idejercicioactual', 'foreign' => 'idejercicio'));
        $this->hasOne('DatEstructurasubsist', array('local' => 'idestructurasubsist', 'foreign' => 'idestructurasubsist'));
        $this->hasOne('DatPeriodocontable', array('local' => 'idperiodoactual', 'foreign' => 'idperiodo'));
        $this->hasOne('DatEjerciciocontable', array('local' => 'idejercicioactual', 'foreign' => 'idejercicio'));
        $this->hasOne('DatEstructurasubsist', array('local' => 'idestructurasubsist', 'foreign' => 'idestructurasubsist'));
        $this->hasOne('DatPeriodocontable', array('local' => 'idperiodoactual', 'foreign' => 'idperiodo'));
    }

    /**
     * Busca si el Ejercicio se encuentra en uso
     * @param Integer $idejercicio identificador del ejercicio
     * @return Array
     */
    public function getCierreByEjercicio($idejercicio)
    {
        $query = Doctrine_Query::create();
        $result = $query->select('d.*')
                ->from('DatCierre d')
                ->where("d.idejercicioactual = $idejercicio")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();
        return $result;
    }

    /**
     * Devuelve los datos del Cierre para el subsistema.
     * @param Integer $idSubsistema
     * @param Integer $idestructura
     * @return Array datos del Cierre para el subsistema
     */
    public function getCierre($idSubsistema, $idestructura)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT dc.*, f.* FROM mod_maestro.dat_cierre dc "
                        . "INNER JOIN mod_maestro.dat_fecha f ON dc.idestructurasubsist = f.idestructurasubsist "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON f.idestructurasubsist = es.idestructurasubsist "
                        . "WHERE es.idsubsistema = $idSubsistema AND es.idestructuracomun = $idestructura;");
    }

}
