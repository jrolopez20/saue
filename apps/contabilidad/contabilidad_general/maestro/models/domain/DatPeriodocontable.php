<?php

/**
 * Clase dominio para configurar los Periodos Contables de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatPeriodocontable extends BaseDatPeriodocontable
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatEjerciciocontable', array('local' => 'idejercicio', 'foreign' => 'idejercicio'));
        $this->hasOne('DatEjerciciocontable', array('local' => 'idejercicio', 'foreign' => 'idejercicio'));
        $this->hasMany('DatCierre', array('local' => 'idperiodo', 'foreign' => 'idperiodoactual'));
    }

    /**
     * Lista los datos de los Periodos Contables
     * @param Array $start, $limit, $idejercicio
     * @return Array Listado de los Periodos Contables
     */
    public function loadPeriodos($start = 0, $limit = 0, $idejercicio = 0)
    {
        $query = Doctrine_Query::create();
        $result = $query->select('p.*')
                ->from('DatPeriodocontable p')
                ->where("p.idejercicio = $idejercicio")
                ->limit($limit)
                ->offset($start)
                ->orderby("p.inicio")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return $result;
    }

    /**
     * Devuelve el Periodo COntable dada la Fecha.
     * @param Integer $idfecha
     * @return Array Periodo Contable
     */
    public function loadPriodoByFecha($idfecha)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT p.*, f.fecha FROM mod_maestro.dat_periodocontable p "
                . "INNER JOIN mod_maestro.dat_cierre c ON p.idperiodo = c.idperiodoactual "
                . "INNER JOIN mod_maestro.dat_fecha f ON c.idestructurasubsist = f.idestructurasubsist "
                . "WHERE f.idfecha = $idfecha;");

        return $data_return;
    }

    /**
     * Devuelve el Periodo Contable para el subsistema.
     * @param Integer $idestructura
     * @param Integer $idSubsistema
     * @return Array Periodo Contable para el subsistema
     */
    public function getPeriodoSubsistema($idestructura, $idSubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT p.*, f.fecha FROM mod_maestro.dat_periodocontable p "
                        . "INNER JOIN mod_maestro.dat_cierre c ON p.idperiodo = c.idperiodoactual "
                        . "INNER JOIN mod_maestro.dat_fecha f ON c.idestructurasubsist = f.idestructurasubsist "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON f.idestructurasubsist = es.idestructurasubsist "
                        . "INNER JOIN mod_maestro.nom_subsistema s ON es.idsubsistema = s.idsubsistema "
                        . "WHERE s.idsubsistema = $idSubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Devuelve el Periodo Contable siguiente.
     * @param Integer $idperiodo
     * @param Integer $idejercicio
     * @param Date $fechafin
     * @return Array Periodo Contable siguiente
     */
    public function getNextPeriodo($idperiodo, $idejercicio, $fechafin)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT p.* FROM mod_maestro.dat_periodocontable p "
                        . "WHERE p.inicio >= '$fechafin' AND p.idejercicio = $idejercicio AND p.idperiodo <> $idperiodo "
                        . "ORDER BY p.inicio ASC LIMIT 1 OFFSET 0;");
    }

}
