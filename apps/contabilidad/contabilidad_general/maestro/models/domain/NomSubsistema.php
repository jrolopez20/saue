<?php

/**
 * Clase dominio para configurar los Subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomSubsistema extends BaseNomSubsistema
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstructurasubsist', array('local' => 'idsubsistema', 'foreign' => 'idsubsistema'));
    }

    /**
     * Carga los Subsistemas
     * @return Array Listado de los Subsistemas
     */
    public function listSubsistemas()
    {
        $query = Doctrine_Query::create();
        return $query->select('s.*')
                        ->from('NomSubsistema s')
                        ->orderby("s.denominacion")
                        ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                        ->execute();
    }

    /**
     * Carga los Subsistemas configurados para la Entidad
     * @param Integer $idestructura
     * @return Array Listado de los Subsistemas
     */
    public function listSubsistemasConfigurados($idestructura)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT s.* FROM mod_maestro.nom_subsistema s "
                        . "WHERE s.idsubsistema IN (SELECT es.idsubsistema FROM mod_maestro.dat_estructurasubsist es "
                        . "WHERE es.idestructuracomun = $idestructura) "
                        . "ORDER BY s.denominacion ASC;");
    }

    /**
     * Carga los Subsistemas que contabilizan.
     * @param none
     * @return Array Listado de los Subsistemas que contabilizan
     */
    public function listSubsistemasContabiliza()
    {
        $query = Doctrine_Query::create();
        return $query->select('s.*')
                        ->from('NomSubsistema s')
                        ->where('s.contabiliza = 1')
                        ->orderby("s.denominacion")
                        ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                        ->execute();
    }

    /**
     * Carga los Subsistemas no configurados para la Entidad
     * @param Integer $idestructura
     * @return Array Listado de los Subsistemas
     */
    public function listSubsistemasNoConfigurados($idestructura)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT s.* FROM mod_maestro.nom_subsistema s "
                        . "WHERE s.idsubsistema NOT IN (SELECT es.idsubsistema FROM mod_maestro.dat_estructurasubsist es "
                        . "WHERE es.idestructuracomun = $idestructura) "
                        . "ORDER BY s.denominacion ASC;");
    }

    /**
     * Carga el Subsistema dado su identificador
     * @return Array Subsistema buscado
     */
    public function getSubsistema($idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT s.*, df.* FROM mod_maestro.nom_subsistema s "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es  ON s.idsubsistema = es.idsubsistema "
                        . "INNER JOIN mod_maestro.dat_fecha df  ON es.idestructurasubsist = df.idestructurasubsist "
                        . "WHERE s.idsubsistema = $idsubsistema;");
    }

    /**
     * Carga la precedencia del subsistema
     * @param Integer $idestructura
     * @param Integer $topPrecedencia
     * @return Array precedencia del subsistema
     */
    public function getSubsistemasPrecedentes($idestructura, $topPrecedencia)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT s.*, df.* FROM mod_maestro.nom_subsistema s "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es  ON s.idsubsistema = es.idsubsistema "
                        . "INNER JOIN mod_maestro.dat_fecha df  ON es.idestructurasubsist = df.idestructurasubsist "
                        . "WHERE es.idestructuracomun = $idestructura AND s.cierra= 1 "
                        . "AND s.precedencia < $topPrecedencia ORDER BY s.denominacion ASC;");
    }

}
