<?php

/**
 * Clase dominio para configurar los Formatos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatFormato extends BaseDatFormato
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatParteformato', array('local' => 'idformato', 'foreign' => 'idformato'));
        $this->hasMany('DatFormatoEntidad', array('local' => 'idformato', 'foreign' => 'idformato'));
        $this->hasMany('NomSubsistema', array('local' => 'idsubsistema', 'foreign' => 'idsubsistema'));
    }

    /**
     * Lista los datos de los Formatos
     * @param Array $start, $limit, $filter, $idestructura
     * @return Array Listado de los Formatos
     */
    public function loadFormatos($start, $limit, $filter, $idestructura)
    {
        $where = "";
        if ($filter['nombre'] && $where != '') {
            $where.= " and e.nombre ILIKE '%" . $filter['nombre'] . "%'";
        } elseif ($filter['nombre']) {
            $where.= " e.nombre ILIKE '%" . $filter['nombre'] . "%'";
        }if ($filter != '') {
            $where.= " AND (fe.idestructuracomun = $idestructura OR estandar = 1)";
        } else {
            $where.= "fe.idestructuracomun = $idestructura OR estandar = 1";
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT DISTINCT e.*, s.denominacion as subsistema, "
                . "(CASE WHEN e.idestructuracomun = $idestructura THEN 1 ELSE 0 END) as propietario FROM mod_maestro.dat_formato e  "
                . "LEFT JOIN mod_maestro.dat_formatoentidad fe ON e.idformato = fe.idformato "
                . "INNER JOIN mod_maestro.nom_subsistema s ON e.idsubsistema = s.idsubsistema "
                . "WHERE $where ORDER BY s.denominacion,e.nombre ASC LIMIT {$limit} OFFSET {$start};");
        return $data_return;
    }

    /**
     * Devuelve la cantidad de Formatos
     * @param Array $filter, $idestructura
     * @return Integer Cantidad de Formatos
     */
    public function getCantFormatos($filter, $idestructura)
    {
        $query = Doctrine_Query::create();
        $query->select('fe.*')
                ->from('DatFormatoEntidad fe')
                ->where("fe.idestructuracomun = $idestructura");

        ($filter) ? $query->addWhere(" AND f.nombre = $filter") : '';
        $cant = $query->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();

        return count($cant);
    }

    /**
     * Busca el Formato por nombre
     * @param Array $filter (nombre , idestructuracomun)
     * @return Boolean true si lo encuentra, false caso contrario
     */
    public function buscarFormato($filter)
    {
        extract($filter);
        $query = Doctrine_Query::create();
        $data_result = $query->select('f.*')
                ->from('DatFormato f')
                ->leftJoin('f.DatFormatoEntidad fe')
                ->where("(f.nombre = '$nombre' AND fe.idestructuracomun = $idestructuracomun) OR (f.nombre = '$nombre' AND f.estandar = 1)")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return (count($data_result)) ? 1 : 0;
    }

    /**
     * Lista los datos de los Formatos para Heredar
     * @param Integer $idestructura identificador de la estrutura logueada
     * @param Integer $idpadre identificador del padre de la estrutura logueada
     * @return Array Listado de los Formatos
     */
    public function loadFormatosHeredar($idestructura, $idpadre)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT DISTINCT f.*, s.denominacion as subsistema, "
                . "(CASE WHEN f.idestructuracomun = $idestructura THEN 1 ELSE 0 END) as propietario "
                . "FROM mod_maestro.dat_formato f "
                . "LEFT JOIN mod_maestro.dat_formatoentidad fe on f.idformato = fe.idformato "
                . "INNER JOIN mod_maestro.nom_subsistema s ON f.idsubsistema = s.idsubsistema "
                . "WHERE (f.idestructuracomun = $idpadre OR fe.idestructuracomun = $idpadre) "
                . "AND f.idformato NOT IN(SELECT d.idformato FROM  mod_maestro.dat_formatoentidad d "
                . "WHERE d.idestructuracomun = $idestructura)"
                . "AND f.estandar = 0 ORDER BY s.denominacion, f.nombre ASC;");

        return $data_return;
    }

    /**
     * Obtiene los formatos asociados al subsistema
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array formatos asociados al subsistema
     */
    public function loadFormatosSubsistema($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT DISTINCT f.* "
                . "FROM mod_maestro.dat_formato f "
                . "INNER JOIN mod_maestro.dat_formatoentidad fe on f.idformato = fe.idformato "
                . "INNER JOIN mod_maestro.nom_subsistema s ON f.idsubsistema = s.idsubsistema "
                . "WHERE (fe.idestructuracomun = $idestructura AND f.idsubsistema = $idsubsistema) OR "
                . "(f.estandar = 1 AND f.idsubsistema = $idsubsistema) "
                . "ORDER BY f.nombre ASC;");
        return $data_return;
    }

}
