<?php

class DatEstructurasubsist extends BaseDatEstructurasubsist
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('NomEstadosubsistema', array('local' => 'idestado', 'foreign' => 'idestado'));
        $this->hasOne('NomSubsistema', array('local' => 'idsubsistema', 'foreign' => 'idsubsistema'));
        $this->hasMany('DatFecha', array('local' => 'idestructurasubsist', 'foreign' => 'idestructurasubsist'));
        $this->hasMany('DatCierre', array('local' => 'idestructurasubsist', 'foreign' => 'idestructurasubsist'));
    }

}
