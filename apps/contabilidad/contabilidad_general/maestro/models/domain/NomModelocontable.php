<?php

class NomModelocontable extends BaseNomModelocontable
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('ConfTipodiariomodelo', array('local' => 'idmodelocontable', 'foreign' => 'idmodelocontable'));
    }

    /**
     * Lista los datos de los Modelos Contables
     * @param Integer $start
     * @param Integer $limit
     * @param Integer $filter
     * @return Array Listado de los Modelos Contables
     */
    public function loadModelo($start = 0, $limit = 0, $filter = 0)
    {
        $query = Doctrine_Query::create();
        $query->from('NomModelocontable n')
                ->limit($limit)
                ->offset($start)
                ->orderby("n.denominacion");

        ($filter) ? $query->addWhere("n.denominacion = $filter") : '';
        $result = $query->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();
        return $result;
    }

    /**
     * Obtiene la cantidad de Modelos Contables
     * @param Array $filter
     * @return Array Listado de los Modelos Contables
     */
    public function getCantModelos($filter = 0)
    {
        $query = Doctrine_Query::create();
        $query->from('NomModelocontable n');

        ($filter) ? $query->addWhere("n.denominacion = $filter") : '';
        $cant = $query->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();

        return count($cant);
    }

    /**
     * Verifica si el Modelos Contable esta en uso
     * @param Integer $idmodelo
     * @return Array Listado de los Modelos Contables
     */
    public function findUseModelo($idmodelo)
    {
        $query = Doctrine_Query::create();
        $result = $query->from('NomModelocontable n')
                ->innerjoin('n.ConfTipodiariomodelo t')
                ->where("t.idmodelocontable = $idmodelo")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return $result;
    }

}
