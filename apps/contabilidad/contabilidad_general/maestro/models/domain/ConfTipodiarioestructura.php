<?php

class ConfTipodiarioestructura extends BaseConfTipodiarioestructura
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomTipodiario', array('local' => 'idtipodiario', 'foreign' => 'idtipodiario'));
        $this->hasOne('NomSubsistema', array('local' => 'idsubsistema', 'foreign' => 'idsubsistema'));
    }

    /**
     * Verifica si el Tipo de Diario  esta asociado a estructura
     * @param Integer $idtipodiario
     * @return boolean true if ocurred, false if failure
     */
    public function isAsociado($idtipodiario)
    {
        $query = Doctrine_Query::create();
        $data_return = $query->select('te.*')
                ->from('ConfTipodiarioestructura te')
                ->where("te.idtipodiario = $idtipodiario")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return count($data_return) ? true : false;
    }

    /**
     * Lista los datos de los Tipo de Diario y sus usos.
     * @param Integer $idestructuracomun identificador de la estructura
     * @param Integer $idsubsistema identificador del subsistema
     * @param Integer $start inicio lectura de los datos
     * @param Integer $limit fin lectura de los datos
     * @param Integer $filter filtro a aplicar (codigo o denominacion)
     * @return Array Listado de los Tipo de Diario y sus usos.
     */
    public function getTipoDiarioEstructura($idestructuracomun, $idsubsistema, $start = 0, $limit = 0, $filter = null)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT ntd.idtipodiario, ntd.codigo, ntd.denominacion, ntd.descripcion, "
                . "ctd.idtipodiarioestructura, (CASE WHEN ctd.idtipodiarioestructura IS NULL THEN 0 ELSE 1 END) as uso "
                . "FROM mod_maestro.nom_tipodiario ntd "
                . "LEFT JOIN mod_maestro.conf_tipodiarioestructura ctd "
                . "ON (ntd.idtipodiario = ctd.idtipodiario AND ctd.idestructuracomun = {$idestructuracomun} AND ctd.idsubsistema = {$idsubsistema}) ";

        if (!empty($filter)) {
            $sql .= "WHERE ntd.codigo ilike '%$filter%' OR ntd.denominacion ilike '%$filter%'";
        }
        $sql .= "LIMIT {$limit} OFFSET {$start};";

        $stmt = $query->getConnection()->prepare($sql);

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

}
