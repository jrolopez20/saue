<?php

/**
 * Clase dominio para asociar el Tipo diario al Modelo contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConfTipodiariomodelo extends BaseConfTipodiariomodelo
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomTipodiario', array('local' => 'idtipodiario', 'foreign' => 'idtipodiario'));
        $this->hasOne('NomModelocontable', array('local' => 'idmodelocontable', 'foreign' => 'idmodelocontable'));
    }

    /**
     * Verifica si el Tipo de Diario  esta asociado a modelo
     * @param Integer $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function isAsociado($idtipodiario)
    {
        $query = Doctrine_Query::create();
        $data_return = $query->select('ct.*')
                ->from('ConfTipodiariomodelo ct')
                ->where("ct.idtipodiario = $idtipodiario")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return count($data_return) ? true : false;
    }

    /**
     * Obtiene los Modelos asociados al Tipo Diario
     * @param Integer $start
     * @param Integer $limit
     * @param Integer $idtipodiario
     * @return Array Modelos asociados
     */
    public function getModeloAsociado($start, $limit, $idtipodiario)
    {
        $query = Doctrine_Query::create();
        $data_return = $query->select('nc.*')
                ->from('NomModelocontable nc')
                ->innerJoin('nc.ConfTipodiariomodelo td')
                ->where("td.idtipodiario = $idtipodiario")
                ->limit($limit)
                ->offset($start)
                ->orderby("nc.denominacion")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return $data_return;
    }

    /**
     * Obtiene el total de los Modelos asociados al Tipo Diario
     * @param Integer $idtipodiario
     * @return Intger Cantidad de Modelos asociados
     */
    public function getTotalModeloAsociado($idtipodiario)
    {
        $query = Doctrine_Query::create();
        $data_return = $query->select('nc.*')
                ->from('NomModelocontable nc')
                ->innerJoin('nc.ConfTipodiariomodelo td')
                ->where("td.idtipodiario = $idtipodiario")
                ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                ->execute();

        return count($data_return);
    }

    /**
     * Obtiene los Modelos no asociados al Tipo Diario
     * @param Integer $start
     * @param Integer $limit
     * @param Integer $idtipodiario
     * @return Array Modelos no asociados
     */
    public function getModeloNoAsociado($start, $limit, $idtipodiario)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT cc.* FROM mod_maestro.nom_modelocontable cc "
                . "WHERE cc.idmodelocontable NOT IN "
                . "(SELECT tdc.idmodelocontable FROM mod_maestro.conf_tipodiariomodelo tdc "
                . "INNER JOIN mod_maestro.nom_tipodiario td ON tdc.idtipodiario = td.idtipodiario "
                . "WHERE td.idtipodiario = $idtipodiario) ORDER BY cc.denominacion ASC LIMIT {$limit} OFFSET {$start};");
        return $data_return;
    }

    /**
     * Obtiene el total de los Modelos no asociados al Tipo Diario
     * @param Integer $idtipodiario
     * @return Integer Cantidad de Modelos no asociados
     */
    public function getTotalModeloNoAsociado($idtipodiario)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT cc.* FROM mod_maestro.nom_modelocontable cc "
                . "WHERE cc.idmodelocontable NOT IN "
                . "(SELECT tdc.idmodelocontable FROM mod_maestro.conf_tipodiariomodelo tdc "
                . "INNER JOIN mod_maestro.nom_tipodiario td ON tdc.idtipodiario = td.idtipodiario "
                . "WHERE td.idtipodiario = $idtipodiario) ORDER BY cc.denominacion ASC;");
        return count($data_return);
    }

}
