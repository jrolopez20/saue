<?php

/**
 * Clase modelo para configurar la fecha de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatFechaModel extends ZendExt_Model
{

    public function DatFechaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de las fechas registradas
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de las fehas configuradas por subsistema
     */
    public function listDataFechas($argparams)
    {
        if ($argparams['filter'])
            $argparams['filter'][] = array('idestructuracomun' => $this->global->Estructura->idestructura);
        else
            $argparams['filter'] = array('idestructuracomun' => $this->global->Estructura->idestructura);
        $datFecha = new DatFecha();
        $datos = $datFecha->loadFechas($argparams['start'], $argparams['limit'], $argparams['filter']);
        $total = $datFecha->getTotalFechas($argparams['filter']);
        if (count($datos)) {
            $this->addPeriodoFecha($datos);
        }
        return array('datos' => $datos, 'total' => $total);
    }

    /**
     * Lista los subsistemas no configurados aun
     * @return Array Listado de los subsistemas
     */
    public function loadSubsistemasNoConfigurados()
    {
        $NomSubsistema = new NomSubsistema();
        $subsist = $NomSubsistema->listSubsistemasNoConfigurados($this->global->Estructura->idestructura);
        return ($subsist) ? array('datos' => $subsist) : array('datos' => array());
    }

    /**
     * Lista los ejercicios contables configurados
     * @param Array $argparams (filter)
     * @return Array Listado de los ejericios contables configurados
     */
    public function loadEjercicios()
    {
        $ejercicioModel = new DatEjerciciocontableModel();
        return $ejercicioModel->listDataEjercicio();
    }

    /**
     * Lista los periodos contables configurados
     * @param Array $argparams (idejercicio) 
     * @return Array Listado de los periodos contables configurados
     */
    public function loadPeriodos($argparams)
    {
        if (!$argparams['idfecha']) {
            $periodoModel = new DatPeriodocontableModel();
            return $periodoModel->listDataPeriodo(array('start' => 0, 'limit' => 0, 'idejercicio' => $argparams['idejercicio']));
        } else {
            $datPeriodo = new DatPeriodocontable();
            return $datPeriodo->loadPriodoByFecha($argparams['idfecha']);
        }
    }

    /**
     * Adiciona la fecha
     * @param Array $argparams (filter)
     * @return boolean true if ocurred, false if failure
     */
    public function addDate($argparams)
    {
        $estSubsistModel = new DatEstructurasubsistModel();
        $cierreModel = new DatCierreModel();
        $idEstSubsis = $estSubsistModel->addEstructuraSubsistema($argparams['idsubsistema']);
        if ($idEstSubsis) {
            $cierreModel->addCierre($idEstSubsis, $argparams['idejercicio'], $argparams['idperiodo']);
            $objFecha = new DatFecha();
            $objFecha->fecha = $argparams['fecha'];
            $objFecha->incremento = $argparams['incremento'];
            $objFecha->idestructurasubsist = $idEstSubsis;
            $success = $this->Insertar($objFecha);
            return "{'success':$success, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaAdicionada}";
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgFallido}";
        }
    }

    /**
     * Modifica la fecha
     * @param Array $argparams (idfecha, incremento)
     * @return boolean true if ocurred, false if failure
     */
    public function updateDate($argparams)
    {
        $objFecha = Doctrine::getTable('DatFecha')->find($argparams['idfecha']);
        $objFecha->incremento = ($argparams['incremento'] == 0) ? 1 : 0;
        try {
            $objFecha->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaAdicionada}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina la Fecha
     * @param Array $argparams (idfecha)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteDate($argparams)
    {
        if (!$this->comprobarUsoFecha($argparams['idfecha'])) {
            $objFecha = Doctrine::getTable('DatFecha')->find($argparams['idfecha']);
            $cierreModel = new DatCierreModel();
            $cierreModel->delCierre($objFecha->idestructurasubsist); //eliminar de cierre
            $this->delete($objFecha); //eliminar de fecha
            $esructSubsistema = new DatEstructurasubsistModel();
            $esructSubsistema->delEstructuraSubsist($objFecha->idestructurasubsist); //eliminar de estructsubsist
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaEliminada}";
        }return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgFallido}";
    }

    /**
     * Elimina los datos de la Fecha en CASCADA
     * @param stdClass $objFecha 
     * @return boolean true if ocurred, false if failure
     */
    public function delete($objFecha)
    {
        try {
            $objFecha->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaEliminada}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Verifica si la Fecha esta en uso
     * @param Integer $idfecha Identificador de la fecha
     * @return boolean true if ocurred, false if failure
     */
    public function comprobarUsoFecha($idfecha)
    {
        //implementar validaciones aqui
        return false; //cable para que elimine
    }

    /**
     * Pasa la fecha en caso de Manual
     * @param Array $argparams (idfecha)
     * @return date fecha que paso
     */
    public function nextDate($argparams)
    {
        $objFecha = Doctrine::getTable('DatFecha')->find($argparams['idfecha']);
        $objFecha->fecha = $argparams['fecha'];
        try {
            $objFecha->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaCambiada}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Adiciona la Fecha.
     * @param stdClass $objFecha
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($objFecha)
    {
        try {
            $objFecha->save();
            return true;
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Agrega el periodo actual de la fecha parametrizada.
     * @param Arrar $arrayfechas
     * @return void
     */
    public function addPeriodoFecha(&$arrayfechas)
    {
        $datPeriodo = new DatPeriodocontable();
        foreach ($arrayfechas as &$v) {
            $periodo = $datPeriodo->loadPriodoByFecha($v['idfecha']);
            $v['iniperiodo'] = $periodo[0]['inicio'];
            $v['finperiodo'] = $periodo[0]['fin'];
        }
    }

}
