<?php

/**
 * Clase modelo para asociar los Tipo de Diario a Modelo.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConfTipodiariomodeloModel extends ZendExt_Model
{

    public function ConfTipodiarioemodeloModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los Modelos asociados al Tipo Diario
     * @param Array $argparams (limit, start, idtipodiario)
     * @return Array Modelos asociados
     */
    public function loadModeloAsociado($argparams)
    {
        $ConfTipodiariomodelo = new ConfTipodiariomodelo();
        $tipoDc = $ConfTipodiariomodelo->getModeloAsociado($argparams['start'], $argparams['limit'], $argparams['idtipodiario']);
        $cant = $ConfTipodiariomodelo->getTotalModeloAsociado($argparams['idtipodiario']);
        return array('datos' => $tipoDc, 'cant' => $cant);
    }

    /**
     * Obtiene los Modelos no asociados al Tipo Diario
     * @param Array $argparams (limit, start, idtipodiario)
     * @return Array Modelos no asociados
     */
    public function loadModeloNoAsociado($argparams)
    {
        $ConfTipodiariomodelo = new ConfTipodiariomodelo();
        $tipoDc = $ConfTipodiariomodelo->getModeloNoAsociado($argparams['start'], $argparams['limit'], $argparams['idtipodiario']);
        $cant = $ConfTipodiariomodelo->getTotalModeloNoAsociado($argparams['idtipodiario']);
        return array('datos' => $tipoDc, 'cant' => $cant);
    }

    /**
     * Asocia los Modelos al Tipo Diario
     * @param Array $argparams (idtipodiario, arrayidmodelo)
     * @return boolean true if ocurred, false if failure
     */
    public function asociarModelo($argparams)
    {
        $idtipodiario = $argparams['idtipodiario'];
        $modelos = json_decode($argparams['arridmodelo']);
        foreach ($modelos as $v) {
            $this->Insertar($idtipodiario, $v);
        }
        return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgModeloGuardado}";
    }

    /**
     * Desasocia los Modelos al Tipo Diario
     * @param Array $argparams (limit, start, idtipodiario)
     * @return boolean true if ocurred, false if failure
     */
    public function desasociarModelo($argparams)
    {
        $idtipodiario = $argparams['idtipodiario'];
        $modelos = json_decode(stripslashes($argparams['arridmodelo']));
        $str_modelos = implode(",", $modelos);
        return $this->delete($idtipodiario, $str_modelos);
    }

    /**
     * Adiciona el Asociar Tipo de Diario a Modelo
     * @param stdClass $ConfTipodiariomodelo 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($idtipodiario, $modelo)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute("INSERT INTO mod_maestro.conf_tipodiariomodelo (idtipodiario,idmodelocontable) VALUES ($idtipodiario,$modelo);");
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Asociar Tipo de Diario a Modelo
     * @param Integer $idtipodiario
     * @param String $str_modelos
     * @return boolean true if ocurred, false if failure
     */
    public function delete($idtipodiario, $str_modelos)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->fetchAll("DELETE FROM mod_maestro.conf_tipodiariomodelo dc "
                    . "WHERE dc.idtipodiario = $idtipodiario AND "
                    . "dc.idmodelocontable IN ($str_modelos);");

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgModeloGuardado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
