<?php

/**
 * Clase modelo para configurar el Ejercicio Contable de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatEjerciciocontableModel extends ZendExt_Model
{

    public function DatFechaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Ejercicios Contables
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Ejercicios Contables
     */
    public function listDataEjercicio($argparams)
    {
        $datEj = new DatEjerciciocontable();
        return array('datos' => $datEj->loadEjercicios($argparams['start'], $argparams['limit'], $argparams['filter']));
    }

    /**
     * Adiciona el Ejercicio Contable y sus Periodos
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function addEjercicio()
    {
        $DatEjContable = new DatEjerciciocontable();
        $last = $DatEjContable->getLastEjercicio();
        $datEjActual = self::createEjercicio($last);
        $pContable = new DatPeriodocontableModel();
        return $pContable->addPeriodo($datEjActual['id'], $datEjActual['anno']);
    }

    /**
     * Adiciona el Ejercicio Contable
     * @param Array $objEj (Ejercicio Contable)
     * @return boolean true if ocurred, false if failure
     */
    public function createEjercicio($objEj)
    {
        $isOne = ($objEj == 0) ? true : false; //true si es el primer ejercicio
        $newEj = new DatEjerciciocontable();
        $newEj->nombre = ($isOne) ? Date('Y') : $objEj['nombre'] + 1;
        $newEj->inicio = ($isOne) ? Date('Y') . '-01-01' : ($objEj['nombre'] + 1) . '-01-01';
        $newEj->fin = ($isOne) ? Date('Y') . '-12-31' : ($objEj['nombre'] + 1) . '-12-31';
        $newEj->antecesor = ($isOne) ? 0 : $objEj['idejercicio'];
        try {
            $newEj->save();
            return array('id' => $newEj->idejercicio, 'anno' => $newEj->nombre);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el ejercicio contable
     * @param none
     * @return boolean true si elimino, false caso contrario
     */
    public function deleteLastEjercicio()
    {
        $delete = self::testDeleteEjercicio();
        if ($delete) {
            $currentEj = Doctrine::getTable('DatEjerciciocontable')->find($delete);
            try {
                $currentEj->delete(); //se elimina en cascada
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgEjercicioEliminado}";
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.lbMsgErrorEliminado}";
        }
    }

    /**
     * Verifica si el Ejercicio puede ser eliminado
     * @param none
     * @return boolean true si puede ser eliminado, false caso contrario
     */
    public function testDeleteEjercicio()
    {
        $DatEjerciciocontable = new DatEjerciciocontable();
        $lastEj = $DatEjerciciocontable->getLastEjercicio();
        if ($lastEj['idejercicio']) {//si se esta eliminando el ultimo
            $cierre = new DatCierre();
            $exist = $cierre->getCierreByEjercicio($lastEj['idejercicio']);
            return (count($exist)) ? false : $lastEj['idejercicio'];
        } else {
            return false;
        }
    }

    /**
     * Cierra el ejercicio contable.
     * @param Integer $idejercicio
     * @param Integer $idcierre
     * @param Integer $idfecha
     * @return boolean true si cerro, false caso contrario
     */
    public function cerrarEjercicio($idejercicio, $idcierre, $idfecha)
    {
        $DatEjerciciocontable = new DatEjerciciocontable();
        $ej = $DatEjerciciocontable->getEjercicioSiguiente($idejercicio);
        if (count($ej)) {
            $periodoModel = new DatPeriodocontableModel();
            $periodos = $periodoModel->listDataPeriodo(array('idejercicio' => $ej[0]['idejercicio']));
            if (count($periodos)) {
                $datCierre = new DatCierreModel();
                $close = $datCierre->updateCierre($idcierre, $periodos['datos'][0]['idperiodo'], $ej[0]['idejercicio'], $idfecha, $periodos['datos'][0]['inicio']);
                if ($close['success']) {
                    return array('success' => true, 'ejercicio' => $ej[0]['nombre'], 'periodo' => $periodos['datos'][0]['nombre'], 'fecha' => $periodos['datos'][0]['inicio']);
                } else {
                    return $close;
                }
            } else {
                $nameEj = $ej[0]['nombre'];
                return array('success' => false, "No existen per&iacute;odos contables definidos para el ejercicio $nameEj.");
            }
        } else {
            return array('success' => false, 'No quedan ejercicios contables disponibles. Por favor configure nuevos ejercicios.');
        }
    }

    /**
     * Busca si fue cerrado el ultimo periodo.
     * @param Integer $idejercicio
     * @param Integer $idfecha
     * @return boolean true si cerro, false caso contrario
     */
    public function ultimoPeriodoCerrado($idejercicio, $idfecha)
    {
        $DatEjerciciocontable = new DatEjerciciocontable();
        $ej = $DatEjerciciocontable->getEjercicio($idejercicio);
        $fecha = $this->getDataFecha($idfecha);
        $fEjercicio = implode('', explode('-', $ej[0]['fin']));
        $date = implode('', explode('-', $fecha[0]['fecha']));
        return ($fEjercicio === $date) ? true : false;
    }

    /**
     * Obtiene los datos de la fecha (metralla local porque no ve la clase DatFecha).
     * @param Integer $idfecha
     * @return Integer datos de la fecha
     */
    public function getDataFecha($idfecha)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT f.* FROM mod_maestro.dat_fecha f "
                        . "WHERE f.idfecha = $idfecha;");
    }
    
    /**
     * Obtiene los datos del ejercicio dado su id.
     * @param type $idejercicio 
     * @return type Datos del ejercicio
     */
    public function getEjercicio($idejercicio)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $result = $connection->fetchAll("SELECT e.* FROM mod_maestro.dat_ejerciciocontable e WHERE e.idejercicio = $idejercicio;");
    }

}
