<?php

/**
 * Clase modelo para gestionar los Tipo de Diario.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomTipodiarioModel extends ZendExt_Model
{

    public function DatFechaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Tipo de Diario
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Tipo de Diario
     */
    public function listDataTipoDiario($argparams)
    {
        $NomTipodiario = new NomTipodiario();
        $tipodiario = $NomTipodiario->listarTipoDiario($argparams);
        $cant = $NomTipodiario->getTotalTipoDiario();
        return array('datos' => $tipodiario, 'total' => $cant);
    }

    /**
     * Carga los Subsistemas reistrados para la entidad.
     * @param none
     * @return Array Listado de los Subsistemas
     */
    public function loadDataSubsistemasContabiliza()
    {
        $nomSubsistema = new NomSubsistema();
        return array('datos' => $nomSubsistema->listSubsistemasContabiliza());
    }

    /**
     * Adiciona el Tipo de Diario
     * @param Array $argparams (idtipodiario, codigo, denominacion, descripcion)
     * @return boolean true if ocurred, false if failure
     */
    public function saveTipoDiario($argparams)
    {
        if (!$this->existeCodigo()) {
            if (!$argparams['idtipodiario']) {
                $NomTipodiario = new NomTipodiario();
                $NomTipodiario->codigo = $argparams['codigo'];
                $NomTipodiario->denominacion = $argparams['denominacion'];
                $NomTipodiario->descripcion = $argparams['descripcion'];
                $NomTipodiario->idsubsistema = $argparams['idsubsistema'];
                return $this->Insertar($NomTipodiario);
            } else {
                $objNomTipodiario = Doctrine::getTable('NomTipodiario')->find($argparams['idtipodiario']);
                $objNomTipodiario->codigo = $argparams['codigo'];
                $objNomTipodiario->denominacion = $argparams['denominacion'];
                $objNomTipodiario->descripcion = $argparams['descripcion'];
                $objNomTipodiario->idsubsistema = $argparams['idsubsistema'];
                return $this->Actualizar($objNomTipodiario);
            }
        } else {
            return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgExiste, 'code':1}";
        }
    }

    /**
     * Adiciona el Tipo de Diario
     * @param stdClass $NomTipodiario 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar(NomTipodiario $NomTipodiario)
    {
        try {
            $NomTipodiario->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Modifica el Tipo de Diario
     * @param stdClass $objNomTipodiario 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar(NomTipodiario $objNomTipodiario)
    {
        try {
            $objNomTipodiario->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Verifica si ya existe el Tipo de Diario
     * @param Integer $codigo 
     * @return boolean true if ocurred, false if failure
     */
    public function existeCodigo($codigo)
    {
        $NomTipodiario = new NomTipodiario();
        $cant = $NomTipodiario->buscarTipoDiario(array('codigo' => $codigo));
        (count($cant)) ? true : false;
    }

    /**
     * Elimina el Tipo de Diario
     * @param Array $argparams (idtipodiario)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteTipoDiario($argparams)
    {
        if (!$this->comprobarUsoTipoDiario($argparams['idtipodiario'])) {
            $objFecha = Doctrine::getTable('NomTipodiario')->find($argparams['idtipodiario']);
            return $this->delete($objFecha);
        }return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgEnUso}";
    }

    /**
     * Elimina el Tipo de Diario
     * @param stdClass $objTipodiario 
     * @return boolean true if ocurred, false if failure
     */
    public function delete($objTipodiario)
    {
        try {
            $objTipodiario->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Verifica si el Tipo de Diario esta en uso
     * @param Integer $idtipodiario Identificador del Tipo de Diario
     * @return boolean true if ocurred, false if failure
     */
    public function comprobarUsoTipoDiario($idtipodiario)
    {
        $ConfTipodiariomodelo = new ConfTipodiariomodelo();
        $ConfTipodiarioestructura = new ConfTipodiarioestructura();
        if ($ConfTipodiariomodelo->isAsociado($idtipodiario)) {
            return true;
        } elseif ($ConfTipodiarioestructura->isAsociado($idtipodiario, $this->global->Estructura->idestructura)) {
            return true;
        } else {
            return false;
        }
    }

}
