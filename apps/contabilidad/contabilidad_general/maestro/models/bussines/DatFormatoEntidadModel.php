<?php

/**
 * Clase modelo para asociar los Formatos a la Entidad.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatFormatoEntidadModel extends ZendExt_Model
{

    public function DatFormatoEntidadModel()
    {
        parent::ZendExt_Model();
    }

    public function Insertar(DatFormatoEntidad $DatFormatoEntidad)
    {
        try {
            $DatFormatoEntidad->save();
            return is_numeric($DatFormatoEntidad->idformatoentidad);
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    public function Eliminar($idformato, $idestructuracomun)
    {
        try {
            if ($this->verificarUso($idformato, $idestructura)) {
                return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgFormatoUso}";
            } else {
                $query = Doctrine_Query::create();
                $rows_delete = $query->delete()
                        ->from('DatFormatoEntidad')
                        ->where("idformato=? AND idestructuracomun=?", array($idformato, $idestructuracomun))
                        ->execute();
                return ($rows_delete > 0) ? "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgFormatoEliminado}" : "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.lbMsgErrorEliminado}";
            }
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function verificarUso($idformato, $idestructura)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT fn.* FROM mod_contabilidad.conf_formatonomenclador fn "
                . "WHERE fn.idestructura = $idestructura AND fn.idformato = $idformato;");
        return (count($data_return)) ? 1 : 0;
    }

}
