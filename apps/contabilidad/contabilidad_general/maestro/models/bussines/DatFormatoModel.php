<?php

/**
 * Clase modelo para gestionar los Formatos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatFormatoModel extends ZendExt_Model
{

    public function DatFormatoModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Formatos
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Formatos
     */
    public function listDataFormato($argparams)
    {
        $datFormato = new DatFormato();
        $formatos = $datFormato->loadFormatos($argparams['start'], $argparams['limit'], $argparams['filter'], $this->global->Estructura->idestructura);
        $cant = $datFormato->getCantFormatos($argparams['filter'], $this->global->Estructura->idestructura);
        if ($cant > 0) {
            $this->getComplementaryDataFormat($formatos);
        }
        return array('datos' => $formatos, 'cant' => $cant);
    }

    /**
     * Lista los datos de los Formatos a Heredar
     * @param none
     * @return Array Listado de los Formatos
     */
    public function listDataHeredar()
    {
        $datFormato = new DatFormato();
        $dataEst = $this->integrator->metadatos->DameEstructura($this->global->Estructura->idestructura);
        if ($dataEst[0]->idpadre != $dataEst[0]->idestructura) {
            $formatos = $datFormato->loadFormatosHeredar($dataEst[0]->idestructura, $dataEst[0]->idpadre);
            if (count($formatos) > 0) {
                $this->getComplementaryDataFormat($formatos);
            }
            return array('datos' => $formatos);
        }
        return array('datos' => array());
    }

    /**
     * Obtiene la informacion complementaria para los Formatos
     * @param Array $formatos 
     * @return void
     */
    public function getComplementaryDataFormat(&$formatos)
    {
        $datParteFormato = new DatParteformato();
        foreach ($formatos as &$v) {
            $parteFormato = $datParteFormato->listParteFormatos($v['idformato']);
            $estruct = array(); //estructura del formato
            $vistap = array(); //vista previa del formato
            $longitud = 0; //longituddel formato
            foreach ($parteFormato as $pf) {
                $estruct[] = $pf['abreviatura'];
                $vistap[] = $this->replaceCero($pf['longitud']);
                $longitud += $pf['longitud'];
            }
            $v['estructura'] = (count($estruct)) ? implode($v['separador'], $estruct) : '';
            $v['vistap'] = (count($vistap)) ? implode($v['separador'], $vistap) : '';
            $v['longitud'] = $longitud;
        }
    }

    /**
     * Formatea con ceros una expresion
     * @param Integer $length 
     * @return String La longitud representada con ceros
     */
    public function replaceCero($length)
    {
        $str_return = "";
        while ($length > 0) {
            $str_return .="0";
            $length--;
        }
        return $str_return;
    }

    /**
     * Carga los Subsistemas reistrados para la entidad
     * @param none
     * @return Array Listado de los Formatos
     */
    public function loadDataSubsistemas()
    {
        $NomSubsistemaModel = new NomSubsistemaModel();
        return $NomSubsistemaModel->listSubsistemas();
    }

    /**
     * Adiciona el Formato
     * @param Array $argparams (nombre, descripcion, separador, estandar, idsubsistema)
     * @return boolean true if ocurred, false if failure
     */
    public function addFormato($argparams)
    {
        $objFormato = new DatFormato();
        $objFormato->nombre = $argparams['nombre'];
        $objFormato->descripcion = $argparams['descripcion'];
        $objFormato->separador = $argparams['separador'];
        $objFormato->estandar = ($argparams['estandar']) ? 1 : 0;
        $objFormato->idsubsistema = $argparams['idsubsistema'];
        $objFormato->idestructuracomun = $this->global->Estructura->idestructura;
        $idformato = $this->Insertar($objFormato);
        if ($idformato) {
            $DatFormatoEntidad = new DatFormatoEntidad();
            $DatFormatoEntidad->idformato = $idformato;
            $DatFormatoEntidad->idestructuracomun = $this->global->Estructura->idestructura;
            try {
                $DatFormatoEntidad->save();
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgFormatoAdicionado}";
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.lbMsgErrorAdicionando}";
        }
    }

    /**
     * Modifica el Formato
     * @param Array $argparams (idformato, nombre, descripcion, separador, estandar, idsubsistema)
     * @return boolean true if ocurred, false if failure
     */
    public function updateFormato($argparams)
    {
        $objFormato = Doctrine::getTable('DatFormato')->find($argparams['idformato']);
        $objFormato->idformato = $argparams['idformato'];
        $objFormato->nombre = $argparams['nombre'];
        $objFormato->descripcion = $argparams['descripcion'];
        $objFormato->separador = $argparams['separador'];
        $objFormato->estandar = ($argparams['estandar']) ? 1 : 0;
        $objFormato->idsubsistema = $argparams['idsubsistema'];
        $objFormato->idestructuracomun = $this->global->Estructura->idestructura;
        return $this->Actualizar($objFormato);
    }

    /**
     * Adiciona el Formato
     * @param stdClass $objFormato 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($objFormato)
    {
        try {
            $formato = $objFormato->buscarFormato(array('nombre' => $objFormato->nombre, 'idestructuracomun' => $objFormato->idestructuracomun));
            if ($formato == 0) {
                $objFormato->save();
                return $objFormato->idformato;
            } else {
                return false;
            }
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Actualiza el Formato
     * @param stdClass $objFormato 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($objFormato)
    {
        try {
            $objFormato->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgFormatoAdicionado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Formato en CASCADA
     * @param stdClass $objFormato 
     * @return boolean true if ocurred, false if failure
     */
    public function delete($objFormato)
    {
        try {
            $objFormato->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgFormatoEliminado}";
        } catch (Doctrine_Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgFormatoUso}";
        }
    }

    /**
     * Elimina el Formato
     * @param Array $argparams (idformato) 
     * @return boolean true if ocurred, false if failure
     */
    public function deleteDatosFormato($argparams)
    {
        if ($argparams['propietario'] == 1) {//si no fue heredado se eliminan todos los datos
            $objFormato = Doctrine::getTable('DatFormato')->find($argparams['idformato']);
            return $this->delete($objFormato);
        } else {//si fue heredado se elimina de la relacion
            $datFormatoEntidadModel = new DatFormatoEntidadModel();
            return $datFormatoEntidadModel->Eliminar($argparams['idformato'], $this->global->Estructura->idestructura);
        }
    }

    /**
     * Hereda los formatos
     * @param Array $argparams Listado de idformato 
     * @return boolean true if ocurred, false if failure
     */
    public function heredarFormato($argparams)
    {
        $arrayFormatos = json_decode(stripslashes($argparams['idformato']));
        foreach ($arrayFormatos as $v) {
            if (!$this->createAddFormatoEntidad($v)) {
                return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.lbMsgErrorheredando}";
            }
        }return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgHeredando}";
    }

    /**
     * Crea la relacion Formato - Entidad y la adiciona
     * @param Integer $idformato Identificaador del Formato 
     * @return boolean true if ocurred, false if failure
     */
    public function createAddFormatoEntidad($idformato)
    {
        $datFormatoEnt = new DatFormatoEntidad();
        $datFormatoEnt->idformato = $idformato;
        $datFormatoEnt->idestructuracomun = $this->global->Estructura->idestructura;
        try {
            $datFormatoEnt->save();
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
