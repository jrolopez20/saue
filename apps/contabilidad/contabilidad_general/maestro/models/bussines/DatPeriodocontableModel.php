<?php

/**
 * Clase modelo para configurar los Periodos Contables de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatPeriodocontableModel extends ZendExt_Model
{

    public function DatPeriodocontableModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Periodos Contables
     * @param Array $argparams (start, limit, idejercicio)
     * @return Array Listado de los Periodos Contables
     */
    public function listDataPeriodo($argparams)
    {
        $datEj = new DatPeriodocontable();
        return array('datos' => $datEj->loadPeriodos($argparams['start'], $argparams['limit'], $argparams['idejercicio']));
    }

    /**
     * Adiciona los Periodos Contables del Ejercicio
     * @param Integer $idEjercicio Identificador del Ejercicio Contable
     * @param Integer $anno Anno del Ejercicio Contable
     * @return boolean true if ocurred, false if failure
     */
    public function addPeriodo($idEjercicio, $anno)
    {
        $meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre');
        for ($i = 1; $i <= 12; $i++) {
            $objDatPeriodoContable[$i - 1] = new DatPeriodocontable;
            $fin = mktime(0, 0, 0, $i, 0, $anno);
            $ffin = $anno . '-' . strftime("%m", $fin) . '-' . strftime("%d", $fin);
            $finicio = $anno . '-' . strftime("%m", $fin) . '-' . '1';
            $objDatPeriodoContable[$i - 1]->nombre = $meses[strftime("%m", $fin)];
            $objDatPeriodoContable[$i - 1]->inicio = $finicio;
            $objDatPeriodoContable[$i - 1]->fin = $ffin;
            $objDatPeriodoContable[$i - 1]->idejercicio = $idEjercicio;
        }
        return self::savePeriodo($objDatPeriodoContable);
    }

    /**
     * Adiciona los Periodos Contables del Ejercicio
     * @param Array $arrPeriodos Objetos de periodos Contable
     * @return boolean true if ocurred, false if failure
     */
    public function savePeriodo($arrPeriodos)
    {
        try {
            foreach ($arrPeriodos as $v) {
                $v->save();
            }
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgEjercicioAdicionado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Periodo Contable
     * @param Array $idejercicio
     * @return boolean true si elimino, false caso contrario
     */
    public function deletePeriodo($idejercicio)
    {
        $query = Doctrine_Query::create();
        try {
            $delete = $query->delete()
                    ->from('DatPeriodocontable e')
                    ->where("e.idejercicio = $idejercicio")
                    ->execute();
            return $delete;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    
    /**
     * Obtiene los datos del periodo dado su id.
     * @param type $idperiodo 
     * @return type Datos del periodo.
     */
    public function getPeriodo($idperiodo)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $result = $connection->fetchAll("SELECT e.* FROM mod_maestro.dat_periodocontable e WHERE e.idperiodo = $idperiodo;");
    }
}
