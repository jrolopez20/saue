<?php

/**
 * Clase modelo para configurar el Cierre de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatCierreModel extends ZendExt_Model
{

    public function DatFechaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Adiciona el Cierre
     * @param Integer $idestructurasubsist
     * @param Integer $idejercicio
     * @param Integer $idperiodo
     * @return boolean true if ocurred, false if failure
     */
    public function addCierre($idestructurasubsist, $idejercicio, $idperiodo)
    {
        $objCierre = new DatCierre();
        $objCierre->idestructurasubsist = $idestructurasubsist;
        $objCierre->idejercicioactual = $idejercicio;
        $objCierre->idperiodoactual = $idperiodo;
        return array('success' => $this->Insertar($objCierre));
    }

    /**
     * Adiciona el Cierre
     * @param stdClass $objCierre
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($objCierre)
    {
        try {
            $objCierre->save();
            return true;
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Cierre
     * @param Integer $idestructurasubsist
     * @return boolean true if ocurred, false if failure
     */
    public function delCierre($idestructurasubsist)
    {
        $query = Doctrine_Query::create();
        try {
            $delete = $query->delete()
                    ->from('DatCierre c')
                    ->where("c.idestructurasubsist = $idestructurasubsist")
                    ->execute();
            return $delete;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Actualiza los datos del cierre y la fecha contable.
     * @param Integer $idcierre
     * @param Integer $idperiodo
     * @param Integer $idejercicio
     * @param Integer $idfecha
     * @param Date $fini
     * @return boolean true if ocurred, false if failure
     */
    public function updateCierre($idcierre, $idperiodo, $idejercicio, $idfecha, $fini)
    {
        $objCierre = Doctrine::getTable('DatCierre')->find($idcierre);
        $objCierre->idperiodoactual = $idperiodo;
        $objCierre->idejercicioactual = $idejercicio;
        try {
            $objCierre->save();
            $this->updateFecha($idfecha, $fini);
            return array('success' => true);
        } catch (Exception $exc) {
            return array('success' => false);
        }
    }

    /**
     * Cierra el periodo contable actual
     * @param Integer $idSubsistema
     * @param Array $next
     * @return boolean true if ocurred, false if failure
     */
    public function cerrarPeriodo($idSubsistema, $next)
    {
        $datCierre = new DatCierre();
        $cierre = $datCierre->getCierre($idSubsistema, $this->global->Estructura->idestructura);
        $objCierre = Doctrine::getTable('DatCierre')->find($cierre[0]['idcierre']);
        $objCierre->idperiodoactual = $next[0]['idperiodo'];
        try {
            $objCierre->save();
            $this->updateFecha($cierre[0]['idfecha'], $next[0]['inicio']);
            return array('success' => true);
        } catch (Exception $exc) {
            return array('success' => false);
        }
    }

    /**
     * Actualiza los datos de la fecha
     * @param Integer $idfecha
     * @param Date $fecha
     * @return boolean true if ocurred, false if failure
     */
    public function updateFecha($idfecha, $fecha)
    {
        $query = "UPDATE mod_maestro.dat_fecha SET fecha = '" . $fecha . "'";
        $query .= " WHERE idfecha = " . $idfecha;

        $conn = Zend_Registry::get('conexion');
        try {
            $conn->execute($query);
            return array('success' => true);
        } catch (Exception $exc) {
            return array('success' => false);
        }
    }

    /**
     * Pasa la fecha al ultimo dia del periodo
     * @param Integer $idSubsistema
     * @param Date $fechafin
     * @return boolean true if ocurred, false if failure
     */
    public function ultimoDiaPeriodo($idSubsistema, $fechafin)
    {
        $datCierre = new DatCierre();
        $cierre = $datCierre->getCierre($idSubsistema, $this->global->Estructura->idestructura);
        return $this->updateFecha($cierre[0]['idfecha'], $fechafin);
    }

}
