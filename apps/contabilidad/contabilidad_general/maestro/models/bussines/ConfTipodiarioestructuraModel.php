<?php

/**
 * Clase modelo para gestionar los Tipo Diario por Estructura.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConfTipodiarioestructuraModel extends ZendExt_Model
{

    public function ConfTipodiarioestructuraModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los datos de los Tipo de Diario y sus usos
     * @param Array $argparams (start, limit)
     * @return Array Listado de los Tipo de Diario
     */
    public function getConfTipoDiarioEstructura($limit, $start, $filter)
    {
        $result = array();
        $domain = new ConfTipodiarioestructura();
        $result['datos'] = $domain->getTipoDiarioEstructura($this->global->Estructura->idestructura, $limit, $start, $filter);
        $result['total'] = $domain->getTotalTipoDiarioEstructura($this->global->Estructura->idestructura, $filter);
        return $result;
    }

    /**
     * Adiciona la relacion del Tipo Diario con Estructura y Subsistema.
     * @param Array $argparams (idsubsistema, arrTipoDiario)
     * @return boolean true if ocurred, false if failure
     */
    public function saveConfTipoDiario($argparams)
    {
        $arrTipoDiario = json_decode($argparams['arrTipoAsiento']);
        $this->deleteAllSubsistema($this->global->Estructura->idestructura, $argparams['idsubsistema']);
        foreach ($arrTipoDiario as $tipoDiario) {
            if (is_float($tipoDiario)) {
                $dec = sprintf("%f", $tipoDiario);
                $dec = explode('.', $dec);
                $tipoDiario = $dec[0];
            }
            $this->adicionar($this->global->Estructura->idestructura, $argparams['idsubsistema'], $tipoDiario);
        }
        return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
    }

    public function eliminar($idtipodiarioestructura)
    {
        try {
            $query = Doctrine_Query::create();
            $sql = "DELETE FROM mod_maestro.conf_tipodiarioestructura
                    WHERE idtipodiarioestructura = {$idtipodiarioestructura};";

            $stmt = $query->getConnection()->prepare($sql);
            $stmt->execute();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina las asociaciones entre Tipo Diario y Estructura.
     * @param Integer $idestructuracomun identificador de la estructura
     * @param Integer $idsubsistema identificador del subsistema
     * @return boolean true if ocurred, false if failure
     */
    public function deleteAllSubsistema($idestructuracomun, $idsubsistema)
    {
        try {
            $query = Doctrine_Query::create();
            $sql = "DELETE FROM mod_maestro.conf_tipodiarioestructura "
                    . "WHERE idestructuracomun = {$idestructuracomun} AND "
                    . "idsubsistema = {$idsubsistema} ;";
            $stmt = $query->getConnection()->prepare($sql);
            $stmt->execute();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Adiciona la asociacion entre Tipo Diario y Estructura.
     * @param stdClass $objFecha
     * @return boolean true if ocurred, false if failure
     */
    public function adicionar($idestructuracomun, $idsubsistema, $idtipodiario)
    {
        $obj = new ConfTipodiarioestructura();
        $obj->idestructuracomun = $idestructuracomun;
        $obj->idsubsistema = $idsubsistema;
        $obj->idtipodiario = $idtipodiario;
        try {
            $obj->save();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Carga los Subsistemas reistrados para la entidad.
     * @param none
     * @return Array Listado de los Subsistemas
     */
    public function loadDataSubsistemas()
    {
        $nomSubsistema = new NomSubsistema();
        return array('datos' => $nomSubsistema->listSubsistemasConfigurados($this->global->Estructura->idestructura));
    }

    /**
     * Lista los datos de los Tipo de Diario por Subsistema.
     * @param Array $argparams (idsubsistema, start, limit, filter)
     * @return Array Listado de los Tipo de Diario
     */
    public function loadTipoDiarioSubsistema($argparams)
    {
        $tipoDiarioEstructura = new ConfTipodiarioestructura();
        $NomTipodiario = new NomTipodiario();
        $tipodiario = $tipoDiarioEstructura->getTipoDiarioEstructura($this->global->Estructura->idestructura, $argparams['idsubsistema'], $argparams['start'], $argparams['limit'], $argparams['filter']);
        $cant = $NomTipodiario->getTotalTipoDiario();
        return array('datos' => $tipodiario, 'total' => $cant);
    }

}
