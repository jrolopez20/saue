<?php

/**
 * Clase modelo para gestionar los Subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomSubsistemaModel extends ZendExt_Model
{

    public function NomSubsistemaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Subsistemas
     * @param none
     * @return Array Listado de los Subsistemas
     */
    public function listSubsistemas()
    {
        $nomSubsistema = new NomSubsistema();
        return array('datos' => $nomSubsistema->listSubsistemas());
    }

}
