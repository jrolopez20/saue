<?php

/**
 * Clase modelo para configurar los Modelos Contables.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomModelocontableModel extends ZendExt_Model
{

    public function NomModelocontableModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Modelos Contables.
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Modelos Contables
     */
    public function listDataModelo($argparams)
    {
        $NomModelocontable = new NomModelocontable();
        $modelos = $NomModelocontable->loadModelo($argparams['start'], $argparams['limit'], $argparams['filter']);
        $cant = $NomModelocontable->getCantModelos($argparams['filter']);
        return array('datos' => $modelos, 'cant' => $cant);
    }

    /**
     * Lista los datos de los Pases.
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Pases
     */
    public function listDataPase($argparams)
    {
        $ConfModelocontablecuenta = new ConfModelocontablecuenta();
        $pases = $ConfModelocontablecuenta->loadDataPases($argparams['idmodelocontable']);
        return array('datos' => $pases, 'cant' => count($pases));
    }

    /**
     * Obtiene las Cuentas contables por nivel.
     * @param Array $argparams (node)
     * @return Array Cuentas contables
     */
    public function loadDataCuentas($argparams)
    {
        $NomCuenta = new NomCuenta();
        if ($argparams['filter']) {
            return $NomCuenta->buscarCuentasContables($this->global->Estructura->idestructura, $argparams['filter'], true, 1);
        } else {
            return $NomCuenta->loadDataCuentas((is_numeric($argparams['node']) ? $argparams['node'] : null), true, 1);
        }
    }

    /**
     * Adiciona el Modelo Contable.
     * @param Array $argparams (start, limit, filter)
     * @return boolean true if ocurred, false if failure
     */
    public function saveModelo($argparams)
    {
        if (!$argparams['idmodelocontable']) {
            $NomModelocontable = new NomModelocontable();
            $NomModelocontable->denominacion = $argparams['denominacion'];
            return $this->Insertar($NomModelocontable);
        } else {
            $objNomModelocontable = Doctrine::getTable('NomModelocontable')->find($argparams['idmodelocontable']);
            $objNomModelocontable->denominacion = $argparams['denominacion'];
            return $this->Actualizar($objNomModelocontable);
        }
    }

    /**
     * Adiciona el Modelo Contable.
     * @param stdClass $NomModelocontable 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar(NomModelocontable $NomModelocontable)
    {
        try {
            $NomModelocontable->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Modifica el Modelo Contable.
     * @param stdClass $NomModelocontable 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar(NomModelocontable $NomModelocontable)
    {
        try {
            $NomModelocontable->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Modelo Contable.
     * @param stdClass $objModelo 
     * @return boolean true if ocurred, false if failure
     */
    public function delete($objModelo)
    {
        try {
            $objModelo->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
        } catch (Doctrine_Exception $exc) {
            if($exc->getCode() == 23503){
                return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgModeloUso}";
            }else{
                echo $exc->getTraceAsString();
            }
        }
    }

    /**
     * Elimina el Modelo Contable.
     * @param Integer $idmodelo Identificador del Modelo
     * @return boolean true if ocurred, false if failure
     */
    public function deleteModelo($idmodelo)
    {
        if (!$this->comprobarUsoModelo($idmodelo)) {
            $objModelo = Doctrine::getTable('NomModelocontable')->find($idmodelo);
            return $this->delete($objModelo);
        } else {
            return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgModeloUso, 'code':1}";
        }
    }

    /**
     * Verifica si el Modelo Contable esta en uso.
     * @param Integer $idmodelo Identificador del Modelo
     * @return boolean true if ocurred, false if failure
     */
    public function comprobarUsoModelo($idmodelo)
    {
        $NomModelocontable = new NomModelocontable();
        $found = $NomModelocontable->findUseModelo($idmodelo);
        return (count($found)) ? true : false; //cable para que elimine
    }

    /**
     * Guarda el Pase.
     * @param Array $argparams (cuentas, debitocredito, idmodelocontablecuenta)
     * @return boolean true if ocurred, false if failure
     */
    public function savePase($argparams)
    {
        if (!$argparams['idmodelocontablecuenta']) {
            return $this->createPases($argparams);
        } else {
            $objModeloCta = Doctrine::getTable('ConfModelocontablecuenta')->find($argparams['idmodelocontablecuenta']);
            $objModeloCta->debitocredito = $argparams['debitocredito'];
            return $this->ActualizarPase($objModeloCta);
        }
    }

    /**
     * Crea el objeto pase.
     * @param stdClass $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function createPases($argparams)
    {
        $ModeloCta = new ConfModelocontablecuenta();
        $ctas = json_decode($argparams['idcuentas']);
        foreach ($ctas as $v) {
            if (!$ModeloCta->existePase($argparams['idmodelocontable'], $v)) {
                try {
                    $this->InsertarPase($argparams['idmodelocontable'], $v, $argparams['debitocredito']);
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                    return;
                }
            }
        }
        return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgPaseGuardado}";
    }

    /**
     * Adiciona el Modelo Contable.
     * @param stdClass $NomModelocontable 
     * @return boolean true if ocurred, false if failure
     */
    public function InsertarPase($idmodelocontable, $idcuenta, $debitocredito)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute("INSERT INTO mod_maestro.conf_modelocontablecuenta (idmodelocontable,idcuenta,debitocredito) VALUES ($idmodelocontable,$idcuenta,$debitocredito);");
            return;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    /**
     * Modifica el Pase.
     * @param stdClass $NomModelocontable 
     * @return boolean true if ocurred, false if failure
     */
    public function ActualizarPase(ConfModelocontablecuenta $ConfModelocontablecuenta)
    {
        try {
            $ConfModelocontablecuenta->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgPaseGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Pase.
     * @param Array $arridpases Identificador del Pase
     * @return boolean true if ocurred, false if failure
     */
    public function deletePase($arridpases)
    {
        try {
            $str_idpases = implode(',', json_decode($arridpases));
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->fetchAll("DELETE FROM mod_maestro.conf_modelocontablecuenta mcta "
                    . "WHERE mcta.idmodelocontablecuenta IN ($str_idpases);");
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgPaseEliminado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
