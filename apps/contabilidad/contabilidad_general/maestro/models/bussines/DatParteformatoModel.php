<?php

/**
 * Clase dominio para configurar las Partes Formatos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatParteformatoModel extends ZendExt_Model
{

    public function DatParteformatoModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de las Partes Formatos
     * @param Array $argparams (idformato)
     * @return Array Listado de las Parte Formatos
     */
    public function listDataParteFormato($argparams)
    {
        $datParteFormato = new DatParteformato();
        $parteF = $datParteFormato->listParteFormatos($argparams['idformato']);
        return array('datos' => $parteF, 'cant' => count($parteF));
    }

    /**
     * Adiciona la Parte Formato
     * @param Array $argparams (idformato, nombre, abreviatura, longitud, nivel)
     * @return boolean true if ocurred, false if failure
     */
    public function addParteFormato($argparams)
    {
        $objParteFormato = new DatParteformato();
        $objParteFormato->idformato = $argparams['idformato'];
        $objParteFormato->nombre = $argparams['nombre'];
        $objParteFormato->abreviatura = $argparams['abreviatura'];
        $objParteFormato->longitud = $argparams['longitud'];
        $objParteFormato->nivel = $argparams['nivel'];
        return $this->Insertar($objParteFormato);
    }

    /**
     * Modifica la Parte Formato
     * @param Array $argparams (idformato, idparteformato, nombre, abreviatura, longitud, nivel)
     * @return boolean true if ocurred, false if failure
     */
    public function updateParteFormato($argparams)
    {
        $objParteFormato = Doctrine::getTable('DatParteformato')->find($argparams['idparteformato']);
        $objParteFormato->idformato = $argparams['idformato'];
        $objParteFormato->nombre = $argparams['nombre'];
        $objParteFormato->abreviatura = $argparams['abreviatura'];
        $objParteFormato->longitud = $argparams['longitud'];
        $objParteFormato->nivel = $argparams['nivel'];
        return $this->Actualizar($objParteFormato);
    }

    /**
     * Adiciona la Parte Formato
     * @param stdClass $objParteFormato 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($objParteFormato)
    {
        try {
            $existe = $objParteFormato->buscarParteFormato(array('idformato' => $objParteFormato->idformato, 'nombre' => $objParteFormato->nombre, 'abreviatura' => $objParteFormato->abreviatura, 'nivel' => $objParteFormato->nivel));
            if ($existe == 0) {
                $objParteFormato->save();
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgNivelAdicionado}";
            } else {
                return "{'success':false, 'codMsg':3,'code': 1, 'mensaje':perfil.etiquetas.lbNivelExiste}";
            }
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Actualiza la Parte Formato
     * @param stdClass $objParteFormato 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($objParteFormato)
    {
        try {
            $existe = $objParteFormato->buscarParteFormato(array('idparteformato' => $objParteFormato->idparteformato, 'idformato' => $objParteFormato->idformato, 'nombre' => $objParteFormato->nombre, 'abreviatura' => $objParteFormato->abreviatura, 'nivel' => $objParteFormato->nivel));
            if ($existe == 0) {
                $objParteFormato->save();
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgNivelAdicionado}";
            } else {
                return "{'success':false, 'codMsg':3,'code': 1, 'mensaje':perfil.etiquetas.lbNivelExiste}";
            }
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina la Parte Formato
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function deleteParteFormato($argparams)
    {
        try {
            $objParteFormato = Doctrine::getTable('DatParteformato')->find($argparams['idparteformato']);
            $objParteFormato->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgNivelEliminado}";
        } catch (Doctrine_Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgNivelUso}";
        }
    }

}
