<?php

/**
 * Clase modelo que relaciona Estrutura y Subsistema.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatEstructurasubsistModel extends ZendExt_Model
{

    public function DatFechaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Adiciona la relacion Estrutura y Subsistema.
     * @param Integer $idsubsistema
     * @return boolean true if ocurred, false if failure
     */
    public function addEstructuraSubsistema($idsubsistema)
    {
        $estructuraSubsist = new DatEstructurasubsist();
        $estructuraSubsist->idsubsistema = $idsubsistema;
        $estructuraSubsist->idestructuracomun = $this->global->Estructura->idestructura;
        return $this->Insertar($estructuraSubsist);
    }

    /**
     * Adiciona la relacion Estrutura y Subsistema.
     * @param stdClass $objEstructuraSubsist
     * @return Integer Idenificador del elemento insertado
     */
    public function Insertar($objEstructuraSubsist)
    {
        try {
            $objEstructuraSubsist->save();
            return $objEstructuraSubsist->idestructurasubsist;
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina la relacion Estrutura y Subsistema.
     * @param Integer $idestructurasubsist
     * @return boolean true if ocurred, false if failure
     */
    public function delEstructuraSubsist($idestructurasubsist)
    {
        $objEstructuraSubsist = Doctrine::getTable('DatEstructurasubsist')->find($idestructurasubsist);
        try {
            $objEstructuraSubsist->delete();
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
