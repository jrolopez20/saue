<?php

/**
 * Clase controladora para asociar los Tipo Diario al Subsistema.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TipodiariosubsistemaController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new ConfTipodiarioestructuraModel();
    }

    public function tipodiariosubsistemaAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function listDataSubsistemaAction()
    {
        echo json_encode($this->model->loadDataSubsistemas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadTipoDiarioAction()
    {
        echo json_encode($this->model->loadTipoDiarioSubsistema($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function updateTipoDiarioSubsistemaAction()
    {
        echo $this->model->saveConfTipoDiario($this->_request->getPost());
    }

}
