<?php

/**
 * Clase controladora para gestionar los Tipo Diario.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TipodiarioController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->model = new NomTipodiarioModel();
        $this->diariomodelomodel = new ConfTipodiariomodeloModel();
    }

    public function tipodiarioAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function listDataTipoDiarioAction()
    {
        echo json_encode($this->model->listDataTipoDiario($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataSubsistemasAction()
    {
        echo json_encode($this->model->loadDataSubsistemasContabiliza());
    }

    /**
     * call this model action
     */
    public function updateTipoDiarioAction()
    {
        echo $this->model->saveTipoDiario($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteTipoDiarioAction()
    {
        echo $this->model->deleteTipoDiario($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function loadModeloAsociadoAction()
    {
        echo json_encode($this->diariomodelomodel->loadModeloAsociado($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadModeloNoAsociadoAction()
    {
        echo json_encode($this->diariomodelomodel->loadModeloNoAsociado($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function asociarModeloAction()
    {
        echo $this->diariomodelomodel->asociarModelo($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function desasociarModeloAction()
    {
        echo $this->diariomodelomodel->desasociarModelo($this->_request->getPost());
    }

}
