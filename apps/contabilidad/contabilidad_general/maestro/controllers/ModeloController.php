<?php

/**
 * Clase controladora para gestionar los Modelos Contables
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ModeloController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new NomModelocontableModel();
    }

    public function modeloAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function listDataModeloAction()
    {
        echo json_encode($this->model->listDataModelo($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function listDataPaseAction()
    {
        echo json_encode($this->model->listDataPase($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadCuentasAction()
    {
        echo json_encode($this->model->loadDataCuentas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function saveModeloAction()
    {
        echo $this->model->saveModelo($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function savePaseAction()
    {
        echo $this->model->savePase($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteModeloAction()
    {
        echo $this->model->deleteModelo($this->_request->getPost('idmodelocontable'));
    }

    /**
     * call this model action
     */
    public function deletePaseAction()
    {
        echo $this->model->deletePase($this->_request->getPost('idpases'));
    }

}
