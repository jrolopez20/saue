<?php

/**
 * Clase controladora para gestionar los Ejercicios y Periodos Contables.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class EjercicioController extends ZendExt_Controller_Secure
{

    private $ejercicioModel;
    private $periodoModel;

    public function init()
    {
        $this->ejercicioModel = new DatEjerciciocontableModel();
        $this->periodoModel = new DatPeriodocontableModel();
        parent::init();
    }

    public function ejercicioAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function listDataEjercicioAction()
    {
        echo json_encode($this->ejercicioModel->listDataEjercicio($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function listDataPeriodoAction()
    {
        echo json_encode($this->periodoModel->listDataPeriodo($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function addEjercicioAction()
    {
        echo $this->ejercicioModel->addEjercicio();
    }

    /**
     * call this model action
     */
    public function deleteEjercicioAction()
    {
        echo $this->ejercicioModel->deleteLastEjercicio();
    }

}
