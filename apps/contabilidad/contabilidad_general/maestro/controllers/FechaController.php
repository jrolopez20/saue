<?php

/**
 * Clase controladora para configurar la fecha de los subsistemas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class FechaController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new DatFechaModel();
        parent::init();
    }

    public function fechaAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function listDataFechasAction()
    {
        echo json_encode($this->model->listDataFechas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadSubsistemasAction()
    {
        echo json_encode($this->model->loadSubsistemasNoConfigurados());
    }

    /**
     * call this model action
     */
    public function loadEjerciciosAction()
    {
        echo json_encode($this->model->loadEjercicios());
    }

    /**
     * call this model action
     */
    public function loadPeriodosAction()
    {
        echo json_encode($this->model->loadPeriodos($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function addDateAction()
    {
        echo $this->model->addDate($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function updateDateAction()
    {
        echo $this->model->updateDate($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteDateAction()
    {
        echo $this->model->deleteDate($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function nextDateAction()
    {
        echo $this->model->nextDate($this->_request->getPost());
    }

}
