<?php

/**
 * Clase controladora para gestionar los Formatos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class FormatoController extends ZendExt_Controller_Secure
{

    public function init()
    {
        $this->formatoModel = new DatFormatoModel();
        $this->parteModel = new DatParteformatoModel();
        parent::init();
    }

    public function formatoAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function listDataFormatoAction()
    {
        echo json_encode($this->formatoModel->listDataFormato($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function listDataParteFormatoAction()
    {
        echo json_encode($this->parteModel->listDataParteFormato($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadSubsistemaAction()
    {
        echo json_encode($this->formatoModel->loadDataSubsistemas());
    }

    /**
     * call this model action
     */
    public function addFormatoAction()
    {
        echo $this->formatoModel->addFormato($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function updateFormatoAction()
    {
        echo $this->formatoModel->updateFormato($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteFormatoAction()
    {
        echo $this->formatoModel->deleteDatosFormato($this->_request->getPost());
    }

    /*     * addParteFormato
     * call this model action
     */

    public function addParteFormatoAction()
    {
        echo $this->parteModel->addParteFormato($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function updateParteFormatoAction()
    {
        echo $this->parteModel->updateParteFormato($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteParteFormatoAction()
    {
        echo $this->parteModel->deleteParteFormato($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function loadDataHeredarAction()
    {
        echo json_encode($this->formatoModel->listDataHeredar());
    }

    /**
     * call this model action
     */
    public function heredarFormatoAction()
    {
        echo $this->formatoModel->heredarFormato($this->_request->getPost());
    }

}
