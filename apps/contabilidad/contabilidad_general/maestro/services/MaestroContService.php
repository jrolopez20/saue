<?php

class MaestroContService
{

    /**
     * Guarda el comprobante de operaciones.
     * @param Integer $idestructura
     * @param Integer $idsubsisttema
     * @return boolean true if ocurred, false if failure
     */
    public function cerrarEjercicio($idestructura, $idsubsistema)
    {
        $datCierre = new DatCierre();
        $ejercicioModel = new DatEjerciciocontableModel();
        $cierre = $datCierre->getCierre($idsubsistema, $idestructura);
        return $ejercicioModel->cerrarEjercicio($cierre[0]['idejercicioactual'], $cierre[0]['idcierre'], $cierre[0]['idfecha']);
    }

    /**
     * Busca si fue cerrado el ultimo periodo.
     * @param Integer $idestructura
     * @param Integer $idsubsisttema
     * @return boolean true if ocurred, false if failure
     */
    public function ultimoPeriodoCerrado($idestructura, $idsubsistema)
    {
        $datCierre = new DatCierre();
        $ejercicioModel = new DatEjerciciocontableModel();
        $cierre = $datCierre->getCierre($idsubsistema, $idestructura);
        return $ejercicioModel->ultimoPeriodoCerrado($cierre[0]['idejercicioactual'], $cierre[0]['idfecha']);
    }

    /**
     * Carga la Fecha actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Fecha actual del subsistema
     */
    public function obtenerFechaSubsistema($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT f.* FROM mod_maestro.dat_fecha f "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON es.idestructurasubsist = f.idestructurasubsist "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Obtiene el periodo dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Periodo actual del subsistema
     */
    public function getPeriodoSubsistema($idestructura, $idsubsistema)
    {
        $datPeriodocontable = new DatPeriodocontable();
        return $datPeriodocontable->getPeriodoSubsistema($idestructura, $idsubsistema);
    }

    /**
     * Obtiene el periodo siguiente.
     * @param Integer $idperiodo
     * @param Integer $idejercicio
     * @param Integer $fechafin
     * @return Array Periodo siguiente
     */
    public function getPeriodoSiguiente($idperiodo, $idejercicio, $fechafin)
    {
        $datPeriodocontable = new DatPeriodocontable();
        return $datPeriodocontable->getNextPeriodo($idperiodo, $idejercicio, $fechafin);
    }

    /**
     * Pasa al ultimo dia del periodo.
     * @param Integer $idsubsistema
     * @param Integer $fechafin
     * @return Array Periodo siguiente
     */
    public function ultimoDiaPeriodo($idsubsistema, $fechafin)
    {
        $cierreModel = new DatCierreModel();
        return $cierreModel->ultimoDiaPeriodo($idsubsistema, $fechafin);
    }

    /**
     * Cierre el periodo actual del sbsistema.
     * @param Integer $idsubsistema
     * @param Integer $nextP
     * @return Array Periodo siguiente
     */
    public function cerrarPeriodo($idsubsistema, $nextP)
    {
        $cierreModel = new DatCierreModel();
        return $cierreModel->cerrarPeriodo($idsubsistema, $nextP);
    }

    /**
     * Obtiene los datos del subsistema.
     * @param Integer $idsubsistema
     * @return Array datos del subsistema
     */
    public function getSubsistemaByID($idsubsistema)
    {
        $nomSubsistema = new NomSubsistema();
        return $nomSubsistema->getSubsistema($idsubsistema);
    }

    /**
     * Obtiene los subsistemas precedentes que no han cerrado.
     * @param Integer $idestructura
     * @param Integer $precedencia
     * @return Array datos del subsistema
     */
    public function getSubsistemasPrecedentes($idestructura, $precedencia)
    {
        $nomSubsistema = new NomSubsistema();
        return $nomSubsistema->getSubsistemasPrecedentes($idestructura, $precedencia);
    }

}
