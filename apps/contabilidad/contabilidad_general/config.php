<?php

/**
 * Fichero de configuracion de Contabilidad General.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
//Direccion de la servidora
$dir_index = $_SERVER['SCRIPT_FILENAME'];
//Direccion del fichero de configuracion
$config_file = substr($dir_index, 0, strrpos($dir_index, 'web')) . 'apps/comun/config.php';
//Inclusion del Fichero de Configuracion del marco de trabajo
include($config_file);
set_include_path($config['include_path']);
//Inicializando el include path de php a partir de la variable de configuracion
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/maestro/models/domain/generated';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/maestro/models/domain';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/maestro/models/bussines';

$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/clasificador_cuentas/models/domain/generated';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/clasificador_cuentas/models/domain';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/clasificador_cuentas/models/bussines';

$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/comprobante/models/domain/generated';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/comprobante/models/domain';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/comprobante/models/bussines';

$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/cierre/models/domain/generated';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/cierre/models/domain';
$config['include_path'] .= PATH_SEPARATOR . $dir_abs_mt . 'apps/contabilidad/contabilidad_general/cierre/models/bussines';
