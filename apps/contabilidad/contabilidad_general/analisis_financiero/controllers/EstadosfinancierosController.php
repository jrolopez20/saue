<?php

/**
 * Clase controladora para gestionar los estados financieros.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class EstadosfinancierosController extends ZendExt_Controller_Secure
{

    public function init()
    {
        $this->modelEF = new EstadosFinancierosModel();
        parent::init();
    }

    public function estadosfinancierosAction()
    {
        $this->render();
    }
    
    public function loadDefaultsValuesAction()
    {
        echo json_encode(EstadosFinancierosModel::loadDefaultsValues());
    }

    /**
     * call this model action
     */
    public function loadCuentasAgrupadasAction()
    {
        echo json_encode(EstadosFinancierosModel::loadDataCuentasAgrupadas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataEjerciciosAction()
    {
        echo json_encode(EstadosFinancierosModel::loadDataEjercicios());
    }

    /**
     * call this model action
     */
    public function loadDataPeriodosAction()
    {
        echo json_encode(EstadosFinancierosModel::loadDataPeriodos($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataEstadosFinancAction()
    {
        echo json_encode(EstadosFinancierosModel::getEstadosFinancieros());
    }
    
    /**
     * call this model action
     */
    public function loadDimensionesAction()
    {
        echo json_encode(EstadosFinancierosModel::getDimensiones());
    }

    /**
     * call this model action
     */
    public function updateEstadofinancieroAction()
    {
        echo $this->modelEF->setEstadoFinanciero($this->_request->getPost());
    }
    
    /**
     * call this model action
     */
    public function getReporteFinancieroAction()
    {
        $datosReporte = EstadosFinancierosModel::getReporteFinanciero($this->_request->getPost());
        echo json_encode();
        $dataSources = EstadosFinancierosModel::getDataRptEstadoFinanciero($datosReporte, $this->_request->getPost());
            if ($dataSources['cantidad'] > 0) {
                echo json_encode($dataSources);
            } else {
                echo json_encode(array('idreporte' => 0, 'cantidad' => 0, 'datos' => array(), "{'codMsg':-3}"));
            }
    }
    
    /**
     * call this model action
     */
    public function deleteEstadofinancieroAction()
    {
        echo $this->modelEF->delEstadoFinanciero($this->_request->getPost('idestadof'));
    }

}
