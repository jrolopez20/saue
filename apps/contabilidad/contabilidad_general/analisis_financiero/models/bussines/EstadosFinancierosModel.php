<?php

/**
 * Clase modelo para gestionar el Cierre Contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class EstadosFinancierosModel extends ZendExt_Model
{

    public function EstadosFinancierosModel()
    {
        parent::ZendExt_Model();
    }

    static public function loadDefaultsValues()
    {
        $globalConcept = ZendExt_GlobalConcept::getInstance();
        $idEstructura = $globalConcept->Estructura->idestructura;
        $datComprobante = new DatComprobante();
        $ejercicio = $datComprobante->obtenerEjercicio($idEstructura, 4);
        $periodo = $datComprobante->obtenerPeriodo($idEstructura, 4);
        $fecha = $datComprobante->obtenerFecha($idEstructura, 4);
        return array('ejercicio' => (count($ejercicio) ? $ejercicio[0] : ''), 'periodo' => (count($periodo) ? $periodo[0]
                        : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Carga el arbol de Cuentas Contables agrupadas.
     * @param Array $argparams (node)
     * @return Array arbol de Cuentas Contables agrupadas
     */
    static public function loadDataCuentasAgrupadas($argparams)
    {
        $ctas = new NomCuentaModel();
        return $ctas->getCuentas($argparams);
    }

    /**
     * Lista los datos de los Ejercicios Contables.
     * @param none
     * @return Array Listado de los Ejercicios Contables
     */
    static public function loadDataEjercicios()
    {
        $datEj = new DatEjerciciocontable();
        return array('datos' => $datEj->loadEjercicios());
    }

    /**
     * Lista los datos de los Periodos Contables.
     * @param Array $argparams (idejercicio)
     * @return Array Listado de los Periodos Contables
     */
    static public function loadDataPeriodos($argparams)
    {
        $datEj = new DatPeriodocontable();
        return array('datos' => $datEj->loadPeriodos(0, 0, $argparams['idejercicio']));
    }

    public function setEstadoFinanciero($argArrayData)
    {
        $pArrayNodos = json_decode(stripslashes($argArrayData['nodos']));
        $notacionPolacaInversa = $this->ObtenerPolaca($argArrayData['nomformula'], $pArrayNodos);
        $globalConcept = ZendExt_GlobalConcept::getInstance();
        $idEstructura = $globalConcept->Estructura->idestructura;
        if ($argArrayData['modificado'] == 1) {
            $id = $argArrayData['idmodificado'];
            $objDatEstadofinanciero = Doctrine::getTable('DatEstadofinanciero')->find($id);
        } else {
            $objDatEstadofinanciero = new DatEstadofinanciero();
        }
        $objDatEstadofinanciero->codigo = $argArrayData['codigo'];
        $objDatEstadofinanciero->idestructura = $idEstructura;
        $objDatEstadofinanciero->denominacion = $argArrayData['denominacion'];
        $objDatEstadofinanciero->formulaid = $argArrayData['nomformula'];
        $objDatEstadofinanciero->formulam = $argArrayData['formulamostrada'];
        $objDatEstadofinanciero->formulad = $argArrayData['formulastr'];
        $objDatEstadofinanciero->formulap = implode(',', $notacionPolacaInversa['polaca']);
        $objDatEstadofinanciero->formulac = implode(',', explode(" ", $argArrayData['formulamostrada']));
        $objDatEstadofinanciero->arraypostfija = serialize($notacionPolacaInversa['nodos']);
        $objDatEstadofinanciero->arrayinfija = serialize($pArrayNodos);
        if ($this->saveEstadoFinanciero($objDatEstadofinanciero)) {
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        }
    }

    public function saveEstadoFinanciero(DatEstadofinanciero $objDatEstadofinanciero)
    {
        $cc = Doctrine_Manager::getInstance()->getCurrentConnection();
        try {
            $cc->beginTransaction();
            $objDatEstadofinanciero->save();
            $cc->commit();
            return true;
        } catch (Exception $e) {
            $cc->rollback();
            throw $e;
        }
    }

    public function delEstadoFinanciero($idEstadoFinanciero)
    {
        $cc = Doctrine_Manager::getInstance()->getCurrentConnection();
        try {
            $cc->beginTransaction();
            $objDatEstadofinanciero = Doctrine::getTable('DatEstadofinanciero')->find($idEstadoFinanciero);
            $objDatEstadofinanciero->delete();
            if ($cc->commit()) {
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
            }
        } catch (Exception $e) {
            $cc->rollback();
            throw $e;
        }
    }

    public function existEstadoFinanciero($codigo, $denominacion)
    {
        $existeCodigo = Doctrine::getTable('DatEstadofinanciero')->findBy('codigo', $codigo)->toArray();
        $existeDenominacion = Doctrine::getTable('DatEstadofinanciero')->findBy('denominacion', $denominacion)->toArray();
        if ($existeDenominacion) {
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgExistDenominacion}";
        }
        if ($existeCodigo) {
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgExistCodigo}";
        }
        return -1;
    }

    static public function getEstadosFinancieros()
    {
        $data = DatEstadofinanciero::getAll()->toArray();
        foreach ($data as $indexData => $valueData) {
            $data[$indexData]['arrayinfija'] = unserialize($valueData['arrayinfija']);
            $data[$indexData]['arraypostfija'] = unserialize($valueData['arraypostfija']);
        }
        $cant = DatEstadofinanciero::getAll()->count();
        return array('datos' => $data, 'cantidad' => $cant);
    }

    static public function getReporteFinanciero($argArrayData)
    {
        try {
            $globalConcept = ZendExt_GlobalConcept::getInstance();
            $idEstructura = $globalConcept->Estructura->idestructura;
            $datComprobante = new DatComprobante();
            $ejercicio = $datComprobante->obtenerEjercicio($idEstructura, 4);
            $periodo = $datComprobante->obtenerPeriodo($idEstructura, 4);
            $postEjercicios = (strlen($argArrayData['idejercicio']) > 0) ? json_decode($argArrayData['idejercicio']) : $ejercicio[0]['idejercicio'];
            $postPeriodos = (strlen($argArrayData['idperiodo']) > 0) ? json_decode($argArrayData['idperiodo']) : $periodo[0]['idperiodo'];
            $arrayIdEjercicios = is_array($postEjercicios) ? $postEjercicios : array($postEjercicios);
            $arrayIdPeriodos = is_array($postPeriodos) ? $postPeriodos : array($postEjercicios);
            $postDimensiones = json_decode($argArrayData['iddimension']);
            $arrayDimensiones = is_array($postDimensiones) ? $postDimensiones : array($postDimensiones);
            $agFechaDesde = $argArrayData['desde'];
            $agFechaHasta = $argArrayData['hasta'];
            $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í",
                "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
            $agNombreAnalisis = strtr(strtoupper($argArrayData['titlef']), $utf8);
            $objEntidad = $globalConcept->Estructura;
            $arrIdEstructurae = $objEntidad->idestructura;
            $arrIdEjercicio = implode(",", $arrayIdEjercicios);
            $arrIdPeriodo = implode(",", $arrayIdPeriodos);
            $arrIdDimensiones = implode(",", $arrayDimensiones);
            $dm = Doctrine_Manager::getInstance();
            $cxn = $dm->getCurrentConnection();
            $iocExterno = ZendExt_IoC::getInstance();  // IoC Externo
            $nodosFormula = json_decode($argArrayData['nodos']);
            $fsubtotal = true;
            $fIgual = false;
            $resultf = array();
            $ejercicioModel = new DatEjerciciocontableModel();
            $periodoModel = new DatPeriodocontableModel();
            $objNomCuenta = new NomCuenta();
            foreach ($arrayIdEjercicios as $idejercicio) {
                $objEjercicio = $ejercicioModel->getEjercicio($idejercicio);
                $nombreEjercicio = $objEjercicio[0]['nombre'];
                foreach ($arrayIdPeriodos as $idperiodo) {
                    $objPeriodo = $periodoModel->getPeriodo($idperiodo);
                    $nombrePeriodo = $objPeriodo[0]['nombre'];
                    $objEstructura = $iocExterno->metadatos->DameEstructura($objEntidad->idestructura);
                    $glue = array();
                    $stack = new EvalMathStack();
                    foreach ($nodosFormula as $indexNodo => $token) {
                        if (in_array($token->concat, array('+', '-', '*', '/', '^', '%', '>', '<', '=', '>=', '<='))) {
                            if (is_null($op2 = $stack->pop()) || is_null($op1 = $stack->pop())) {
                                throw new Exception("Ha ocurrido un error cuando se procesaba los datos.");
                            }
                            switch ($token->concat) {
                                case '+': {
                                        $stack->push($op1->concat + $op2->concat);
                                        if (count($glue) < 2) {
                                            if (count($glue) == 1) {
                                                $opg1 = array_pop($glue);
                                                if (!$op2->concat) {
                                                    $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op1));
                                                } else {
                                                    $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                                }
                                            } else {
                                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                    Convert::ObjectToArray($op2));
                                            }
                                        } else {
                                            $opg2 = array_pop($glue);
                                            $opg1 = array_pop($glue);
                                            $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á",
                                                "é" => "É", "í" => "Í", "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î",
                                                "ô" => "Ô", "û" => "Û", "ç" => "Ç");
                                            $opg2[0]['descripcionpartida'] = "<b>" . strtr(strtoupper($token->descripcion),
                                                            $utf8) . '</b> ' . $opg2[0]['partida'];
                                            $glue[] = array($opg1, $opg2);
                                        }
                                        $subTotalInicialOp2 = $opg2[count($opg2) - 1]['sinicialf'] ? $opg2[count($opg2) - 1]['sinicialf']
                                                    : $opg2['sinicialf'];
                                        $subTotalInicialOp1 = $opg1[count($opg1) - 1]['sinicialf'] ? $opg1[count($opg1) - 1]['sinicialf']
                                                    : $opg1['sinicialf'];
                                        $subTotalPeriodoOp2 = $opg2[count($opg2) - 1]['speriodof'] ? $opg2[count($opg2) - 1]['speriodof']
                                                    : $opg2['speriodof'];
                                        $subTotalPeriodoOp1 = $opg1[count($opg1) - 1]['speriodof'] ? $opg1[count($opg1) - 1]['speriodof']
                                                    : $opg1['speriodof'];
                                        $subTotalAcumuladoOp2 = $opg2[count($opg2) - 1]['sacumuladof'] ? $opg2[count($opg2)
                                                - 1]['sacumuladof'] : $opg2['sacumuladof'];
                                        $subTotalAcumuladoOp1 = $opg1[count($opg1) - 1]['sacumuladof'] ? $opg1[count($opg1)
                                                - 1]['sacumuladof'] : $opg1['sacumuladof'];
                                        $subTotalDiferenciaOp2 = $opg2[count($opg2) - 1]['sdiferenciaf'] ? $opg2[count($opg2)
                                                - 1]['sdiferenciaf'] : $opg2['sdiferenciaf'];
                                        $subTotalDiferenciaOp1 = $opg1[count($opg1) - 1]['sdiferenciaf'] ? $opg1[count($opg1)
                                                - 1]['sdiferenciaf'] : $opg1['sdiferenciaf'];
                                        $subTotalEjercicioAntOp2 = $opg2[count($opg2) - 1]['sejercicioanteriorf'] ? $opg2[count($opg2)
                                                - 1]['sejercicioanteriorf'] : $opg2['sejercicioanteriorf'];
                                        $subTotalEjercicioAntOp1 = $opg1[count($opg1) - 1]['sejercicioanteriorf'] ? $opg1[count($opg1)
                                                - 1]['sejercicioanteriorf'] : $opg1['sejercicioanteriorf'];
                                        $subtotalSI += $subTotalInicialOp2;
                                        $subtotalSP += $subTotalPeriodoOp2;
                                        $subtotalSA += $subTotalAcumuladoOp2;
                                        $subtotalSD += $subTotalDiferenciaOp2;
                                        $subtotalSEA += $subTotalEjercicioAntOp2;
                                    }
                                    break;
                                case '-': {
                                        $stack->push($op1->concat - $op2->concat);
                                        if (count($glue) < 2) {
                                            if (count($glue) == 1) {
                                                $opg1 = array_pop($glue);
                                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                            } else {
                                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                    Convert::ObjectToArray($op2));
                                            }
                                        } else {
                                            $opg2 = array_pop($glue);
                                            $opg1 = array_pop($glue);
                                            if ($opg2[0]['descripcionpartida']) {
                                                $opg2[0]['descripcionpartida'] = "<b>" . strtoupper($token->descripcion) . '</b> ' . $opg2[0]['partida'];
                                            } else {
                                                $opg2['descripcionpartida'] = "<b>" . strtoupper($token->descripcion) . '</b> ' . $opg2[0]['partida'];
                                            }
                                            $glue[] = array($opg1, $opg2);
                                        }
                                        $subTotalInicialOp2 = $opg2[count($opg2) - 1]['sinicialf'] ? $opg2[count($opg2) - 1]['sinicialf']
                                                    : $opg2['sinicialf'];
                                        $subTotalInicialOp1 = $opg1[count($opg1) - 1]['sinicialf'] ? $opg1[count($opg1) - 1]['sinicialf']
                                                    : $opg1['sinicialf'];
                                        $subTotalPeriodoOp2 = $opg2[count($opg2) - 1]['speriodof'] ? $opg2[count($opg2) - 1]['speriodof']
                                                    : $opg2['speriodof'];
                                        $subTotalPeriodoOp1 = $opg1[count($opg1) - 1]['speriodof'] ? $opg1[count($opg1) - 1]['speriodof']
                                                    : $opg1['speriodof'];
                                        $subTotalAcumuladoOp2 = $opg2[count($opg2) - 1]['sacumuladof'] ? $opg2[count($opg2)
                                                - 1]['sacumuladof'] : $opg2['sacumuladof'];
                                        $subTotalAcumuladoOp1 = $opg1[count($opg1) - 1]['sacumuladof'] ? $opg1[count($opg1)
                                                - 1]['sacumuladof'] : $opg1['sacumuladof'];
                                        $subTotalDiferenciaOp2 = $opg2[count($opg2) - 1]['sdiferenciaf'] ? $opg2[count($opg2)
                                                - 1]['sdiferenciaf'] : $opg2['sdiferenciaf'];
                                        $subTotalDiferenciaOp1 = $opg1[count($opg1) - 1]['sdiferenciaf'] ? $opg1[count($opg1)
                                                - 1]['sdiferenciaf'] : $opg1['sdiferenciaf'];
                                        $subTotalEjercicioAntOp2 = $opg2[count($opg2) - 1]['sejercicioanteriorf'] ? $opg2[count($opg2)
                                                - 1]['sejercicioanteriorf'] : $opg2['sejercicioanteriorf'];
                                        $subTotalEjercicioAntOp1 = $opg1[count($opg1) - 1]['sejercicioanteriorf'] ? $opg1[count($opg1)
                                                - 1]['sejercicioanteriorf'] : $opg1['sejercicioanteriorf'];
                                        $subtotalSI -= $subTotalInicialOp2;
                                        $subtotalSP -= $subTotalPeriodoOp2;
                                        $subtotalSA -= $subTotalAcumuladoOp2;
                                        $subtotalSD -= $subTotalDiferenciaOp2;
                                        $subtotalSEA -= $subTotalEjercicioAntOp2;
                                    }
                                    break;
                                case '*': {
                                        $stack->push($op1->concat * $op2->concat);
                                        if (count($glue) < 2) {
                                            if (count($glue) >= 1) {
                                                $opg1 = array_pop($glue);
                                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                            } else {
                                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                    Convert::ObjectToArray($op2));
                                            }
                                        } else {
                                            $opg2 = array_pop($glue);
                                            $opg1 = array_pop($glue);
                                            $opg2[0]['descripcionpartida'] = "<b>" . strtoupper($token->descripcion) . '</b> ' . $opg2[0]['partida'];
                                            $glue[] = array($opg1, $opg2);
                                        }
                                        $subTotalInicialOp2 = $opg2[count($opg2) - 1]['sinicialf'] ? $opg2[count($opg2) - 1]['sinicialf']
                                                    : $opg2['sinicialf'];
                                        $subTotalInicialOp1 = $opg1[count($opg1) - 1]['sinicialf'] ? $opg1[count($opg1) - 1]['sinicialf']
                                                    : $opg1['sinicialf'];
                                        $subTotalPeriodoOp2 = $opg2[count($opg2) - 1]['speriodof'] ? $opg2[count($opg2) - 1]['speriodof']
                                                    : $opg2['speriodof'];
                                        $subTotalPeriodoOp1 = $opg1[count($opg1) - 1]['speriodof'] ? $opg1[count($opg1) - 1]['speriodof']
                                                    : $opg1['speriodof'];
                                        $subTotalAcumuladoOp2 = $opg2[count($opg2) - 1]['sacumuladof'] ? $opg2[count($opg2)
                                                - 1]['sacumuladof'] : $opg2['sacumuladof'];
                                        $subTotalAcumuladoOp1 = $opg1[count($opg1) - 1]['sacumuladof'] ? $opg1[count($opg1)
                                                - 1]['sacumuladof'] : $opg1['sacumuladof'];
                                        $subTotalDiferenciaOp2 = $opg2[count($opg2) - 1]['sdiferenciaf'] ? $opg2[count($opg2)
                                                - 1]['sdiferenciaf'] : $opg2['sdiferenciaf'];
                                        $subTotalDiferenciaOp1 = $opg1[count($opg1) - 1]['sdiferenciaf'] ? $opg1[count($opg1)
                                                - 1]['sdiferenciaf'] : $opg1['sdiferenciaf'];
                                        $subTotalEjercicioAntOp2 = $opg2[count($opg2) - 1]['sejercicioanteriorf'] ? $opg2[count($opg2)
                                                - 1]['sejercicioanteriorf'] : $opg2['sejercicioanteriorf'];
                                        $subTotalEjercicioAntOp1 = $opg1[count($opg1) - 1]['sejercicioanteriorf'] ? $opg1[count($opg1)
                                                - 1]['sejercicioanteriorf'] : $opg1['sejercicioanteriorf'];

                                        $subtotalSI *= $subTotalInicialOp2;
                                        $subtotalSP *= $subTotalPeriodoOp2;
                                        $subtotalSA *= $subTotalAcumuladoOp2;
                                        $subtotalSD *= $subTotalDiferenciaOp2;
                                        $subtotalSEA *= $subTotalEjercicioAntOp2;
                                    }
                                    break;
                                case '/': {
                                        if ($op2 == 0) {
                                            throw new Exception("Divisi&oacute;n por cero");
                                        }
                                        $stack->push($op1 / $op2);
                                        if (count($glue) < 2) {
                                            if (count($glue) > 1) {
                                                $opg1 = array_pop($glue);
                                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                            } else {
                                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                    Convert::ObjectToArray($op2));
                                            }
                                        } else {
                                            $opg2 = array_pop($glue);
                                            $opg1 = array_pop($glue);
                                            $opg2[0]['descripcionpartida'] = "<b>" . strtoupper($token->descripcion) . '</b> ' . $opg2[0]['partida'];
                                            $glue[] = array($opg1, $opg2);
                                        }
                                        $subTotalInicialOp2 = $opg2[count($opg2) - 1]['sinicialf'];
                                        $subTotalInicialOp1 = $opg1[count($opg1) - 1]['sinicialf'] ? $opg1[count($opg1) - 1]['sinicialf']
                                                    : $opg1['sinicialf'];
                                        $subTotalPeriodoOp2 = $opg2[count($opg2) - 1]['speriodof'] ? $opg2[count($opg2) - 1]['speriodof']
                                                    : $opg2['speriodof'];
                                        $subTotalPeriodoOp1 = $opg1[count($opg1) - 1]['speriodof'] ? $opg1[count($opg1) - 1]['speriodof']
                                                    : $opg1['speriodof'];
                                        $subTotalAcumuladoOp2 = $opg2[count($opg2) - 1]['sacumuladof'] ? $opg2[count($opg2)
                                                - 1]['sacumuladof'] : $opg2['sacumuladof'];
                                        $subTotalAcumuladoOp1 = $opg1[count($opg1) - 1]['sacumuladof'] ? $opg1[count($opg1)
                                                - 1]['sacumuladof'] : $opg1['sacumuladof'];
                                        $subTotalDiferenciaOp2 = $opg2[count($opg2) - 1]['sdiferenciaf'] ? $opg2[count($opg2)
                                                - 1]['sdiferenciaf'] : $opg2['sdiferenciaf'];
                                        $subTotalDiferenciaOp1 = $opg1[count($opg1) - 1]['sdiferenciaf'] ? $opg1[count($opg1)
                                                - 1]['sdiferenciaf'] : $opg1['sdiferenciaf'];
                                        $subTotalEjercicioAntOp2 = $opg2[count($opg2) - 1]['sejercicioanteriorf'] ? $opg2[count($opg2)
                                                - 1]['sejercicioanteriorf'] : $opg2['sejercicioanteriorf'];
                                        $subTotalEjercicioAntOp1 = $opg1[count($opg1) - 1]['sejercicioanteriorf'] ? $opg1[count($opg1)
                                                - 1]['sejercicioanteriorf'] : $opg1['sejercicioanteriorf'];

                                        $subtotalSI /= $subTotalInicialOp2;
                                        $subtotalSP /= $subTotalPeriodoOp2;
                                        $subtotalSA /= $subTotalAcumuladoOp2;
                                        $subtotalSD /= $subTotalDiferenciaOp2;
                                        $subtotalSEA /= $subTotalEjercicioAntOp2;
                                    }
                                    break;
                                case '^':
                                    $stack->push(pow($op1, $op2));
                                    if (count($glue) < 2) {
                                        if (count($glue) > 1) {
                                            $opg1 = array_pop($glue);
                                            $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                        } else {
                                            $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                Convert::ObjectToArray($op2));
                                        }
                                    } else {
                                        $opg2 = array_pop($glue);
                                        $opg1 = array_pop($glue);
                                        $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                                    }
                                    break;
                                case '%':
                                    $stack->push(($op1 * $op2) / 100);
                                    if (count($glue) < 2) {
                                        if (count($glue) > 1) {
                                            $opg1 = array_pop($glue);
                                            $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                        } else {
                                            $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                Convert::ObjectToArray($op2));
                                        }
                                    } else {
                                        $opg2 = array_pop($glue);
                                        $opg1 = array_pop($glue);
                                        $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                                    }
                                    break;
                                case '>':
                                    $stack->push($op1 > $op2);
                                    $glue[] = Convert::ObjectToArray($token);
                                    $glue[] = Convert::ObjectToArray($op2);
                                    break;
                                case '<':
                                    $stack->push($op1 < $op2);
                                    $glue[] = Convert::ObjectToArray($token);
                                    $glue[] = Convert::ObjectToArray($op2);
                                    break;
                                case '>=':
                                    $stack->push($op1 >= $op2);
                                    $glue[] = Convert::ObjectToArray($token);
                                    $glue[] = Convert::ObjectToArray($op2);
                                    break;
                                case '<=':
                                    $stack->push($op1 <= $op2);
                                    $glue[] = Convert::ObjectToArray($token);
                                    $glue[] = Convert::ObjectToArray($op2);
                                    break;
                                case '=': {
                                        /** @todo Mostrar Total MI y MD
                                         * retornar directo desde aqui glue
                                         */
                                        $fIgual = true;
                                        $stack->push($op1 == $op2);
                                        if (count($glue) < 2) {
                                            if (count($glue) > 1) {
                                                $opg1 = array_pop($glue);
                                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                            } else {
                                                $opg2 = array_pop($glue);
                                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token),
                                                    Convert::ObjectToArray($opg2));
                                            }
                                        } else {

                                            $opg2 = array_pop($glue);
                                            $opg1 = array_pop($glue);
                                            if (array_key_exists('descripcionpartida', $opg2)) {
                                                $opg2['descripcionpartida'] = "<b>" . strtoupper($token->descripcion) . '</b> ' . $opg2['partida'];
                                                $glue[] = array($opg1, $opg2);
                                            } else {
                                                $glue[] = array($opg1, array('idestructura' => $objEstructura[0]->idestructura,
                                                        'codigoestructura' => $objEstructura[0]->codigo,
//                                                                'descripcionestructura' => $objEstructuraEcon->text, 'descripcionpartida' => "<b>" . strtoupper($token->descripcion) . '</b> ', 'partida' => $token->descripcion,
                                                        'descripcionestructura' => $objEstructura[0]->denominacion, 'descripcionpartida' => '',
                                                        'partida' => $token->descripcion,
                                                        'type' => $token->idtipoconcepto, 'concat' => $token->concat, 'text' => $token->descripcion,
                                                        'sinicial' => '', 'speriodo' => '', 'sacumulado' => '', 'sdiferencia' => '',
                                                        'sejercicioanterior' => ''), $opg2);
                                            }
                                        }
                                        break;
                                    }
                            }
                        } else {
                            if (is_numeric($token->concat) || preg_match('[\d+\-\d+]', $token->concat)) {
                                switch ($token->idtipo) {
                                    case '1': {
                                            $grupoN1 = array();
                                            $stinicialg = 0;
                                            $stperiodog = 0;
                                            $stacumuladog = 0;
                                            $stdiferenciag = 0;
                                            $stejercicioanteriorg = 0;
                                            //1. Grupo
                                            $grupoN1[] = array(
                                                'idestructura' => $objEstructura[0]->idestructura,
                                                'codigoestructura' => $objEstructura[0]->codigo,
                                                'descripcionestructura' => $objEstructura[0]->denominacion,
                                                'type' => $token->idtipo,
                                                'concat' => $token->concat,
                                                'idpartida' => $token->idtipoconcepto,
                                                'descripcionpartida' => '<b>' . $token->descripcion . '</b>',
                                                'partida' => '<b>' . $token->descripcion . '</b>',
                                                'idejercicio' => $idejercicio,
                                                'ejercicio' => $nombreEjercicio,
                                                'idperiodo' => $idperiodo,
                                                'periodo' => $nombrePeriodo,
                                                'text' => $token->concat . ' ' . $token->descripcion,
                                                'sinicial' => '',
                                                'speriodo' => '',
                                                'sacumulado' => '',
                                                'sdiferencia' => '',
                                                'sejercicioanterior' => '');
                                            $arrayCuentasGrupoCont = $objNomCuenta->loadCuentasHijasPorGrupo($objEntidad->idestructura,
                                                    array($token->idtipoconcepto));
                                            if (count($arrayCuentasGrupoCont) > 0) {
                                                $arrayIdCuentas = self::getArrayIdCuentas($arrayCuentasGrupoCont);
                                                $arrIdCuentas = implode(", ", $arrayIdCuentas);
                                                $nodosFormula[$indexNodo]->subnodos = $cxn->fetchAll("SELECT * FROM mod_contabilidad.f_estadofinanciero(array[$arrIdEstructurae], array[$arrIdCuentas], array[4], '$agFechaDesde'::date, '$agFechaHasta'::date, array[$arrIdDimensiones], array[$arrIdEjercicio], array[$arrIdPeriodo])");
                                                foreach ($nodosFormula[$indexNodo]->subnodos as $arrayCuenta) {
                                                    //1. Grupo - Cuentas
//                                                if ($arrayCuenta['idejercicio'] == $idejercicio && $arrayCuenta['idperiodo'] == $idperiodo) {
                                                    $grupoN1[] = array(
                                                        'idestructura' => $arrayCuenta['idestructura'],
                                                        'codigoestructura' => $arrayCuenta['codigoestructura'],
                                                        'descripcionestructura' => $arrayCuenta['descripcionestr'],
                                                        'idejercicio' => $arrayCuenta['idejercicio'], 'ejercicio' => $arrayCuenta['descripcionejercicio'],
                                                        'idperiodo' => $arrayCuenta['idperiodo'], 'periodo' => $arrayCuenta['descripcionperiodo'],
                                                        'type' => '3',
                                                        'concat' => $arrayCuenta['concat'],
                                                        'idpartida' => $arrayCuenta['idcuenta'],
                                                        'descripcionpartida' => $arrayCuenta['descripcioncta'],
                                                        'partida' => $arrayCuenta['descripcioncta'],
                                                        'text' => $arrayCuenta['concat'] . ' ' . $arrayCuenta['descripcioncta'],
                                                        'sinicialf' => $arrayCuenta['saldoinicial'],
                                                        'speriodof' => $arrayCuenta['saldoperiodo'],
                                                        'sacumuladof' => $arrayCuenta['saldoacumulado'],
                                                        'sdiferenciaf' => $arrayCuenta['saldodiferencia'],
                                                        'sejercicioanteriorf' => $arrayCuenta['saldoejercicioanterior'],
                                                        'sinicial' => $arrayCuenta['saldoinicial'],
                                                        'speriodo' => $arrayCuenta['saldoperiodo'],
                                                        'sacumulado' => $arrayCuenta['saldoacumulado'],
                                                        'sdiferencia' => $arrayCuenta['saldodiferencia'],
                                                        'sejercicioanterior' => $arrayCuenta['saldoejercicioanterior']
                                                    );
//                                                }

                                                    $stinicialg += $arrayCuenta['saldoinicial'];
                                                    $stperiodog += $arrayCuenta['saldoperiodo'];
                                                    $stacumuladog += $arrayCuenta['saldoacumulado'];
                                                    $stdiferenciag += $arrayCuenta['saldodiferencia'];
                                                    $stejercicioanteriorg += $arrayCuenta['saldoejercicioanterior'];
                                                }
                                                $grupoN1[] = array(
                                                    'idestructura' => $objEstructura[0]->idestructura,
                                                    'codigoestructura' => $objEstructura[0]->codigo,
                                                    'descripcionestructura' => $objEstructura[0]->denominacion,
                                                    'type' => 'total',
                                                    'descripcionpartida' => '<b>TOTAL ' . $token->descripcion . ' </b>',
                                                    'partida' => '<b>TOTAL  ' . $token->descripcion . '</b>',
                                                    'text' => '<b>TOTAL  ' . $token->descripcion . ' </b>',
                                                    'sinicialf' => $stinicialg,
                                                    'speriodof' => $stperiodog,
                                                    'sacumuladof' => $stacumuladog,
                                                    'sdiferenciaf' => $stdiferenciag,
                                                    'sejercicioanteriorf' => $stejercicioanteriorg,
                                                    'sinicial' => $stinicialg,
                                                    'speriodo' => $stperiodog,
                                                    'sacumulado' => $stacumuladog,
                                                    'sdiferencia' => $stdiferenciag,
                                                    'sejercicioanterior' => $stejercicioanteriorg
                                                );
                                            } else {
                                                $grupoN1[] = array(
                                                    'idestructura' => $objEntidad->idestructura,
                                                    'codigoestructura' => $objEntidad->codigoestructura,
                                                    'descripcionestructura' => $objEntidad->descripcionestr,
                                                    'idejercicio' => $idejercicio, 'ejercicio' => $nombreEjercicio,
                                                    'idperiodo' => $idperiodo, 'periodo' => $nombrePeriodo,
                                                    'type' => '',
                                                    'concat' => '',
                                                    'idpartida' => '',
                                                    'descripcionpartida' => 'No existe',
                                                    'partida' => 'No existe',
                                                    'text' => 'No existe',
                                                    'sinicialf' => 0,
                                                    'speriodof' => 0,
                                                    'sacumuladof' => 0,
                                                    'sdiferenciaf' => 0,
                                                    'sejercicioanteriorf' => 0,
                                                    'sinicial' => 0,
                                                    'speriodo' => 0,
                                                    'sacumulado' => 0,
                                                    'sdiferencia' => 0,
                                                    'sejercicioanterior' => 0
                                                );
                                            }

//                                            if ($subtotalSI == 0 && $subtotalSP == 0 && $subtotalSA == 0 && $subtotalSD == 0 && $subtotalSEA == 0) {
//                                                return array();
//                                            }

                                            if ($fsubtotal == true) {
                                                $subtotalSI = $stinicialg;
                                                $subtotalSP = $stperiodog;
                                                $subtotalSA = $stacumuladog;
                                                $subtotalSD = $stdiferenciag;
                                                $subtotalSEA = $stejercicioanteriorg;
                                                $fsubtotal = false;
                                            }
                                            $glue[] = $grupoN1;
                                        }
                                        break;
                                    case '2': {
                                            $contenidoN2 = array();
                                            $stinicialc = 0;
                                            $stperiodoc = 0;
                                            $stacumuladoc = 0;
                                            $stdiferenciac = 0;
                                            $stejercicioanteriorc = 0;
                                            //1. Contenidos
                                            $contenidoN2[] = array(
                                                'idestructura' => $objEstructura[0]->idestructura,
                                                'codigoestructura' => $objEstructura[0]->codigo,
                                                'descripcionestructura' => $objEstructura[0]->denominacion,
                                                'type' => $token->idtipo,
                                                'concat' => $token->concat,
                                                'idpartida' => $token->idtipoconcepto,
                                                'descripcionpartida' => $token->descripcion,
                                                'partida' => '<b>' . $token->descripcion . '</b>',
                                                'idejercicio' => $idejercicio, 'ejercicio' => $nombreEjercicio,
                                                'idperiodo' => $idperiodo, 'periodo' => $nombrePeriodo,
                                                'text' => $token->concat . ' ' . $token->descripcion,
                                                'sinicial' => '',
                                                'speriodo' => '',
                                                'sacumulado' => '',
                                                'sdiferencia' => '',
                                                'sejercicioanterior' => '');
                                            $arrayCuentasGrupoCont = $objNomCuenta->loadCuentasHijasPorContenido($objEntidad->idestructura,
                                                    array($token->idtipoconcepto));
                                            $arrayIdCuentas = self::getArrayIdCuentas($arrayCuentasGrupoCont);
                                            $arrIdCuentas = implode(", ", $arrayIdCuentas);
                                            /* die("SELECT * FROM mod_contabilidad.f_estadofinanciero(array[$arrIdEstructurae], array[$arrIdCuentas], array[4], '$agFechaDesde', '$agFechaHasta', array[$arrIdDimensiones], array[$arrIdEjercicio], array[$arrIdPeriodo])"); */
                                            $nodosFormula[$indexNodo]->subnodos = $cxn->fetchAll("SELECT * FROM mod_contabilidad.f_estadofinanciero(array[$arrIdEstructurae], array[$arrIdCuentas], array[4], '$agFechaDesde', '$agFechaHasta', array[$arrIdDimensiones], array[$arrIdEjercicio], array[$arrIdPeriodo])");
                                            foreach ($nodosFormula[$indexNodo]->subnodos as $arrayCuenta) {
                                                //2. Contenidos - Cuentas
//                                                if ($arrayCuenta['idejercicio'] == $idejercicio && $arrayCuenta['idperiodo'] == $idperiodo) {
                                                $contenidoN2[] = array(
                                                    'idestructura' => $arrayCuenta['idestructura'],
                                                    'codigoestructura' => $arrayCuenta['codigoestructura'],
                                                    'descripcionestructura' => $arrayCuenta['descripcionestr'],
                                                    'idejercicio' => $arrayCuenta['idejercicio'], 'ejercicio' => $arrayCuenta['descripcionejercicio'],
                                                    'idperiodo' => $arrayCuenta['idperiodo'], 'periodo' => $arrayCuenta['descripcionperiodo'],
                                                    'type' => '3',
                                                    'idpartida' => $arrayCuenta['idcuenta'],
                                                    'concat' => $arrayCuenta['concat'],
                                                    'descripcionpartida' => $arrayCuenta['descripcioncta'],
                                                    'partida' => $arrayCuenta['descripcioncta'],
                                                    'text' => $arrayCuenta['concat'] . ' ' . $arrayCuenta['descripcioncta'],
                                                    'sinicialf' => $arrayCuenta['saldoinicial'],
                                                    'speriodof' => $arrayCuenta['saldoperiodo'],
                                                    'sacumuladof' => $arrayCuenta['saldoacumulado'],
                                                    'sdiferenciaf' => $arrayCuenta['saldodiferencia'],
                                                    'sejercicioanteriorf' => $arrayCuenta['saldoejercicioanterior'],
                                                    'sinicial' => $arrayCuenta['saldoinicial'],
                                                    'speriodo' => $arrayCuenta['saldoperiodo'],
                                                    'sacumulado' => $arrayCuenta['saldoacumulado'],
                                                    'sdiferencia' => $arrayCuenta['saldodiferencia'],
                                                    'sejercicioanterior' => $arrayCuenta['saldoejercicioanterior']
                                                );
//                                                }
                                                $stinicialc += $arrayCuenta['saldoinicial'];
                                                $stperiodoc += $arrayCuenta['saldoperiodo'];
                                                $stacumuladoc += $arrayCuenta['saldoacumulado'];
                                                $stdiferenciac += $arrayCuenta['saldodiferencia'];
                                                $stejercicioanteriorc += $arrayCuenta['saldoejercicioanterior'];
                                            }
//                                            $contenidoN2[] = array(
//                                                'idestructura' => $objEstructura[0]->idestructura,
//                                                'codigoestructura' => $objEstructura[0]->codigo,
//                                                'descripcionestructura' => $objEstructura[0]->denominacion,
//                                                'type' => 'total',
//                                                'descripcionpartida' => 'TOTAL  ' . $token->descripcion,
//                                                'partida' => 'TOTAL  ' . $token->descripcion,
//                                                'text' => 'TOTAL  ' . $token->descripcion,
//                                                'sinicialf' => $stinicialc,
//                                                'speriodof' => $stperiodoc,
//                                                'sacumuladof' => $stacumuladoc,
//                                                'sdiferenciaf' => $stdiferenciac,
//                                                'sejercicioanteriorf' => $stejercicioanteriorc,
//                                                'sinicial' => $stinicialc,
//                                                'speriodo' => $stperiodoc,
//                                                'sacumulado' => $stacumuladoc,
//                                                'sdiferencia' => $stdiferenciac,
//                                                'sejercicioanterior' => $stejercicioanteriorc
//                                            );
                                            if ($fsubtotal == true) {
                                                $subtotalSI = $stinicialc;
                                                $subtotalSP = $stperiodoc;
                                                $subtotalSA = $stacumuladoc;
                                                $subtotalSD = $stdiferenciac;
                                                $subtotalSEA = $stejercicioanteriorc;
                                                $fsubtotal = false;
                                            }
                                            $glue[] = $contenidoN2;
                                        }
                                        break;
                                    case '3': {
                                            $cuentaN3 = array();
                                            $arrayIdCuentas = array($token->idtipoconcepto);
                                            $arrIdCuentas = implode(", ", $arrayIdCuentas);
                                            $nodosFormula[$indexNodo]->subnodos = $cxn->fetchAll("SELECT * FROM mod_contabilidad.f_estadofinanciero(array[$arrIdEstructurae], array[$arrIdCuentas], array[4], '$agFechaDesde', '$agFechaHasta', array[$arrIdDimensiones], array[$arrIdEjercicio], array[$arrIdPeriodo])");
                                            foreach ($nodosFormula[$indexNodo]->subnodos as $arrayCuenta) {
                                                //3. Cuentas
//                                                if ($arrayCuenta['idejercicio'] == $idejercicio && $arrayCuenta['idperiodo'] == $idperiodo) {
                                                $cuentaN3[] = array(
                                                    'idestructura' => $arrayCuenta['idestructura'],
                                                    'codigoestructura' => $arrayCuenta['codigoestructura'],
                                                    'descripcionestructura' => $arrayCuenta['descripcionestr'],
                                                    'idejercicio' => $objEjercicio[0]->idejercicio, 'ejercicio' => $arrayCuenta['descripcionejercicio'],
                                                    'idperiodo' => $idperiodo, 'periodo' => $arrayCuenta['descripcionperiodo'],
                                                    'type' => '3',
                                                    'concat' => $arrayCuenta['concat'],
                                                    'idpartida' => $arrayCuenta['idcuenta'],
                                                    'descripcionpartida' => $arrayCuenta['descripcioncta'],
                                                    'partida' => $arrayCuenta['descripcioncta'],
                                                    'text' => $arrayCuenta['concat'] . ' ' . $arrayCuenta['descripcioncta'],
                                                    'sinicialf' => $arrayCuenta['saldoinicial'],
                                                    'speriodof' => $arrayCuenta['saldoperiodo'],
                                                    'sacumuladof' => $arrayCuenta['saldoacumulado'],
                                                    'sdiferenciaf' => $arrayCuenta['saldodiferencia'],
                                                    'sejercicioanteriorf' => $arrayCuenta['saldoejercicioanterior'],
                                                    'sinicial' => $arrayCuenta['saldoinicial'],
                                                    'speriodo' => $arrayCuenta['saldoperiodo'],
                                                    'sacumulado' => $arrayCuenta['saldoacumulado'],
                                                    'sdiferencia' => $arrayCuenta['saldodiferencia'],
                                                    'sejercicioanterior' => $arrayCuenta['saldoejercicioanterior']
                                                );
//                                                }
                                            }
                                            $glue[] = $cuentaN3;
                                        }
                                        break;
                                }
                                $stack->push($token);
                            } elseif (array_key_exists($token->concat, $this->v)) {
                                $stack->push($this->v[$token]);
                            } else {
                                throw new Exception("Error durante la evaluaci&oacute;n de la f&oacute;rmula. Variable indefinida " . $token->concat . ".");
                            }
                        }
                    }
                    if ($stack->count != 1) {
                        throw new Exception("Ha ocurrido un error cuando se procesaba los datos.");
                    }
                    $stack->pop();
                    self::getArrayByNodosRecursivo($glue, $resultf);
                    $resultf[] = array(
                        'Entidad' => '',
                        'idestructura' => $objEstructura[0]->idestructura,
                        'codigoestructura' => $objEstructura[0]->codigo,
                        'descripcionestructura' => $objEstructura[0]->denominacion,
                        'descripcionpartida' => "<b>$agNombreAnalisis</b>",
                        'partida' => "<b>$agNombreAnalisis</b>",
                        'type' => 'totalneto',
                        'idmoneda' => '',
                        'codigoiso' => '',
                        'idejercicio' => '',
                        'ejercicio' => '',
                        'idperiodo' => '',
                        'periodo' => '',
                        'sinicialf' => $subtotalSI,
                        'speriodof' => $subtotalSP,
                        'sacumuladof' => $subtotalSA,
                        'sdiferenciaf' => $subtotalSD,
                        'sejercicioanteriorf' => $subtotalSEA,
                        'sinicial' => $subtotalSI,
                        'speriodo' => $subtotalSP,
                        'sacumulado' => $subtotalSA,
                        'sdiferencia' => $subtotalSD,
                        'promedio' => '',
                        'sejercicioanterior' => $subtotalSEA,
                    );
                }
            }
            if ($subtotalSI == 0 && $subtotalSP == 0 && $subtotalSA == 0 && $subtotalSD == 0 && $subtotalSEA == 0) {
                return array();
            } else {
                return $resultf;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getDataRptEstadoFinanciero(& $datosReporte, $arrayConfRpt)
    {
        $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í", "ó" => "Ó",
            "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
        $agIdReporte = '10005';
        $arrayDimensiones = json_decode($arrayConfRpt['iddimension']);
        $agFechaDesde = $arrayConfRpt['desde'];
        $agFechaHasta = $arrayConfRpt['hasta'];
        $titlef = strtr(strtoupper($arrayConfRpt['titlef']), $utf8);
        $globalConcept = ZendExt_GlobalConcept::getInstance();
        $objEntidad = $globalConcept->Estructura;
        $idestructura = $objEntidad->idestructura;
        $iocExterno = ZendExt_IoC::getInstance();  // IoC Externo
        $dataSet = array();
        foreach ($datosReporte as $data) {
            $dataSet[] = $data;
            $datoCuerpo[] = $data;
        }
        if (count($datosReporte) > 0) {
            $datosEmpresa = $iocExterno->configuracion->getDatosEmpresa($idestructura);
            list($datoGeneral['diaPeriodoDesde'], $datoGeneral['mesPeriodoDesde'], $datoGeneral['yearPeriodoDesde']) = explode("/",
                    $agFechaDesde);
            list($datoGeneral['diaPeriodo'], $datoGeneral['mesPeriodo'], $datoGeneral['yearPeriodo']) = explode("/",
                    $agFechaHasta);
            list($datoGeneral['diaActual'], $datoGeneral['mesActual'], $datoGeneral['yearActual']) = explode("/",
                    date('d/m/Y'));
            $datComprobante = new DatComprobante();
            $fechaContable = $datComprobante->obtenerFecha($globalConcept->Estructura->idestructura, 4);
            list($datoGeneral['diaContable'], $datoGeneral['mesContable'], $datoGeneral['yearContable']) = explode("/",
                    $fechaContable);
            $datoGeneral['fechaDesde'] = $agFechaDesde;
            $datoGeneral['fechaHasta'] = $agFechaHasta;
            $datoGeneral['usuario'] = $globalConcept->Perfil->nombre . ' ' . $globalConcept->Perfil->apellidos;
            $datoGeneral['titulo'] = $titlef;
            $datoGeneral['dimensiones'] = $arrayDimensiones;
            $dataEntidad = $iocExterno->metadatos->DameEstructura($idestructura);
            $datoGeneral['datosEntidad'] = $dataEntidad;
            $datoGeneral['reporte'] = $agIdReporte;
            $datoGeneral['logo'] = $datosEmpresa['logo'];
            $datosReporteHtmlPdf[] = array('datoGeneral' => $datoGeneral, 'datoCuerpo' => $datoCuerpo, 'datoPie' => null);
            $dataEstadoFinanciero = array('idreporte' => $agIdReporte, 'datos' => $dataSet, 'cantidad' => count($dataSet),
                'dataSources' => $datosReporteHtmlPdf);
        }
        return $dataEstadoFinanciero;
    }

    static public function getDimensiones()
    {
        $objNomDimension = new NomDimension();
        $result = $objNomDimension->GetTodos();
        return $dimensiones = array('cantidad' => $result->count(), 'datos' => $result->toArray());
    }

    /**
     * Obtener el arreglo de id cuentas a partir de un arreglo de objetos cuentas.
     * @param Array $arrayDataCuentas Arreglo de objetos cuentas.
     * @return Array Devuelve un arreglo id cuentas.
     */
    public function getArrayIdCuentas($arrayDataCuentas)
    {
        foreach ($arrayDataCuentas as $dataCuenta) {
            if (is_array($dataCuenta)) {
                $arrayIdCuentas[] = $dataCuenta['id'];
            } else {
                $arrayIdCuentas[] = $dataCuenta->id;
            }
        }
        return $arrayIdCuentas;
    }

    var $suppress_errors = false;
    var $last_error = null;
    var $v = array('e' => 2.71, 'pi' => 3.14);
    var $f = array();
    var $vb = array('e', 'pi');
    var $fb = array('sin', 'sinh', 'arcsin', 'asin', 'arcsinh', 'asinh', 'cos', 'cosh', 'arccos', 'acos', 'arccosh', 'acosh',
        'tan', 'tanh', 'arctan', 'atan', 'arctanh', 'atanh', 'sqrt', 'abs', 'ln', 'log');

    public function NotacionPolacaInversa()
    {
        $this->v['pi'] = pi();
        $this->v['e'] = exp(1);
    }

    public function FunctionValue($agExpr)
    {
        return $this->Evaluar($agExpr);
    }

    public function Evaluar($agExpression)
    {
        $expr = $agExpression;
        $this->last_error = null;
        $expr = trim($expr);
        if (substr($expr, -1, 1) == ';') {
            $expr = substr($expr, 0, strlen($expr) - 1);
        }
        if (preg_match('/^\s*([a-z]\w*)\s*=\s*(.+)$/', $expr, $matches)) {
            if (in_array($matches[1], $this->vb)) {
                throw new Exception("No es posible asignar constante '$matches[1]'.");
            }
            if (($tmp = $this->evaluarPolaca($this->ObtenerPolaca($matches[2]))) === false) {
                return false;
            }
            $this->v[$matches[1]] = $tmp;
            return $this->v[$matches[1]];
        } elseif (preg_match('/^\s*([a-z]\w*)\s*\(\s*([a-z]\w*(?:\s*,\s*[a-z]\w*)*)\s*\)\s*=\s*(.+)$/', $expr, $matches)) {
            $fnn = $matches[1];
            if (in_array($matches[1], $this->fb)) {
                throw new Exception("No se puede redefinir la funciÃ³n incorporada '$matches[1]()'.");
            }
            $args = explode(",", preg_replace("/\s+/", "", $matches[2]));
            if (($stack = $this->ObtenerPolaca($matches[3])) === false) {
                return false;
            }
            for ($i = 0; $i < count($stack); $i++) {
                $token = $stack[$i];
                if (preg_match('/^[a-z]\w*$/', $token) and !in_array($token, $args)) {
                    if (array_key_exists($token, $this->v)) {
                        $stack[$i] = $this->v[$token];
                    } else {
                        throw new Exception("Variable indefinida '$token' en la definiciÃ³n de la funciÃ³n.");
                    }
                }
            }
            $this->f[$fnn] = array('args' => $args, 'func' => $stack);
            return true;
        } else {
            return $this->evaluarPolaca($this->ObtenerPolaca($expr));
        }
    }

    public function vars()
    {
        $output = $this->v;
        unset($output['pi']);
        unset($output['e']);
        return $output;
    }

    public function funcs()
    {
        $output = array();
        foreach ($this->f as $fnn => $dat) {
            $output[] = $fnn . '(' . implode(',', $dat['args']) . ')';
        }
        return $output;
    }

    /**
     * Obtener la Notaci&oacute;n Polaca a partir de una f&oacute;rmula y su arreglo de nodos.
     * @param String $agFormulaStr F&oacute;rmula para obtener su notaci&oacute;n polaca.
     * @param Array $agArrayNodos Nodos de la f&oacute;rmula.
     * @return Array Arreglo de nodos en <b>notaci&oacute;n polaca</b>.
     */
    public function ObtenerPolaca($agFormulaStr, $agArrayNodos)
    {
        $index = 0;
        $indexObjs = 0;
        $stack = new EvalMathStack;
        $stactObjs = new EvalMathStack;
        $output = array();
        $polacap = array();
        $outputObjs = array();
        $agFormulaStr = trim(strtolower($agFormulaStr));

        $ops = array('+', '-', '*', '/', '^', '_', '%', '>', '<', '=', '>=', '<=');
        $ops_r = array('+' => 0, '-' => 0, '*' => 0, '/' => 0, '^' => 1, '%' => 0, '>' => 0, '<' => 0, '=' => 0, '>=' => 0,
            '<=' => 0);
        $ops_p = array('+' => 0, '-' => 0, '*' => 1, '/' => 1, '_' => 1, '%' => 1, '^' => 2, '>' => 0, '<' => 0, '=' => -1,
            '>=' => 0, '<=' => 0);

        $expecting_op = false;

        if (preg_match("/[^\w\s+*^%\/()\.,-<>=]/", $agFormulaStr, $matches)) {
            throw new Exception("Car&aacute;cter incorrecto '{$matches[0]}'.");
        }

        while (1) {
            $op = substr($agFormulaStr, $index, 1);
            $ex = preg_match('/^([a-z]\w*\(?|\d+(?:\.\d*)?|\.\d+|\()/', substr($agFormulaStr, $index), $match);
            if ($op == '-' and !$expecting_op) {
                $stack->push('_');
                $index++;
                $indexObjs++;
            } elseif ($op == '_') {
                throw new Exception("Car&aacute;cter incorrecto '_'.");
            } elseif ((in_array($op, $ops) or $ex) and $expecting_op) {
                if ($ex) {
                    $op = '*';
                    $index--;
                    $indexObjs--;
                }
                while ($stack->count > 0 and ( $o2 = $stack->last()) and in_array($o2, $ops) and ( $ops_r[$op] ? $ops_p[$op]
                        < $ops_p[$o2] : $ops_p[$op] <= $ops_p[$o2])) {
                    $outOp = $stack->pop();
                    $output[] = NomOperador::BuscarIdOperador($outOp);
                    $polacap[] = $outOp;
                    $out = new stdClass();
                    $out->idtipoconcepto = 5;
                    $out->idtipo = $output[count($output) - 1];
                    $out->concat = NomOperador::BuscarOperador($output[count($output) - 1]);
                    $out->valor = NomOperador::BuscarOperador($output[count($output) - 1]);
                    $out->descripcion = NomOperador::BuscarDescripcionOperador($output[count($output) - 1]);
                    $outputObjs[] = $out;
                }

                $stack->push($op);
                $index++;
                $indexObjs++;
                $expecting_op = false;
            } elseif ($op == ')' and $expecting_op) {
                while (($o2 = $stack->pop()) != '(') {
                    if (is_null($o2)) {
                        throw new Exception("Inesperado ')'.");
                    } else {
                        $polacap[] = $o2;
                        $output[] = NomOperador::BuscarIdOperador($o2);
                        $out = new stdClass();
                        $out->idtipoconcepto = 5;
                        $out->idtipo = $output[count($output) - 1];
                        $out->concat = NomOperador::BuscarOperador($output[count($output) - 1]);
                        $out->valor = NomOperador::BuscarOperador($output[count($output) - 1]);
                        $out->descripcion = NomOperador::BuscarDescripcionOperador($output[count($output) - 1]);
                        $outputObjs[] = $out;
                    }
                }
                if (preg_match("/^([a-z]\w*)\($/", $stack->last(2), $matches)) {
                    $fnn = $matches[1];
                    $arg_count = $stack->pop();
                    $output[] = $stack->pop();
                    $polacap[] = $output[count($output) - 1];
                    $outputObjs[] = $stactObjs->pop();
                    if (in_array($fnn, $this->fb)) {
                        if ($arg_count > 1) {
                            throw new Exception("Demasiados argumentos ($arg_count encontrado, 1 esperado).");
                        }
                    } elseif (array_key_exists($fnn, $this->f)) {
                        if ($arg_count != count($this->f[$fnn]['args'])) {
                            throw new Exception("N&uacute;mero incorrecto de argumentos ($arg_count encontrado, " . count($this->f[$fnn]['args']) . " esperado).");
                        }
                    } else {
                        throw new Exception("Error interno.");
                    }
                }
                $index++;
                $indexObjs++;
            } elseif ($op == '.' and $expecting_op) {
                while (($o2 = $stack->pop()) != '(') {
                    $obj = $stactObjs->pop();
                    if (is_null($o2)) {
                        throw new Exception("Se esperaba '.'.");
                    } else {
                        $polacap[] = $o2;
                        $output[] = NomOperador::BuscarIdOperador($o2);
                        $out = new stdClass();
                        $out->idtipoconcepto = 5;
                        $out->idtipo = $output[count($output) - 1];
                        $out->concat = NomOperador::BuscarOperador($output[count($output) - 1]);
                        $out->valor = NomOperador::BuscarOperador($output[count($output) - 1]);
                        $out->descripcion = NomOperador::BuscarDescripcionOperador($output[count($output) - 1]);
                        $outputObjs[] = $out;
                    }
                }
                if (!preg_match("/^([a-z]\w*)\($/", $stack->last(2), $matches)) {
                    throw new Exception("Se esperaba '.'.");
                }
                $stack->push($stack->pop() + 1);
                $stack->push('(');
                $index++;
                $indexObjs++;
                $expecting_op = false;
            } elseif ($op == '(' and !$expecting_op) {
                $stack->push('(');
                $index++;
                $indexObjs++;
                $allow_neg = true;
            } elseif ($ex and !$expecting_op) {
                $expecting_op = true;
                $val = $match[1];
                if (preg_match("/^([a-z]\w*)\($/", $val, $matches)) {
                    if (in_array($matches[1], $this->fb) or array_key_exists($matches[1], $this->f)) {
                        $stack->push($val);
                        $stack->push(1);
                        $stack->push('(');
                        $expecting_op = false;
                    } else {
                        $val = $matches[1];
                        $output[] = $val;
                        $polacap[] = $val;
                        $outputObjs[] = $agArrayNodos[$indexObjs];
                    }
                } else {
                    $output[] = $val;
                    $polacap[] = $val;
                    $outputObjs[] = $agArrayNodos[$indexObjs];
                }

                $index += strlen($val);
                $indexObjs++;
            } elseif ($op == ')') {
                throw new Exception("Se esperaba ')'.");
            } elseif (in_array($op, $ops) and !$expecting_op) {
                throw new Exception("Operador esperado '$op'.");
            } else {
                throw new Exception("Ha ocurrido un error inesperado.");
            }
            if ($index == strlen($agFormulaStr)) {
                if (in_array($op, $ops)) {
                    throw new Exception("Operador '$op' le falta operando.");
                } else {
                    break;
                }
            }
            while (substr($agFormulaStr, $index, 1) == ' ') {
                $index++;
            }
        }
        while (!is_null($op = $stack->pop())) {
            if ($op == '(') {
                throw new Exception("Se esperaba ')'.");
            }
            $polacap[] = $op;
            $output[] = NomOperador::BuscarIdOperador($op);
            $out = new stdClass();
            $out->idtipoconcepto = 5;
            $out->idtipo = $output[count($output) - 1];
            $out->concat = NomOperador::BuscarOperador($output[count($output) - 1]);
            $out->valor = NomOperador::BuscarOperador($output[count($output) - 1]);
            $out->descripcion = NomOperador::BuscarDescripcionOperador($output[count($output) - 1]);
            $outputObjs[] = $out;
        }
        return array('polaca' => $output, 'nodos' => $outputObjs, 'polaca_op' => $polacap);
    }

    /**
     * Evaluar la Notaci&oacute;n Polaca a partir de su arreglo de nodos en polaca y devolver todo en notaci&oacute;n infija.
     * @param Array $agArrayNodosRPN Nodos de la f&oacute;rmula en Notaci&oacute;n Polaca.
     * @return Array Arreglo de nodos en <b>notaci&oacute;n infija</b>.
     */
    public function evaluarPolaca($agArrayNodosRPN, $agVars = array())
    {
        $glue = array();
        if ($agArrayNodosRPN == false) {
            return false;
        }
        $stack = new EvalMathStack;
        foreach ($agArrayNodosRPN as $token) {
            if (in_array($token->concat, array('+', '-', '*', '/', '^', '%', '>', '<', '=', '>=', '<='))) {
                if (is_null($op2 = $stack->pop())) {
                    throw new Exception("Error durante la evaluaci&oacute; de la f&oacute;rmula.");
                }
                if (is_null($op1 = $stack->pop())) {
                    throw new Exception("Error durante la evaluaci&oacute; de la f&oacute;rmula.");
                }
                switch ($token->concat) {
                    case '+':
                        $stack->push($op1->concat + $op2->concat);
                        if (count($glue) < 2) {
                            if (count($glue) == 1) {
                                $opg1 = array_pop($glue);
                                if (!$op2->concat) {
                                    $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op1));
                                } else {
                                    $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                                }
                            } else {
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                    case '-':
                        $stack->push($op1->concat - $op2->concat);
                        if (count($glue) < 2) {
                            if (count($glue) == 1) {
                                $opg1 = array_pop($glue);
                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            } else {
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                    case '*':
                        $stack->push($op1->concat * $op2->concat);
                        if (count($glue) < 2) {
                            if (count($glue) >= 1) {
                                $opg1 = array_pop($glue);
                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            } else {
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                    case '/':
                        if ($op2 == 0) {
                            throw new Exception("Divisi&oacute;n por cero.");
                        }
                        $stack->push($op1 / $op2);
                        if (count($glue) < 2) {
                            if (count($glue) > 1) {
                                $opg1 = array_pop($glue);
                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            } else {
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                    case '^':
                        $stack->push(pow($op1, $op2));
                        if (count($glue) < 2) {
                            if (count($glue) > 1) {
                                $opg1 = array_pop($glue);
                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            } else {
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                    case '%':
                        $stack->push(($op1 * $op2) / 100);
                        if (count($glue) < 2) {
                            if (count($glue) > 1) {
                                $opg1 = array_pop($glue);
                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            } else {
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                    case '>':
                        $stack->push($op1 > $op2);
                        $glue[] = Convert::ObjectToArray($token);
                        $glue[] = Convert::ObjectToArray($op2);
                        break;
                    case '<':
                        $stack->push($op1 < $op2);
                        $glue[] = Convert::ObjectToArray($token);
                        $glue[] = Convert::ObjectToArray($op2);
                        break;
                    case '>=':
                        $stack->push($op1 >= $op2);
                        $glue[] = Convert::ObjectToArray($token);
                        $glue[] = Convert::ObjectToArray($op2);
                        break;
                    case '<=':
                        $stack->push($op1 <= $op2);
                        $glue[] = Convert::ObjectToArray($token);
                        $glue[] = Convert::ObjectToArray($op2);
                        break;
                    case '=':
                        $stack->push($op1 == $op2);
                        if (count($glue) < 2) {
                            if (count($glue) > 1) {
                                $opg1 = array_pop($glue);
                                $glue[] = array($opg1, Convert::ObjectToArray($token), Convert::ObjectToArray($op2));
                            } else {
                                $opg2 = array_pop($glue);
                                $glue[] = array(Convert::ObjectToArray($op1), Convert::ObjectToArray($token), Convert::ObjectToArray($opg2));
                            }
                        } else {
                            $opg2 = array_pop($glue);
                            $opg1 = array_pop($glue);
                            $glue[] = array($opg1, Convert::ObjectToArray($token), $opg2);
                        }
                        break;
                }
            } elseif ($token->concat == "_") {
                $stack->last()->concat *= -1;
            } elseif (preg_match("/^([a-z]\w*)\($/", $token->concat, $matches)) {
                $fnn = $matches[1];
                if (in_array($fnn, $this->fb)) { // built-in function:
                    if (is_null($op1 = $stack->pop())) {
                        throw new Exception("Error durante la evaluaci&oacute; de la f&oacute;rmula.");
                    }
                    $fnn = preg_replace("/^arc/", "a", $fnn);
                    if ($fnn == 'ln') {
                        $fnn = 'log';
                    }
                    eval('$stack->push(' . $fnn . '($op1));');
                } elseif (array_key_exists($fnn, $this->f)) {
                    $args = array();
                    for ($i = count($this->f[$fnn]['args']) - 1; $i >= 0; $i--) {
                        if (is_null($args[$this->f[$fnn]['args'][$i]] = $stack->pop())) {
                            throw new Exception("Error durante la evaluaci&oacute; de la f&oacute;rmula.");
                        }
                    }
                    $stack->push($this->evaluarPolaca($this->f[$fnn]['func'], $args)); // Recursividad
                }
            } else {
                if (is_numeric($token->concat) || is_float($token->concat) || is_double($token->concat) || is_real($token->concat)) {

                    $stack->push($token);
                } elseif (array_key_exists($token->concat, $this->v)) {
                    $stack->push($this->v[$token]);
                } elseif (array_key_exists($token->concat, $agVars)) {
                    $stack->push($agVars[$token]);
                } else {
                    throw new Exception("Error durante la evaluaci&oacute; de la f&oacute;rmula. Variable indefinida '$token'.");
                }
            }
        }
        if ($stack->count != 1) {
            throw new Exception("Error durante la evaluaci&oacute; de la f&oacute;rmula.");
        }
        $stack->pop();
        $resultf = $this->getArrayByNodosRecursivo($glue);
        return $resultf;
    }

    /**
     * Construir el arreglo final de nodos que permitir&aacute; evaluar una f&oacute;rmula nodo a nodo.
     * @param Array $agArrayNodos Arreglo de Nodos.
     * @param Array $result Par&aacute;metro pasado por referencia que ser&aacute; el Arreglo de Nodos final.
     * @return Array Arreglo de nodos final.
     */
    public function getArrayByNodosRecursivo($agArrayNodos, & $result)
    {
        foreach ($agArrayNodos as $value) {
            if (is_array($value) && $value['descripcionpartida']) {
                $result[] = $value;
            } else {
                self::getArrayByNodosRecursivo($value, $result);
            }
        }
        return $result;
    }

}

class EvalMathStack
{

    var $stack = array();
    var $count = 0;

    public function push($val)
    {
        $this->stack[$this->count] = $val;
        $this->count++;
    }

    public function pop()
    {
        if ($this->count > 0) {
            $this->count--;
            return $this->stack[$this->count];
        }
        return null;
    }

    public function last($n = 1)
    {
        return $this->stack[$this->count - $n];
    }

}

final class Convert
{

    static public function ObjectToArray(stdClass $Class)
    {
        $Class = (array) $Class;
        foreach ($Class as $key => $value) {
            if (is_object($value) && get_class($value) === 'stdClass') {
                $Class[$key] = self::ObjectToArray($value);
            } else {
                if (is_array($value)) {
                    $Class[$key] = self::ObjectToArray($value);
                }
            }
        }
        return $Class;
    }

    static public function ArrayToObject(array $array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = self::ArrayToObject($value);
            }
        }
        return (object) $array;
    }

}
