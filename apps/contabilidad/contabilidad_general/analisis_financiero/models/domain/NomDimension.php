<?php

class NomDimension extends BaseNomDimension
{

    public function setUp()
    {
        parent::setUp();
    }

    public function Buscar($idnomdimension)
    {
        $query = new Doctrine_Query();
        return $result = Doctrine::getTable('NomDimension')->find($idnomdimension)->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();
    }

    public function GetTodos()
    {
        $query = new Doctrine_Query();
        return $result = $query->from('NomDimension')->setHydrationMode(Doctrine::HYDRATE_RECORD)->execute();
    }

}
?>


