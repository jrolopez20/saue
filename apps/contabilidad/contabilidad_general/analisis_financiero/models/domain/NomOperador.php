<?php

class NomOperador extends BaseNomOperador {

    public function setUp() {
        parent::setUp();
    }

    public function Buscar($idoperador) {
        $temp = $this->conn->getTable('NomOperador')->find($idoperador);
        return $temp->toArray();
    }

    public function GetTodos() {
        $query = new Doctrine_Query ();
        $result = $query->from('NomOperador')->execute();
        return $result->toArray();
    }

    public function GetPorLimite($limite = 120, $inicio = 0) {
        $query = new Doctrine_Query ();
        $result = $query->from('NomOperador')->limit($limite = 120)->offset($inicio = 0)->execute();
        return $result->toArray();
    }

    static public function BuscarIdOperador($operador) {
        $query = new Doctrine_Query ();
        $result = $query->select('n.idoperador')
                ->from('NomOperador n')
                ->where("n.operador = '$operador'")
                ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                ->execute();
        return $result[0]['idoperador'];
    }

    static public function BuscarOperador($idOperador) {
        $query = new Doctrine_Query ();
        $result = $query->select('n.idoperador, n.operador')
                ->from('NomOperador n')
                ->where("n.idoperador = '$idOperador'")
                ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                ->execute();
        return $result[0]['operador'];
    }

    static public function BuscarDescripcionOperador($idOperador) {
        $query = new Doctrine_Query ();
        $result = $query->select('n.idoperador, n.descripcion')
                ->from('NomOperador n')
                ->where("n.idoperador = '$idOperador'")
                ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                ->execute();
        return $result[0]['descripcion'];
    }

    public function isNomOperador($operador) {
        $query = new Doctrine_Query ();
        $result = $query->select('count(n.idoperador)')
                ->from('NomOperador n')
                ->where("n.operador = '$operador'")
                ->setHydrationMode(Doctrine :: HYDRATE_ARRAY)
                ->execute();
        return ($result[0]['count'] > 0) ? true : false;
    }

}

?>


