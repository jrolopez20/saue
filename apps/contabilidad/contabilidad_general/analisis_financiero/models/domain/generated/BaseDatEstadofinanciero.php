<?php

abstract class BaseDatEstadofinanciero extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.dat_estadofinanciero');
        $this->hasColumn('idestadofinanciero', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_contabilidad.sec_datestadofinanciero'));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('formulaid', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('formulac', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('formulap', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('formulad', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('formulam', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('arraypostfija', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('arrayinfija', 'character varying', null, array ('notnull' => false,'primary' => false));
    }
    


}

