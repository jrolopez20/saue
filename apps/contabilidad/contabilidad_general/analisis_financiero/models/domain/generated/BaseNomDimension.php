<?php 

abstract class BaseNomDimension extends Doctrine_Record 
 { 
   public function setTableDefinition() 
    { 
       $this->setTableName('mod_contabilidad.nom_dimension'); 
       $this->hasColumn('iddimension', 'numeric', null, array('notnull' => true, 'primary' => true)); 
       $this->hasColumn('denominacion', 'character varying', 50 , array('notnull' => false, 'primary' => false)); 
    } 
 
   public function Setup() 
    { 
       parent::setUp(); 
    } 
 }
?>

