<?php

abstract class BaseNomOperador extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.nom_operador');
        $this->hasColumn('idoperador', 'numeric', null, array('notnull' => true, 'primary' => true));
        $this->hasColumn('operador', 'character varying', 10, array('notnull' => true, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', 255, array('notnull' => true, 'primary' => false));
    }

    public function Setup()
    {
        parent::setUp();
    }

}

?>

