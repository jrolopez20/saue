<?php

class CuentasService
{

    /**
     * Obtiene las Cuentas por su id.
     *
     * @param Array $arridcuenta
     * @return Array Cuentas por id
     */
    public function getCuentasPorID($arridcuenta)
    {
        $nomcta = new NomCuenta();
        $arrCtas = array();
        foreach ($arridcuenta as $cta) {
            $cta = $nomcta->loadCuentasPorId($cta);
            if (count($cta)) {
                $arrCtas[] = $cta[0];
            }
        }

        return $arrCtas;
    }

    /**
     * Obtiene el saldo de las Cuentas.
     *
     * @param Integer $arridctas
     * @param Integer $idestructura
     * @param Date $fecha
     * @return Array saldo de las Cuentas
     */
    public function getSaldoCuentas($arridctas, $idestructura, $fecha)
    {
        $nomcta = new NomCuenta();

        return $nomcta->saldoAcumuladoCtas($arridctas, $idestructura, $fecha);
    }

    /**
     * Obtiene el formato asociado a un nomenclador de $idnomenclador.
     *
     * @param Integer $arridctas
     * @return Array formato asociado a un nomenclador de $idnomenclador
     */
    public function getFormatoNomenclador($idnomenclador)
    {
        $nomctaM = new NomCuentaModel();

        return $nomctaM->loadDataFormatos($idnomenclador);
    }

    /**
     * Obtiene las Cuentas contables por nivel.
     *
     * @param Array $argparams (node)
     * @return Array Cuentas contables
     */
    public function getArbolCuentas($argparams)
    {
        $NomCuenta = new NomCuenta();
        if ($argparams['filter']) {
            return $NomCuenta->buscarCuentasContables($this->global->Estructura->idestructura, $argparams['filter'], TRUE, 1);
        } else {
            return $NomCuenta->loadDataCuentas((is_numeric($argparams['node']) ? $argparams['node'] : NULL), TRUE, 1);
        }
    }

    /**
     * Obtiene las Cuentas contables por nivel.
     *
     * @return Array Cuentas contables
     */
    public function getCuentasHojas($filter)
    {
        $NomCuenta = new NomCuenta();
        return $NomCuenta->loadDataCuentasNivel(1, $filter);
    }

}
