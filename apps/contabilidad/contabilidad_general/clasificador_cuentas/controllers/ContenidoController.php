<?php

/**
 * Clase controladora para gestionar los Contenidos economicos
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ContenidoController extends ZendExt_Controller_Secure
{

    private $model;
    private $nomContenido;

    public function init()
    {
        parent::init();
        $this->model = new NomContenidoModel();
        $this->nomContenido = new NomContenido();
    }

    public function contenidoAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadContenidosAction()
    {
        echo json_encode($this->model->loadContenidos($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadGruposContablesAction()
    {
        echo json_encode($this->model->loadGruposContables($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadNaturalezaAction()
    {
        echo json_encode($this->nomContenido->loadNaturaleza());
    }

    /**
     * call this model action
     */
    public function loadDataAddContenidoAction()
    {
        echo json_encode($this->model->loadDataAddContenido());
    }

    /**
     * call this model action
     */
    public function updateContenidoAction()
    {
        echo $this->model->updateContenido($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteContenidoAction()
    {
        echo $this->model->deleteContenido($this->_request->getPost());
    }

}
