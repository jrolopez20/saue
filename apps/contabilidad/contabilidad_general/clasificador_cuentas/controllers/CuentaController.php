<?php

/**
 * Clase controladora para gestionar el Clasificador de Cuentas Contables.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CuentaController extends ZendExt_Controller_Secure
{

    private $model;
    private $gmodel;

    public function init()
    {
        parent::init();
        $this->model = new NomCuentaModel();
        $this->gmodel = new CuentasAgrupadasModel();
    }

    public function cuentaAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadCuentasAction()
    {
        echo json_encode($this->model->loadDataCuentas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadCuentasAgrupadasAction()
    {
        echo json_encode($this->gmodel->loadDataCuentasAgrupadas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadContenidosAction()
    {
        echo json_encode($this->model->loadContenidos());
    }

    /**
     * call this model action
     */
    public function loadDataAddCuentaAction()
    {
        echo json_encode($this->model->loadDataAddCuenta($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function updateCuentaContableAction()
    {
        echo $this->model->updateCuentaContable($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function activarCuentaAction()
    {
        echo $this->model->activarCuenta($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteCuentaContableAction()
    {
        echo $this->model->deleteCuentaContable($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function loadNaturalezaAction()
    {
        echo json_encode(NomGrupo::getNaturaleza());
    }

    /**
     * call this model action
     */
    public function loadGruposAction()
    {
        echo json_encode(NomGrupo::loadGruposHojas());
    }

    /**
     * call this model action
     */
    public function getTreeCuentasAction()
    {
        echo json_encode($this->model->getCuentas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function vistaPreviaPlanCuentaAction()
    {
        echo json_encode($this->model->vistaPreviaPlanCuenta('root'));
    }

}
