<?php

/**
 * Clase controladora para gestionar los Grupos y Subgrupos Contables
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class GrupocontableController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new NomGrupoModel();
    }

    public function GrupocontableAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadGruposContablesAction()
    {
        echo json_encode($this->model->loadGruposContables($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataAddGrupoAction()
    {
        echo json_encode($this->model->loadDataAddGrupo($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function updateGrupoContableAction()
    {
        echo $this->model->updateGrupoContable($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteGrupoContableAction()
    {
        echo $this->model->deleteGrupoContable($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function loadNaturalezaAction()
    {
        echo json_encode(NomGrupo::getNaturaleza());
    }

}
