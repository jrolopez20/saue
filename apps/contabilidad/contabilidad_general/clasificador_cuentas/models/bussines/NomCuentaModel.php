<?php

/**
 * Clase modelo para configurar el Clasificador de Cuentas Contables.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomCuentaModel extends ZendExt_Model
{

    public function NomCuentaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene las Cuentas contables por nivel.
     *
     * @param Array $argparams (node)
     * @return Array Cuentas contables
     */
    public function loadDataCuentas($argparams)
    {
        $NomCuenta = new NomCuenta();
        if ($argparams['filter']) {
            return $NomCuenta->buscarCuentasContables($this->global->Estructura->idestructura, $argparams['filter'],
                            $argparams['checked'], $argparams['activa'], $argparams['saldo']);
        } else {
            return $NomCuenta->loadDataCuentas($this->global->Estructura->idestructura,
                            (is_numeric($argparams['node']) ? $argparams['node'] : NULL), $argparams['checked'],
                            $argparams['activa'], $argparams['saldo']);
        }
    }

    /**
     * Obtiene los Contenidos economicos
     *
     * @param none
     * @return Array Contenidos economicos
     */
    public function loadContenidos()
    {
        $NomContenido = new NomContenido();
        $datos = $NomContenido->loadContenidosAll();

        return array('datos' => $datos);
    }

    /**
     * Obtiene los datos para adicionar las Cuentas contables por nivel.
     *
     * @param Array $argparams (nivel)
     * @return Array  Cuentas contables
     */
    public function loadDataAddCuenta($argparams)
    {
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        if (!count($formato)) {
            $DatFormato = new DatFormato();
            $formato = $DatFormato->loadFormatosSubsistema($this->global->Estructura->idestructura, 4);
            if (count($formato)) {
                $this->asociarNivelFormato($formato, $argparams['nivel']);
            }

            return array('formato' => $formato);
        } else {
            return array('nivel' => $this->getNivelFormato($formato, $argparams['nivel']));
        }
    }

    /**
     * Asocia el Nivel a formato
     *
     * @param stdClass $objformato
     * @param Integer $nivel nivel a buscar
     * @return stdClass Formato con sus niveles
     */
    public function asociarNivelFormato(&$objformato, $nivel)
    {
        $DatParteformato = new DatParteformato();
        foreach ($objformato as &$v) {
            $v['nivel'] = $DatParteformato->buscarNivelFormato(array('idformato' => $v['idformato'], 'nivel' => $nivel, 'idparteformato' => ''));
            if (count($v['nivel'])) {
                $v['nivel'][0]['separador'] = $v['separador'];
            }
        }
    }

    /**
     * Obtiene los datos del Nivel de formato dado el idformato y nivel
     *
     * @param Array $formato
     * @param Integer $nivel nivel a buscar
     * @return stdClass Nivel de formato
     */
    public function getNivelFormato($formato, $nivel)
    {
        $DatParteformato = new DatParteformato();
        $nivel = $DatParteformato->buscarNivelFormato(array('idformato' => $formato[0]['idformato'], 'nivel' => $nivel, 'idparteformato' => ''));
        if (count($nivel)) {
            $nivel['0']['separador'] = $formato[0]['separador'];
        }

        return $nivel;
    }

    /**
     * Adiciona o modifica las Cuentas contables.
     *
     * @param Array $argparams (idcuenta, codigo, concatcta, denominacion, nivel, privado, idarbol, idcontenido, idcuentapadre, idnaturaleza, idparteformato)
     * @return boolean true if ocurred, false if failure
     */
    public function updateCuentaContable($argparams)
    {
        if ($argparams['idcuenta']) {
            return $this->Actualizar($argparams);
        } else {
            $NomCuenta = new NomCuenta();
            $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
            if (!count($formato)) {
                $NomCuenta->insertFromatoAsociado($this->global->Estructura->idestructura, $argparams['idformato']);
            }

            return $this->Insertar($argparams);
        }
    }

    /**
     * Adiciona la Cuenta contable.
     *
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($argparams)
    {

        $objCuenta = new NomCuenta();
        $objCuenta->codigo = $argparams['codigo'];
        $objCuenta->concatcta = $argparams['concatcta'];
        $objCuenta->denominacion = $argparams['denominacion'];
        $objCuenta->nivel = $argparams['nivel'];
        $objCuenta->privado = $argparams['privado'];
        $objCuenta->idarbol = $argparams['idarbol'];
        //$objCuenta->idcontenido = $argparams['idcontenido'];
        $objCuenta->idcuentapadre = ($argparams['idcuentapadre'] && $argparams['idcuentapadre'] != $argparams['idgrupo'])
                    ? $argparams['idcuentapadre'] : NULL;
        $objCuenta->idnaturaleza = $argparams['idnaturaleza'];
        $objCuenta->idparteformato = $argparams['idparteformato'];
        $objCuenta->idestructura = $this->global->Estructura->idestructura;
        $objCuenta->idgrupo = !empty($argparams['idgrupo']) ? $argparams['idgrupo'] : NULL;

        try {
            $objCuenta->save();

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            //echo $exc->getTraceAsString();
            throw $exc;
        }
    }

    /**
     * Modifica la Cuenta contable.
     *
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($argparams)
    {
        $objCuenta = Doctrine::getTable('NomCuenta')->find($argparams['idcuenta']);
        $objCuenta->codigo = $argparams['codigo'];
        $objCuenta->concatcta = $argparams['concatcta'];
        $objCuenta->denominacion = $argparams['denominacion'];
        $objCuenta->nivel = $argparams['nivel'];
        $objCuenta->privado = $argparams['privado'];
        $objCuenta->idarbol = $argparams['idarbol'];
        //$objCuenta->idcontenido = $argparams['idcontenido'];
        $objCuenta->idcuentapadre = ($argparams['idcuentapadre']) ? $argparams['idcuentapadre'] : NULL;
        $objCuenta->idnaturaleza = $argparams['idnaturaleza'];
        $objCuenta->idparteformato = $argparams['idparteformato'];
        $objCuenta->idestructura = $this->global->Estructura->idestructura;
        $objCuenta->idgrupo = !empty($argparams['idgrupo']) ? $argparams['idgrupo'] : NULL;
        try {
            $objCuenta->save();

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina la Cuenta contable.
     *
     * @param Integer $argparams (node)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteCuentaContable($argparams)
    {
        if (!$this->verificarUsoCuenta($argparams['idcuenta'])) {
            try {
                $objCuenta = Doctrine::getTable('NomCuenta')->find($argparams['idcuenta']);
                $objCuenta->delete();
                $this->deleteAsociacionCuenta();

                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
            } catch (Exception $exc) {
                if ($exc->getCode() == 23503) {
                    return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgCuentaUso}";
                } else {
                    throw $exc;
                }
            }
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgCuentaUso}";
        }
    }

    /**
     * Verifica si la Cuenta contable se encuentra en uso.
     *
     * @param Integer $idcuenta
     * @return boolean true if ocurred, false if failure
     */
    public function verificarUsoCuenta($idcuenta)
    {
        $ConfModelocontablecuenta = new ConfModelocontablecuenta();
        if ($ConfModelocontablecuenta->findUseCta($idcuenta)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Elimina el formato asociado al nomenclador de Cuentas contables si es el ultimo.
     *
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function deleteAsociacionCuenta()
    {
        $NomCuenta = new NomCuenta();
        $ctas = $NomCuenta->loadDataCuentas(NULL);
        if (!count($ctas)) {
            $NomCuenta->deleteFromatoAsociado($this->global->Estructura->idestructura);
        }
    }

    /**
     * Activa/desactiva la Cuenta contable.
     *
     * @param Integer $argparams (idcuenta, activa)
     * @return boolean true if ocurred, false if failure
     */
    public function activarCuenta($argparams)
    {
        try {
            $objCuenta = Doctrine::getTable('NomCuenta')->find($argparams['idcuenta']);
            $objCuenta->activa = $argparams['activa'];
            $objCuenta->save();
            $msg = ($argparams['activa']) ? "perfil.etiquetas.msgActivada" : "perfil.etiquetas.msgDesactivada";

            return "{'success':true, 'codMsg':1,'mensaje':$msg}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Busca el formato configurado para el clasificador de Grupo contable.
     *
     * @param none
     * @return Array formatos configurados para Grupo contable
     */
    public function getFormatoNomenclador($idnomenclador = 3)
    {
        $nomCuenta = new NomCuenta();
        $idestructura = $this->global->Estructura->idestructura;

        return ($idestructura) ? $nomCuenta->getFormatoAsociado($idestructura, $idnomenclador) : array();
    }

    /**
     * Lista los formatos configurados para Contabilidad.
     *
     * @param none
     * @return Array formatos configurados para Contabilidad
     */
    public function loadDataFormatos()
    {
        $formatos = $this->getFormatoNomenclador();
        if (count($formatos)) {
            $this->getComplementaryDataFormat($formatos);
        }

        return $formatos;
    }

    /**
     * Obtiene la informacion complementaria para los Formatos
     *
     * @param Array $formatos
     * @return void
     */
    public function getComplementaryDataFormat(&$formatos)
    {
        $nomCuenta = new NomCuenta();
        foreach ($formatos as &$v) {
            $parteFormato = $nomCuenta->listParteFormatos($v['idformato']);
            $estruct = array(); //estructura del formato
            $vistap = array(); //vista previa del formato
            foreach ($parteFormato as &$pf) {
                $estruct[] = $pf['abreviatura'];
                $pf['vistap'] = $this->replaceCero($pf['longitud']);
                $vistap[] = $pf['vistap'];
            }
            $v['estructura'] = (count($estruct)) ? implode($v['separador'], $estruct) : '';
            $v['vistap'] = (count($vistap)) ? implode($v['separador'], $vistap) : '';
            $v['partes'] = $parteFormato;
        }
    }

    /**
     * Formatea con 0 una expresion
     *
     * @param Integer $length
     * @return String La longitud representada con #
     */
    public function replaceCero($length)
    {
        $str_return = "";
        while ($length > 0) {
            $str_return .= "0";
            $length--;
        }

        return $str_return;
    }

    /**
     * Obtiene el arbol de cuentas
     *
     * @param array $post Arreglo con la peticion al servidor
     * @return array cuentas para el nivel solicitado
     */
    public function getCuentas($post)
    {
        $naturaleza = !empty($post['idnaturaleza']) ? json_decode(stripslashes($post['idnaturaleza'])) : array(8030, 8031,
            8032);
        $NomCuenta = new NomCuenta();

        if ($post['filter']) {
            return $this->getMatches($post['filter']);
//            $a = $NomCuenta->GetByCondition('concatcta ILIKE \'%' . $post['filter'] . '%\' OR lower(denominacion) ILIKE \'%' . strtolower($post['filter']) . '%\'');
//            return $NomCuenta->buscarCuentasContables($this->global->Estructura->idestructura, $post['filter'], $post['checked'], $post['activa'], $post['saldo']);
        } else {
            if ($post['node'] == 'root') {
                $data = $this->getTreeGrupos(NULL);

                return $data;
            } else {
                return $NomCuenta->loadDataCuentas($this->global->Estructura->idestructura,
                                (is_numeric($post['node']) ? $post['node'] : NULL), $post['checked'], $post['activa'],
                                $post['saldo'], $naturaleza);
            }
        }
    }

    private function getMatches($filter)
    {
        $array = array();
        $final = array();
        $model = new NomCuenta();

        $cuentas = $model->GetByCondition('concatcta ILIKE \'%' . $filter . '%\' OR lower(denominacion) ILIKE \'%' . strtolower($filter) . '%\'');

        foreach ($cuentas as $cuenta) {
            $array[$cuenta['idarbol']][$cuenta['nivel']][$cuenta['idcuenta']] = $cuenta;
        }

//        print_r($array);die;
//        $test = array(3 => 'tres');
//        $arrayobject = new ArrayObject($test);
//
//        for($iterator = $arrayobject->getIterator();
//            $iterator->valid();
//            $iterator->next()) {
//            if($iterator->key() > 1){
//                $test[$iterator->key()-1] = $iterator->key() - 1;
//                $iterator->offsetSet($iterator->key()-1,'');
//            }
//        }

        foreach ($array as &$trees) {
            foreach ($trees as $nivel => &$ctas) {
                foreach ($ctas as $idcta => &$cta) {
                    if ($nivel > 1) {
                        if (empty($trees[$nivel - 1][$cta['idcuentapadre']])) {
                            // si no existe la cuenta padre el el nivel anterior se busca y se agrega
                            $cuenta = $model->GetById($cta['idcuentapadre']);
                            $trees[$nivel - 1][$cuenta['idcuenta']] = $cuenta;
                        }
                        $trees[$nivel - 1][$cta['idcuentapadre']]['expanded'] = TRUE;
                        $trees[$nivel - 1][$cta['idcuentapadre']]['children'][] = $cta;
                    } else {
                        $final = array_merge($final, array_values($trees[$nivel]));
                    }
                }
            }
        }

        return $final;
    }

    private function getTreeGrupos($grupo = NULL)
    {
        if (!empty($grupo) && $grupo['leaf']) {
            return;
        }

        $NomGrupo = new NomGrupo();
        $grupos = (is_null($grupo)) ? $NomGrupo->loadGruposContables(NULL) : $NomGrupo->loadGruposContables($grupo['idgrupo']);

        if (!empty($grupos)) {
            foreach ($grupos as $grupo) {
                $obj = new stdClass();
                $obj->concatcta = $grupo['concatgrupo'];
                $obj->denominacion = $grupo['denominacion'];
                $obj->idcuenta = $grupo['idgrupo'];
                $obj->idgrupo = $grupo['idgrupo'];
                $obj->idnaturaleza = $grupo['idnaturaleza'];
                $obj->nodetype = 1; // 1 grupo o subgrupo, 2 cuenta
                $obj->nivel = $grupo['nivel'];
                $obj->qtip = $grupo['concatgrupo'] . ' ' . $grupo['denominacion'];
                $obj->leaf = FALSE;
//                $obj->cls ='x-grid-group-hd';

                $obj->separador = $grupo['DatParteformato']['DatFormato']['separador'];

                if (!$grupo['leaf']) {
                    $obj->expanded = TRUE;
                    $obj->text = '<b>' . $grupo['concatgrupo'] . ' ' . $grupo['denominacion'] . '</b>';
                    $obj->naturaleza = '<b>' . $grupo['NomNaturaleza']['naturaleza'] . '</b>';
                    $obj->allowaction = 0;
                    $obj->children = $this->getTreeGrupos($grupo);
                } else {
                    $obj->allowaction = 1;
                    $obj->text = '<i>' . $grupo['concatgrupo'] . ' ' . $grupo['denominacion'] . '</i>';
                    $obj->naturaleza = '<i>' . $grupo['NomNaturaleza']['naturaleza'] . '</i>';
                }

                $data [] = $obj;
            }

            return $data;
        }
    }

    /**
     * Carga el Plan de cuentas para la vista de reporte
     * @param String/float $agRoot Raiz a expandir
     * @return Array Plan de cuentas para la vista de reporte
     */
    public function vistaPreviaPlanCuenta($agRoot, &$agArrCtas = array())
    {
        $gCtas = $this->getCuentas(array('node' => $agRoot));
        foreach ($gCtas as $v) {
            $validation = (is_object($v) && get_class($v) === 'stdClass') ? $v->children : $v['children'];

            $agArrCtas[] = $this->formatPlanCuenta($v);
            if (count($validation)) {
                foreach ($validation as $child) {
                    $childValidation = (is_object($child) && get_class($child) === 'stdClass') ? $child->idcuenta : $child['idcuenta'];
                    $agArrCtas[] = $this->formatPlanCuenta($child);
                    $this->vistaPreviaPlanCuenta($childValidation, $agArrCtas);
                }
            } else {
                $validation = (is_object($v) && get_class($v) === 'stdClass') ? $v->idcuenta : $v['idcuenta'];
                $this->vistaPreviaPlanCuenta($validation, $agArrCtas);
            }
        }

        return $this->loadDataReport($agArrCtas);
    }

    private function formatPlanCuenta($agNode)
    {
        if (is_object($agNode) && get_class($agNode) === 'stdClass') {
            return $this->generateFormatPlanCuenta($agNode, 'object');
        } else {
            return $this->generateFormatPlanCuenta($agNode, 'array');
        }
    }

    private function generateFormatPlanCuenta($agDataNode, $agType)
    {
        $dataReturn = array();
        $dataReturn['nombre'] = ($agType == 'object') ? $agDataNode->text : $agDataNode['text'];
        $dataReturn['naturaleza'] = ($agType == 'object') ? $agDataNode->naturaleza : $agDataNode['naturaleza'];
        return $dataReturn;
    }

    /**
     * Obtiene el formato para el reporte de cuentas.
     * @param Array $agDataCuentas 
     * @return Array Datos de los reportes
     */
    public function loadDataReport($agDataCuentas)
    {
        $agIdReporte = '10008';
        $datosReporteHtmlPdf = array();
        $datosReporte = $agDataCuentas;
        $datoCuerpo = $agDataCuentas;
        $datoGeneral = array();
        $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
        $datoGeneral['reporte'] = $agIdReporte;
        $estructura = $this->integrator->metadatos->DameEstructura($this->global->Estructura->idestructura);
        $datoGeneral['entidad'] = $estructura[0]->denominacion;
        $datosEmpresa = $this->integrator->configuracion->getDatosEmpresa($this->global->Estructura->idestructura);
        $datoGeneral['logo'] = $datosEmpresa['logo'];

        if (count($datosReporte) > 0) {
            $datosReporteHtmlPdf[] = array('datoGeneral' => $datoGeneral, 'datoCuerpo' => $datoCuerpo, 'datoPie' => $datoPie);
        }
        return (count($datosReporte) > 0) ? array('idreporte' => $agIdReporte, 'datos' => $datosReporte, 'dataSources' => $datosReporteHtmlPdf)
                    : array('idreporte' => 0, 'datos' => array());
    }

}
