<?php

/**
 * Clase modelo para cargar las Cuentas Agrupadas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CuentasAgrupadasModel extends ZendExt_Model
{

    public function CuentasAgrupadasModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene las Cuentas contables agrupadas (Grupo/Contenido/Cta).
     * @param Array $argparams (node, type, filter)
     * @return Array Cuentas contables agrupadas (Grupo/Contenido/Cta)
     */
    public function loadDataCuentasAgrupadas($argparams)
    {
        $idEstructura = $this->global->Estructura->idestructura;
        $NomGrupo = new NomGrupo();
        $NomContenido = new NomContenido();
        $NomCuenta = new NomCuenta();
        $cierreContableModel = new CierreContableModel();
        $concepto = $cierreContableModel->getConcepto($argparams['action']);
        if ($concepto == '3' || $concepto == '4' || $concepto == '5') {
            $NomCuenta = new NomCuenta();
            return $this->formatCtasCierre($NomCuenta->loadDataCuentas((is_numeric($argparams['node']) ? $argparams['node']
                                                : null)));
        } else {
            if ($argparams['filter']) {
                return $this->findNodesContained($argparams['filter'], $idEstructura);
            } else {
                if ($argparams['type'] === '3') {
                    return $NomCuenta->loadCuentasPorContenidoEconomico($idEstructura, null, $argparams['node']);
                } else if ($argparams['type'] === '1') {
                    return $NomContenido->loadContenidosAsociadosGrupoCuenta($argparams['node']);
                } else if ($argparams['type'] === '2') {
                    return $NomCuenta->loadCuentasPorContenidoEconomico($idEstructura, $argparams['node']);
                } else {
                    return $this->getTree($NomGrupo->loadGruposAsociadosContenidosCuentas($argparams['filter']));
                }
            }
        }
    }

    public function findNodesContained($filter, $idEstructura)
    {
        $NomGrupo = new NomGrupo();
        $NomContenido = new NomContenido();
        $NomCuenta = new NomCuenta();
        $treeBuilded = array();

        $this->getTree($this->coloringFilter($NomGrupo->loadGruposAsociadosContenidosCuentas($filter), $filter), 4,
                $treeBuilded);
        $this->treeObtainedByConcept($this->coloringFilter($NomContenido->loadContenidosAsociadosGrupoCuenta(null,
                                $filter), $filter), $treeBuilded, 2);

        $resultCuentasContables = $NomCuenta->buscarCuentasContables($idEstructura, $filter, false, 1);

        if (isset($resultCuentasContables[0])) {
            $this->goTreeColoringFilter($resultCuentasContables, $filter);
            $this->treeObtainedByConcept($resultCuentasContables, $treeBuilded, 3);
        }return $treeBuilded;
    }

    public function coloringFilter($agResult, $filter,
            $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í",
        "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç"))
    {
        foreach ($agResult as & $objConcept)
                $objConcept['text'] = str_replace(strtr(strtoupper($filter), $utf8),
                    "<span style=\"background-color: rgb(255, 255, 0);\">" . strtr(strtoupper($filter), $utf8) . "</span>",
                    strtr(strtoupper($objConcept['text']), $utf8));
        return $agResult;
    }

    public function goTreeColoringFilter(& $agResult, $filter,
            $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í",
        "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç"))
    {
        foreach ($agResult as & $objConcept) {
            $objConcept['text'] = str_replace(strtr(strtoupper($filter), $utf8),
                    "<span style=\"background-color: rgb(255, 255, 0);\">" . strtr(strtoupper($filter), $utf8) . "</span>",
                    strtr(strtoupper($objConcept['text']), $utf8));
            if (count($objConcept['children']) > 0) {
                $this->goTreeColoringFilter($objConcept['children'], $filter, $utf8);
            }
        }
    }

    public function treeObtainedByConcept($agResult, & $agTreeBuilded, $agType)
    {
        $NomGrupo = new NomGrupo();
        $NomContenido = new NomContenido();
        foreach ($agResult as $objConcept) {
            $founded = false;
            $this->getParentNodeInTree($objConcept, $agTreeBuilded, $founded, 4);
            if (!$founded) {
                $arrayConcept = ($agType == 2) ? $NomGrupo->obtenerGrupoPadre($objConcept['idpadre']) : $NomContenido->loadContenidoPorId($objConcept['idpadre']);
                $arrayConcept[0]['children'] = array();
                $arrayConcept[0]['children'][] = $objConcept;
                $arrayConcept[0]['expanded'] = true;
                $this->getTree($arrayConcept, 4, $agTreeBuilded);
            }
        }
    }

    public function getTree($agResult, $agNodeType = 0, & $treeBuilded = array())
    {
        $count = count($agResult);
        if ($count > 0)
                for ($i = 0; $i < $count; $i++)
                    if (!$agResult[$i]['visitado']) {
                    $agResult[$i]['visitado'] = true;
                    $agResult[$i]['leaf'] = false;
                    $treeBuilded = $this->buildingTree($agNodeType, $agResult, $agResult[$i], $treeBuilded);
                }
        return $treeBuilded;
    }

    public function buildingTree($agNodeType, &$agResults, $agNodeIterator, $agTreeBuilded)
    {
        if ($agNodeIterator['id'] != $agNodeIterator['idpadre']) {
            $founded = false;
            $this->getParentNodeInTree($agNodeIterator, $agTreeBuilded, $founded, $agNodeType);
            if (!$founded) {
                $parentNode = $this->getParentNodeInResult($agNodeIterator, $agResults);
                if (is_null($parentNode)) $parentNode = $this->getParentNodeQuery($agNodeType, $agNodeIterator);
                $parentNode['children'] = array();

                $parentNode['expanded'] = ($agNodeType == 4) ? true : false;

                $parentNode['leaf'] = false;

                $parentNode['children'][] = $agNodeIterator;

                $agTreeBuilded = $this->buildingTree($agNodeType, $agResults, $parentNode, $agTreeBuilded);
            }
        } else $agTreeBuilded[] = $agNodeIterator;
        return $agTreeBuilded;
    }

    public function getParentNodeInTree($agNodeIterator, & $agTreeBuilded, & $founded, $agNodeType)
    {
        for ($j = 0; $j < count($agTreeBuilded); $j++)
                if ($agTreeBuilded[$j]['id'] == $agNodeIterator['idpadre']) {
                $agTreeBuilded[$j]['expanded'] = ($agNodeType == 4) ? true : false;
                $agTreeBuilded[$j]['leaf'] = false;
                $agTreeBuilded[$j]['children'][] = $agNodeIterator;
                $founded = true;
                break;
            } elseif (count($agTreeBuilded[$j]['children']) > 0)
                    $this->getParentNodeInTree($agNodeIterator, $agTreeBuilded[$j]['children'], $founded, $agNodeType);
    }

    public function getParentNodeInResult($agNodeIterator, & $agResults)
    {
        for ($k = 0; $k < count($agResults); $k++)
                if (!$agResults[$k]['visitado'])
                    if ($agNodeIterator['idpadre'] == $agResults[$k]['id']) {
                    $agResults[$k]['visitado'] = true;
                    return $agResults[$k];
                }
        return NULL;
    }

    public function getParentNodeQuery($agNodeType, $agNodeIterator)
    {
        $NomGrupo = new NomGrupo();
        switch ($agNodeType) {
            default : {
                    $result = $NomGrupo->obtenerGrupoPadre($agNodeIterator['idpadre']);
                    return $result[0];
                }
        }
    }

    /**
     * Da formato a las Cuentas para su uso en el cierre.
     * @param Array $arrctas 
     * @return Array Cuentas para su uso en el cierre
     */
    public function formatCtasCierre($arrctas)
    {
        foreach ($arrctas as &$v) {
            $v['id'] = $v['idcuenta'];
            $v['type'] = 3;
        }
        return $arrctas;
    }

}
