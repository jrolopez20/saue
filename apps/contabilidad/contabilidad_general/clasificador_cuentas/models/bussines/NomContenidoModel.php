<?php

/**
 * Clase modelo para configurar los Contenidos economicos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomContenidoModel extends ZendExt_Model
{

    public function NomContenidoModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los Contenidos economicos
     * @param Array $argparams (node)
     * @return Array Contenidos economicos
     */
    public function loadContenidos($argparams)
    {
        $NomContenido = new NomContenido();
        $filter = $this->defineFilters($argparams['filtros']);
        $datos = $NomContenido->loadContenidos($argparams['start'], $argparams['limit'], $filter);
        $cant = $NomContenido->getTotalContenidos($filter);
        return array('datos' => $datos, 'cant' => $cant);
    }

    /**
     * Obtiene los Grupos contables por nivel
     * @param Array $argparams (node)
     * @return Array Grupos contables
     */
    public function loadGruposContables($argparams)
    {
        $NomGrupo = new NomGrupo();
        return $NomGrupo->loadGruposContables((is_numeric($argparams['node']) ? $argparams['node'] : null));
    }

    /**
     * Obtiene los datos para adicionar Contenidos economicos.
     * @param none
     * @return Array datos para adicionar Contenidos economicos
     */
    public function loadDataAddContenido()
    {
        $NomContenido = new NomContenido();
        $longitud = $NomContenido->obtenerLongitudNivel($this->global->Estructura->idestructura);
        if ($longitud == -1) {
            $DatFormato = new DatFormato();
            $formatos = $DatFormato->loadFormatosSubsistema($this->global->Estructura->idestructura, 4);
            if (count($formatos)) {
                $this->asociarNivelFormato($formatos, 1);
            }
            return array('formato' => $formatos);
        } else {
            $NomCuenta = new NomCuenta();
            $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura,2);
            $NomCuentaModel = new NomCuentaModel();
            return array('nivel' => $NomCuentaModel->getNivelFormato($formato, 1));
        }
    }


    /**
     * Asocia el Nivel a formato
     * @param stdClass $objformato 
     * @param Integer $nivel nivel a buscar
     * @return stdClass Formato con sus niveles
     */
    public function asociarNivelFormato(&$objformato, $nivel)
    {
        $DatParteformato = new DatParteformato();
        foreach ($objformato as &$v) {
            $v['nivel'] = $DatParteformato->buscarNivelFormato(array('idformato' => $v['idformato'], 'nivel' => $nivel, 'idparteformato' => ''));
            if (count($v['nivel'])) {
                $v['nivel'][0]['separador'] = $v['separador'];
            }
        }
    }

    /**
     * Adiciona o modifica el Contenido economico.
     * @param Array $argparams (idcontenido, codigoinicio, codigofin, denominacion, idgrupo, idnaturaleza, idformato, idparteformato)
     * @return boolean true if ocurred, false if failure
     */
    public function updateContenido($argparams)
    {
        if ($argparams['idcontenido']) {
            return $this->Actualizar($argparams);
        } else {
            $NomContenido = new NomContenido();
            $formato = $NomContenido->getFormatoAsociado($this->global->Estructura->idestructura);
            if (!count($formato)) {
                $NomContenido->insertFromatoAsociado($this->global->Estructura->idestructura, $argparams['idformato']);
            }
            return $this->Insertar($argparams);
        }
    }

    /**
     * Adiciona el Contenido economico.
     *  @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($argparams)
    {
        $objContenido = new NomContenido();
        $objContenido->codigoinicio = $argparams['codigoinicio'];
        $objContenido->codigofin = $argparams['codigofin'];
        $objContenido->denominacion = $argparams['denominacion'];
        $objContenido->idgrupo = $argparams['idgrupo'];
        $objContenido->idnaturaleza = $argparams['idnaturaleza'];
        $objContenido->idparteformato = $argparams['idparteformato'];
        try {
            $objContenido->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Modifica el Contenido economico.
     * @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($argparams)
    {
        $objContenido = Doctrine::getTable('NomContenido')->find($argparams['idcontenido']);
        $objContenido->codigoinicio = $argparams['codigoinicio'];
        $objContenido->codigofin = $argparams['codigofin'];
        $objContenido->denominacion = $argparams['denominacion'];
        $objContenido->idgrupo = $argparams['idgrupo'];
        $objContenido->idnaturaleza = $argparams['idnaturaleza'];
        try {
            $objContenido->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el Contenido economico.
     * @param Integer $argparams (idcontenido)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteContenido($argparams)
    {
        if (!$this->verificarUsoContenido($argparams['idcontenido'])) {
            try {
                $objContenido = Doctrine::getTable('NomContenido')->find($argparams['idcontenido']);
                $objContenido->delete();
                $this->deleteAsociacionContenido();
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgContenidoUso}";
        }
    }

    /**
     * Verifica si el Contenido economico se encuentra en uso
     * @param Integer $idcontenido  
     * @return boolean true if ocurred, false if failure
     */
    public function verificarUsoContenido($idcontenido)
    {
        $NomContenido = new NomContenido();
        return $NomContenido->verificarUsoContenido($idcontenido);
    }

    /**
     * Elimina el formato asociado al nomenclador de Contenido economico si es el ultimo
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function deleteAsociacionContenido()
    {
        $NomContenido = new NomContenido();
        $cant = $NomContenido->getTotalContenidos();
        if ($cant != 0) {
            $NomContenido->deleteFromatoAsociado($this->global->Estructura->idestructura);
        }
    }

    /**
     * procesa los filtros y devuelve la sentencia SQL.
     * @param Array $filter
     * @return String cadena SQL
     */
    public function defineFilters($filter)
    {
        $filter = json_decode($filter);
        //$where.= "nc.codigoinicio ILIKE '%$filter%' OR n.codigofin ILIKE '%$filter%' OR nc.denominacion ILIKE '%$filter%'";
        $data_return = "";
        if (count($filter)) {
            foreach ($filter as $k => $v) {
                $data_return .= ($k == 0) ? "nc." . $v->property . " ILIKE '%" . $v->value . "%'" : " OR nc." . $v->property . " ILIKE '%" . $v->value . "%'";
            }
        }
        return $data_return;
    }

}
