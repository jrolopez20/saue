<?php

/**
 * Clase modelo para configurar los Grupos Contables.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomGrupoModel extends ZendExt_Model
{

    public function NomGrupoModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los Grupos contables por nivel
     * @param Array $argparams (node)
     * @return Array Grupos contables
     */
    public function loadGruposContables($argparams)
    {
        $NomGrupo = new NomGrupo();
        return $NomGrupo->loadGruposContables((is_numeric($argparams['node']) ? $argparams['node'] : null));
    }

    /**
     * Obtiene los datos para adicionar Grupos contables por nivel
     * @param Array $argparams (nivel)
     * @return Array Grupos contables
     */
    public function loadDataAddGrupo($argparams)
    {
        $NomGrupo = new NomGrupo();
        $formato = $NomGrupo->getFormatoAsociado($this->global->Estructura->idestructura);
        if (!count($formato)) {
            $DatFormato = new DatFormato();
            $formato = $DatFormato->loadFormatosSubsistema($this->global->Estructura->idestructura, 4);
            if (count($formato)) {
                $this->asociarNivelFormato($formato, $argparams['nivel']);
            }
            return array('formato' => $formato);
        } else {
            return array('nivel' => $this->getNivelFormato($formato, $argparams['nivel']));
        }
    }

    /**
     * Asocia el Nivel a formato
     * @param stdClass $objformato 
     * @param Integer $nivel nivel a buscar
     * @return stdClass Formato con sus niveles
     */
    public function asociarNivelFormato(&$objformato, $nivel)
    {
        $DatParteformato = new DatParteformato();
        foreach ($objformato as &$v) {
            $v['nivel'] = $DatParteformato->buscarNivelFormato(array('idformato' => $v['idformato'], 'nivel' => $nivel, 'idparteformato' => ''));
            if (count($v['nivel'])) {
                $v['nivel'][0]['separador'] = $v['separador'];
            }
        }
    }

    /**
     * Obtiene los datos del Nivel de formato dado el idformato y nivel
     * @param Array $formato 
     * @param Integer $nivel nivel a buscar
     * @return stdClass Nivel de formato
     */
    public function getNivelFormato($formato, $nivel)
    {
        $DatParteformato = new DatParteformato();
        $nivel = $DatParteformato->buscarNivelFormato(array('idformato' => $formato[0]['idformato'], 'nivel' => $nivel, 'idparteformato' => ''));
        if (count($nivel)) {
            $nivel['0']['separador'] = $formato[0]['separador'];
        }
        return $nivel;
    }

    /**
     * Adiciona o modifica el Grupo Contable
     * @param Array $argparams (idgrupo,codigo,denominacion,descripcion,concatgrupo,nivel,idgrupopadre,idformato,idparteformato,idarbol)
     * @return boolean true if ocurred, false if failure
     */
    public function updateGrupoContable($argparams)
    {
        if ($argparams['idgrupo']) {
            return $this->Actualizar($argparams);
        } else {
            $NomGrupo = new NomGrupo();
            $formato = $NomGrupo->getFormatoAsociado($this->global->Estructura->idestructura);
            if (!count($formato)) {
                $NomGrupo->insertFromatoAsociado($this->global->Estructura->idestructura, $argparams['idformato']);
            }
            return $this->Insertar($argparams);
        }
    }

    /**
     * Elimina el Grupo Contable
     * @param Integer $argparams (node)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteGrupoContable($argparams)
    {
        if (!$this->verificarUsoGrupo($argparams['idgrupo'])) {
            try {
                $objGrupo = Doctrine::getTable('NomGrupo')->find($argparams['idgrupo']);
                $objGrupo->delete();
                $this->deleteAsociacionGrupoContable();
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
            } catch (Exception $exc) {
                if ($exc->getCode() == 23503) {//Code for foreignKeyViolation
                    return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgGrupoUso}";
                } else {
                    echo $exc->getTraceAsString();
                }
            }
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgGrupoUso}";
        }
    }

    /**
     * Adiciona el Grupo Contable
     *  @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($argparams)
    {
        $objGrupo = new NomGrupo();
        $objGrupo->codigo = $argparams['codigo'];
        $objGrupo->denominacion = $argparams['denominacion'];
        $objGrupo->descripcion = $argparams['descripcion'];
        $objGrupo->concatgrupo = $argparams['concatgrupo'];
        $objGrupo->nivel = $argparams['nivel'];
        $objGrupo->idgrupopadre = ($argparams['idgrupopadre']) ? $argparams['idgrupopadre'] : null;
        $objGrupo->idparteformato = $argparams['idparteformato'];
        $objGrupo->idarbol = $argparams['idarbol'];
        $objGrupo->idnaturaleza = $argparams['idnaturaleza'] ? $argparams['idnaturaleza'] : NULL;
        try {
            $objGrupo->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Modifica el Grupo Contable
     * @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($argparams)
    {
        $objGrupo = Doctrine::getTable('NomGrupo')->find($argparams['idgrupo']);
        $objGrupo->codigo = $argparams['codigo'];
        $objGrupo->denominacion = $argparams['denominacion'];
        $objGrupo->descripcion = $argparams['descripcion'];
        $objGrupo->concatgrupo = $argparams['concatgrupo'];
        $objGrupo->idnaturaleza = $argparams['idnaturaleza'] ? $argparams['idnaturaleza'] : NULL;
        try {
            $objGrupo->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Verifica si el Grupo Contable se encuentra en uso
     * @param Integer $idgrupo  
     * @return boolean true if ocurred, false if failure
     */
    public function verificarUsoGrupo($idgrupo)
    {
        $NomGrupo = new NomGrupo();
        return $NomGrupo->verificarUsoGrupo($idgrupo);
    }

    /**
     * Elimina el formato asociado al nomenclador de Grupo y Subgrupo contable si es el ultimo
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function deleteAsociacionGrupoContable()
    {
        $NomGrupo = new NomGrupo();
        $grupos = $NomGrupo->loadGruposContables(null);
        if (!count($grupos)) {
            $NomGrupo->deleteFromatoAsociado($this->global->Estructura->idestructura);
        }
    }

    /**
     * Devuelve la Naturaleza de las cuentas.
     * @param none
     * @return Array Naturaleza de las cuentas.
     */
//    public function loadNaturaleza(){
//        return NomGrupo::getNaturaleza();
//    }

}
