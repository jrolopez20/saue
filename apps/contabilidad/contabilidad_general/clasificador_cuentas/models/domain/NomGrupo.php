<?php

/**
 * Clase dominio para configurar los Grupos Contables.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomGrupo extends BaseNomGrupo
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('DatParteformato', array('local' => 'idparteformato', 'foreign' => 'idparteformato'));
        $this->hasOne('DatParteformato', array('local' => 'idparteformato', 'foreign' => 'idparteformato'));
        $this->hasOne('NomNaturaleza', array('local' => 'idnaturaleza', 'foreign' => 'idnaturaleza'));
    }

    /**
     * Lista los datos de los Grupos contables
     *
     * @param Integer $idgrupopadre identificador del nodo padre
     * @return Array Listado de los Grupos contables
     */
    public function loadGruposContables($idgrupopadre)
    {
        $query = Doctrine_Query::create();
        $where = ($idgrupopadre) ? "n.idgrupopadre = $idgrupopadre AND n.idgrupopadre <> n.idgrupo" : "n.idgrupopadre = n.idgrupo";

        return $query->select("idgrupo as id, CONCAT(CONCAT(concatgrupo ,' '), descripcion) text, CONCAT(CONCAT(concatgrupo ,' '),
        descripcion) as qtip, ( (n.ordender - n.ordenizq) = 1) as leaf, codigo, concatgrupo, descripcion, denominacion,
        denominacion as denomgrupo, idgrupo, idgrupopadre, idparteformato, ordender, ordenizq, nivel, pf.longitud sa longitud,
        true as expanded, f.separador as separador, n.idnaturaleza, na.descripcion as naturaleza")
            ->from('NomGrupo n')
            ->where($where)
            ->innerJoin('n.DatParteformato pf')
            ->innerJoin('pf.DatFormato f')
            ->leftJoin('n.NomNaturaleza na')
            ->orderby('n.codigo')
            ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
            ->execute();
    }

    /**
     * Obtiene el formato asociado al nomenclador de Grupo y Subgrupo contable
     *
     * @param Integer $idestructura
     * @param Integer $idnomenclador = 1 (nomenclador de grupo y subgrupo)
     * @return Array formato asociado al nomenclador de Grupo y Subgrupo contable
     */
    public function getFormatoAsociado($idestructura, $idnomenclador = 1)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT df.* FROM mod_maestro.dat_formato df "
            . "INNER JOIN mod_contabilidad.conf_formatonomenclador fn ON df.idformato = fn.idformato "
            . "WHERE fn.idestructura = $idestructura AND fn.idnomenclador = $idnomenclador "
            . "ORDER BY df.nombre ASC;");

        return $data_return;
    }

    /**
     * Agrega el formato asociado al nomenclador de Grupo y Subgrupo contable
     *
     * @param Integer $idestructura
     * @param Integer $idformato
     * @param Integer $idnomenclador = 1 (nomenclador de grupo y subgrupo)
     * @return Array formato asociado al nomenclador de Grupo y Subgrupo contable
     */
    public function insertFromatoAsociado($idestructura, $idformato, $idnomenclador = 1)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("INSERT INTO mod_contabilidad.conf_formatonomenclador (idestructura,idformato,idnomenclador) VALUES ($idestructura,$idformato,$idnomenclador);");

        return;
    }

    /**
     * Elimina el formato asociado al nomenclador de Grupo y Subgrupo contable
     *
     * @param Integer $idestructura
     * @param Integer $idnomenclador = 1 (nomenclador de grupo y subgrupo)
     * @return Array formato asociado al nomenclador de Grupo y Subgrupo contable
     */
    public function deleteFromatoAsociado($idestructura, $idnomenclador = 1)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("DELETE FROM mod_contabilidad.conf_formatonomenclador fn WHERE fn.idestructura = $idestructura AND fn.idnomenclador = $idnomenclador;");

        return;
    }

    /**
     * Verifica si el Grupo esta en uso
     *
     * @param Integer $idgrupo
     * @return Boolean
     */
    public function verificarUsoGrupo($idgrupo)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT nc.* FROM mod_contabilidad.nom_contenido nc WHERE nc.idgrupo = $idgrupo;");

        return count($data_return) ? TRUE : FALSE;
    }

    /**
     * Obtiene los Grupos contables asociados a Contenidos y Cuentas.
     *
     * @param Integer $filter
     * @return Array Grupos contables asociados a Contenidos y Cuentas
     */
    public function loadGruposAsociadosContenidosCuentas($filter)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $SELECT = "DISTINCT ng.idgrupo, ng.idgrupo as id, ng.idgrupopadre as idpadre, (ng.concatgrupo||' '||ng.denominacion) as text, true as leaf, ng.denominacion, ng.codigo as codigo, ng.concatgrupo as concat, ng.descripcion as descripcion, ng.nivel, '1' as type, ng.ordenizq, ng.ordender, false as visitado";
        $QUERY = "select $SELECT from mod_contabilidad.nom_grupo ng "
            . "INNER JOIN mod_contabilidad.nom_contenido ce ON ng.idgrupo = ce.idgrupo "
            . "INNER JOIN mod_contabilidad.nom_cuenta c ON ce.idcontenido = c.idcontenido "
            . "WHERE c.activa = 1";
        if (isset($filter) && $filter != 'null') {
            $QUERY .= " AND (lower(ng.denominacion) LIKE lower('%$filter%') OR lower(ng.concatgrupo) LIKE lower('%$filter%'))";
        }
        $QUERY .= ' ORDER BY ng.codigo';

        return $connection->fetchAll($QUERY);
    }

    /**
     * Obtiene el Grupo contable Padre.
     *
     * @param Integer $idpadre
     * @return Array datos del Grupo contables Padre
     */
    public function obtenerGrupoPadre($idpadre)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $QUERY = "SELECT false as visitado, ng.idgrupo as id, (ng.concatgrupo ||' '|| ng.denominacion) as text, ( (ng.ordender - ng.ordenizq) = 1) as leaf, ng.denominacion, ng.codigo, ng.concatgrupo, ng.descripcion, ng.idgrupo, ng.idgrupopadre, ng.ordender, ng.ordenizq, ng.nivel, ng.idgrupopadre as idpadre, '1' as type, ng.concatgrupo as concat "
            . "FROM mod_contabilidad.nom_grupo ng WHERE ng.idgrupo =$idpadre";

        return $connection->fetchAll($QUERY);
    }

    /**
     * Obtiene el Grupo contable.
     *
     * @param Integer $idgrupo
     * @return Array datos del Grupo contables Padre
     */
    public function obtenerGrupoByID($idgrupo)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT ng.* FROM mod_contabilidad.nom_grupo ng WHERE ng.idgrupo = $idgrupo;");
    }

    /**
     * Devuelve la Naturaleza de las cuentas.
     *
     * @param none
     * @return Array Naturaleza de las cuentas.
     */
    static public function getNaturaleza()
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT n.idnaturaleza, n.descripcion as naturaleza FROM mod_contabilidad.nom_naturaleza n");
    }

    /**
     * Lista los datos de los Grupos contables que son hojas
     *
     * @return Array Listado de los Grupos contables
     */
    static public function loadGruposHojas()
    {
        $query = Doctrine_Query::create();
        $where = "(n.ordender - n.ordenizq) = 1";

        return $query->select("idgrupo as id, CONCAT(CONCAT(concatgrupo ,' '), denominacion) as text, CONCAT(CONCAT(concatgrupo ,' '),
        descripcion) as qtip, ( (n.ordender - n.ordenizq) = 1) as leaf, codigo, concatgrupo, descripcion, denominacion,
        denominacion as denomgrupo, idgrupo, idgrupopadre, idparteformato, ordender, ordenizq, nivel, pf.longitud sa longitud,
        true as expanded, f.separador as separador, n.idnaturaleza, na.descripcion as naturaleza")
            ->from('NomGrupo n')
            ->where($where)
            ->innerJoin('n.DatParteformato pf')
            ->innerJoin('pf.DatFormato f')
            ->leftJoin('n.NomNaturaleza na')
            ->orderby('n.concatgrupo')
            ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
            ->execute();
    }

}
