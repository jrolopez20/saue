<?php

/**
 * Clase dominio para configurar los Contenidos economicos.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomContenido extends BaseNomContenido
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomGrupo', array('local' => 'idgrupo', 'foreign' => 'idgrupo'));
    }

    /**
     * Lista los datos de los Contenidos economicos.
     * @param Integer $start
     * @param Integer $limit
     * @param String $filter
     * @return Array Listado de los Contenidos economicos.
     */
    public function loadContenidos($start, $limit, $filter)
    {
        $select = "nc.*, nn.descripcion as naturaleza, (ng.concatgrupo || ' ' || ng.denominacion) as grupo, ng.denominacion as denomgrupo, pf.longitud as longitud";
        $SQL = "SELECT $select FROM mod_contabilidad.nom_contenido nc "
                . "INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = nc.idnaturaleza "
                . "INNER JOIN mod_contabilidad.nom_grupo ng ON ng.idgrupo = nc.idgrupo "
                . "INNER JOIN mod_maestro.dat_parteformato pf ON pf.idparteformato = nc.idparteformato ";

        $SQL .= ($filter) ? " WHERE $filter" : "";
        $SQL .= " ORDER BY nc.codigoinicio ASC LIMIT {$limit} OFFSET {$start};";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll($SQL);
    }

    /**
     * Devuelve la cantidad total de Contenidos economicos.
     * @param String $filter
     * @return Integer cantidad de Contenidos economicos.
     */
    public function getTotalContenidos($filter)
    {
        $SQL = "SELECT nc.* FROM mod_contabilidad.nom_contenido nc ";
        $SQL .= ($filter) ? " WHERE $filter;" : ";";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll($SQL);
        return count($data_return);
    }

    /**
     * Lista los datos de todos los Contenidos economicos.
     * @param none
     * @return Array Listado de los Contenidos economicos.
     */
    public function loadContenidosAll()
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $select = "nc.*, nn.descripcion as naturaleza, (ng.concatgrupo || ' ' || ng.denominacion) as grupo, ng.denominacion as denomgrupo, pf.longitud as longitud";
        $SQL = "SELECT $select FROM mod_contabilidad.nom_contenido nc "
                . "INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = nc.idnaturaleza "
                . "INNER JOIN mod_contabilidad.nom_grupo ng ON ng.idgrupo = nc.idgrupo "
                . "INNER JOIN mod_maestro.dat_parteformato pf ON pf.idparteformato = nc.idparteformato "
                . "ORDER BY nc.codigoinicio ASC ;";
        return $connection->fetchAll($SQL);
    }

    /**
     * Devuelve la Naturaleza de las cuentas.
     * @param none
     * @return Array Naturaleza de las cuentas.
     */
    public function loadNaturaleza()
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT n.idnaturaleza, n.descripcion as naturaleza FROM mod_contabilidad.nom_naturaleza n");
    }

    /**
     * Obtiene la longitud del Nivel de Formato configurado para Contenidos economicos.
     * @param $idestructura
     * @return Integer longitud del Nivel de Formato
     */
    public function obtenerLongitudNivel($idestructura)
    {
        $formato = $this->getFormatoAsociado($idestructura);
        if (!count($formato)) {
            return -1; //no existen formatos
        }
        $nivel = $this->getNivelFormato($formato, 1);
        if (count($nivel)) {
            return $nivel[0]['longitud'];
        } else {
            return 0; //no hay niveles configurados
        }
    }

    /**
     * Obtiene el formato asociado al nomenclador de Contenidos economicos.
     * @param Integer $idestructura
     * @param Integer $idnomenclador = 2 (nomenclador de contenidos economicos)
     * @return Array formato asociado al nomenclador de Contenidos economicos
     */
    public function getFormatoAsociado($idestructura, $idnomenclador = 2)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT df.* FROM mod_maestro.dat_formato df "
                . "INNER JOIN mod_contabilidad.conf_formatonomenclador fn ON df.idformato = fn.idformato "
                . "WHERE fn.idestructura = $idestructura AND fn.idnomenclador = $idnomenclador "
                . "ORDER BY df.nombre ASC;");
        return $data_return;
    }

    /**
     * Obtiene los datos del Nivel de formato dado el idformato y nivel
     * @param Array $formato 
     * @param Integer $nivel nivel a buscar
     * @return stdClass Nivel de formato
     */
    public function getNivelFormato($formato, $nivel)
    {
        $DatParteformato = new DatParteformato();
        $nivel = $DatParteformato->buscarNivelFormato(array('idformato' => $formato[0]['idformato'], 'nivel' => $nivel, 'idparteformato' => ''));
        if (count($nivel)) {
            $nivel['0']['separador'] = $formato[0]['separador'];
        }
        return $nivel;
    }

    /**
     * Agrega el formato asociado al nomenclador de Contenidos economicos.
     * @param Integer $idestructura
     * @param Integer $idformato
     * @param Integer $idnomenclador = 2 (nomenclador de contenidos economicos)
     * @return Array formato asociado al nomenclador de Contenidos economicos.
     */
    public function insertFromatoAsociado($idestructura, $idformato, $idnomenclador = 2)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("INSERT INTO mod_contabilidad.conf_formatonomenclador (idestructura,idformato,idnomenclador) VALUES ($idestructura,$idformato,$idnomenclador);");
        return;
    }

    /**
     * Verifica si el Contenidos esta en uso
     * @param Integer $idcontenido
     * @return Boolean
     */
    public function verificarUsoContenido($idcontenido)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT nc.* FROM mod_contabilidad.nom_cuenta nc WHERE nc.idcontenido = $idcontenido;");
        return count($data_return) ? true : false;
    }

    /**
     * Elimina el formato asociado al nomenclador de Contenido economico.
     * @param Integer $idestructura
     * @param Integer $idnomenclador = 2 (nomenclador de contenidos economicos)
     * @return Array formato asociado al nomenclador de Contenido economico
     */
    public function deleteFromatoAsociado($idestructura, $idnomenclador = 2)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("DELETE FROM mod_contabilidad.conf_formatonomenclador fn WHERE fn.idestructura = $idestructura AND fn.idnomenclador = $idnomenclador;");
        return;
    }

    /**
     * Obtiene los Contenidos asociados a Grupos contables y Cuentas.
     * @param Integer $idgrupo
     * @param Integer $filter
     * @return Array Contenidos asociados a Grupos contables y Cuentas
     */
    public function loadContenidosAsociadosGrupoCuenta($idgrupo, $filter)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $SELECT = "DISTINCT n.idcontenido, n.idcontenido as id, n.idgrupo as idpadre, n.denominacion, n.codigoinicio, n.codigofin, (n.codigoinicio||'-'||n.codigofin||' '||n.denominacion) as text, (n.codigoinicio||'-'||n.codigofin) as codigo, (n.codigoinicio||'-'||n.codigofin) as concat, '2' as type, false as leaf";

        if (isset($idgrupo) && $idgrupo != 'null')
            $WHERE .= " n.idgrupo = $idgrupo";
        if (isset($filter) && $filter != 'null')
            $WHERE .= "AND (n.codigoinicio LIKE '%$filter%' OR n.codigofin LIKE '%$filter%' OR lower(n.denominacion) LIKE lower('%$filter%')) ";

        return $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_contenido n "
                        . "INNER JOIN mod_contabilidad.nom_cuenta cta ON n.idcontenido = cta.idcontenido "
                        . "WHERE $WHERE ORDER BY n.codigoinicio");
    }

    /**
     * Obtiene los datos del Contenido dado el ID.
     * @param Integer $idcontenido
     * @return Array datos del Contenido
     */
    public function loadContenidoPorId($idcontenido)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $SELECT = "n.idcontenido, n.denominacion descripcontenidoe, n.codigoinicio, n.codigofin, (n.codigoinicio || '-' || n.codigofin || '-' || n.denominacion) as text, n.idcontenido as id, n.idgrupo as idpadre, n.denominacion, (n.codigoinicio || '-' || n.codigofin) as codigo, '2' as type";
        $WHERE .= " n.idcontenido = $idcontenido";

        return $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_contenido n "
                        . "WHERE $WHERE ORDER BY n.codigoinicio");
    }

}
