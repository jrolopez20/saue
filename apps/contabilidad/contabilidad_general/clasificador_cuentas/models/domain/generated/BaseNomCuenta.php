<?php

abstract class BaseNomCuenta extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.nom_cuenta');
        $this->hasColumn('idcuenta', 'numeric', null,
                array('notnull' => false, 'primary' => true, 'sequence' => 'mod_contabilidad.sec_nomcuenta'));
        $this->hasColumn('codigo', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('concatcta', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('nivel', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('privado', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ordenizq', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ordender', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idarbol', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcuentapadre', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idnaturaleza', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idparteformato', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('activa', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idgrupo', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
