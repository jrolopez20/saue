<?php

abstract class BaseNomNaturaleza extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.nom_naturaleza');
        $this->hasColumn('idnaturaleza', 'numeric', null, array('notnull' => true, 'primary' => true));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => true, 'primary' => false));
    }

}
