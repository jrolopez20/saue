<?php

abstract class BaseNomContenido extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.nom_contenido');
        $this->hasColumn('idcontenido', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_contabilidad.sec_nomcontenidoe'));
        $this->hasColumn('idgrupo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idnaturaleza', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idparteformato', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('codigoinicio', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('codigofin', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}
