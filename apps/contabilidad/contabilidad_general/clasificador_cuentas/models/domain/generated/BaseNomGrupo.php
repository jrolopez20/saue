<?php

abstract class BaseNomGrupo extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.nom_grupo');
        $this->hasColumn('idgrupo', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_contabilidad.sec_nomgrupo'));
        $this->hasColumn('codigo', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('concatgrupo', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('nivel', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ordenizq', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ordender', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idarbol', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idgrupopadre', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idparteformato', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idnaturaleza', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
