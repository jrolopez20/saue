<?php

/**
 * Clase dominio para configurar el Clasificador de Cuentas Contables.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomCuenta extends BaseNomCuenta
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('DatParteformato', array('local' => 'idparteformato', 'foreign' => 'idparteformato'));
        $this->hasOne('NomNaturaleza', array('local' => 'idnaturaleza', 'foreign' => 'idnaturaleza'));
    }

    /**
     * Lista las Cuentas contables por nivel.
     *
     * @param Integer $idcuentapadre identificador del nodo padre
     * @param boolean $checked
     * @param Integer $activa (0- inactiva, 1- activa, 2- todas)
     * @param Integer $saldo
     * @return Array Listado de las Cuentas contables
     */
    public function loadDataCuentas($idestructura, $idcuentapadre, $checked = FALSE, $activa = 2, $saldo = FALSE, $idnaturaleza = array(8030, 8031, 8032))
    {
        if ($activa == '')
            $activa = 2;
        if ($checked == '')
            $checked = FALSE;
        if (empty($idestructura)) {
            $global = ZendExt_GlobalConcept::getInstance();
            $idestructura = $global->Estructura->idestructura;
        }
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $SELECT = "nc.idcuenta, nc.idcuenta as id, (nc.concatcta ||' '|| nc.denominacion) as text, (nc.concatcta ||' '|| nc.denominacion) as qtip,
        ( (nc.ordender - nc.ordenizq) = 1) as leaf, nc.codigo, nc.concatcta, nc.denominacion, nc.denominacion as denomcuenta,
        nc.idcuentapadre, nc.idparteformato, nc.nivel, nc.activa, nc.idestructura,
        nn.idnaturaleza, nn.descripcion as naturaleza, pf.longitud as longitud, df.separador as separador, nc.idgrupo, 1 as allowaction";
        if ($checked) {
            $SELECT .= ", (case WHEN nc.ordender - nc.ordenizq = 1 THEN false else true END) as checked";
        }
        /* Aki puse la condicion 'nc.idgrupo = $idcuentapadre' para el nuevo arbol de cuentas con los grupos */
//        $WHERE = ($idcuentapadre) ? "((nc.idcuentapadre = $idcuentapadre  AND nc.idcuentapadre <> nc.idcuenta) OR nc.idgrupo = $idcuentapadre" : "nc.idcuentapadre = nc.idcuenta";
        $WHERE = ($idcuentapadre) ? "((nc.idcuentapadre = $idcuentapadre  AND nc.idcuentapadre <> nc.idcuenta) OR (nc.idgrupo = $idcuentapadre AND nc.idcuentapadre = nc.idcuenta))" : "nc.idcuentapadre = nc.idcuenta";
        $WHERE .= ($activa == 1) ? " AND nc.activa = 1" : (($activa == 0) ? " AND nc.activa = 0" : " AND (nc.activa = 0 OR nc.activa = 1)");
        $WHERE .= " AND nc.idestructura = $idestructura";
        $WHERE .= ' AND nc.idnaturaleza IN(' . implode(',', $idnaturaleza) . ')';

        $data_return = $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta nc "
//                . "LEFT JOIN mod_contabilidad.nom_contenido cont ON cont.idcontenido = nc.idcontenido "
                . "INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = nc.idnaturaleza "
                . "INNER JOIN mod_maestro.dat_parteformato pf ON nc.idparteformato = pf.idparteformato "
                . "INNER JOIN mod_maestro.dat_formato df ON df.idformato = pf.idformato "
                . "WHERE $WHERE "
                . "ORDER BY nc.codigo ASC;");
        $this->deleteColumnChecked($data_return);
        if ($saldo) {
            $this->asociarSaldoCuenta($data_return);
        }

        return $data_return;
    }

    /**
     * Obtiene el formato asociado al nomenclador de Cuentas contables.
     *
     * @param Integer $idestructura
     * @param Integer $idnomenclador = 3 (nomenclador de cuentas contables)
     * @return Array formato asociado al nomenclador de Cuentas contables
     */
    public function getFormatoAsociado($idestructura, $idnomenclador = 3)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT df.* FROM mod_maestro.dat_formato df "
                . "INNER JOIN mod_contabilidad.conf_formatonomenclador fn ON df.idformato = fn.idformato "
                . "WHERE fn.idestructura = $idestructura AND fn.idnomenclador = $idnomenclador "
                . "ORDER BY df.nombre ASC;");

        return $data_return;
    }

    /**
     * Lista los Niveles de un Formato determinado.
     *
     * @param Integer $idformato
     * @return Array Niveles de un Formato determinado
     */
    public function listParteFormatos($idformato)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT pf.* FROM mod_maestro.dat_parteformato pf "
                        . "WHERE pf.idformato = $idformato ORDER BY pf.nivel ASC;");
    }

    /**
     * Agrega el formato asociado al nomenclador de Cuentas contables.
     *
     * @param Integer $idestructura
     * @param Integer $idformato
     * @param Integer $idnomenclador = 3 (nomenclador de cuentas contables)
     * @return Array formato asociado al nomenclador de Cuentas contables
     */
    public function insertFromatoAsociado($idestructura, $idformato, $idnomenclador = 3)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("INSERT INTO mod_contabilidad.conf_formatonomenclador (idestructura,idformato,idnomenclador) VALUES ($idestructura,$idformato,$idnomenclador);");

        return;
    }

    /**
     * Elimina el formato asociado al nomenclador de Cuentas contables.
     *
     * @param Integer $idestructura
     * @param Integer $idnomenclador = 3 (nomenclador de cuentas contables)
     * @return Array formato asociado al nomenclador de Cuentas contables
     */
    public function deleteFromatoAsociado($idestructura, $idnomenclador = 3)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $connection->execute("DELETE FROM mod_contabilidad.conf_formatonomenclador fn WHERE fn.idestructura = $idestructura AND fn.idnomenclador = $idnomenclador;");

        return;
    }

    /**
     * Raliza la busqueda en el nomenclador de Cuentas contables.
     *
     * @param Integer $idestructura
     * @param String $filter
     * @param boolean $checked
     * @param Integer $activa (0- inactiva, 1- activa, 2- todas)
     * @param Integer $saldo
     * @return Array resultado busqueda en el nomenclador de Cuentas contables
     */
    public function buscarCuentasContables($idestructura, $filter, $checked, $activa = 2, $saldo = FALSE)
    {
        if (empty($idestructura)) {
            $global = ZendExt_GlobalConcept::getInstance();
            $idestructura = $global->Estructura->idestructura;
        }
        $resBusqued = $this->obtenerCuentaPorCriterio($idestructura, $filter, $checked, $activa, $saldo);
        $arbolBusqueda = array();
        $count = count($resBusqued);
        if ($count <= 500) {
            for ($i = 0; $i < $count; $i++) {
                if (!$resBusqued[$i]['visitado']) {
                    $resBusqued[$i]['visitado'] = TRUE;
                    $arbolBusqueda = $this->construirArbol($idestructura, $resBusqued, $resBusqued[$i], $arbolBusqueda);
                }
            }
        }

        return $arbolBusqueda;
    }

    /**
     * Raliza la busqueda en el nomenclador de Cuentas contables.
     *
     * @param Integer $idestructura
     * @param String $filter
     * @param boolean $checked
     * @param Integer $activa (0- inactiva, 1- activa, 2- todas)
     * @param Integer $saldo
     * @return Array resultado busqueda en el nomenclador de Cuentas contables
     */
    public function obtenerCuentaPorCriterio($idestructura, $filter, $checked = FALSE, $activa = 2, $saldo = FALSE)
    {
        if ($activa == '')
            $activa = 2;
        if ($checked == '')
            $checked = FALSE;
        $WHERE = "(n.concatcta ||' '|| n.denominacion) ILIKE '%$filter%' AND n.idestructura = $idestructura ";
        $WHERE .= ($activa == 1) ? " AND n.activa = 1" : (($activa == 0) ? " AND n.activa = 0" : " AND (n.activa = 0 OR n.activa = 1)");
        $WHERECase = "cta.idcuentapadre = n.idcuenta AND cta.idestructura = $idestructura AND cta.idcuentapadre <> cta.idcuenta ";

        $SELECT = "false as visitado, n.idcuenta as id, n.idcuenta, n.idcuentapadre, n.codigo, n.ordenizq,
            n.ordender, n.denominacion, n.nivel, n.activa, n.concatcta, n.idnaturaleza, t.descripcion as naturaleza,
            (case WHEN n.ordender - n.ordenizq = 1 THEN true WHEN n.ordender - n.ordenizq <> 1 THEN 
            (case WHEN (SELECT count(cta.idcuenta) FROM mod_contabilidad.nom_cuenta cta WHERE $WHERECase) > 0 THEN false else true end) 
             else false END) as leaf, '3' as type, n.idgrupo";
        if ($checked) {
            $SELECT .= ", (case WHEN n.ordender - n.ordenizq = 1 THEN false else true END) as checked";
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta n
                                inner join mod_contabilidad.nom_naturaleza t ON n.idnaturaleza = t.idnaturaleza 
                                WHERE $WHERE
                                ORDER BY n.concatcta, n.codigo, n.denominacion offset 0 limit 501");
        $this->deleteColumnChecked($data_return);
        if ($saldo) {
            $this->asociarSaldoCuenta($data_return);
        }

        return $data_return;
    }

    /**
     * Reconstruye el camino hacia los nodos encontrados.
     *
     * @param Integer $idestructura identificador de la entidad
     * @param Array $results nodos encontrados
     * @param Integer $nodoBuscado nodo que se busca
     * @param Array $arbolBusqueda recorido encontrado
     * @return Array resultado busqueda en el nomenclador de Cuentas contables
     */
    public function construirArbol($idestructura, &$results, $nodoBuscado, $arbolBusqueda)
    {
        if ($nodoBuscado['idcuentapadre'] != $nodoBuscado['idcuenta']) {
            $encArbol = FALSE;
            $this->buscaPadreEnArbol($nodoBuscado, $arbolBusqueda, $encArbol);
            if (!$encArbol) {
                $nodoPadre = $this->buscaPadreEnArray($nodoBuscado, $results);
                if (is_null($nodoPadre)) {
                    $getpadre = $this->obtenerPadre($nodoBuscado['idcuentapadre'], $idestructura);
                    $nodoPadre = $getpadre[0];
                }
                $nodoPadre['children'] = array();
                $nodoPadre['expanded'] = TRUE;
                $nodoPadre['children'][] = $nodoBuscado;
                $arbolBusqueda = $this->construirArbol($idestructura, $results, $nodoPadre, $arbolBusqueda);
            }
        } else {
//            $nodoBuscado['idpadre'] = $nodoBuscado['idcontenido']; //esto es para mostrar el arbol desde grupos/contenidos/cuentas
            $arbolBusqueda[] = $nodoBuscado;
        }

        return $arbolBusqueda;
    }

    /**
     * Busca el nodo padre en los recorridos obtenidos.
     *
     * @param Integer $nodoBuscado nodo que se busca
     * @param Array $arbolesConst recorridos contruidos
     * @param Array $encArbol indica si lo encuentra
     * @return Array resultado busqueda en el nomenclador de Cuentas contables
     */
    public function buscaPadreEnArbol($nodoBuscado, &$arbolesConst, &$encArbol)
    {
        foreach ($arbolesConst as &$v) {
            if ($v['idcuenta'] == $nodoBuscado['idcuentapadre']) {
                $v['expanded'] = TRUE;
                $v['children'][] = $nodoBuscado;
                $encArbol = TRUE;
                break;
            } elseif (count($v['children']) > 0) {
                $this->buscaPadreEnArbol($nodoBuscado, $v['children'], $encArbol);
            }
        }
    }

    /**
     * Busca el nodo padre en los datos devueltos de la consulta.
     *
     * @param Integer $nodoBuscado nodo que se busca
     * @param Array $results nodos encontrados
     * @return Array resultado busqueda en el nomenclador de Cuentas contables
     */
    public function buscaPadreEnArray($nodoBuscado, &$results)
    {
        foreach ($results as &$v) {
            if (!$v['visitado']) {
                if ($nodoBuscado['idcuentapadre'] == $v['idcuenta']) {
                    $v['visitado'] = TRUE;

                    return $v;
                }
            }
        }

        return NULL;
    }

    /**
     * Obtiene los datos de nodo padre.
     *
     * @param Integer $idpadre
     * @param Integer $idestructura
     * @return Array resultado busqueda en el nomenclador de Cuentas contables
     */
    public function obtenerPadre($idpadre, $idestructura)
    {
        $WHERE = "nc.idcuenta = $idpadre AND nc.idestructura = $idestructura";
        $SELECT = "nc .idestructura, false AS visitado, nc.idcuenta as id, nc.idcuenta, (nc.concatcta ||' '|| nc.denominacion) AS text, "
                . "(nc.concatcta ||' '|| nc.denominacion) AS qtip, "
                . "( (nc.ordender - nc.ordenizq) = 1) AS leaf, nc.codigo, nc.concatcta, nc.denominacion, "
                . "nc.denominacion AS denomcuenta, nc.idcuentapadre, nc.idparteformato, nc.nivel, "
                . "nn.idnaturaleza, nn.descripcion AS naturaleza, pf.longitud AS longitud, "
                . "df.separador AS separador, true AS expANDed";
        $dm = Doctrine_Manager::getInstance();
        $conn = $dm->getCurrentConnection();

        return $conn->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta nc
                                    INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = cont.idnaturaleza 
                                    INNER JOIN mod_maestro.dat_parteformato pf ON nc.idparteformato = pf.idparteformato 
                                    INNER JOIN mod_maestro.dat_formato df ON df.idformato = pf.idformato  
                                    WHERE $WHERE
                                    ORDER BY nc.concatcta, nc.codigo, nc.denominacion");
    }

    public function deleteColumnChecked(&$arrCtas)
    {
        foreach ($arrCtas as &$v) {
            if ($v['checked']) {
                unset($v['checked']);
            }
        }
    }

    /**
     * Lista las Cuentas contables de primer nivel.
     *
     * @param Integer $level (0 - Ctas primer nivel, 1 Ctas hoja)
     * @return Array Listado de las Cuentas contables de primer nivel
     */
    public function loadDataCuentasNivel($level = 0, $filter = -1)
    {
        $SELECT = "nc.idcuenta, (nc.concatcta ||' '|| nc.denominacion) as text, (nc.concatcta ||' '|| nc.denominacion) as qtip, ( (nc.ordender - nc.ordenizq) = 1) as leaf, nc.codigo, nc.concatcta, nc.denominacion, nc.denominacion as denomcuenta, nc.idcuentapadre, nc.idparteformato, nc.nivel, nn.idnaturaleza, nn.descripcion as naturaleza, pf.longitud as longitud, df.separador as separador";

        $conditions = array();
        ($level) ? $conditions[] = "(nc.ordender - nc.ordenizq) = 1" : $conditions[] = "nc.idcuentapadre = nc.idcuenta";

        if ($filter != -1 && !empty($filter)) {
            $conditions[] = '(lower(nc.concatcta) ILIKE lower(\'%' . $filter . '%\') OR lower(nc.denominacion) ILIKE lower(\'%' . $filter . '%\')
            OR lower(nc.concatcta ||\' \'|| nc.denominacion) ILIKE lower(\'%' . $filter . '%\'))';
        }

        $WHERE = implode(' AND ', $conditions);

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta nc "
                . "INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = nc.idnaturaleza "
                . "INNER JOIN mod_maestro.dat_parteformato pf ON nc.idparteformato = pf.idparteformato "
                . "INNER JOIN mod_maestro.dat_formato df ON df.idformato = pf.idformato "
                . "WHERE $WHERE "
                . "ORDER BY nc.concatcta ASC;");

        return $data_return;
    }

    /**
     * Obtiene las Cuentas por Contenidos.
     *
     * @param Integer $idestructura
     * @param Integer $idpadre
     * @param Integer $idnodo
     * @return Array Cuentas por Contenidos
     */
    public function loadCuentasPorContenidoEconomico($idestructura, $idpadre, $idnodo)
    {
        $WHERE = ($idnodo) ? "n.idcuentapadre = $idnodo AND n.idcuentapadre <> n.idcuenta" : "n.idcuenta = n.idcuentapadre AND n.idcontenido = $idpadre";
        $WHERE .= " AND n.idestructura = $idestructura AND n.activa = 1";
        $SELECT = "( (n.ordender - n.ordenizq) = 1) as leaf, n.concatcta||' '||n.denominacion as text, n.denominacion, n.idcuenta as id, n.idcuentapadre as idpadre, n.concatcta as concat, n.codigo, '3' as type";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta n "
                        . "WHERE $WHERE ORDER BY n.concatcta, n.codigo, n.denominacion");
    }

    /**
     * Obtiene las Cuentas hijas por Contenidos.
     *
     * @param Integer $idestructura
     * @param Integer $arridcontenido
     * @return Array Cuentas hijas por Contenidos
     */
    public function loadCuentasHijasPorContenido($idestructura, $arridcontenido)
    {
        $arrIds = (count($arridcontenido) > 1) ? implode(',', json_decode($arridcontenido)) : '' . $arridcontenido[0];
        $WHERE = "((n.ordender - n.ordenizq) = 1) AND n.idcontenido IN ($arrIds)";
        $WHERE .= " AND n.idestructura = $idestructura";
        $SELECT = "( (n.ordender - n.ordenizq) = 1) as leaf, n.concatcta||' '||n.denominacion as text, n.denominacion, n.idcuenta as id, n.idcuentapadre as idpadre, n.concatcta as concat, n.codigo, '3' as type";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta n "
                        . "WHERE $WHERE ORDER BY n.concatcta, n.codigo, n.denominacion");
    }

    /**
     * Obtiene las Cuentas hijas por Grupos contables.
     *
     * @param Integer $idestructura
     * @param Integer $arridgrupo
     * @return Array Cuentas hijas por Grupos contables
     */
    public function loadCuentasHijasPorGrupo($idestructura, $arridgrupo)
    {
        $arrIds = (count($arridgrupo) > 1) ? implode(',', json_decode($arridgrupo)) : '' . $arridgrupo[0];
        $WHERE = "(n.ordender - n.ordenizq) = 1 AND n.idgrupo IN (SELECT nng.idgrupo FROM mod_contabilidad.nom_grupo ng, mod_contabilidad.nom_grupo nng
       WHERE ng.idgrupo IN ($arrIds) and ng.idarbol = nng.idarbol order by nng.ordenizq)";
        $WHERE .= " AND n.idestructura = $idestructura";
        $SELECT = "( (n.ordender - n.ordenizq) = 1) as leaf, n.concatcta||' '||n.denominacion as text, n.denominacion, n.idcuenta as id, n.idcuentapadre as idpadre, n.concatcta as concat, n.codigo, '3' as type";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta n WHERE $WHERE ORDER BY n.concatcta, n.codigo, n.denominacion");
    }

    /**
     * Obtiene las Cuentas por su id.
     *
     * @param Integer $idcuenta
     * @return Array Cuentas por id
     */
    public function loadCuentasPorId($idcuenta)
    {
        $WHERE .= " n.idcuenta = $idcuenta";
        $SELECT = "( (n.ordender - n.ordenizq) = 1) as leaf, n.concatcta||' '||n.denominacion as text, n.denominacion, n.idcuenta as id, n.idcuentapadre as idpadre, n.concatcta as concat, n.codigo, '3' as type";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta n "
                        . "WHERE $WHERE ORDER BY n.concatcta, n.codigo, n.denominacion");
    }

    /**
     * Obtiene el saldo de las Cuentas.
     *
     * @param Integer $arridctas
     * @param Integer $idestructura
     * @param Date $fecha
     * @return Array saldo de las Cuentas
     */
    public function saldoAcumuladoCtas($arridctas, $idestructura, $fecha)
    {
        $arrIds = (count($arridctas) > 1) ? implode(',', $arridctas) : '' . $arridctas[0];
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT * FROM mod_contabilidad.f_saldocuenta (array[" . $arrIds . "],'$idestructura', '$fecha') ");
    }

    /**
     * Asocia el saldo a las cuentas las Cuentas.
     *
     * @param Array $arrctas
     * @return Array saldo de las Cuentas
     */
    public function asociarSaldoCuenta(&$arrctas)
    {
        $global = ZendExt_GlobalConcept::getInstance();
        $idestructura = $global->Estructura->idestructura;
        $fecha = FALSE;
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        foreach ($arrctas as &$v) {
            if ($v['leaf']) {
                if (!$fecha) {
                    $f = $this->obtenerFecha($idestructura, 4);
                    $fecha = $f[0]['fecha'];
                }
                $saldo = $connection->fetchAll("SELECT * FROM mod_contabilidad.f_saldocuenta (array[" . $v['id'] . "],'$idestructura', '$fecha') ");
                $v['saldocuenta'] = $saldo[0]['saldocuenta'];
            }
        }
    }

    /**
     * Carga la Fecha actual dado el subsistema.
     *
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Fecha actual del subsistema
     */
    public function obtenerFecha($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT f.* FROM mod_maestro.dat_fecha f "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON es.idestructurasubsist = f.idestructurasubsist "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    static public function getTreesMachesCriteria($criteria)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        try {
            return $connection->fetchAll("SELECT MAX(idcuenta),idarbol FROM mod_contabilidad.nom_cuenta
                  WHERE $criteria GROUP BY idarbol ORDER BY idarbol");
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function getTreeByCuenta($idcuenta)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        try {
            return $connection->fetchAll("SELECT h.* FROM mod_contabilidad.nom_cuenta p, mod_contabilidad.nom_cuenta h
                WHERE p.idarbol = h.idarbol and p.idcuenta = $idcuenta
                and h.idcuenta <= $idcuenta ORDER BY h.idcuenta");
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    public function GetById($idcuenta)
    {
        $data_return = $this->GetByCondition('nc.idcuenta = ' . $idcuenta);

        return count($data_return) ? $data_return[0] : $data_return;
    }

    public function GetByCondition($condition)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $SELECT = "nc.idcuenta, nc.idcuenta as id, (nc.concatcta ||' '|| nc.denominacion) as text, (nc.concatcta ||' '|| nc.denominacion) as qtip,
        ( (nc.ordender - nc.ordenizq) = 1) as leaf, nc.codigo, nc.concatcta, nc.denominacion, nc.denominacion as denomcuenta,
        nc.idcuentapadre, nc.idparteformato, nc.nivel, nc.activa, nc.idestructura,nc.idarbol,
        nn.idnaturaleza, nn.descripcion as naturaleza, pf.longitud as longitud, df.separador as separador, nc.idgrupo, 1 as allowaction";

        try {
            $data_return = $connection->fetchAll("SELECT $SELECT FROM mod_contabilidad.nom_cuenta nc "
                    . "INNER JOIN mod_contabilidad.nom_naturaleza nn ON nn.idnaturaleza = nc.idnaturaleza "
                    . "INNER JOIN mod_maestro.dat_parteformato pf ON nc.idparteformato = pf.idparteformato "
                    . "INNER JOIN mod_maestro.dat_formato df ON df.idformato = pf.idformato "
                    . "WHERE $condition ORDER BY nc.idarbol, nc.nivel DESC");

            return $data_return;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

}
