<?php

/**
 * Clase dominio para configurar los Grupos Contables.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomNaturaleza extends BaseNomNaturaleza
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('NomGrupo', array('local' => 'idnaturaleza', 'foreign' => 'idnaturaleza'));
        $this->hasMany('NomCuenta', array('local' => 'idnaturaleza', 'foreign' => 'idnaturaleza'));
    }


}
