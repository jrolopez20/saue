<?php

class ComprobanteService
{

    /**
     * Guarda el comprobante de operaciones.
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function saveComprobante($argparams)
    {
        $comprobanteModel = new DatComprobanteModel();
        return $comprobanteModel->updateComprobante($argparams);
    }

    /**
     * Obtiene los datos del comprobante de operaciones.
     * @param Array $argparams
     * @return Array
     */
    public function getComprobantePorId($idcomprobante)
    {
        $datComprobanteModel = new DatComprobanteModel();
        return $datComprobanteModel->getDataComprobante($idcomprobante);
    }

}
