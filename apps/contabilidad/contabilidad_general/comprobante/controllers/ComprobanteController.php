<?php

/**
 * Clase controladora para gestionar los Comprobantes Contables.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ComprobanteController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatComprobanteModel();
    }

    public function comprobanteAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadComprobantesAction()
    {
        echo json_encode($this->model->listDataComprobante($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataComprobanteAction()
    {
        echo json_encode($this->model->loadDataComprobante($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataTipoAsientoAction()
    {
        echo json_encode($this->model->listDataTipoAsiento($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataModeloAction()
    {
        echo json_encode($this->model->listDataModeloPases($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataFechaAction()
    {
        echo json_encode($this->model->loadDataFecha());
    }

    /**
     * call this model action
     */
    public function loadCuentasAction()
    {
        echo json_encode($this->model->loadDataCuentas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function updateComprobanteAction()
    {
        echo $this->model->updateComprobante($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function asentarComprobanteAction()
    {
        echo $this->model->asentarComprobante($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function vistaPreviaComprobanteAction()
    {
        echo json_encode($this->model->vistaPreviaComprobante($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function graficarComprobantesPorEstadoAction()
    {
        echo json_encode($this->model->graficarComprobantesPorEstado());
    }

    /**
     * call this model action
     */
    public function nextDateAction()
    {
        echo $this->model->nextDate();
    }

}
