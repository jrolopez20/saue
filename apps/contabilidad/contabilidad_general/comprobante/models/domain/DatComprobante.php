<?php

/**
 * Clase dominio para configurar los Comprobantes.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatComprobante extends BaseDatComprobante
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('ConfTipodiarioestructura', array('local' => 'idtipodiario', 'foreign' => 'idtipodiario'));
        $this->hasMany('ConfTipodiariomodelo', array('local' => 'idtipodiario', 'foreign' => 'idtipodiario'));
        $this->hasMany('NomSubsistema', array('local' => 'idsubsistema', 'foreign' => 'idsubsistema'));
    }

    /**
     * Lista los datos de los Comprobantes de Operaciones.
     * @param Array $argparams (start, limit)
     * @param String $filter
     * @param Integer $idestructura
     * @return Array Listado de los Comprobantes de Operaciones
     */
    public function listarComprobantes($argparams, $filter, $idestructura)
    {
        $start = ($argparams['start']) ? $argparams['start'] : 0;
        $limit = ($argparams['limit']) ? $argparams['limit'] : 1000;

        $ejercicio = $this->obtenerEjercicio($idestructura, 4);
        $f_ini = $ejercicio[0]['inicio'];
        $f_fin = $ejercicio[0]['fin'];

        $sql = "SELECT dc.*, ec.descripcion as estado, su.nombreusuario as usuario, ns.denominacion as subsistema "
                . "FROM mod_contabilidad.dat_comprobante dc "
                . "INNER JOIN mod_contabilidad.nom_estadocomprobante ec ON ec.idestadocomprobante = dc.idestadocomprobante "
                . "INNER JOIN mod_seguridad.seg_usuario su ON su.idusuario = dc.idusuario "
                . "INNER JOIN mod_maestro.nom_subsistema ns ON ns.idsubsistema = dc.idsubsistema "
                . "WHERE dc.idestructura = $idestructura AND (dc.fechaemision BETWEEN '" . $f_ini . "' AND '" . $f_fin . "')";
        if ($filter != '') {
            $sql .=" AND $filter";
        }
        $sql .= " ORDER BY dc.numerocomprobante ASC LIMIT $limit OFFSET $start;";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll($sql);
    }

    /**
     * Carga los datos para modificar los Comprobantes de Operaciones.
     * @param Array $idcomprobante
     * @return Array datos del Comprobante de Operaciones
     */
    public function loadDataComprobante($idcomprobante)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT dc.idcomprobante, dp.*, da.codigodocumento, cta.* "
                        . "FROM mod_contabilidad.dat_comprobante dc "
                        . "INNER JOIN mod_contabilidad.dat_asiento da ON da.idcomprobante = dc.idcomprobante "
                        . "INNER JOIN mod_contabilidad.dat_pase dp ON dp.idasiento = da.idasiento "
                        . "INNER JOIN mod_contabilidad.nom_cuenta cta ON cta.idcuenta = dp.idcuenta "
                        . "WHERE dc.idcomprobante = $idcomprobante;");
    }

    /**
     * Obtiene el total de los Comprobantes de Operaciones.
     * @param Array $filter
     * @param Integer $idestructura
     * @return Integer total de los Comprobantes de Operaciones
     */
    public function getTotalComprobantes($filter, $idestructura)
    {
        $ejercicio = $this->obtenerEjercicio($idestructura, 4);
        $f_ini = $ejercicio[0]['inicio'];
        $f_fin = $ejercicio[0]['fin'];

        $sql = "SELECT dc.* FROM mod_contabilidad.dat_comprobante dc "
                . "INNER JOIN mod_contabilidad.nom_estadocomprobante ec ON ec.idestadocomprobante = dc.idestadocomprobante "
                . "WHERE dc.idestructura = $idestructura AND (dc.fechaemision BETWEEN '" . $f_ini . "' AND '" . $f_fin . "')";
        if ($filter != '') {
            $sql .=" AND $filter";
        }
        $sql .= ";";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return count($connection->fetchAll($sql));
    }

    /**
     * Lista los datos de los modelos y sus pases.
     * @param Integer $idtipodiario
     * @return Array Listado de los modelos y sus pases
     */
    public function listDataModeloPases($idtipodiario)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT DISTINCT mc.* "
                . "FROM mod_maestro.conf_tipodiariomodelo dm "
                . "INNER JOIN mod_maestro.nom_modelocontable mc ON mc.idmodelocontable = dm.idmodelocontable "
                . "INNER JOIN mod_maestro.conf_modelocontablecuenta mcta ON mcta.idmodelocontable = mc.idmodelocontable "
                . "WHERE dm.idtipodiario = $idtipodiario "
                . "ORDER BY mc.denominacion ASC;");
        if (count($data_return)) {
            $modContable = new NomModelocontableModel();
            foreach ($data_return as &$v) {
                $pases = $modContable->listDataPase(array('idmodelocontable' => $v['idmodelocontable']));
                $v['pases'] = $pases['datos'];
            }
        }
        return $data_return;
    }

    /**
     * Carga los datos del Ejercicio actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Periodo actual del subsistema
     */
    public function obtenerEjercicio($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT e.* FROM mod_maestro.dat_estructurasubsist es "
                        . "INNER JOIN mod_maestro.dat_cierre c ON c.idestructurasubsist = es.idestructurasubsist "
                        . "INNER JOIN mod_maestro.dat_ejerciciocontable e ON e.idejercicio = c.idejercicioactual "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Carga los datos del Periodo actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Periodo actual del subsistema
     */
    public function obtenerPeriodo($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT p.* FROM mod_maestro.dat_estructurasubsist es "
                        . "INNER JOIN mod_maestro.dat_cierre c ON c.idestructurasubsist = es.idestructurasubsist "
                        . "INNER JOIN mod_maestro.dat_periodocontable p ON p.idperiodo = c.idperiodoactual "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Carga la Fecha actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Fecha actual del subsistema
     */
    public function obtenerFecha($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT f.* FROM mod_maestro.dat_fecha f "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON es.idestructurasubsist = f.idestructurasubsist "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Obtiene los Comprobantes por estado.
     * @param Integer $idestructura
     * @return Array Listado de los Comprobantes por estado
     */
    public function getComprobantesPorEstado($idestructura)
    {
        $estados = $this->getEstadosComprobante();
        $datos = array();
        foreach ($estados as $v) {
            $cantidad = $this->cantComprobantesPorEstado($idestructura, $v['idestadocomprobante']);
            $datos[] = array('estado' => $v['descripcion'], 'cantidad' => $cantidad);
        }
        return $datos;
    }

    /**
     * Obtiene los datos del clasificador de Estados del Comprobante.
     * @param none
     * @return Array Listado de los Estados del Comprobante
     */
    public function getEstadosComprobante()
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT ec.* FROM mod_contabilidad.nom_estadocomprobante ec;");
    }

    /**
     * Obtiene la cantidad de Comprobantes por estado.
     * @param Integer $idestructura
     * @param Integer $idestadocomprobante
     * @return Integer cantidad de Comprobantes por estado
     */
    public function cantComprobantesPorEstado($idestructura, $idestadocomprobante)
    {
        $ejercicio = $this->obtenerEjercicio($idestructura, 4);
        $f_ini = $ejercicio[0]['inicio'];
        $f_fin = $ejercicio[0]['fin'];

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data_return = $connection->fetchAll("SELECT COUNT(dc.idcomprobante) as cantidad FROM mod_contabilidad.dat_comprobante dc "
                . "WHERE dc.idestructura = $idestructura AND dc.idestadocomprobante = $idestadocomprobante "
                . "AND (dc.fechaemision BETWEEN '" . $f_ini . "' AND '" . $f_fin . "');");
        return $data_return[0]['cantidad'];
    }

    /**
     * Obtiene los datos de los Comprobantes dado los identificadores.
     * @param Array $arrIdCmp
     * @return Array datos de los Comprobantes
     */
    public function getComprobantes($arrIdCmp)
    {
        $arrIds = implode(',', $arrIdCmp);
        $query = "SELECT * FROM mod_contabilidad.dat_comprobante WHERE idcomprobante IN ($arrIds)";
        $connection = Zend_Registry::get('conexion');
        return $connection->fetchAll($query);
    }

    /**
     * Carga los datos para modificar los Comprobantes de Operaciones.
     * @param Integer $idestructura
     * @param String $estados
     * @param Integer $periodo
     * @return Array datos del Comprobante de Operaciones
     */
    public function loadComprobantePorEstado($idestructura, $estados, $periodo)
    {
        $WHERE = (count($estados)) ? "ec.idestadocomprobante IN ($estados) AND " : "";
        $WHERE .= "dc.idperiodo = $periodo";
        $sql = "SELECT dc.*, ec.descripcion as estado, su.nombreusuario as usuario, ns.denominacion as subsistema "
                . "FROM mod_contabilidad.dat_comprobante dc "
                . "INNER JOIN mod_contabilidad.nom_estadocomprobante ec ON ec.idestadocomprobante = dc.idestadocomprobante "
                . "INNER JOIN mod_seguridad.seg_usuario su ON su.idusuario = dc.idusuario "
                . "INNER JOIN mod_maestro.nom_subsistema ns ON ns.idsubsistema = dc.idsubsistema "
                . "WHERE dc.idestructura = $idestructura AND $WHERE";
        $sql .= " ORDER BY dc.numerocomprobante ASC;";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll($sql);
    }

    /**
     * Carga los datos de los Comprobantes de Operaciones por estado y fecha.
     * @param Integer $idestructura
     * @param String $estados
     * @param Date $fInicio
     * @param Date $fFin
     * @return Array datos del Comprobante de Operaciones por estado y fecha.
     */
    public function loadComprobantePorEstadoFecha($idestructura, $estados, $fInicio, $fFin)
    {
        $WHERE = (count($estados)) ? "ec.idestadocomprobante IN ($estados) " : "";
        $sql = "SELECT dc.*, ec.descripcion as estado, su.nombreusuario as usuario, ns.denominacion as subsistema "
                . "FROM mod_contabilidad.dat_comprobante dc "
                . "INNER JOIN mod_contabilidad.nom_estadocomprobante ec ON ec.idestadocomprobante = dc.idestadocomprobante "
                . "INNER JOIN mod_seguridad.seg_usuario su ON su.idusuario = dc.idusuario "
                . "INNER JOIN mod_maestro.nom_subsistema ns ON ns.idsubsistema = dc.idsubsistema "
                . "WHERE dc.idestructura = $idestructura AND $WHERE AND (dc.fechaemision BETWEEN '" . $fInicio . "' AND '" . $fFin . "')";
        $sql .= " ORDER BY dc.numerocomprobante ASC;";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll($sql);
    }

}
