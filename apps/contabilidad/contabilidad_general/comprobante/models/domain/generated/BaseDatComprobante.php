<?php

abstract class BaseDatComprobante extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_contabilidad.dat_comprobante');
        $this->hasColumn('idcomprobante', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_contabilidad.sec_datcomprobante'));
        $this->hasColumn('numerocomprobante', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('referencia', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('fechaemision', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idestadocomprobante', 'decimal', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idperiodo', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idtipodiario', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('detalle', 'character varying', 250, array('fixed' => false, 'notnull' => false, 'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('contabilidad', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}
