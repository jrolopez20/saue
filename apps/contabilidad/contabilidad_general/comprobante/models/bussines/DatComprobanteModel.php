<?php

/**
 * Clase modelo para gestionar los Comprobantes de operaciones.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatComprobanteModel extends ZendExt_Model
{

    public function DatComprobanteModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Comprobantes de Operaciones.
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Comprobantes de Operaciones
     */
    public function listDataComprobante($argparams)
    {
        $DatComprobante = new DatComprobante();
        $filter = $this->defineFilters($argparams['filtros']);
        $cmp = $DatComprobante->listarComprobantes($argparams, $filter, $this->global->Estructura->idestructura);
        $cant = $DatComprobante->getTotalComprobantes($filter, $this->global->Estructura->idestructura);
        return array('datos' => $cmp, 'total' => $cant);
    }

    /**
     * Carga los datos para modificar los Comprobantes de Operaciones.
     * @param Array $argparams (idcomprobante)
     * @return Array Listado de los Comprobantes de Operaciones
     */
    public function loadDataComprobante($argparams)
    {
        $DatComprobante = new DatComprobante();
        $cmp = $DatComprobante->loadDataComprobante($argparams['idcomprobante']);
        return array('datos' => $cmp);
    }

    /**
     * Lista los datos de los Tipo de Asiento
     * @param $argparams
     * @return Array Listado de los Tipo de Asiento
     */
    public function listDataTipoAsiento($argparams)
    {
        $NomTipodiario = new NomTipodiario();
        $tipodiario = $NomTipodiario->listarTipoDiarioSubsistema($argparams);
        return array('datos' => $tipodiario);
    }

    /**
     * Lista los datos de los modelos y sus pases.
     * @param Array $argparams (idtipodiario)
     * @return Array Listado de los modelos y sus pases
     */
    public function listDataModeloPases($argparams)
    {
        $datComprobante = new DatComprobante();
        $modelos = $datComprobante->listDataModeloPases($argparams['idtipodiario']);
        return array('datos' => $modelos);
    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $datComprobante = new DatComprobante();
        $periodo = $datComprobante->obtenerPeriodo($this->global->Estructura->idestructura, 4);
        $fecha = $datComprobante->obtenerFecha($this->global->Estructura->idestructura, 4);
        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Obtiene las Cuentas contables por nivel.
     * @param Array $argparams (node)
     * @return Array Cuentas contables
     */
    public function loadDataCuentas($argparams)
    {
        $NomCuenta = new NomCuenta();
        $checked = json_decode($argparams['checked']);
        if ($argparams['filter']) {
            return $NomCuenta->buscarCuentasContables($this->global->Estructura->idestructura, $argparams['filter'],
                            $checked, 1);
        } else {
            return $NomCuenta->loadDataCuentas((is_numeric($argparams['node']) ? $argparams['node'] : null), $checked, 1);
        }
    }

    /**
     * Adiciona el Comprobante de Operaciones.
     * @param Array $argparams (idtipodiario, codigo, denominacion, descripcion)
     * @return boolean true if ocurred, false if failure
     */
    public function updateComprobante($argparams)
    {
        $pases = json_decode($argparams['pases']);
        $datComprobante = new StdClass();
        if (!$argparams['idcomprobante']) {
            $datComprobante->referencia = $argparams['referencia'];
            $datComprobante->fechaemision = $argparams['fechaemision'];
            $datComprobante->idestadocomprobante = $this->determinarEstadoCmp($pases);
            $datComprobante->idsubsistema = ($argparams['idsubsistema']) ? $argparams['idsubsistema'] : 4;
            $datComprobante->idperiodo = $argparams['idperiodo'];
            if (is_numeric($argparams['idtipodiario'])) $datComprobante->idtipodiario = $argparams['idtipodiario'];
            $datComprobante->detalle = $argparams['detalle'];
            $datComprobante->idusuario = $this->global->Perfil->idusuario;
            $datComprobante->idestructura = $this->global->Estructura->idestructura;
            return $this->Insertar($datComprobante, $pases);
        } else {
            $datComprobante->idcomprobante = $argparams['idcomprobante'];
            $datComprobante->referencia = $argparams['referencia'];
            $datComprobante->fechaemision = $argparams['fechaemision'];
            $datComprobante->idestadocomprobante = $this->determinarEstadoCmp($pases);
            $datComprobante->idsubsistema = ($argparams['idsubsistema']) ? $argparams['idsubsistema'] : 4;
            $datComprobante->idperiodo = $argparams['idperiodo'];
            $datComprobante->detalle = $argparams['detalle'];
            if (is_numeric($argparams['idtipodiario'])) $datComprobante->idtipodiario = $argparams['idtipodiario'];
            return $this->Actualizar($datComprobante, $pases);
        }
    }

    /**
     * Adiciona el Comprobante de Operaciones.
     * @param stdClass $DatCmp 
     * @param Array $pases 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($DatCmp, $pases)
    {
        try {
            $contabilidad = ($DatCmp->idsubsistema == 4) ? 1 : 0;
            $tipoD = ($DatCmp->idtipodiario) ? $DatCmp->idtipodiario : null;
            if ($tipoD) {
                $query = "INSERT INTO mod_contabilidad.dat_comprobante(referencia,fechaemision,idestadocomprobante,idsubsistema,idperiodo,idtipodiario,detalle,idusuario,idestructura,contabilidad) VALUES ";
                $query .= "('" . $DatCmp->referencia . "','" . $DatCmp->fechaemision . "'," . $DatCmp->idestadocomprobante . "," . $DatCmp->idsubsistema . "," . $DatCmp->idperiodo . "," . $tipoD . ",'" . $DatCmp->detalle . "'," . $DatCmp->idusuario . "," . $DatCmp->idestructura . ",$contabilidad)";
            } else {
                $query = "INSERT INTO mod_contabilidad.dat_comprobante(referencia,fechaemision,idestadocomprobante,idsubsistema,idperiodo,detalle,idusuario,idestructura,contabilidad) VALUES ";
                $query .= "('" . $DatCmp->referencia . "','" . $DatCmp->fechaemision . "'," . $DatCmp->idestadocomprobante . "," . $DatCmp->idsubsistema . "," . $DatCmp->idperiodo . ",'" . $DatCmp->detalle . "'," . $DatCmp->idusuario . "," . $DatCmp->idestructura . ",$contabilidad)";
            }
            $query .= " RETURNING idcomprobante, numerocomprobante";
            $conn = Zend_Registry::get('conexion');
            $execute = $conn->execute($query);
            $datCmp = $execute->fetchAll(PDO::FETCH_ASSOC);
            $idcomprobante = $datCmp[0]['idcomprobante'];
            $nocomprobante = $datCmp[0]['numerocomprobante'];

            $this->registerActionComprobante($idcomprobante, $DatCmp->idsubsistema, 0);
            return $this->actionInsertarPases($idcomprobante, $pases, $nocomprobante);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Actualiza el Comprobante de Operaciones.
     * @param stdClass $DatCmp 
     * @param Array $pases 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($DatCmp, $pases)
    {
        try {
            $query = "UPDATE mod_contabilidad.dat_comprobante SET referencia = '" . $DatCmp->referencia . "', fechaemision = '" . $DatCmp->fechaemision . "', "
                    . "idestadocomprobante = " . $DatCmp->idestadocomprobante . ", idsubsistema = " . $DatCmp->idsubsistema . ", idperiodo = " . $DatCmp->idperiodo . ", "
                    . "detalle = '" . $DatCmp->detalle . "'";
            if ($DatCmp->idtipodiario) {
                $query .= ", idtipodiario = " . $DatCmp->idtipodiario;
            }
            $query .= " WHERE idcomprobante = " . $DatCmp->idcomprobante;

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            $this->registerActionComprobante($DatCmp->idcomprobante, $DatCmp->idsubsistema, 1);
            $this->actionEliminarAsientos($DatCmp->idcomprobante);
            return $this->actionInsertarPases($DatCmp->idcomprobante, $pases);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Adiciona los pases del Comprobante de Operaciones.
     * @param Integer $idComprobante 
     * @param Array $pases 
     * @param Integer $nocomprobante 
     * @return boolean true if ocurred, false if failure
     */
    public function actionInsertarPases($idComprobante, $pases, $nocomprobante = null)
    {
        $dataAsientos = $this->formatoAsientoPase($idComprobante, $pases);
        try {
            foreach ($dataAsientos as $asiento) {
                $idAsiento = $this->insertarAsiento($asiento['idcomprobante'], $asiento['codigodocumento']);
                foreach ($asiento['pases'] as $pase) {
                    $this->insertarPase($idAsiento, $pase->idcuenta,
                            ($pase->debito != 0) ? $pase->debito * -1 : $pase->credito);
                }
            }
            $msg = !empty($nocomprobante) ?
                    "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado, 'nocomp':$nocomprobante, 'id':$idComprobante}"
                        : "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado, 'id':$idComprobante}";
            return $msg;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Adiciona el Asiento Contable.
     * @param Integer $idcomprobante 
     * @param Integer $codigodocumento 
     * @return boolean true if ocurred, false if failure
     */
    public function insertarAsiento($idcomprobante, $codigodocumento)
    {
        try {
            $query = "INSERT INTO mod_contabilidad.dat_asiento (codigodocumento,idcomprobante) VALUES ";
            $query .= "('" . $codigodocumento . "'," . $idcomprobante . ")";
            $returnning = " RETURNING idasiento";
            $query .= $returnning;

            $conn = Zend_Registry::get('conexion');
            $execute = $conn->execute($query);
            return $execute->fetchColumn(0);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Adiciona el Pase.
     * @param Integer $idasiento 
     * @param Integer $idcuenta 
     * @param Integer $importe
     * @return boolean true if ocurred, false if failure
     */
    public function insertarPase($idasiento, $idcuenta, $importe)
    {
        try {
            $query = "INSERT INTO mod_contabilidad.dat_pase (importe, idcuenta, idasiento) VALUES ";
            $query .= "(" . $importe . "," . $idcuenta . "," . $idasiento . ")";
            $conn = Zend_Registry::get('conexion');
            $execute = $conn->execute($query);
            return $execute;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Crea el formato Asiento-Pases.
     * @param Integer $idComprobante 
     * @param Array $pases
     * @return Array Formato Asiento-Pases
     */
    public function formatoAsientoPase($idComprobante, $pases)
    {
        $asientos = array();
        if ($pases[0]->codigodocumento) {
            $pases = $this->orderMultiDimensionalArrayStdClass($pases, 'codigodocumento');

            $currentPase = $pases[0]->codigodocumento;
            $arrPases = array();
            $count = count($pases);
            foreach ($pases as $k => $v) {
                if ($currentPase != $v->codigodocumento) {
                    $asientos[] = array('idcomprobante' => $idComprobante, 'codigodocumento' => $currentPase, 'pases' => $arrPases);
                    $arrPases = array();
                    $arrPases[] = $v;
                    $currentPase = $v->codigodocumento;
                    if ((($k + 1) == $count)) {
                        $asientos[] = array('idcomprobante' => $idComprobante, 'codigodocumento' => $currentPase, 'pases' => $arrPases);
                    }
                } else if (($k + 1) == $count) {
                    $arrPases[] = $v;
                    $asientos[] = array('idcomprobante' => $idComprobante, 'codigodocumento' => $currentPase, 'pases' => $arrPases);
                } else {
                    $arrPases[] = $v;
                }
            }
        } else {
            $asientos[] = array('idcomprobante' => $idComprobante, 'codigodocumento' => '', 'pases' => $pases);
        }
        return $asientos;
    }

    /**
     * Asienta el Comprobante de Operaciones.
     * @param Array $argparams (idcomprobante) 
     * @return boolean true if ocurred, false if failure
     */
    public function asentarComprobante($argparams)
    {
        try {
            $arrIds = implode(',', json_decode($argparams['idcomprobantes']));
            $query = "UPDATE mod_contabilidad.dat_comprobante SET idestadocomprobante = 8032 WHERE idcomprobante IN ($arrIds)";
            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            $datComprobante = new DatComprobante();
            $dataCmp = $datComprobante->getComprobantes(json_decode($argparams['idcomprobantes']));
            $this->registerActionComprobante($dataCmp[0]['idcomprobante'], $dataCmp[0]['idsubsistema'], 2);

            if (count($argparams['idcomprobante']) > 1) {
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgAsentados}";
            } else {
                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgAsentado}";
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Accion para guardar los datos del Historial en el Comprobante de Operaciones.
     * @param Integer $idComprobante 
     * @param Integer $idsubsistema
     * @param String $accion
     * @return void
     */
    public function registerActionComprobante($idComprobante, $idsubsistema, $accion)
    {
        $user = $this->global->Perfil->idusuario;
        $action = ($accion == 0) ? "CREADO" : (($accion == 1) ? "MODIFICADO" : "CONTABILIZADO");
        $date = Date('d/m/Y');
        $dataSubsistema = $this->integrator->contabilidad->getSubsistemaByID($idsubsistema);
        $subsistema = $dataSubsistema[0]['denominacion'];
        $query = "INSERT INTO mod_contabilidad.dat_historialcmp (idcomprobante, usuario, subsistema, fechamaquina, accion) VALUES ";
        $query .= "(" . $idComprobante . "," . $user . ",'" . $subsistema . "','" . $date . "','" . $action . "')";
        $conn = Zend_Registry::get('conexion');
        try {
            $conn->execute($query);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Determina el estado del Comprobante al comprobar sus pases.
     * @param Array $arrPases arreglo de pases a analizar
     * @return Integer Estado del Comprobante
     */
    public function determinarEstadoCmp($arrPases)
    {
        $sumaComprobacion = 0;
        foreach ($arrPases as $v) {
            $sumaComprobacion += ($v->debito) ? $v->debito * -1 : $v->credito;
        }
        return ($sumaComprobacion == 0) ? 8030 : 8033;
    }

    /**
     * Elimina los Asientos y sus Pases asociados al Comprobante.
     * @param Integer $idcomprobante
     * @return void 
     */
    public function actionEliminarAsientos($idcomprobante)
    {
        try {
            $query = "DELETE FROM mod_contabilidad.dat_asiento WHERE idcomprobante in (" . $idcomprobante . ")";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
            return;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Muestra una grafica de los Comprobantes por estado.
     * @param none
     * @return Array
     */
    public function graficarComprobantesPorEstado()
    {
        $DatComprobante = new DatComprobante();
        $resultado = $DatComprobante->getComprobantesPorEstado($this->global->Estructura->idestructura);
        $total = 0;
        foreach ($resultado as $fila) {
            $total += $fila['cantidad'];
        }

        $datos = array();
        foreach ($resultado as $fila) {
            if ($fila['cantidad'] > 0) {
                $porciento = 100 * $fila['cantidad'] / $total;
                $porciento = round($porciento * 100) / 100;

                $datos[] = array(
                    'estado' => $fila['estado'],
                    'porciento' => $porciento,
                    'cantidad' => $fila['cantidad']
                );
            }
        }
        return $datos;
    }

    /*
      $toOrderArray -> Array StdClass a ordenar
      $field -> Campo del array por el que queremos ordenarlo (entre comillas).
      $inverse -> Su valor serï¿½ true o false. El valor true lo ordenarï¿½ de manera descendente y el false (valor por defecto) lo ordenarï¿½ de manera ascendente.
     */

    public function orderMultiDimensionalArrayStdClass($toOrderArray, $field, $inverse = false)
    {
        foreach ($toOrderArray as $key => $row) {
            $position[$key] = $row->$field;
            $newRow[$key] = $row;
        }
        if ($inverse) arsort($position);
        else asort($position);
        foreach ($position as $key => $pos) $returnArray[] = $newRow[$key];
        return $returnArray;
    }

    /**
     * procesa los filtros y devuelve la sentencia SQL.
     * @param Array $filter
     * @return String cadena SQL
     */
    public function defineFilters($filter)
    {
        $filter = json_decode($filter);
        $data_return = "";
        if (count($filter)) {
            foreach ($filter as $k => $v) {
                if ($v->property == 'numerocomprobante' && is_numeric($v->value))
                        $data_return .= ($k == 0) ? "dc." . $v->property . " = '" . $v->value . "'" : " OR dc." . $v->property . " = '" . $v->value . "'";
                else if ($v->property != 'numerocomprobante')
                        $data_return .= ($k == 0 || $data_return == '') ? "dc." . $v->property . " ILIKE '%" . $v->value . "%'"
                                : " OR dc." . $v->property . " ILIKE '%" . $v->value . "%'";
            }
        }
        return $data_return;
    }

    /**
     * Carga los datos para la vista previa del Comprobante de operaciones.
     * @param Array $argparams (idcomprobante) 
     * @return Array Datos del Comprobante de operaciones
     */
    public function vistaPreviaComprobante($argparams)
    {
        $idCmp = $argparams['idcomprobante'];
        $conn = Zend_Registry::get('conexion');
        $dataCmp = $conn->fetchAll("select * from mod_contabilidad.f_imprimir_comprobante($idCmp)");
        $this->getDataPrinter($dataCmp);
        $datoCuerpo = (count($datoCuerpo) > 0) ? array_merge($datoCuerpo, $dataCmp) : $dataCmp;
        $datoGeneral = array();
        $datosEmpresa = $this->integrator->configuracion->getDatosEmpresa($this->global->Estructura->idestructura);
        $datoGeneral['reporte'] = 10004;
        $datoGeneral['logo'] = $datosEmpresa['logo'];
        return array('dataSources' => $datoCuerpo, 'datoGeneral' => $datoGeneral);
    }

    /**
     * Crea el formato de los datos para la vista previa del Comprobante de operaciones.
     * @param Array $dataComprobante
     * @return Array Datos del Comprobante de operaciones formateados
     */
    public function getDataPrinter(&$dataComprobante)
    {
        $certificado = $this->global->Perfil->certificado;
        $idUsuarioConfecciona = null;
        $nombreUsuarioConfecciona = null;
        $idUsuarioAsienta = null;
        $nombreUsuarioAsienta = null;

        foreach ($dataComprobante as &$row) {
            if (!isset($idUsuarioConfecciona) || ($idUsuarioConfecciona != $row->idusuario && isset($row->idusuario))) {
                $arrayDatosUsuario = $this->integrator->seguridad->getUsersProfile(array('idusuario' => $row->idusuario),
                        $certificado);
                if ($arrayDatosUsuario['idusuario']['nombre']) {
                    $nombreUsuarioConfecciona = $arrayDatosUsuario['idusuario']['nombre'] . " " . $arrayDatosUsuario['idusuario']['apellido1'] . " " . $arrayDatosUsuario['idusuario']['apellido2'];
                } else {
                    $nombreUsuarioConfecciona = $arrayDatosUsuario['idusuario']['usuario'];
                }
                $idUsuarioConfecciona = $row->idusuario;
            }
            if (!isset($idUsuarioAsienta) || ($idUsuarioAsienta != $row->idusuarioAsienta && isset($row->idusuarioAsienta))) {
                $arrayDatosUsuario = $this->integrator->seguridad->getUsersProfile(array('idusuario' => $row->idusuarioasienta),
                        $certificado);
                if ($arrayDatosUsuario['idusuario']['nombre']) {
                    $nombreUsuarioAsienta = $arrayDatosUsuario['idusuario']['nombre'] . " " . $arrayDatosUsuario['idusuario']['apellido1'] . " " . $arrayDatosUsuario['idusuario']['apellido2'];
                } else {
                    $nombreUsuarioAsienta = $arrayDatosUsuario['idusuario']['usuario'];
                }
                $idUsuarioAsienta = $row->idusuarioasienta;
            }
            $row->confecciona = $nombreUsuarioConfecciona;
            $row->asienta = $nombreUsuarioAsienta;
        }
    }

    /**
     * Pasa la fecha al dia siguiente
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function nextDate()
    {
        $datComprobante = new DatComprobante();
        $dataPeriodo = $datComprobante->obtenerPeriodo($this->global->Estructura->idestructura, 4);
        $dataFecha = $datComprobante->obtenerFecha($this->global->Estructura->idestructura, 4);
        if (implode('', explode('-', $dataFecha[0]['fecha'])) == implode('', explode('-', $dataPeriodo[0]['fin']))) {
            return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgUltimoDia}";
        } else {
            list($y, $m, $d) = explode('-', $dataFecha[0]['fecha']);
            $data = $y . '-' . $m . '-' . ($d + 1);
            $query = "UPDATE mod_maestro.dat_fecha SET fecha = '" . $data . "'";
            $query .= " WHERE idfecha = " . $dataFecha[0]['idfecha'];
            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaSiguiente}";
        }
    }

    public function getDataComprobante($idcomprobante)
    {
        $datComprobante = new DatComprobante();
        $dataCmp = $datComprobante->getComprobantes(array($idcomprobante));
        $data_return = $dataCmp[0];
        $data_return['pases'] = $datComprobante->loadDataComprobante($idcomprobante);
        return $data_return;
    }

}
