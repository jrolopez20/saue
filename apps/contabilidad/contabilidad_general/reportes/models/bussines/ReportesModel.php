<?php

/**
 * Clase modelo para gestionar los Reportes de contabilidad.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ReportesModel extends ZendExt_Model
{

    public function ReportesModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los Reportes disponibles.
     * @param none
     * @return Array Listado de los Reportes disponibles
     */
    public function loadDataTipoReporte()
    {
        $reportes[] = array('idtiporeporte' => 10001, 'nombre' => 'Mayor');
        $reportes[] = array('idtiporeporte' => 10002, 'nombre' => 'Sub mayor');
        $reportes[] = array('idtiporeporte' => 10003, 'nombre' => 'Balance de comprobación de saldo');
        $reportes[] = array('idtiporeporte' => 10006, 'nombre' => 'Comprobantes de operaciones contabilizados');
        $reportes[] = array('idtiporeporte' => 10007, 'nombre' => 'Comprobantes de operaciones con error');
        return array('datos' => $reportes);
    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $datComprobante = new DatComprobante();
        $periodo = $datComprobante->obtenerPeriodo($this->global->Estructura->idestructura, 4);
        $fecha = $datComprobante->obtenerFecha($this->global->Estructura->idestructura, 4);
        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Lista los datos de los Ejercicios Contables.
     * @param none
     * @return Array Listado de los Ejercicios Contables
     */
    public function loadDataEjercicios()
    {
        $datEj = new DatEjerciciocontable();
        return array('datos' => $datEj->loadEjercicios());
    }

    /**
     * Lista los datos de los Periodos Contables.
     * @param Array $argparams (idejercicio)
     * @return Array Listado de los Periodos Contables
     */
    public function loadDataPeriodos($argparams)
    {
        $datEj = new DatPeriodocontable();
        return array('datos' => $datEj->loadPeriodos(0, 0, $argparams['idejercicio']));
    }

    /**
     * Lista los datos de las Cuentas Contables.
     * @param Array $argparams (idtiporeporte)
     * @return Array Listado de las Cuentas Contables
     */
    public function loadDataCuentas($argparams)
    {
        $NomCuenta = new NomCuenta();
        return array('datos' => $NomCuenta->loadDataCuentasNivel(($argparams['idtiporeporte'] == 10001) ? 0 : 1));
    }

    /**
     * Lista los niveles establecidos en el formato del clasificador de cuentas.
     * @return Array Listado de los Niveles del clasificador de cuentas
     */
    public function loadDataNiveles()
    {
        $response = array('datos' => array());
        $model = new NomCuentaModel();
        $niveles = $model->loadDataFormatos();
        if (count($niveles) && !empty($niveles[0]['partes'])) {
            $response['datos'] = $niveles[0]['partes'];
        }
        return $response;
    }

    /**
     * Lista los niveles del formato de la Cuenta contable.
     * @param none
     * @return Array Listado de niveles del formato de la Cuenta contable
     */
    public function loadDataBalance()
    {
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        if (count($formato)) {
            $DatParteformato = new DatParteformato();
            $niveles = $DatParteformato->listParteFormatos($formato[0]['idformato']);
            return array('datos' => $niveles);
        } else {
            return array();
        }
    }

    /**
     * Obtiene los datos para adicionar las Cuentas contables por nivel.
     * @param Array $argparams (nivel)
     * @return Array  Cuentas contables
     */
    public function loadDataAddCuenta($argparams)
    {
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        if (!count($formato)) {
            $DatFormato = new DatFormato();
            $formato = $DatFormato->loadFormatosSubsistema($this->global->Estructura->idestructura, 4);
            if (count($formato)) {
                $this->asociarNivelFormato($formato, $argparams['nivel']);
            }
            return array('formato' => $formato);
        } else {
            return array('nivel' => $this->getNivelFormato($formato, $argparams['nivel']));
        }
    }

    /**
     * Obtiene los datos para los diferentes reportes.
     * @param Array $argparams (cuentas, desde, hasta, idreporte)
     * @return Array Datos de los reportes
     */
    public function loadDataReport($argparams)
    {
        $agFechaDesde = $argparams['desde'];
        $agFechaHasta = $argparams['hasta'];
        $agIdReporte = $argparams['tiporeporte'];
        $agArrayIdDato = json_decode($argparams['cuenta']);

        $datosReporteHtmlPdf = array();
        $datosReporte = array();
        $datoGeneral = array();
        $datoCuerpo = array();
        $datoCuerpoCtasVsNaturaleza = array();
        $flagCtasSaldoVsNaturaleza = false;
        $estructura = $this->integrator->metadatos->DameEstructura($this->global->Estructura->idestructura);
        $objEstructura = $estructura[0];
        $datosEmpresa = $this->integrator->configuracion->getDatosEmpresa($this->global->Estructura->idestructura);
        $datoGeneral['logo'] = $datosEmpresa['logo'];

        $this->getDatosReporte($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agFechaDesde,
                $agFechaHasta, $agIdReporte, $agArrayIdDato, $flagCtasSaldoVsNaturaleza, $datoCuerpoCtasVsNaturaleza);
        if (count($datosReporte) > 0) {
            $datosReporteHtmlPdf[] = array('datoGeneral' => $datoGeneral, 'datoCuerpo' => $datoCuerpo, 'datoPie' => $datoPie);
            if ($flagCtasSaldoVsNaturaleza) {
                $datosReporteHtmlPdf[] = array('datoGeneral' => $datoGeneral, 'datoCuerpo' => $datoCuerpoCtasVsNaturaleza,
                    'datoPie' => $datoPie);
            }
        }
        return (count($datosReporte) > 0) ? array('idreporte' => $agIdReporte, 'datos' => $datosReporte, 'dataSources' => $datosReporteHtmlPdf)
                    : array('idreporte' => 0, 'datos' => array());
    }

    /**
     * Redirecciona a la implementacion del reporte solicitado.
     * @param Array $datosReporte Datos principales del reporte en forma de grid
     * @param Array $datoGeneral Datos principales del reporte (encabezado)
     * @param Array $datoCuerpo Cuerpo del reporte
     * @param StdClass $objEstructura Estructura logueada
     * @param Date $agFechaDesde Fecha inicio del reporte
     * @param Date $agFechaHasta Fecha fin del reporte
     * @param Integer $agIdReporte Identificador del reporte
     * @param Array $agArrayIdDato Cuentas contables
     * @param Boolean $flagCtasSaldoVsNaturaleza
     * @param Array $datoCuerpoCtasVsNaturaleza
     * @return Reporte solicitado
     */
    public function getDatosReporte($argparams, &$datosReporte, &$datoGeneral, &$datoCuerpo, $objEstructura,
            $agFechaDesde, $agFechaHasta, $agIdReporte, $agArrayIdDato, & $flagCtasSaldoVsNaturaleza,
            & $datoCuerpoCtasVsNaturaleza)
    {
        switch ($agIdReporte) {
            case '10001':
                return $this->funct_execute_Mayor($datosReporte, $datoGeneral, $datoCuerpo, $objEstructura,
                                $agFechaDesde, $agFechaHasta, $agIdReporte, $agArrayIdDato);
            case '10002':
                return $this->funct_execute_SubMayor($datosReporte, $datoGeneral, $datoCuerpo, $objEstructura,
                                $agFechaDesde, $agFechaHasta, $agIdReporte, $agArrayIdDato);
            case '10003':
                return $this->BalanceComprobacionSaldo($argparams, $datosReporte, $datoGeneral, $datoCuerpo,
                                $objEstructura, $agFechaDesde, $agFechaHasta, $agIdReporte);
            case '10006':
            case '10007':
                return $this->loadDataComprobantes($datosReporte, $datoGeneral, $datoCuerpo, $objEstructura,
                                $agFechaDesde, $agFechaHasta, $agIdReporte);
        }
    }

    private function loadDataComprobantes(&$datosReporte, &$datoGeneral, &$datoCuerpo, $objEstructura, $agFechaDesde,
            $agFechaHasta, $agIdReporte)
    {
        $objDatComprobante = new DatComprobante();
        $estados = ($agIdReporte == 10006) ? "8032" : "8033";
        $comprobantes = $objDatComprobante->loadComprobantePorEstadoFecha($objEstructura->idestructura, $estados,
                $agFechaDesde, $agFechaHasta);
        foreach ($comprobantes as &$v) {
            $v['fechaemision'] = implode('/', array_reverse(explode("-", $v['fechaemision'])));
        }
        if (count($datosReporte) == 0) {
            $datoGeneral['fechaDesde'] = $agFechaDesde;
            $datoGeneral['fechaHasta'] = $agFechaHasta;
            $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
            $datoGeneral['reporte'] = $agIdReporte;
            $datoGeneral['entidad'] = $objEstructura->denominacion;
        }
        $datoCuerpo = $comprobantes;
        $datosReporte = $comprobantes;
    }

    private function BalanceComprobacionSaldo($argparams, &$datosReporte, &$datoGeneral, &$datoCuerpo, $objEstructura,
            $agFechaDesde, $agFechaHasta, $agIdReporte)
    {
        $conn = Zend_Registry::get('conexion');
        $nivel = $argparams['nivel'];
        try {
            $resultado = $conn->fetchAll("select * from mod_contabilidad.f_balancecomprobacion($nivel, '$agFechaDesde', '$agFechaHasta',$objEstructura->idestructura)");
            $cantidad = count($resultado);

            if ($cantidad) {
                if (count($datosReporte) == 0) {
                    $datoGeneral['fechaDesde'] = $agFechaDesde;
                    $datoGeneral['fechaHasta'] = $agFechaHasta;
                    list($datoGeneral['diaPeriodo'], $datoGeneral['mesPeriodo'], $datoGeneral['yearPeriodo']) = explode("/",
                            $agFechaHasta);
                    list($datoGeneral['diaActual'], $datoGeneral['mesActual'], $datoGeneral['yearActual']) = explode("/",
                            date('d/m/Y'));
                    $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
                    $datoGeneral['reporte'] = $agIdReporte;
                    $datoGeneral['entidad'] = $objEstructura->denominacion;
                }
                $datoCuerpo = $resultado;
                $datosReporte = $resultado;
            }
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    /**
     * Ejecuta la llamada al procedimiento del Mayor.
     * @param Array Elements
     * @return void
     */
    public function funct_execute_Mayor(& $datosReporte, & $datoGeneral, & $datoCuerpo, $objEstructura, $agFechaDesde,
            $agFechaHasta, $agIdReporte, $agArrayIdDato)
    {
        $conn = Zend_Registry::get('conexion');
        $agArrayIdDato = implode(",", $agArrayIdDato);
        $resultado = $conn->fetchAll("select * from mod_contabilidad.f_mayor(array[$agArrayIdDato], array[$objEstructura->idestructura], '$agFechaDesde', '$agFechaHasta')");
        $this->funct_execute_Mayor_SubMayor('MAYOR', $datosReporte, $datoGeneral, $datoCuerpo, $agFechaDesde,
                $agFechaHasta, $agIdReporte, $objEstructura, $resultado);
    }

    /**
     * Ejecuta la llamada al procedimiento del Submayor.
     * @param Array Elements
     * @return void
     */
    public function funct_execute_SubMayor(& $datosReporte, & $datoGeneral, & $datoCuerpo, $objEstructura,
            $agFechaDesde, $agFechaHasta, $agIdReporte, $agArrayIdDato)
    {
        $conn = Zend_Registry::get('conexion');
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        $separador = (count($formato)) ? $formato[0]['separador'] : '.';

        $agArrayIdDato = implode(",", $agArrayIdDato);
        $resultado = $conn->fetchAll("select * from mod_contabilidad.f_submayor(array[$agArrayIdDato], array[$objEstructura->idestructura], '$agFechaDesde', '$agFechaHasta', '$separador')");
        $this->funct_execute_Mayor_SubMayor('SUBMAYOR', $datosReporte, $datoGeneral, $datoCuerpo, $agFechaDesde,
                $agFechaHasta, $agIdReporte, $objEstructura, $resultado);
    }

    /**
     * Da el formato de salida a los reportes de Mayor y Submayor.
     * @param Array Elements
     * @return Array Cuerpo del reporte
     */
    public function funct_execute_Mayor_SubMayor($titulo, &$datosReporte, &$datoGeneral, &$datoCuerpo, $agFechaDesde,
            $agFechaHasta, $agIdReporte, $objEstructura, $resultado)
    {
        $count = count($resultado);
        if ($count > 0) {
            if (count($datosReporte) == 0) {
                $datoGeneral['fechaDesde'] = $agFechaDesde;
                $datoGeneral['fechaHasta'] = $agFechaHasta;
                list($datoGeneral['diaPeriodo'], $datoGeneral['mesPeriodo'], $datoGeneral['yearPeriodo']) = explode("/",
                        $agFechaHasta);
                list($datoGeneral['diaActual'], $datoGeneral['mesActual'], $datoGeneral['yearActual']) = explode("/",
                        date('d/m/Y'));
                $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
                $datoGeneral['reporte'] = $agIdReporte;
                $datoGeneral['titulo'] = $titulo;
                $datoGeneral['entidad'] = $objEstructura->denominacion;
                list($dia, $mes, $datoGeneral['year']) = array_reverse(explode("-", $resultado[1]['fechaemision']));
            }

//            $datosReporte[] = array('cuenta' => "<b>$objEstructura->denominacion</b>", 'fechaemision' => '', 'comprobante' => '', 'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');

            $text = $resultado[$key + 1]['clavecuenta'] . " " . $resultado[$key + 1]['descripcioncuenta'];
            $datosReporte[] = array('cuenta' => "<b>CUENTA: </b>$text", 'fechaemision' => '', 'numerocomprobante' => '',
                'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');
            if (isset($resultado[$key + 1]['clavesubcuenta']) && strlen($resultado[$key + 1]['clavesubcuenta'])) {
                $text = $resultado[$key + 1]['clavesubcuenta'] . " " . $resultado[$key + 1]['descripcionsubcuenta'];
                $datosReporte[] = array('cuenta' => "<b>SUBCUENTA: </b>$text", 'fechaemision' => '', 'numerocomprobante' => '',
                    'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');
            }
            if (isset($resultado[$key + 1]['claveanalisis']) && strlen($resultado[$key + 1]['claveanalisis'])) {
                $text = $resultado[$key + 1]['claveanalisis'] . " " . $resultado[$key + 1]['descripcionanalisis'];
                $datosReporte[] = array('cuenta' => "<b>AN&Aacute;LISIS: </b>$text", 'fechaemision' => '', 'numerocomprobante' => '',
                    'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');
            }
            foreach ($resultado as $key => $fila) {
                $keepCero = ($fila['descripcion'] == "<b>SALDO ANTERIOR</b>" || $fila['descripcion'] == "<b>SUMA ACUMULADA</b>")
                            ? true : false;
                $datosReporte[] = array('cuenta' => "", 'fechaemision' => $fila['fechaemision'], 'numerocomprobante' => $fila['comprobante'],
                    'idcomprobante' => $fila['idcomprobante'], 'idtipocomprobante' => $fila['idtipocomprobante'], 'idsubsistema' => $fila['idsubsistema'],
                    'idestructurae' => $fila['idestructurae'], 'descripcion' => $fila['descripcion'], 'debe' => $this->Format($fila['debe'],
                            $keepCero), 'haber' => $this->Format($fila['haber'], $keepCero), 'saldo' => $this->Format($fila['saldo'],
                            true));
                if ($resultado[$key + 1]['descripcion'] == "<b>SALDO ANTERIOR</b>") {

                    $text = $resultado[$key + 2]['clavecuenta'] . " " . $resultado[$key + 2]['descripcioncuenta'];
                    $datosReporte[] = array('cuenta' => "<b>CUENTA: </b>$text", 'fechaemision' => '', 'numerocomprobante' => '',
                        'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');
                    if (isset($resultado[$key + 2]['clavesubcuenta']) && strlen($resultado[$key + 2]['clavesubcuenta'])) {
                        $text = $resultado[$key + 2]['clavesubcuenta'] . " " . $resultado[$key + 2]['descripcionsubcuenta'];
                        $datosReporte[] = array('cuenta' => "<b>SUBCUENTA: </b>$text", 'fechaemision' => '', 'numerocomprobante' => '',
                            'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');
                    }
                    if (isset($resultado[$key + 2]['claveanalisis']) && strlen($resultado[$key + 2]['claveanalisis'])) {
                        $text = $resultado[$key + 2]['claveanalisis'] . " " . $resultado[$key + 2]['descripcionanalisis'];
                        $datosReporte[] = array('cuenta' => "<b>AN&Aacute;LISIS: </b>$text", 'fechaemision' => '', 'numerocomprobante' => '',
                            'descripcion' => '', 'debe' => '', 'haber' => '', 'saldo' => '');
                    }
                }
                list($dia, $mes) = array_reverse(explode("-", $fila['fechaemision']));
                $datoCuerpo[] = array('clavecuenta' => $fila['clavecuenta'], 'descripcioncuenta' => $fila['descripcioncuenta'],
                    'clavesubcuenta' => $fila['clavesubcuenta'], 'descripcionsubcuenta' => $fila['descripcionsubcuenta'],
                    'claveanalisis' => $fila['claveanalisis'], 'descripcionanalisis' => $fila['descripcionanalisis'], 'Entidad' => $objEstructura->descripcion,
                    'NUMEROUM' => $fila['codigoestructura'], 'fechaemision' => $fila['fechaemision'], 'comprobante' => $fila['comprobante'],
                    'descripcion' => $fila['descripcion'], 'debe' => $fila['debe'], 'haber' => $fila['haber'], 'saldo' => $fila['saldo'],
                    'dia' => $dia, 'mes' => $mes);
            }
        }
    }

    /**
     * Da Formto a los saldos del reporte.
     * @param $num
     * @param $keepCero
     * @return Saldo formateado a dos lugares despues de la coma
     */
    public function Format($num, $keepCero = false)
    {
        if ($num == 0) {
            if (!$keepCero) {
                return '';
            } else if ($keepCero) {
                return number_format($num, '2', '.', ',');
            }
        } elseif ($num < 0) {
            return "(" . number_format($num * -1, '2', '.', ',') . ")";
        } else {
            return number_format($num, '2', '.', ',');
        }
    }

}
