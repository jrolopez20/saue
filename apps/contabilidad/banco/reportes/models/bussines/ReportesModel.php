<?php

/**
 * Clase modelo para gestionar los Reportes de contabilidad.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ReportesModel extends ZendExt_Model
{

    public function ReportesModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los Reportes disponibles.
     * @param none
     * @return Array Listado de los Reportes disponibles
     */
    public function loadDataTipoReporte()
    {
        $reportes[] = array('idtiporeporte' => 20001, 'nombre' => 'Conciliación bancaria');
        /*$reportes[] = array('idtiporeporte' => 20002, 'nombre' => 'Saldo disponible en bancos');*/
        $reportes[] = array('idtiporeporte' => 20003, 'nombre' => 'Movimiento bancario por período');
        $reportes[] = array('idtiporeporte' => 20004, 'nombre' => 'Resumen de saldos bancarios');
        /*$reportes[] = array('idtiporeporte' => 20005, 'nombre' => 'Reporte de cheques posfechados');*/
        return array('datos' => $reportes);
    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $datComprobante = new DatComprobante();
        $periodo = $datComprobante->obtenerPeriodo($this->global->Estructura->idestructura, 4);
        $fecha = $datComprobante->obtenerFecha($this->global->Estructura->idestructura, 4);
        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Lista los datos de los Ejercicios Contables.
     * @param none
     * @return Array Listado de los Ejercicios Contables
     */
    public function loadDataEjercicios()
    {
        $datEj = new DatEjerciciocontable();
        return array('datos' => $datEj->loadEjercicios());
    }

    /**
     * Lista los datos de los Periodos Contables.
     * @param Array $argparams (idejercicio)
     * @return Array Listado de los Periodos Contables
     */
    public function loadDataPeriodos($argparams)
    {
        $datEj = new DatPeriodocontable();
        return array('datos' => $datEj->loadPeriodos(0, 0, $argparams['idejercicio']));
    }

    /**
     * Lista los datos de las Cuentas Contables.
     * @param Array $argparams (idtiporeporte)
     * @return Array Listado de las Cuentas Contables
     */
    public function loadDataCuentas($argparams)
    {
        $NomCuenta = new NomCuenta();
        return array('datos' => $NomCuenta->loadDataCuentasNivel(($argparams['idtiporeporte'] == 10001) ? 0 : 1));
    }

    /**
     * Lista los niveles establecidos en el formato del clasificador de cuentas.
     * @return Array Listado de los Niveles del clasificador de cuentas
     */
    public function loadDataNiveles()
    {
        $response = array('datos' => array());
        $model = new NomCuentaModel();
        $niveles = $model->loadDataFormatos();
        if (count($niveles) && !empty($niveles[0]['partes'])) {
            $response['datos'] = $niveles[0]['partes'];
        }
        return $response;
    }

    /**
     * Lista los niveles del formato de la Cuenta contable.
     * @param none
     * @return Array Listado de niveles del formato de la Cuenta contable
     */
    public function loadDataBalance()
    {
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        if (count($formato)) {
            $DatParteformato = new DatParteformato();
            $niveles = $DatParteformato->listParteFormatos($formato[0]['idformato']);
            return array('datos' => $niveles);
        } else {
            return array();
        }
    }

    /**
     * Obtiene los datos para adicionar las Cuentas contables por nivel.
     * @param Array $argparams (nivel)
     * @return Array  Cuentas contables
     */
    public function loadDataAddCuenta($argparams)
    {
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        if (!count($formato)) {
            $DatFormato = new DatFormato();
            $formato = $DatFormato->loadFormatosSubsistema($this->global->Estructura->idestructura, 4);
            if (count($formato)) {
                $this->asociarNivelFormato($formato, $argparams['nivel']);
            }
            return array('formato' => $formato);
        } else {
            return array('nivel' => $this->getNivelFormato($formato, $argparams['nivel']));
        }
    }

    /**
     * Obtiene los datos para los diferentes reportes.
     * @param Array $argparams (cuentas, desde, hasta, idreporte)
     * @return Array Datos de los reportes
     */
    public function loadDataReport($argparams)
    {
        $agIdReporte = $argparams['tiporeporte'];

        $datosReporte = array();
        $datoGeneral = array();
        $datoCuerpo = array();

        $estructura = $this->integrator->metadatos->DameEstructura($this->global->Estructura->idestructura);
        $objEstructura = $estructura[0];

        return $this->getDatosReporte($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agIdReporte);
    }

    /**
     * Redirecciona a la implementacion del reporte solicitado.
     * @param Array $datosReporte Datos principales del reporte en forma de grid
     * @param Array $datoGeneral Datos principales del reporte (encabezado)
     * @param Array $datoCuerpo Cuerpo del reporte
     * @param StdClass $objEstructura Estructura logueada
     * @param Date $agFechaDesde Fecha inicio del reporte
     * @param Date $agFechaHasta Fecha fin del reporte
     * @param Integer $agIdReporte Identificador del reporte
     * @param Array $agArrayIdDato Cuentas contables
     * @return Reporte solicitado
     */
    public function getDatosReporte($argparams, & $datosReporte, & $datoGeneral, & $datoCuerpo, $objEstructura, $agIdReporte)
    {
        switch ($agIdReporte) {
            case '20001':
                return $this->Concialiacion($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agIdReporte);
            case '20002':
                return $this->Concialiacion($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agIdReporte);
            case '20003':
                return $this->MovBancarios($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agIdReporte);
            case '20004':
                return $this->ResumenSaldos($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agIdReporte);
            case '20005':
                return $this->Concialiacion($argparams, $datosReporte, $datoGeneral, $datoCuerpo, $objEstructura, $agIdReporte);
            default:
                throw new Exception('Reporte no encontrado');
        }
    }

    private function Concialiacion($argparams, &$datosReporte, &$datoGeneral, &$datoCuerpo, $objEstructura, $agIdReporte)
    {
        $agFechaDesde = $argparams['desde'];
        $agFechaHasta = $argparams['hasta'];
        $estructura = 'ARRAY[' . $objEstructura->idestructura . ']';
        $tipoCuenta = 'ARRAY[' . implode(',', json_decode($argparams['idtipocuenta'])) . ']';

        $dm = Doctrine_Manager::getInstance();
        $cxn = $dm->getCurrentConnection();

        $data = $this->calcularconciliacion($estructura, $tipoCuenta, $agFechaDesde, $agFechaHasta, $cxn);
        return $data;
//        try{
//            $resultado = $cxn->fetchAll("select * from mod_banco.\"f_conciliacionResumen\"($estructura, $tipoCuenta, '$agFechaDesde', '$agFechaHasta')");
//            $cantidad = count($resultado);
//
//            if($cantidad){
//                if (count($datosReporte) == 0) {
//                    $datoGeneral['fechaDesde'] = $agFechaDesde;
//                    $datoGeneral['fechaHasta'] = $agFechaHasta;
////                    list($datoGeneral['diaPeriodo'], $datoGeneral['mesPeriodo'], $datoGeneral['yearPeriodo']) = explode("/", $agFechaHasta);
////                    list($datoGeneral['diaActual'], $datoGeneral['mesActual'], $datoGeneral['yearActual']) = explode("/", date('d/m/Y'));
//                    $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
//                    $datoGeneral['reporte'] = $agIdReporte;
//                    $datoGeneral['descripcionentidad'] = strtoupper($objEstructura->denominacion);
//                }
//                foreach($resultado as $res){
//
//                }
//                $datoCuerpo = $resultado;
//                $datosReporte = $resultado;
//            }
//        }catch (Doctrine_Exception $e){
//            throw $e;
//        }
    }

    public function calcularconciliacion($estructura, $tipoCuenta, $agFechaDesde, $agFechaHasta, $cxn)
    {
        $fechafin = $agFechaHasta; //se obtiene la fecha fin

        $fechainicio = $agFechaDesde; //se obtiene la fecha inicio
        //operaciones para el resumen***************************************/
        $datosresumen = $this->conciliacionBancariaResumen($estructura, $tipoCuenta, $fechainicio, $fechafin, $cxn);
//        print_r($datosresumen);die;
        $ctabancTmp = ''; //almacena la entidad actual del resumen para comparar
        if (count($datosresumen)) {
            foreach ($datosresumen as $k => $v) {//se almacenan en un array las entidades por separado
                if ($k > 0 && $k == count($datosresumen) - 1) {//si es el ultimo elemento se adiciona al array general
                    $datosactuales[] = $v;
                    $datosresumenOrg[] = $datosactuales;
                }
                if ($ctabancTmp != $v['idcuentabancaria']) {//si cambia la entidad se adicionan los datos ya almacenados en $datosactuales
                    $ctabancTmp = $v['idcuentabancaria'];
                    if ($k > 0) {
                        $datosresumenOrg[] = $datosactuales;
                        $datosactuales = array();
                        $datosactuales[] = $v;
                    } else
                        $datosactuales[] = $v;
                }//mientras se mantenga la misma entidad los datos se almacenan en $datosactuales
                else
                    $datosactuales[] = $v;
            }
            $datosresumen = $datosresumenOrg;
        }
        //operaciones para el desglose***************************************/
        $datosdesglose = $this->conciliacionBancariaDesglose($estructura, $tipoCuenta, $fechainicio, $fechafin, $cxn);
        $ctabancTmp = ''; //almacena la entidad actual del desglose para comparar
        $datosactuales = array();
        if (count($datosdesglose)) {
            foreach ($datosdesglose as $k => &$v) {//se almacenan en un array las entidades por separado
                $v['fechacreacion'] = $this->formatFecha($v);
                if ($k > 0 && $k == count($datosdesglose) - 1) {//si es el ultimo elemento se adiciona al array general
                    $datosactuales[] = $v;
                    $datosdesgloseOrg[] = $datosactuales;
                }
                if ($ctabancTmp != $v['idcuentabancaria']) {//si cambia la estructura se adicionan los datos ya almacenados en $datosactuales
                    $ctabancTmp = $v['idcuentabancaria'];
                    if ($k > 0) {
                        $datosdesgloseOrg[] = $datosactuales;
                        $datosactuales = array();
                        $datosactuales[] = $v;
                    } else
                        $datosactuales[] = $v;
                }//mientras se mantenga la misma estructura los datos se almacenan en $datosactuales
                else
                    $datosactuales[] = $v;
            }
            if (count($datosdesgloseOrg) == 0 && count($datosactuales) > 0) {//para caso un solo elemento
                $datosdesgloseOrg[] = $datosactuales;
            }
            $datosdesglose = $datosdesgloseOrg;
        }
        if (count($datosresumen) > 0 && (count($datosresumen) != count($datosdesglose))) {//si no se recupero el desglose de todas las estructuras
            $datosdesgloseTmp = array(); //almacena los desgloses X estructuras, si no esta en el desglose guarda vacio
            /* aqui */ foreach ($datosresumen as $k => $v) {
                /* aqui */ foreach ($datosdesglose as $j => $dd) {
                    /* aqui */ if ($v[0]['idcuentabancaria'] == $dd[0]['idcuentabancaria'])//encontro desglose
                    /* aqui */
                        $datosdesgloseTmp[$k] = $dd;
                    /* aqui */ else if ($k == count($datosdesglose) - 1)
                    /* aqui */
                        $datosdesgloseTmp[$k] = array(); //si no encontro desglose se agrega vacio
                        /* aqui */
                }
                /* aqui */
            }
            $datosdesglose = $datosdesgloseTmp; //se asigna a datosdesglose $datosdesgloseTmp
        }
        $usuario = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
        return array('resumen' => $datosresumen, 'cantidadRes' => count($datosresumen), 'desglose' => $datosdesglose, 'fechainicio' => $fechainicio, 'fechafin' => $fechafin, 'usuario' => $usuario); //RESUMEN=resumen de conciliacion, DESGLOSE = desglose de la conciliacion
    }

    public function formatFecha($agDate)
    {
        $date = explode('-', $agDate);
        return implode('/', array_reverse($date));
    }

    private function conciliacionBancariaResumen($estructura, $tipoCuenta, $agFechaDesde, $agFechaHasta, $cxn)
    {
        try {
            $resultado = $cxn->fetchAll("select * from mod_banco.\"f_conciliacionResumen\"($estructura, $tipoCuenta, '$agFechaDesde', '$agFechaHasta')");
            return $resultado;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    public function conciliacionBancariaDesglose($estructura, $tipoCuenta, $agFechaDesde, $agFechaHasta, $cxn)
    {
        try {
            $resultado = $cxn->fetchAll("select * from mod_banco.\"f_conciliacionbancariaDesg\"($estructura, $tipoCuenta, '$agFechaDesde', '$agFechaHasta')");
            return $resultado;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    public function MovBancarios($argparams, &$datosReporte, &$datoGeneral, &$datoCuerpo, $objEstructura, $agIdReporte)
    {
        $datoGeneral['fechaDesde'] = $argparams['desde'];
        $datoGeneral['fechaHasta'] = $argparams['hasta'];
        $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
        $datoGeneral['reporte'] = $agIdReporte;
        $datoGeneral['descripcionentidad'] = strtoupper($objEstructura->denominacion);
        $datoCuerpo = $this->listMovimientosBancarios($argparams['desde'], $argparams['hasta'], $argparams['cuenta']);
        if (count($datoCuerpo)) {
            foreach ($datoCuerpo as &$v) {
                $v['fechaemision'] = $this->formatFecha($v['fechaemision']);
            }
        }
        return array('datoGeneral' => $datoGeneral, 'datoCuerpo' => $datoCuerpo, 'reporte' => $agIdReporte);
    }

    public function listMovimientosBancarios($fini, $ffin, $ctas)
    {
        $fini = "'$fini'::date";
        $ffin = "'$ffin'::date";
        $filter = " t.fechaemision BETWEEN $fini AND $ffin";
        if (count(json_decode($ctas))) {
            $fctas = $tipoCuenta = implode(',', json_decode($ctas));
            $filter .= " AND t.idcuentabancaria IN ($fctas)";
        }
        $DatTransaccion = new DatTransaccion();
        return $DatTransaccion->getDataTransacciones($this->global->Estructura->idestructura, $filter);
    }

    public function ResumenSaldos($argparams, &$datosReporte, &$datoGeneral, &$datoCuerpo, $objEstructura, $agIdReporte)
    {
        $datoGeneral['fechaHasta'] = $argparams['hasta'];
        $datoGeneral['usuario'] = $this->global->Perfil->nombre . ' ' . $this->global->Perfil->apellidos;
        $datoGeneral['descripcionentidad'] = strtoupper($objEstructura->denominacion);
        $CuentaBancariaModel = new CuentaBancariaModel();
        $ctasBanc = $CuentaBancariaModel->listDataCuentaBancaria();

        if (count($ctasBanc['datos'])) {
            foreach ($ctasBanc['datos'] as $v) {
                $data = array();
                $data['numerocuenta'] = $v['numerocuenta'];
                $data['banco'] = $v['codigo'] . ' ' . $v['nombrebanco'];
                $data['saldolibro'] = $this->getSaldoContables($v['cuentas'], $argparams['hasta']);
                $data['saldobanco'] = $this->getSaldoBanco($v['idcuentabancaria'], $argparams['hasta']);
                $datoCuerpo[] = $data;
            }
        }
        return array('datoGeneral' => $datoGeneral, 'datoCuerpo' => $datoCuerpo, 'reporte' => $agIdReporte);
    }

    public function getSaldoContables($arrCtasContables, $fecha)
    {
        $idestructura = $this->global->Estructura->idestructura;
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $saldoFinal = 0;
        foreach ($arrCtasContables as $v) {
            $saldo = $connection->fetchAll("SELECT * FROM mod_contabilidad.f_saldocuenta (array[" . $v['idcuenta'] . "],'$idestructura', '$fecha') ");
            $saldoFinal += (count($saldo)) ? $saldo[0]['saldocuenta'] : 0;
        }
        return number_format($saldoFinal, 2);
    }

    public function getSaldoBanco($idCtaBancaria, $fecha)
    {
        $WHERE = " c.idcuentabancaria = $idCtaBancaria AND c.fecha < '$fecha' ";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $data = $connection->fetchAll("SELECT c.* FROM mod_banco.dat_conciliacion c
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = c.idcuentabancaria 
                                WHERE $WHERE ORDER BY c.noestadocta DESC");
        return (count($data)) ? number_format($data[0]['saldoctabancaria'], 2) : number_format(0,2);
    }

    /**
     * Da Formto a los saldos del reporte.
     * @param $num
     * @param $keepCero
     * @return Saldo formateado a dos lugares despues de la coma
     */
    public function Format($num, $keepCero = false)
    {
        if ($num == 0) {
            if (!$keepCero) {
                return '';
            } else if ($keepCero) {
                return number_format($num, '2', '.', ',');
            }
        } elseif ($num < 0) {
            return "(" . number_format($num * -1, '2', '.', ',') . ")";
        } else {
            return number_format($num, '2', '.', ',');
        }
    }

    public function getTipoCuentaBanc($post)
    {
        $estructura = $this->global->Estructura->idestructura;

        $conditions = array('idestructura = ?');
        $params = array($estructura);

        if (!empty($post['query'])) {
            $conditions[] = 'descripcion ILIKE \'%' . $post['query'] . '%\'';
        }
        $res = NomTipocuentabancaria::GetByCondition(implode(' AND ', $conditions), $params, TRUE);
        return $res;
    }

    public function getCuentaBancaria()
    {
        $NomCuentabancaria = new NomCuentabancaria();
        return $NomCuentabancaria->getDataCuentaBancaria($this->global->Estructura->idestructura);
    }

}
