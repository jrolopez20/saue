<?php

/**
 * Clase controladora para gestionar los Reportes de contabilidad.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ReportesController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new ReportesModel();
    }

    public function reportesAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadDataTipoReporteAction()
    {
        echo json_encode($this->model->loadDataTipoReporte());
    }

    /**
     * call this model action
     */
    public function loadDataFechaAction()
    {
        echo json_encode($this->model->loadDataFecha());
    }

    /**
     * call this model action
     */
    public function loadDataEjerciciosAction()
    {
        echo json_encode($this->model->loadDataEjercicios());
    }

    /**
     * call this model action
     */
    public function loadDataPeriodosAction()
    {
        echo json_encode($this->model->loadDataPeriodos($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataCuentasAction()
    {
        echo json_encode($this->model->loadDataCuentas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataNivelesAction()
    {
        echo json_encode($this->model->loadDataNiveles());
    }

    /**
     * call this model action
     */
    public function loadDataBalanceAction()
    {
        echo json_encode($this->model->loadDataBalance());
    }

    /**
     * call this model action
     */
    public function loadDataReportAction()
    {
        echo json_encode($this->model->loadDataReport($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function getTipoCuentaBancAction()
    {
        echo json_encode($this->model->getTipoCuentaBanc($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function getCuentaBancariaAction()
    {
        echo json_encode($this->model->getCuentaBancaria());
    }

}
