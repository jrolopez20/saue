<?php

class NomTipocuentabancaria extends BaseNomTipocuentabancaria
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasMany ('NomCuentabancaria', array ('local' => 'idtipocuentabancaria', 'foreign' => 'idtipocuentabancaria'));
    }

    public function findById($idtipocuentabancaria)
    {
        return Doctrine::getTable('NomTipocuentabancaria')->find($idtipocuentabancaria);
    }

    public function getTipoCuentaBancaria($limit = null, $start = null, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idtipocuentabancaria, codigo, descripcion, idestructura')
            ->from('NomTipocuentabancaria');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if($k > 0){
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }
        $datos = $query->fetchArray();

        foreach($datos as &$dato){
            $integrator = ZendExt_IoC::getInstance();
            $estructura = $integrator->metadatos->DameEstructura($dato['idestructura']);
            $dato['estructura'] = $estructura[0]->denominacion;
        }
        return $datos;
    }

    public function getCantTipoCuentaBancaria($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idtipocuentabancaria) as cantidad')
            ->from('NomTipocuentabancaria');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if($k > 0){
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }
        return $query->fetchOne()->cantidad;
    }

   public function GetById($idtipocuentabancaria, $toArray = false)
    {
        try {
            $result = $this->findById($idtipocuentabancaria);
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }


    static public function GetByCondition($condition, $params, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomTipocuentabancaria')
                ->where("$condition", $params)
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

}