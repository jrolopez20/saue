<?php

/**
 * Clase dominio para configurar las cuentas bancarias.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomCuentabancaria extends BaseNomCuentabancaria
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomTipocuentabancaria', array('local' => 'idtipocuentabancaria', 'foreign' => 'idtipocuentabancaria'));
        $this->hasMany('DatCuentacontable', array('local' => 'idcuentabancaria', 'foreign' => 'idcuentabancaria'));
    }

    /**
     * Lista los datos de las cuentas bancarias de la entidad.
     *
     * @param Integer $idestructura
     * @param Array $filter
     * @param Integer $start
     * @param Integer $limit
     * @return Array datos de las cuentas bancarias de la entidad
     */
    public function getDataCuentaBancaria($idestructura, $filter = null, $start = null, $limit = null)
    {
        $SELECT = "cb.idcuentabancaria, cb.numerocuenta, cb.nombre, cb.fechaapertura, cb.idtipocuentabancaria, "
            . "cb.idestructura, cb.saldo, cb.saldoinicial, (cb.numerocuenta ||' '|| cb.nombre) as cuenta, ns.idsucursal, ns.numero AS numerosucursal, ns.direccion, "
            . "nb.idbanco, nb.codigo, nb.nombre AS nombreBanco, nb.abreviatura, tc.descripcion as tipocuenta";
        $WHERE = " cb.idestructura = $idestructura ";
        if ($filter != '' && !is_null($filter)) {
            $WHERE .= " AND $filter";
        }

        $pager = "";
        if (!is_null($start)) {
            $pager .= " OFFSET $start";
        }
        if (!is_null($limit)) {
            $pager .= " LIMIT $limit";
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.nom_cuentabancaria cb
                                INNER JOIN mod_maestro.nom_sucursal ns ON ns.idsucursal = cb.idsucursal 
                                INNER JOIN mod_maestro.nom_banco nb ON nb.idbanco = ns.idbanco 
                                INNER JOIN mod_banco.nom_tipocuentabancaria tc ON tc.idtipocuentabancaria = cb.idtipocuentabancaria 
                                WHERE $WHERE
                                ORDER BY cb.numerocuenta, cb.nombre $pager");
    }

    /**
     * Obtiene el total de las cuentas bancarias de la entidad.
     *
     * @param Integer $idestructura
     * @param Array $filter
     * @return Integer total de las cuentas bancarias de la entidad
     */
    public function getTotalCuentaBancaria($idestructura, $filter)
    {
        $SELECT = "cb.*";
        $WHERE = " cb.idestructura = $idestructura ";
        if ($filter != '') {
            $WHERE .= " AND $filter";
        }
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();

        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.nom_cuentabancaria cb
                                WHERE $WHERE");
    }

}
