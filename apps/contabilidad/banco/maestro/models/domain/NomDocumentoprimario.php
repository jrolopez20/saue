<?php

class NomDocumentoprimario extends BaseNomDocumentoprimario
{

    public function setUp()
    {
        parent :: setUp ();
    }

    public function findById($iddocumentoprimario)
    {
        return Doctrine::getTable('NomDocumentoprimario')->find($iddocumentoprimario);
    }

    public function getDocumentoPrimario($limit, $start, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('iddocumentoprimario, codigo, denominacion, sigla, idsubsistema')
            ->from('NomDocumentoprimario');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if($k > 0){
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }
        $datos = $query->fetchArray();
        return $datos;
    }

    public function getCantDocumentoPrimario($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(iddocumentoprimario) as cantidad')
            ->from('NomDocumentoprimario');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if($k > 0){
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }
        return $query->fetchOne()->cantidad;
    }

}