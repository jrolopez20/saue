<?php

abstract class BaseNomCuentabancaria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.nom_cuentabancaria');
        $this->hasColumn('idcuentabancaria', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_nomcuentabancaria'));
        $this->hasColumn('numerocuenta', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fechaapertura', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idsucursal', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idtipocuentabancaria', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fechaserver', 'timestamp without time zone', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('saldo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('saldoinicial', 'numeric', null, array ('notnull' => true,'primary' => false));
    }


}