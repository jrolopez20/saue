<?php

abstract class BaseNomTipocuentabancaria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.nom_tipocuentabancaria');
        $this->hasColumn('idtipocuentabancaria', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_tipocuenta'));
        $this->hasColumn('descripcion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => false,'primary' => false));
    }

}