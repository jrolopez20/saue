<?php

abstract class BaseDatCuentacontable extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.dat_cuentacontable');
        $this->hasColumn('idcuentacontable', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_cuentacontable'));
        $this->hasColumn('idcuenta', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idcuentabancaria', 'numeric', null, array ('notnull' => true,'primary' => false));
    }


}