<?php

abstract class BaseNomDocumentoprimario extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_documentoprimario');
        $this->hasColumn('iddocumentoprimario', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_documentoprimario'));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('sigla', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}