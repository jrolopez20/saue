<?php

class NomDocumentoprimarioModel extends ZendExt_Model
{
    public function NomDocumentoprimarioModel()
    {
        parent::ZendExt_Model();
    }


    public function updateDocumentoPrimario($iddocumentoprimario = null, $codigo = '', $denominacion = '', $sigla = '')
    {
        try {
            $objDomain = new NomDocumentoprimario();
            if (!empty($iddocumentoprimario)) {
                $objDomain = $objDomain->findById($iddocumentoprimario);
            } else {
                $objDomain->idsubsistema = 12;
            }
            $objDomain->codigo = $codigo;
            $objDomain->denominacion = $denominacion;
            $objDomain->sigla = $sigla;
            $objDomain->save();

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgDBGuardado}";
        } catch (Exception $e) {
            if ($e->getCode() == 23505) { //Code for uniqueConstrainViolation
                return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgDBCodigoEnUso}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public
    function deleteDocumentoPrimario($iddocumentoprimario)
    {
        try {
            $objDomain = new NomDocumentoprimario();
            $objContenido = $objDomain->findById($iddocumentoprimario);
            $objContenido->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgDBEliminado}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgDBEnUso}";
            } else {
                return $e->getMessage();
            }
        }
    }

}

