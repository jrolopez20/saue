<?php

class NomTipocuentabancariaModel extends ZendExt_Model
{
    public function NomTipocuentabancariaModel()
    {
        parent::ZendExt_Model();
    }


    public function updateTipoCuentaBancaria($idtipocuentabancaria = null, $codigo = '', $descripcion = '')
    {
        try {
            $objDomain = new NomTipocuentabancaria();
            if (!empty($idtipocuentabancaria)) {
                $objDomain = $objDomain->findById($idtipocuentabancaria);
            }else{
                $objDomain->idestructura = $this->global->Estructura->idestructura;
            }
            $objDomain->codigo = $codigo;
            $objDomain->descripcion = $descripcion;
            $objDomain->save();

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgTCBGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function deleteTipoCuentaBancaria($idtipocuentabancaria)
    {
        try {
            $objDomain = new NomTipocuentabancaria();
            $objContenido = $objDomain->findById($idtipocuentabancaria);
            $objContenido->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgTCBEliminado}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgTCBEnUso}";
            } else {
                return $e->getMessage();
            }
        }
    }
    
}