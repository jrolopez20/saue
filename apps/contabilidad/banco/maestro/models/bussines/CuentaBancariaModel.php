<?php

/**
 * Clase modelo para gestionar las cuentas bancarias.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CuentaBancariaModel extends ZendExt_Model
{

    public function CuentaBancariaModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Lista los datos de los Pases.
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de los Pases
     */
    public function listDataCuentaBancaria($argparams = null)
    {
        $cuentabancaria = new NomCuentabancaria();
        $filter = $this->defineFilters($argparams['filtros']);
        $dataCuenta = $cuentabancaria->getDataCuentaBancaria($this->global->Estructura->idestructura, $filter, $argparams['start'], $argparams['limit']);
        $this->getDataCtasContables($dataCuenta);
        $total = $cuentabancaria->getTotalCuentaBancaria($this->global->Estructura->idestructura, $filter);
        return array('datos' => $dataCuenta, 'cant' => count($total));
    }

    /**
     * Guarda los datos asociados a la cuenta bancaria.
     * @param Array $argparams (start, limit, filter)
     * @return boolean true if ocurred, false if failure
     */
    public function updateCuentaBancaria($argparams)
    {

        $objCtaBancaria = new stdClass();
        $ctasContables = array($argparams['idcuenta']);

        $objCtaBancaria->numerocuenta = $argparams['numerocuenta'];
        $objCtaBancaria->nombre = $argparams['nombre'];
        $objCtaBancaria->fechaapertura = $argparams['fechaapertura'];
        $objCtaBancaria->idsucursal = $argparams['idsucursal'];
        $objCtaBancaria->idtipocuentabancaria = $argparams['idtipocuentabancaria'];
        $objCtaBancaria->saldo = $argparams['saldo'];
        $objCtaBancaria->saldoinicial = $argparams['saldo'];
        $objCtaBancaria->idestructura = $this->global->Estructura->idestructura;
        $objCtaBancaria->idcuentabancaria = $argparams['idcuentabancaria'];

        if ($argparams['idcuentabancaria']) {
            return $this->Actualizar($objCtaBancaria, $ctasContables);
        } else {
            return $this->Insertar($objCtaBancaria, $ctasContables);
        }
    }

//    public function updateCuentaBancaria($argparams)
//    {
//
//        $objCtaBancaria = new stdClass();
//        $ctasContables = json_decode($argparams['cuentas']);
//
//        $objCtaBancaria->numerocuenta = $argparams['numerocuenta'];
//        $objCtaBancaria->nombre = $argparams['nombre'];
//        $objCtaBancaria->fechaapertura = $argparams['fechaapertura'];
//        $objCtaBancaria->idsucursal = $argparams['idsucursal'];
//        $objCtaBancaria->idtipocuentabancaria = $argparams['idtipocuentabancaria'];
//        $objCtaBancaria->saldo = $argparams['saldo'];
//        $objCtaBancaria->saldoinicial = $argparams['saldo'];
//        $objCtaBancaria->idestructura = $this->global->Estructura->idestructura;
//        $objCtaBancaria->idcuentabancaria = $argparams['idcuentabancaria'];
//
//        if ($argparams['idcuentabancaria']) {
//            return $this->Actualizar($objCtaBancaria, $ctasContables);
//        } else {
//            return $this->Insertar($objCtaBancaria, $ctasContables);
//        }
//    }

    /**
     * Adiciona la Cuenta bancaria.
     * @param stdClass $Ctabanc 
     * @param Array $ctasCont
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($Ctabanc, $ctasCont)
    {
        try {
            $query = "INSERT INTO mod_banco.nom_cuentabancaria(numerocuenta,nombre,fechaapertura,idsucursal,idtipocuentabancaria,idestructura,saldo,saldoinicial) VALUES ";
            $query .= "('" . $Ctabanc->numerocuenta . "','" . $Ctabanc->nombre . "','" . $Ctabanc->fechaapertura . "'," . $Ctabanc->idsucursal . "," . $Ctabanc->idtipocuentabancaria . "," . $Ctabanc->idestructura . "," . $Ctabanc->saldo . "," . $Ctabanc->saldoinicial . ")";
            $query .= " RETURNING idcuentabancaria";

            $conn = Zend_Registry::get('conexion');
            $execute = $conn->execute($query);
            $idbancaria = $execute->fetchColumn(0);

            return $this->actionInsertarContables($idbancaria, $ctasCont);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Adiciona la Cuenta bancaria.
     * @param stdClass $Ctabanc 
     * @param Array $ctasCont
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($Ctabanc, $ctasCont)
    {
        try {
            $query = "UPDATE mod_banco.nom_cuentabancaria SET numerocuenta = '" . $Ctabanc->numerocuenta . "', nombre = '" . $Ctabanc->nombre . "', "
                    . "fechaapertura = '" . $Ctabanc->fechaapertura . "', idsucursal = " . $Ctabanc->idsucursal . ", idtipocuentabancaria = " . $Ctabanc->idtipocuentabancaria . ", "
                    . "idestructura = " . $Ctabanc->idestructura . ", saldo = " . $Ctabanc->saldo . ",saldoinicial = " . $Ctabanc->saldoinicial;
            $query .= " WHERE idcuentabancaria = " . $Ctabanc->idcuentabancaria;

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            $this->actionEliminarContables($Ctabanc->idcuentabancaria);
            return $this->actionInsertarContables($Ctabanc->idcuentabancaria, $ctasCont);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Adiciona las Ctas contables asociadas a la bancaria.
     * @param Integer $idbancaria 
     * @param Array $ctasCont 
     * @return boolean true if ocurred, false if failure
     */
    public function actionInsertarContables($idbancaria, $ctasCont)
    {
        try {
            foreach ($ctasCont as $v) {
                $this->insertarCtasCont($v, $idbancaria);
            }
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Adiciona la Cta contable asociada a la bancaria.
     * @param Integer $idbancaria 
     * @param Array $ctasCont 
     * @return boolean true if ocurred, false if failure
     */
    public function insertarCtasCont($idctacont, $idbancaria)
    {
        try {
            $query = "INSERT INTO mod_banco.dat_cuentacontable (idcuenta,idcuentabancaria) VALUES ";
            $query .= "(" . $idctacont . "," . $idbancaria . ")";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Elimina lss Ctas contables asociadas a la bancaria.
     * @param Integer $idbancaria
     * @return void 
     */
    public function actionEliminarContables($idbancaria)
    {
        try {
            $query = "DELETE FROM mod_banco.dat_cuentacontable WHERE idcuentabancaria in (" . $idbancaria . ")";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
            return;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Elimina lss Ctas contables asociadas a la bancaria.
     * @param Integer $idbancaria
     * @return boolean true if ocurred, false if failure
     */
    public function deleteCuentaBancaria($idbancaria)
    {
        try {
            $query = "DELETE FROM mod_banco.nom_cuentabancaria WHERE idcuentabancaria in (" . $idbancaria . ")";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminada}";
        } catch (Exception $exc) {
            return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgUso}";
        }
    }

    /**
     * Agrega las Ctas contables asociadas a las ctas bancarias al listado de ctas bancarias.
     * @param Array $arrCtasBanc
     * @return Array listado de Ctas contables asociadas a las ctas bancarias
     */
    public function getDataCtasContables(&$arrCtasBanc)
    {
        foreach ($arrCtasBanc as &$v) {
            $v['cuentas'] = $this->getCtasContables($v['idcuentabancaria']);
            $v['uso'] = $this->findUseCtaBancaria($v['idcuentabancaria']);
            $saldolibro = 0;
            foreach ($v['cuentas'] as $m) {
                $saldolibro += $m['saldocuenta'];
            }
            $v['saldolibro'] = number_format($saldolibro, 2);
        }
    }

    /**
     * Obtiene las Ctas contables asociadas a la bancaria.
     * @param Integer $idcuentabancaria
     * @return Array listado de Ctas contables asociadas a la cta bancaria
     */
    public function getCtasContables($idcuentabancaria)
    {
        $SELECT = "cc.*";
        $WHERE = " cc.idcuentabancaria = $idcuentabancaria ";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $ctas = $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_cuentacontable cc WHERE $WHERE");
        $this->getSaldoCuenta($ctas);
        return $ctas;
    }

    /**
     * Asocia el saldo a las cuentas las Cuentas.
     * @param Array $arrctas
     * @return Array saldo de las Cuentas
     */
    public function getSaldoCuenta(&$arrctas)
    {
        $fecha = false;
        $idestructura = $this->global->Estructura->idestructura;
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        foreach ($arrctas as &$v) {
            if (!$fecha) {
                $f = $this->integrator->contabilidad->getFechaSubsistema($idestructura, 12);
                $fecha = $f[0]['fecha'];
            }
            $saldo = $connection->fetchAll("SELECT * FROM mod_contabilidad.f_saldocuenta (array[" . $v['idcuenta'] . "],'$idestructura', '$fecha') ");
            $v['saldocuenta'] = (count($saldo)) ? $saldo[0]['saldocuenta'] : 0;
            $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($v['idcuenta']));
            $v['codigo'] = $dataCta[0]['codigo'];
            $v['concatcta'] = $dataCta[0]['concat'];
            $v['denominacion'] = $dataCta[0]['denominacion'];
            $v['text'] = $dataCta[0]['text'];
        }
    }

    /**
     * Busca los usos de la Cta bancaria.
     * @param Integer $idcuentabancaria
     * @return boolean true if ocurred, false if failure
     */
    public function findUseCtaBancaria($idcuentabancaria)
    {
        $SELECT = "t.idtransaccion";
        $WHERE = " t.idcuentabancaria = $idcuentabancaria ";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $transacciones = $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_transaccion t WHERE $WHERE");
        return (count($transacciones)) ? true : false;
    }

    /**
     * procesa los filtros y devuelve la sentencia SQL.
     * @param Array $filter
     * @return String cadena SQL
     */
    public function defineFilters($filter)
    {
        $filter = json_decode($filter);
        $data_return = "";
        if (count($filter)) {
            foreach ($filter as $k => $v) {
                $data_return .= ($k == 0 || $data_return == '') ? "cb." . $v->property . " ILIKE '%" . $v->value . "%'" : " OR cb." . $v->property . " ILIKE '%" . $v->value . "%'";
            }
        }
        return $data_return;
    }

}
