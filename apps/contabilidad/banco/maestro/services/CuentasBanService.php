<?php

class CuentasBanService
{

    /**
     * Obtiene las Cuentas bancarias.
     *
     * @param int $idestructura Identificador de la entidad
     * @return Array Cuentas bancarias
     */
    public function getCuentas($idestructura)
    {
        if(!is_numeric($idestructura)){
            return -1;
        }
        $domain = new NomCuentabancaria();
        return $domain->getDataCuentaBancaria($idestructura);
    }

}
