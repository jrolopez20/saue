<?php

/**
 * Clase controladora para gestionar las cuentas bancarias.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CuentabancariaController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new CuentaBancariaModel();
    }

    public function cuentabancariaAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadCuentaBancariaAction()
    {
        echo json_encode($this->model->listDataCuentaBancaria($this->_request->getPost()));
    }

    public function loadTipoCuentaBancariaAction()
    {
        $objDomain = new NomTipocuentabancaria();
        echo json_encode(array('datos' => $objDomain->getTipoCuentaBancaria()));
    }

    /**
     * call service action
     */
    public function loadBancosAction()
    {
        echo json_encode(array('datos' => $this->integrator->configuracion->getBancos()));
    }

    /**
     * call service action
     */
    public function loadSucursalesAction()
    {
        echo json_encode(array('datos' => $this->integrator->configuracion->getSucursales($this->_request->getPost('idbanco'))));
    }

    /**
     * call this model action
     */
    public function updateCuentaBancariaAction()
    {
        echo $this->model->updateCuentaBancaria($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteCuentaBancariaAction()
    {
        echo $this->model->deleteCuentaBancaria($this->_request->getPost('idcuentabancaria'));
    }

}
