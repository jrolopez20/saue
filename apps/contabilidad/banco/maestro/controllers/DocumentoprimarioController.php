<?php

/**
 * Clase controladora para gestionar el nomenclador de tipo de cuenta bancaria
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DocumentoprimarioController extends ZendExt_Controller_Secure
{
    public function init()
    {
        parent::init();
    }

    public function documentoprimarioAction()
    {
        $this->render();
    }

    public function loadDocumentoPrimarioAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filters = json_decode($this->_request->getPost('filtros'));

        $objDomain = new NomDocumentoprimario();
        $r['datos'] = $objDomain->getDocumentoprimario($limit, $start, $filters);
        $r['cantidad'] = $objDomain->getCantDocumentoPrimario($filters);
        echo json_encode($r);
    }

    public function updateDocumentoPrimarioAction()
    {
        $iddocumentoprimario = $this->_request->getPost('iddocumentoprimario');
        $codigo = $this->_request->getPost('codigo');
        $sigla = $this->_request->getPost('sigla');
        $denominacion = $this->_request->getPost('denominacion');

        $objModel = new NomDocumentoprimarioModel();
        $r = $objModel->updateDocumentoPrimario($iddocumentoprimario, $codigo, $denominacion, $sigla);
        echo $r;
    }


    public function deleteDocumentoPrimarioAction()
    {
        $iddocumentoprimario = $this->_request->getPost('iddocumentoprimario');

        $objModel = new NomDocumentoprimarioModel();
        $r = $objModel->deleteDocumentoPrimario($iddocumentoprimario);
        echo $r;
    }

}