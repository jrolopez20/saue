<?php

/**
 * Clase controladora para gestionar el nomenclador de tipo de cuenta bancaria
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TipocuentabancariaController extends ZendExt_Controller_Secure
{
    public function init()
    {
        parent::init();
    }

    public function tipocuentabancariaAction()
    {
        $this->render();
    }

    public function loadTipoCuentaBancariaAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filters = json_decode($this->_request->getPost('filtros'));

        $objDomain = new NomTipocuentabancaria();
        $r['datos'] = $objDomain->getTipoCuentaBancaria($limit, $start, $filters);
        $r['cantidad'] = $objDomain->getCantTipoCuentaBancaria($filters);
        echo json_encode($r);
    }

    public function updateTipoCuentaBancariaAction()
    {
        $idtipocuentabancaria = $this->_request->getPost('idtipocuentabancaria');
        $codigo = $this->_request->getPost('codigo');
        $descripcion = $this->_request->getPost('descripcion');

        $objModel = new NomTipocuentabancariaModel();
        $r = $objModel->updateTipoCuentaBancaria($idtipocuentabancaria, $codigo, $descripcion);
        echo $r;
    }


    public function deleteTipoCuentaBancariaAction()
    {
        $idtipocuentabancaria = $this->_request->getPost('idtipocuentabancaria');

        $objModel = new NomTipocuentabancariaModel();
        $r = $objModel->deleteTipoCuentaBancaria($idtipocuentabancaria);
        echo $r;
    }

}