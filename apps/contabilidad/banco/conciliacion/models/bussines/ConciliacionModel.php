<?php

/**
 * Clase modelo para gestionar las cuentas bancarias.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConciliacionModel extends ZendExt_Model
{

    public function ConciliacionModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Listado de conciliaciones registradas.
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de conciliaciones
     */
    public function listDataConciliaciones($argparams)
    {
        $datConciliacion = new DatConciliacion();
        $filter = $this->defineFilters($argparams['filtros']);
        $dataTrans = $datConciliacion->getDataConciliaciones($this->global->Estructura->idestructura, $filter, $argparams['start'], $argparams['limit']);
        $total = $datConciliacion->getTotalConciliaciones($this->global->Estructura->idestructura, $filter);
        return array('datos' => $dataTrans, 'cant' => $total);
    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $datConciliacion = new DatConciliacion();
        $periodo = $datConciliacion->obtenerPeriodo($this->global->Estructura->idestructura, 12);
        $fecha = $datConciliacion->obtenerFecha($this->global->Estructura->idestructura, 12);
        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Obtiene las cuentas bancarias registradas.
     * @param none
     * @return Array Listado de cuentas bancarias
     */
    public function getCuentasBancarias()
    {
        $ctaBancariaModel = new CuentaBancariaModel();
        $ctas = $ctaBancariaModel->listDataCuentaBancaria();
        return $ctas['datos'];
    }

    /**
     * Obtiene las transacciones no conciliadas.
     * @param none
     * @return Array transacciones no conciliadas
     */
    public function getTransaccionesNoConciliadas($argparams)
    {
        $datConciliacion = new DatConciliacion();
        return array('datos' => $datConciliacion->getTransaccionesNoConciliadas($this->global->Estructura->idestructura, $argparams['idcuentabancaria']));
    }

    /**
     * Guarda los datos asociados a la transaccion.
     * @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function updateConciliacion($argparams)
    {

        $objConciliacion = new stdClass();
        $dataDif = json_decode($argparams['diferencias']);
        $transacciones = json_decode($argparams['transacciones']);
        $diferencias = ($dataDif->tipodiferencia == 'registrar') ? $dataDif : null;
        $correcciones = ($dataDif->tipodiferencia == 'corregir') ? $dataDif : null;

        $objConciliacion->fecha = $argparams['fecha'];
        $objConciliacion->noestadocta = $argparams['noestadocta'];
        $objConciliacion->saldoestadocta = $argparams['saldoestadocta'];
        $objConciliacion->saldoctabancaria = $argparams['saldoctabancaria'];
        $objConciliacion->conciliada = $argparams['conciliada'];
        $objConciliacion->idcuentabancaria = $argparams['idcuentabancaria'];
        $objConciliacion->idestructura = $this->global->Estructura->idestructura;

        return $this->Insertar($objConciliacion, $transacciones, $diferencias, $correcciones);
    }

    /**
     * Adiciona la conciliacion.
     * @param stdClass $objConc 
     * @param Array $transacciones
     * @param Array $diferencias
     * @param Array $correcciones
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($objConc, $transacciones, $diferencias, $correcciones)
    {
        try {
            $query = "INSERT INTO mod_banco.dat_conciliacion(fecha, noestadocta, saldoestadocta, saldoctabancaria, conciliada, "
                    . "idcuentabancaria, idestructura) VALUES ";
            $query .= "('" . $objConc->fecha . "','" . $objConc->noestadocta . "'," . $objConc->saldoestadocta . ","
                    . $objConc->saldoctabancaria . "," . $objConc->conciliada . "," . $objConc->idcuentabancaria . ","
                    . $objConc->idestructura . ")";
            $query .= " RETURNING idconciliacion";

            $conn = Zend_Registry::get('conexion');
            $execute = $conn->execute($query);
            $idconciliacion = $execute->fetchColumn(0);
            if (count($diferencias)) {
                $this->registrarDiferencias($idconciliacion, $diferencias);
            } else if (count($correcciones)) {
                $this->registrarCorrecciones($idconciliacion, $correcciones);
            }
            $this->actualizarSaldoCtaBancaria($objConc->idcuentabancaria, $objConc->saldoctabancaria);
            return $this->actionInsertarTransacciones($idconciliacion, $transacciones);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Asocia la conciliacion a la transaccion.
     * @param Integer $idconciliacion 
     * @param Array $transacciones 
     * @return boolean true if ocurred, false if failure
     */
    public function actionInsertarTransacciones($idconciliacion, $transacciones)
    {
        try {
            foreach ($transacciones as $v) {
                $this->conciliarTransacciones($idconciliacion, $v);
            }
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Asocia la conciliacion a la transaccion y actualiza el estado a Conciliado.
     * @param Integer $idconciliacion 
     * @param Array $transaccion
     * @return void
     */
    public function conciliarTransacciones($idconciliacion, $transaccion)
    {
        try {
            $conn = Zend_Registry::get('conexion');

            $query = "INSERT INTO mod_banco.dat_conctransaccion (idconciliacion, idtransaccion) VALUES ";
            $query .= "($idconciliacion, $transaccion)";
            $conn->execute($query);

            $query1 = "UPDATE mod_banco.dat_transaccion SET idestado = 3 WHERE idtransaccion = " . $transaccion;
            $conn->execute($query1);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Asocia la diferencia asociada a la conciliacion.
     * @param Integer $idconciliacion 
     * @param Array $correccion
     * @return void
     */
    public function registrarCorrecciones($idconciliacion, $correccion)
    {
        try {
            $descripcion = $correccion->descripcion;
            $saldo = ($correccion->diferenciadebito) ? $correccion->diferenciadebito * -1 : $correccion->diferenciacredito;
            $conn = Zend_Registry::get('conexion');

            $query = "INSERT INTO mod_banco.his_correccion (idconciliacion, saldo, descripcion) VALUES ";
            $query .= "($idconciliacion, $saldo, '$descripcion')";
            $conn->execute($query);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Asocia la diferencia asociada a la conciliacion.
     * @param Integer $idconciliacion 
     * @param Array $diferencia
     * @return void
     */
    public function registrarDiferencias($idconciliacion, $diferencia)
    {
        try {

            $descripcion = $diferencia->descripcion;
            $saldo = ($diferencia->diferenciadebito) ? $diferencia->diferenciadebito * -1 : $diferencia->diferenciacredito;


            // esto es una cable
            $saldo = 0;

            $conn = Zend_Registry::get('conexion');

            $query = "INSERT INTO mod_banco.his_diferencia (idconciliacion, saldo, descripcion) VALUES ";
            $query .= "($idconciliacion, $saldo, '$descripcion')";

            $conn->execute($query);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Actualiza el saldo de la cuenta bancaria.
     * @param Integer $idcuentabancaria 
     * @param Integer $saldo
     * @return void
     */
    public function actualizarSaldoCtaBancaria($idcuentabancaria, $saldo)
    {
        try {
            $query = "UPDATE mod_banco.nom_cuentabancaria SET saldo = $saldo";
            $query .= " WHERE idcuentabancaria = $idcuentabancaria";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return;
        }
    }

    /**
     * Obtiene la diferencia acumulada de la cuenta bancaria.
     * @param Integer $idcuentabancaria 
     * @return Integer
     */
    public function getDiferenciaAcumulada($idcuentabancaria)
    {
        $datConciliacion = new DatConciliacion();
        $dif = $datConciliacion->getDiferenciaAcumulada($idcuentabancaria);
        return ($dif[0]['sum']) ? $dif[0]['sum'] : 0;
    }

    /**
     * procesa los filtros y devuelve la sentencia SQL.
     * @param Array $filter
     * @return String cadena SQL
     */
    public function defineFilters($filter)
    {
        $filter = json_decode($filter);
        $data_return = "";
        if (count($filter)) {
            foreach ($filter as $k => $v) {
                $data_return .= ($k == 0 || $data_return == '') ? "c." . $v->property . " ILIKE '%" . $v->value . "%'" : " OR c." . $v->property . " ILIKE '%" . $v->value . "%'";
            }
        }
        return $data_return;
    }

}
