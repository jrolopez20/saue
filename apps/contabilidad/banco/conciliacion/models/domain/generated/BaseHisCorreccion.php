<?php

abstract class BaseHisCorreccion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.his_correccion');
        $this->hasColumn('idcorreccion', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_correccion'));
        $this->hasColumn('idconciliacion', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('saldo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}
