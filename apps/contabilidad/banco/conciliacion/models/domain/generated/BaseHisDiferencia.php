<?php

abstract class BaseHisDiferencia extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.his_diferencia');
        $this->hasColumn('iddiferencia', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_diferencia'));
        $this->hasColumn('idconciliacion', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('saldo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}
