<?php

abstract class BaseDatConciliacion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.dat_conciliacion');
        $this->hasColumn('idconciliacion', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_conciliacion'));
        $this->hasColumn('fecha', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('noestadocta', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('saldoestadocta', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('saldoctabancaria', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('conciliada', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcuentabancaria', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idesructura', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
