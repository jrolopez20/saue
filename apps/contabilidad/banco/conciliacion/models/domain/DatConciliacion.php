<?php

/**
 * Clase dominio para gestionar las conciliaciones bancarias.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatConciliacion extends BaseDatConciliacion
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('HisDiferencia', array('local' => 'idconciliacion', 'foreign' => 'idconciliacion'));
        $this->hasMany('HisCorreccion', array('local' => 'idconciliacion', 'foreign' => 'idconciliacion'));
    }

    /**
     * Lista los datos de las cuentas bancarias de la entidad.
     * @param Integer $idestructura
     * @param Array $filter
     * @param Integer $start
     * @param Integer $limit
     * @return Array datos de las cuentas bancarias de la entidad
     */
    public function getDataConciliaciones($idestructura, $filter, $start, $limit)
    {
        $SELECT = "c.*, cb.numerocuenta, cb.nombre, (cb.numerocuenta ||' '|| cb.nombre) as cuenta, cb.saldo, "
                . "hc.idcorreccion, hc.saldo as saldocorreccion, hc.descripcion as descripcorreccion, "
                . "hd.iddiferencia, hd.saldo as saldodiferencia, hd.descripcion as descripdiferencia";
        $WHERE = " c.idestructura = $idestructura ";
        if ($filter != '') {
            $WHERE .=" AND $filter";
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_conciliacion c
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = c.idcuentabancaria 
                                LEFT JOIN mod_banco.his_correccion hc ON hc.idconciliacion = c.idconciliacion 
                                LEFT JOIN mod_banco.his_diferencia hd ON hd.idconciliacion = c.idconciliacion 
                                WHERE $WHERE
                                ORDER BY c.noestadocta, c.fecha OFFSET $start LIMIT $limit");
    }

    /**
     * Obtiene el total de las cuentas bancarias de la entidad.
     * @param Integer $idestructura
     * @param Array $filter
     * @return Integer total de las cuentas bancarias de la entidad
     */
    public function getTotalConciliaciones($idestructura, $filter)
    {
        $WHERE = " c.idestructura = $idestructura ";
        if ($filter != '') {
            $WHERE .=" AND $filter";
        }
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT c.* FROM mod_banco.dat_conciliacion c WHERE $WHERE");
    }

    /**
     * Carga los datos del Periodo actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Periodo actual del subsistema
     */
    public function obtenerPeriodo($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT p.* FROM mod_maestro.dat_estructurasubsist es "
                        . "INNER JOIN mod_maestro.dat_cierre c ON c.idestructurasubsist = es.idestructurasubsist "
                        . "INNER JOIN mod_maestro.dat_periodocontable p ON p.idperiodo = c.idperiodoactual "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Carga la Fecha actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Fecha actual del subsistema
     */
    public function obtenerFecha($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT f.* FROM mod_maestro.dat_fecha f "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON es.idestructurasubsist = f.idestructurasubsist "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Lista los datos de las transacciones de la entidad.
     * @param Integer $idestructura
     * @param Array $filter
     * @param Integer $start
     * @param Integer $limit
     * @return Array datos de las transacciones de la entidad
     */
    public function getTransaccionesNoConciliadas($idestructura, $idcuentabancaria)
    {
        $SELECT = "t.idtransaccion, t.tipotransaccion, t.numero, t.fechaemision, t.fecharecibo, "
                . "t.idcuentabancaria, t.saldo, t.idbeneficiario, t.recibido, t.concepto, t.observacion, "
                . "t.idestado, t.nocomprobante, t.idestructura, cb.numerocuenta, cb.nombre, "
                . "(cb.numerocuenta ||' '|| cb.nombre) as cuenta, e.denominacion as estado, cp.nombre as beneficiario";

        $WHERE = " t.idestructura = $idestructura AND t.idestado = 2 AND t.idcuentabancaria = $idcuentabancaria";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_transaccion t
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = t.idcuentabancaria 
                                INNER JOIN mod_banco.nom_estado e ON e.idestado = t.idestado 
                                INNER JOIN mod_maestro.nom_clientesproveedores cp ON cp.idclientesproveedores = t.idbeneficiario 
                                WHERE $WHERE
                                ORDER BY t.numero, t.tipotransaccion");
    }

    /**
     * Obtiene la diferencia acumulada de la cuenta bancaria.
     * @param Integer $idcuentabancaria 
     * @return Integer
     */
    public function getDiferenciaAcumulada($idcuentabancaria)
    {
        $SELECT = "SUM (hd.saldo)";
        $WHERE = " c.idcuentabancaria = $idcuentabancaria ";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_conciliacion c
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = c.idcuentabancaria 
                                LEFT JOIN mod_banco.his_diferencia hd ON hd.idconciliacion = c.idconciliacion 
                                WHERE $WHERE ");
    }

}
