<?php

/**
 * Clase controladora para gestionar las conciliaciones bancarias.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ConciliacionController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new ConciliacionModel();
    }

    public function conciliacionAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadConciliacionesAction()
    {
        echo json_encode($this->model->listDataConciliaciones($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadCuentaBancariaAction()
    {
        echo json_encode($this->model->getCuentasBancarias());
    }

    /**
     * call this model action
     */
    public function loadDiferenciaAcumuladaAction()
    {
        echo $this->model->getDiferenciaAcumulada($this->_request->getPost('idcuentabancaria'));
    }

    /**
     * call this model action
     */
    public function loadTransaccionesNoConciliadasAction()
    {
        echo json_encode($this->model->getTransaccionesNoConciliadas($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataFechaAction()
    {
        echo json_encode($this->model->loadDataFecha());
    }

    /**
     * call this model action
     */
    public function updateConciliacionAction()
    {
        echo $this->model->updateConciliacion($this->_request->getPost());
    }

}
