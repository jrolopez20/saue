<?php

/**
 * Clase modelo para gestionar el Cierre de Banco.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CierreBancoModel extends ZendExt_Model
{

    public function CierreBancoModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $periodo = $this->integrator->contabilidad->getPeriodoSubsistema($this->global->Estructura->idestructura, 12);
        $fecha = $this->integrator->contabilidad->getFechaSubsistema($this->global->Estructura->idestructura, 12);
        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Cierra el Periodo actual del subsistema.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function cerrarPeriodo()
    {
        $pendientes = $this->verificarTransaccionesNoConciliadas();
        if (count($pendientes)) {
            return array('success' => false, 'status' => 'transacciones', 'datos' => $pendientes);
        } else {
            return $this->goToClosePeriodo();
        }
    }

    /**
     * Busca las transacciones que no han sido Conciliadas.
     * @param none
     * @return Array Listado de transacciones no Conciliadas
     */
    public function verificarTransaccionesNoConciliadas()
    {
        $datTransaccion = new DatTransaccion();
        $dataFecha = $this->integrator->contabilidad->getFechaSubsistema($this->global->Estructura->idestructura, 12);
        return $datTransaccion->loadTransaccionesNoConciliadas($this->global->Estructura->idestructura, $dataFecha[0]['fecha']);
    }

    /**
     * Cierra el Periodo actual de Banco.
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function goToClosePeriodo()
    {
        $precedencia = $this->verificarPrecedencia();
        if (!count($precedencia)) {
            $lastPeriodo = $this->integrator->contabilidad->ultimoPeriodoCerrado($this->global->Estructura->idestructura, 12);
            if (!$lastPeriodo) {
                $periodoActual = $this->integrator->contabilidad->getPeriodoSubsistema($this->global->Estructura->idestructura, 12);
                $nextP = $this->integrator->contabilidad->getPeriodoSiguiente($periodoActual[0]['idperiodo'], $periodoActual[0]['idejercicio'], $periodoActual[0]['fin']);
                if (count($nextP)) {
                    return $this->integrator->contabilidad->cerrarPeriodo(12, $nextP);
                } else {
                    return $this->integrator->contabilidad->ultimoDiaPeriodo(12, $periodoActual[0]['fin']);
                }
            } else {
                return array('success' => false, 'msg' => 'El &uacute;ltimo per&iacute;odo ya ha sido cerrado.');
            }
        } else {
            return array('success' => false, 'status' => 'subsistemas', 'datos' => $precedencia);
        }
    }

    /**
     * Cierra el Ejercicio contable.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function cerrarEjercicio()
    {
        $pendientes = $this->verificarTransaccionesNoConciliadas();
        if (count($pendientes)) {
            return array('success' => false, 'status' => 'transacciones', 'datos' => $pendientes);
        } else {
            $precedencia = $this->verificarPrecedencia();
            if (!count($precedencia)) {
                $lastPeriodo = $this->integrator->contabilidad->ultimoPeriodoCerrado($this->global->Estructura->idestructura, 12);
                if ($lastPeriodo) {
                    return $this->integrator->contabilidad->cerrarEjercicio($this->global->Estructura->idestructura, 12);
                } else {
                    return array('success' => false, 'msg' => 'El &uacute;ltimo per&iacute;odo no ha sido cerrado.');
                }
            } else {
                return array('success' => false, 'status' => 'subsistemas', 'datos' => $precedencia);
            }
        }
    }

    /**
     * Revisa la precedencia para el cierre contable.
     * @param none
     * @return Array Causas que impiden el cierre
     */
    public function verificarPrecedencia()
    {
        $subsistOpen = array();
        $datasubsist = $this->integrator->contabilidad->getSubsistemaByID(12);
        $precedentes = $this->integrator->contabilidad->getSubsistemasPrecedentes($this->global->Estructura->idestructura, $datasubsist[0]['precedencia']);
        if (count($precedentes)) {
            foreach ($precedentes as $v) {
                if (implode('', explode('-', $datasubsist[0]['fecha'])) > implode('', explode('-', $v[0]['fecha']))) {
                    $subsistOpen[] = $v;
                }
            }
        }return $subsistOpen;
    }

}
