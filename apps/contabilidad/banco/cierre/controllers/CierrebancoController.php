<?php

/**
 * Clase controladora para gestionar el Cierre Contable.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CierrebancoController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new CierreBancoModel();
        parent::init();
    }

    public function cierrebancoAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadDataFechaAction()
    {
        echo json_encode($this->model->loadDataFecha());
    }

    /**
     * call this model action
     */
    public function cerrarPeriodoAction()
    {
        echo json_encode($this->model->cerrarPeriodo());
    }

    /**
     * call this model action
     */
    public function cerrarEjercicioAction()
    {
        echo json_encode($this->model->cerrarEjercicio());
    }

}
