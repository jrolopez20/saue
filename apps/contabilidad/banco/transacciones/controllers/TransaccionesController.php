<?php

/**
 * Clase controladora para gestionar las transacciones bancarias..
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TransaccionesController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new TransaccionesModel();
    }

    public function transaccionesAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadTransaccionesAction()
    {
        echo json_encode($this->model->listDataTransacciones($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadCuentaBancariaAction()
    {
        echo json_encode($this->model->getCuentasBancarias());
    }

    /**
     * call this model action
     */
    public function loadCuentasContablesAction()
    {
        echo json_encode($this->model->getCuentasContables());
    }

    /**
     * call this model action
     */
    public function loadDataCuentasAction()
    {
        echo json_encode($this->model->loadDataCuentas());
    }

    /**
     * call this model action
     */
    public function loadDataUpdateAction()
    {
        echo json_encode($this->model->getDataUpdate($this->_request->getPost('idtransaccion')));
    }

    /**
     * call this model action
     */
    public function updateTransaccionAction()
    {
        echo $this->model->updateTransaccion($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteTransaccionAction()
    {
        echo $this->model->deleteTransaccion($this->_request->getPost('idtransaccion'));
    }

    /**
     * call this model action
     */
    public function anularTransaccionAction()
    {
        echo $this->model->anularTransaccion($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function loadDataContabilizarAction()
    {
        echo json_encode($this->model->loadDataContabilizar($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function updateRelComprobanteAction()
    {
        echo json_encode($this->model->updateRelComprobante($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDataFechaAction()
    {
        echo json_encode($this->model->loadDataFecha());
    }

    /**
     * call this model action
     */
    public function nextDateAction()
    {
        echo $this->model->nextDate();
    }

    /**
     * Carga los Beneficiarios a partir del servicio de cliente y proveedores
     */
    public function loadBeneficiariosAction()
    {
        echo json_encode($this->model->getClientesProveedores($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function saveComprobanteAction()
    {
        echo $this->model->saveComprobante($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function loadCuentasPorPagarAction()
    {
        echo json_encode($this->model->loadCuentasPorPagar($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function loadDerechouObligacionAction()
    {
        echo json_encode($this->model->loadDerechouObligacion($this->_request->getPost()));
    }

    /**
     * Carga los Beneficiarios a partir del servicio de cliente y proveedores
     */
    public function loadClientesProveedoresAction()
    {
        echo json_encode($this->model->loadClientesProveedores($this->_request->getPost()));
    }

    /**
     * Carga los Beneficiarios a partir del servicio de cliente y proveedores
     */
    public function loadConsecutivoAction()
    {
        echo json_encode($this->model->loadConsecutivo($this->_request->getPost()));
    }

    /**
     * Carga los Beneficiarios a partir del servicio de cliente y proveedores
     */
    public function loadInstrumentoPagoAction()
    {
        echo json_encode($this->model->loadInstrumentoPago($this->_request->getPost()));
    }

}
