<?php

/**
 * Clase modelo para gestionar las transacciones bancarias.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TransaccionesModel extends ZendExt_Model
{

    public function TransaccionesModelModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Listado de transacciones registradas.
     *
     * @param Array $argparams (start, limit, filter)
     * @return Array Listado de transacciones
     */
    public function listDataTransacciones($argparams)
    {
        $datTransaccion = new DatTransaccion();
        $filter = $this->defineFilters($argparams['filtros']);
        $dataTrans = $datTransaccion->getDataTransacciones($this->global->Estructura->idestructura, $filter, $argparams['start'], $argparams['limit']);
        $total = $datTransaccion->getTotalTransacciones($this->global->Estructura->idestructura, $filter);

        return array('datos' => $dataTrans, 'cant' => $total);
    }

    /**
     * Obtiene las cuentas bancarias registradas.
     *
     * @param none
     * @return Array Listado de cuentas bancarias
     */
    public function getCuentasBancarias()
    {
        $ctaBancariaModel = new CuentaBancariaModel();

        return $ctaBancariaModel->listDataCuentaBancaria();
    }

    /**
     * Obtiene las cuentas contables asociadas a la bancaria.
     *
     * @param Integer $idcuentabancaria
     * @return Array Listado de cuentas contables
     */
    public function getCuentasContables($idcuentabancaria)
    {
        $datTransaccion = new DatTransaccion();

        return $datTransaccion->getCtasContables($idcuentabancaria);
    }

    /**
     * Lista los datos de las Cuentas Contables hojas.
     *
     * @param none
     * @return Array Listado de las Cuentas Contables hojas
     */
    public function loadDataCuentas()
    {
        $NomCuenta = new NomCuenta();

        return array('datos' => $NomCuenta->loadDataCuentasNivel(1));
    }

    /**
     * Obtiene los datos de las operaciones de la transaccion.
     *
     * @param Integer $idtransaccion
     * @return Array Listado de las operaciones de la transaccion
     */
    public function getDataUpdate($idtransaccion)
    {
        $datTransaccion = new DatTransaccion();
        $operaciones = $datTransaccion->getDataOperaciones($idtransaccion);
        $liquidaciones = array(); //hasta que se implemente
        return array('operaciones' => $operaciones, 'liquidaciones' => $liquidaciones);
    }

    /**
     * Guarda los datos asociados a la transaccion.
     *
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function updateTransaccion($argparams)
    {

        $objTransaccion = new stdClass();
        $operaciones = json_decode($argparams['operaciones']);

        $objTransaccion->tipotransaccion = $argparams['tipotransaccion'];
        $objTransaccion->numero = $argparams['numero'];
        $objTransaccion->fechaemision = $argparams['fechaemision'];
        $objTransaccion->fecharecibo = $argparams['fecharecibo'];
        $objTransaccion->idcuentabancaria = $argparams['idcuentabancaria'];
        $objTransaccion->saldo = $argparams['saldo'];

        $objTransaccion->idbeneficiario = !empty($argparams['idbeneficiario']) ? $argparams['idbeneficiario'] : NULL;
        $objTransaccion->recibido = $argparams['recibido'];
        $objTransaccion->concepto = $argparams['concepto'];
        $objTransaccion->observacion = $argparams['observacion'];
        $objTransaccion->idestado = $argparams['idestado'];
        $objTransaccion->nocomprobante = $argparams['nocomprobante'];
        $objTransaccion->idestructura = $this->global->Estructura->idestructura;
        $objTransaccion->idtransaccion = $argparams['idtransaccion'];
        /* campo obligacion para la CxPagar quese agrego */
        $objTransaccion->idobligacion = !empty($argparams['idobligacion']) ? $argparams['idobligacion'] : NULL;

        /* ULTIMOS CAMPOS AGREGADOS */
        $objTransaccion->numerocheque = !empty($argparams['numerocheque']) ? $argparams['numerocheque'] : NULL;
        $objTransaccion->idinstrumentopago = !empty($argparams['idinstrumentopago']) ? $argparams['idinstrumentopago'] : NULL;
        $objTransaccion->liquidarcxc = !empty($argparams['liquidarcxc']) ? $argparams['liquidarcxc'] : NULL;

        if ($argparams['idtransaccion']) {
            return $this->Actualizar($objTransaccion, $operaciones);
        } else {
            return $this->Insertar($objTransaccion, $operaciones);
        }
    }

    /**
     * Adiciona la transaccion.
     *
     * @param stdClass $objTran
     * @param Array $operaciones
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($objTran, $operaciones)
    {
        try {
            $query = $this->createInsert($objTran);
            $conn = Zend_Registry::get('conexion');
            $execute = $conn->execute($query);
            $idtransaccion = $execute->fetchColumn(0);

            if (count($operaciones)) {
                return $this->actionInsertarOperaciones($idtransaccion, $operaciones);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function createInsert($objTran)
    {
        $fecharecibo = ($objTran->fecharecibo) ? $objTran->fecharecibo : NULL;
        $beneficiario = ($objTran->idbeneficiario) ? $objTran->idbeneficiario : 'NULL';
        $recibido = ($objTran->recibido) ? $objTran->recibido : NULL;
        $observacion = ($objTran->observacion) ? $objTran->observacion : NULL;
        $idobligacion = ($objTran->idobligacion) ? $objTran->idobligacion : 'NULL';
        $estado = 1;

        /* ULTIMOS CAMPOS AGREGADOS */
        $numerocheque = ($objTran->numerocheque) ? $objTran->numerocheque : 'NULL';
        $idinstrumentopago = ($objTran->idinstrumentopago) ? $objTran->idinstrumentopago : 'NULL';
        $liquidarcxc = ($objTran->liquidarcxc) ? $objTran->liquidarcxc : 'NULL';

        if ($fecharecibo != NULL) {
            $query = "INSERT INTO mod_banco.dat_transaccion(tipotransaccion, numero, fechaemision, fecharecibo, idcuentabancaria, saldo, "
                . "idbeneficiario, recibido, concepto, observacion, idestado, idestructura, idobligacion, numerocheque, idinstrumentopago, liquidarcxc) VALUES ";
            $query .= "(" . $objTran->tipotransaccion . ",'" . $objTran->numero . "','" . $objTran->fechaemision . "',"
                . $fecharecibo . "'," . $objTran->idcuentabancaria . "," . $objTran->saldo . ",'"
                . $beneficiario . ",'" . $recibido . "','" . $objTran->concepto . "','" . $observacion
                . "'," . $estado . "," . $objTran->idestructura . ", " . $idobligacion . ",'"
                . $numerocheque . "', " . $idinstrumentopago . ", " . $liquidarcxc . ")";
        } else {
            $query = "INSERT INTO mod_banco.dat_transaccion(tipotransaccion, numero, fechaemision, idcuentabancaria, saldo, "
                . "idbeneficiario, recibido, concepto, observacion, idestado, idestructura, idobligacion, numerocheque, idinstrumentopago, liquidarcxc) VALUES ";
            $query .= "(" . $objTran->tipotransaccion . ",'" . $objTran->numero . "','" . $objTran->fechaemision . "',"
                . $objTran->idcuentabancaria . "," . $objTran->saldo . "," . $beneficiario . ",'" . $recibido . "','"
                . $objTran->concepto . "','" . $observacion . "'," . $estado . "," . $objTran->idestructura . ", " . $idobligacion . ",'"
                . $numerocheque . "', " . $idinstrumentopago . ", " . $liquidarcxc . ")";
        }

        $query .= " RETURNING idtransaccion";

        return $query;
    }

    /**
     * Actualiza la transaccion.
     *
     * @param stdClass $objTran
     * @param Array $operaciones
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($objTran, $operaciones)
    {
        try {
            $fecharecibo = ($objTran->fecharecibo) ? $objTran->fecharecibo : NULL;
            $beneficiario = ($objTran->idbeneficiario) ? $objTran->idbeneficiario : 'NULL';
            $recibido = ($objTran->recibido) ? $objTran->recibido : NULL;
            $observacion = ($objTran->observacion) ? $objTran->observacion : NULL;
            $idobligacion = ($objTran->idobligacion) ? $objTran->idobligacion : 'NULL';

            /* ULTIMOS CAMPOS AGREGADOS */
            $numerocheque = ($objTran->numerocheque) ? $objTran->numerocheque : 'NULL';
            $idinstrumentopago = ($objTran->idinstrumentopago) ? $objTran->idinstrumentopago : 'NULL';
            $liquidarcxc = ($objTran->liquidarcxc) ? $objTran->liquidarcxc : 'NULL';

            $query = "UPDATE mod_banco.dat_transaccion SET tipotransaccion = " . $objTran->tipotransaccion . ", numero = '" . $objTran->numero . "', "
                . "fechaemision = '" . $objTran->fechaemision . "', idcuentabancaria = " . $objTran->idcuentabancaria . ", "
                . "saldo = " . $objTran->saldo . ", idbeneficiario = $beneficiario, recibido = '$recibido', "
                . "concepto = '" . $objTran->concepto . "', observacion = '$observacion', idestructura = " . $objTran->idestructura . ", idobligacion = " . $idobligacion
                . ", numerocheque = '" . $numerocheque . "', idinstrumentopago = $idinstrumentopago , liquidarcxc = $liquidarcxc";
            if ($fecharecibo) {
                $query .= ",'fecharecibo = $fecharecibo'";
            }
            $query .= " WHERE idtransaccion = " . $objTran->idtransaccion;

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            $this->actionEliminarOperaciones($objTran->idtransaccion);

            return $this->actionInsertarOperaciones($objTran->idtransaccion, $operaciones);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();

            return;
        }
    }

    /**
     * Adiciona las operaciones asociadas a la transaccion.
     *
     * @param Integer $idtransaccion
     * @param Array $operaciones
     * @return boolean true if ocurred, false if failure
     */
    public function actionInsertarOperaciones($idtransaccion, $operaciones)
    {
        try {
            foreach ($operaciones as $v) {
                $this->insertarOperacionesbk($idtransaccion, $v);
            }

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();

            return;
        }
    }

    /**
     * Adiciona la operacion asociada a la transaccion.
     *
     * @param Integer $idtransaccion
     * @param Array $operacion
     * @return boolean true if ocurred, false if failure
     */
    public function insertarOperacionesbk($idtransaccion, $operacion)
    {
        try {
            $query = "INSERT INTO mod_banco.dat_operacionbk (denominacion, idcuenta, idtransaccion, saldo) VALUES ";
            $query .= "('" . $operacion->denominacion . "'," . $operacion->idcuenta . "," . $idtransaccion . "," . $operacion->saldo . ")";
            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();

            return;
        }
    }

    /**
     * Elimina las operaciones asociadas a la transaccion.
     *
     * @param Integer $idbancaria
     * @return void
     */
    public function actionEliminarOperaciones($idtransaccion)
    {
        try {
            $query = "DELETE FROM mod_banco.dat_operacionbk WHERE idtransaccion in (" . $idtransaccion . ")";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            return;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();

            return;
        }
    }

    /**
     * Elimina la transaccion bancaria.
     *
     * @param Integer $idtransaccion
     * @return boolean true if ocurred, false if failure
     */
    public function deleteTransaccion($idtransaccion)
    {
        try {
            $query = "DELETE FROM mod_banco.dat_transaccion WHERE idtransaccion in (" . $idtransaccion . ")";

            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminada}";
        } catch (Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgError}";
        }
    }

    /**
     * procesa los filtros y devuelve la sentencia SQL.
     *
     * @param Array $filter
     * @return String cadena SQL
     */
    public function defineFilters($filter)
    {
        $filter = json_decode($filter);
        $data_return = "";
        if (count($filter)) {
            foreach ($filter as $k => $v) {
                $data_return .= ($k == 0 || $data_return == '') ? "t." . $v->property . " ILIKE '%" . $v->value . "%'" : " OR t." . $v->property . " ILIKE '%" . $v->value . "%'";
            }
        }

        return $data_return;
    }

    /**
     * Anula la transaccion.
     *
     * @param $argparams (idtransaccion, motivo)
     * @return boolean true if ocurred, false if failure
     */
    public function anularTransaccion($argparams)
    {
        try {
            $idtransaccion = json_decode($argparams['arridtransaccion']);
            $conn = Zend_Registry::get('conexion');
            $query = "UPDATE mod_banco.dat_transaccion SET idestado = 4 WHERE idtransaccion = $idtransaccion[0]";
            $conn->execute($query);

            $query = "INSERT INTO mod_banco.dat_anulacion (idtransaccion, motivo) VALUES ";
            $query .= "($idtransaccion[0],'" . $argparams['motivo'] . "')";
            $conn->execute($query);

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgAnulada}";
        } catch (Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgErrorAnular}";
        }
    }

    /**
     * Marca como conciliada la transaccion.
     *
     * @param Integer $idtransaccion
     * @return boolean true if ocurred, false if failure
     */
    public function conciliarTransaccion($idtransaccion)
    {
        try {
            $conn = Zend_Registry::get('conexion');
            $query = "UPDATE mod_banco.dat_transaccion SET idestado = 3 WHERE idtransaccion = " . $idtransaccion;
            $conn->execute($query);

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgConciliada}";
        } catch (Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgError}";
        }
    }

    /**
     * Actualiza la relacion entre transaccion y comprobante.
     *
     * @param $argparams (idtransaccion, idcomprobante, nocomprobante, accion, motivo)
     * @return boolean true if ocurred, false if failure
     */
    public function updateRelComprobante($argparams)
    {
        try {
            $conn = Zend_Registry::get('conexion');
            $query = "UPDATE mod_banco.dat_transaccion SET idcomprobante = " . $argparams['idcomprobante'] . ", "
                . "nocomprobante = " . $argparams['nocomprobante'] . ", idestado = 2 WHERE idtransaccion = " . $argparams['idtransaccion'];
            $conn->execute($query);
            if ($argparams['accion'] == 'anular') {
                return $this->anularTransaccion(array('idtransaccion' => $argparams['idtransaccion'], 'motivo' => $argparams['motivo']));
            } else {
                /* Actualizar la cuenta */
                $datTransaccion = new DatTransaccion();
                $dataTrans = $datTransaccion->GetById($argparams['idtransaccion'], TRUE);
                if ($dataTrans) {
                    $this->actualizarDerechoUObligacion($dataTrans, $datTransaccion,$argparams['nocomprobante'],$argparams['idcomprobante']);
                }

                return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgContabilizada}";
            }
        } catch (Exception $exc) {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgError}";
        }
    }

    private function actualizarDerechoUObligacion($transaccion, $datTransaccion, $nocomprobante, $idcomprobante)
    {
        if (is_numeric($transaccion['idobligacion'])) {
            /* determinar el saldo */
            $operaciones = $datTransaccion->getDataOperaciones($transaccion['idtransaccion']);

            if (empty($operaciones) || empty($operaciones['datos'])) {
                return;
            }
            $saldo = $operaciones['datos'][0]['saldo'];

            $derOblig = $this->integrator->contabilidad->obtenerDerechoObligacionPorId($transaccion['idobligacion']);

            if (empty($derOblig)) {
                return;
            }

            $params = array();
            $params['idobligacion'] = $transaccion['idobligacion'];
            $params['idcomprobante'] = $idcomprobante;
            $params['nocomprobante'] = $nocomprobante;

            if ($derOblig['liquidado'] < $derOblig['importe']) {
                $params['liquidado'] = $derOblig['liquidado'] + $saldo;

                $params['estado'] = $derOblig['importe'] > ($derOblig['liquidado'] + $saldo) ? 2 : 3;
            } else {
                $params['estado'] = 3;
            }

            $this->integrator->contabilidad->modificarObligacion($params);
        }

    }

    /**
     * Carga los datos asociados a la Fecha contable del subsistema.
     *
     * @param none
     * @return Array Carga la fecha contable
     */
    public function loadDataFecha()
    {
        $datTransaccion = new DatTransaccion();
        $periodo = $datTransaccion->obtenerPeriodo($this->global->Estructura->idestructura, 12);
        $fecha = $datTransaccion->obtenerFecha($this->global->Estructura->idestructura, 12);

        return array('periodo' => (count($periodo) ? $periodo[0] : ''), 'fecha' => (count($fecha) ? $fecha[0] : ''));
    }

    /**
     * Pasa la fecha al dia siguiente
     *
     * @param none
     * @return boolean true if ocurred, false if failure
     */
    public function nextDate()
    {
        $datTransaccion = new DatTransaccion();
        $dataPeriodo = $datTransaccion->obtenerPeriodo($this->global->Estructura->idestructura, 12);
        $dataFecha = $datTransaccion->obtenerFecha($this->global->Estructura->idestructura, 12);
        if (implode('', explode('-', $dataFecha[0]['fecha'])) == implode('', explode('-', $dataPeriodo[0]['fin']))) {
            return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgUltimoDia}";
        } else {
            list($y, $m, $d) = explode('-', $dataFecha[0]['fecha']);
            $data = $y . '-' . $m . '-' . ($d + 1);
            $query = "UPDATE mod_maestro.dat_fecha SET fecha = '" . $data . "'";
            $query .= " WHERE idfecha = " . $dataFecha[0]['idfecha'];
            $conn = Zend_Registry::get('conexion');
            $conn->execute($query);

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgFechaSiguiente}";
        }
    }

    /**
     * Obtiene el listado de clientes y proveedores.
     *
     * @param String $query
     * @return Array listado de clientes y proveedores
     */
    public function getClientesProveedores($query = '')
    {
        $idestructura = $this->global->Estructura->idestructura;

        return $this->integrator->configuracion->getClientesProveedores($idestructura, $query);
    }

    /**
     * Obtiene el listado de cuentas por pagar.
     *
     * @param String $query
     * @return Array listado de cuentas por pagar
     */
    public function loadCuentasPorPagar($post)
    {
        $idestructura = $this->global->Estructura->idestructura;
        /* Si viene el proveedor entonces es ese sino 0 para que busque todas las CxP de la entidad */
        $proveedor = !empty ($post['idproveedor']) ? $post['idproveedor'] : 0;

        $data = $this->integrator->contabilidad->obtenerCuentasPorPagar($idestructura, $proveedor);

        return array(
            'success' => TRUE,
            'datos' => $data,
            'cantidad' => count($data),
        );
    }

    /**
     * Obtiene el listado de cuentas por pagar.
     *
     * @param String $query
     * @return Array listado de cuentas por pagar
     */
    public function loadCuentasPorCobrar($post)
    {
        $idestructura = $this->global->Estructura->idestructura;
        /* Si viene el proveedor entonces es ese sino 0 para que busque todas las CxP de la entidad */
        $cliente = !empty ($post['idcliente']) ? $post['idcliente'] : 0;

        $data = $this->integrator->contabilidad->obtenerCuentasPorCobrar($idestructura, $cliente);

        return array(
            'success' => TRUE,
            'datos' => $data,
            'cantidad' => count($data),
        );
    }

    /**
     * Carga los datos para contabilizar o anular el comprobante de transaccion.
     *
     * @param $argparams (arridtransaccion, accion, motivo)
     * @return Array datos para contabilizar o anular el comprobante de transaccion
     */
    public function loadDataContabilizar($argparams)
    {
        $datTransaccion = new DatTransaccion();
        $dataTrans = $datTransaccion->getTransaccionesPorId($argparams['arridtransaccion']);

        $periodoActual = $this->integrator->contabilidad->getPeriodoSubsistema($this->global->Estructura->idestructura, 12);
        $dataFecha = $this->integrator->contabilidad->getFechaSubsistema($this->global->Estructura->idestructura, 12);
        $comp = new stdClass();
        if ($argparams['accion'] == 'anular') {
            $dataCmp = $this->integrator->contabilidad->getComprobantePorId($dataTrans[0]['idcomprobante']);
            $comp->referencia = 'ANULANDO ';
            $comp->detalle = '';
            $comp->fechaemision = $dataFecha[0]['fecha'];
            $comp->idsubsistema = 12;
            $comp->idperiodo = $periodoActual[0]['idperiodo'];
            $arrpases = $dataCmp['pases'];
            foreach ($dataTrans as $k => $v) {
                if ($k > 0) {
                    $comp->referencia .= ', ';
                }
                $comp->referencia .= $v['concepto'];
                $comp->detalle .= ($v['observacion']) ? $v['observacion'] . ', ' : '';
                $this->createPasesAnulados($arrpases);
            }
            $comp->pases = $arrpases;
            if ($comp->detalle == '') {
                $comp->detalle = $comp->referencia;
            }
        } else {
            $comp->referencia = 'CONTABILIZANDO ';
            $comp->detalle = '';
            $comp->fechaemision = $dataFecha[0]['fecha'];
            $comp->idsubsistema = 12;
            $comp->idperiodo = $periodoActual[0]['idperiodo'];
            $arrpases = array();
            foreach ($dataTrans as $k => $v) {
                if ($k > 0) {
                    $comp->referencia .= ', ';
                }
                $comp->referencia .= $v['concepto'];
                $comp->detalle .= ($v['observacion']) ? $v['observacion'] . ', ' : '';
                $this->createPases($v['tipotransaccion'], $v['idtransaccion'], $argparams['accion'], $arrpases, $v);
            }
            $comp->pases = $arrpases;
            if ($comp->detalle == '') {
                $comp->detalle = $comp->referencia;
            }
        }

        return $comp;
    }

    /**
     * Crea los pases para el comprobante de banco.
     *
     * @param Integer $tipo
     * @param Integer $idtransaccion
     * @param String $accion
     * @param Array $arrpases
     * @return void by reference
     */
    public function createPases($tipo, $idtransaccion, $accion, &$arrPases, $transaccion)
    {
        $datTransaccion = new DatTransaccion();
        $operaciones = $datTransaccion->getDataOperaciones($idtransaccion);
        if ($tipo == 0 || $tipo == 2) {//acreditar - contabilizar, debitar - anular
            $this->paseIterator($operaciones['datos'], ($accion == 'anular') ? 'cred' : 'deb', $arrPases, $transaccion);
        } else if ($tipo == 1 || $tipo == 3) {//anular  - contabilizar, debitar - acreditar
            $this->paseIterator($operaciones['datos'], ($accion == 'anular') ? 'deb' : 'cred', $arrPases, $transaccion);
        }
    }
//    public function createPases($tipo, $idtransaccion, $accion, &$arrPases, $transaccion)
//    {
//        $datTransaccion = new DatTransaccion();
//        $operaciones = $datTransaccion->getDataOperaciones($idtransaccion);
//        if ($tipo == 0 || $tipo == 2) {//acreditar - contabilizar, debitar - anular
//            $this->paseIterator($operaciones['datos'], ($accion == 'anular') ? 'deb' : 'cred', $arrPases);
//        } else if ($tipo == 1 || $tipo == 3) {//anular  - contabilizar, debitar - acreditar
//            $this->paseIterator($operaciones['datos'], ($accion == 'anular') ? 'cred' : 'deb', $arrPases);
//        }
//    }

    /**
     * Agrupador de funciones de codigo comun.
     *
     * @param Array $operaciones
     * @param String $accion
     * @param Array $arrPases
     * @return void by reference
     */
    public function paseIterator($operaciones, $accion, &$arrPases, $transaccion)
    {
        foreach ($operaciones as $oper) {
            $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($oper['idcuenta']));
            $data = array();
            $data['idcuenta'] = $dataCta[0]['id'];
            $data['concatcta'] = $dataCta[0]['concat'];
            $data['denominacion'] = $dataCta[0]['denominacion'];
            $data['numero'] = $oper['denominacion'];
            $data['saldo'] = $oper['saldo'];
            $arrPases[] = $this->makePase($data, $accion);
        }

        /* Para liquidar una CxC o CxP*/
        if (!empty($transaccion['idobligacion']) && is_numeric($transaccion['idbeneficiario'])) {

            $clienteprov = $this->integrator->configuracion->getClienteProvById($transaccion['idbeneficiario']);
            $indicecuenta = !empty($transaccion['liquidarcxc']) ? 'idcuentacobrar' : 'idcuentapagar';
            $autoaction = !empty($transaccion['liquidarcxc']) ? 'cred' : 'deb';

            if (!empty($clienteprov) && is_numeric($clienteprov[$indicecuenta])) {
                $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($clienteprov[$indicecuenta]));
                if (!empty($dataCta)) {
                    $data = array();
                    $data['idcuenta'] = $dataCta[0]['id'];
                    $data['concatcta'] = $dataCta[0]['concat'];
                    $data['denominacion'] = $dataCta[0]['denominacion'];
                    $data['numero'] = $oper['denominacion'];
                    $data['saldo'] = $oper['saldo'];
                    $arrPases[] = $this->makePase($data, $autoaction);
                }

            }

        }
    }

    /**
     * Crea un pase para el comprobante.
     *
     * @param Array $ctas
     * @param Integer $debcred
     * @return Array pase del comprobante
     */
    public function makePase($cta, $debcred)
    {
        $pase = new stdClass();
        $pase->readOnly = TRUE;
        $pase->idpase = 0;
        $pase->codigodocumento = $cta['numero'];
        $pase->idcuenta = $cta['idcuenta'];
        $pase->concatcta = $cta['concatcta'];
        $pase->denominacion = $cta['denominacion'];
        $pase->debito = number_format(($debcred == 'deb') ? abs($cta['saldo']) : 0, 2);
        $pase->credito = number_format(($debcred == 'cred') ? abs($cta['saldo']) : 0, 2);

        return $pase;
    }

    /**
     * Crea los pases para anular el comprobante de banco.
     *
     * @param Array $arrpases
     * @return void by reference
     */
    public function createPasesAnulados(&$arrPases)
    {
        foreach ($arrPases as &$v) {
            $data = array();
            $data['numero'] = $v['codigodocumento'];
            $data['idcuenta'] = $v['idcuenta'];
            $data['concatcta'] = $v['concatcta'];
            $data['denominacion'] = $v['denominacion'];
            $data['saldo'] = $v['importe'];
            $v = $this->makePaseAnulado($data);
        }
    }

    /**
     * Crea un pase para anular el comprobante.
     *
     * @param Array $data
     * @return Array pase del comprobante
     */
    public function makePaseAnulado($data)
    {
        $pase = new stdClass();
        $pase->readOnly = TRUE;
        $pase->idpase = 0;
        $pase->codigodocumento = $data['numero'];
        $pase->idcuenta = $data['idcuenta'];
        $pase->concatcta = $data['concatcta'];
        $pase->denominacion = $data['denominacion'];
        $pase->debito = number_format(($data['saldo'] > 0) ? abs($data['saldo']) : 0, 2);
        $pase->credito = number_format(($data['saldo'] < 0) ? abs($data['saldo']) : 0, 2);

        return $pase;
    }

    /**
     * Adiciona los comprobantes de banco.
     *
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function saveComprobante($argparams)
    {
        $save = $this->integrator->contabilidad->saveComprobante($argparams);
        $id = explode("'id':", $save);
        $num = explode("'nocomp':", $save);

        if (count($id) > 1) {
            $id = substr($id[1], 0, -1);
        }

        if (count($num) > 1) {
            $num = substr($num[1], 0, -1);
            $numco = explode(",", $num);
        }
        if (is_numeric($id)) {
            return $this->updateRelComprobante(array('idtransaccion' => $argparams['idtransaccion'], 'idcomprobante' => $id, 'nocomprobante' => $numco[0], 'accion' => $argparams['accion'], 'motivo' => $argparams['motivo']));
        } else {
            return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgError}";
        }
    }

    /**
     * Obtiene el listado de cuentas por pagar o por cobrar en dependencia de los parametros.
     *
     * @param String $post Parametros enviados en la peticion
     * @return Array listado de cuentas por pagar
     */
    public function loadDerechouObligacion($post)
    {
        $idestructura = $this->global->Estructura->idestructura;
        /* Si viene el proveedor entonces es ese sino 0 para que busque todas las CxP de la entidad */
        $idclienteproveedor = !empty ($post['idclienteproveedor']) ? $post['idclienteproveedor'] : 0;
        $tipo = $post['tipo'];

        if ($tipo == 2) {
            $data = $this->integrator->contabilidad->obtenerCuentasPorPagar($idestructura, $idclienteproveedor, array(4));
        } else {
            $data = $this->integrator->contabilidad->obtenerCuentasPorCobrar($idestructura, $idclienteproveedor, array(4));
        }

        /* SOLO LAS QUE NO HAN SIDO LIQUIDADAS TOTALMENTE */
        $final = array();
        foreach ($data as $dat) {
            if ($dat['importe'] > $dat['liquidado']) {
                $final[] = $dat;
            }
        }

        return array(
            'success' => TRUE,
            'datos' => $final,
            'cantidad' => count($final),
        );
    }


    /**
     * Obtiene el listado de clientes y proveedores.
     *
     * @param String $query
     * @return Array listado de clientes y proveedores
     */
    public function loadClientesProveedores($post)
    {
        $query = !empty($post['query']) ? $post['query'] : '';
        $tipo = $post['tipo'];
        $idestructura = $this->global->Estructura->idestructura;

        if ($tipo == 2) { // Proveedores
            return $this->integrator->configuracion->getProveedores($idestructura, $query);
        } else { // Clientes
            return $this->integrator->configuracion->getClientes($idestructura, $query);
        }
    }

    /**
     * Obtiene el numero consecutivo para un tipo de transaccon determinda.
     *
     * @param Array $post Arreglo con los datos de la peticion
     * @return int consecutivo
     */
    public function loadConsecutivo($post)
    {
        $response = DatTransaccion::GetMaxNum($post['tipo']);

        return is_numeric($response) ? $response + 1 : 0;
    }

    /**
     * Obtiene el los instrumentos de pago.
     *
     * @param Array $post Arreglo con los datos de la peticion
     * @return Array Arreglo con los instrumentos de pago
     */
    public function loadInstrumentoPago($post)
    {
        $data = DatTransaccion::loadInstrumentoPago();

        return array(
            'success' => TRUE,
            'datos' => $data,
            'cantidad' => count($data),
        );
    }

}
