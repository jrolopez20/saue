<?php

/**
 * Clase dominio para gestionar las transacciones bancarias.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatTransaccion extends BaseDatTransaccion
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatOperacionbk', array('local' => 'idtransaccion', 'foreign' => 'idtransaccion'));
        $this->hasOne('NomEstado', array('local' => 'idestado', 'foreign' => 'idestado'));
    }

    /**
     * Lista los datos de las transacciones de la entidad.
     * @param Integer $idestructura
     * @param Array $filter
     * @param Integer $start
     * @param Integer $limit
     * @return Array datos de las transacciones de la entidad
     */
    public function getDataTransacciones($idestructura, $filter, $start, $limit)
    {
        $SELECT = "t.idtransaccion, t.tipotransaccion, t.numero, t.fechaemision, t.fecharecibo, "
                . "t.idcuentabancaria, t.saldo, t.idbeneficiario, t.recibido, t.concepto, t.observacion, "
                . "t.idestado, t.nocomprobante, t.idestructura, cb.numerocuenta, cb.nombre, "
                . "(cb.numerocuenta ||' '|| cb.nombre) as cuenta, e.denominacion as estado, "
                . "nb.idbanco, nb.codigo, nb.nombre AS nombreBanco, nb.abreviatura, cp.nombre as beneficiario,"
                . 't.idobligacion, t.numerocheque, t.idinstrumentopago, t.liquidarcxc';

        $WHERE = " t.idestructura = $idestructura ";
        if ($filter != '') {
            $WHERE .=" AND $filter";
        }
        
        $PAGIN = ($start || $limit)? " OFFSET $start LIMIT $limit" : "";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_transaccion t
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = t.idcuentabancaria
                                INNER JOIN mod_maestro.nom_sucursal ns ON ns.idsucursal = cb.idsucursal 
                                INNER JOIN mod_maestro.nom_banco nb ON nb.idbanco = ns.idbanco
                                INNER JOIN mod_banco.nom_estado e ON e.idestado = t.idestado 
                                LEFT JOIN mod_maestro.nom_clientesproveedores cp ON cp.idclientesproveedores = t.idbeneficiario
                                WHERE $WHERE
                                ORDER BY t.numero, t.tipotransaccion $PAGIN");
    }

    /**
     * Obtiene el total de las transacciones de la entidad.
     * @param Integer $idestructura
     * @param Array $filter
     * @return Integer total de las cuentas bancarias de la entidad
     */
    public function getTotalTransacciones($idestructura, $filter)
    {
        $SELECT = "t.*";
        $WHERE = " t.idestructura = $idestructura ";
        if ($filter != '') {
            $WHERE .=" AND $filter";
        }
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_transaccion t WHERE $WHERE");
    }

    /**
     * Obtiene las Ctas contables asociadas a la bancaria.
     * @param Integer $idcuentabancaria
     * @return Array listado de Ctas contables asociadas a la cta bancaria
     */
    public function getCtasContables($idcuentabancaria)
    {
        $SELECT = "nc.*";
        $WHERE = " cc.idcuentabancaria = $idcuentabancaria ";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_cuentacontable cc "
                        . "INNER JOIN mod_contabilidad.nom_cuenta nc ON nc.idcuenta= cc.idcuenta "
                        . "WHERE $WHERE");
    }

    /**
     * Obtiene las operaciones de la transaccion.
     * @param Integer $idtransaccion
     * @return Array listado de las operaciones de la transaccion
     */
    public function getDataOperaciones($idtransaccion)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $operaciones = $connection->fetchAll("SELECT o.*, (nc.concatcta||' '||nc.denominacion) as text FROM mod_banco.dat_operacionbk o "
                . "INNER JOIN mod_contabilidad.nom_cuenta nc ON nc.idcuenta= o.idcuenta "
                . "WHERE o.idtransaccion = $idtransaccion");
        return array('datos' => $operaciones);
    }

    /**
     * Carga los datos del Periodo actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Periodo actual del subsistema
     */
    public function obtenerPeriodo($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT p.* FROM mod_maestro.dat_estructurasubsist es "
                        . "INNER JOIN mod_maestro.dat_cierre c ON c.idestructurasubsist = es.idestructurasubsist "
                        . "INNER JOIN mod_maestro.dat_periodocontable p ON p.idperiodo = c.idperiodoactual "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Carga la Fecha actual dado el subsistema.
     * @param Integer $idestructura
     * @param Integer $idsubsistema
     * @return Array Fecha actual del subsistema
     */
    public function obtenerFecha($idestructura, $idsubsistema)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT f.* FROM mod_maestro.dat_fecha f "
                        . "INNER JOIN mod_maestro.dat_estructurasubsist es ON es.idestructurasubsist = f.idestructurasubsist "
                        . "WHERE es.idsubsistema = $idsubsistema AND es.idestructuracomun = $idestructura;");
    }

    /**
     * Lista las transacciones no Conciliadas.
     * @param Integer $idestructura
     * @param Date $fecha
     * @return Array datos de las transacciones de la entidad
     */
    public function loadTransaccionesNoConciliadas($idestructura, $fecha)
    {
        $fecha = "'$fecha'::date";
        $SELECT = "t.idtransaccion, t.tipotransaccion, t.numero, t.fechaemision, t.fecharecibo, "
                . "t.idcuentabancaria, t.saldo, t.idbeneficiario, t.recibido, t.concepto, t.observacion, "
                . "t.idestado, t.nocomprobante, t.idestructura, cb.numerocuenta, cb.nombre, "
                . "(cb.numerocuenta ||' '|| cb.nombre) as cuenta, e.denominacion as estado, cp.nombre as beneficiario,"
                . 't.idobligacion, t.numerocheque, t.idinstrumentopago, t.liquidarcxc';

        $WHERE = " t.idestructura = $idestructura AND (t.idestado = 1 OR t.idestado = 2) AND t.fechaemision <= $fecha";

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_transaccion t
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = t.idcuentabancaria
                                INNER JOIN mod_banco.nom_estado e ON e.idestado = t.idestado
                                INNER JOIN mod_maestro.nom_clientesproveedores cp ON cp.idclientesproveedores = t.idbeneficiario 
                                WHERE $WHERE ORDER BY t.numero, t.tipotransaccion");
    }
    
     /**
     * Lista los datos de las transacciones dado array de id.
     * @param Array $arridtransaccion
     * @return Array datos de las transacciones de la entidad
     */
    public function getTransaccionesPorId($arridtransaccion)
    {
        $arrIds = implode(',', json_decode($arridtransaccion));
        $SELECT = "t.idtransaccion, t.tipotransaccion, t.numero, t.fechaemision, t.fecharecibo, "
                . "t.idcuentabancaria, t.saldo, t.idbeneficiario, t.recibido, t.concepto, t.observacion, "
                . "t.idestado, t.nocomprobante, t.idcomprobante, t.idestructura, cb.numerocuenta, cb.nombre, "
                . "(cb.numerocuenta ||' '|| cb.nombre) as cuenta, e.denominacion as estado, "
                . "nb.idbanco, nb.codigo, nb.nombre AS nombreBanco, nb.abreviatura, cp.nombre as beneficiario, "
                . 't.idobligacion, t.numerocheque, t.idinstrumentopago, t.liquidarcxc';

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_banco.dat_transaccion t
                                INNER JOIN mod_banco.nom_cuentabancaria cb ON cb.idcuentabancaria = t.idcuentabancaria
                                INNER JOIN mod_maestro.nom_sucursal ns ON ns.idsucursal = cb.idsucursal 
                                INNER JOIN mod_maestro.nom_banco nb ON nb.idbanco = ns.idbanco
                                INNER JOIN mod_banco.nom_estado e ON e.idestado = t.idestado 
                                INNER JOIN mod_maestro.nom_clientesproveedores cp ON cp.idclientesproveedores = t.idbeneficiario 
                                WHERE t.idtransaccion IN ($arrIds) ORDER BY t.numero, t.tipotransaccion");
    }

    static public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatTransaccion')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            echo "{'success':false, 'codMsg':3, 'mensaje':'". $e->getMessage()."'}";
        }
    }

    static public function GetMaxNum($tipo)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->select('count(idtransaccion) as cantidad')
                ->from('DatTransaccion')
                ->where("tipotransaccion = ?", array($tipo));
//                ->execute();

            return $result->fetchOne()->cantidad;
        } catch (Doctrine_Exception $e) {
            echo "{'success':false, 'codMsg':3, 'mensaje':'". $e->getMessage()."'}";
        }
    }


    /**
     * Obtiene el los instrumentos de pago.
     *
     * @param Array $post Arreglo con los datos de la peticion
     * @return Array Arreglo con los instrumentos de pago
     */
    public function loadInstrumentoPago()
    {
        $SELECT = "doc.*";
        $WHERE = "doc.idsubsistema = 12 ";
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll("SELECT $SELECT FROM mod_maestro.nom_documentoprimario doc "
            . "WHERE $WHERE");
    }

    public function GetById($idtransaccion, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatTransaccion')->find($idtransaccion);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

}
