<?php

abstract class BaseDatTransaccion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.dat_transaccion');
        $this->hasColumn('idtransaccion', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_transaccion'));
        $this->hasColumn('tipotransaccion', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('numero', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('fechaemision', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('fecharecibo', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcuentabancaria', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('saldo', 'numeric', null, array('notnull' => false, 'primary' => false));
//        $this->hasColumn('numerocuenta', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('recibido', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('concepto', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('observacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestado', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('nocomprobante', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idbeneficiario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcomprobante', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idobligacion', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('numerocheque', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idinstrumentopago', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('liquidarcxc', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
