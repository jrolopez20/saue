<?php

abstract class BaseDatOperacionbk extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.dat_operacionbk');
        $this->hasColumn('idoperacionbk', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_banco.sec_operacionbk'));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcuenta', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idtransaccion', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('saldo', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
