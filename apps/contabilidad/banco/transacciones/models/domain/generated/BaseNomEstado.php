<?php

abstract class BaseNomEstado extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_banco.nom_estado');
        $this->hasColumn('idestado', 'numeric', null, array('notnull' => false, 'primary' => true));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}
