<?php

/**
 * Clase dominio para gestionar las operaciones bancarias.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DatOperacionbk extends BaseDatOperacionbk
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatTransaccion', array('local' => 'idtransaccion', 'foreign' => 'idtransaccion'));
    }

}
