<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PHP_DBF
 *
 * @author al3jandro
 */
class PHP_DBF {

    private $bodyData;
    private $db;

    public function __construct($objR) {
        $this->bodyData = $objR->bodyData;
    }

    function extraerCampos() {
        $campos = array();
        $i = 0;
        $body = (array) $this->bodyData[0];
        foreach ($body as $key => $value) {
            $k = 0;
            $index = 1;
            $camp[$k] = '"' . $index . '"';
            $index++;
            $k++;
            $camp[$k] = "C";
            $k++;
            $camp[$k] = 50;
            $campos[$i] = $camp;
            $i++;
        }
        return $campos;
    }

    function llenarCampos($dir) {
        $this->db = dbase_open($dir, 2);
        if ($this->db) {
            foreach ($this->bodyData as $values) {
                $datos = array();
                $i = 0;
                $values = (array) $values;
                foreach ($values as $val) {
                    $datos[$i] = $val;
                    $i++;
                }
                dbase_add_record($this->db, $datos);
            }
            dbase_pack($this->db);
            dbase_close($this->db);
        } else {
            die("Error, no se puede exportar los datos. \n");
        }
    }

    public function exportarDBF($dir) {
        $def = $this->extraerCampos();
        if (!dbase_create($dir, $def)) {
            die("Error, no se puede crear la base de datos. \n");
        } else {
            self::llenarCampos($dir);
        }
    }

    public function outputFile($file, $name, $mime_type = '') {
        if (file_exists($file)) {
            if (!is_readable($file)) {
                die('El archivo de base datos no se ha encontrado en el servidor o esta inaccesible. \n');
            } else {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($name));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($this->db));
                ob_clean();
                flush();
                readfile($name);
            }
//            $size = filesize($file);
//            $name = rawurldecode($name);
//            $known_mime_types = array(
//                "pdf" => "application/pdf",
//                "txt" => "text/plain",
//                "html" => "text/html",
//                "htm" => "text/html",
//                "exe" => "application/octet-stream",
//                "zip" => "application/zip",
//                "doc" => "application/msword",
//                "xls" => "application/vnd.ms-excel",
//                "ppt" => "application/vnd.ms-powerpoint",
//                "gif" => "image/gif",
//                "png" => "image/png",
//                "jpeg" => "image/jpg",
//                "jpg" => "image/jpg",
//                "php" => "text/plain"
//            );
//            if ($mime_type == '') {
//                $file_extension = strtolower(substr(strrchr($file, "."), 1));
//                if (array_key_exists($file_extension, $known_mime_types)) {
//                    $mime_type = $known_mime_types[$file_extension];
//                } else {
//                    $mime_type = "application/force-download";
//                }
//            }
//            @ob_end_clean();
//            if (ini_get('zlib.output_compression')) {
//                ini_set('zlib.output_compression', 'Off');
//            }
//            header('Content-Type: ' . $mime_type);
//            header('Content-Disposition: attachment; filename="' . $name . '"');
//            header("Content-Transfer-Encoding: binary");
//            header('Accept-Ranges: bytes');
//            header("Cache-control: private");
//            header('Pragma: private');
//            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
//            if (isset($_SERVER['HTTP_RANGE'])) {
//                list($a, $range) = explode("=", $_SERVER['HTTP_RANGE'], 2);
//                list($range) = explode(",", $range, 2);
//                list($range, $range_end) = explode("-", $range);
//                $range = intval($range);
//                if (!$range_end) {
//                    $range_end = $size - 1;
//                } else {
//                    $range_end = intval($range_end);
//                }
//
//                $new_length = $range_end - $range + 1;
//                header("HTTP/1.1 206 Partial Content");
//                header("Content-Length: $new_length");
//                header("Content-Range: bytes $range-$range_end/$size");
//            } else {
//                $new_length = $size;
//                header("Content-Length: " . $size);
//            }
//            $chunksize = 1 * (1024 * 1024);
//            $bytes_send = 0;
////            if ($file == @fopen($file, 'r')) {
//            if (isset($_SERVER['HTTP_RANGE'])) {
//                fseek($file, $range);
//            }
//            while (!feof($file) && (!connection_aborted()) && ($bytes_send < $new_length)) {
//                $buffer = fread($file, $chunksize);
//                print($buffer);
//                flush();
//                $bytes_send += strlen($buffer);
//            }
//            fclose($file);
//            } else {
//                echo('Error, no se pudo abrir el archivo.');
//                die;
//                return;
//            }
        } else {
            die('Error, no se pudo encontrar el archivo de base de datos.');
        }
    }

    //combierte la fecha a formato DBF
    private function toDateDBF($timestamp) {
        if (empty($timestamp)) {
            $timestamp = 0;
        }

        if (!is_numeric($timestamp) && !is_array($timestamp)) {
            throw new InvalidArgumentException('$timestamp was not in expected format(s).');
        }

        if (is_array($timestamp) && (!isset($timestamp['year']) || !isset($timestamp['mon']) || !isset($timestamp['mday']))) {
            throw new InvalidArgumentException('$timestamp array did not contain expected key(s).');
        }

        if (is_string($timestamp) && strlen($timestamp) === 8 && validate_date_string($timestamp)) {
            return $timestamp;
        }

        if (!is_array($timestamp)) {
            $date = getdate($timestamp);
        } else {
            $date = $timestamp;
        }

        return substr(str_pad($date['year'], 4, '0', STR_PAD_LEFT), 0, 4) .
                substr(str_pad($date['mon'], 2, '0', STR_PAD_LEFT), 0, 2) .
                substr(str_pad($date['mday'], 2, '0', STR_PAD_LEFT), 0, 2);
    }

    //validad si la cadena que contiene la fecha cumple el formato DBF
    private function validate_date_string($string) {
        $time = mktime(
                0, 0, 0, intval(substr($string, 4, 2)), intval(substr($string, 6, 2)), intval(substr($string, 0, 4))
        );
        if ($time === false || $time === -1) {
            return false;
        }
        return true;
    }

    //obtine la fecha con formato DBF
    private function _dateDBF($data) {
        $aux;
        if (is_string($data))
            $aux = split('/', $data);

        if (is_int($data)) {
            $tmp = strval($data);
            if (strlen($tmp) == 8 && validate_date_string($tmp)) {
                $data = $tmp;
            }
        } elseif (count($aux) > 0) {
            if (strlen($aux[0]) == 4)
                return toDateDBF($aux[0] . $aux[1] . $aux[2]);
            else
                return toDateDBF($aux[2] . $aux[1] . $aux[0]);
        } else
            return toDateDBF($data);
    }

    //convierte un booleano a formato DBF
    private function toLogicalDBF($value) {
        if ($value === 'F' || $value === false) {
            return 'F';
        }

        if (is_string($value)) {
            if ($value == 'True' || $value == 'true' || $value == 'TRUE') {
                return 'T';
            } elseif ($value == 'False' || $value == 'false' || $value == 'FALSE') {
                return 'F';
            }
        }
        if ($value === 'T' || $value === true) {
            return 'T';
        }

        return ' ';
    }

    //obtiene un booleano con formato DBF
    private function logicalDBF($data) {
        return toLogical($data);
    }

}

//clase
