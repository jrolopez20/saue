<?php

class ReportModel extends ZendExt_Model
{
    private $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í", "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");

    public function buildReport($argArrayReportData, $argReportFormat)
    {
        $NomCuenta = new NomCuenta();
        $formato = $NomCuenta->getFormatoAsociado($this->global->Estructura->idestructura);
        $separador = (count($formato)) ? $formato[0]['separador'] : '.';
        $reportList = array();

        foreach ($argArrayReportData as $objReportData) {
            if (file_exists(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/models/FactoryReport.php")) {
                require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/models/FactoryReport.php");
                $objReport = FactoryReport::Create(($objReportData->datoGeneral->reporte) ? $objReportData->datoGeneral->reporte : $objReportData->reporte);
                $objReport->gTitle = ($objReportData->datoGeneral->titulo) ? strtr(strtoupper($objReportData->datoGeneral->titulo), $this->utf8) : strtr(strtoupper($objReportData->gTitle), $this->utf8);
                $objReport->codeSeparator = $separador;
                $objReport->generalData = $objReportData->datoGeneral;
                $objReport->bodyData = $objReportData->datoCuerpo;
                $objReport->footData = (isset($objReportData->datoPie)) ? $objReportData->datoPie : null;
                $reportList[] = $objReport;
            } else {
                die('LA CLASE <b>FactoryReport.php</b> NO HA SIDO ENCONTRADA EN EL SERVIDOR.');
            }
        }
        if (file_exists(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/CoreReport.php")) {
            require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/CoreReport.php");
            new CoreReport($argReportFormat, $reportList);
        } else {
            die('LA CLASE <b>CoreReport.php</b> NO HA SIDO ENCONTRADA EN EL SERVIDOR.');
        }
    }

}