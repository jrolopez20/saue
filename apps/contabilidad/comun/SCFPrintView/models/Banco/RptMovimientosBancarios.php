<?php

/**
 * Reporte Movimiento bancario por período
 * @version 1.0
 */
class RptMovimientosBancarios extends ElementsReport
{

    private $pagina;

    public function __construct()
    {
        $this->fieldName = 'beneficiario';
        $this->gTitle = 'MOVIMIENTOS BANCARIOS POR PER&Iacute;ODOS';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return "<tr'>
                <td width='12%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='15%'>&nbsp;</td>
                <td width='20%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='5%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
            </tr>";
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        return $this->GetHtmlHeadTable($this->generalData, $argPageCount);
    }

//Método Obligatorio
    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE), $this->getFootTable(0, 0, 0, 0));
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(22, 40), array(24, 30), array(22, 44), array(22, 48));
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'beneficiario';
        return $this->CasePaperFormat(array(32, 35), array(60, 45), array(65, 45), array(65, 45));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        if ($j == 0) {
            $TR.="<tr style='border: 1px solid; font-size:10pt;'>
                <td style='border: 1px solid;' width='12%' align='left'>$argData->numerocuenta</td>
                <td style='border: 1px solid;' width='7%' align='center'>" . $this->showTipoTransaccion($argData->tipotransaccion) . "</td>
                <td style='border: 1px solid;' width='7%' align='center'>$argData->numero</td>
                <td style='border: 1px solid;' width='7%' align='center'>$argData->fechaemision</td>
                <td style='border: 1px solid;' width='15%' align='left'>$argData->concepto</td>
                <td style='border: 1px solid;' width='20%' align='left'>$argData->beneficiario</td>
                <td style='border: 1px solid;' width='7%' align='center'>$argData->estado</td>
                <td style='border: 1px solid;' width='5%' align='center'>$argData->nocomprobante</td>
                <td style='border: 1px solid;' width='10%' align='right'>$argData->saldo</td>
                </tr>";
        } else {
            $TR.="<tr style='border-top:hidden; font-size:10pt;'>
                <td width='12%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='15%'>&nbsp;</td>
                <td width='20%'>$TR_TEXT</td>
                <td width='7%'>&nbsp;</td>
                <td width='5%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
            </tr>";
        }
    }

    public function GetHtmlHeadTable($gData, $PAGECOUNT)
    {
        $this->pagina = $PAGECOUNT;
        return "<table  width='100%' border='0' style='font-size:11pt; position:relative; border-collapse:collapse; border-spacing:0px;'>
            <tr>
                <td colspan='9' align='center'><b>" . strtoupper("$gData->descripcionentidad") . "</b></td>"
                . "</tr>
            <tr>
                <td colspan='9' align='center'><b>MOVIMIENTOS BANCARIOS POR PER&Iacute;ODOS</b></td>"
                . "</tr>
            <tr>
                <td height='46' valign = 'top' colspan='9' align='center'><b>Desde: $gData->fechaDesde Hasta: $gData->fechaHasta</b></td>"
                . "</tr>
            <tr>
                <td align='center' style='border: 1px solid;' ><b>Cuenta bancaria</b></td>
                <td align='center' style='border: 1px solid;' ><b>Transacci&oacute;n</b></td>
                <td align='center' style='border: 1px solid;' ><b>N&uacute;mero</b></td>
                <td align='center' style='border: 1px solid;' ><b>Fecha</b></td>
                <td align='center' style='border: 1px solid;' ><b>Concepto</b></td>
                <td align='center' style='border: 1px solid;' ><b>Beneficiario</b></td>
                <td align='center' style='border: 1px solid;' ><b>Estado</b></td>
                <td align='center' style='border: 1px solid;' ><b>No. Co</b></td>
                <td align='center' style='border: 1px solid;' ><b>Saldo</b></td>
                </tr>";
    }

    public function getFootTable($DIA, $MES, $YEAR, $USUARIO)
    {
        return "<table border='0' style='font-size:10pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;'>
                  <tr>
                    <td height=50; style='text-align:right;' font-size:10pt;'>P&aacute;gina $this->pagina </td>
                  </tr>
                </table>";
    }

    public function showTipoTransaccion($arg)
    {
        $str = '';
        if ($arg == 0) {
            $str = 'Dép/banc';
        } else if ($arg == 1) {
            $str = 'Déb/banc';
        } else if ($arg == 2) {
            $str = 'Créd/banc';
        } else {
            $str = 'Em/cheques';
        }
        return $str;
    }

}
