<?php

/**
 * Reporte Saldo disponible en bancos
 * @version 1.0
 */
class RptSaldoDisponibleBanco extends ElementsReport
{

    private $pagina;

    public function __construct()
    {
        $this->fieldName = 'nombrecliente';
        //$this->gTitle = 'LISTADO DE INSTRUMENTOS EN TR&Aacute;NSITO';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return "<tr>
                <td width='6%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='18%'>&nbsp;</td>
                <td width='18%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
                </tr>";
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        return $this->GetHtmlHeadTable($this->generalData, $argPageCount);
    }

    //Método Obligatorio
    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE));
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(26, 44), array(28, 36), array(27, 50), array(27, 54));
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'nombrecliente';
        return $this->CasePaperFormat(array(95, 48), array(80, 60), array(85, 60), array(85, 60));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {

        if (strpos($argData->flag, '1') !== FALSE) {
            $TR.="<tr><td colspan='5' style='border-bottom:hidden;'>CUENTA BANCARIA:" . strtoupper("$argData->concatcuentabancaria $argData->nombrecuentabancaria") . "</td></tr>
            <tr><td colspan='5'>&nbsp;</td></tr>
            <tr><td width='6%' align='center'>No. CO </td><td width='7%' align='center'>FECHA</td>
            <td width='11%' align='center'>No.CHEQ</td><td width='13%' align='right'>IMPORTE</td><td width='30%' style=\'margin-left: 10px\'>BENEFICIARIO</td></tr>";
        } else {
            if ($j == 0) {
                $TR.="<tr>

                <td width='6%' align='center'>$argData->numcomprobante</td>
                <td width='7%' align='center'>$argData->fechacreacion</td>
                <td width='18%' align='center'>$argData->numdocumento</td>
                <td width='18%' align='right'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 5, $argData->importe, true, true)}</td>
                <td width='30%' style=\'margin-left: 10px\'>$argData->nombrecliente</td>
                </tr>";
            } else {
                $TR.="<tr style='border-top:hidden;'>

                <td width='6%'>&nbsp;</td>
                <td width='7%'>&nbsp;</td>
                <td width='18%'>&nbsp;</td>
                <td width='18%'>&nbsp;</td>
                <td width='30%'>$TR_TEXT</td>
            </tr>";
            }
        }
    }

    public function GetHtmlHeadTable($gData, $PAGECOUNT)
    {
        $this->pagina = $PAGECOUNT;
        return "<table  width='100%' border='1' style='font-size:11pt; position:relative; border-collapse:collapse; border-spacing:0px;'>
            <tr><td height='46' colspan='6' align='left'>LISTADO DE TRANSACCIONES NO CARGADAS POR EL BANCO: <b>" . strtoupper("$gData->codigoentidad $gData->descripcionentidad") . "</b></td></tr>";
    }

}
