<?php

/**
 * Reporte Resumen de saldos bancarios
 * @version 1.0
 */
class RptResumenSaldosBancarios extends ElementsReport
{

    private $pagina;

    public function __construct()
    {
        $this->fieldName = 'banco';
        $this->gTitle = 'RESUMEN DE SALDOS BANCARIOS';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return "<tr'>
                <td width='45%'>&nbsp;</td>
                <td width='25%'>&nbsp;</td>
                <td width='15%'>&nbsp;</td>
                <td width='15%'>&nbsp;</td>
            </tr>";
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        return $this->GetHtmlHeadTable($this->generalData, $argPageCount);
    }

    //Método Obligatorio
    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE), $this->getFootTable(0, 0, 0, 0));
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(26, 44), array(28, 36), array(27, 50), array(27, 54));
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'banco';
        return $this->CasePaperFormat(array(95, 48), array(80, 60), array(85, 60), array(85, 60));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        if ($j == 0) {
            $TR.="<tr style='border: 1px solid; font-size:10pt;'>
                <td style='border: 1px solid;' width='45%' align='left'>$argData->banco</td>
                <td style='border: 1px solid;' width='25%' align='center'>$argData->numerocuenta</td>
                <td style='border: 1px solid;' width='15%' align='right'>$argData->saldolibro</td>
                <td style='border: 1px solid;' width='15%' align='right'>$argData->saldobanco</td>
                </tr>";
        } else {
            $TR.="<tr style='border: 1px solid; border-top:hidden; font-size:10pt;'>
                <td width='45%'>$TR_TEXT</td>
                <td width='25%'>&nbsp;</td>
                <td width='15%'>&nbsp;</td>
                <td width='15%'>&nbsp;</td>
            </tr>";
        }
    }

    public function GetHtmlHeadTable($gData, $PAGECOUNT)
    {
        $this->pagina = $PAGECOUNT;
        return "<table  width='100%' border='0' style='font-size:11pt; position:relative; border-collapse:collapse; border-spacing:0px;'>
            <tr>
                <td colspan='4' align='center'><b>" . strtoupper("$gData->descripcionentidad") . "</b></td>"
                . "</tr>
            <tr>
                <td colspan='4' align='center'><b>RESUMEN DE SALDOS BANCARIOS</b></td>"
                . "</tr>
            <tr>
                <td height='46' valign = 'top' colspan='4' align='center'><b>Hasta: $gData->fechaHasta</b></td>"
                . "</tr>
            <tr>
                <td align='center' style='border: 1px solid;' ><b>Banco</b></td>
                <td align='center' style='border: 1px solid;' ><b>No. cuenta</b></td>
                <td align='center' style='border: 1px solid;' ><b>Saldo en libro</b></td>
                <td align='center' style='border: 1px solid;' ><b>Saldo en banco</b></td>
                </tr>";
    }

    public function getFootTable($DIA, $MES, $YEAR, $USUARIO)
    {
        return "<table border='0' style='font-size:10pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;'>
                  <tr>
                    <td height=50; style='text-align:right;' font-size:10pt;'>P&aacute;gina $this->pagina </td>
                  </tr>
                </table>";
    }

}
