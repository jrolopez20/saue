<?php

/**
 * Reporte Conciliación bancaria
 * @version 1.0
 */
class RptConciliacionBancaria extends ElementsReport implements IEStrategyAdicionalPages {

    private $pagina;

    public function __construct() {
        $this->fieldName = 'salto';
        $this->gTitle = 'CONCILIACI&Oacute;N MENSUAL';
        $this->textPageBreak = 'break';
    }

    public function GetBlankRows($argArrayColumns) {
        return "";
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns) {
        return $this->GetHtmlHeadTable($this->generalData, $argPageCount);
    }

    //Método Obligatorio
    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE) {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE), $this->getFootTable());
    }

    public function NumberRowsPaperFormat() {
        return $this->CasePaperFormat(array(30, 48), array(32, 40), array(31, 56), array(31, 61));
    }

    public function LengthStringPaperFormat(& $argField) {
        $argField = 'salto';
        return $this->CasePaperFormat(array(95, 48), array(80, 60), array(85, 60), array(85, 60));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE) {
        
        if (strpos($TR_TEXT, 'break') !== FALSE) {
            if($j == 0){
                //La otra parte del encabezado
                $listadoEstatico = $argData->listEstatico;
                
                $TR.="<tr style='border-top:hidden;'><td colspan='3' style='border-top: hidden;'>CUENTA BANCARIA: {$listadoEstatico[0]->concatcuentabancaria} {$listadoEstatico[0]->nombrecuentabancaria}</td></tr>
                <tr style='border-top:hidden;'><td colspan='3' style='border-top: hidden;'>FECHA: {$this->generalData->fechafin} </td></tr>
                <tr style='border-top:hidden;'><td colspan='3' style='border-top: hidden;border-bottom: hidden;'>&nbsp;</td></tr></table>";

                //Cuerpo del reporte
                $TR.="<table width='100%' border='1' style='font-size:11pt; position:relative; border-collapse:collapse; border-spacing:0px;'>
                <tr><td width='54%'>{$listadoEstatico[1]->texo} </td><td width='24%'>&nbsp;</td>
                    <td width='22%' align='right'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $listadoEstatico[1]->importe, true, true)}</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>SALDO EN BANCO SEG&Uacute;N LIBRO </td><td style='border-top: hidden;'>&nbsp;</td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $listadoEstatico[2]->importe, true, true)}</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>M&Aacute;S TRANSACCIONES NO CARGADAS </td><td style='border-top: hidden;'>&nbsp;</td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $listadoEstatico[3]->importe, true, true)}</td></tr>
               <!-- <tr style='border-top:hidden;'><td style='border-top: hidden;'>Pendiente a&ntilde;o anterior </td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $listadoEstatico[4]->importe, true, true)}</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>Pendiente a&ntilde;o actual </td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $listadoEstatico[5]->importe, true, true)}</td><td style='border-top: hidden;'>&nbsp;</td></tr> -->
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>TOTAL </td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $listadoEstatico[6]->importe, true, true)}</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>M&Aacute;S O (MENOS) </td><td style='border-top: hidden;'>&nbsp;</td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $listadoEstatico[7]->importe, true, true)}</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>SALDO SEG&Uacute;N ESTADO DE CUENTA BANCARIA </td><td style='border-top: hidden;'>&nbsp;</td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $listadoEstatico[8]->importe, true, true)}</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>DIFERENCIA ENTRE LIBRO Y BANCO </td><td style='border-top: hidden;'>&nbsp;</td>
                    <td align='right' style='border-top: hidden;'>{$this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $listadoEstatico[9]->importe, true, true)}</td></tr>
                <tr style='border-top:hidden;'><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td><td style='border-top: hidden;'>&nbsp;</td></tr>
                </table>";
            }else $TR.="";            
        }        
    }

    public function GetHtmlHeadTable($gData, $PAGECOUNT) {
        $this->pagina = $PAGECOUNT;
        return "<table width='100%' border='1' style='font-size:11pt; position:relative; border-collapse:collapse; border-spacing:0px;'>
            <tr><td height='43' colspan='3' align='center'><b>".strtoupper("$gData->codigoentidad $gData->descripcionentidad")."</b><br/>CONCILIACI&Oacute;N DE LAS CUENTAS BANCARIAS</td></tr>
            <tr><td colspan='3'>CUENTA BANCARIA DE: $gData->tipocuentabancaria</td></tr>";
    }
    
    public function getFootTable(){
        return "<table width='100%' border='1' style='font-size:11pt; position:relative; border-collapse:collapse; border-spacing:0px;'>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-right:hidden; border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            <tr style='border-top:hidden;'>
                <td style='border-right:hidden; border-top:hidden;' align='center'>{$this->footData}</td>
                <td style='border-right:hidden; border-top:hidden;' align='center'></td>
                <td align='center' style='border-top:hidden;'>&nbsp;</td>
               </tr>
            <tr style='border-top:hidden;'>
                <td style='border-right:hidden;border-top:hidden;' align='center'>____________________________</td>
                <td style='border-right:hidden;border-top:hidden;' align='center'>____________________________</td>
                <td align='center' style='border-top:hidden;'>____________________________</td>
            </tr>
            <tr style='border-top:hidden;'><td style='border-right:hidden;border-top:hidden;' align='center'>Confeccionado</td>
                <td style='border-right:hidden;border-top:hidden;' align='center'>Revisado</td><td align='center' style='border-top:hidden;'>Aprobado</td></tr>
            <tr style='border-top:hidden;'><td style='border-right:hidden;border-top:hidden;'>&nbsp;</td><td style='border-right:hidden;border-top:hidden;'>&nbsp;</td><td style='border-top:hidden;'>&nbsp;</td></tr>
            </table>";
    }
    
    public function GetNewObjectWithAdditionalData($argData) {
        return $argData->instrumentosT;
    }

    public function InstanceFactoryCreate($argBodyData) {
        $objReport = FactoryReport::Create('20002');
        $objReport->bodyData = $argBodyData;
        $objReport->codeSeparator = $this->codeSeparator;
        $objReport->gTitle = $this->gTitle . ' - ' . $objReport->gTitle;
        $objReport->generalData = $this->generalData;
        $objReport->footData = null;
        $objReport->REPORT_TYPE = $this->REPORT_TYPE;
        $objReport->REPORT_ORIENTATION = $this->REPORT_ORIENTATION;
        $objReport->REPORT_PAPERSIZE = $this->REPORT_PAPERSIZE;
        $objReport->FORMAT_PAPERSIZE = $this->FORMAT_PAPERSIZE;
        return $objReport;
    }

}