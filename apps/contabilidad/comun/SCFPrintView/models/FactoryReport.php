<?php

require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/ElementsReport.php");
require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/IEStrategyAdicionalPages.php");
require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/IEStrategyColumns.php");
require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/IEStrategyColumnsGrow.php");
require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/IEStrategySummary.php");
require_once(Zend_Registry::get('config')->dir_aplication . "/contabilidad/comun/SCFPrintView/comun/IEStrategySetSummary.php");

/*
 * reportes de Contabilidad 10000
 */

class FactoryReport
{

    public static function Create($argCase)
    {
        switch ($argCase) {
            case '10001' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptMayor/RptMayor.php");
                return new RptMayor();
            case '10002' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptSubMayor/RptSubMayor.php");
                return new RptSubMayor();
            case '10003' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptBalanceComprobacionSaldos/RptBalanceComprobacionSaldosEmpresarial.php");
                return new RptBalanceComprobacionSaldosEmpresarial();
            case '10004' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptComprobanteOperaciones/RptComprobanteOperaciones.php");
                return new RptComprobanteOperaciones();
            case '10005' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptEstadoFinanciero/RptEstadoFinanciero.php");
                return new RptEstadoFinanciero();
            case '10006' :
            case '10007' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptComprobantesPorEstado/RptComprobantesPorEstado.php");
                return new RptComprobantesPorEstado();
            case '10008' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Contabilidad/RptPlanCuenta/RptPlanCuenta.php");
                return new RptPlanCuenta();

            case '20001' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Banco/RptConciliacionBancaria/RptConciliacionBancaria.php");
                return new RptConciliacionBancaria();
            case '20002' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Banco/RptSaldoDisponibleBanco.php");
                return new RptSaldoDisponibleBanco();
            case '20003' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Banco/RptMovimientosBancarios.php");
                return new RptMovimientosBancarios();
            case '20004' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Banco/RptResumenSaldosBancarios.php");
                return new RptResumenSaldosBancarios();
            case '20005' :
                self::getRpt("/contabilidad/comun/SCFPrintView/models/Banco/RptChequesPosfechados.php");
                return new RptChequesPosfechados();
            default : die('EL REPORTE NO HA SIDO CONFIGURADO CORRECTAMENTE.');
        }
    }

    public static function getRpt($argPathRpt)
    {
        if (file_exists(Zend_Registry::get('config')->dir_aplication . $argPathRpt)) {
            require_once(Zend_Registry::get('config')->dir_aplication . $argPathRpt);
        } else {
            die("<b>ERROR</b>: EL MODELO PARA ESTE REPORTE NO HA SIDO ENCONTRADO.</br> <b>RUTA</b>:  $argPathRpt </br>");
        }
    }

}
