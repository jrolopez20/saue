<?php

class RptEstadoFinanciero extends ElementsReport implements IEStrategySummary
{

    private $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í",
        "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
    private $sInicial;
    private $sPeriodo;
    private $sAcumulado;
    private $sDiferencia;
    private $sEjercicioAnterior;

    public function __construct()
    {
        $this->fieldName = 'descripcionpartida';
        $this->textPageBreak = '<b>TOTAL</b>';
        $this->gTitle = 'ESTADO FINANCIERO';
    }

    public function GetBlankRows($argArrayColumns)
    {
        $TR_DIM = '';
        foreach ($agGeneralData->dimensiones as $idDimension) {
            if ($idDimension == 8030) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>";
            } elseif ($idDimension == 8031) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>";
            } elseif ($idDimension == 8032) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>";
            } elseif ($idDimension == 8033) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>";
            } elseif ($idDimension == 8034) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>";
            }
            return $TR_DIM;
        }
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        return $this->GetHtmlHeadTable($argArrayBodyData, $this->generalData, $argPageCount, $PAINTEDROWS);
    }

    public function GetHtmlHeadTable($agArrayBodyData, $agGeneralData, $PAGECOUNT, & $PAINTEDROWS)
    {
        $imageData = $this->generalData->logo;
        $this->gTitle = $this->generalData->titulo;
        $entidad = $agArrayBodyData[0]->descripcionestructura;
        $year = $agGeneralData->yearActual;
        $fDesde = $agGeneralData->fechaDesde;
        $fHasta = $agGeneralData->fechaHasta;
        $showColumnSI = 'none';
        $showColumnSP = 'none';
        $showColumnSA = 'none';
        $showColumnSD = 'none';
        $showColumnSEA = 'none';
        $TR_DIM = '';
        sort($agGeneralData->dimensiones);
        foreach ($agGeneralData->dimensiones as $idDimension) {
            if ($idDimension == 8030) {
                $showColumnSI = '';
                $TR_DIM .= "<td bgcolor=\"#D6D6D6\" width='10%' style='font-size: 9pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; text-align:center;'> SALDO INICIO </td>";
            } elseif ($idDimension == 8031) {
                $showColumnSP = '';
                $TR_DIM .= "<td bgcolor=\"#D6D6D6\" width='10%' style='font-size: 9pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; text-align:center;'> SALDO PER&Iacute;ODO </td>";
            } elseif ($idDimension == 8032) {
                $showColumnSA = '';
                $TR_DIM .= "<td bgcolor=\"#D6D6D6\" width='10%' style='font-size: 9pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; text-align:center;'> SALDO ACUMULADO </td>";
            } elseif ($idDimension == 8033) {
                $showColumnSD = '';
                $TR_DIM .= "<td bgcolor=\"#D6D6D6\" width='10%' style='font-size: 9pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; text-align:center;'> SALDO DIFERENCIA </td>";
            } elseif ($idDimension == 8034) {
                $showColumnSEA = '';
                $TR_DIM .= "<td bgcolor=\"#D6D6D6\" width='10%' style='font-size: 9pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; text-align:center;'> SALDO EJERCICIO ANTERIOR </td>";
            }
        }
        return "<table style='font-size:9pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;'>
                    <tr>
                        <td width='10%' rowspan='3'> <img width= '80' height='80' src='$imageData' style='border: 1px solid;'/></td>
                        <td colspan=\"3\" style='font-size:9pt; margin:1px; text-align:left;'><strong>Entidad:</strong>$entidad</td>
                        <td colspan=\"2\" style='font-weight: bold;font-size:10pt; text-align:right; vertical-align:top;'>No. $PAGECOUNT</td>
                    <tr>
                        <td height='49' colspan=\"6\" style='font-size: 12pt;text-align:left;'><b>" . strtr(strtoupper($this->gTitle),
                        $this->utf8) . "</b></td>
                    <tr>
                        <td colspan=\"2\" width='14%' style='text-align:left; vertical-align: top;'><b>A&Ntilde;O: </b>$year </td>
                        <td width='40%' colspan=\"2\" style='text-align:left; vertical-align: top;'><b>DESDE: </b>$fDesde</td>
                        <td width='40%' colspan=\"2\" style='text-align:left; vertical-align: top;'><b>HASTA: </b>$fHasta</td>
                </table>	
                <table style='font-size:9pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;'>
                    <tr>
                        <td bgcolor=\"#D6D6D6\" style='font-size: 9pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; text-align:center;'> Cuenta </td>" . $TR_DIM;
    }

    public function GetHtmlValues($agArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE),
                        $this->getFootTable_Empresarial($this->generalData->diaActual, $this->generalData->mesActual,
                                $this->generalData->yearActual, $this->generalData->usuario));
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(22, 40), array(30, 41), array(30, 49), array(30, 54));
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'descripcionpartida';
        return $this->CasePaperFormat(array(45, 45), array(41, 30), array(41, 36), array(41, 36));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        $TR_DIM = '';
        foreach ($this->generalData->dimensiones as $idDimension) {
            if ($idDimension == 8030) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                0, $argData->sinicial, $keepCero, $keepCero) . "</td>";
            } elseif ($idDimension == 8031) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                1, $argData->speriodo, $keepCero, $keepCero) . "</td>";
            } elseif ($idDimension == 8032) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                2, $argData->sacumulado, $keepCero, $keepCero) . "</td>";
            } elseif ($idDimension == 8033) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                3, $argData->sdiferencia, $keepCero, $keepCero) . "</td>";
            } elseif ($idDimension == 8034) {
                $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                4, $argData->sejercicioanterior, $keepCero, $keepCero) . "</td>";
            }
        }
        $TR .= (preg_match("/TOTAL /", $TR_TEXT) || preg_match("/<b>TOTAL  /", $TR_TEXT) || preg_match("/RESULTADO TOTAL /",
                        $TR_TEXT)) ?
                ("<tr>
                        <td style='font-weight:bold; text-align:left; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $TR_TEXT . "<b></td>$TR_DIM</tr>" )
                    :
                ("<tr>
                        <td style='text-align:left; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>$argData->descripcionpartida</td>$TR_DIM</tr>");
    }

    public function SetSumaAcumuladaFolio($TEXTALTERNATIVO, $TEXTSALDOFOLIO, $argArrayBodyData, $argFieldName, $i,
            $PREVIOUSPAGE, $CURRENTPAGE, &$FORMATPESOCOLUMNS)
    {
        $TRSALDOFOLIO = "";
        $this->sInicial = 0;
        $this->sPeriodo = 0;
        $this->sAcumulado = 0;
        $this->sAcumulado = 0;
        $this->sDiferencia = 0;
        $this->sEjercicioAnterior = 0;
        $TR_DIM = '';
        foreach ($argArrayBodyData as $j => $data) {
            if ($j <= $i) {
                if (!preg_match("<b>TOTAL", $data->$argFieldName)) {
                    $this->sInicial += $data->sinicial;
                    $this->sPeriodo += $data->speriodo;
                    $this->sAcumulado += $data->sacumulado;
                    $this->sDiferencia += $data->sdiferencia;
                    $this->sEjercicioAnterior += $data->sejercicioanterior;
                }
                if ($j == $i) {
                    if (isset($this->arraySumaAcumulada[$PREVIOUSPAGE])) {
                        if (count($this->arraySumaAcumulada[$PREVIOUSPAGE]) > 0) {
                            $this->sInicial += $this->arraySumaAcumulada[$PREVIOUSPAGE][1];
                            $this->sPeriodo += $this->arraySumaAcumulada[$PREVIOUSPAGE][2];
                            $this->sAcumulado += $this->arraySumaAcumulada[$PREVIOUSPAGE][3];
                            $this->sDiferencia += $this->arraySumaAcumulada[$PREVIOUSPAGE][4];
                            $this->sEjercicioAnterior += $this->arraySumaAcumulada[$PREVIOUSPAGE][5];
                        }
                    }
                    $this->arraySumaAcumulada[$CURRENTPAGE] = array($this->sInicial, $this->sPeriodo, $this->sAcumulado,
                        $this->sDiferencia, $this->sEjercicioAnterior);
                    foreach ($this->generalData->dimensiones as $idDimension) {
                        if ($idDimension == 8030) {
                            $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                            0, $this->sInicial, true) . "</td>";
                        } elseif ($idDimension == 8031) {
                            $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                            1, $this->sPeriodo, true) . "</td>";
                        } elseif ($idDimension == 8032) {
                            $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                            2, $this->sAcumulado, true) . "</td>";
                        } elseif ($idDimension == 8033) {
                            $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                            3, $this->sDiferencia, true) . "</td>";
                        } elseif ($idDimension == 8034) {
                            $TR_DIM .= "<td style='text-align:right; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                            4, $this->sEjercicioAnterior, true) . "</td>";
                        }
                    }
                    $TRSALDOFOLIO = "<tr><td style='text-align:left; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->textSummaryPage . "<b> " . $CURRENTPAGE . "</b>&nbsp;&nbsp;</td>$TR_DIM</tr>";
                }
            }
        }
        return $TRSALDOFOLIO;
    }

}
?>

