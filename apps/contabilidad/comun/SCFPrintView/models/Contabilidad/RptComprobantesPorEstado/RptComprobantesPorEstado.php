<?php

/**
 * Reporte Comprobantes por estado.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class RptComprobantesPorEstado extends ElementsReport
{

    private $gPageCount;

    public function __construct()
    {
        $this->fieldName = 'referencia';
        $this->gTitle = 'COMPROBANTES POR ESTADO';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return "";
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(26, 51), array(30, 47), array(29, 58), array(30, 63));
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        $this->gPageCount = $argPageCount;
        return $this->GetHtmlHeadTable($this->generalData, $this->gPageCount);
    }

    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE), $this->getFootTable());
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'referencia';
        return $this->CasePaperFormat(array(55, 31), array(55, 31), array(42, 31), array(42, 31));
    }

    public function GetHtmlHeadTable($generalData, $PAGECOUNT)
    {
        $imageData = $generalData->logo;
        $documentTitle = ($generalData->reporte == 10006) ? 'COMPROBANTES DE OPERACIONES CONTABILIZADOS' : 'COMPROBANTES DE OPERACIONES CON ERROR';
        return "<table width='100%' style='text-align:left; position:relative; border-collapse:collapse; 
            border-spacing:0px; font-size:10pt; border:none;'>
                    <tr>
                        <td width='10%' rowspan='3'> <img width= '80' height='80' src='$imageData' style='border: 1px solid;'/></td>
                        <td width='50%'><b>ENTIDAD:</b> $generalData->entidad</td>
                        <td width='30%' style='text-align:right;'> <b>No. $PAGECOUNT</b></td>
                    </tr> 
                    <tr>
                        <td colspan='2'><b>$documentTitle</b></td>
                    </tr> 
                    <tr>
                        <td colspan='2'><b>DESDE:</b> $generalData->fechaDesde &nbsp;&nbsp;<b>HASTA:</b> $generalData->fechaHasta</td>
                    </tr> 
                    <tr>
                        <td colspan='2'>&nbsp;</td>
                    </tr> 
               </table>
               
              <table width='100%' style='text-align:left; border-collapse:collapse; border-spacing:0px; font-size:10pt; border:1px solid;'>
                    <tr>
                        <td width='7%' style='border:1px solid;'><b>No.</b></td>
                        <td style='border:1px solid;'><b>REFERENCIA</b></td>
                        <td width='10%' style='border:1px solid;'><b>ESTADO</b></td>
                        <td width='10%' style='border:1px solid;'><b>FECHA</b></td>
                        <td width='16%' style='border:1px solid;'><b>USUARIO</b></td>
                        <td width='16%' style='border:1px solid;'><b>SUBSISTEMA</b></td>
                    </tr>";
    }

    public function RowValues($j, $TR_TEXT, & $TR, $argData, & $FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        $td = "<td style='border: 1px solid;'>";
        if ($j == 0) {
            $TR.="<tr>
                  $td $argData->numerocomprobante</td>
                  $td $TR_TEXT</td>
                  $td $argData->estado</td>
                  $td $argData->fechaemision</td>
                  $td $argData->usuario</td>
                  $td $argData->subsistema</td>
                </tr>";
        } else {
            $TR.= "<tr>
                  $td</td>
                  $td $TR_TEXT</td>
                  $td</td>
                  $td</td>
                  $td</td>
                  $td </td>
                </tr>";
        }
    }

    public function getFootTable()
    {
        return "";
    }

}
