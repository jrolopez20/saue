<?php

/**
 * Reporte Plan de cuentas.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class RptPlanCuenta extends ElementsReport
{

    private $gPageCount;

    public function __construct()
    {
        $this->fieldName = 'nombre';
        $this->gTitle = 'PLAN DE CUENTAS CONTABLES';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return "";
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(26, 51), array(30, 47), array(29, 58), array(30, 63));
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        $this->gPageCount = $argPageCount;
        return $this->GetHtmlHeadTable($this->generalData, $this->gPageCount);
    }

    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE), $this->getFootTable());
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'nombre';
        return $this->CasePaperFormat(array(55, 31), array(55, 31), array(42, 31), array(42, 31));
    }

    public function GetHtmlHeadTable($generalData, $PAGECOUNT)
    {
        $imageData = $generalData->logo;
        return "<table width='100%' style='text-align:left; position:relative; border-collapse:collapse; 
            border-spacing:0px; font-size:10pt; border:none;'>
                    <tr>
                        <td width='10%' rowspan='2'> <img width= '80' height='80' src='$imageData' style='border: 1px solid;'/></td>
                        <td width='50%'> <b>ENTIDAD:</b> $generalData->entidad</td>
                        <td width='40%' style='text-align:right;'> <b>No. $PAGECOUNT</b></td>
                    </tr> 
                    <tr>
                        <td colspan='2' style='vertical-align:top;'><b>PLAN DE CUENTAS CONTABLES</b></td>
                    </tr> 
                    <tr>
                        <td colspan='2'>&nbsp;</td>
                    </tr> 
               </table>
               
              <table width='100%' style='text-align:left; border-collapse:collapse; border-spacing:0px; font-size:10pt; border:1px solid;'>
                    <tr>
                        <td width='80%' style='border:1px solid;'><b>NOMBRE DE LA CUENTA</b></td>
                        <td width='20%' style='border:1px solid;'><b>NATURALEZA</b></td>
                    </tr>";
    }

    public function RowValues($j, $TR_TEXT, & $TR, $argData, & $FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        $td = "<td style='border: 1px solid;'>";
        if ($j == 0) {
            $TR.="<tr>
                  $td $TR_TEXT</td>
                  $td $argData->naturaleza</td>
                </tr>";
        } else {
            $TR.= "<tr>
                  $td $TR_TEXT</td>
                  $td</td>
                </tr>";
        }
    }

    public function getFootTable()
    {
        return "";
    }

}
