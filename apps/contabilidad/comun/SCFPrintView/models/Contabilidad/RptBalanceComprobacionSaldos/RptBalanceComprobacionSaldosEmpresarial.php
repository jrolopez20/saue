<?php

/**
 * Reporte Balance Comprobacion de Saldos
 * @author Team Delta
 * @version 1.0
 */
class RptBalanceComprobacionSaldosEmpresarial extends ElementsReport implements IEStrategySummary
{

    private $gsaldoinicio;
    private $gparcial;
    private $gdebe;
    private $ghaber;
    private $gsaldoperiodo;
    private $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í",
        "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");

    public function __construct()
    {
        $this->fieldName = 'descripcioncuenta';
        $this->textPageBreak = '<b>TOTAL</b>';
        $this->gTitle = 'BALANCE COMPROBACI&Oacute;N DE SALDOS';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return
                "<tr>
            <td style='text-align:left;'>&nbsp;</td>
            <td style='text-align:left;'>&nbsp;</td>
            <td style='text-align:left;'>&nbsp;</td>
            <td style='text-align:left;'>&nbsp;</td>
            <td style='text-align:left;'>&nbsp;</td>
            <td style='text-align:left;'>&nbsp;</td>
            <td style='text-align:left;'>&nbsp;</td>
        </tr>";
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        return $this->GetHtmlHeadTable($argArrayBodyData[1]->descripcionestructura, $this->generalData->fechaDesde,
                        $this->generalData->fechaHasta, $argPageCount, $PAINTEDROWS, $argArrayBodyData);
    }

    public function GetHtmlHeadTable($Entidad, $FECHADESDE, $FECHAHASTA, $PAGECOUNT, & $PAINTEDROWS, $argArrayBodyData)
    {
        $TR = '';
        if (isset($argArrayBodyData[1]->CONSOLIDANDO)) {
            $TR = "<tr><td colspan='3' style='text-align:left; vertical-align: top; width:100%; height: 40px;'><b>CONSOLIDANDO:</b> " . $argArrayBodyData[1]->CONSOLIDANDO . "</td></tr>";
            $PAINTEDROWS += 2;
        }
        $imageData = $this->generalData->logo;
        return "<table style='font-size:9pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;'>
             <tr>
                <td width='10%' rowspan='3'> <img width='80' height='80' src='$imageData' style='border: 1px solid;'/></td>
                <td colspan='5' style='font-size:9pt; margin:1px; text-align:left;'><strong>ENTIDAD</strong>:" . strtr(strtoupper($Entidad),
                        $this->utf8) . "</td>
                <td width='20%' style='font-weight: bold;font-size:10pt; text-align:right; vertical-align:top;'>No. $PAGECOUNT</td>
             </tr>
             <tr>
               <td height='49' colspan=3 style='text-align:left;'><b>BALANCE COMPROBACI&Oacute;N DE SALDOS</b></td>
             </tr>
                $TR
              <tr>
                      <td width='15%' style='text-align:left; vertical-align: top;'><b>DESDE: </b>$FECHADESDE</td>
                      <td colspan='2' style='text-align:left; vertical-align: top;'><b>HASTA: </b>$FECHAHASTA</td>
              </tr>
              <tr>
                    <td colspan='5'>&nbsp;</td>
                </tr>
            </table>	
                <table style='font-size:9pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;'>
                    <tr>
                        <td width='10%' style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:left;'>C&Oacute;DIGO</td>
                        <td style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:left;'>DESCRIPCI&Oacute;N</td>
                        <td width='10%' style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:right;'>SALDO INICIO</td>
                        <td width='12%' style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:right;'>PARCIAL</td>
                        <td width='10%' style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:right;'>DEBE</td>
                        <td width='10%' style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:right;'>HABER</td>
                        <td width='10%' style='font-size: 8pt; font-weight:bold; border-top: 1px solid; border-bottom: 1px solid; text-align:right;'>SALDO PER&Iacute;ODO</td>
                    </tr>";
    }

    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE),
                        $this->getFootTable_Empresarial($this->generalData->diaActual, $this->generalData->mesActual,
                                $this->generalData->yearActual, $this->generalData->usuario));
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(21, 38), array(23, 35), array(23, 42), array(23, 47));
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'descripcioncuenta';
        return $this->CasePaperFormat(array(250, 30), array(41, 30), array(41, 36), array(41, 36));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        $TR .= ($TR_TEXT == "<b>CUENTAS DE ORDEN</b>" || $TR_TEXT == "<b>CUENTAS PATRIMONIALES</b>" || $TR_TEXT == "<b>SUBTOTAL</b>"
                || $TR_TEXT == "<b>TOTAL</b>") ?
                "<tr>
                <td colspan = '2' style='text-align:center;'>$TR_TEXT</td>
                <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 0, $argData->saldoinicio,
                        $keepCero, $keepCero) . "</td>
                <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 1, $argData->parcial,
                        $keepCero, $keepCero) . "</td>
                <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $argData->debe,
                        $keepCero, $keepCero) . "</td>
                <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 3, $argData->haber,
                        $keepCero, $keepCero) . "</td>
                <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 4, $argData->saldoperiodo,
                        $keepCero, $keepCero) . "</td>
            </tr>" : (($j == 0) ?
                        "<tr>
                <td style='text-align:left; border-bottom:1px solid !important; border-top:1px solid !important;'>$argData->concatcuenta</td>
                <td style='text-align:left; border-bottom:1px solid !important; border-top:1px solid !important;'>$TR_TEXT</td>
                <td style='text-align:right; border-bottom:1px solid !important; border-top:1px solid !important;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                0, $argData->saldoinicio, $keepCero, $keepCero) . "</td>
                <td style='text-align:right; border-bottom:1px solid !important; border-top:1px solid !important;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                1, $argData->parcial, $keepCero, $keepCero) . "</td>
                <td style='text-align:right; border-bottom:1px solid !important; border-top:1px solid !important;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                2, $argData->debe, $keepCero, $keepCero) . "</td>
                <td style='text-align:right; border-bottom:1px solid !important; border-top:1px solid !important;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                3, $argData->haber, $keepCero, $keepCero) . "</td>
                <td style='text-align:right; border-bottom:1px solid !important; border-top:1px solid !important;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                4, $argData->saldoperiodo, $keepCero, $keepCero) . "</td>
            </tr>" :
                        "<tr>
                <td style='text-align:left; border-bottom:1px solid !important;'></td>
                <td style='text-align:left; border-bottom:1px solid !important;'>$TR_TEXT</td>
                <td style='text-align:right; border-bottom:1px solid !important;'></td>
                <td style='text-align:right; border-bottom:1px solid !important;'></td>
                <td style='text-align:right; border-bottom:1px solid !important;'></td>
                <td style='text-align:right; border-bottom:1px solid !important;'></td>
                <td style='text-align:right; border-bottom:1px solid !important;'></td>
            </tr>");
    }

    public function SetSumaAcumuladaFolio($TEXTALTERNATIVO, $TEXTSALDOFOLIO, $argArrayBodyData, $argFieldName, $i,
            $PREVIOUSPAGE, $CURRENTPAGE, &$FORMATPESOCOLUMNS)
    {
        $TRSALDOFOLIO = "";
        $this->gsaldoinicio = 0;
        $this->gparcial = 0;
        $this->gdebe = 0;
        $this->ghaber = 0;
        $this->gsaldoperiodo = 0;
        foreach ($argArrayBodyData as $j => $data) {
            if ($j <= $i) {
                if ($data->$argFieldName != '<b>CUENTAS DE ORDEN</b>' || $data->$argFieldName != '<b>CUENTAS PATRIMONIALES</b>'
                        || $data->$argFieldName != '<b>SUBTOTAL</b>' || $data->$argFieldName != '<b>TOTAL</b>') {
                    $this->gsaldoinicio += $data->saldoinicio;
                    $this->gparcial += $data->parcial;
                    $this->gdebe += $data->debe;
                    $this->ghaber += $data->haber;
                    $this->gsaldoperiodo += $data->saldoperiodo;
                }
                if ($j == $i) {
                    if (isset($this->arraySumaAcumulada[$PREVIOUSPAGE])) {
                        if (count($this->arraySumaAcumulada[$PREVIOUSPAGE]) > 0) {
                            $this->gsaldoinicio += $this->arraySumaAcumulada[$PREVIOUSPAGE][0];
                            $this->gparcial += $this->arraySumaAcumulada[$PREVIOUSPAGE][1];
                            $this->gdebe += $this->arraySumaAcumulada[$PREVIOUSPAGE][2];
                            $this->ghaber += $this->arraySumaAcumulada[$PREVIOUSPAGE][3];
                            $this->gsaldoperiodo += $this->arraySumaAcumulada[$PREVIOUSPAGE][4];
                        }
                    }
                    $this->arraySumaAcumulada[$CURRENTPAGE] = array($this->gsaldoinicio, $this->gparcial, $this->gdebe, $this->ghaber,
                        $this->gsaldoperiodo);
                    $TRSALDOFOLIO = "<tr>
                                        <td colspan = '2' style='text-align:right;'>$this->textSummaryPage <b>$CURRENTPAGE</b>&nbsp;&nbsp;</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 0,
                                    $this->gsaldoinicio, true) . "</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 1,
                                    $this->gparcial, true) . "</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 2,
                                    $this->gdebe, true) . "</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 3,
                                    $this->ghaber, true) . "</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 4,
                                    $this->gsaldoperiodo, true) . "</td>
                                    </tr>";
                }
            }
        }
        return $TRSALDOFOLIO;
    }

}

?>