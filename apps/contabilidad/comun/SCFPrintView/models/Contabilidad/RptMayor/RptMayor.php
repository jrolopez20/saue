<?php

/**
 * Reporte Mayor.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class RptMayor extends ElementsReport implements IEStrategySummary
{

    private $gdebe;
    private $ghaber;
    private $gsaldo;

    public function __construct()
    {
        $this->fieldName = 'descripcion';
        $this->textPageBreak = '<b>SUMA ACUMULADA</b>';
        $this->gTitle = 'MAYOR';
    }

    public function GetBlankRows($argArrayColumns)
    {
        return "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        $this->gPageCount = $argPageCount;
        return $this->GetHtmlHeadTable($this->generalData, $argPageCount, $argArrayBodyData[1]->clavecuenta, $argArrayBodyData[1]->descripcioncuenta);
    }

    public function GetHtmlHeadTable($generalData, $PAGECOUNT, $CLAVECUENTA, $DESCRIPCIONCUENTA)
    {
        $imageData = $generalData->logo;
        return "<table style='font-size:10pt; position:relative; border-collapse:collapse; border-spacing:0px; width:100%;' >
                    <tr>
                      <td width='10%' rowspan='3'> <img width='80' height='80' src='$imageData' style='border: 1px solid;'/></td>
                      <td colspan='5' style='vertical-align:top'><b>ENTIDAD</b>: $generalData->entidad</td>
                      <td width='13%' style='font-weight: bold;font-size:10pt; text-align:right; vertical-align:top;'>No. $PAGECOUNT</td>
                    </tr>
                    <tr>
                      <td height='33' colspan='7' style='font-size:12pt; text-align:left; font-weight:bold' >$this->gTitle</td>
                    </tr>
                    <tr>
                      <td colspan='2' style='text-align:left;'><b>DESDE:</b>&nbsp;$generalData->fechaDesde&nbsp;&nbsp;&nbsp;</td>
                      <td colspan='4' style='text-align:left;'><b>HASTA:</b>&nbsp;$generalData->fechaHasta</td>
                    </tr>
                    <tr>
                        <td colspan='6'>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan='3' style='text-align:left;'><b>C&Oacute;DIGO</b></td>
                        <td colspan='4' style='font-weight:bold;'>NOMBRE DE LA CUENTA</td>
                    </tr>
                    <tr>
                        <td width='16%' colspan='3' style='text-align:left;'>$CLAVECUENTA</td>
                        <td colspan='4'>$DESCRIPCIONCUENTA</td>
                    </tr>
                    <tr style='border-bottom: 1px solid !important;border-top: 1px solid !important;'>
                        <td width='6%' colspan='2' style='text-align:center; border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'><b>A&Ntilde;O</b></td>
                        <td width='16%' rowspan='3' style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'><b>No. <br>COMPROBANTE</b></td>
                        <td rowspan='3' style='text-align:left; border-top: 1px solid; border-bottom: 1px solid;'><b>DESCRIPCI&Oacute;N</b></td>
                        <td width='17%' rowspan='3' style='text-align:right; border-top: 1px solid; border-bottom: 1px solid;'><b>DEBE</b></td>
                        <td width='17%' rowspan='3' style='text-align:right; border-top: 1px solid; border-bottom: 1px solid;'><b>HABER</b></td>
                        <td width='17%' rowspan='3' style='text-align:right; border-top: 1px solid; border-bottom: 1px solid;'><b>SALDO</b></td>
                    </tr>
                    <tr>
                        <td colspan='2' style='text-align:center; border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>$generalData->year</td>
                    </tr>
                    <tr>
                        <td width='3%' style='text-align:center; border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'><b>D</b></td>
                        <td width='3%' style='text-align:center; border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'><b>M</b></td>
                    </tr>";
    }

    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE));
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(27, 46), array(30, 43), array(30, 52), array(31, 57));
    }

    public function LengthStringPaperFormat(&$argField)
    {
        return $this->CasePaperFormat(array(100, 22), array(32, 26), array(33, 25), array(39, 23));
    }

    public function RowValues($j, $TR_TEXT, &$TR, $argData, &$FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        $TRSUMA = "";
        $TRSUMA .= ($TR_TEXT == $this->textPageBreak) ?
                "<td colspan = '4' style='text-align:right; border-top: 1px solid;'>$TR_TEXT&nbsp;</td>
          <td style='text-align:right; border-top: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 0, $argData->debe, true, true) . "</td>
          <td style='text-align:right; border-top: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 1, $argData->haber, true, true) . "</td>
          <td style='text-align:right; border-top: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $argData->saldo, true, true) . "</td>
          " : "<td colspan = '4' style='text-align:right;'>$TR_TEXT&nbsp;</td>
          <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 0, $argData->debe, true, true) . "</td>
          <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 1, $argData->haber, true, true) . "</td>
          <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $argData->saldo, true, true) . "</td>";
        $TR .= ($TR_TEXT == "<b>SALDO ANTERIOR</b>" || $TR_TEXT == $this->textPageBreak) ?
                "<tr>
          $TRSUMA
          </tr>" : (($j == 0) ? "<tr>
          <td style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'>$argData->dia</td>
          <td style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'>$argData->mes</td>
          <td style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;&nbsp;$argData->comprobante</td>
          <td style='text-align:left; border-top: 1px solid; border-bottom: 1px solid;'>$TR_TEXT</td>
          <td style='text-align:right; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 0, $argData->debe, $keepCero, $keepCero) . "</td>
          <td style='text-align:right; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 1, $argData->haber, $keepCero, $keepCero) . "</td>
          <td style='text-align:right; border-top: 1px solid; border-bottom: 1px solid;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $argData->saldo, true, $keepCero) . "</td>
          </tr>" : "<tr>
          <td style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>
          <td style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>
          <td style='text-align:center; border-top: 1px solid; border-bottom: 1px solid;'>&nbsp;</td>
          <td style='text-align:left; border-bottom: 1px solid;'>$TR_TEXT</td>
          <td style='text-align:right; border-bottom: 1px solid;'></td>
          <td style='text-align:right; border-bottom: 1px solid;'></td>
          <td style='text-align:right; border-bottom: 1px solid;'></td>
          </tr>");
    }

    public function SetSumaAcumuladaFolio($TEXTALTERNATIVO, $TEXTSALDOFOLIO, $argArrayBodyData, $argFieldName, $i, $PREVIOUSPAGE, $CURRENTPAGE, &$FORMATPESOCOLUMNS)
    {
        $TRSALDOFOLIO = "";
        $this->gdebe = 0;
        $this->ghaber = 0;
        $this->gsaldo = 0;

        foreach ($argArrayBodyData as $j => $data)
            if ($j <= $i) {
                if ($data->$argFieldName != $this->textPageBreak || $data->$argFieldName != '<b>SALDO ANTERIOR</b>') {
                    $this->gdebe += $data->debe;
                    $this->ghaber += $data->haber;
                    $this->gsaldo += $data->saldo;
                }
                if ($j == $i) {
                    if (isset($this->arraySumaAcumulada[$PREVIOUSPAGE]))
                        if (count($this->arraySumaAcumulada[$PREVIOUSPAGE]) > 0) {
                            $this->gdebe += $this->arraySumaAcumulada[$PREVIOUSPAGE][0];
                            $this->ghaber += $this->arraySumaAcumulada[$PREVIOUSPAGE][1];
                            $this->gsaldo += $this->arraySumaAcumulada[$PREVIOUSPAGE][2];
                        }
                    $this->arraySumaAcumulada[$CURRENTPAGE] = array($this->gdebe, $this->ghaber, $this->gsaldo);
                    $TRSALDOFOLIO = "<tr>
                                        <td colspan = '4' style='text-align:right;'>$this->textSummaryPage $CURRENTPAGE&nbsp;&nbsp;</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 0, $this->gdebe, true) . "</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 1, $this->ghaber, true) . "</td>
                                        <td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS, 2, $this->gsaldo, true) . "</td>
                                    </tr>";
                }
            }
        return $TRSALDOFOLIO;
    }

}
