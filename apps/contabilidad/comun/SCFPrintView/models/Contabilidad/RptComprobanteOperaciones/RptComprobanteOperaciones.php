<?php

/**
 * Reporte Comprobante de Operaciones.
 * 
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class RptComprobanteOperaciones extends ElementsReport implements IEStrategySetSummary
{

    private $gDebe;
    private $gHaber;
    private $gPageCount;

    public function __construct()
    {
        $this->fieldName = 'descripcion';
        $this->textPageBreak = '<b>TOTAL</b>';
        $this->gTitle = 'COMPROBANTE DE OPERACIONES';
    }

    public function NumberRowsPaperFormat()
    {
        return $this->CasePaperFormat(array(26, 51), array(30, 47), array(29, 58), array(30, 63));
    }

    public function LengthStringPaperFormat(& $argField)
    {
        $argField = 'descripcion';
        return $this->CasePaperFormat(array(42, 31), array(31, 31), array(42, 31), array(42, 31));
    }

    public function GetHeadTable(&$PAINTEDROWS, $argArrayBodyData, $argPageCount, &$argArrayColumns)
    {
        $this->gPageCount = $argPageCount;
        return $this->GetHtmlHeadTable($argArrayBodyData[0]->descripcionestructura, $argArrayBodyData[0]->fechaemision);
    }

    public function GetHtmlHeadTable($ENTIDAD, $FECHAEMISION)
    {
        $imageData = $this->generalData->logo;
        list($dia, $mes, $year) = array_reverse(explode('-', $FECHAEMISION));
        return "<table width='100%' style='text-align:center; vertical-align:middle; position:relative; border-collapse:collapse; border-spacing:0px; font-size:9pt;  margin:0px;'>
                 <tr>
                    <td width='10%' rowspan='3'> <img width= '80' height='80' src='$imageData' style='border: 1px solid;'/></td>
                    <td style='text-align:left; font-size:10pt;'><b>ENTIDAD: </b>$ENTIDAD</td>
                 </tr> 
                 <tr>
                    <td colspan='4' style='text-align:left; vertical-align:middle; font-size:10pt; font-weight:bold;'>COMPROBANTE DE OPERACIONES</td>
                    <td width='2%' style='text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>D</td>
                    <td width='2%' style='text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>M</td>
                    <td width='4%' style='text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>A</td>
                  </tr>
                  <tr>
                    <td colspan='4'></td>
                    <td>$dia</td>
                    <td>$mes</td>
                    <td>$year</td>
                  </tr>
               </table>
              <table width='100%' style='text-align:center; vertical-align:middle; position:relative; border-collapse:collapse; border-spacing:0px; font-size:9pt;  margin:0px;'>
              <tr style='border-bottom: 1px solid !important;border-top: 1px solid !important;'>
                <td width='7%' height='40' style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>CUENTA<br>No.</td>
                <td width='8%' style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>SUBCUENTA<br>No.</td>
                <td style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>CONCEPTO</td>
                <td width='16%' style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:center; vertical-align:middle; font-size:10pt; font-weight:bold;'>ANÁLISIS</td>
                <td width='14%' style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:right; vertical-align:middle; font-size:10pt; font-weight:bold;'>PARCIAL</td>
                <td width='14%' style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:right; vertical-align:middle; font-size:10pt; font-weight:bold;'>DEBE</td>
                <td width='14%' style='border-bottom: 1px solid !important;border-top: 1px solid !important; text-align:right; vertical-align:middle; font-size:10pt; font-weight:bold;'>HABER</td>
              </tr>";
    }

    public function GetSumaAcumuladaFolio($TEXTALTERNATIVO, $TEXTSALDOFOLIO, & $FORMATPESOCOLUMNS, $PREVIOUSPAGE)
    {
        return "<tr style='border-top:1px solid !important;'><td colspan = '5' style='border-top:1px solid !important;text-align:left;'>$TEXTSALDOFOLIO</td><td style='border-top:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                        1, $this->arraySumaAcumulada[$PREVIOUSPAGE][1], true) . "</td><td style='border-top:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                        2, $this->arraySumaAcumulada[$PREVIOUSPAGE][2], true) . "</td></tr>";
    }

    public function SetSumaAcumuladaFolio($TEXTALTERNATIVO, $TEXTSALDOFOLIO, $argArrayBodyData, $argFieldName, $i,
            $PREVIOUSPAGE, $CURRENTPAGE, & $FORMATPESOCOLUMNS)
    {
        $TRSALDOFOLIO = "";
        $this->gDebe = 0;
        $this->gHaber = 0;
        foreach ($argArrayBodyData as $j => $data)
                if ($j <= $i) {
                if (!$this->IsSubTotalRow($data->$argFieldName)) {
                    $this->gDebe += $data->debe;
                    $this->gHaber += $data->haber;
                }
                if ($j == $i) {
                    if (isset($this->arraySumaAcumulada[$PREVIOUSPAGE]))
                            if (count($this->arraySumaAcumulada[$PREVIOUSPAGE]) > 0) {
                            $this->gDebe += $this->arraySumaAcumulada[$PREVIOUSPAGE][0];
                            $this->gHaber += $this->arraySumaAcumulada[$PREVIOUSPAGE][1];
                        }
                    $this->arraySumaAcumulada[$CURRENTPAGE] = array(1 => $this->gDebe, 2 => $this->gHaber);
                    $TRSALDOFOLIO = "<tr style='border-bottom:1px solid !important;'><td colspan = '5' style='border-bottom:1px solid !important; text-align:left;'>$TEXTSALDOFOLIO</td><td style='border-bottom:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                    0, $this->gDebe, true) . "</td><td style='border-bottom:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                    1, $this->gHaber, true) . "</td></tr>";
                }
            }
        return $TRSALDOFOLIO;
    }

    public function RowValues($j, $TR_TEXT, & $TR, $argData, & $FORMATPESOCOLUMNS, $keepCero, $argArrayColumns, $ENDPAGE)
    {
        $TR .= ($keepCero) ? "<tr><td>&nbsp;</td><td>&nbsp;</td><td style='text-align:left;'>$TR_TEXT</td><td>&nbsp;</td><td>&nbsp;</td><td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                        1, $argData->debe, true) . "</td><td style='text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                        2, $argData->haber, true) . "</td></tr>" : (($j == 0) ? " <tr><td style='border-bottom:1px solid !important;'>" . $argData->cuenta . "</td><td style='border-bottom:1px solid !important;'>" . $argData->subcuenta . "</td><td style='border-bottom:1px solid !important; text-align:left;'>$TR_TEXT</td><td style='border-bottom:1px solid !important; text-align:left;'>" . $argData->analisis . "</td><td style='border-bottom:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                0, $argData->parcial, $keepCero) . "</td><td style='border-bottom:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                1, $argData->debe, $keepCero) . "</td><td style='border-bottom:1px solid !important; text-align:right;'>" . $this->ColumnsFormat($FORMATPESOCOLUMNS,
                                2, $argData->haber, $keepCero) . "</td></tr>" : "<tr><td style='border-bottom:1px solid !important;'>&nbsp;</td><td style='border-bottom:1px solid !important;'>&nbsp;</td><td style='border-bottom:1px solid !important; text-align:left;'>$TR_TEXT</td><td style='border-bottom:1px solid !important;'>&nbsp;</td><td style='border-bottom:1px solid !important;'>&nbsp;</td><td style='border-bottom:1px solid !important;'>&nbsp;</td><td style='border-bottom:1px solid !important;'>&nbsp;</td></tr>");
    }

    public function GetBlankRows($argArrayColumns)
    {
        return " <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
    }

    public function GetHtmlValues($argArrayBodyData, $agHeader, $TRBODYTABLE)
    {
        return $this->GetHTMLBodyFormat($agHeader, $this->GetBodyTable($TRBODYTABLE),
                        $this->getFootTable($argArrayBodyData[0]->descripcomp, $argArrayBodyData[0]->confecciona,
                                $argArrayBodyData[0]->asienta, $argArrayBodyData[0]->numerocomprobante));
    }

    public function getFootTable($OBSERVACION, $CONFECCIONADO, $ASENTADO, $NUMCOMPROBANTE)
    {
        return "<table width='100%' style='border-top:1px solid; text-align:center; vertical-align:middle; position:relative; border-collapse:collapse; border-spacing:0px; font-size:9pt;  margin:0px;'>
                  <tr>
                    <td height='40' colspan='5' style=' text-align:left; vertical-align:top; font-size:10pt;'><b>OBSERVACI&Oacute;N:</b> $OBSERVACION</td>
                  </tr>
                  <tr>
                    <td width='25%' height='60' style='text-align:left; vertical-align:top; font-size:10pt;'><b>CONFECCIONADO POR:</b> <br> $CONFECCIONADO</td>
                    <td width='25%' style='text-align:left; vertical-align:top; font-size:10pt;'><b>ASENTADO POR:</b> <br> $ASENTADO</td>
                    <td style='text-align:left; vertical-align:top; font-size:10pt;'><b>APROBADO POR:</b> <br> </td>
                    <td width='20%' style='text-align:center; vertical-align:top; font-size:10pt;'><b>COMPROBANTE<br>No.</b><br> $NUMCOMPROBANTE</td>
                    <td width='8%' style='text-align:center; vertical-align:top; font-size:10pt;'><b>FOLIO<br>No.</b><br> $this->gPageCount </td>
                  </tr>
                </table>";
    }

}
