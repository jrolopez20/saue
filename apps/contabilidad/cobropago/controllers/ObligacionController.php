<?php

/**
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ObligacionController extends ZendExt_Controller_Secure
{

    private $model;
    public function init()
    {
        parent::init();
        $this->model = new DatObligacionModel();
    }

    public function obligacionAction()
    {
        $this->render();
    }

    public function getObligacionesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');

        $r = $this->model->getObligacionesCobroPago($limit, $start);
//        $r = [
//            [
//                'idobligacion' => '134',
//                'numero' => '341',
//                'fechaemision' => '1/01/2015',
//                'idclientesproveedores' => '342',
//                'clienteproveedor' => 'Javier Rodriguez Lopez',
//                'comentario' => 'Dsd asdasd',
//                'estado' => '1',
//                'idtipodocumento' => '1',
//                'importe' => '89.36',
//                'liquidado' => '78.5',
//                'tipo' => 'Cuenta por cobrar',
//            ]
//        ];

        echo json_encode($r);
    }

}