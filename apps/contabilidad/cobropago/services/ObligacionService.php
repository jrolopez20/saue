<?php

class ObligacionService
{

    /**
     * Obtiene los datos del comprobante de operaciones.
     * @param Array $argparams
     * @return Array
     */
    public function adicionarObligacion($params)
    {
        $model = new DatObligacionModel();
        return $model->adicionarObligacion($params);
    }


    public function modificarObligacion($params)
    {
        $model = new DatObligacionModel();
        return $model->modificarObligacion($params);
    }

    /**
     * Obtiene las cuentas por pagar.
     * @param Array $argparams
     * @return Array
     */
    public function obtenerCuentasPorPagar($idestructura, $idproveedor = 0 , $estados = array(1,2,3,4))
    {
        $est = implode(',', $estados);
        $conditions = array('idestructura = ?','tipo = ?', 'estadodocumento IN('.$est.')');
        $params = array($idestructura, 2);

        if($idproveedor){
            $conditions[] = 'idclientesproveedores = ?';
            $params[] = $idproveedor;
        }

        $conditions = implode(' AND ', $conditions);

        $data = DatObligacion::GetByCondition($conditions, $params, TRUE);

        return $data;
    }

    /**
     * Obtiene las cuentas por cobrar.
     * @param Array $argparams
     * @return Array
     */
    public function obtenerCuentasPorCobrar($idestructura, $idcliente = 0, $estados = array(1,2,3,4))
    {
        $est = implode(',', $estados);
        $conditions = array('idestructura = ?','tipo = ?', 'estadodocumento IN('.$est.')');
        $params = array($idestructura, 1);

        if($idcliente){
            $conditions[] = 'idclientesproveedores = ?';
            $params[] = $idcliente;
        }

        $conditions = implode(' AND ', $conditions);

        $data = DatObligacion::GetByCondition($conditions, $params, TRUE);

        return $data;
    }

    public function obtenerObligacion($tipo, $numero, $idtipodocumento)
    {
        $model = new DatObligacionModel();
        return $model->obtenerObligacion($tipo, $numero, $idtipodocumento);
    }

    public function obtenerDerechoObligacionPorId($id)
    {
        return DatObligacion::GetById($id, TRUE);
    }

}
