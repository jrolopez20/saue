<?php

class DatObligacion extends BaseDatObligacion
{

    public function setUp()
    {
        parent:: setUp();
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->idobligacion;

        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('DatObligacion')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatObligacion')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idobligacion, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatObligacion')->find($idobligacion);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatObligacion')
                ->whereIn('idobligacion', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatObligacion')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            echo "{'success':false, 'codMsg':3, 'mensaje':'". $e->getMessage()."'}";
        }
    }

    public function getObligacionesCobroPago($limit, $start)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $conn = $objDoctrine->getCurrentConnection();

        $sql = "SELECT numero, comentario, fechaemision, fecharegistro,
                importe, liquidado, idsubsistema, estadodocumento, estado,
                (CASE WHEN o.tipo = 1 THEN 'Cuenta por Cobrar' ELSE 'Cuenta por Pagar' END) as tipo,
                idtipodocumento, idperiodo, nocomprobante, c.codigo as codigoclienteproveedor, c.nombre as clienteproveedor,
                tp.nombre as terminodepago
            FROM mod_cobropago.dat_obligacion o
            INNER JOIN mod_maestro.nom_clientesproveedores c ON o.idclientesproveedores = c.idclientesproveedores
            LEFT JOIN mod_facturacion.nom_terminosdepago tp ON o.idterminodepago = tp.idterminodepago";

        $sql .= " ORDER BY o.fechaemision DESC";

        if ($limit) {
            $sql .= " LIMIT {$limit}";
        }
        if ($start) {
            $sql .= " OFFSET {$start}";
        }

        return $conn->fetchAll($sql);
    }

    public function obtenerObligacion($tipo, $numero, $idtipodocumento)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $conn = $objDoctrine->getCurrentConnection();

        $sql = "SELECT numero, comentario, fechaemision, fecharegistro,
                importe, liquidado, idsubsistema, estadodocumento, estado,
                (CASE WHEN o.tipo = 1 THEN 'Cuenta por Cobrar' ELSE 'Cuenta por Pagar' END) as tipo,
                idtipodocumento, idperiodo, nocomprobante
            FROM mod_cobropago.dat_obligacion o WHERE 1=1 ";

        if ($tipo) {
            $sql .= " AND o.tipo={$tipo}";
        }
        if ($numero) {
            $sql .= " AND o.numero='{$numero}'";
        }
        if ($idtipodocumento) {
            $sql .= " AND o.idtipodocumento={$idtipodocumento}";
        }

        return $conn->fetchAll($sql);
    }
}