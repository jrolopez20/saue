<?php

abstract class BaseDatObligacion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_cobropago.dat_obligacion');
        $this->hasColumn('idobligacion', 'numeric', null, array('notnull' => false, 'primary' => true, 'default' => '', 'sequence' => 'mod_cobropago.sec_obligacion'));
        $this->hasColumn('numero', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('comentario', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fechaemision', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('fecharegistro', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('importe', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('liquidado', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idsubsistema', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('estadodocumento', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('estado', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('tipo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idtipodocumento', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idperiodo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idclientesproveedores', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idcomprobante', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('nocomprobante', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}