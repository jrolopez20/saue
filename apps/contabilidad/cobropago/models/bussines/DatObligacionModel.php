<?php

class DatObligacionModel extends ZendExt_Model
{
    public function __construct()
    {
        parent::ZendExt_Model();
    }

    public function adicionarObligacion($params)
    {
        $domain = new DatObligacion();
        $domain->numero = $params['numero'];
        $domain->fechaemision = $params['fechaemision'];
        $domain->importe = $params['importe'];
        $domain->liquidado = $params['liquidado'];
        $domain->idsubsistema = $params['idsubsistema'];
        $domain->estadodocumento = $params['estadodocumento'];
        $domain->estado = $params['estado'];
        $domain->idterminodepago = $params['idterminodepago'];
        $domain->tipo = $params['tipo'];
        $domain->idtipodocumento = $params['idtipodocumento'];
        $domain->idestructura = $params['idestructura'];
        $domain->idperiodo = $params['idperiodo'];
        $domain->idclientesproveedores = $params['idclientesproveedores'];
        $domain->comentario = $params['comentario'];
        $domain->idcomprobante = $params['idcomprobante'];
        $domain->nocomprobante = $params['nocomprobante'];
        $domain->save();
    }

    public function modificarObligacion($params)
    {

        if (!empty($params['idobligacion']) && is_numeric($params['idobligacion'])) { //para modificar
            $domain = DatObligacion::GetById($params['idobligacion']);
        }
        if (!empty($params['numero']))
            $domain->numero = $params['numero'];

        if (!empty($params['fechaemision']))
        $domain->fechaemision = $params['fechaemision'];

        if (!empty($params['importe']))
        $domain->importe = $params['importe'];

        if (!empty($params['liquidado']))
        $domain->liquidado = $params['liquidado'];

        if (!empty($params['estadodocumento']))
        $domain->estadodocumento = $params['estadodocumento'];

        if (!empty($params['estado']))
        $domain->estado = $params['estado'];

        if (!empty($params['comentario']))
        $domain->comentario = $params['comentario'];

        if (!empty($params['idcomprobante']))
        $domain->idcomprobante = $params['idcomprobante'];

        if (!empty($params['nocomprobante']))
        $domain->nocomprobante = $params['nocomprobante'];

        $domain->save();
    }

    public function getObligacionesCobroPago($limit, $start)
    {
        $domain = new DatObligacion();
        $r = $domain->getObligacionesCobroPago($limit, $start);

        return $r;
    }

    public function obtenerObligacion($tipo, $numero, $idtipodocumento)
    {
        $domain = new DatObligacion();
        $r = $domain->obtenerObligacion($tipo, $numero, $idtipodocumento);

        return $r;
    }

}