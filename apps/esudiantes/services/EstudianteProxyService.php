<?php


class EstudianteProxyService
{
    /**
     * loadTipoxEstudiante
     * Obtiene el tipo del estudiante a partir del identificador del estudiante
     *
     * @param int $idestudiante - Identificador del estudiante
     * @return string - Tipo de estudiante.
     */
    public function loadTipoxEstudiante($idestudiante)
    {
        $tipo = new DatTipoAlumnoModel();
        return $tipo->cargarTipoxEstudiante($idestudiante);
    } 
}