<?php

class GestdocxestudianteController extends ZendExt_Controller_Secure {

    public function init() {
        parent::init();
        $this->estudModel = new DatAlumnoModel();
        $this->DocReqModel = new DatDocumentoRequeridoModel();
    }

    public function gestdocxestudianteAction() {
        $this->render();
    }

    public function cargarDocRequeridosAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idalumno = $this->_request->getPost('idalumno');

        if ($idalumno == 0) {
            $docrequerido = $this->DocReqModel->cargarDocReq($limit, $start);
        }else{
            $docrequerido = $this->DocReqModel->cargarDocReqEst($idalumno, $limit, $start);
        }

        $cant = $this->DocReqModel->countDocReq();

        $result = array('cantidad' => $cant, 'datos' => $docrequerido);

        echo json_encode($result);
    }

    public function insertarDocRequeridoEstudianteAction() {
        $iddocumentorequeridos = json_decode($this->_request->getPost('iddocumentorequeridos'));
        $idalumno = $this->_request->getPost('idalumno');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $model = new DatAlumnoDatDocumentoRequeridoModel();
        $model->adicionarDocAEst($idalumno, $iddocumentorequeridos, $usuario, $fecha);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfAdicionar}";
    }

    public function eliminarDocRequeridoEstudianteAction() {
        $iddocumentorequeridos = json_decode($this->_request->getPost('iddocumentorequeridos'));
        $idalumno = $this->_request->getPost('idalumno');

        $model = new DatAlumnoDatDocumentoRequeridoModel();
        $model->eliminarDocEst($idalumno, $iddocumentorequeridos);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>