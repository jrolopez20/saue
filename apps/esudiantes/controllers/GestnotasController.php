<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestnotasController extends ZendExt_Controller_Secure
{

    private $model;
    
    public function init()
    {
        parent::init();
        $this->model = new DatNotaModel();
    }


    public function gestnotasAction()
    {
        $this->render();
    }

    public function cargarNotasAction(){
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $type = $this->_request->getPost('type');
        $idpd = $this->_request->getPost('idpd');
        if($type == "alumno"){
            $idestudiante = $this->_request->getPost('idt');
            $notas = $this->model->cargarNotasByAlumno($limit, $start, $idestudiante, $idpd);
            $cant = $this->model->countNotasAlumno($idestudiante, $idpd);
        } else {
            $idmateria = $this->_request->getPost('idt');
            $notas = $this->model->cargarNotasByCurso($limit, $start, $idmateria, $idpd);
            $cant = $this->model->countNotasCurso($idmateria, $idpd);
        }   
        

        $result = array('cantidad' => $cant, 'datos' => $notas);

        echo json_encode($result);

    }

    public function cargarPeriodosAction(){
        $anno = $this->_request->getPost('anno');
        $array = $this->integrator->calendar->cargarPeriodoAnno($anno);
        echo json_encode(array('datos'=>$array));
      }
    
    public function cargarCursosAction(){
        $filter = json_decode($this->_request->getPost('filtros'));
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        echo  json_encode(array('cantidad'=>$this->integrator->curso->countCursos($filter), 'datos'=>$this->integrator->curso->cargarCursos($limit,$start, $filter)));
        // echo '{"cantidad":4,"datos":[{"idcurso":1,"idmateria":1000007,"codmateria":"MAT110","materia":"Cálculo I", "profesor":"Juan L", "horario":"Lun-Mar 06:00-09:00"},{"idcurso":2,"idalumno":2,"idmateria":1,"codmateria":2,"materia":"AA"},{"idcurso":3,"idalumno":1,"idmateria":1,"codmateria":2,"materia":"OOuuajnasas"},{"idcurso":4,"idalumno":2,"idmateria":1,"codmateria":2,"materia":"xa"}]}';
    }
    public function modificarNotaAction(){
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idpd = $datos->idpd;
        $idmateria = $datos->idmateria;
        $idalumno = $datos->idalumno;
        $nota1 = $datos->nota1;
        $nota2 = $datos->nota2;
        $nota3 = $datos->nota3;
        $nota4 = $datos->nota4;
       if($this->model->modificarNota($idpd,  $idmateria, $idalumno, $nota1, $nota2, $nota3, $nota4))
         echo  json_encode(array("success"=> true));
    }
}

?>