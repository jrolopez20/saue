<?php

class GesttipoestudianteController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->model = new DatTipoAlumnoModel();
    }

    public function gesttipoestudianteAction()
    {
        $this->render();
    }

    public function cargarTipoEstudiantesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $estudiantes = $this->model->cargarTipoEst($limit, $start);

        $cant = $this->model->countTipoEst();

        $result = array('cantidad' => $cant, 'datos' => $estudiantes);

        echo json_encode($result);
    }

    public function insertarTipoEstudianteAction()
    {
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->model->insertarTipoEst($fecha, $usuario, $descripcion, $estado);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarTipoEstudianteAction()
    {
        $idtipoestudiante = $this->_request->getPost('idtipoestudiante');
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->model->modificarTipoEst($idtipoestudiante, $fecha, $usuario, $descripcion, $estado);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarTipoEstudianteAction()
    {
        $idtipoestudiantes = json_decode($this->_request->getPost('idtipoestudiantes'));

        $this->model->eliminarTipoEst($idtipoestudiantes);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }
}

?>