<?php

class GestdocrequiredController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->model = new DatDocumentoRequeridoModel();
    }

    public function gestdocrequiredAction()
    {
        $this->render();
    }

    public function cargarDocRequeridoAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $docrequerido = $this->model->cargarDocReq($limit, $start);

        $cant = $this->model->countDocReq();

        $result = array('cantidad' => $cant, 'datos' => $docrequerido);

        echo json_encode($result);
    }

    public function insertarDocRequeridoAction()
    {
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->model->insertarDocReq($descripcion, $usuario, $estado, $fecha);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarDocRequeridoAction()
    {
        $iddocumentorequerido = $this->_request->getPost('iddocumentorequerido');
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $this->model->modificarDocReq($iddocumentorequerido, $descripcion, $usuario, $estado, $fecha);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarDocRequeridoAction()
    {
        $iddocumentorequeridos = json_decode($this->_request->getPost('iddocumentorequeridos'));

        $this->model->eliminarDocReq($iddocumentorequeridos);
        //hay que agregar la eriqueta
        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }
}

?>