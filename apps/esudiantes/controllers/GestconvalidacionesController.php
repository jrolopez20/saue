<?php


class GestconvalidacionesController extends ZendExt_Controller_Secure
{


    private $model;
    private $modelMat;

    public function init()
    {
        parent::init();
        $this->model = new DatMateriaConvalidadaModel();
        $this->modelMat = new DatAlumnoMateDetaModel();
    }
    public function gestconvalidacionesAction()
    {
        $this->render();
    }

    public function cargarAnnosAction(){
    	echo json_encode(array("datos"=>$this->integrator->calendar->getAnnos()));
    }

    public function cargarUniversidadesAction()
    {
        echo json_encode(array("datos"=>$this->integrator->nomencladores->cargarUniversidades()));
    }
    public function insertarMateriaConvalidacionAction(){
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idmateria = $this->_request->getPost('idmateria');
        $fecha = date('Y-m-d H:i');
         if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
       if($this->model->insertarMateriaConvalidacion($fecha, $usuario, $descripcion, $estado, $iduniversidad, $idmateria))
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }
    public function eliminarMateriaConvalidacionAction(){
        $idmateriaconva = $this->_request->getPost('idmateriaconva');
       if($this->model->eliminarMateriaConvalidacion($idmateriaconva))
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfEliminar}");
    }    
    public function convalidarMateriasAction(){

        $idalumno = $_GET['idalumno'];
        //Verificar si el alumno es convalidante <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        $tipoalumno = $this->integrator->estudiante->cargarTipoxEstudiante($idalumno);
        if(strtolower($tipoalumno) != "convalidante"){
            echo("{'codMsg':3,'mensaje':perfil.etiquetas.lbMsgInfCovalidante, 'success':false}");
            return;
        }

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        $iduniversidad = $_GET['iduniversidad'];
        $idenfasis = $_GET['idenfasis'];
        $idpensum = $_GET['idpensum'];
        $idperiodo= $_GET['idperiodo'];
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        if(count($datos)>1){
            foreach ($datos as $key) {
                  $this->modelMat->insertarConvalidacion($key, $idenfasis, $idpensum, $iduniversidad, $idperiodo, $idalumno, $usuario, $fecha);
                }
                echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfCovalidar, 'success':true}");
        } else {
            
            if($this->modelMat->insertarConvalidacion($datos, $idenfasis, $idpensum, $iduniversidad, $idperiodo, $idalumno, $usuario, $fecha))
                echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfCovalidar, 'success':true}");
        }
        

    }
    public function principalMateriaConvalidacionAction(){
        $idmateriaconva = $this->_request->getPost('idmateriaconva');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idmateria = $this->_request->getPost('idmateria');
       if($this->model->principalMateriaConvalidacion($idmateriaconva, $iduniversidad, $idmateria))
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfPrincipal}");
    }
    
    public function cargarFacultadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $facultades = $this->model->cargarFacultades($limit, $start);

        echo json_encode(array('datos'=>$facultades));
    }


    public function cargarCarrerasAction()
    {
        //obtener todas las carreras
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idfacultad = $this->_request->getPost('idfacultad');

        $carreras = $this->model->cargarCarreras($idfacultad, $limit, $start);

        echo json_encode(array('datos' => $carreras));
    }

    public function cargarEnfasisAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');
        $enfasis = $this->model->cargarEnfasis($idcarrera, $limit, $start);
        /*$cant = $this->model->countEnfasis($idfacultad);
        $result = array('cantidad' => $cant, 'datos' => $enfasis);*/
        echo json_encode(array('datos' => $enfasis));
    }

    public function cargarPensumAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');

        $pensums = $this->model->cargarPensums($idcarrera, $limit, $start);

        //$result = array('cantidad' => $cant, 'datos' => $pensums);
        echo json_encode(array('datos' => $pensums));
    }

    public function cargarMateriasConvalidarAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idpensum = $this->_request->getPost('idpensum');
        $idenfasis = $this->_request->getPost('idenfasis');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idalumno = $this->_request->getPost('idalumno');
        
        $materias = $this->model->cargarMateriasConvalidar($limit, $start,  $idenfasis, $idpensum,  $iduniversidad, $idalumno);
        $cant = $this->model->countMateriasConvalidar($idenfasis, $idpensum, $idalumno);
        echo json_encode(array('cantidad' => $cant,'datos' => $materias));
    }

    public function cargarMateriasConvaAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idmateria = $this->_request->getPost('idmateria');
        $materias = $this->model->cargarMateriasConva($limit, $start, $iduniversidad, $idmateria);
        $cant = $this->model->countMateriasConva( $iduniversidad, $idmateria);
        echo json_encode(array('cantidad' => $cant,'datos' => $materias));
    }
}

?>