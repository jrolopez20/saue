<?php

class GestestudiantesController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->model = new DatAlumnoModel();
        $this->alumnoDatos = new DatAlumnosDatosModel();
    }

    public function gestestudiantesAction()
    {
        $this->render();
    }

    public function cargarEstudiantesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $estudiantes = $this->model->cargarAlumnos($limit, $start);

        $cant = $this->model->countAlumnos();

        $result = array('cantidad' => $cant, 'datos' => $estudiantes);
//print_r($result);die;
        echo json_encode($result);
    }

    public function cargarEstudiantesByNAAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filter = json_decode($this->_request->getPost('filtros'));
        $estudiantes = $this->model->cargarAlumnosByNA($limit, $start, $filter);

        $cant = $this->model->countAlumnosByNA($filter);

        $result = array('cantidad' => $cant, 'datos' => $estudiantes);

        echo json_encode($result);
    }

    public function insertarEstudianteAction()
    {
        //datos propios del alumno (dat_alumno)
        $nombre = $this->_request->getPost('nombre');
        $apellidos = $this->_request->getPost('apellidos');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');
        $idestadocivil = $this->_request->getPost('idestadocivil');
        $sexo = $this->_request->getPost('sexo');
        $idprovincia = $this->_request->getPost('idprovincia');
        $idtipoestudiante = $this->_request->getPost('idtipoalumno');
        $idsectorcuidad = $this->_request->getPost('idsectorciudad');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idcanton = $this->_request->getPost('idcanton');
        $idcolegio = $this->_request->getPost('idcolegio');
        $idpensumenfasismateriatipo = $this->_request->getPost('idpensum');
        //usuarioasig se inserta automaticamente después de insertar el alumno
        //codigo se genera automaticamente
        $fecha_nacimiento = $this->_request->getPost('fec_nacimiento');
        $domicilio = $this->_request->getPost('domicilio');
        $telefono = $this->_request->getPost('telefono');
        $celular = $this->_request->getPost('celular');
        $lugar_nacimiento = $this->_request->getPost('lugar_nacimiento');
        $nacionalidad = $this->_request->getPost('nacionalidad');
        $pais = $this->_request->getPost('pais');
        $religion = $this->_request->getPost('religion');
        $e_mail = $this->_request->getPost('e_mail');
        $e_mail2 = $this->_request->getPost('e_mail2');
        $ced_pas = $this->_request->getPost('cedpas'); //cedula o pasaporte vienen en la misma variable
        $rb = $this->_request->getPost('rb');
        if ($rb == 'cedula') {
            $cedula = $ced_pas;
        } else {
            $pasaporte = $ced_pas;
        }
        $enfasis = $this->_request->getPost('idenfasis');
        $idprovinciauniv = $this->_request->getPost('idprovinciauniv');
        $idprovinciacoleg = $this->_request->getPost('idprovinciacoleg');
        $observaciones = $this->_request->getPost('observaciones');
        $hijos = $this->_request->getPost('hijos');
        $estado = $this->_request->getPost('estado');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $fac = $this->_request->getPost('idestructura'); //esta es la facultad a la que pertenece el alumno

        //datos adicionales del alumno (dar_alumno_datos)
        $empresa_trab = $this->_request->getPost('empresa_trab');
        $cargo_empresa = $this->_request->getPost('cargo_empresa');
        $ciudad_trabajo = $this->_request->getPost('ciudad_trabajo');
        $direccion_trbajo = $this->_request->getPost('direccion_trbajo');
        $telefono_trabajo = $this->_request->getPost('telefono_trabajo');
        $nombre_padre = $this->_request->getPost('nombre_padre');
        $apellidos_padre = $this->_request->getPost('apellidos_padre');
        $direccion_padre = $this->_request->getPost('direccion_padre');
        $telefono_padre = $this->_request->getPost('telefono_padre');
        $profesion_padre = $this->_request->getPost('profesion_padre');
        $cargo_padre = $this->_request->getPost('cargo_padre');
        $empresa_padre = $this->_request->getPost('empresa_padre');
        $apellidos_madre = $this->_request->getPost('apellidos_madre');
        $nombre_madre = $this->_request->getPost('nombre_madre');
        $direccion_madre = $this->_request->getPost('direccion_madre');
        $telefono_madre = $this->_request->getPost('telefono_madre');
        $profesion_madre = $this->_request->getPost('profesion_madre');
        $cargo_madre = $this->_request->getPost('cargo_madre');
        $empresa_madre = $this->_request->getPost('empresa_madre');


        $idalumno = $this->model->insertarAlumno($nombre, $apellidos, $usuario, $fecha, $idestadocivil, $sexo, $idprovincia, $idtipoestudiante, $idsectorcuidad, $iduniversidad, $idcanton, $idcolegio, $idpensumenfasismateriatipo, $fecha_nacimiento, $domicilio, $telefono, $celular, $lugar_nacimiento, $nacionalidad, $pais, $religion, $e_mail, $e_mail2, $cedula, $pasaporte, $enfasis, $idprovinciauniv, $idprovinciacoleg, $observaciones, $hijos, $estado, $fac);

        $this->alumnoDatos->insertarDatosAlumno($empresa_trab, $cargo_empresa, $ciudad_trabajo, $direccion_trbajo, $telefono_trabajo, $nombre_padre, $apellidos_padre, $direccion_padre, $telefono_padre, $profesion_padre, $cargo_padre, $empresa_padre, $apellidos_madre, $nombre_madre, $direccion_madre, $telefono_madre, $profesion_madre, $cargo_madre, $empresa_madre, $fecha, $usuario, $estado, $idalumno);

        if ($idalumno) {
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
        } else {
            echo("{'codMsg':3,'mensaje':perfil.etiquetas.lbMsgInfAnno.}");
        }
    }

    public function modificarEstudianteAction()
    {
        $idestudiante = $this->_request->getPost('idalumno');
        $id_alumnodatos = $this->_request->getPost('id_alumnodatos');
        //datos propios del alumno (dat_alumno)
        $nombre = $this->_request->getPost('nombre');
        $apellidos = $this->_request->getPost('apellidos');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');
        $idestadocivil = $this->_request->getPost('idestadocivil');
        $sexo = $this->_request->getPost('sexo');
        $idprovincia = $this->_request->getPost('idprovincia');
        $idtipoestudiante = $this->_request->getPost('idtipoalumno');
        $idsectorcuidad = $this->_request->getPost('idsectorciudad');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idcanton = $this->_request->getPost('idcanton');
        $idcolegio = $this->_request->getPost('idcolegio');
        $idpensumenfasismateriatipo = $this->_request->getPost('idpensum');
        //usuarioasig se inserta automaticamente después de insertar el alumno
        //codigo se genera automaticamente
        $fecha_nacimiento = $this->_request->getPost('fec_nacimiento');
        $domicilio = $this->_request->getPost('domicilio');
        $telefono = $this->_request->getPost('telefono');
        $celular = $this->_request->getPost('celular');
        $lugar_nacimiento = $this->_request->getPost('lugar_nacimiento');
        $nacionalidad = $this->_request->getPost('nacionalidad');
        $pais = $this->_request->getPost('pais');
        $religion = $this->_request->getPost('religion');
        $e_mail = $this->_request->getPost('e_mail');
        $e_mail2 = $this->_request->getPost('e_mail2');
        $ced_pas = $this->_request->getPost('cedpas'); //cedula o pasaporte vienen en la misma variable
        $rb = $this->_request->getPost('rb');
        if ($rb == 'cedula') {
            $cedula = $ced_pas;
        } else {
            $pasaporte = $ced_pas;
        }
        $enfasis = $this->_request->getPost('idenfasis');
        $idprovinciauniv = $this->_request->getPost('idprovinciauniv');
        $idprovinciacoleg = $this->_request->getPost('idprovinciacoleg');
        $observaciones = $this->_request->getPost('observaciones');
        $hijos = $this->_request->getPost('hijos');
        $estado = $this->_request->getPost('estado');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $fac = $this->_request->getPost('idestructura'); //esta es la facultad a la que pertenece el alumno 

        //datos adicionales del alumno (dar_alumno_datos)
        $empresa_trab = $this->_request->getPost('empresa_trab');
        $cargo_empresa = $this->_request->getPost('cargo_empresa');
        $ciudad_trabajo = $this->_request->getPost('ciudad_trabajo');
        $direccion_trbajo = $this->_request->getPost('direccion_trab');
        $telefono_trabajo = $this->_request->getPost('telefono_trab');
        $nombre_padre = $this->_request->getPost('nombre_padre');
        $apellidos_padre = $this->_request->getPost('apellidos_padre');
        $direccion_padre = $this->_request->getPost('direccion_padre');
        $telefono_padre = $this->_request->getPost('telefono_padre');
        $profesion_padre = $this->_request->getPost('profesion_padre');
        $cargo_padre = $this->_request->getPost('cargo_padre');
        $empresa_padre = $this->_request->getPost('empresa_padre');
        $apellidos_madre = $this->_request->getPost('apellidos_madre');
        $nombre_madre = $this->_request->getPost('nombre_madre');
        $direccion_madre = $this->_request->getPost('direccion_madre');
        $telefono_madre = $this->_request->getPost('telefono_madre');
        $profesion_madre = $this->_request->getPost('profesion_madre');
        $cargo_madre = $this->_request->getPost('cargo_madre');
        $empresa_madre = $this->_request->getPost('empresa_madre');

        $flag = $this->model->modificarAlumno($idestudiante, $nombre, $apellidos, $usuario, $fecha, $idestadocivil, $sexo, $idprovincia, $idtipoestudiante, $idsectorcuidad, $iduniversidad, $idcanton, $idcolegio, $idpensumenfasismateriatipo, $fecha_nacimiento, $domicilio, $telefono, $celular, $lugar_nacimiento, $nacionalidad, $pais, $religion, $e_mail, $e_mail2, $cedula, $pasaporte, $enfasis, $idprovinciauniv, $idprovinciacoleg, $observaciones, $hijos, $estado, $fac);
        if ($id_alumnodatos) {
            $this->alumnoDatos->modificarDatosAlumno($id_alumnodatos, $empresa_trab, $cargo_empresa, $ciudad_trabajo, $direccion_trbajo, $telefono_trabajo, $nombre_padre, $apellidos_padre, $direccion_padre, $telefono_padre, $profesion_padre, $cargo_padre, $empresa_padre, $apellidos_madre, $nombre_madre, $direccion_madre, $telefono_madre, $profesion_madre, $cargo_madre, $empresa_madre, $fecha, $usuario, $estado, $idestudiante);
        } else {
            $this->alumnoDatos->insertarDatosAlumno($empresa_trab, $cargo_empresa, $ciudad_trabajo, $direccion_trbajo, $telefono_trabajo, $nombre_padre, $apellidos_padre, $direccion_padre, $telefono_padre, $profesion_padre, $cargo_padre, $empresa_padre, $apellidos_madre, $nombre_madre, $direccion_madre, $telefono_madre, $profesion_madre, $cargo_madre, $empresa_madre, $fecha, $usuario, $estado, $idestudiante);
        }

        //insertar documentos requeridos
        //devolver los datos, hay que agregar la etiqueta
        $docsAdd = json_decode($this->_request->getPost('iddocsAdd'));
        $docsDel = json_decode($this->_request->getPost('iddocsDel'));
        $docReq = new DatAlumnoDatDocumentoRequeridoModel();
        $docReq->eliminarDocEst($idestudiante, $docsDel);
        $docReq->adicionarDocAEst($idestudiante, $docsAdd, $usuario, $fecha);

        if ($flag) {
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
        } else {
            echo("{'codMsg':3,'mensaje':perfil.etiquetas.lbMsgInfAnno}");
        }
    }

    public function eliminarEstudianteAction()
    {
        $idalumno = json_decode($this->_request->getPost('idalumnos'));

        $this->model->eliminarAlumno($idalumno);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

    //cargar los nomencaldores
    public function cargarEstadoCivilAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $result = $this->model->cargarEstadosCivil($limit, $start);

        echo json_encode($result);
    }

    public function cargarProvinciasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $result = $this->model->cargarProvincias($limit, $start);

        echo json_encode($result);
    }

    public function cargarFacultadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $fac = $this->model->cargarFacultades($limit, $start);

        $result = array('datos' => $fac);

        echo json_encode($result);
    }

    public function cargarCarrerasAction()
    {
        $idfacultad = $this->_request->getPost('idfacultad');
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $carreras = $this->model->cargarCarreras($idfacultad, $limit, $start);

        $result = array('datos' => $carreras);

        echo json_encode($result);
    }

    public function cargarEnfasisAction()
    {
        $idcarrera = $this->_request->getPost('idcarrera');
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $enfasis = $this->model->cargarEnfasis($idcarrera, $limit, $start);

        $result = array('datos' => $enfasis);

        echo json_encode($result);
    }

    public function cargarColegiosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $result = $this->model->cargarColegios($limit, $start);

        echo json_encode($result);
    }

    public function cargarSectoresAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $result = $this->model->cargarSectores($limit, $start);

        echo json_encode($result);
    }

    public function cargarUniversidadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $result = $this->model->cargarUniversidades($limit, $start);

        echo json_encode($result);
    }

    public function cargarPensumsAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');

        $result = $this->model->cargarPensums($idcarrera, $limit, $start);

        echo json_encode($result);
    }

    public function cargarAnnosAction()
    {
        $result = $this->model->cargarAnnos();

        echo json_encode($result);
    }

    //Menciones
    public function cargarMencionesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $result = $this->model->cargarMenciones($limit, $start);

        echo json_encode($result);
    }

    //Estudios
    public function cargarEstudiosAction()
    {


        return true;
    }

    public function insertarEstudioAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        return true;
    }

    public function modificarEstudioAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idestudio = $this->_request->getPost('idestudio');

        return true;
    }

    public function eliminarEstudioAction()
    {
        $idestudio = $this->_request->getPost('idestudios');

        return true;
    }

    //guardar información por partes según el tab
    public  function guardarInfoPrincAction(){
        //datos propios del alumno (dat_alumno)
        $nombre = $this->_request->getPost('nombre');
        $apellidos = $this->_request->getPost('apellidos');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');
        $sexo = $this->_request->getPost('sexo');
        $idtipoestudiante = $this->_request->getPost('idtipoalumno');
        $idsectorcuidad = $this->_request->getPost('idsectorciudad');
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $idcolegio = $this->_request->getPost('idcolegio');
        $idpensumenfasismateriatipo = $this->_request->getPost('idpensum');
        //usuarioasig se inserta automaticamente después de insertar el alumno
        //codigo se genera automaticamente
        $fecha_nacimiento = $this->_request->getPost('fec_nacimiento');
        $domicilio = $this->_request->getPost('domicilio');
        $telefono = $this->_request->getPost('telefono');
        $celular = $this->_request->getPost('celular');
        $e_mail = $this->_request->getPost('e_mail');
        $ced_pas = $this->_request->getPost('cedpas'); //cedula o pasaporte vienen en la misma variable
        $rb = $this->_request->getPost('rb');
        if ($rb == 'cedula') {
            $cedula = $ced_pas;
        } else {
            $pasaporte = $ced_pas;
        }
        $enfasis = $this->_request->getPost('idenfasis');
        $idprovinciauniv = $this->_request->getPost('idprovinciauniv');
        $idprovinciacoleg = $this->_request->getPost('idprovinciacoleg');
        $observaciones = $this->_request->getPost('observaciones');
        $estado = $this->_request->getPost('estado');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $fac = $this->_request->getPost('idestructura'); //esta es la facultad a la que pertenece el alumno

        $idalumno = $this->model->guardarInfoPrinc($nombre, $apellidos, $usuario, $fecha, $sexo, $idtipoestudiante, $idsectorcuidad, $iduniversidad, $idcolegio, $idpensumenfasismateriatipo, $fecha_nacimiento, $domicilio, $telefono, $celular, $e_mail, $cedula, $pasaporte, $enfasis, $idprovinciauniv, $idprovinciacoleg, $observaciones, $estado, $fac);

        $result = array('datos' => $idalumno);

        echo json_encode($result);
    }
}

?>