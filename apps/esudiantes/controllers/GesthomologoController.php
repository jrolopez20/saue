<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GesthomologoController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
    }

    public function gesthomologoAction()
    {
        $this->render();
    }


}

?>