<?php

class DatAlumnoDatDocumentoRequeridoModel extends ZendExt_Model
{
    public function DatAlumnoDatDocumentoRequeridoModel()
    {
        parent::ZendExt_Model();
    }

    public function eliminarDocEst($idestudiante, $iddocreq)
    {
        try {
            foreach($iddocreq as $id){
                DatAlumnoDatDocumentoRequerido::eliminarDocEst($idestudiante, $id);
            }

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function adicionarDocAEst($idestudiante, $iddocreq, $usuario, $fecha)
    {
        try {
            foreach ($iddocreq as $id) {
                $docReqEst = new DatAlumnoDatDocumentoRequerido();
                $docReqEst->idestudiante = $idestudiante;
                $docReqEst->iddocumentorequerido = $id;
                $docReqEst->idusuario = $usuario;
                $docReqEst->fecha = $fecha;

                $docReqEst->save();
            }
            return true;

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }
}

