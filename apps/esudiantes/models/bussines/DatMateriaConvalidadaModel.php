<?php

class DatMateriaConvalidadaModel extends ZendExt_Model
{

    public function DatMateriaConvalidadaModel()
    {
        parent::ZendExt_Model();
    }

 public function cargarFacultades($limit, $start)
    {
        try {
            return $this->integrator->metadatos->ListadoEstructurasT($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarMateriaConvalidacion ($fecha, $idusuario, $descripcion,$estado, $iduniversidad, $idmateria)
    {


        try {
            $matconv = new DatMateriaConvalidada();
            $matconv->fecha = $fecha;
            $matconv->idusuario = $idusuario;
            $matconv->iduniversidad = $iduniversidad;
            $matconv->idmateria = $idmateria;
            $matconv->descripcion = $descripcion;
            $matconv->estado = $estado;
            $matconv->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
        public function eliminarMateriaConvalidacion($idmateriaconva){
            try {
                    DatMateriaConvalidada::eliminarMateriaConvalidacion($idmateriaconva);
                return true;
            } catch (Doctrine_Exception $e) {
                if (DEBUG_SAUE) {
                    echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
                }
                return false;
            }
        }        
        public function principalMateriaConvalidacion($idmateriaconva, $iduniversidad, $idmateria){
            try {
                    DatMateriaConvalidada::updatePrinMateriaConvalidacion($iduniversidad, $idmateria);
                    $materia = Doctrine::getTable("DatMateriaConvalidada")->find($idmateriaconva);
                    $materia->principal = true;
                    $materia->save();
                    
                return true;
            } catch (Doctrine_Exception $e) {
                if (DEBUG_SAUE) {
                    echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
                }
                return false;
            }
        }
        public function cargarCarreras($idfacultad, $limit, $start)
    {
        try {
            $filtro = new stdClass();
            $filtro->property = 'idfacultad';
            $filtro->value = $idfacultad;
            $filtros = array($filtro);

            $carreras = $this->integrator->facultad->cargarCarreras($limit, $start, $filtros);
            return $carreras;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

        public function cargarEnfasis($idcarrera, $limit, $start)
    {
        try {
            $carreras = $this->integrator->facultad->cargarEnfasis($idcarrera, $limit, $start);
            return $carreras;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function cargarMateriasConvalidar($limit, $start,  $idenfasis, $idpensum,  $iduniversidad, $idalumno){
        try {
            $materias =  DatMateriaConvalidada::cargarMateriasConvalidar($limit, $start,  $idenfasis, $idpensum,  $iduniversidad, $idalumno);
            return $materias;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function countMateriasConvalidar($idenfasis, $idpensum, $idalumno){
    	try {
            $cant = DatMateriaConvalidada::countMateriasConvalidar( $idenfasis, $idpensum, $idalumno);
            return $cant;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMateriasConva($limit, $start, $iduniversidad, $idmateria){
        try {
            $materias =  DatMateriaConvalidada::cargarMateriasConva($limit, $start, $iduniversidad, $idmateria);
            return $materias;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function countMateriasConva($iduniversidad, $idmateria){
    	try {
            $cant = DatMateriaConvalidada::countMateriasConva( $iduniversidad, $idmateria);
            return $cant;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function cargarPensums($idcarrera, $limit, $start)
    {
        try {
            $filtro = new stdClass();
            $filtro->property = 'idcarrera';
            $filtro->value = $idcarrera;
            $filtros = array($filtro);

            $pensums = $this->integrator->facultad->cargarPensum($limit, $start, $filtros); //DatPensumMateriaEnfasisTipomateria::cargarPensums($idcarrera, $limit, $start);
            return $pensums;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

