<?php

class DatAlumnoModel extends ZendExt_Model
{

    public function DatAlumnoModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarAlumnos($limit, $start)
    {
        try {
            $alumnos = DatAlumno::cargarAlumnos($limit, $start);

            for ($i = 0; $i < count($alumnos); $i++) {
                //pongo el sexo
                if ($alumnos[$i]['sexo'] == 'M') {
                    $alumnos[$i]['sexo'] = 'Masculino';
                } else {
                    $alumnos[$i]['sexo'] = 'Femenino';
                }
                //pongo la cedula o pasaporte segun corresponda
                if ($alumnos[$i]['cedula'] != '') {
                    $alumnos[$i]['cedpas'] = $alumnos[$i]['cedula'];
                } else {
                    $alumnos[$i]['cedpas'] = $alumnos[$i]['pasaporte'];
                }
            }
            return $alumnos;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    public function cargarAlumnosByNA($limit, $start, $filter)
    {
        try {
            $alumnos = DatAlumno::cargarAlumnosByNA($limit, $start, $filter);

            for ($i = 0; $i < count($alumnos); $i++) {
                //pongo el sexo
                if ($alumnos[$i]['sexo'] == 'M') {
                    $alumnos[$i]['sexo'] = 'Masculino';
                } else {
                    $alumnos[$i]['sexo'] = 'Femenino';
                }
                //pongo la cedula o pasaporte segun corresponda
//                if ($alumnos[$i]['cedula'] != '') {
//                    $alumnos[$i]['cedpas'] = $alumnos[$i]['cedula'];
//                } else {
//                    $alumnos[$i]['cedpas'] = $alumnos[$i]['pasaporte'];
//                }
            }

            return $alumnos;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    public function countAlumnosByNA($filter)
    {
        try {
            return DatAlumno::countAlumnosByNA($filter);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    public function countAlumnos()
    {
        try {
            return DatAlumno::countAlumnos();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    public function insertarAlumno($nombre, $apellidos, $usuario, $fecha, $idestadocivil, $sexo, $idprovincia, $idtipoestudiante, $idsectorcuidad, $iduniversidad, $idcanton, $idcolegio, $idpensumenfasismateriatipo, $fecha_nacimiento, $domicilio, $telefono, $celular, $lugar_nacimiento, $nacionalidad, $pais, $religion, $e_mail, $e_mail2, $cedula, $pasaporte, $enfasis, $idprovinciauniv, $idprovinciacoleg, $observaciones, $hijos, $estado, $fac)
    {
        $anno_nac = split('/', $fecha_nacimiento);
        $anno_actual = split('-', $fecha);
        //print_r($anno_actual[0] - $anno_nac[2]);die();
        if ($anno_actual[0] - $anno_nac[2] >= 16) {
            try {
                $estudiante = new DatAlumno();
                $estudiante->nombre = strtoupper($nombre);
                $estudiante->apellidos = strtoupper($apellidos);
                $estudiante->idusuario = $usuario;
                $estudiante->fecha = $fecha;
                if ($idestadocivil != "")
                    $estudiante->idestadocivil = $idestadocivil;

                if ($sexo == 'Masculino') {
                    $estudiante->sexo = 'M';
                } else {
                    $estudiante->sexo = 'F';
                }
                if ($idprovincia != "")
                    $estudiante->idprovincia = $idprovincia;
                $estudiante->idtipoalumno = $idtipoestudiante;
                $estudiante->idsectorciudad = $idsectorcuidad;
                $estudiante->iduniversidad = $iduniversidad;
                $estudiante->idcanton = $idcanton;
                $estudiante->idcolegio = $idcolegio;
                $estudiante->idpensum = $idpensumenfasismateriatipo;
                $idusuarioasig = $this->integrator->seguridad->insertarUsuario($nombre, $apellidos, $fac, 1, 220000000024);
                $estudiante->idusuarioasig = $idusuarioasig;
                $codigo = rand(1000000000, 9999999999);
                $estudiante->codigo = $codigo;
                $estudiante->fec_nacimiento = $fecha_nacimiento;
                $estudiante->domicilio = $domicilio;
                $estudiante->telefono = $telefono;
                $estudiante->celular = $celular;
                $estudiante->lugar_nacimiento = $lugar_nacimiento;
                $estudiante->nacionalidad = $nacionalidad;
                $estudiante->pais = $pais;
                $estudiante->religion = $religion;
                $estudiante->e_mail = $e_mail;
                $estudiante->e_mail2 = $e_mail2;
                $estudiante->cedula = $cedula;
                $estudiante->pasaporte = $pasaporte;
                $estudiante->idenfasis = $enfasis;
                $estudiante->idprovinciauniv = $idprovinciauniv;
                $estudiante->idprovinciacoleg = $idprovinciacoleg;
                $estudiante->observaciones = $observaciones;
                if ($hijos != "")
                    $estudiante->cant_hijos = $hijos;
                else
                    $estudiante->cant_hijos = 0;

                $estudiante->estado = $estado;

                $estudiante->save();

                return $estudiante->idalumno;
            } catch (Doctrine_Exception $e) {
                if (DEBUG_SAUE) {
                    echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
                }
                return false;
            }
        } else
            return false;
    }

    public function guardarInfoPrinc($nombre, $apellidos, $usuario, $fecha, $sexo, $idtipoestudiante, $idsectorcuidad, $iduniversidad, $idcolegio, $idpensumenfasismateriatipo, $fecha_nacimiento, $domicilio, $telefono, $celular, $e_mail, $cedula, $pasaporte, $enfasis, $idprovinciauniv, $idprovinciacoleg, $observaciones, $estado, $fac)
    {
        $estudiante = new DatAlumno();
        $estudiante->nombre = strtoupper($nombre);
        $estudiante->apellidos = strtoupper($apellidos);
        $estudiante->idusuario = $usuario;
        $estudiante->fecha = $fecha;
        if ($sexo == 'Masculino') {
            $estudiante->sexo = 'M';
        } else {
            $estudiante->sexo = 'F';
        }
        $estudiante->idtipoalumno = $idtipoestudiante;
        $estudiante->idsectorciudad = $idsectorcuidad;
        $estudiante->iduniversidad = $iduniversidad;
        $estudiante->idcolegio = $idcolegio;
        $estudiante->idpensum = $idpensumenfasismateriatipo;
        $idusuarioasig = $this->integrator->seguridad->insertarUsuario($nombre, $apellidos, $fac, 1, 220000000024);
        $estudiante->idusuarioasig = $idusuarioasig;
        $codigo = rand(1000000000, 9999999999);
        $estudiante->codigo = $codigo;
        $estudiante->fec_nacimiento = $fecha_nacimiento;
        $estudiante->domicilio = $domicilio;
        $estudiante->telefono = $telefono;
        $estudiante->celular = $celular;
        $estudiante->e_mail = $e_mail;
        $estudiante->cedula = $cedula;
        $estudiante->pasaporte = $pasaporte;
        $estudiante->idenfasis = $enfasis;
        $estudiante->idprovinciauniv = $idprovinciauniv;
        $estudiante->idprovinciacoleg = $idprovinciacoleg;
        $estudiante->observaciones = $observaciones;
        $estudiante->estado = $estado;

        $estudiante->save();

        return $estudiante->idalumno;
    }

    public function modificarAlumno($idestudiante, $nombre, $apellidos, $usuario, $fecha, $idestadocivil, $sexo, $idprovincia, $idtipoestudiante, $idsectorcuidad, $iduniversidad, $idcanton, $idcolegio, $idpensumenfasismateriatipo, $fecha_nacimiento, $domicilio, $telefono, $celular, $lugar_nacimiento, $nacionalidad, $pais, $religion, $e_mail, $e_mail2, $cedula, $pasaporte, $enfasis, $idprovinciauniv, $idprovinciacoleg, $observaciones, $hijos, $estado)
    {
        $anno_nac = split('/', $fecha_nacimiento);
        $anno_actual = split('-', $fecha);
        //print_r($anno_actual[0] - $anno_nac[2]);die();
        if ($anno_actual[0] - $anno_nac[2] >= 16) {
            try {
                $estudiante = Doctrine::getTable("DatAlumno")->find($idestudiante);

                $estudiante->nombre = strtoupper($nombre);
                $estudiante->apellidos = strtoupper($apellidos);
                $estudiante->idusuario = $usuario;
                $estudiante->fecha = $fecha;
                if ($idestadocivil != "")
                    $estudiante->idestadocivil = $idestadocivil;
                if ($sexo == 'Masculino') {
                    $estudiante->sexo = 'M';
                } else {
                    $estudiante->sexo = 'F';
                }
                if ($idprovincia != "")
                    $estudiante->idprovincia = $idprovincia;
                $estudiante->idtipoalumno = $idtipoestudiante;
                $estudiante->idsectorciudad = $idsectorcuidad;
                $estudiante->iduniversidad = $iduniversidad;
                $estudiante->idcanton = $idcanton;
                $estudiante->idcolegio = $idcolegio;
                $estudiante->idpensum = $idpensumenfasismateriatipo;
                $estudiante->fec_nacimiento = $fecha_nacimiento;
                $estudiante->domicilio = $domicilio;
                $estudiante->telefono = $telefono;
                $estudiante->celular = $celular;
                $estudiante->lugar_nacimiento = $lugar_nacimiento;
                $estudiante->nacionalidad = $nacionalidad;
                $estudiante->pais = $pais;
                $estudiante->religion = $religion;
                $estudiante->e_mail = $e_mail;
                $estudiante->e_mail2 = $e_mail2;
                if ($cedula != null && $estudiante->cedula != $cedula) {
                    $estudiante->cedula = $cedula;
                }
                if ($pasaporte != null && $estudiante->pasaporte != $pasaporte) {
                    $estudiante->pasaporte = $pasaporte;
                }
                $estudiante->idenfasis = $enfasis;
                $estudiante->idprovinciauniv = $idprovinciauniv;
                $estudiante->idprovinciacoleg = $idprovinciacoleg;
                $estudiante->observaciones = $observaciones;
                if ($hijos != "")
                    $estudiante->cant_hijos = $hijos;
                else
                    $estudiante->cant_hijos = 0;

                $estudiante->estado = $estado;
                //print_r($estudiante);die;
                $estudiante->save();

                return true;
            } catch (Doctrine_Exception $e) {
                if (DEBUG_SAUE) {
                    echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
                }
                return false;
            }
        } else
            return false;
    }

    public function eliminarAlumno($idestdiantes)
    {
        try {
            foreach ($idestdiantes as $idestud) {
                DatAlumno::eliminarAlumno($idestud);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    //cargar los nomencladores
    public function cargarEnfasis($idcarrera, $limit, $start)
    {
        try {
            return $this->integrator->facultad->cargarEnfasis($idcarrera, $limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    public function cargarCarreras($idfacultad, $limit, $start)
    {
        try {
            $carreras = $this->integrator->facultad->cargarCarreras($idfacultad, $limit, $start);
            return $carreras;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarFacultades($limit, $start)
    {
        try {
            $facultades = $this->integrator->metadatos->ListadoEstructurasT($limit, $start);

            return $facultades;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarProvincias($limit, $start)
    {
        try {
            $var = $this->integrator->nomencladores->cargarProvincias($limit, $start);

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarEstadosCivil($limit, $start)
    {
        try {
            $var = $this->integrator->nomencladores->cargarEstadosCivil($limit, $start);

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarColegios($limit, $start)
    {
        try {
            $var = $this->integrator->nomencladores->cargarColegios($limit, $start);

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarSectores($limit, $start)
    {
        try {
            $var = $this->integrator->nomencladores->cargarSectoresCiudad($limit, $start);

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarUniversidades($limit, $start)
    {
        try {
            $var = $this->integrator->nomencladores->cargarUniversidades($limit, $start);

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarPensums($idcarrera, $limit, $start)
    {
        try {
            $var = $this->integrator->facultad->cargarPensum($idcarrera, $limit, $start);

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAnnos()
    {
        try {
            $var = $this->integrator->calendar->getAnnos();

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMenciones()
    {
        try {
            $var = $this->integrator->facultad->cargarMensiones();
            $cant = $this->integrator->facultad->contarMensiones();

            return array('cantidad' => $cant, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }


}
