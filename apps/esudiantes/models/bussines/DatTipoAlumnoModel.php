<?php

class DatTipoAlumnoModel extends ZendExt_Model
{

    public function DatTipoAlumnoModel()
    {

        parent::ZendExt_Model();

    }

    public function cargarTipoEst($limit, $start)
    {
        try {
            return DatTipoAlumno::cargarTiposEstudiantes($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }
    
    public function cargarTipoxEstudiante($idestudiante)
    {
        try {
            return DatTipoAlumno::cargarTipoxEstudiante($idestudiante);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countTipoEst()
    {

        try {
            return DatTipoAlumno::countTipoEst();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarTipoEst($fecha, $idusuario, $descripcion, $estado)
    {

        try {
            $tipoestudiante = new DatTipoAlumno();
            $tipoestudiante->fecha = $fecha;
            $tipoestudiante->idusuario = $idusuario;
            $tipoestudiante->descripcion = $descripcion;
            if ($estado)
                $tipoestudiante->estado = $estado;
            else
                $tipoestudiante->estado = false;

            $tipoestudiante->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTipoEst($idtipoestudiante, $fecha, $idusuario, $descripcion, $estado)
    {

        try {
            $tipoestudiante = Doctrine::getTable("DatTipoAlumno")->find($idtipoestudiante);

            $tipoestudiante->fecha = $fecha;
            $tipoestudiante->idusuario = $idusuario;
            $tipoestudiante->descripcion = $descripcion;
            if ($estado)
                $tipoestudiante->estado = $estado;
            else
                $tipoestudiante->estado = false;

            $tipoestudiante->save();
            return true;

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarTipoEst($idtipoestudiantes)
    {

        try {
            foreach($idtipoestudiantes as $idestud){
                DatTipoAlumno::eliminarTipoEstud($idestud);
            }
            return true;

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

