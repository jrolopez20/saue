<?php

class DatNotaModel extends ZendExt_Model
{
 public function DatEstudianteModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarNotasByCurso($limit, $start, $idcurso, $idpd)
    {
        try {
            return DatNota::cargarNotasCurso($limit, $start, $idcurso, $idpd);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }
        public function cargarNotasByAlumno($limit, $start, $idalumno, $idpd)
    {
        try {
            return DatNota::cargarNotasAlumno($limit, $start, $idalumno, $idpd);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countNotasCurso($idmateria, $idpd)
    {
        try {
            return DatNota::countNotasCurso($idmateria, $idpd);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
        public function countNotasAlumno($idestudiante, $idpd)
    {
        try {
            return DatNota::countNotasAlumno($idestudiante, $idpd);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarNota($idpd,  $idmateria, $idalumno, $nota1, $nota2, $nota3, $nota4)
    {
        try {
            // print_r(array($idpd, $idalumno, $idmateria));
            $nota = Doctrine::getTable("DatNota")->find(array($idmateria,$idalumno,$idpd));
            $nota->nota1 = $nota1;
            $nota->nota2 = $nota2;
            $nota->nota3 = $nota3;
            $nota->nota4 = $nota4;
            $nota->save();

            return true;

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}

