<?php

class DatDocumentoRequeridoModel extends ZendExt_Model
{

    public function DatDocumentoRequeridoModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarDocReq($limit, $start)
    {
        try {
            $docRed = DatDocumentoRequerido::cargarDocReq($limit, $start);

            for ($i = 0; $i < count($docRed); $i++) {
                $docRed[$i]['checked'] = '';
            }

            return $docRed;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarDocReqEst($idalumno, $limit, $start)
    {
        try {
            $docReqAlumno = DatDocumentoRequerido::cargarDocReqEst($limit, $start);
            $docReqTodos = $this->cargarDocReq($limit, $start);

            for ($i = 0; $i < count($docReqAlumno); $i++) {
                for ($m = 0; $m < count($docReqTodos); $m++) {
                    if ($docReqAlumno[$i]['iddocumentorequerido'] == $docReqTodos[$m]['iddocumentorequerido'] && $docReqAlumno[$i]['idestudiante']==$idalumno) {
                        $docReqTodos[$m]['checked'] = 'true';
                    }
                }
            }
            return $docReqTodos;

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countDocReq()
    {
        try {
            return DatDocumentoRequerido::countDocReq();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarDocReq($descripcion, $usuario, $estado, $fecha)
    {
        try {
            $docreq = new DatDocumentoRequerido();
            $docreq->fecha = $fecha;
            $docreq->idusuario = $usuario;
            $docreq->descripcion = $descripcion;
            if ($estado)
                $docreq->estado = $estado;
            else
                $docreq->estad0 = false;

            $docreq->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarDocReq($iddocumentorequerido, $descripcion, $usuario, $estado, $fecha)
    {
        try {
            $docreq = Doctrine::getTable("DatDocumentoRequerido")->find($iddocumentorequerido);

            $docreq->fecha = $fecha;
            $docreq->idusuario = $usuario;
            $docreq->descripcion = $descripcion;
            if ($estado)
                $docreq->estado = $estado;
            else
                $docreq->estado = false;

            $docreq->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarDocReq($iddocumentorequeridos)
    {
        try {
            foreach ($iddocumentorequeridos as $iddoc) {
                DatDocumentoRequerido::eliminarDocReq($iddoc);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarDocReqDist($idestudiante, $limit, $start)
    {
        $obj = new DatDocumentoRequerido();
        return $obj->cargarDocReqDist($idestudiante, $limit, $start);
    }

}
