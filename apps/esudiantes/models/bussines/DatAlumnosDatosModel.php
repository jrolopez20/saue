<?php

class DatAlumnosDatosModel extends ZendExt_Model {

    public function DatAlumnosDatosModel() {
        parent::ZendExt_Model();
    }

    public function insertarDatosAlumno($empresa_trab, $cargo_empresa, $ciudad_trabajo, $direccion_trbajo, $telefono_trabajo, $nombre_padre, $apellidos_padre, $direccion_padre, $telefono_padre, $profesion_padre, $cargo_padre, $empresa_padre, $apellidos_madre, $nombre_madre, $direccion_madre, $telefono_madre, $profesion_madre, $cargo_madre, $empresa_madre, $fecha, $usuario, $estado, $idalumno) {
        try {
            $alumnodatos = new DatAlumnosDatos();
            $alumnodatos->empresa_trab = $empresa_trab;
            $alumnodatos->cargo_empresa = $cargo_empresa;
            $alumnodatos->ciudad_trabajo = $ciudad_trabajo;
            $alumnodatos->direccion_trab = $direccion_trbajo;
            $alumnodatos->telefono_trab = $telefono_trabajo;
            $alumnodatos->nombre_padre = $nombre_padre;
            $alumnodatos->apellidos_padre = $apellidos_padre;
            $alumnodatos->direccion_padre = $direccion_padre;
            $alumnodatos->telefono_padre = $telefono_padre;
            $alumnodatos->profesion_padre = $profesion_padre;
            $alumnodatos->cargo_padre = $cargo_padre;
            $alumnodatos->empresa_padre = $empresa_padre;
            $alumnodatos->apellidos_madre = $apellidos_madre;
            $alumnodatos->nombre_madre = $nombre_madre;
            $alumnodatos->direccion_madre = $direccion_madre;
            $alumnodatos->telefono_madre = $telefono_madre;
            $alumnodatos->profesion_madre = $profesion_madre;
            $alumnodatos->cargo_madre = $cargo_madre;
            $alumnodatos->empresa_madre = $empresa_madre;
            $alumnodatos->fecha = $fecha;
            $alumnodatos->idusuario = $usuario;
            $alumnodatos->estado = $estado;
            $alumnodatos->idalumno = $idalumno;

            $alumnodatos->save();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }
    public function modificarDatosAlumno($id_alumnodatos, $empresa_trab, $cargo_empresa, $ciudad_trabajo, $direccion_trbajo, $telefono_trabajo, $nombre_padre, $apellidos_padre, $direccion_padre, $telefono_padre, $profesion_padre, $cargo_padre, $empresa_padre, $apellidos_madre, $nombre_madre, $direccion_madre, $telefono_madre, $profesion_madre, $cargo_madre, $empresa_madre, $fecha, $usuario, $estado, $idalumno) {
        try {
            $alumnodatos = Doctrine::getTable("DatAlumnosDatos")->find($id_alumnodatos);
            //print_r($alumnodatos);die('afdsfsgfs');
            $alumnodatos->empresa_trab = $empresa_trab;
            $alumnodatos->cargo_empresa = $cargo_empresa;
            $alumnodatos->ciudad_trabajo = $ciudad_trabajo;
            $alumnodatos->direccion_trab = $direccion_trbajo;
            $alumnodatos->telefono_trab = $telefono_trabajo;
            $alumnodatos->nombre_padre = $nombre_padre;
            $alumnodatos->apellidos_padre = $apellidos_padre;
            $alumnodatos->direccion_padre = $direccion_padre;
            $alumnodatos->telefono_padre = $telefono_padre;
            $alumnodatos->profesion_padre = $profesion_padre;
            $alumnodatos->cargo_padre = $cargo_padre;
            $alumnodatos->empresa_padre = $empresa_padre;
            $alumnodatos->apellidos_madre = $apellidos_madre;
            $alumnodatos->nombre_madre = $nombre_madre;
            $alumnodatos->direccion_madre = $direccion_madre;
            $alumnodatos->telefono_madre = $telefono_madre;
            $alumnodatos->profesion_madre = $profesion_madre;
            $alumnodatos->cargo_madre = $cargo_madre;
            $alumnodatos->empresa_madre = $empresa_madre;
            $alumnodatos->fecha = $fecha;
            $alumnodatos->idusuario = $usuario;
            $alumnodatos->estado = $estado;
            $alumnodatos->idalumno = $idalumno;

            $alumnodatos->save();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

}
