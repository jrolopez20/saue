<?php

class DatDocumentoRequerido extends BaseDatDocumentoRequerido
{
    static public function cargarDocReq($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatDocumentoRequerido d")
            ->limit($limit)
            ->offset($start)
            ->orderBy('d.iddocumentorequerido')
            ->execute();
        return $result->toArray();
    }

    static public function countDocReq()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatDocumentoRequerido d")
            ->execute();
        return $result->count();

    }

    static public function eliminarDocReq($iddocumentorequerido)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatDocumentoRequerido d")
            ->where("d.iddocumentorequerido=?", $iddocumentorequerido)
            ->execute();
        return $result;
    }

    static public function cargarDocReqEst($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("select dr.descripcion, dr.iddocumentorequerido,edr.idestudiante
        from mod_saue.dat_documento_requerido dr
        LEFT JOIN mod_saue.dat_alumno_dat_documento_requerido edr
        on dr.iddocumentorequerido = edr.iddocumentorequerido order by dr.iddocumentorequerido LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();
 
        return $result;
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatAlumnoDatDocumentoRequerido', array('local' => 'iddocumentorequerido', 'foreign' => 'iddocumentorequerido'));
    }

}

