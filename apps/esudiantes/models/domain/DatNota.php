<?php

class DatNota extends BaseDatNota
{
  static public function cargarNotasCurso($limit, $start, $idmateria, $idpd)
    {
    	$query = Doctrine_Query::create();

    	$stmt = $query->getConnection()->prepare("SELECT n.idperiododocente as idpd, n.idalumno as idalumno, 
    		n.idmateria as idmateria , n.nota1 as nota1,
    		n.nota2 as nota2, n.nota3 as nota3, n.nota4 as nota4, a.nombre as nombre, a.apellidos as apllidos,
    		 a.codigo as codigo, m.codmateria as codmateria, m.descripcion as materia, est.abreviatura as facultad
       FROM mod_saue.dat_materia m, mod_saue.dat_alumno a, mod_saue.dat_carrera c, mod_saue.dat_nota n, 
       mod_estructuracomp.dat_estructura est, mod_saue.dat_enfasis enf
       WHERE n.idperiododocente = " . $idpd . " AND  n.idalumno = a.idalumno   AND n.idmateria= " . $idmateria . "
       AND m.idmateria = " . $idmateria . " AND a.idenfasis = enf.idenfasis AND enf.idcarrera = c.idcarrera 
       AND c.idfacultad = est.idestructura
       ORDER BY  a.nombre  ASC LIMIT " . $limit . " OFFSET " . $start . ";");

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
	
	static public function cargarNotasAlumno($limit, $start, $idalumno, $idpd)
    {
    	$query = Doctrine_Query::create();

    	$stmt = $query->getConnection()->prepare("SELECT n.idperiododocente as idpd, n.idalumno as idalumno, 
    		n.idmateria as idmateria , n.nota1 as nota1,
    		n.nota2 as nota2, n.nota3 as nota3, n.nota4 as nota4, a.nombre as nombre, a.apellidos as apllidos,
    		 a.codigo as codigo, m.codmateria as codmateria, m.descripcion as materia, est.abreviatura as facultad
       FROM mod_saue.dat_materia m, mod_saue.dat_alumno a, mod_saue.dat_carrera c, mod_saue.dat_nota n, 
       mod_estructuracomp.dat_estructura est, mod_saue.dat_enfasis enf
       WHERE n.idperiododocente = " . $idpd . " AND  n.idalumno = " . $idalumno . " AND  a.idalumno = " . $idalumno . "  AND n.idmateria=m.idmateria
       AND a.idenfasis = enf.idenfasis AND enf.idcarrera = c.idcarrera AND c.idfacultad = est.idestructura
       ORDER BY  a.codigo  ASC LIMIT " . $limit . " OFFSET " . $start . ";");

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

	static public function countNotasAlumno($idestudiante, $idpd)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatNota n")
            ->where("n.idperiododocente = " . $idpd . " AND  n.idalumno = " . $idestudiante )
            ->execute();
        return $result->count();

    }
	static public function countNotasCurso($idmateria, $idpd)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatNota n")
            ->where("n.idperiododocente = " . $idpd . " AND  n.idmateria = " . $idmateria)
            ->execute();
        return $result->count();

    }
    public function setUp()
    {
        parent :: setUp ();
    }


}

