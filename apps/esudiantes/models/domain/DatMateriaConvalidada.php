<?php

class DatMateriaConvalidada extends BaseDatMateriaConvalidada
{

	
	static public function cargarMateriasConvalidar($limit, $start,  $idenfasis, $idpensum , $iduniversidad, $idalumno )
    {
    	$query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("
			SELECT m.codmateria, m.descripcion, m.idmateria,
        	(Select  mc.descripcion  from  mod_saue.dat_materia ma, mod_saue.dat_materia_convalidada mc 
        		WHERE mc.idmateria = m.idmateria AND mc.iduniversidad = ".$iduniversidad." AND mc.principal = true LIMIT 1) 
        		as materiaprincipal,
        	(Select  mc.idmateriaconva  from  mod_saue.dat_materia ma, mod_saue.dat_materia_convalidada mc 
        		WHERE mc.idmateria = m.idmateria AND mc.iduniversidad = ".$iduniversidad." AND mc.principal = true LIMIT 1) 
        		as idmateriaconva
                FROM mod_saue.dat_pensum_materia_enfasis_tipomateria pme, mod_saue.dat_materia m
                WHERE pme.idenfasis = ".$idenfasis." AND pme.idpensum = ".$idpensum." 
                AND m.idmateria = pme.idmateria AND m.idmateria NOT IN (SELECT idmateria from  mod_saue.dat_alumno_mate_deta md WHERE idalumno = ".$idalumno.")
        		LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

   	static public function countMateriasConvalidar( $idenfasis, $idpensum, $idalumno)
    {
    	$query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("
			SELECT m.codmateria
                FROM mod_saue.dat_pensum_materia_enfasis_tipomateria pme, mod_saue.dat_materia m
                WHERE pme.idenfasis = ".$idenfasis." AND pme.idpensum = ".$idpensum." 
                AND m.idmateria = pme.idmateria AND  m.idmateria NOT IN (SELECT idmateria from  mod_saue.dat_alumno_mate_deta md WHERE idalumno = ".$idalumno.");");
        $stmt->execute();
        $result = $stmt->fetchAll();
        return count($result);
    }
	static public function cargarMateriasConva($limit, $start, $iduniversidad, $idmateria)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMateriaConvalidada mc")
            ->limit($limit)
            ->offset($start)
            ->where('mc.iduniversidad= ? AND mc.idmateria= ?' , array($iduniversidad, $idmateria))
            ->execute();
        return $result->toArray();
    }

    static public function eliminarMateriaConvalidacion($idmateriaconva){
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatMateriaConvalidada mc")
            ->where("mc.idmateriaconva=?", $idmateriaconva)
            ->execute();
        return true;
    }    
    static public function updatePrinMateriaConvalidacion($iduniversidad, $idmateria){
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("UPDATE mod_saue.dat_materia_convalidada
                   SET  principal='FALSE' WHERE iduniversidad=".$iduniversidad." and idmateria=".$idmateria.";");
        $stmt->execute();
        return true;
    }

   	static public function countMateriasConva( $iduniversidad, $idmateria)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMateriaConvalidada mc")
            ->where('mc.iduniversidad= ? AND mc.idmateria= ?' , array($iduniversidad, $idmateria))
            ->execute();
        return $result->count();

    }


    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatMateria', array ('local' => 'idmateria', 'foreign' => 'idmateria'));
        $this->hasOne ('DatUniversidad', array ('local' => 'iduniversidad', 'foreign' => 'iduniversidad'));
    }


}

