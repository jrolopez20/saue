<?php

abstract class BaseDatAlumnoMateDeta extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_alumno_mate_deta');
        $this->hasColumn('idmateriahomo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idtipoaprobado', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idmateriaconva', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('observacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('falta', 'integer', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nota', 'double precision', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idenfasis', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('par_curs', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idmateria', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idperiodo', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idalumno_mate_deta', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_alumno_mate_deta'));
    }


}

