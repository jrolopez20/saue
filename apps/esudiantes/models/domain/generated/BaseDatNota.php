<?php

abstract class BaseDatNota extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_nota');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('recurso', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('observacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nota4', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nota3', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nota2', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nota1', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idmateria', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idperiododocente', 'numeric', null, array ('notnull' => false,'primary' => true));
    }


}

