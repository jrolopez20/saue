<?php

abstract class BaseDatAlumno extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_alumno');
        $this->hasColumn('idpensum', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('cant_hijos', 'integer', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('observaciones', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idprovinciacoleg', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idprovinciauniv', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idenfasis', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('pasaporte', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cedula', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('e_mail2', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('e_mail', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('religion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('pais', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nacionalidad', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('lugar_nacimiento', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('celular', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('telefono', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('domicilio', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fec_nacimiento', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('codigo', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuarioasig', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpensumenfasismateriatipo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idcolegio', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idcanton', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('iduniversidad', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idsectorciudad', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idtipoalumno', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idprovincia', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('sexo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestadocivil', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('apellidos', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_alumno'));
    }


}

