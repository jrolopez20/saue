<?php

abstract class BaseDatEstudios extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_estudios');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false ));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idenfasis', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idcarrera', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idfacultad', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => false,'primary' => true));
    }


}

