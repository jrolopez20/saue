<?php

abstract class BaseDatAlumnoDatMencion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_alumno_dat_mencion');
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idmencion', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idalumnomencion', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_alumno_dat_mencion'));
    }


}

