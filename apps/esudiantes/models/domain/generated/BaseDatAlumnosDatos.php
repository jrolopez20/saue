<?php

abstract class BaseDatAlumnosDatos extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_alumnos_datos');
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('empresa_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cargo_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('profesion_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('telefono_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('direccion_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nombre_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('apellidos_madre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('empresa_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cargo_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('profesion_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('telefono_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('direccion_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('apellidos_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nombre_padre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('telefono_trab', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('direccion_trab', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('ciudad_trabajo', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cargo_empresa', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('empresa_trab', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('id_alumnodatos', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_alumno_datos'));
    }


}

