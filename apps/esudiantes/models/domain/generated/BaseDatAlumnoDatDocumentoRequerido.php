<?php

abstract class BaseDatAlumnoDatDocumentoRequerido extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_alumno_dat_documento_requerido');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('iddocumentorequerido', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idestudiante', 'numeric', null, array ('notnull' => false,'primary' => true));
    }


}

