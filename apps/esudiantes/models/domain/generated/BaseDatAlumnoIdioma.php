<?php

abstract class BaseDatAlumnoIdioma extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_alumno_idioma');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('examen', 'real', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('ididioma', 'numeric', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('idalumno', 'numeric', null, array ('notnull' => false,'primary' => true));
    }


}

