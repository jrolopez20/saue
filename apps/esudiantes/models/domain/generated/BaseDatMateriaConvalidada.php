<?php

abstract class BaseDatMateriaConvalidada extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_materia_convalidada');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('principal', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idmateria', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('iduniversidad', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idmateriaconva', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_materia_convalidada'));
    }


}

