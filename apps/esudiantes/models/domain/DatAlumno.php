<?php

class DatAlumno extends BaseDatAlumno
{
    static public function cargarAlumnos($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT e.idalumno as alumno, e.nombre, e.apellidos, e.idusuario, 
                e.fecha, e.idestadocivil,ec.descripcion as estadocivil, e.sexo, e.idprovincia, p.descripcion as provincia,
                e.idtipoalumno,te.descripcion as tipoalumno, e.idsectorciudad, sc.descripcion as sectorciudad, 
                e.iduniversidad,uni.descripcion as universidad, e.idcanton, e.idcolegio, col.descripcion as colegio,
                e.idpensumenfasismateriatipo, e.estado, e.idusuarioasig, e.codigo, e.fec_nacimiento, e.domicilio,
                e.telefono, e.celular,e.lugar_nacimiento, e.nacionalidad, e.pais, e.religion, e.e_mail, e.e_mail2,
                e.cedula, e.pasaporte, e.idenfasis, enf.descripcion as enfasis, e.observaciones, e.idprovinciauniv, 
                p1.descripcion as provUniv, e.idprovinciacoleg, p2.descripcion as provColeg, e.cant_hijos,
                car.idcarrera, car.descripcion as carrera,e.idpensum, pen.descripcion as pensum, car.idfacultad as idestructura, us.nombreusuario,
                est.denominacion as facultad, ad.*
                FROM mod_saue.dat_alumno e
                LEFT JOIN mod_saue.dat_provincia p ON p.idprovincia=e.idprovincia
                INNER JOIN mod_saue.dat_provincia p1 ON p1.idprovincia=e.idprovinciauniv
                LEFT JOIN mod_saue.dat_pensum pen ON pen.idpensum=e.idpensum
                INNER JOIN mod_saue.dat_provincia p2 ON p2.idprovincia=e.idprovinciacoleg
                LEFT JOIN mod_saue.dat_estadocivil ec ON ec.idestadocivil=e.idestadocivil
                LEFT JOIN mod_saue.dat_sector_delaciudad sc ON sc.idsectorciudad=e.idsectorciudad                 
                LEFT JOIN mod_saue.dat_colegios col ON col.idcolegio=e.idcolegio                
                LEFT JOIN mod_saue.dat_universidad uni ON uni.iduniversidad=e.iduniversidad
                INNER JOIN mod_saue.dat_tipo_alumno te ON te.idtipoalumno=e.idtipoalumno
                INNER JOIN mod_seguridad.seg_usuario us ON us.idusuario=e.idusuario
                INNER JOIN mod_saue.dat_enfasis enf ON e.idenfasis=enf.idenfasis
                INNER JOIN mod_saue.dat_carrera car ON car.idcarrera=enf.idcarrera
                INNER JOIN mod_estructuracomp.dat_estructura est ON est.idestructura=car.idfacultad
                LEFT JOIN mod_saue.dat_alumnos_datos ad ON ad.idalumno=e.idalumno
                ORDER BY e.apellidos ASC LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();
        // print_r($result);die;
        return $result;
    }

    static public function cargarAlumnosByNA($limit, $start, $filter)
    {
        $query = Doctrine_Query::create();

        $stmt = $query->getConnection()->prepare("SELECT e.idalumno as alumno, e.nombre, e.apellidos, e.idusuario, 
                e.fecha, e.idestadocivil,ec.descripcion as estadocivil, e.sexo, e.idprovincia, p.descripcion as provincia,
                e.idtipoalumno,te.descripcion as tipoalumno, e.idsectorciudad, sc.descripcion as sectorciudad, 
                e.iduniversidad,uni.descripcion as universidad, e.idcanton, e.idcolegio, col.descripcion as colegio,
                e.idpensumenfasismateriatipo, e.estado, e.idusuarioasig, e.codigo, e.fec_nacimiento, e.domicilio,
                e.telefono, e.celular,e.lugar_nacimiento, e.nacionalidad, e.pais, e.religion, e.e_mail, e.e_mail2,
                e.cedula, e.pasaporte, e.idenfasis, enf.descripcion as enfasis, e.observaciones, e.idprovinciauniv,
                p1.descripcion as provUniv, e.idprovinciacoleg, p2.descripcion as provColeg, e.cant_hijos,
                car.idcarrera, e.idpensum, pen.descripcion as pensum, car.descripcion as carrera,car.idfacultad as idestructura,
                us.nombreusuario, est.denominacion as facultad, ad.*
                FROM mod_saue.dat_alumno e
                LEFT JOIN mod_saue.dat_provincia p ON p.idprovincia=e.idprovincia
                INNER JOIN mod_saue.dat_provincia p1 ON p1.idprovincia=e.idprovinciauniv
                INNER JOIN mod_saue.dat_provincia p2 ON p2.idprovincia=e.idprovinciacoleg
                LEFT JOIN mod_saue.dat_estadocivil ec ON ec.idestadocivil=e.idestadocivil
                LEFT JOIN mod_saue.dat_pensum pen ON pen.idpensum=e.idpensum
                LEFT JOIN mod_saue.dat_sector_delaciudad sc ON sc.idsectorciudad=e.idsectorciudad                 
                LEFT JOIN mod_saue.dat_colegios col ON col.idcolegio=e.idcolegio                
                LEFT JOIN mod_saue.dat_universidad uni ON uni.iduniversidad=e.iduniversidad
                INNER JOIN mod_saue.dat_tipo_alumno te ON te.idtipoalumno=e.idtipoalumno
                INNER JOIN mod_seguridad.seg_usuario us ON us.idusuario=e.idusuario
                INNER JOIN mod_saue.dat_enfasis enf ON e.idenfasis=enf.idenfasis
                INNER JOIN mod_saue.dat_carrera car ON car.idcarrera=enf.idcarrera
                INNER JOIN mod_estructuracomp.dat_estructura est ON est.idestructura=car.idfacultad
                LEFT JOIN mod_saue.dat_alumnos_datos ad ON ad.idalumno=e.idalumno 
                WHERE LOWER(e.nombre) LIKE '%" . strtolower($filter[0]->value) . "%' OR LOWER(e.apellidos) LIKE '%".  strtolower($filter[1]->value) ."%'
                ORDER BY e.apellidos ASC LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    static public function countAlumnosByNA($filter)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAlumno e")
            ->where("LOWER(e.nombre) LIKE '%" . strtolower($filter[0]->value) . "%' OR LOWER(e.apellidos) LIKE '%".  strtolower($filter[1]->value) ."%' ")
            ->execute();
        return $result->count();

    }

    static public function countAlumnos()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAlumno e")
            ->execute();
        return $result->count();

    }

    static public function eliminarAlumno($idalumno)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatAlumno e")
            ->where("e.idalumno=?", $idalumno)
            ->execute();
        return true;
    }

    public function setUp()
    {
        parent :: setUp();
        //$this->hasOne('DatEstadocivil', array('local' => 'idestadocivil', 'foreign' => 'idestadocivil'));
        //$this->hasOne('DatProvincia', array('local' => 'idprovincia', 'foreign' => 'idprovincia'));
        $this->hasOne('DatTipoAlumno', array('local' => 'idtipoalumno', 'foreign' => 'idtipoalumno'));
        //$this->hasOne('DatSectorDelaciudad', array('local' => 'idsectorciudad', 'foreign' => 'idsectorciudad'));
        //$this->hasOne('DatUniversidad', array('local' => 'iduniversidad', 'foreign' => 'iduniversidad'));
        //$this->hasOne('DatCanton', array('local' => 'idcanton', 'foreign' => 'idcanton'));
        //$this->hasOne('DatColegios', array('local' => 'idcolegio', 'foreign' => 'idcolegio'));
        //$this->hasOne('PensumMateriaEnfasisTipomateria', array('local' => 'idpensumenfasismateriatipo', 'foreign' => 'idpensumenfasismateriatipo'));
        $this->hasMany('DatAlumnoDatDocumentoRequerido', array('local' => 'idestudiante', 'foreign' => 'idalumno'));
    }

}



