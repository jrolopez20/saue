<?php

class DatEstudios extends BaseDatEstudios
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatAlumno', array ('local' => 'idalumno', 'foreign' => 'idalumno'));
        $this->hasOne ('DatCarrera', array ('local' => 'idcarrera', 'foreign' => 'idcarrera'));
        $this->hasOne ('DatEnfasis', array ('local' => 'idenfasis', 'foreign' => 'idenfasis'));
    }


}

