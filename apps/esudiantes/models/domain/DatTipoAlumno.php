<?php

class DatTipoAlumno extends BaseDatTipoAlumno
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatAlumno', array('local' => 'idtipoalumno', 'foreign' => 'idtipoalumno'));
    }


    static public function cargarTiposEstudiantes($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAlumno te")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }   

    static public function cargarTipoxEstudiante($idestudiante)
    {

        $query = Doctrine_Query::create();
        $result = $query
            ->select("te.descripcion")
            ->from("DatTipoAlumno te, DatAlumno a")
            ->where("a.idalumno = ".$idestudiante." ")
            ->addWhere("te.idtipoalumno = a.idtipoalumno")
            ->execute();
            $value =$result->toArray(); 
        return $value[0]['descripcion'];
    }

    static public function countTipoEst()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAlumno te")
            ->execute();
        return $result->count();

    }

    static public function eliminarTipoEstud($idtipoestudiante)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatTipoAlumno te")
            ->where("te.idtipoalumno=?", $idtipoestudiante)
            ->execute();
        return $result;
    }

}



