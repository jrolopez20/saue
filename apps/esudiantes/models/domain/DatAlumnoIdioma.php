<?php

class DatAlumnoIdioma extends BaseDatAlumnoIdioma
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatAlumno', array ('local' => 'idalumno', 'foreign' => 'idalumno'));
        $this->hasOne ('DatIdioma', array ('local' => 'ididioma', 'foreign' => 'ididioma'));
    }


}

