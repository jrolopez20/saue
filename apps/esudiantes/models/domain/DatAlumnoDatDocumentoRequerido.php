<?php

class DatAlumnoDatDocumentoRequerido extends BaseDatAlumnoDatDocumentoRequerido
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatAlumno', array('local' => 'idalumno', 'foreign' => 'idestudiante'));
        $this->hasOne('DatDocumentoRequerido', array('local' => 'iddocumentorequerido', 'foreign' => 'iddocumentorequerido'));
    }

    static public function eliminarDocEst($idalumno, $iddocreq)
    {
        $query = Doctrine_Query::create();
        $query->delete()
            ->from("DatAlumnoDatDocumentoRequerido edr")
            ->where("edr.idestudiante=? AND edr.iddocumentorequerido=?", array($idalumno, $iddocreq))
            ->execute();
        return true;
    }
}

