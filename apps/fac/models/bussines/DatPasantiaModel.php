<?php

class DatPasantiaModel extends ZendExt_Model
{

    public function DatPasantiaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarPasantias($limit = 20, $start = 0, $filtros = null)
    {
        try {
            return DatPasantia::cargarPasantias($limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarEnfasis($limit = 20, $start = 0, $idcarrera = null)
    {
        try {
            return DatPasantia::cargarEnfasis($idcarrera, $limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarTiposPasantias($limit = 20, $start = 0)
    {
        try {
            return DatPasantia::cargarTipoPasantias($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countPasantias($filtros = null)
    {
        try {
            return DatPasantia::countPasantias($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarPasantiasService($idcarrera, $limit, $start)
    {
        try {
            return DatPasantia::cargarPasantiasService($idcarrera, $limit = 20, $start = 0);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countPasantiasService()
    {
        try {
            return DatPasantia::countPasantiasService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarPasantias($idfacultad, $idcarrera, $idenfasis, $idtipopasantia, $empresa, $horas,
                                      $estado, $usuario, $fecha)
    {
        try {
            $pasantias = new DatPasantia();

            $pasantias->idfacultad = $idfacultad;
            $pasantias->idcarrera = $idcarrera;
            $pasantias->idenfasis = $idenfasis;
            $pasantias->idtipopractica = $idtipopasantia;
            $pasantias->empresa = $empresa;
            $pasantias->horas = $horas;

            $pasantias->idusuario = $usuario;
            $pasantias->fecha = $fecha;
            $pasantias->estado = $estado;

            $pasantias->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarPasantias($idpasantia, $idfacultad, $idcarrera, $idenfasis, $idtipopasantia, $empresa,
                                       $horas, $estado, $usuario, $fecha)
    {
        try {
            $pasantias = Doctrine::getTable("DatPasantia")->find($idpasantia);

            $pasantias->idfacultad = $idfacultad;
            $pasantias->idcarrera = $idcarrera;
            $pasantias->idenfasis = $idenfasis;
            $pasantias->idtipopractica = $idtipopasantia;
            $pasantias->empresa = $empresa;
            $pasantias->horas = $horas;

            $pasantias->idusuario = $usuario;
            $pasantias->fecha = $fecha;
            $pasantias->estado = $estado;

            $pasantias->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarPasantias($idpasantia)
    {
        try {
            $pasantias = Doctrine::getTable("DatPasantia")->find($idpasantia);
            $pasantias->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
