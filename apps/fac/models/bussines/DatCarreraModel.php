<?php

class DatCarreraModel extends ZendExt_Model
{

    public function DatCarreraModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarCarreras($limit = 20, $start = 0, $filtros = null)
    {
        return DatCarrera::cargarCarreras($limit, $start, $filtros);
    }

    public function countCarreras($filtros = null)
    {
        try {
            return DatCarrera::countCarreras($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarFacultades($limit, $start)
    {
        try {
            return $this->integrator->metadatos->ListadoEstructurasT($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarCarrera($idfacultad, $descripcion, $usuario, $estado, $fecha)
    {
        try {
            $carrera = new DatCarrera();
            $carrera->idusuario = $usuario;
            $carrera->idfacultad = $idfacultad;
            $carrera->descripcion = $descripcion;
            $carrera->estado = $estado;
            $carrera->fecha = $fecha;
            $carrera->save();
            return $carrera->idcarrera;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarCarrera($idcarrera, $idfacultad, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $carrera = Doctrine::getTable("DatCarrera")->find($idcarrera);
            $carrera->idfacultad = $idfacultad;
            $carrera->idusuario = $usuario;
            $carrera->descripcion = $descripcion;
            $carrera->estado = $estado;
            $carrera->fecha = $fecha;
            $carrera->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarCarrera($idcarrera)
    {
        try {
            $carrera = Doctrine::getTable("DatCarrera")->find($idcarrera);
            $carrera->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
