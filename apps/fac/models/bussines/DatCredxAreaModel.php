<?php

class DatCredxAreaModel extends ZendExt_Model
{

    public function DatCredxAreaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarCredsxArea($limit = 20, $start = 0, $filtros = null)
    {
        try {
            return DatAreaCredito::cargarCredsxArea($limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countCredsxArea($filtros = null)
    {
        try {
            return DatAreaCredito::countCredsxArea($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarEnfasis($idcarrera, $limit, $start)
    {
        try {
            return DatAreaCredito::cargarEnfasis($idcarrera, $limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarPensums($idcarrera, $limit, $start)
    {
        try {
            return DatAreaCredito::cargarPensums($idcarrera, $limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAreasGenerales($limit, $start)
    {
        try {
            return $this->integrator->materia->cargarAreasGenerales($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAreas($idareageneral, $limit, $start)
    {
        try {
            if ($idareageneral) {
                $filtro = new stdClass();
                $filtro->property = 'idareageneral';
                $filtro->value = $idareageneral;
                $filtros = array($filtro);
            }
            return $this->integrator->materia->cargarAreas($limit, $start, $filtros);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarCredsxArea($idenfasis, $idpensum, $idareageneral, $idarea, $creditos, $usuario,
                                       $estado, $fecha)
    {
        try {
            $area_credito = new DatAreaCredito();
            $area_credito->idenfasis = $idenfasis;
            $area_credito->idpensum = $idpensum;
            $area_credito->idareageneral = $idareageneral;
            $area_credito->idarea = $idarea;
            $area_credito->creditos = $creditos;
            $area_credito->estado = $estado;
            $area_credito->fecha = $fecha;
            $area_credito->idusuario = $usuario;
            $area_credito->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarCredsxArea($idareacredito, $idenfasis, $idpensum, $idareageneral, $idarea, $creditos,
                                        $usuario, $estado, $fecha)
    {
        try {
            $area_credito = Doctrine::getTable("DatAreaCredito")->find($idareacredito);
            $area_credito->idenfasis = $idenfasis;
            $area_credito->idpensum = $idpensum;
            $area_credito->idareageneral = $idareageneral;
            $area_credito->idarea = $idarea;
            $area_credito->creditos = $creditos;
            $area_credito->estado = $estado;
            $area_credito->fecha = $fecha;
            $area_credito->idusuario = $usuario;
            $area_credito->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarCredsxArea($idareacredito)
    {
        try {
            $area_credito = Doctrine::getTable("DatAreaCredito")->find($idareacredito);
            $area_credito->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
