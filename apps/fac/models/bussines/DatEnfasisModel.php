<?php

class DatEnfasisModel extends ZendExt_Model
{

    public function DatEnfasisModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarEnfasis($limit = 20, $start = 0, $filtros = null)
    {
        return DatEnfasis::cargarEnfasis($limit, $start, $filtros);
    }

    public function cargarCarreras($limit = 20, $start = 0, $idfacultad = null )
    {
        return DatEnfasis::cargarCarreras($idfacultad, $limit, $start);
    }

    public function countEnfasis($filtros = null)
    {
        try {
            return DatEnfasis::countEnfasis($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarEnfasisService($idcarrera, $limit, $start)
    {
        return DatEnfasis::cargarEnfasisService($idcarrera, $limit = 20, $start = 0);
    }

    public function countEnfasisService()
    {
        try {
            return DatEnfasis::countEnfasisService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarEnfasis($descripcion, $estado, $idcarrera, $usuario, $fecha)
    {
        try {
            $enfasis = new DatEnfasis();
            $enfasis->idusuario = $usuario;
            $enfasis->descripcion = $descripcion;
            $enfasis->estado = $estado;
            $enfasis->fecha = $fecha;
            $enfasis->idcarrera = $idcarrera;
            $enfasis->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarEnfasis($idenfasis, $descripcion, $estado, $idcarrera, $usuario, $fecha)
    {
        try {
            $enfasis = Doctrine::getTable("DatEnfasis")->find($idenfasis);
            $enfasis->idusuario = $usuario;
            $enfasis->descripcion = $descripcion;
            $enfasis->estado = $estado;
            $enfasis->fecha = $fecha;
            $enfasis->idcarrera = $idcarrera;
            $enfasis->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarEnfasis($idenfasis)
    {
        try {
            $enfasis = Doctrine::getTable("DatEnfasis")->find($idenfasis);
            $enfasis->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
