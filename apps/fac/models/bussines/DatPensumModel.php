<?php

class DatPensumModel extends ZendExt_Model
{

    public function DatPensumModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarPensums($limit = 20, $start = 0, $filtros = null)
    {
        return DatPensum::cargarPensum($limit, $start, $filtros);
    }

    public function countPensums($filtros = null)
    {
        try {
            return DatPensum::countPensum($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarPensum($idcarrera, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $pensum = new DatPensum();
            $pensum->idusuario = $usuario;
            $pensum->idcarrera = $idcarrera;
            $pensum->descripcion = $descripcion;
            $pensum->estado = $estado;
            $pensum->fecha = $fecha;
            $pensum->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarPensum($idpensum, $idcarrera, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $pensum = Doctrine::getTable("DatPensum")->find($idpensum);
            $pensum->idusuario = $usuario;
            $pensum->idcarrera = $idcarrera;
            $pensum->descripcion = $descripcion;
            $pensum->estado = $estado;
            $pensum->fecha = $fecha;
            $pensum->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarPensum($idpensum)
    {
        try {
            $pensum = Doctrine::getTable("DatPensum")->find($idpensum);
            $pensum->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarCarreras($limit = 20, $start = 0, $idfacultad = null )
    {
        return DatPensum::cargarCarreras($idfacultad, $limit, $start);
    }
}
