<?php

class DatMencionModel extends ZendExt_Model
{

    public function DatMencionModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarMencion($limit = 20, $start = 0, $filtros = null)
    {
        return DatMencion::cargarMencion($limit, $start, $filtros);
    }

    public function countMencion($filtros = null)
    {
        try {
            return DatMencion::countPernsum($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarMencion($idfacultad, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $mencion = new DatMencion();
            $mencion->idusuario = $usuario;
            $mencion->idfacultad = $idfacultad;
            $mencion->descripcion = $descripcion;
            $mencion->estado = $estado;
            $mencion->fecha = $fecha;
            $mencion->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarMencion($idmencion, $idfacultad, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $mencion = Doctrine::getTable("DatMencion")->find($idmencion);
            $mencion->idusuario = $usuario;
            $mencion->idfacultad = $idfacultad;
            $mencion->descripcion = $descripcion;
            $mencion->estado = $estado;
            $mencion->fecha = $fecha;
            $mencion->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarMencion($idmencion)
    {
        try {
            $mencion = Doctrine::getTable("DatMencion")->find($idmencion);
            $mencion->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
