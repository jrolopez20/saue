<?php

class DatMencion extends BaseDatMencion
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasOne('DatCarrera', array('local' => 'idcarrera', 'foreign' => 'idcarrera'));
    }

    static public function cargarMencion($limit, $start, $filtros)

    {
        $query = Doctrine_Query::create();

        $sql = "SELECT
                    m.*,

                    e.denominacion
                FROM
                    mod_saue.dat_mencion m
                    INNER JOIN mod_estructuracomp.dat_estructura e ON m.idfacultad=e.idestructura";

        $params = array();
        if ($filtros) {
            $sql .= "\nWHERE ";
            $hasWhere = false;
            foreach ($filtros as $filtro) {
                if ($hasWhere)
                    $sql .= "\nAND ";
                if ($filtro->property == 'descripcion')
                    $sql .= "LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'";
                else {
                    $sql .= "m.$filtro->property = ?";
                    $params[] = $filtro->value;
                }
                $hasWhere = true;
            }
        }

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countPernsum($filtros)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT (m.idmencion)
                FROM
                    mod_saue.dat_mencion m
                    INNER JOIN mod_estructuracomp.dat_estructura e ON m.idfacultad=e.idestructura";

        $params = array();
        if ($filtros) {
            $sql .= "\nWHERE ";
            $hasWhere = false;
            foreach ($filtros as $filtro) {
                if ($hasWhere)
                    $sql .= "\nAND ";
                $sql .= "m.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        }

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result['count'];
    }

    static public function cargarMencionAll($limit, $start)
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->from("DatMencion p")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }

    static public function countMencionAll()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMencion p")
            ->execute();
        return $result->count();

    }

    /*static public function eliminarMencion($idmencion)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatMencion p")
            ->where("p.idmencion=?", $idmencion)
            ->execute();
        return $result->toArray();
    }*/
}

