<?php

class DatAreaCredito extends BaseDatAreaCredito
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatPensum', array('local' => 'idpensum', 'foreign' => 'idpensum'));
        $this->hasMany('DatEnfasis', array('local' => 'idenfasis', 'foreign' => 'idenfasis'));
        //$this->hasMany('NomAreaGeneral', array('local' => 'idareageneral', 'foreign' => 'idareageneral'));
        //$this->hasMany('DatArea', array('local' => 'idarea', 'foreign' => 'idarea'));
    }

    static public function cargarCredsxArea($limit, $start, $filtros)
    {
        try {
            $query = Doctrine_Query::create();

            $sql = "SELECT
                    ac.*,

                    a.descripcion as descripcion
                FROM
                    mod_saue.dat_area_credito ac
                    INNER JOIN mod_saue.dat_enfasis e ON e.idenfasis=ac.idenfasis
                    INNER JOIN mod_saue.dat_pensum p ON p.idpensum=ac.idpensum
                    INNER JOIN mod_saue.nom_area_general ag ON ag.idareageneral=ac.idareageneral
                    INNER JOIN mod_saue.dat_area a ON a.idarea=ac.idarea";

            $params = array();
            if ($filtros) {
                $sql .= "\nWHERE ";
                $hasWhere = false;
                foreach ($filtros as $filtro) {
                    if ($hasWhere)
                        $sql .= "\nAND ";
                    $sql .= "ac.$filtro->property = ?";
                    $params[] = $filtro->value;
                    $hasWhere = true;
                }
            }

            $sql .= "\nLIMIT ? OFFSET ?";
            $params[] = $limit;
            $params[] = $start;

            $stmt = $query
                ->getConnection()
                ->prepare($sql);
            $stmt->execute($params);
            $result = $stmt->fetchAll();

            return $result;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    static public function countCredsxArea($filtros)
    {
        try {
            $query = Doctrine_Query::create();

            $sql = "SELECT COUNT (ac.idareacredito)
                FROM
                    mod_saue.dat_area_credito ac
                    INNER JOIN mod_saue.dat_enfasis e ON e.idenfasis=ac.idenfasis
                    INNER JOIN mod_saue.dat_pensum p ON p.idpensum=ac.idpensum
                    INNER JOIN mod_saue.nom_area_general ag ON ag.idareageneral=ac.idareageneral
                    INNER JOIN mod_saue.dat_area a ON a.idarea=ac.idarea";

            $params = array();
            if ($filtros) {
                $sql .= "\nWHERE ";
                $hasWhere = false;
                foreach ($filtros as $filtro) {
                    if ($hasWhere)
                        $sql .= "\nAND ";
                    $sql .= "ac.$filtro->property = ?";
                    $params[] = $filtro->value;
                    $hasWhere = true;
                }
            }

            $stmt = $query
                ->getConnection()
                ->prepare($sql);
            $stmt->execute($params);
            $result = $stmt->fetch();

            return $result['count'];
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    static public function cargarEnfasis($idcarrera, $limit, $start)
    {
        try {
            $query = Doctrine_Query::create()
                ->from("DatEnfasis e")
                ->where('e.estado = ?', true);

            if (isset($idcarrera))
                $query->addWhere("e.idcarrera = ?", $idcarrera);

            $result = $query->limit($limit)
                ->offset($start)
                ->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    static public function cargarPensums($idcarrera, $limit, $start)
    {
        try {
            $query = Doctrine_Query::create()
                ->from("DatPensum p")
                ->where('p.estado = ?', true);

            if (isset($idcarrera))
                $query->addWhere("p.idcarrera = ?", $idcarrera);

            $result = $query->limit($limit)
                ->offset($start)
                ->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

