<?php

class DatCarrera extends BaseDatCarrera
{

    static public function cargarCarreras($limit, $start, $filtros)
    {
        try {
            $query = Doctrine_Query::create();

            $query
                ->from("DatCarrera c");

            if ($filtros) {
                foreach($filtros as $filtro){
                    if(strstr($filtro->property, 'descripcion')){
                        $query->addWhere("LOWER(c.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                    }else{
                        $query->addWhere("c.$filtro->property=?", $filtro->value);
                    }
                }
            }

            $result = $query->limit($limit)
                ->offset($start)
                ->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    static public function countCarreras($filtros)
    {
        try {
            $query = Doctrine_Query::create();

            $query->from("DatCarrera c");

            if ($filtros) {
                foreach($filtros as $filtro){
                    if(strstr($filtro->property, 'descripcion')){
                        $query->addWhere("LOWER(c.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                    }else{
                        $query->addWhere("c.$filtro->property=?", $filtro->value);
                    }
                }
            }

            $result = $query->execute();

            return $result->count();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    static public function eliminarCarrera($idCarrera)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query
                ->delete()
                ->from("DatCarrera c")
                ->where("c.idcarrera=?", $idCarrera)
                ->execute();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatPensum', array('local' => 'idcarrera', 'foreign' => 'idcarrera'));
        $this->hasMany('DatEnfasis', array('local' => 'idcarrera', 'foreign' => 'idcarrera'));
    }
}

