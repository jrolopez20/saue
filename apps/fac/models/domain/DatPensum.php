<?php

class DatPensum extends BaseDatPensum
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatCarrera', array('local' => 'idcarrera', 'foreign' => 'idcarrera'));
        //$this->hasMany('DatPensumMateriaEnfasisTipomateria', array('local' => 'idpensum', 'foreign' => 'idpensum'));
        //$this->hasMany('DatPreRequisitosMateria', array('local' => 'idpensum', 'foreign' => 'idpensum'));
        //$this->hasMany('DatPreRequisitosMateria', array('local' => 'idpensum', 'foreign' => 'idmateriapre'));
    }

    static public function cargarPensum($limit, $start, $filtros)

    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatPensum p")
            ->innerJoin('p.DatCarrera c');

        if ($filtros) {
            foreach($filtros as $filtro){
                if(strstr($filtro->property, 'descripcion')){
                    $query->addWhere("LOWER(p.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                }else{
                    $query->addWhere("p.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray(true);
    }

    static public function countPensum($filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatPensum p")
            ->innerJoin('p.DatCarrera c');

        if ($filtros) {
            foreach($filtros as $filtro){
                if(strstr($filtro->property, 'descripcion')){
                    $query->addWhere("LOWER(p.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                }else{
                    $query->addWhere("p.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->execute();

        return $result->count();
    }

    static public function cargarPensumAll($limit, $start)
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->from("DatPensum p")
            ->innerJoin('p.DatCarrera c')
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray(true);
    }

    static public function countPernsumAll()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatPensum p")
            ->innerJoin('p.DatCarrera c');
        $result->execute();
        return $result->count();

    }

    static public function eliminarPensum($idpensum)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatPensum p")
            ->where("p.idpensum=?", $idpensum)
            ->execute();
        return $result->toArray();
    }

    static public function cargarCarreras($idfacultad, $limit, $start)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatCarrera c")
            ->where("c.estado = ?", true);
        if(isset($idfacultad))
            $query->addWhere("c.idfacultad = ?", $idfacultad);
        $result = $query
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }
}

