<?php

abstract class BaseDatTipoPasantia extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_tipo_practica');
        $this->hasColumn('idtipopractica', 'numeric', 19, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_enfasis'));
        $this->hasColumn('descripcion', 'text', 200, array ('notnull' => true,'primary' => false));

        $this->hasColumn('idusuario', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false, 'default' => true));
    }
}

