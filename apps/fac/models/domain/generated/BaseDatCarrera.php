<?php

abstract class BaseDatCarrera extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_carrera');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => true,'primary' => false, 'default' => true));
        $this->hasColumn('descripcion', 'text', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idfacultad', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcarrera', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_carrera'));
    }


}

