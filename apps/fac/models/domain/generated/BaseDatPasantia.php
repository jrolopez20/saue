<?php

abstract class BaseDatPasantia extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_practica');
        $this->hasColumn('idpractica', 'numeric', 19, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_practica'));
        $this->hasColumn('idfacultad', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idcarrera', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idenfasis', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idtipopractica', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('empresa', 'text', 200, array ('notnull' => true,'primary' => false));
        $this->hasColumn('horas', 'int', null, array ('notnull' => true,'primary' => false));

        $this->hasColumn('idusuario', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false, 'default' => true));
    }


}

