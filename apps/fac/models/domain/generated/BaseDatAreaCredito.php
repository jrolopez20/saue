<?php

abstract class BaseDatAreaCredito extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_area_credito');
        $this->hasColumn('idareacredito', 'numeric', 19, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_area_credito'));
        $this->hasColumn('idenfasis', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idpensum', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idareageneral', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idarea', 'numeric', 19, array ('notnull' => true,'primary' => false));
        $this->hasColumn('creditos', 'real', null, array ('notnull' => true,'primary' => false));

        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => true,'primary' => false, 'default' => true));
    }


}

