<?php

class DatEnfasis extends BaseDatEnfasis
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasMany ('PensumMateriaEnfasisTipomateria', array ('local' => 'idenfasis', 'foreign' => 'idenfasis'));
        //$this->hasMany ('PreRequisitosMateria', array ('local' => 'idenfasis', 'foreign' => 'idenfasis'));
        $this->hasOne('DatCarrera', array('local' => 'idcarrera', 'foreign' => 'idcarrera'));
    }

    static public function cargarEnfasis($limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();
        $query
            ->from("DatEnfasis e")
            ->innerJoin("e.DatCarrera c");

        if ($filtros) {
            foreach ($filtros as $filtro) {
                if (strstr($filtro->property, 'descripcion')) {
                    $query->addWhere("LOWER(e.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                } else {
                    $query->addWhere("e.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray(true);
    }

    static public function countEnfasis($filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatEnfasis e")
            ->innerJoin("e.DatCarrera c");

        if ($filtros) {
            foreach ($filtros as $filtro) {
                if (strstr($filtro->property, 'descripcion')) {
                    $query->addWhere("LOWER(e.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                } else {
                    $query->addWhere("e.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->execute();

        return $result->count();

    }

    static public function cargarEnfasisService($idcarrera, $limit = 20, $start = 0)
    {
        $query = Doctrine_Query::create();
        $query
            ->from("DatEnfasis e")
            ->innerJoin("e.DatCarrera c")
            ->where("e.estado=?", true);
        if (isset($idcarrera))
            $query->addWhere("e.idcarrera = ?", $idcarrera);
        $result = $query->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray(true);
    }

    static public function cargarCarreras($idfacultad, $limit, $start)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatCarrera c")
            ->where("c.estado = ?", true);
        if(isset($idfacultad))
            $query->addWhere("c.idfacultad = ?", $idfacultad);
        $result = $query
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function eliminarEnfasis($idenfasis)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatEnfasis e")
            ->where("e.idenfasis=?", $idenfasis)
            ->execute();
        return $result->toArray();
    }

}




