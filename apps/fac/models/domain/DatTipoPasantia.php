<?php

class DatTipoPasantia extends BaseDatTipoPasantia
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany ('DatPasantia', array ('local' => 'idtipopractica', 'foreign' => 'idtipopractica'));
    }

    static public function cargarTipoPasantias($limit, $start)
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->from("DatTipoPasantia tp")
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function countTipoPasantias()
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->from("DatTipoPasantia tp")
            ->execute();

        return $result->count();

    }
}




