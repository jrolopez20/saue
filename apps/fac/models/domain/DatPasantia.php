<?php

class DatPasantia extends BaseDatPasantia
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasMany ('PensumMateriaPasantiasTipomateria', array ('local' => 'idPasantias', 'foreign' => 'idPasantias'));
        //$this->hasMany ('PreRequisitosMateria', array ('local' => 'idPasantias', 'foreign' => 'idPasantias'));
        $this->hasOne('DatCarrera', array('local' => 'idcarrera', 'foreign' => 'idcarrera'));
        $this->hasOne('DatEnfasis', array('local' => 'idenfasis', 'foreign' => 'idenfasis'));
        $this->hasOne('DatTipoPasantia', array('local' => 'idtipopractica', 'foreign' => 'idtipopractica'));
    }

    static public function cargarPasantias($limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();
        $query
            ->from("DatPasantia p")
            ->innerJoin("p.DatCarrera c")
            ->innerJoin("p.DatEnfasis e")
            ->innerJoin("p.DatTipoPasantia tp");

        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'empresa') {
                    $query->addWhere("LOWER(p.$filtro->property) LIKE '%" . $filtro->value . "%'");
                } else {
                    $query->addWhere("p.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray(true);
    }

    static public function countPasantias($filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatPasantia p")
            ->innerJoin("p.DatCarrera c")
            ->innerJoin("p.DatEnfasis e")
            ->innerJoin("p.DatTipoPasantia tp");

        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'empresa') {
                    $query->addWhere("LOWER(p.$filtro->property) LIKE '%" . $filtro->value . "%'");
                } else {
                    $query->addWhere("p.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->execute();

        return $result->count();

    }

    static public function cargarPasantiasService($idcarrera, $limit = 20, $start = 0)
    {
        $query = Doctrine_Query::create();
        $query
            ->from("DatPasantia p")
            ->innerJoin("p.DatCarrera c")
            ->where("p.estado=?", true);
        if (isset($idcarrera))
            $query->addWhere("p.idcarrera = ?", $idcarrera);
        $result = $query->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray(true);
    }

    static public function cargarCarreras($idfacultad, $limit, $start)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatCarrera c")
            ->where("c.estado = ?", true);
        if (isset($idfacultad))
            $query->addWhere("c.idfacultad = ?", $idfacultad);

        $result = $query
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function cargarEnfasis($idcarrera, $limit, $start)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatEnfasis e")
            ->where("e.estado = ?", true);
        if (isset($idcarrera))
            $query->addWhere("e.idcarrera = ?", $idcarrera);

        $result = $query
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function cargarTipoPasantias($limit, $start)
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->from("DatTipoPasantia tp")
            ->where("tp.estado = ?", true)
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function eliminarPasantias($idPasantias)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatPasantia p")
            ->where("p.idPasantias=?", $idPasantias)
            ->execute();
        return $result->toArray();
    }

}




