<?php

class GestpasantiasController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatPasantiaModel();
    }

    public function gestpasantiasAction()
    {
        $this->render();
    }

    public function cargarPasantiasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $pasantias = $this->model->cargarPasantias($limit, $start, $filtros);
        $cant = $this->model->countPasantias($filtros);

        $result = array('cantidad' => $cant, 'datos' => $pasantias);
        echo json_encode($result);
    }

    public function cargarEnfasisAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');

        $pasantias = $this->model->cargarEnfasis($limit, $start, $idcarrera);

        $result = array('datos' => $pasantias);
        echo json_encode($result);
    }

    public function cargarTiposPasantiasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $tipospasantias = $this->model->cargarTiposPasantias($limit, $start);

        $result = array('datos' => $tipospasantias);
        echo json_encode($result);
    }

    /*public function cargarCarrerasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idfacultad = $this->_request->getPost('idfacultad');

        $carreras = $this->model->cargarCarreras($limit, $start, $idfacultad);

        $result = array('datos' => $carreras);
        echo json_encode($result);
    }*/

    public function insertarPasantiasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idfacultad = $datos->idfacultad;
        $idcarrera = $datos->idcarrera;
        $idenfasis = $datos->idenfasis;
        $idtipopasantia = $datos->idtipopasantia;
        $empresa = $datos->empresa;
        $horas = $datos->horas;
        $estado = $datos->estado;

        //hay que agregar el campo idcarrera en el pasantias
        if ($this->model->insertarPasantias($idfacultad, $idcarrera, $idenfasis, $idtipopasantia, $empresa, $horas,
            $estado, $usuario, $fecha)
        )
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarPasantiasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idpasantia = $datos->idpasantia;
        $idfacultad = $datos->idfacultad;
        $idcarrera = $datos->idcarrera;
        $idenfasis = $datos->idenfasis;
        $idtipopasantia = $datos->idtipopasantia;
        $empresa = $datos->empresa;
        $horas = $datos->horas;
        $estado = $datos->estado;

        //hay que agregar el campo idcarrera en el pasantias
        if ($this->model->modificarPasantias($idpasantia, $idfacultad, $idcarrera, $idenfasis, $idtipopasantia, $empresa,
            $horas, $estado, $usuario, $fecha)
        )
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarPasantiasAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idpasantia = $datos->idpasantia;

        if ($this->model->eliminarPasantias($idpasantia))
            echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>