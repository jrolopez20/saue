<?php

/**
 * Componente para gestionar los sistemas.
 *
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestcarrerasController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatCarreraModel();
    }

    public function gestcarrerasAction()
    {
        $this->render();
    }

    public function cargarCarrerasAction()
    {
        //obtener todas las carreras
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $carreras = $this->model->cargarCarreras($limit, $start, $filtros);
        //ver esto en todas las cargas, si realmente hace falta.
        $cant = $this->model->countCarreras($filtros);

        $result = array('cantidad' => $cant, 'datos' => $carreras);
        echo json_encode($result);
    }

    public function cargarFacultadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $facultades = $this->model->cargarFacultades($limit, $start);

        echo json_encode(array('datos'=>$facultades));
    }

    public function insertarCarrerasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idfacultad = $datos->idfacultad;
        $descripcion = $datos->descripcion_carrera;
        $estado = $datos->estado;

        $idcarrera = $this->model->insertarCarrera($idfacultad, $descripcion, $usuario, $estado, $fecha);
        if ($idcarrera != null)
            //devolver los datos, hay que agregar la etiqueta
            echo("{'idcarrera':$idcarrera,'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarCarrerasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idcarrera = $datos->idcarrera;
        $idfacultad = $datos->idfacultad;
        $descripcion = $datos->descripcion_carrera;
        $estado = $datos->estado;

        if ($this->model->modificarCarrera($idcarrera, $idfacultad, $descripcion, $estado, $usuario, $fecha))
            //hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarCarrerasAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idcarrera = $datos->idcarrera;

        if ($this->model->eliminarCarrera($idcarrera))
            echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>
