<?php

class GestenfasisController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatEnfasisModel();
    }

    public function gestenfasisAction()
    {
        $this->render();
    }

    public function cargarEnfasisAction()
    {
        $idcarrea = $this->_request->getPost('idcarrea');
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $enfasis = $this->model->cargarEnfasis($limit, $start, $filtros);
        $cant = $this->model->countEnfasis($filtros);

        $result = array('cantidad' => $cant, 'datos' => $enfasis);
        echo json_encode($result);
    }

    public function cargarCarrerasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idfacultad = $this->_request->getPost('idfacultad');

        $carreras = $this->model->cargarCarreras($limit, $start, $idfacultad);

        $result = array('datos' => $carreras);
        echo json_encode($result);
    }

    public function insertarEnfasisAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion_enfasi;
        $idcarrera = $datos->idcarrera;
        $estado = $datos->estado;

        //hay que agregar el campo idcarrera en el enfasis
        if ($this->model->insertarEnfasis($descripcion, $estado, $idcarrera, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarEnfasisAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idenfasis = $datos->idenfasis;
        $descripcion = $datos->descripcion_enfasi;
        $idcarrera = $datos->idcarrera;
        $estado = $datos->estado;

        if ($this->model->modificarEnfasis($idenfasis, $descripcion, $estado, $idcarrera, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarEnfasisAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idenfasis = $datos->idenfasis;

        if ($this->model->eliminarEnfasis($idenfasis))
            echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>