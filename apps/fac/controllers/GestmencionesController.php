<?php

class GestmencionesController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatMencionModel();
    }

    public function gestmencionesAction()
    {
        $this->render();
    }

    public function cargarMencionAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $menciones = $this->model->cargarMencion($limit, $start, $filtros);
        $cant = $this->model->countMencion($filtros);

        $result = array('cantidad' => $cant, 'datos' => $menciones);
        echo json_encode($result);
    }

    public function insertarMencionAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion;
        $idfacultad = $datos->idfacultad;
        $estado = $datos->estado;

        if ($this->model->insertarMencion($idfacultad, $descripcion, $estado, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'succes': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");

    }

    public function modificarMencionAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion;
        $idfacultad = $datos->idfacultad;
        $estado = $datos->estado;
        $idmencion = $datos->idmencion;

        if ($this->model->modificarMencion($idmencion, $idfacultad, $descripcion, $estado, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarMencionAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idmencion = $datos->idmencion;

        if ($this->model->eliminarMencion($idmencion))
            echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}