<?php

class GestpensumController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatPensumModel();
    }

    public function gestpensumAction()
    {
        $this->render();
    }

    public function cargarPensumAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $pensums = $this->model->cargarPensums($limit, $start, $filtros);
        $cant = $this->model->countPensums($filtros);

        $result = array('cantidad' => $cant, 'datos' => $pensums);
        echo json_encode($result);
    }

    public function insertarPensumAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion_pensum;
        $idcarrera = $datos->idcarrera;
        $estado = $datos->estado;

        if ($this->model->insertarPensum($idcarrera, $descripcion, $estado, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'succes': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");

    }

    public function cargarCarrerasAction()
    {
        //obtener todas las carreras
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idfacultad = $this->_request->getPost('idfacultad');

        $carreras = $this->model->cargarCarreras($limit, $start, $idfacultad);

        $result = array('datos' => $carreras);
        echo json_encode($result);
    }

    public function modificarPensumAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion_pensum;
        $idcarrera = $datos->idcarrera;
        $estado = $datos->estado;
        $idpensum = $datos->idpensum;

        if ($this->model->modificarPensum($idpensum, $idcarrera, $descripcion, $estado, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarPensumAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idpensum = $datos->idpensum;

        if ($this->model->eliminarPensum($idpensum))
            echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>