<?php

class CredxareaController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatCredxAreaModel();
    }

    public function credxareaAction()
    {

        $this->render();
    }

    public function cargarCredsxAreaAction()
    {
        //obtener todos los créditos asociados a un tipo de carrera
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $credsxarea = $this->model->cargarCredsxArea($limit, $start, $filtros);
        //ver esto en todas las cargas, si realmente hace falta.
        $cant = $this->model->countCredsxArea($filtros);

        $result = array('cantidad' => $cant, 'datos' => $credsxarea);
        echo json_encode($result);
    }

    public function cargarEnfasisAction()
    {
        //obtener todos los énfasis asociados a un tipo de carrera
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');

        $enfasis = $this->model->cargarEnfasis($idcarrera, $limit, $start);

        $result = array('datos' => $enfasis);
        echo json_encode($result);
    }

    public function cargarPensumsAction()
    {
        //obtener todos los énfasis asociados a un tipo de carrera
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');

        $pensums = $this->model->cargarPensums($idcarrera, $limit, $start);

        $result = array('datos' => $pensums);
        echo json_encode($result);
    }

    public function cargarAreasGeneralesAction()
    {
        //obtener todos los énfasis asociados a un tipo de carrera
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $areas_generales = $this->model->cargarAreasGenerales($limit, $start);

        $result = array('datos' => $areas_generales);
        echo json_encode($result);
    }

    public function cargarAreasAction()
    {
        //obtener todos los énfasis asociados a un tipo de carrera
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idareageneral = $this->_request->getPost('idareageneral');

        $areas = $this->model->cargarAreas($idareageneral, $limit, $start);

        $result = array('datos' => $areas);
        echo json_encode($result);
    }

    public function insertarCredsxAreaAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idenfasis = $datos->idenfasis;
        $idpensum = $datos->idpensum;
        $idareageneral = $datos->idareageneral;
        $idarea = $datos->idarea;
        $creditos = $datos->creditos;

        $estado = $datos->estado;

        if ($this->model->insertarCredsxArea($idenfasis, $idpensum, $idareageneral, $idarea, $creditos, $usuario,
            $estado, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarCredsxAreaAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idareacredito = $datos->idareacredito;
        $idenfasis = $datos->idenfasis;
        $idpensum = $datos->idpensum;
        $idareageneral = $datos->idareageneral;
        $idarea = $datos->idarea;
        $creditos = $datos->creditos;

        $estado = $datos->estado;

        if ($this->model->modificarCredsxArea($idareacredito, $idenfasis, $idpensum, $idareageneral, $idarea,
            $creditos, $usuario, $estado, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function eliminarCredsxAreaAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idareacredito = $datos->idareacredito;

        if ($this->model->eliminarCredsxArea($idareacredito))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }
}

?>
