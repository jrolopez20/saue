<?php


class CarreraService {

    /**
     * cargarCarreras
     * Devuelve las carreras asociadas a una facultad (permite paginado)
     *
     * @param int $idfacultad - Identificador de la estructura "Facultad"
     * @param int $limit - Cantidad de carreras a cargar
     * @param int $start - Inicio de la los registros de carreras a cargar
     * @return array - Carreras.
     */
    public function cargarCarreras($idfacultad, $limit, $start){
        $DatCarreraModel = new DatCarreraModel();
        $carreras = $DatCarreraModel->cargarCarreras($idfacultad, $limit, $start);
        return ZendExt_ClassStandard::ArrayObject($carreras);
    }

    /**
     * cargarCarreras
     * Devuelve la cantidad de carreras asociadas a una facultad
     *
     * @param int $idfacultad - Identificador de la estructura "Facultad"
     * @return int $cantidad - Cantidad de carreras asociadas a una Facultad
     */
    public function countCarreras($idfacultad){
        $DatCarreraModel = new DatCarreraModel();
        $cantidad = $DatCarreraModel->countCarreras($idfacultad);
        return $cantidad;
    }
} 