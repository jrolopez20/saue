<?php

class FacultadProxyService
{
    public function loadAllPensum($limit, $start)
    {
        return DatPensum::cargarPensumAll($start, $limit);
    }

    public function countAllPensum()
    {
        return DatPensum::countPernsumAll();
    }

    public function loadPensum($limit, $start, $filtros)
    {
        $DatPensumModel = new DatPensumModel();
        return $DatPensumModel->cargarPensums($limit, $start, $filtros);
    }

    public function countPensum($filtros)
    {
        return DatPensum::countPensum($filtros);
    }

    /**
     * cargarCarreras
     * Devuelve los énfasis asociados a una carrera (permite paginado)
     *
     * @param int $idcarrera - Identificador de la Carrera
     * @param int $limit - Cantidad de énfasis  a cargar
     * @param int $start - Inicio de los registros de Énfasis a cargar
     * @return array - Énfasis.
     */
    public function cargarEnfasis($idcarrera, $limit, $start){
        $DatEnfasisModel = new DatEnfasisModel();
        $enfasis = $DatEnfasisModel->cargarEnfasisService($idcarrera, $limit, $start);
        return ZendExt_ClassStandard::ArrayObject($enfasis);
    }

    /**
     * cargarCarreras
     * Devuelve la cantidad de énfasis registrados
     *
     * @param int $idcarrera - Identificador de la Carrera
     * @return int $cantidad - Cantidad de carreras asociadas a una Facultad
     */
    public function countEnfasis($idcarrera){
        $DatEnfasisModel = new DatEnfasisModel();
        $cantidad = $DatEnfasisModel->countEnfasisService($idcarrera);
        return $cantidad;
    }

    /**
     * cargarCarreras
     * Devuelve las carreras asociadas a una facultad (permite paginado)
     *
     * @param int $limit - Cantidad de carreras a cargar
     * @param int $start - Inicio de la los registros de carreras a cargar
     * @param array $filtros - Filtros a aplicar para la carga de carreras
     * @return array - Carreras.
     */
    public function cargarCarreras($limit, $start, $filtros){
        $DatCarreraModel = new DatCarreraModel();
        $carreras = $DatCarreraModel->cargarCarreras($limit, $start, $filtros);
        return ZendExt_ClassStandard::ArrayObject($carreras);
    }

    /**
     * cargarCarreras
     * Devuelve las carreras asociadas a una facultad (permite paginado)
     *
     * @param int $idfacultad - Identificador de la estructura "Facultad"
     * @param int $limit - Cantidad de carreras a cargar
     * @param int $start - Inicio de la los registros de carreras a cargar
     * @return array - Carreras.

    public function cargarCarreras($idfacultad, $limit, $start){
        $DatCarreraModel = new DatCarreraModel();
        $carreras = $DatCarreraModel->cargarCarreras($idfacultad, $limit, $start);
        return ZendExt_ClassStandard::ArrayObject($carreras);
    }*/
} 