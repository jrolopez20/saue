<?php


class EnfasisService {

    /**
     * cargarCarreras
     * Devuelve los énfasis asociados a una carrera (permite paginado)
     *
     * @param int $idcarrera - Identificador de la Carrera
     * @param int $limit - Cantidad de énfasis  a cargar
     * @param int $start - Inicio de los registros de Énfasis a cargar
     * @return array - Énfasis.
     */
    public function cargarEnfasis($idcarrera, $limit, $start){
        $DatEnfasisModel = new DatEnfasisModel();
        $enfasis = $DatEnfasisModel->cargarEnfasisService($idcarrera, $limit, $start);
        return ZendExt_ClassStandard::ArrayObject($enfasis);
    }

    /**
     * cargarCarreras
     * Devuelve la cantidad de énfasis registrados
     *
     * @param int $idcarrera - Identificador de la Carrera
     * @return int $cantidad - Cantidad de carreras asociadas a una Facultad
     */
    public function countEnfasis($idcarrera){
        $DatEnfasisModel = new DatEnfasisModel();
        $cantidad = $DatEnfasisModel->countEnfasisService($idcarrera);
        return $cantidad;
    }
} 
