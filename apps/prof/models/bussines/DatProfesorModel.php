<?php

class DatProfesorModel extends ZendExt_Model
{
    public function DatProfesorModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarProfesor($limit, $start, $filter)
    {
        return DatProfesor::cargarProfesor($limit, $start, $filter);
    }

    public function countProfesor($filter)
    {
        try {
            return DatProfesor::countProfesor($filter);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function countHistorial($idProfesor)
    {
        try {
            return DatProfesor::countHistorial($idProfesor);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }    

    public function cargarHistorial($limit, $start, $idProfesor)
    {
        try {
            return DatProfesor::cargarHistorial($limit, $start, $idProfesor);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarProfesor($nombre, $apellidos, $fecha_nacimiento, $cedula, $pasaporte, $correo, $instruccion, $especializacion, $sexo, $idestadocivil, $usuario, $fecha, $usuarioAsig, $estado,$domicilio, $tel, $cel)
    {
        try {
            $Profesor = new DatProfesor();
            $Profesor->nombre = $nombre;
            $Profesor->apellidos = $apellidos;
            $Profesor->fecha_nacimiento = $fecha_nacimiento;
            $Profesor->cedula = $cedula;
            $Profesor->pasaporte = $pasaporte;
            $Profesor->correo = $correo;
            $Profesor->instruccion = $instruccion;
            $Profesor->especializacion = $especializacion;
            $Profesor->sexo = $sexo;
            $Profesor->idestadocivil = $idestadocivil;
            $Profesor->idusuario = $usuario;
            $Profesor->fecha = $fecha;
            $Profesor->idusuarioasig = $usuarioAsig;
            $Profesor->estado = $estado;
            $Profesor->domicilio = $domicilio;
            $Profesor->telefono = $tel;
            $Profesor->celular = $cel;
            $Profesor->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarProfesor($idProfesor, $nombre, $apellidos, $fecha_nacimiento, $cedula, $pasaporte, $correo, $instruccion, $especializacion, $sexo, $idestadocivil, $usuario, $fecha,$estado,$domicilio, $tel, $cel)
    {
        try {
            $Profesor = Doctrine::getTable("DatProfesor")->find($idProfesor);
            $Profesor->nombre = $nombre;
            $Profesor->apellidos = $apellidos;
            $Profesor->fecha_nacimiento = $fecha_nacimiento;
            $Profesor->cedula = $cedula;
            $Profesor->pasaporte = $pasaporte;
            $Profesor->correo = $correo;
            $Profesor->instruccion = $instruccion;
            $Profesor->especializacion = $especializacion;
            $Profesor->sexo = $sexo;
            $Profesor->idestadocivil = $idestadocivil;
            $Profesor->idusuario = $usuario;
            $Profesor->fecha = $fecha;
            $Profesor->estado = $estado;
            $Profesor->domicilio = $domicilio;
            $Profesor->telefono = $tel;
            $Profesor->celular = $cel;
            $Profesor->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarProfesor($idProfesor)
    {
        return DatProfesor::eliminarProfesor($idProfesor);
    }

}