<?php

abstract class BaseDatProfesor extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_profesor');
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('celular', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('telefono', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('domicilio', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('tipo_identificacion', 'character', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idusuarioasig', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestadocivil', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('sexo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('especializacion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('instruccion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('correo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('pasaporte', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cedula', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha_nacimiento', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('apellidos', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idprofesor', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_profesor'));
    }


}

