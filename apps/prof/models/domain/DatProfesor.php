<?php

class DatProfesor extends BaseDatProfesor
{

    static public function cargarProfesor($limit, $start, $filter='')
    {
        $query = Doctrine_Query::create();
        if($filter)
            $filterSQL ="AND  (". self::createStringFilter($filter).")";
        $stmt = $query->getConnection()
            ->prepare("SELECT p.idprofesor, p.nombre, p.apellidos, p.fecha_nacimiento,p.cedula, p.pasaporte,p.estado, p.telefono, p.celular, p.domicilio,
       p.correo, p.instruccion, p.especializacion, p.sexo, p.idestadocivil, ec.descripcion as estadocivil,
       p.idusuario, p.fecha  FROM mod_saue.dat_profesor p,mod_seguridad.seg_usuario u, mod_saue.dat_estadocivil ec
       WHERE p.idusuarioasig = u.idusuario AND  p.idestadocivil = ec.idestadocivil
       ". $filterSQL ."  ORDER BY p.apellidos ASC LIMIT " . $limit . " OFFSET " . $start . ";");

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;


    }
    static public function countHistorial($idProfesor)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT(c.idcurso)
                FROM
                    mod_saue.dat_curso c
                    INNER JOIN mod_saue.dat_aula a ON c.idaula=a.idaula
                    INNER JOIN mod_saue.dat_materia m ON c.idmateria=m.idmateria
                    INNER JOIN mod_saue.dat_profesor p ON c.idprofesor=p.idprofesor
                    INNER JOIN mod_saue.dat_horario h ON c.idhorario=h.idhorario
                    INNER JOIN mod_saue.dat_periododocente pr ON c.idperiododocente=pr.idperiododocente
                    WHERE p.idprofesor = ".$idProfesor."";
                    $stmt = $query
                            ->getConnection()
                            ->prepare($sql);
                        $stmt->execute();
                        $result = $stmt->fetch();
                        return $result['count'];

    }    

    static public function cargarHistorial($limit, $start, $idProfesor)
    {
        //cuando este la fecha de inicio y fin de los horarios por periodo devolverlos
        //para entonces diferenciar los activos
        $query = Doctrine_Query::create();

        $sql = "SELECT
                    a.descripcion as local,
                    m.descripcion as materia,
                    h.descripcion as horario,
                    pr.descripcion as periodo
                FROM
                    mod_saue.dat_curso c
                    INNER JOIN mod_saue.dat_aula a ON c.idaula=a.idaula
                    INNER JOIN mod_saue.dat_materia m ON c.idmateria=m.idmateria
                    INNER JOIN mod_saue.dat_profesor p ON c.idprofesor=p.idprofesor
                    INNER JOIN mod_saue.dat_horario h ON c.idhorario=h.idhorario
                    INNER JOIN mod_saue.dat_periododocente pr ON c.idperiododocente=pr.idperiododocente
                    WHERE p.idprofesor = ".$idProfesor."";
                    $stmt = $query
                            ->getConnection()
                            ->prepare($sql);
                        $stmt->execute();
                        $result = $stmt->fetchAll();
                        return $result;
    }

    static public function cargarProfesorService($limit, $start, $filter='')
    {
        $query = Doctrine_Query::create();
        if($filter)
            $filterSQL ="AND  (". self::createStringFilter($filter).")";
        $stmt = $query->getConnection()
            ->prepare("SELECT p.idprofesor, p.nombre, p.apellidos FROM mod_saue.dat_profesor p
                 WHERE p.estado = true  ". $filterSQL ."  LIMIT " . $limit . " OFFSET " . $start . ";");

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;


    }

    static public function countProfesor($filter='')
    {
        $query = Doctrine_Query::create();
        $query->from("DatProfesor p");
      
        if($filter){
             $filterSQL = self::createStringFilter($filter);
             $query->where($filterSQL);
        }
           
            $result = $query->execute();

        return $result->count();

    }
    static public function countProfesorService($filter='')
    {
        $query = Doctrine_Query::create();    
        if($filter)
            $filterSQL ="AND  (". self::createStringFilter($filter).")";
        $result = $query
            ->from("DatProfesor p")
            ->where("p.estado=true ".$filterSQL)
            ->execute();
        return $result->count();

    }

    static public function eliminarProfesor($idprofesor)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatProfesor p")
            ->where("p.idprofesor=?", $idprofesor)
            ->execute();
        return true;
    }

    static public  function createStringFilter($filtros){
        $str = "";
        foreach ($filtros as $key) {
            $str .=" (lower(p.".$key->property.") LIKE '%" . strtolower($key->value) . "%') OR";
        }
       return substr( $str ,0, -2);
             }
    public function setUp()
    {
        parent :: setUp();
       // $this->hasOne('DatEstadocivil', array('local' => 'idestadocivil', 'foreign' => 'idestadocivil'));
       // $this->hasMany('DatCalendarioDocente', array('local' => 'idprofesor', 'foreign' => 'idprofesor'));
       // $this->hasMany('DatPeriododocenteHorario', array('local' => 'idprofesor', 'foreign' => 'idprofesor'));
    }
}
