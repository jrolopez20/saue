<?php

class GestprofesoresController extends ZendExt_Controller_Secure {

    private $model;

    public function init() {
        parent::init();
        $this->model = new DatProfesorModel();
    }

    public function gestprofesoresAction() {
        $this->render();
    }

    public function cargarProfesoresAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));
        $profesores = $this->model->cargarProfesor($limit, $start, $filtros);
        $cant = $this->model->countProfesor($filtros);
        $result = array('cantidad' => $cant, 'datos' => $profesores);
        echo json_encode($result);
    }
    public function cargarHistorialAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idProfesor = $this->_request->getPost('idprofesor');
        $cursos = $this->model->cargarHistorial($limit, $start, $idProfesor);
        $cant = $this->model->countHistorial($idProfesor);
        $result = array('cantidad' => $cant, 'datos' => $cursos);
        echo json_encode($result);
    }
    public function insertarProfesorAction() {
        $usuario = $this->global->Perfil->idusuario;
        $nombre = $this->_request->getPost('nombre');
        $apellidos = $this->_request->getPost('apellidos');
        $sexo = $this->_request->getPost('sexo');
        $idestadocivil = $this->_request->getPost('idestadocivil');
        $especializacion = $this->_request->getPost('especializacion');
        $instruccion = $this->_request->getPost('instruccion');
        $correo = $this->_request->getPost('correo');
        $ident = $this->_request->getPost('rb');
        if($ident == "pasaporte"){
            $pasaporte = $this->_request->getPost('cedpas');
        } else{
            $cedula = $this->_request->getPost('cedpas');
        }
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $fecha_nacimiento = $this->_request->getPost('fecha_nacimiento');
        $domicilio = $this->_request->getPost('domicilio');
        $tel = $this->_request->getPost('telefono');
        $cel = $this->_request->getPost('celular');

        $identidad  = $this->global->Perfil->idestructuracomun;
        $idusuarioasig = $this->integrator->seguridad->insertarUsuario($nombre, $apellidos, $identidad, 10, 220000000023);

        $this->model->insertarProfesor($nombre, $apellidos, $fecha_nacimiento, $cedula, $pasaporte, $correo, $instruccion, $especializacion, $sexo, $idestadocivil, $usuario, $fecha, $idusuarioasig, $estado, $domicilio, $tel, $cel);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarProfesorAction() {
        $usuario = $this->global->Perfil->idusuario;
        $nombre = $this->_request->getPost('nombre');
        $apellidos = $this->_request->getPost('apellidos');
        $sexo = $this->_request->getPost('sexo');
        $idestadocivil = $this->_request->getPost('idestadocivil');
        $especializacion = $this->_request->getPost('especializacion');
        $instruccion = $this->_request->getPost('instruccion');
        $correo = $this->_request->getPost('correo');
                $ident = $this->_request->getPost('rb');
        if($ident == "pasaporte"){
            $pasaporte = $this->_request->getPost('cedpas');
        } else{
            $cedula = $this->_request->getPost('cedpas');
        }
        $fecha_nacimiento = $this->_request->getPost('fecha_nacimiento');
        $estado = $this->_request->getPost('estado');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $fecha = date('Y-m-d H:i');
        $idProfesor = $this->_request->getPost('idprofesor');
        $domicilio = $this->_request->getPost('domicilio');
        $tel = $this->_request->getPost('telefono');
        $cel = $this->_request->getPost('celular');
        $this->model->modificarProfesor($idProfesor, $nombre, $apellidos, $fecha_nacimiento, $cedula, $pasaporte, $correo, $instruccion, $especializacion, $sexo, $idestadocivil, $usuario, $fecha, $estado, $domicilio, $tel, $cel);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function cargarEstadoCivilAction() {
        $var = $this->integrator->nomencladores->cargarEstadosCivil();
        $count = $this->integrator->nomencladores->contarEstadosCivil();

        $result = array('cantidad' => $count, 'datos' => $var);

        echo json_encode($result);
    }

    public function eliminarProfesorAction() {
        $ArrayProfDel = json_decode(stripslashes($this->_request->getPost('ArrayProfDel')));
        $idProfesor = $this->_request->getPost('idProfesor');
        foreach ($ArrayProfDel as $idProfesor) {
            $this->model->eliminarProfesor($idProfesor);
        }
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>