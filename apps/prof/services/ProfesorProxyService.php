<?php

class ProfesorProxyService
{
    public function loadProfesores($limit, $start, $filters)
    {
        return DatProfesor::cargarProfesorService($limit, $start, $filters);
    }

    public function countProfesores($filters)
    {
        return DatProfesor::countProfesorService($filters);
    }
} 