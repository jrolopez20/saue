<?php

class PersonaProxyService
{
    public function getPersonas($limit, $start, $filter)
    {
        return DatPersona::getPersonas($limit, $start, $filter);
    }

    public function countPersona($filter)
    {
        return DatPersona::countPersona($filter);
    }
} 