<?php

class NomDepartamento extends BaseNomDepartamento
{

    public function setUp()
    {
        parent :: setUp ();
    }

    public function listDepartamento($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('iddepartamento, denominacion, desde, hasta')
            ->from('NomDepartamento')
            ->orderBy('denominacion ASC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchArray();
    }

    public function countDepartamento($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(iddepartamento) as cantidad')
            ->from('NomDepartamento');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }
    
}