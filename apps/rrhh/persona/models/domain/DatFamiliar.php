<?php

class DatFamiliar extends BaseDatFamiliar
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('DatPersona', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne('NomRelacionfamiliar', array('local' => 'idrelacionfamiliar', 'foreign' => 'idrelacionfamiliar'));
    }

    public function listFamiliares($idpersona)
    {
        $query = Doctrine_Query::create()
            ->select('f.idfamiliar, f.idpersona, f.codigo, f.nombre, f.sexo, f.fechanacimiento, f.estudia,
                f.idrelacionfamiliar, rf.denominacion as relacionfamiliar')
            ->from('DatFamiliar f')
            ->innerJoin('f.NomRelacionfamiliar rf')
            ->where('f.idpersona = ?', $idpersona)
            ->orderBy('f.nombre ASC');

        return $query->fetchArray();
    }

    public function countFamiliares($idpersona)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idfamiliar) as cantidad')
            ->from('DatFamiliar')
            ->where('idpersona = ?', $idpersona);

        return $query->fetchOne()->cantidad;
    }
}