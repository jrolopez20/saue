<?php

class NomTipocontacto extends BaseNomTipocontacto
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatContacto', array('local' => 'idtipocontacto', 'foreign' => 'idtipocontacto'));
    }

    public function listTipoContacto($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idtipocontacto, denominacion, desde, hasta')
            ->from('NomTipocontacto')
            ->orderBy('denominacion ASC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchArray();
    }

    public function countTipoContacto($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idtipocontacto) as cantidad')
            ->from('NomTipocontacto');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }

}