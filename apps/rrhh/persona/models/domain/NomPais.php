<?php

class NomPais extends BaseNomPais
{

    public function setUp()
    {
        parent:: setUp();
    }

    static public function getAll()
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->select('idpais, nombrepais, codigopais, siglas, nacionalidad')
                ->from('NomPais')->orderBy('nombrepais')->execute();
            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

}