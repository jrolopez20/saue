<?php

class DatPersona extends BaseDatPersona
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatFamiliar', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne('DatNacionalidad', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasMany('DatContacto', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasMany('DatDireccion', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne('DatNacimiento', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne('NomEstadocivil', array('local' => 'idestadocivil', 'foreign' => 'idestadocivil'));
        $this->hasMany('DatDocidentidad', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne('DatOcupacion', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasMany('DatPrestamo', array('local' => 'idpersona', 'foreign' => 'idpersona'));
    }

    public function listPersona($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('p.idpersona, p.nombre, p.apellidos, p.sexo, p.foto, p.idestadocivil, p.tipopersona,
                ec.denominacion as estadocivil, doc.iddocidentidad, doc.tipo as tipodocidentidad, doc.codigo as numid,
                nac.idnacimiento, nac.fechanacimiento, nac.lugar as lugarnacimiento, ncd.idnacionalidad,
                pais.nacionalidad')
            ->from('DatPersona p')
            ->innerJoin('p.DatDocidentidad doc WITH doc.principal = 1')
            ->leftJoin('p.DatNacimiento nac')
            ->leftJoin('p.NomEstadocivil ec')
            ->leftJoin('p.DatNacionalidad ncd')
            ->leftJoin('ncd.NomPais pais')
            ->orderBy('p.nombre ASC, p.apellidos ASC')
            ->where('p.fechaeliminado IS NULL');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $query->addWhere("p.nombre ilike '%{$filters[0]->value}%'
                OR p.apellidos ilike '%{$filters[0]->value}%'
                OR doc.codigo ilike '%{$filters[0]->value}%'");
        }

        return $query->fetchArray();
    }

    static public function countPersona($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(p.idpersona) as cantidad')
            ->from('DatPersona p')
            ->innerJoin('p.DatDocidentidad doc WITH doc.principal = 1')
            ->where('p.fechaeliminado IS NULL');

        if (!empty($filters)) {
            if (is_array($filters)) {
                $query->addWhere("p.nombre ilike '%{$filters[0]->value}%'
                OR p.apellidos ilike '%{$filters[0]->value}%'
                OR doc.codigo ilike '%{$filters[0]->value}%'");
            } else {
                $query->addWhere("doc.codigo = '%{$filters}%' OR p.nombre ilike '%{$filters}%' OR p.apellidos ilike '%{$filters}%'");
            }
        }


        return $query->fetchOne()->cantidad;
    }

    static public function getPersonas($limit = 25, $start = 0, $filter)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $sql = "SELECT p.idpersona, p.nombre, p.apellidos, p.sexo, p.foto, p.idestadocivil, p.tipopersona, "
            . "doc.iddocidentidad, doc.tipo as tipodocidentidad, doc.codigo as numid, "
            . "(p.nombre || ' ' || p.apellidos) as fullname "
            . "FROM mod_persona.dat_persona p "
            . "INNER JOIN mod_persona.dat_docidentidad doc ON(doc.idpersona = p.idpersona AND doc.principal = 1) ";

        if (!empty($filter)) {
            $sql .= " WHERE doc . codigo = '%{$filter}%' OR p . nombre ilike '%{$filter}%' OR p . apellidos ilike '%{$filter}%'";
        }

        if ($limit) {
            $sql .= ' LIMIT '.$limit;
        }

        if ($start) {
            $sql .= ' OFFSET '.$start;
        }

        return $connection->fetchAll($sql);
    }

}