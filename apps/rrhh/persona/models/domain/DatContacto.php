<?php

class DatContacto extends BaseDatContacto
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('DatPersona', array('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne('NomTipocontacto', array('local' => 'idtipocontacto', 'foreign' => 'idtipocontacto'));
        $this->hasOne('NomMediocontacto', array('local' => 'idmediocontacto', 'foreign' => 'idmediocontacto'));
    }

    public function listContactos($idpersona, $limit = null, $start = null)
    {
        $query = Doctrine_Query::create()
            ->select('c.idcontacto, c.idpersona, c.contacto, c.idmediocontacto, c.idtipocontacto,
                tc.denominacion as tipocontacto, mc.denominacion as mediocontacto')
            ->from('DatContacto c')
            ->leftJoin('c.NomTipocontacto tc')
            ->leftJoin('c.NomMediocontacto mc')
            ->where('idpersona = ?', $idpersona);

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        return $query->fetchArray();
    }

}