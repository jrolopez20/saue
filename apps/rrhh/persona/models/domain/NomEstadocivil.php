<?php

class NomEstadocivil extends BaseNomEstadocivil
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasMany ('DatPersona', array ('local' => 'idestadocivil', 'foreign' => 'idestadocivil'));
    }

    public function listEstadoCivil($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idestadocivil, denominacion, desde, hasta')
            ->from('NomEstadocivil')
            ->orderBy('denominacion ASC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchArray();
    }

    public function countEstadoCivil($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idestadocivil) as cantidad')
            ->from('NomEstadocivil');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }
}