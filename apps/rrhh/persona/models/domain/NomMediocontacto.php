<?php

class NomMediocontacto extends BaseNomMediocontacto
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatContacto', array('local' => 'idmediocontacto', 'foreign' => 'idmediocontacto'));
    }

    public function listMedioContacto($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idmediocontacto, denominacion, numerico, desde, hasta')
            ->from('NomMediocontacto')
            ->orderBy('denominacion ASC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchArray();
    }

    public function countMedioContacto($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idmediocontacto) as cantidad')
            ->from('NomMediocontacto');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }

}