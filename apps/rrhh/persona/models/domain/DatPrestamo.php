<?php

class DatPrestamo extends BaseDatPrestamo
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatPersona', array ('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasMany ('DatPrestamodividendo', array ('local' => 'idprestamo', 'foreign' => 'idprestamo'));
    }

    public function listPrestamos($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('')
            ->from('DatPrestamo ptm')
            ->innerJoin('ptm.DatPersona p')
            ->orderBy('ptm.fecha DESC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        return $query->fetchArray();
    }

    public function countPrestamos($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idprestamo) as cantidad')
            ->from('DatPrestamo');

        return $query->fetchOne()->cantidad;
    }

}