<?php

abstract class BaseNomCargo extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.nom_cargo');
        $this->hasColumn('idcargo', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_cargo'));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => true,'primary' => false));
    }

}