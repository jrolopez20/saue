<?php

abstract class BaseDatPersona extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_persona');
        $this->hasColumn('idpersona', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_persona'));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('apellidos', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('sexo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fechaeliminado', 'timestamp without time zone', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('foto', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecharegistro', 'timestamp without time zone', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestadocivil', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('tipopersona', 'numeric', null, array ('notnull' => false,'primary' => false));
    }


}

