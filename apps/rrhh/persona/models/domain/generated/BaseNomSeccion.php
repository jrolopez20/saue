<?php

abstract class BaseNomSeccion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.nom_seccion');
        $this->hasColumn('idseccion', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_seccion'));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => true,'primary' => false));
    }

}