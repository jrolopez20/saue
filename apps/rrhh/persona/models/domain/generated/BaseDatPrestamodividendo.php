<?php

abstract class BaseDatPrestamodividendo extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_prestamodividendo');
        $this->hasColumn('idprestamodividendo', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_prestamodividendo'));
        $this->hasColumn('fechavencimiento', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('valor', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idprestamo', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}