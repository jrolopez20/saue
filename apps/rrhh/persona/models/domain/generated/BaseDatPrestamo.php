<?php

abstract class BaseDatPrestamo extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_prestamo');
        $this->hasColumn('idprestamo', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_prestamo'));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('monto', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('numero', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}