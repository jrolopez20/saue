<?php

abstract class BaseDatDireccion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_direccion');
        $this->hasColumn('iddireccion', 'numeric', null, array ('notnull' => false,'primary' => true, 'default' => ''));
        $this->hasColumn('direccion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => true,'primary' => false));
    }


}

