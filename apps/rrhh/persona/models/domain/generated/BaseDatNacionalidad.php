<?php

abstract class BaseDatNacionalidad extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_nacionalidad');
        $this->hasColumn('idnacionalidad', 'numeric', null, array ('notnull' => true,'primary' => true));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => true,'primary' => true));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}