<?php

abstract class BaseDatOcupacion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_ocupacion');
        $this->hasColumn('idocupacion', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_ocupacion'));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('nocontrato', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('noafiliacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('iddepartamento', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idseccion', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcargo', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('actividad', 'character varying', null, array ('notnull' => true,'primary' => false));
    }

}