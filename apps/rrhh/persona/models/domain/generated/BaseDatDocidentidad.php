<?php

abstract class BaseDatDocidentidad extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_docidentidad');
        $this->hasColumn('iddocidentidad', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_docidentidad'));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('tipo', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('principal', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}