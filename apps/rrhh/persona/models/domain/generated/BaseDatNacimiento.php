<?php

abstract class BaseDatNacimiento extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_nacimiento');
        $this->hasColumn('idnacimiento', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_nacimiento'));
        $this->hasColumn('fechanacimiento', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('lugar', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}