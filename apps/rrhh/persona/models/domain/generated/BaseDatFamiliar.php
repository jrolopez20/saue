<?php

abstract class BaseDatFamiliar extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_familiar');
        $this->hasColumn('idfamiliar', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_familiar'));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idrelacionfamiliar', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('sexo', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fechanacimiento', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('estudia', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}