<?php

abstract class BaseNomTipocontacto extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.nom_tipocontacto');
        $this->hasColumn('idtipocontacto', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_tipocontacto'));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('desde', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('hasta', 'date', null, array('notnull' => true, 'primary' => false));
    }
}