<?php

abstract class BaseNomRelacionfamiliar extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.nom_relacionfamiliar');
        $this->hasColumn('idrelacionfamiliar', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_relacionfamiliar'));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => true));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => true,'primary' => false));
    }

}