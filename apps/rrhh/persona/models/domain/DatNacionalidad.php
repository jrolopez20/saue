<?php

class DatNacionalidad extends BaseDatNacionalidad
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatPersona', array ('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne ('NomPais', array ('local' => 'idnacionalidad', 'foreign' => 'idpais'));
    }

}