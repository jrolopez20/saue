<?php

class NomCargo extends BaseNomCargo
{

    public function setUp()
    {
        parent :: setUp ();
    }

    public function listCargo($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idcargo, denominacion, desde, hasta')
            ->from('NomCargo')
            ->orderBy('denominacion ASC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchArray();
    }

    public function countCargo($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idcargo) as cantidad')
            ->from('NomCargo');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }
}