<?php

class NomRelacionfamiliar extends BaseNomRelacionfamiliar
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasMany ('DatFamiliar', array ('local' => 'idrelacionfamiliar', 'foreign' => 'idrelacionfamiliar'));
    }

    public function listRelacionFamiliar($limit = 25, $start = 0, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idrelacionfamiliar, denominacion, desde, hasta')
            ->from('NomRelacionfamiliar')
            ->orderBy('denominacion ASC');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchArray();
    }

    public function countRelacionFamiliar($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idrelacionfamiliar) as cantidad')
            ->from('NomRelacionfamiliar');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }

}