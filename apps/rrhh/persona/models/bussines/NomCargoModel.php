<?php

class NomCargoModel extends ZendExt_Model
{
    public function NomCargoModel()
    {
        parent::ZendExt_Model();
    }

    public function addCargo($params)
    {
        $objCargo = new NomCargo();
        foreach ($params as $k => $value) {
            if ($k != 'idcargo') {
                $objCargo->$k = $value;
            }
        }
        $objCargo->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idcargo':'" . $objCargo->idcargo . "'}";
    }

    public function deleteCargo($idcargo)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomCargo')
                ->where('idcargo = ?', $idcargo)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Cargo en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateCargo($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('NomCargo');

            foreach ($params as $k => $value) {
                if ($k != 'idcargo') {
                    $query->set($k, ($value ? "'$value'" : 'null'));
                }
            }
            $query->where('idcargo = ?', $params->idcargo);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actualizado', 'idcargo':'" . $params->idcargo . "'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }
}