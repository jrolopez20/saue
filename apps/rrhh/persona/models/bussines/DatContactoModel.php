<?php

class DatContactoModel extends ZendExt_Model
{

    public function DatContactoModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Adiciona un nuevo contacto
     * @param $idpersona
     * @param $idmediocontacto
     * @param $idtipocontacto
     * @param $contacto
     */
    public function adicionarContacto($idpersona, $idmediocontacto, $idtipocontacto, $contacto)
    {
        try {
            $objContacto = new DatContacto();
            $objContacto->idpersona = $idpersona;
            $objContacto->idmediocontacto = $idmediocontacto;
            $objContacto->idtipocontacto = $idtipocontacto;
            $objContacto->contacto = $contacto;
            $objContacto->save();
            return "{'success':true, 'codMsg':1, 'mensaje':'Contacto adicionado satisfactoriamente'}";
        } catch (Doctrine_Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    /**
     * Modifica un contacto
     * @param $idcontacto
     * @param $idpersona
     * @param $idmediocontacto
     * @param $idtipocontacto
     * @param $contacto
     */
    public function modificarContacto($idcontacto, $idpersona, $idmediocontacto, $idtipocontacto, $contacto)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('DatContacto');
            $query->set('idpersona', $idpersona);
            $query->set('idmediocontacto', $idmediocontacto);
            $query->set('idtipocontacto', $idtipocontacto);
            $query->set('contacto', '?', $contacto);
            $query->where('idcontacto = ?', $idcontacto);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Contacto modificado satisfactoriamente'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    /**
     * Elimina un contacto
     * @param $idcontacto
     * @return string Mensaje de respuesta
     */
    public function eliminarContacto($idcontacto)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('DatContacto')
                ->where('idcontacto = ?', $idcontacto)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Contacto eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'El contacto se encuentra en uso'}";
            } else {
                return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
            }
        }
    }

}