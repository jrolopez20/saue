<?php

class DatPersonaModel extends ZendExt_Model
{

    public function DatPersonaModel()
    {
        parent::ZendExt_Model();
    }

    public function savePersona($datosBasicos, $datosGenerales = null)
    {
        try {
            $idpersona = null;
            if (empty($datosBasicos->idpersona)) {
                $idpersona = $this->adicionarPersona($datosBasicos);
            } else {
                $idpersona = $this->modificarPersona($datosBasicos, $datosGenerales);
            }
            return "{'success':true, 'codMsg':1,'mensaje': 'Datos guardados satisfactoriamente', 'idpersona':{$idpersona}}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function adicionarPersona($datosBasicos)
    {
        $objPersona = new DatPersona();
        $objPersona->nombre = $datosBasicos->nombre;
        $objPersona->apellidos = $datosBasicos->apellidos;
        $objPersona->sexo = $datosBasicos->sexo;
        $objPersona->tipopersona = $datosBasicos->tipopersona;
        if (!empty($datosBasicos->foto)) {
            $objPersona->foto = $datosBasicos->foto;
        }
        $objPersona->idestadocivil = $datosBasicos->idestadocivil;
        $objPersona->idestructura = $this->global->Estructura->idestructura;

        $objPersona->DatDocidentidad[0]->tipo = $datosBasicos->tipodocidentidad;
        $objPersona->DatDocidentidad[0]->codigo = $datosBasicos->numid;
        $objPersona->DatDocidentidad[0]->principal = 1;

        $objPersona->DatNacionalidad[0]->idnacionalidad = $datosBasicos->idnacionalidad;
//        $objPersona->tipo->lugar = $datosBasicos->lugarnacimiento;

        $objPersona->save();
        return $objPersona->idpersona;
    }

    public function modificarPersona($datosBasicos, $datosGenerales)
    {
        try {
            //Datos basicos
            $query = Doctrine_Query::create()
                ->update('DatPersona');
            $query->set('nombre', "'$datosBasicos->nombre'");
            $query->set('apellidos', "'$datosBasicos->apellidos'");
            $query->set('sexo', $datosBasicos->sexo);
            $query->set('tipopersona', $datosBasicos->tipopersona);
            $query->set('foto', "'$datosBasicos->foto'");
            $query->set('idestadocivil', $datosBasicos->idestadocivil);

            $query->where('idpersona = ?', $datosBasicos->idpersona);
            $query->execute();

            //Nacionalidad
            $query = Doctrine_Query::create()
                ->update('DatNacionalidad');
            $query->set('idnacionalidad', $datosBasicos->idnacionalidad);
            $query->where('idpersona = ? AND idnacionalidad <> ?', array($datosBasicos->idpersona, $datosBasicos->idnacionalidad));
            $query->execute();

            //Datos de nacimiento
//        $objNacimiento = new DatNacimiento();
//        if(!empty($datosGenerales->idnacimiento)){
//            $objNacimiento = $objNacimiento->findById($datosGenerales->idnacimiento);
//        }
//        $objNacimiento->fechanacimiento = $datosGenerales->fechanacimiento;
//        $objNacimiento->lugar = $datosGenerales->lugar;
//        $objNacimiento->idpersona = $datosBasicos->idpersona;
//        $objNacimiento->save();
            return $datosBasicos->idpersona;
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

    public function eliminarPersona($idpersona)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('DatPersona');

            $query->set('fechaeliminado', 'now()');

            $query->where('idpersona = ?', $idpersona);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Persona eliminada'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}