<?php

class NomEstadocivilModel extends ZendExt_Model
{
    public function NomEstadocivilModel()
    {
        parent::ZendExt_Model();
    }

    public function addEstadoCivil($params)
    {
        $objEstadoCivil = new NomEstadocivil();
        foreach ($params as $k => $value) {
            if ($k != 'idestadocivil') {
                $objEstadoCivil->$k = $value;
            }
        }
        $objEstadoCivil->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idestadocivil':'" . $objEstadoCivil->idestadocivil . "'}";
    }

    public function deleteEstadoCivil($idestadocivil)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomEstadocivil')
                ->where('idestadocivil = ?', $idestadocivil)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Estado civil en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateEstadoCivil($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('NomEstadocivil');

            foreach ($params as $k => $value) {
                if ($k != 'idestadocivil') {
                    $query->set($k, ($value ? "'$value'" : 'null'));
                }
            }
            $query->where('idestadocivil = ?', $params->idestadocivil);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actualizado'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}