<?php

class NomRelacionfamiliarModel extends ZendExt_Model
{
    public function NomRelacionfamiliarModel()
    {
        parent::ZendExt_Model();
    }

    public function addRelacionFamiliar($params)
    {
        $objRelacionFamiliar = new NomRelacionfamiliar();
        foreach ($params as $k => $value) {
            if ($k != 'idrelacionfamiliar') {
                $objRelacionFamiliar->$k = $value;
            }
        }
        $objRelacionFamiliar->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idrelacionfamiliar':'" . $objRelacionFamiliar->idrelacionfamiliar . "'}";
    }

    public function deleteRelacionFamiliar($idrelacionfamiliar)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomRelacionfamiliar')
                ->where('idrelacionfamiliar = ?', $idrelacionfamiliar)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Relacion familiar en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateRelacionFamiliar($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('NomRelacionfamiliar');

            foreach ($params as $k => $value) {
                if ($k != 'idrelacionfamiliar') {
                    $query->set($k, ($value ? "'$value'" : 'null'));
                }
            }
            $query->where('idrelacionfamiliar = ?', $params->idrelacionfamiliar);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actulizado'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}