<?php

class NomTipocontactoModel extends ZendExt_Model
{

    public function NomTipocontactoModel()
    {
        parent::ZendExt_Model();
    }

    public function addTipoContacto($params)
    {
        $objTipoContacto = new NomTipocontacto();
        foreach ($params as $k => $value) {
            if ($k != 'idtipocontacto') {
                $objTipoContacto->$k = $value;
            }
        }
        $objTipoContacto->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idtipocontacto':'" . $objTipoContacto->idtipocontacto . "'}";
    }

    public function deleteTipoContacto($idtipocontacto)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomTipocontacto')
                ->where('idtipocontacto = ?', $idtipocontacto)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Tipo de contacto en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateTipoContacto($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('NomTipocontacto');

            foreach ($params as $k => $value) {
                if ($k != 'idtipocontacto') {
                    $query->set($k, ($value ? "'$value'" : 'null'));
                }
            }
            $query->where('idtipocontacto = ?', $params->idtipocontacto);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actulizado'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}