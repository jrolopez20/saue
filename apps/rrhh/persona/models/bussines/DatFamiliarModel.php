<?php

class DatFamiliarModel extends ZendExt_Model
{
    public function DatFamiliarModel()
    {
        parent::ZendExt_Model();
    }

    public function addFamiliar($params)
    {
        $objFamiliar = new DatFamiliar();
        $objFamiliar->idpersona = $params->idpersona;
        $objFamiliar->codigo = $params->codigo;
        $objFamiliar->nombre = $params->nombre;
        $objFamiliar->sexo = $params->sexo;
        $objFamiliar->idrelacionfamiliar = $params->idrelacionfamiliar;
        $objFamiliar->fechanacimiento = $params->fechanacimiento;
        $objFamiliar->estudia = $params->estudia;
        $objFamiliar->save();

        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idfamiliar':'" . $objFamiliar->idfamiliar . "'}";
    }

    public function deleteFamiliar($idfamiliar)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('DatFamiliar')
                ->where('idfamiliar = ?', $idfamiliar)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Carga familiar en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateFamiliar($params)
    {
        try {
            $objFamiliar = new DatFamiliar();
            $query = Doctrine_Query::create()
                ->update('DatFamiliar');

            $flag = false;
            foreach ($params as $k => $value) {
                if ($k != 'idfamiliar' && $objFamiliar->contains($k)) {
                    $query->set($k, ($value != '' ? "'$value'" : 'null'));
                    $flag = true;
                }
            }
            if ($flag) {
                $query->where('idfamiliar = ?', $params->idfamiliar);
                $query->execute();
            }
            return "{'success':true, 'codMsg':1, 'mensaje':'Actulizado', 'idfamiliar':'" . $params->idfamiliar . "'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}