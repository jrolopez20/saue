<?php

class NomDepartamentoModel extends ZendExt_Model
{
    public function NomDepartamentoModel()
    {
        parent::ZendExt_Model();
    }

    public function addDepartamento($params)
    {
        $objDepartamento = new NomDepartamento();
        foreach ($params as $k => $value) {
            if ($k != 'iddepartamento') {
                $objDepartamento->$k = $value;
            }
        }
        $objDepartamento->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'iddepartamento':'" . $objDepartamento->iddepartamento . "'}";
    }

    public function deleteDepartamento($iddepartamento)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomDepartamento')
                ->where('iddepartamento = ?', $iddepartamento)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Departamento en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateDepartamento($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('NomDepartamento');

            foreach ($params as $k => $value) {
                if ($k != 'iddepartamento') {
                    $query->set($k, ($value ? "'$value'" : 'null'));
                }
            }
            $query->where('iddepartamento = ?', $params->iddepartamento);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actualizado', 'iddepartamento':'" . $params->iddepartamento . "'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }
}