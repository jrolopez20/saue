<?php

class NomMediocontactoModel extends ZendExt_Model
{
    public function NomMediocontactoModel()
    {
        parent::ZendExt_Model();
    }

    public function addMedioContacto($params)
    {
        $objMedioContacto = new NomMediocontacto();
        foreach ($params as $k => $value) {
            if ($k != 'idmediocontacto') {
                $objMedioContacto->$k = $value;
            }
        }
        $objMedioContacto->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idmediocontacto':'" . $objMedioContacto->idmediocontacto . "'}";
    }

    public function deleteMedioContacto($idmediocontacto)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomMediocontacto')
                ->where('idmediocontacto = ?', $idmediocontacto)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Medio de contacto en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateMedioContacto($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('NomMediocontacto');

            foreach ($params as $k => $value) {
                if ($k != 'idmediocontacto') {
                    $query->set($k, ($value ? "'$value'" : 'null'));
                }
            }
            $query->where('idmediocontacto = ?', $params->idmediocontacto);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actulizado'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}

