<?php

/**
 * Clase controladora para gestionar los departamentos.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DepartamentoController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new NomDepartamentoModel();
        parent::init();
    }

    public function readAction()
    {
        $limit = $this->_request->get('limit');
        $start = $this->_request->get('start');
        $filters = json_decode($this->_request->get('filtros'));

        $objDomain = new NomDepartamento();

        $r['data'] = $objDomain->listDepartamento($limit, $start, $filters);
        $r['total'] = $objDomain->countDepartamento($filters);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addDepartamento($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateDepartamento($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->deleteDepartamento($request->data->iddepartamento);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}