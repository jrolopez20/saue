<?php

/**
 * Clase controladora para gestionar la relacion entre dos personas que son familia.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class CargafamiliarController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new DatFamiliarModel();
        parent::init();
    }

    public function cargafamiliarAction()
    {
        $this->render();
    }

    public function readAction()
    {
        $idpersona = $this->_request->get('idpersona');
        $objDomain = new DatFamiliar();

        $r['data'] = $objDomain->listFamiliares($idpersona);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addFamiliar($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateFamiliar($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->deleteFamiliar($request->data->idfamiliar);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}