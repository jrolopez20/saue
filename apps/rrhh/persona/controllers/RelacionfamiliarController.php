<?php

/**
 * Clase controladora para gestionar la relacion entre dos personas que son familia.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class RelacionfamiliarController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new NomRelacionfamiliarModel();
        parent::init();
    }

    public function readAction()
    {
        $limit = $this->_request->get('limit');
        $start = $this->_request->get('start');
        $filters = json_decode($this->_request->get('filtros'));

        $objDomain = new NomRelacionfamiliar();

        $r['data'] = $objDomain->listRelacionFamiliar($limit, $start, $filters);
        $r['total'] = $objDomain->countRelacionFamiliar($filters);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addRelacionFamiliar($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateRelacionFamiliar($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->deleteRelacionFamiliar($request->data->idrelacionfamiliar);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}