<?php

/**
 * Clase controladora para los tipo de contacto de la persona.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TipocontactoController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new NomTipocontactoModel();
        parent::init();
    }

    public function readAction()
    {
        $limit = $this->_request->get('limit');
        $start = $this->_request->get('start');
        $filters = json_decode($this->_request->get('filtros'));

        $objDomain = new NomTipocontacto();

        $r['data'] = $objDomain->listTipoContacto($limit, $start, $filters);
        $r['total'] = $objDomain->countTipoContacto($filters);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addTipoContacto($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateTipoContacto($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->deleteTipoContacto($request->data->idtipocontacto);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}