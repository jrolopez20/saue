<?php

/**
 * Clase controladora para los medios de contacto de la persona.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class MediocontactoController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new NomMediocontactoModel();
        parent::init();
    }

    public function readAction()
    {
        $limit = $this->_request->get('limit');
        $start = $this->_request->get('start');
        $filters = json_decode($this->_request->get('filtros'));

        $objDomain = new NomMediocontacto();

        $r['data'] = $objDomain->listMedioContacto($limit, $start, $filters);
        $r['total'] = $objDomain->countMedioContacto($filters);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addMedioContacto($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateMedioContacto($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->deleteMedioContacto($request->data->idmediocontacto);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}