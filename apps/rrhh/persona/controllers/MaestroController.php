<?php

/**
 * Clase controladora para los nomencladores y configuraciones de persona.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class MaestroController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
    }

    public function maestroAction()
    {
        $this->render();
    }

}