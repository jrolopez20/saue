<?php

/**
 * Clase controladora para gestionar las personas
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class InventariopersonaController extends ZendExt_Controller_Secure
{

    private $model;
    private $domain;

    public function init()
    {
        $this->domain = new DatPersona();
        $this->model = new DatPersonaModel();
        parent::init();
    }

    public function inventariopersonaAction()
    {
        $this->render();
    }

    public function getPersonasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filters = json_decode($this->_request->getPost('filtros'));

        $r['data'] = $this->domain->listPersona($limit, $start, $filters);
        $r['total'] = $this->domain->countPersona($filters);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function savePersonaAction()
    {
        $datosBasicos = json_decode($this->_request->getPost('datosBasicos'));
        $datosGenerales = json_decode($this->_request->getPost('datosGenerales'));
        $datosBasicos->foto = $this->_request->getPost('foto');

        $r = $this->model->savePersona($datosBasicos, $datosGenerales);

        echo $r;
    }

    public function eliminarPersonaAction()
    {
        $idpersona = $this->_request->getPost('idpersona');

        $r = $this->model->eliminarPersona($idpersona);

        echo $r;
    }

    public function getDatosContactoAction()
    {
        $idpersona = $this->_request->getPost('idpersona');
        $domainContacto = new DatContacto();
        $r['data'] = $domainContacto->listContactos($idpersona);
        echo json_encode($r);
    }

    public function saveContactoAction()
    {
        $idcontacto = $this->_request->getPost('idcontacto');
        $idpersona = $this->_request->getPost('idpersona');
        $idmediocontacto = $this->_request->getPost('idmediocontacto');
        $idtipocontacto = $this->_request->getPost('idtipocontacto');
        $contacto = $this->_request->getPost('contacto');

        $result = '';
        $modelContacto = new DatContactoModel();
        if (empty($idcontacto)) {
            $result = $modelContacto->adicionarContacto($idpersona, $idmediocontacto, $idtipocontacto, $contacto);
        } else {
            $result = $modelContacto->modificarContacto($idcontacto, $idpersona, $idmediocontacto, $idtipocontacto, $contacto);
        }
        echo $result;
    }

    public function eliminarContactoAction()
    {
        $idcontacto = $this->_request->getPost('idcontacto');
        $modelContacto = new DatContactoModel();
        $result = $modelContacto->eliminarContacto($idcontacto);
        echo $result;
    }

    public function getNacionalidadesAction(){
        $nomPais = new NomPais();
        $r['data'] = $nomPais->getAll();
        $r['success'] = true;
        echo json_encode($r);
    }

}