<?php

/**
 * Clase controladora para los prestamos a los emplados.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class PrestamoController extends ZendExt_Controller_Secure
{

    private $model;
    private $domain;

    public function init()
    {
        $this->model = new DatPrestamoModel();
        $this->domain = new DatPrestamo();
        parent::init();
    }

    public function prestamoAction()
    {
        $this->render();
    }

    public function getPrestamosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $r['data'] = $this->domain->listPrestamos($limit, $start);
        $r['total'] = $this->domain->countPrestamos();
        $r['success'] = true;
        echo json_encode($r);
    }

}