<?php

/**
 * Clase controladora para gestionar loas acceso al comedor segun los eventos.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class AccesocomedorController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new DatAccesocomedorModel();
        parent::init();
    }

    public function accesocomedorAction()
    {
        $this->render();
    }

    public function readAction()
    {
        $idpersona = $this->_request->getPost('idpersona');
        $obj = new DatAccesocomedor();
        $r['data'] = $obj->getAccesosComedor($idpersona);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addAcceso($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateAcceso($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->destroyAcceso($request->data->idaccesocomedor);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}