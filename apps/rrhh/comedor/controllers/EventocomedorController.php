<?php

/**
 * Clase controladora para gestionar los eventos que puedan darse en el comedor.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class EventocomedorController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new NomEventocomedorModel();
        parent::init();
    }

    public function eventocomedorAction()
    {
        $this->render();
    }

    public function getEventosComedorAction()
    {
        $domainEventoComedor = new NomEventocomedor();
        $r['data'] = $domainEventoComedor->getEventosComedor();
        $r['success'] = true;
        echo json_encode($r);
    }

    public function saveEventoAction()
    {
        $r = $this->model->saveEventoComedor($this->_request->getPost());
        echo $r;
    }

    public function eliminarEventoAction(){
        $ideventocomedor = $this->_request->getPost('ideventocomedor');

        $r = $this->model->eliminarEvento($ideventocomedor);

        echo $r;
    }

}