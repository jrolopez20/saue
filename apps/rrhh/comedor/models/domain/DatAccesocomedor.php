<?php

class DatAccesocomedor extends BaseDatAccesocomedor
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatPersona', array ('local' => 'idpersona', 'foreign' => 'idpersona'));
        $this->hasOne ('NomEventocomedor', array ('local' => 'ideventocomedor', 'foreign' => 'ideventocomedor'));
    }

    public function getAccesosComedor()
    {
        $query = Doctrine_Query::create()
            ->select('a.idaccesocomedor, a.idpersona, a.ideventocomedor, e.denominacion, a.desde, a.hasta')
            ->from('DatAccesocomedor a')
            ->leftJoin('a.NomEventocomedor e')
            ->orderBy('e.denominacion ASC');


        return $query->fetchArray();
    }

}