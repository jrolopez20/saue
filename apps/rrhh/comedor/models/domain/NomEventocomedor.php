<?php

class NomEventocomedor extends BaseNomEventocomedor
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany ('DatAccesocomedor', array ('local' => 'ideventocomedor', 'foreign' => 'ideventocomedor'));
    }

    public function getEventosComedor()
    {
        $query = Doctrine_Query::create()
            ->select("ideventocomedor, denominacion,
                (lunes is not null) as lunes, lunes->'inicio'::varchar as lunes_inicio, lunes->'fin'::varchar as lunes_fin,
                (martes is not null) as martes, martes->'inicio'::varchar as martes_inicio, martes->'fin'::varchar as martes_fin,
                (miercoles is not null) as miercoles, miercoles->'inicio'::varchar as miercoles_inicio, miercoles->'fin'::varchar as miercoles_fin,
                (jueves is not null) as jueves, jueves->'inicio'::varchar as jueves_inicio, jueves->'fin'::varchar as jueves_fin,
                (viernes is not null) as viernes, viernes->'inicio'::varchar as viernes_inicio, viernes->'fin'::varchar as viernes_fin,
                (sabado is not null) as sabado, sabado->'inicio'::varchar as sabado_inicio, sabado->'fin'::varchar as sabado_fin,
                (domingo is not null) as domingo, domingo->'inicio'::varchar as domingo_inicio, domingo->'fin'::varchar as domingo_fin")
            ->from('NomEventocomedor')
            ->orderBy('denominacion ASC');


        return $query->fetchArray();
    }

}