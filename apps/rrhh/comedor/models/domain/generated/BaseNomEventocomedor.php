<?php

abstract class BaseNomEventocomedor extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.nom_eventocomedor');
        $this->hasColumn('ideventocomedor', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_eventocomedor'));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('lunes', 'json', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('martes', 'json', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('miercoles', 'json', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('jueves', 'json', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('viernes', 'json', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('sabado', 'json', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('domingo', 'json', null, array ('notnull' => true,'primary' => false));
    }

}