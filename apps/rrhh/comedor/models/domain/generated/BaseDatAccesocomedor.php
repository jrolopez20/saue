<?php

abstract class BaseDatAccesocomedor extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_persona.dat_accesocomedor');
        $this->hasColumn('idaccesocomedor', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_persona.sec_accesocomedor'));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('ideventocomedor', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('desde', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('hasta', 'date', null, array ('notnull' => true,'primary' => false));
    }

}