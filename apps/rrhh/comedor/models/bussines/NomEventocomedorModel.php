<?php

class NomEventocomedorModel extends ZendExt_Model
{

    public function NomEventocomedorModel()
    {
        parent::ZendExt_Model();
    }

    public function saveEventoComedor($args)
    {
        try {
            $objEventoComedor = new NomEventocomedor();
            if ($args['ideventocomedor']) {
                $objEventoComedor = $objEventoComedor->getTable()->find($args['ideventocomedor']);
            }
            $objEventoComedor->denominacion = $args['denominacion'];

            //Lunes
            if ($args['lunes']) {
                $objEventoComedor->lunes = json_encode(array(
                    'inicio' => $args['lunes_inicio'],
                    'fin' => $args['lunes_fin']
                ));
            } else {
                $objEventoComedor->lunes = null;
            }

            //Martes
            if ($args['martes']) {
                $objEventoComedor->martes = json_encode(array(
                    'inicio' => $args['martes_inicio'],
                    'fin' => $args['martes_fin']
                ));
            } else {
                $objEventoComedor->martes = null;
            }

            //Miercoles
            if ($args['miercoles']) {
                $objEventoComedor->miercoles = json_encode(array(
                    'inicio' => $args['miercoles_inicio'],
                    'fin' => $args['miercoles_fin']
                ));
            } else {
                $objEventoComedor->miercoles = null;
            }

            //Jueves
            if ($args['jueves']) {
                $objEventoComedor->jueves = json_encode(array(
                    'inicio' => $args['jueves_inicio'],
                    'fin' => $args['jueves_fin']
                ));
            } else {
                $objEventoComedor->jueves = null;
            }

            //Viernes
            if ($args['viernes']) {
                $objEventoComedor->viernes = json_encode(array(
                    'inicio' => $args['viernes_inicio'],
                    'fin' => $args['viernes_fin']
                ));
            } else {
                $objEventoComedor->viernes = null;
            }

            //Sabado
            if ($args['sabado']) {
                $objEventoComedor->sabado = json_encode(array(
                    'inicio' => $args['sabado_inicio'],
                    'fin' => $args['sabado_fin']
                ));
            } else {
                $objEventoComedor->sabado = null;
            }

            //Domingo
            if ($args['domingo']) {
                $objEventoComedor->domingo = json_encode(array(
                    'inicio' => $args['domingo_inicio'],
                    'fin' => $args['domingo_fin']
                ));
            } else {
                $objEventoComedor->domingo = null;
            }


            $objEventoComedor->save();

            return "{'success':true, 'codMsg':1, 'mensaje':'Evento guardado satisfactoriamente'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

    public function eliminarEvento($ideventocomedor)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('NomEventocomedor')
                ->where('ideventocomedor = ?', $ideventocomedor)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Evento de comedor en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

}