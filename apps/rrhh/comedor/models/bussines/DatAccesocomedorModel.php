<?php

class DatAccesocomedorModel extends ZendExt_Model
{

    public function DatAccesocomedorModel()
    {
        parent::ZendExt_Model();
    }

    public function addAcceso($params)
    {
        $objAcceso = new DatAccesocomedor();
        $objAcceso->ideventocomedor = $params->ideventocomedor;
        $objAcceso->idpersona = $params->idpersona;
        $objAcceso->desde = $params->desde;
        $objAcceso->hasta = $params->hasta;
        $objAcceso->save();

        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idaccesocomedor':'" . $objAcceso->idaccesocomedor . "'}";
    }

    public function updateAcceso($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('DatAccesocomedor');
            if($params->ideventocomedor){
                $query->set('ideventocomedor', $params->ideventocomedor);
            }
            if($params->desde){
                $query->set('desde', '?', $params->desde);
            }
            if($params->hasta){
                $query->set('hasta', '?', $params->hasta);
            }

            $query->where('idaccesocomedor = ?', $params->idaccesocomedor);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actualizado', 'idaccesocomedor':'" . $params->idaccesocomedor . "'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

    public function destroyAcceso($idaccesocomedor)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('DatAccesocomedor')
                ->where('idaccesocomedor = ?', $idaccesocomedor)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Acceso de comedor en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }
}

