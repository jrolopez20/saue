<?php

interface CalendarCalendarioInterface {
/**
 * Devuelve un conjunto de periodos por año.
 * @retorno: arreglo
 * @param integer $anno-El año de los periodos a encontrar.
 */
  public function periodosXAnno($anno);
    
    }

?>
