<?php

class CalendarProxyService
{
    public function loadPeriodos($limit, $start)
    {
        return DatPeriododocente::cargarPeriodoService($limit, $start);
    }
    public function cargarPeriodos($anno)
    {
        return DatPeriododocente::cargarPeriodoServiceAnno($anno);
    }

    public function loadHorarios($limit,$start)
    {
        return DatHorario::cargarHorariosService($limit,$start);
    }
    public function countHorarios(){
        return DatHorario::countHorariosService();
    }
	
	public function getAnnos(){
        return array(array("anno"=>"2002"),array("anno"=>"2003"),array("anno"=>"2004"),array("anno"=>"2005"),array("anno"=>"2006"),array("anno"=>"2007"),array("anno"=>"2008"),array("anno"=>"2009"),array("anno"=>"2010"),array("anno"=>"2011"),array("anno"=>"2012"),array("anno"=>"2013"),array("anno"=>"2014"),array("anno"=>"2015"),array("anno"=>"2016"));
    }

    public function loadAulas($limit, $start)
    {
        return DatAula::cargarLocales($limit, $start);
    }
} 