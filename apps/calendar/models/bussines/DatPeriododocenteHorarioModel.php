<?php

class DatPeriododocenteHorarioModel extends ZendExt_Model
{

    public function DatPeriododocenteHorarioModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarPeriodHor($limit, $start,$idperiodo)

    {
        try {
            return DatPeriododocenteHorario::cargarPeriodHor($limit, $start,$idperiodo);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countPeriodHor()
    {

        try {
            return DatPeriododocenteHorario::countTPeriodHor();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarPeriodHor( $idusuario,$idperiododocente,$idhorario,$idprofesor, $estado,$horas,$semanas,$maximas_falta)
    {

        try {
            $periodhor = new DatPeriododocenteHorario();
            $periodhor->idprofesor = $idprofesor;
            $periodhor->idperiododocente = $idperiododocente;
            $periodhor->idhorario = $idhorario;
            $periodhor->idusuario = $idusuario;
            $periodhor->horas = $horas;
            $periodhor->semanas = $semanas;
            $periodhor->maximas_falta = $maximas_falta;
            if($estado)
                $periodhor->estado = $estado;
            else
                $periodhor->estad0 = false;

            $periodhor->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarPeriodHor($idperidhor,$idusuario,$idperiododocente,$idhorario,$idprofesor, $estado,$horas,$semanas,$maximas_falta)
    {

        try {
            $periodhor = Doctrine::getTable("DatPeriododocenteHorario")->find($idperidhor);
            $periodhor->idprofesor = $idprofesor;
            $periodhor->idperiododocente = $idperiododocente;
            $periodhor->idhorario = $idhorario;
            $periodhor->idusuario = $idusuario;
            $periodhor->horas = $horas;
            $periodhor->semanas = $semanas;
            $periodhor->maximas_falta = $maximas_falta;
            if($estado)
                $periodhor->estado = $estado;
            else
                $periodhor->estado = false;

            $periodhor->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarPeriodHor($idperidhor)
    {

        try {
            return DatPeriododocenteHorario::eliminarPeriodHor($idperidhor);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

