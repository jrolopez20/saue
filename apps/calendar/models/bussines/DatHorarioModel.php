<?php

class DatHorarioModel extends ZendExt_Model
{
    public function DatHorarioModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarHorarios($limit, $start)
    {
        return DatHorario::cargarHorarios($limit, $start);
    }

    public function cargarHorariosNotInPeriodo($limit, $start, $idperiodo)
    {
        $idIna = $idIn = DatPeriododocenteHorario::idHorarioNotIn($idperiodo);
        $idIn=array();
        foreach($idIna as $r){
            $idIn[]=$r['idhorario'];
        }
        return DatHorario::cargarHorariosNotInPeriodo($limit, $start, $idIn);
    }

    public function countHorarios()
    {
        try {
            return DatHorario::countHorarios();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarHorario($descripcion, $usuario, $estado, $fecha, $dias)
    {
        try {
            $horario = new DatHorario();
            $horario->idusuario = $usuario;
            $horario->descripcion = $descripcion;
            $horario->fecha = $fecha;
            if ($estado)
                $horario->estado = $estado;
            else
                $horario->estado = false;

            $horario->dias = $dias;

            $horario->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarHorario($idhorario, $descripcion, $usuario, $estado, $fecha, $dias)
    {
        try {
            $horario = Doctrine::getTable("DatHorario")->find($idhorario);
            $horario->idhorario = $idhorario;
            $horario->descripcion = $descripcion;
            $horario->fecha = $fecha;
            $horario->idusuario = $usuario;
            if ($estado)
                $horario->estado = $estado;
            else
                $horario->estado = false;

            $horario->dias = $dias;

            $horario->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarHorario($idhorarios)
    {
        try {
            foreach ($idhorarios as $idhor) {
                DatHorario::eliminarHorario($idhor);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}

