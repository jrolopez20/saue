<?php

class DatTipoPeriododocenteModel extends ZendExt_Model
{
    public function DatTipoPeriododocenteModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarTipoPeridos($limit, $start)
    {
        try {
            return DatTipoPeriododocente::cargarTipoPerid($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarTipoPeridosA($limit, $start)
    {
        try {
            return DatTipoPeriododocente::cargarTipoPeridA($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }



    public function countTipoPeridos()
    {
        try {
            return DatTipoPeriododocente::countTipoPerid();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarTipoPerido($descripcion, $usuario, $estado, $fecha)
    {
        try {
            $tipoperiodo = new DatTipoPeriododocente();
            $tipoperiodo->fecha = $fecha;
            $tipoperiodo->idusuario = $usuario;
            $tipoperiodo->descripcion = $descripcion;
            if($estado)
                $tipoperiodo->estado = $estado;
            else
                $tipoperiodo->estado = false;

            $tipoperiodo->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTipoPerido($idtipoperiododocente, $descripcion, $usuario, $estado, $fecha)
    {
        try {
            $tipoperiodo = Doctrine::getTable("DatTipoPeriododocente")->find($idtipoperiododocente);
            $tipoperiodo->fecha = $fecha;
            $tipoperiodo->idusuario = $usuario;
            $tipoperiodo->descripcion = $descripcion;
            if($estado)
                $tipoperiodo->estado = $estado;
            else
                $tipoperiodo->estado = false;

            $tipoperiodo->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarTipoPerido($idtipoperido)
    {
        try {
            foreach ($idtipoperido as $idtipperi) {
                DatTipoPeriododocente::eliminarTipoPerid($idtipperi);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}
