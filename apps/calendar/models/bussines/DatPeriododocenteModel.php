<?php

class DatPeriododocenteModel extends ZendExt_Model
{

    public function DatPeriododocenteModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarPeriodosDoc($limit, $start)
    {
        try {

            $periodos = DatPeriododocente::cargarPeriodosDoc($limit, $start);

            for ($i=0; $i<count($periodos);$i++) {
                $periodos[$i]['anno'] = substr($periodos[$i]['codperiodo'], 0, 4);;
                $periodos[$i]['letra'] = substr($periodos[$i]['codperiodo'], 4);
            }

            return $periodos;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }
    public function cargarPeriodosDocAll()
    {
        try {
            return DatPeriododocente::cargarPeriodosDocAll();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countPeriodosDoc()
    {

        try {
            return DatPeriododocente::countPeriodosDoc();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarPeriodosDoc($descripcion, $usuario, $estado, $fecha, $idtipo_periododocente, $codperiodo)
    {
        try {
            $periodo = new DatPeriododocente();
            $periodo->idtipo_periododocente = $idtipo_periododocente;
            $periodo->fecha = $fecha;
            $periodo->idusuario = $usuario;
            $periodo->descripcion = $descripcion;
            $periodo->codperiodo = $codperiodo;
            if ($estado)
                $periodo->estado = $estado;
            else
                $periodo->estado = false;

            $periodo->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarPeriodosDoc($idperiododocente, $descripcion, $usuario, $estado, $fecha, $idtipo_periododocente, $codperiodo)
    {

        try {
            $periodo = Doctrine::getTable("DatPeriododocente")->find($idperiododocente);

            $periodo->idtipo_periododocente = $idtipo_periododocente;
            $periodo->fecha = $fecha;
            $periodo->idusuario = $usuario;
            $periodo->descripcion = $descripcion;
            $periodo->codperiodo = $codperiodo;
            if ($estado)
                $periodo->estado = $estado;
            else
                $periodo->estado = false;

            $periodo->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarPeriodosDoc($idperiododocente)
    {

        try {
            foreach ($idperiododocente as $idper) {
                DatPeriododocente::eliminarPeriodosDoc($idper);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAnnos()
    {
        try {
            $var = $this->integrator->calendar->getAnnos();

            return array('cantidad' => 0, 'datos' => $var);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

