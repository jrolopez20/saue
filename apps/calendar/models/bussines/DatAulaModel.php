<?php

class DatAulaModel extends ZendExt_Model
{
    public function DatAulaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarLocales($limit, $start)
    {
        return DatAula::cargarLocales($limit, $start);
    }

    public function countLocales()
    {
        try {
            return DatAula::countLocales();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarLocalesByD($limit, $start, $filter)
    {
        return DatAula::cargarLocalesByD($limit, $start, $filter);
    }

    public function countLocalesByD($filter)
    {
        try {
            return DatAula::countLocalesByD($filter);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }



    public function insertarLocal($descripcion, $usuario, $estado, $fecha, $idtipolocal, $idubicacion)
    {
        try {
            $local = new DatAula();
            $local->descripcion = $descripcion;
            $local->fecha = $fecha;
            $local->idtipo_aula = $idtipolocal;
            $local->idusuario = $usuario;
            $local->idubicacion = $idubicacion;
            if($estado)
                $local->estado = $estado;
            else
                $local->estado = false;

            $local->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarLocal($idlocal, $descripcion, $usuario, $estado, $fecha, $idtipolocal, $idubicacion)
    {
        try {
            $local = Doctrine::getTable("DatAula")->find($idlocal);

            $local->descripcion = $descripcion;
            $local->fecha = $fecha;
            $local->idtipo_aula = $idtipolocal;
            $local->idusuario = $usuario;
            $local->idubicacion = $idubicacion;
            if($estado)
                $local->estado = $estado;
            else
                $local->estado = false;

            $local->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarLocal($idlocales)
    {
        try {
            foreach ($idlocales as $idloc) {
                DatAula::eliminarLocales($idloc);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}

