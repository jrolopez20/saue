<?php

class DatTipoAulaModel extends ZendExt_Model
{
    public function DatTipoAulaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarTipoLocal($limit, $start)
    {
        try {
            return DatTipoAula::cargarTipoLocal($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function cargarTipoLocalesByD($limit, $start, $filter)
    {
        try {
            return DatTipoAula::cargarTipoLocalesByD($limit, $start, $filter);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE) {
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            }
            return false;
        }
    }

    public function cargarTipoLocalA($limit, $start)
    {
        try {
            return DatTipoAula::cargarTipoLocalA($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }



    public function countTipoLocal()
    {

        try {
            return DatTipoAula::countTipoLocal();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countTipoLocalesByD()
    {

        try {
            return DatTipoAula::countTipoLocalesByD($filter);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }



    public function insertarTipoLocal($fecha, $idusuario, $descripcion, $estado)
    {

        try {
            $tipolocal = new DatTipoAula();
            $tipolocal->fecha = $fecha;
            $tipolocal->idusuario = $idusuario;
            $tipolocal->descripcion = $descripcion;
            if($estado)
                $tipolocal->estado = $estado;
            else
                $tipolocal->estado = false;

            $tipolocal->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTipoLocal($idtipo_local, $descripcion, $usuario, $estado, $fecha)
    {

        try {
            $tipolocal = Doctrine::getTable("DatTipoAula")->find($idtipo_local);

            $tipolocal->fecha = $fecha;
            $tipolocal->idusuario = $usuario;
            $tipolocal->descripcion = $descripcion;
            if($estado)
                $tipolocal->estado = $estado;
            else
                $tipolocal->estado = false;

            $tipolocal->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarTipoLocal($idtipolocales)
    {

        try {
            foreach ($idtipolocales as $idtipoloc) {
                DatTipoAula::eliminarTipoLocal($idtipoloc);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

