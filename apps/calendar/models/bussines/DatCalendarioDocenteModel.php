<?php

class DatCalendarioDocenteModel extends ZendExt_Model
{

    public function DatCalendarioDocenteModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarCalendDoc($limit = 20, $start = 0)
    {
        try {
            return DatCalendarioDocente::cargarCalendDoc($limit = 20, $start = 0);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countCalendDoc()
    {

        try {
            return DatCalendarioDocente::countCalendDoc();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarTipoLocal( $fecha, $idusuario, $idlocal,$idhorario_periododocente,$idpensumenfasismateriatipo,$idprofesor,$cantestudiantes)
    {

        try {
            $calendario = new DatCalendarioDocente();
            $calendario->cantestudiantes = $cantestudiantes;
            $calendario->idprofesor = $idprofesor;
            $calendario->idpensumenfasismateriatipo = $idpensumenfasismateriatipo;
            $calendario->idhorario_periododocente = $idhorario_periododocente;
            $calendario->idlocal = $idlocal;
            $calendario->fecha = $fecha;
            $calendario->idusuario = $idusuario;
            $calendario->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTipoLocal($idcalendario_docente,$fecha, $idusuario, $idlocal,$idhorario_periododocente,$idpensumenfasismateriatipo,$idprofesor,$cantestudiantes)
    {

        try {
            $calendario = Doctrine::getTable("DatCalendarioDocente")->find($idcalendario_docente);
            $calendario->cantestudiantes = $cantestudiantes;
            $calendario->idprofesor = $idprofesor;
            $calendario->idpensumenfasismateriatipo = $idpensumenfasismateriatipo;
            $calendario->idhorario_periododocente = $idhorario_periododocente;
            $calendario->idlocal = $idlocal;
            $calendario->fecha = $fecha;
            $calendario->idusuario = $idusuario;
            $calendario->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarPTipoLocal($idtipolocal)
    {

        try {
            return DatTipoAula::eliminarTipoLocal($idtipolocal);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}
