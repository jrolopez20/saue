<?php

class DatUbicacionModel extends ZendExt_Model
{
    public function DatUbicacionModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarUbicaciones($limit, $start)
    {
        return DatUbicacion::cargarUbicaciones($limit, $start);
    }

    public function cargarUbicacionesA($limit, $start)
    {
        return DatUbicacion::cargarUbicacionesA($limit, $start);
    }



    public function countLocales()
    {
        try {
            return DatUbicacion::countUbicaciones();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarUbicacion($descripcion, $usuario, $estado, $fecha)
    {
        try {
            $ubicacion = new DatUbicacion();
            $ubicacion->descripcion = $descripcion;
            $ubicacion->fecha = $fecha;
            $ubicacion->idusuario = $usuario;
            if($estado)
                $ubicacion->estado = true;
            else
                $ubicacion->estado = false;

            $ubicacion->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarLocal($idubicacion, $descripcion, $usuario, $estado, $fecha)
    {
        try {
            $ubicacion = Doctrine::getTable("DatUbicacion")->find($idubicacion);
            $ubicacion->descripcion = $descripcion;
            $ubicacion->fecha = $fecha;
            $ubicacion->idusuario = $usuario;
            if($estado)
                $ubicacion->estado = true;
            else
                $ubicacion->estado = false;

            $ubicacion->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarUbicaciones($idubicacion)
    {
        try {
            foreach ($idubicacion as $idubic) {
                DatUbicacion::eliminarLocales($idubic);
            }
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}