<?php

class DatAula extends BaseDatLocal
{
    static public function cargarLocales($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT loc.idaula, loc.descripcion, loc.idusuario, loc.fecha, loc.estado, loc.idtipo_aula, tl.descripcion as tipolocal, ubic.descripcion as ubicacion, ubic.idubicacion
                                                  FROM mod_saue.dat_aula loc, mod_saue.dat_tipo_aula tl, mod_saue.dat_ubicacion ubic
                                                  WHERE loc.idtipo_aula = tl.idtipo_aula and loc.idubicacion = ubic.idubicacion LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countLocales()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAula l")
            ->execute()
            ->count();
        return $result;
    }

     static public function cargarLocalesByD($limit, $start, $filter)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT loc.idaula, loc.descripcion, loc.idusuario, loc.fecha, loc.estado, loc.idtipo_aula, tl.descripcion as tipolocal, ubic.descripcion as ubicacion, ubic.idubicacion
                                                  FROM mod_saue.dat_aula loc, mod_saue.dat_tipo_aula tl, mod_saue.dat_ubicacion ubic
                                                  WHERE loc.idtipo_aula = tl.idtipo_aula and loc.idubicacion = ubic.idubicacion
                                                  and loc.descripcion LIKE '%" . $filter[0]->value . "%' ORDER BY loc.descripcion LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countLocalesByD($filter)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAula l")->where("l.descripcion LIKE '%" . $filter[0]->value . "%' ")
            ->execute()
            ->count();
        return $result;
    }

    static public function eliminarLocales($idlocal)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatAula l")
            ->where("l.idaula=?", $idlocal)
            ->execute();
        return true;
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatTipoAula', array('local' => 'idtipo_aula', 'foreign' => 'idtipo_aula'));
        $this->hasMany('DatCalendarioDocente', array('local' => 'idaula', 'foreign' => 'idlocal'));
    }
}
