<?php

class DatCalendarioDocente extends BaseDatCalendarioDocente
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatAula', array('local' => 'idlocal', 'foreign' => 'idlocal'));
        $this->hasOne('DatPeriododocenteHorario', array('local' => 'idhorario_periododocente', 'foreign' => 'idhorario_periododocente'));
        $this->hasOne('PensumMateriaEnfasisTipomateria', array('local' => 'idpensumenfasismateriatipo', 'foreign' => 'idpensumenfasismateriatipo'));
        $this->hasOne('DatProfesor', array('local' => 'idprofesor', 'foreign' => 'idprofesor'));
    }
    static public function cargarCalendDoc($limit = 20, $start = 0)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatCalendarioDocente c")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countCalendDoc()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatCalendarioDocente c")
            ->execute();
        return $result->count();

    }

    static public function eliminarCalendDoc($idcalendario_docente)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatCalendarioDocente c")
            ->where("t.idcalendario_docente=?", $idcalendario_docente)
            ->execute();
        return $result->toArray();
    }

}

