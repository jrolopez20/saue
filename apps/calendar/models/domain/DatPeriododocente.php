<?php

class DatPeriododocente extends BaseDatPeriododocente
{
    static public function cargarPeriodosDoc($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT pd.idperiododocente, pd.descripcion, pd.idusuario, pd.fecha, pd.estado,
                                                    pd.idtipo_periododocente, pd.codperiodo, tpd.descripcion as tipoperiodo
                                                  FROM mod_saue.dat_periododocente pd, mod_saue.dat_tipo_periododocente tpd
                                                  WHERE pd.idtipo_periododocente = tpd.idtipo_periododocente LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function cargarPeriodosDocAll()
    {
        $query = Doctrine_Query::create();
        $result = $query->select("p.idperiododocente,p.descripcion")
        ->from("DatPeriododocente p")
            ->execute();

        return $result->toArray();
    }

    static public function cargarPeriodoService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT pd.idperiododocente, pd.descripcion, pd.idusuario, pd.fecha, pd.estado,
                                                    pd.idtipo_periododocente, tpd.descripcion as tipoperiodo
                                                  FROM mod_saue.dat_periododocente pd, mod_saue.dat_tipo_periododocente tpd
                                                  WHERE pd.idtipo_periododocente = tpd.idtipo_periododocente LIMIT AND pd.estado=true" . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }


    static public function cargarPeriodoServiceAnno($anno)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT pd.idperiododocente, pd.descripcion
                                                  FROM mod_saue.dat_periododocente pd, mod_saue.dat_tipo_periododocente tpd
                                                  WHERE pd.idtipo_periododocente = tpd.idtipo_periododocente AND codperiodo LIKE '". $anno ."%' AND pd.estado=true");
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    static public function countPeriodosDoc()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatPeriododocente p")
            ->execute();
        return $result->count();

    }

    static public function eliminarPeriodosDoc($idperiododocente)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatPeriododocente p")
            ->where("p.idperiododocente=?", $idperiododocente)
            ->execute();
        return $result;
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatTipoPeriododocente', array('local' => 'idtipo_periododocente', 'foreign' => 'idtipo_periododocente'));
        $this->hasMany('DatPeriododocenteHorario', array('local' => 'idperiododocente', 'foreign' => 'idperiododocente'));
    }
}

