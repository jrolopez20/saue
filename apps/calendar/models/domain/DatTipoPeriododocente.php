<?php

class DatTipoPeriododocente extends BaseDatTipoPeriododocente
{

    static public function cargarTipoPerid($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT tpd.idtipo_periododocente, tpd.descripcion as tipoperiodo, tpd.idusuario, tpd.fecha, tpd.estado
                                                  FROM mod_saue.dat_tipo_periododocente tpd LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;

    }

    static public function cargarTipoPeridA($limit, $start)
    {
        $query = Doctrine_Query::create();
        $stmt = $query->getConnection()->prepare("SELECT tpd.idtipo_periododocente, tpd.descripcion as tipoperiodo, tpd.idusuario, tpd.fecha, tpd.estado
                                                  FROM mod_saue.dat_tipo_periododocente tpd where tpd.estado=true LIMIT " . $limit . " OFFSET " . $start . ";");
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;

    }



    static public function countTipoPerid()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoPeriododocente p")
            ->execute();
        return $result->count();

    }

    static public function eliminarTipoPerid($idtipo_periododocente)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatTipoPeriododocente p")
            ->where("p.idtipo_periododocente=?", $idtipo_periododocente)
            ->execute();
        return $result;
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatPeriododocente', array('local' => 'idtipo_periododocente', 'foreign' => 'idtipo_periododocente'));
    }

}

