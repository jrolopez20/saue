<?php

class DatHorario extends BaseDatHorario
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatPeriododocenteHorario', array('local' => 'idhorario', 'foreign' => 'idhorario'));
    }

    static public function cargarHorarios($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatHorario")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }
    static public function cargarPeriodHor($limit, $start, $idperiodo)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->select("p.*,h.*,p,pd.*")
            ->from("DatHorario h")
            ->innerJoin("h.DatPeriododocenteHorario p")
            ->innerJoin("p.DatPeriododocente pd")
            // ->innerJoin("p.DatProfesor pf ON p.idprofesor=pf.idprofesor")
            ->where("p.idperiododocente= ?", $idperiodo)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }
    static public function cargarHorariosNotInPeriodo($limit, $start, $idIn)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatHorario h")
            ->whereNotIn("h.idhorario", $idIn)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }

    static public function cargarHorariosService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatHorario h")
            ->where("h.estado=true")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }

    static public function countHorarios()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatHorario")
            ->execute()
            ->count();
        return $result;
    }

    static public function countHorariosService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatHorario")
            ->where("h.estado=true")
            ->execute()
            ->count();
        return $result;
    }

    static public function eliminarHorario($idhorario)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatHorario h")
            ->where("h.idhorario=?", $idhorario)
            ->execute();
        return true;
    }
}

