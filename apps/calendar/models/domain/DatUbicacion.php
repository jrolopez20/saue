<?php

class DatUbicacion extends BaseDatUbicacion
{

    public function setUp()
    {
        parent :: setUp();
    }

    static public function cargarUbicaciones($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query->from("DatUbicacion")->limit($limit)->offset($start)->execute();

        return $result->toArray(true);
    }

    static public function cargarUbicacionesA($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query->from("DatUbicacion ub")->limit($limit)->offset($start)->where("ub.estado=?",true)->execute();

        return $result->toArray(true);
    }



    static public function countLocales()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAula l")
            ->execute()
            ->count();
        return $result;
    }

    static public function eliminarLocales($idlocal)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatAula l")
            ->where("l.idaula=?", $idlocal)
            ->execute();
        return true;
    }

}

