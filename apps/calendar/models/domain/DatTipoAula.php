<?php

class DatTipoAula extends BaseDatTipoAula
{

    static public function cargarTipoLocal($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAula t")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function cargarTipoLocalesByD($limit, $start, $filter)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAula t")
            ->where("t.descripcion LIKE '%" . $filter[0]->value . "%' ")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countTipoLocalesByD($filter)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAula t")
            ->where("t.descripcion LIKE '%" . $filter[0]->value . "%' ")
            ->execute();
        return $result->count();

    }

    static public function cargarTipoLocalA($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAula t")
            ->limit($limit)
            ->offset($start)
            ->where("t.estado=?", true)
            ->execute();
        return $result->toArray();

    }


    static public function countTipoLocal()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoAula t")
            ->execute();
        return $result->count();

    }

    static public function eliminarTipoLocal($idtipolocal)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatTipoAula t")
            ->where("t.idtipo_aula=?", $idtipolocal)
            ->execute();
        return $result;
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatAula', array('local' => 'idtipo_aula', 'foreign' => 'idtipo_aula'));
    }

}

