<?php

abstract class BaseDatCalendarioDocente extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_calendario_docente');
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('cantestudiantes', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idprofesor', 'numeric', null, array ('notnull' => false,'primary' => false));
       $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpensumenfasismateriatipo', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idhorario_periododocente', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idlocal', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcalendario_docente', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_calendario_docente'));
    }


}

