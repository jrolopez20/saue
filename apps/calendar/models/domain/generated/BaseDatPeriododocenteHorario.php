<?php

abstract class BaseDatPeriododocenteHorario extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_periododocente_horario');
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('maximas_falta', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('semanas', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('horas', 'integer', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idprofesor', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idhorario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idperiododocente', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idhorario_periododocente', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_periododocente_horario'));
    }


}

