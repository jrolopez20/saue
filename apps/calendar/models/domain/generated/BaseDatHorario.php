<?php

abstract class BaseDatHorario extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_horario');
        $this->hasColumn('dias', 'integer', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('codigo', 'integer', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idhorario', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_horario'));
    }


}

