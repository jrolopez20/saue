<?php

class DatPeriododocenteHorario extends BaseDatPeriododocenteHorario
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatPeriododocente', array('local' => 'idperiododocente', 'foreign' => 'idperiododocente'));
        $this->hasOne('DatHorario', array('local' => 'idhorario', 'foreign' => 'idhorario'));
       // $this->hasOne('DatProfesor', array('local' => 'idprofesor', 'foreign' => 'idprofesor'));;
        $this->hasMany('DatCalendarioDocente', array('local' => 'idhorario_periododocente', 'foreign' => 'idhorario_periododocente'));
    }

    static public function cargarPeriodHor($limit, $start, $idperiodo)
    {
        $query = Doctrine_Query::create();
        $sql="SELECT h.idhorario, h.descripcion,p.idhorario_periododocente,p.idperiododocente,p.horas,p.semanas,p.maximas_falta,p.estado,(pf.nombre, pf.apellidos) as profesor,pf.idprofesor
        FROM mod_saue.dat_periododocente_horario p INNER JOIN mod_saue.dat_horario h ON p.idhorario=h.idhorario INNER JOIN mod_saue.dat_profesor pf
        ON pf.idprofesor=p.idprofesor WHERE p.idperiododocente=".$idperiodo.";";
        $stmt = $query->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;

        return $result;

    }

    static public function countTPeriodHor()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatPeriododocenteHorario p")
            ->execute();
        return $result->count();

    }

    static public function idHorarioNotIn($idperiodo)
    {
        $query = Doctrine_Query::create();
        $result = $query->select("p.idhorario")
            ->from("DatPeriododocenteHorario p")
            ->where("p.idperiododocente = ?", $idperiodo)
            ->execute();
        return $result->toArray();

    }

    static public function eliminarPeriodHor($idhorario_periododocente)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatPeriododocenteHorario p")
            ->where("p.idhorario_periododocente=?", $idhorario_periododocente)
            ->execute();
        return true;
    }

}

