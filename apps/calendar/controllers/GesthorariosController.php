<?php

class GesthorariosController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->modelHora = new DatHorarioModel();
    }

    public function gesthorariosAction()
    {
        $this->render();        
    }

    public function cargarHorariosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $horarios = $this->modelHora->cargarHorarios($limit, $start);

        $cant = $this->modelHora->countHorarios();

        $result = array('cantidad' => $cant, 'datos' => $horarios);

        echo json_encode($result);
    }

    public function insertarHorarioAction()
    {
        $descripcion = $this->_request->getPost('descripcion');
        $dias = $this->_request->getPost('dias');
        $estado = $this->_request->getPost('estado');

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->modelHora->insertarHorario($descripcion, $usuario, $estado, $fecha, $dias);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarHorarioAction()
    {
        $idhorario = $this->_request->getPost('idhorario');
        $descripcion = $this->_request->getPost('descripcion');
        $dias = $this->_request->getPost('dias');
        $estado = $this->_request->getPost('estado');

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->modelHora->modificarHorario($idhorario, $descripcion, $usuario, $estado, $fecha, $dias);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarHorarioAction()
    {
        $idhorarios = json_decode($this->_request->getPost('idhorarios'));

        $this->modelHora->eliminarHorario($idhorarios);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }
}

?>