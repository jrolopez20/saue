<?php


class GestlocalesController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->localModel = new DatAulaModel();
        $this->ubicModel=new DatUbicacionModel();
    }

    public function gestlocalesAction()
    {
        $this->render();
    }

    public function cargarLocalesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $locales = $this->localModel->cargarLocales($limit, $start);

        $cant = $this->localModel->countLocales();

        $result = array('cantidad' => $cant, 'datos' => $locales);

        echo json_encode($result);
    }

    public function cargarLocalesByDAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filter = json_decode($this->_request->getPost('filtros'));

        $locales = $this->localModel->cargarLocalesByD($limit, $start, $filter);
        $cant = $this->localModel->countLocalesByD($filter);

        $result = array('cantidad' => $cant, 'datos' => $locales);

        echo json_encode($result);
    }

    public function insertarLocalAction()
    {
        $descripcion = $this->_request->getPost('descripcion');
        $idtipolocal = $this->_request->getPost('idtipolocal');
        $estado = $this->_request->getPost('estado');
        $idubicacion = $this->_request->getPost('idubicacion');

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->localModel->insertarLocal($descripcion, $usuario, $estado, $fecha, $idtipolocal, $idubicacion);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarLocalAction()
    {
        $idlocal = $this->_request->getPost('idlocal');
        $descripcion = $this->_request->getPost('descripcion');
        $idtipolocal = $this->_request->getPost('idtipolocal');
        $estado = $this->_request->getPost('estado');
        $idubicacion = $this->_request->getPost('idubicacion');

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->localModel->modificarLocal($idlocal, $descripcion, $usuario, $estado, $fecha, $idtipolocal, $idubicacion);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarLocalAction()
    {
        $idlocales = json_decode($this->_request->getPost('idlocales'));

        $this->localModel->eliminarLocal($idlocales);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

    public function cargarUbicacionAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $ubicaciones = $this->ubicModel->cargarUbicaciones($limit, $start);

        $result = array('cantidad' => 0, 'datos' => $ubicaciones);

        echo json_encode($result);
    }

    public function cargarUbicacionAAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $ubicaciones = $this->ubicModel->cargarUbicacionesA($limit, $start);

        $result = array('cantidad' => 0, 'datos' => $ubicaciones);

        echo json_encode($result);
    }
}

?>