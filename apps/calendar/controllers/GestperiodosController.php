<?php


class GestperiodosController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->periodoModel = new DatPeriododocenteModel();
    }

    public function gestperiodosAction()
    {
        $this->render();
    }

    public function cargarPeriodosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $locales = $this->periodoModel->cargarPeriodosDoc($limit, $start);

        $cant = $this->periodoModel->countPeriodosDoc();

        $result = array('cantidad' => $cant, 'datos' => $locales);

        echo json_encode($result);
    }

    public function insertarPeriodoAction()
    {
        $descripcion = $this->_request->getPost('descripcion');
        $idtipo_periododocente = $this->_request->getPost('idtipo_periododocente');
        $estado = $this->_request->getPost('estado');
        $codperiodo = $this->_request->getPost('anno') . $this->_request->getPost('letra');

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->periodoModel->insertarPeriodosDoc($descripcion, $usuario, $estado, $fecha, $idtipo_periododocente, $codperiodo);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarPeriodoAction()
    {
        $idperiododocente = $this->_request->getPost('idperiododocente');
        $descripcion = $this->_request->getPost('descripcion');
        $idtipo_periododocente = $this->_request->getPost('idtipo_periododocente');
        $estado = $this->_request->getPost('estado');
        $codperiodo = $this->_request->getPost('anno') . $this->_request->getPost('letra');

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->periodoModel->modificarPeriodosDoc($idperiododocente, $descripcion, $usuario, $estado, $fecha, $idtipo_periododocente, $codperiodo);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarPeriodoAction()
    {
        $idperiododocentes = json_decode($this->_request->getPost('idperiododocentes'));

        $this->periodoModel->eliminarPeriodosDoc($idperiododocentes);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

    public function cargarAnnosAction()
    {
        $result = $this->periodoModel->cargarAnnos();

        echo json_encode($result);
    }
}

?>