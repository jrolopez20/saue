<?php

class GesttipolocalesController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->tipoLocalModel = new DatTipoAulaModel();
    }

    public function gesttipolocalesAction()
    {
        $this->render();
    }

    public function cargarTipoLocalesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $tipolocales = $this->tipoLocalModel->cargarTipoLocal($limit, $start);

        $cant = $this->tipoLocalModel->countTipoLocal();

        $result = array('cantidad' => $cant, 'datos' => $tipolocales);

        echo json_encode($result);
    }

    public function cargarTipoLocalesByDAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filter = json_decode($this->_request->getPost('filtros'));

        $tiposlocales = $this->tipoLocalModel->cargarTipoLocalesByD($limit, $start, $filter);
        $cant = $this->tipoLocalModel->countTipoLocalesByD($filter);

        $result = array('cantidad' => $cant, 'datos' => $tiposlocales);

        echo json_encode($result);
    }

    public function cargarTipoLocalesAAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $tipolocales = $this->tipoLocalModel->cargarTipoLocalA($limit, $start);

        $result = array('cantidad' => 0, 'datos' => $tipolocales);

        echo json_encode($result);
    }



    public function insertarTipoLocalAction()
    {
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->tipoLocalModel->insertarTipoLocal($fecha, $usuario, $descripcion, $estado);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarTipoLocalAction()
    {
        $idtipo_local = $this->_request->getPost('idtipo_aula');
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->tipoLocalModel->modificarTipoLocal($idtipo_local, $descripcion, $usuario, $estado, $fecha);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarTipoLocalAction()
    {
        $idtipo_locales = json_decode($this->_request->getPost('idtipo_locales'));

        $this->tipoLocalModel->eliminarTipoLocal($idtipo_locales);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }
}

?>