<?php

/**
 * Componente para gestinar los sistemas.
 *
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GesthorarioxperiodoController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
    }

    public function gesthorarioxperiodoAction()
    {
        $this->render();
    }

    public function cargarPeriodosAction()
    {

        $model = new DatPeriododocenteModel();
        $periodos = $model->cargarPeriodosDocAll();
        //print_r($periodos);
        //die;
        if (count($periodos)) {
            foreach ($periodos as $valores => $valor) {
                $periodosArr[$valores]['id'] = $valor['idperiododocente'];
                $periodosArr[$valores]['text'] = $valor['descripcion'];
                $periodosArr[$valores]['leaf'] = true;

            }
            echo json_encode($periodosArr);
            return;
        }
    }

    public function cargarHorariosXPeriodosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idperiodo = $this->_request->getPost('idperiodo');
        $periodosxhorario = new DatPeriododocenteHorarioModel();
        $datos = $periodosxhorario->cargarPeriodHor($limit, $start, $idperiodo);
        // $cant=$periodosxhorario->countPeriodHor();
        echo json_encode($datos);
        return;
    }

    public function cargarHorariosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idperiodo = $this->_request->getPost('idperiodo');

        $horario = new DatHorarioModel();

        $datos = $horario->cargarHorariosNotInPeriodo($limit, $start,$idperiodo);
        $cant = $horario->countHorarios();

        // $cant=$periodosxhorario->countPeriodHor();
        $result = array('cantidad' => $cant, 'datos' => $datos);
        echo json_encode($result);
        return;
    }

    public function eliminarAsocAction()
    {
        $arrIds = json_decode($this->_request->getPost('idhorarioxperiodo'));


        $horarioxp = new DatPeriododocenteHorarioModel();
        for($i=0;$i<count($arrIds);$i++){
            $idhorarioxperiodo=$arrIds[$i];
            $horarioxp->eliminarPeriodHor($idhorarioxperiodo);
        }


        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfEliminar}");
    }
    public function cargarProfesoresAction(){
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $prof= $this->integrator->profesor->cargarProfesores($limit,$start);
        $cant= $this->integrator->profesor->contarProfesores();
        $result = array('cantidad' => $cant, 'datos' => $prof);
        echo json_encode($result);
    }

    public function insertarHorarioEnPeriodoAction(){
        $idhorarios = $this->_request->getPost('idhorarios');
        $arrIdhorar=json_decode($idhorarios);
        $idperiodo = $this->_request->getPost('idperiodo');
        $horas = $this->_request->getPost('horas');
        $max_faltas = $this->_request->getPost('maximas_falta');
        $estado = $this->_request->getPost('estado');
        $idprofesor = $this->_request->getPost('idprofesor');
        $semanas = $this->_request->getPost('semanas');
        $usuario = $this->global->Perfil->idusuario;
        $horXperiod= new DatPeriododocenteHorarioModel();

        for($i=0;$i<count($arrIdhorar);$i++){
            $horXperiod->insertarPeriodHor($usuario,$idperiodo,$arrIdhorar[$i],$idprofesor,$estado,$horas,$semanas,$max_faltas);
        }
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");

    }
    public function modificarHorarioEnPeriodoAction(){
        $idhorario_periododocente = $this->_request->getPost('idhorario_periododocente');
        $idperiodo = $this->_request->getPost('idperiodo');
        $horas = $this->_request->getPost('horas');
        $max_faltas = $this->_request->getPost('maximas_falta');
        $estado = $this->_request->getPost('estado');
        $idprofesor = $this->_request->getPost('idprofesor');
        $semanas = $this->_request->getPost('semanas');
        $usuario = $this->global->Perfil->idusuario;
        $idhorario=$this->_request->getPost('idhorario');
        $horXperiod= new DatPeriododocenteHorarioModel();

   //     for($i=0;$i<count($arrIdhorar);$i++){
            $horXperiod->modificarPeriodHor($idhorario_periododocente,$usuario,$idperiodo,$idhorario,$idprofesor,$estado,$horas,$semanas,$max_faltas);
   //     }
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");

    }

}

?>