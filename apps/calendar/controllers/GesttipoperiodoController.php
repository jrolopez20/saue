<?php

class GesttipoperiodoController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->model = new DatTipoPeriododocenteModel();
    }

    public function gesttipoperiodoAction()
    {
        $this->render();
    }

    public function cargarTipoPeriodosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $tipoper = $this->model->cargarTipoPeridos($limit, $start);

        $cant = $this->model->countTipoPeridos();

        $result = array('cantidad' => $cant, 'datos' => $tipoper);

        echo json_encode($result);
    }

    public function cargarTipoPeriodosAAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $tipoper = $this->model->cargarTipoPeridosA($limit, $start);

        $result = array('cantidad' => 0, 'datos' => $tipoper);

        echo json_encode($result);
    }

    public function insertarTipoPeriodoAction()
    {
        $descripcion = $this->_request->getPost('tipoperiodo');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->model->insertarTipoPerido($descripcion, $usuario, $estado, $fecha);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarTipoPeriodoAction()
    {
        $idtipoperiododocente = $this->_request->getPost('idtipoperiododocente');
        $descripcion = $this->_request->getPost('tipoperiodo');
        $estado = $this->_request->getPost('estado');
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d');

        $this->model->modificarTipoPerido($idtipoperiododocente, $descripcion, $usuario, $estado, $fecha);

        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarTipoPeriodoAction()
    {
        $idtipo_per = json_decode($this->_request->getPost('idtipoperiododocentes'));

        $this->model->eliminarTipoPerido($idtipo_per);

        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }
}

?>