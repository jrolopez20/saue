<?php

/**
 * Clase controladora para gestionar las facturas.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class FacturaController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatFacturaModel();
    }

    public function facturaAction()
    {
        $this->render();
    }

    public function getFacturasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));
        $factura = new DatFactura();
        $r['success'] = true;
        $r['data'] = $this->model->getFacturas($limit, $start, $filtros);
        $r['total'] = $factura->GetTotalCount($filtros);

        echo json_encode($r);
    }


    public function getUltimoNumFacturaAction()
    {
        $r = ConfParametrosbasicos::GetByCondition('idestructura = ?', array($this->global->Estructura->idestructura), true);
        echo json_encode($r[0]);
    }

    public function getTerminosPagoAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');
        $model = new DatPedidoModel();
        $r = $model->getTerminosPago($limit, $start, $query);
        echo json_encode($r);
    }

    public function getClientesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');
        $model = new DatPedidoModel();
        $r = $model->getClientes($limit, $start, $query);
        echo json_encode($r);
    }

    public function getDetallePedidoAction()
    {
        $numero = $this->_request->getPost('numero');
        $pedido = new DatPedido();
        $r = $pedido->getPedidoProductos(null, $numero);
        echo json_encode($r[0]);
    }

    public function deleteFacturaAction()
    {
        $idfactura = $this->_request->getPost('idfactura');
        $model = new DatFacturaModel();
        $model->eliminar($idfactura);
        echo "{'success':true, 'codMsg':1, 'mensaje': 'Pedido eliminado satisfactoriamente'}";
    }

    public function getBancosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');
        $r['data'] = ZendExt_IoC::getInstance()->configuracion->getBancos($limit, $start, $query);
        echo json_encode($r);
    }

    public function saveFacturaAction()
    {
        $basicData = json_decode($this->_request->getPost('basicData'));
        $totalData = json_decode($this->_request->getPost('totalData'));
        $items = json_decode($this->_request->getPost('items'));
        $formapago = json_decode($this->_request->getPost('formapago'));
        $r = $this->model->guardarFactura($basicData, $totalData, $items, $formapago);
        echo $r;
    }

    public function getVendedoresAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filter = $this->_request->getPost('query');
        $objDomain = new DatVendedor();
        $r['data'] = $objDomain->listVendedor($limit, $start, $filter, $this->global->Estructura->idestructura);
        $r['total'] = $objDomain->countVendedor($filter, $this->global->Estructura->idestructura);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function imprimirFacturaAction()
    {
        $arrIdFactura = json_decode($this->_request->get('arrIdFactura'));

        $facturaPdf = new FacturaPDF($arrIdFactura);
        $facturaPdf->show();
    }

    public function loadDataContabilizarAction()
    {
        $arrIdFactura = json_decode($this->_request->get('arrIdFactura'));

        $r = $this->model->loadDataContabilizar($arrIdFactura);
        echo json_encode($r);
    }

    public function saveComprobanteAction()
    {
        echo $this->model->saveComprobante($this->_request->getPost());
    }

    public function anularFacturaAction()
    {
        $arrIdFactura = json_decode($this->_request->get('arrIdFactura'));

        //Cambia el estado de la factura a anulada
        $this->model->cambiaEstadoFactura($arrIdFactura, 2);
        echo "{'success':true, 'codMsg':1, 'mensaje': 'Factura(s) anulada(s) satisfactoriamente.'}";
    }


}
