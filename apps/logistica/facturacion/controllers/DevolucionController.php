<?php

/**
 * Clase controladora para gestionar las devolucions.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class DevolucionController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatDevolucionModel();
    }

    public function devolucionAction()
    {
        $this->render();
    }

    public function getDevolucionesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));
        $devolucion = new DatDevolucion();
        $r['success'] = true;
        $r['data'] = $this->model->getDevoluciones($limit, $start, $filtros);
        $r['total'] = $devolucion->GetTotalCount($filtros);

        echo json_encode($r);
    }

    public function getUltimoNumDevolucionAction()
    {
        $r = ConfParametrosbasicos::GetByCondition('idestructura = ?', array($this->global->Estructura->idestructura), true);
        echo json_encode($r[0]);
    }

    public function getFacturaAction()
    {
        $numFactura = $this->_request->getPost('numFactura');
        $facturaModel = new DatFacturaModel();

        $r = $facturaModel->getFacturaByNumero($numFactura);
        echo json_encode($r);
    }

    public function guardarDevolucionAction()
    {
        $numero = $this->_request->getPost('numero');
        $fecha = $this->_request->getPost('fecha');
        $idcliente = $this->_request->getPost('idcliente');
        $comentario = $this->_request->getPost('comentario');
        $importe = $this->_request->getPost('importe');
        $impuesto = $this->_request->getPost('impuesto');
        $idfactura = $this->_request->getPost('idfactura');
        $items = json_decode($this->_request->getPost('items'));

        $r = $this->model->guardarDevolucion($idfactura, $numero, $fecha, $idcliente, $comentario, $importe, $impuesto, $items);
        echo $r;
    }

    public function loadDataContabilizarAction()
    {
        try {
            $iddevolucion = $this->_request->get('iddevolucion');

            $r = $this->model->loadDataContabilizar($iddevolucion);
            echo json_encode($r);
        } catch (Exception $e) {
            echo "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    public function saveComprobanteAction()
    {
        echo $this->model->saveComprobante($this->_request->getPost());
    }

}