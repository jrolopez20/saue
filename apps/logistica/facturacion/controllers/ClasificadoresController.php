<?php

/**
 * Clase controladora para gestionar los parametros basicos del subsitema facturacion
 *
 * @author Team Delta
 * @package Facturacion
 * @copyright Ecotec
 * @version 1.0-0
 */
class ClasificadoresController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new ClasificadoresModel();
        parent::init();
    }

    /**
     * call this model action
     */
    public function loadAction()
    {
        echo json_encode($this->model->load($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function saveAction()
    {
        echo $this->model->save($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteAction()
    {
        echo $this->model->delete($this->_request->getPost());
    }
    /**
     * call this model action
     */
    public function changestateAction()
    {
        echo $this->model->changestate($this->_request->getPost());
    }
}