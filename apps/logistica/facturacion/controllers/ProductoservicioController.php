<?php

/**
 * Clase controladora para gestionar los productos y servicios de la entidad
 *
 * @author Team Delta
 * @package Facturacion
 * @copyright Ecotec
 * @version 1.0-0
 */
class ProductoServicioController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new PoductoServicioModel();
        parent::init();
    }

    public function getProductoServicioAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filter = $this->_request->getPost('filter');
        $activo = $this->_request->getPost('activo');

        $nomProductoServicio = new NomProductoservicio();
        $r['data'] = $nomProductoServicio->getProductoServicio($this->global->Estructura->idestructura, $limit, $start, $filter, $activo);
        $r['total'] = $nomProductoServicio->cantProductoServicio($this->global->Estructura->idestructura, $filter, $activo);

        echo json_encode($r);
    }

    public function loadAction()
    {
        echo json_encode($this->model->load($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function saveAction()
    {
        echo $this->model->save($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteAction()
    {
        echo $this->model->delete($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function changestateAction()
    {
        echo $this->model->changestate($this->_request->getPost());
    }

}