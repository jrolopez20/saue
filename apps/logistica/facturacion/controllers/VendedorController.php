<?php

/**
 * Clase controladora para gestionar los vendedores
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class VendedorController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        $this->model = new DatVendedorModel();
        parent::init();
    }

    public function readAction()
    {
        $limit = $this->_request->get('limit');
        $start = $this->_request->get('start');
        $filters = json_decode($this->_request->get('filtros'));

        $objDomain = new DatVendedor();

        $r['data'] = $objDomain->listVendedor($limit, $start, $filters, $this->global->Estructura->idestructura);
        $r['total'] = $objDomain->countVendedor($filters, $this->global->Estructura->idestructura);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function getPersonasAction()
    {
        $limit = $this->_request->get('limit');
        $start = $this->_request->get('start');
        $filter = $this->_request->get('query');
        $r['data'] = $this->integrator->rrhh->getPersonas($limit, $start, $filter);
        $r['total'] = $this->integrator->rrhh->countPersona($filter);
        $r['success'] = true;
        echo json_encode($r);
    }

    public function createAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->addVendedor($request->data);
        echo $r;
    }

    public function updateAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->updateVendedor($request->data);
        echo $r;
    }

    public function destroyAction()
    {
        $request = $this->parseRequest();
        $r = $this->model->deleteVendedor($request->data->idvendedor);
        echo $r;
    }

    private function parseRequest()
    {
        $raw = '';
        $httpContent = fopen('php://input', 'r');
        while ($kb = fread($httpContent, 1024)) {
            $raw .= $kb;
        }
        return json_decode(stripslashes($raw));
    }

}