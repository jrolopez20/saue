<?php

/**
 * Clase controladora para gestionar los parametros basicos del subsitema facturacion
 *
 * @author Team Delta
 * @package Facturacion
 * @copyright Ecotec
 * @version 1.0-0
 */
class ParametrosBasicosController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new ConfParametrosbasicosModel();
        parent::init();
    }


    /**
     * Carga los terminos de pago.
     */
    public function loadAction()
    {
        $data = $this->model->load();
        echo json_encode($data);
    }

    /**
     * Registra y Modifica los parametros basicos de la entidad.
     */
    public function saveAction()
    {
        $post = $this->_request->getPost();
        $r = $this->model->save($post);
        echo $r;
    }
}