<?php

/**
 * Clase controladora para gestionar los pedidos.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class PedidoController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatPedidoModel();
    }

    public function pedidoAction()
    {
        $this->render();
    }

    public function getPedidosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));
        $r['data'] =$this->model->getPedidos($limit, $start, $filtros);
        echo json_encode($r);
    }

    public function getDetallePedidoAction()
    {
        $idpedido = $this->_request->getPost('idpedido');
        echo json_encode($this->model->getPedido($idpedido));
    }

    public function getUltimoNumPedidoAction()
    {
        $r = ConfParametrosbasicos::GetByCondition('idestructura = ?', array($this->global->Estructura->idestructura), true);
        echo json_encode($r[0]);
    }

    public function guardarPedidoAction()
    {
        $idpedido = $this->_request->getPost('idpedido');
        $numero = $this->_request->getPost('numero');
        $fecha = $this->_request->getPost('fecha');
        $idcliente = $this->_request->getPost('idcliente');
        $idterminodepago = $this->_request->getPost('idterminodepago');
        $flete = $this->_request->getPost('flete');
        $items = json_decode($this->_request->getPost('items'));

        $r = $this->model->guardarPedido($numero, $fecha, $idcliente, $idterminodepago, $items, $flete, $idpedido);
        echo $r;
    }

    public function deletePedidoAction()
    {
        $idpedido = $this->_request->getPost('idpedido');
        $r = $this->model->eliminarPedido($idpedido);
        echo $r;
    }

    public function imprimirPedidoAction()
    {
        $arrIdPedido = json_decode($this->_request->get('arrIdPedido'));

        $pedidoPdf = new PedidoPDF($arrIdPedido);
        $pedidoPdf->show();
    }

    public function getTerminosPagoAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');
        $model = new DatPedidoModel();
        $r = $model->getTerminosPago($limit, $start, $query);
        echo json_encode($r);
    }

    public function getClientesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');
        $model = new DatPedidoModel();
        $r = $model->getClientes($limit, $start, $query);
        echo json_encode($r);
    }

}
