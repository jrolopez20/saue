<?php

/**
 * Clase controladora para gestionar las facturas.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class StickerController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
    }

    public function stickerAction()
    {
        $this->view->empresa = $this->integrator->configuracion->getDatosEmpresa($this->gloabl->Estructura->idestructura);
        $this->render();
    }

    public function getClientesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $query = $this->_request->getPost('query');
        $model = new DatPedidoModel();
        $r = $model->getClientes($limit, $start, $query);
        echo json_encode($r);
    }

}