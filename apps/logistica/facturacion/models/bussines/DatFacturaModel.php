<?php

class DatFacturaModel extends ZendExt_Model
{
    private $domain;

    public function __construct()
    {
        parent::ZendExt_Model();
        $this->domain = new DatFactura();
    }

    /**
     * Elimina una factura dado su identificador
     * @param $idfactura
     * @throws Doctrine_Exception
     * @throws Exception
     * @throws ZendExt_Exception
     */
    public function eliminarFactura($idfactura)
    {
        try {
            $q = new Doctrine_Query();
            $q->delete()
                ->from('DatFactura')
                ->where('idfactura=?', $idfactura)
                ->execute();
            return "{'success':true, 'codMsg':1, 'mensaje': 'Factura eliminada satisfactoriamente'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) { //Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'El factura se encuentra en uso.'}";
            } else {
                return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
            }
        }
    }

    public function guardarFactura($basicData, $totalData, $items, $formapago)
    {
        try {
            $obj = new DatFactura();

            //Datos de la factura
            $obj->numero = $basicData->numero;
            $obj->fecha = $basicData->fecha;
            $obj->idcliente = $basicData->idcliente;
            $obj->idterminodepago = $basicData->idterminodepago;
            $obj->idvendedor = $basicData->idvendedor;
            $obj->flete = empty($basicData->flete) ? 0.00 : $basicData->flete;
            $obj->idusuario = $this->global->Perfil->idusuario;
            $obj->importe = $totalData->importe;
            $obj->impuesto = $totalData->impuesto;

            //Datos de los productos
            foreach ($items as $k => $item) {
                $obj->DatFacturaProducto[$k]->idproductoservicio = $item->idproductoservicio;
                $obj->DatFacturaProducto[$k]->cantidad = $item->cantidad;
                $obj->DatFacturaProducto[$k]->descuento = $item->descuento;
            }

            //Datos de la forma de pago
            if ($formapago->type == 'efectivo') {
                $obj->DatFpagoefectivo[0]->efectivo = $formapago->efectivo;
                $obj->DatFpagoefectivo[0]->cambio = $formapago->cambio;
            } else if ($formapago->type == 'cheque') {
                $obj->DatFpagocheque[0]->numerocheque = $formapago->numerocheque;
                $obj->DatFpagocheque[0]->fechacheque = $formapago->fechacheque;
                $obj->DatFpagocheque[0]->idbanco = $formapago->idbanco;
                $obj->DatFpagocheque[0]->postfechado = $formapago->postfechado;
                $obj->DatFpagocheque[0]->telefono = $formapago->telefono;
            } else if ($formapago->type == 'tarjeta') {
                foreach ($formapago->tarjetas as $k => $tarjeta) {
                    $obj->DatFpagotarjeta[$k]->numerotarjeta = $tarjeta->numerotarjeta;
                    $obj->DatFpagotarjeta[$k]->monto = $tarjeta->monto;
                }
            }

            $obj->save();
            $basicParam = new ConfParametrosbasicosModel();
            $basicParam->setUltimonumfactura($this->global->Estructura->idestructura, $basicData->numero + 1);
            return "{'success':true, 'codMsg':1, 'mensaje': 'Factura guardada satisfactoriamente'}";
        } catch (Doctrine_Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }

    }

    public function getFacturas($limit, $start, $filtros)
    {
        $facturas = $this->domain->GetAll($limit, $start, $filtros);
        foreach ($facturas as &$factura) {
            $factura['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($factura['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($factura['idcliente']);
            $factura['cliente'] = $cliente['nombre'];
        }
        return $facturas;
    }

    public function getFacturaByNumero($numero)
    {
        $factura = $this->domain->getFacturaByNumero($numero);
        if (!empty($factura)) {
            $factura = $factura[0];
            $factura['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($factura['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($factura['idcliente']);
            $factura['cliente'] = $cliente['nombre'];
        }
        return $factura;
    }

    public function getFacturaImpresion($arrIdFactura)
    {
        $facturas = $this->domain->getFacturaProductos($arrIdFactura);

        foreach ($facturas as &$factura) {
            $factura['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($factura['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($factura['idcliente']);
            $factura['cliente'] = $cliente['nombre'];
            $factura['ruc'] = $cliente['codigo'];
            $factura['provincia'] = $cliente['provincia'];
            $factura['direccion'] = $cliente['direccion'];
            $factura['telefono'] = $cliente['telefono'];
            $factura['email'] = $cliente['email'];
        }
        return $facturas;
    }

    public function getFacturaByArrId($arrIdFactura)
    {
        $facturas = $this->domain->getFacturaProductos($arrIdFactura);

        foreach ($facturas as &$factura) {
            $factura['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($factura['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($factura['idcliente']);
            $factura['cliente'] = $cliente['nombre'];
            $factura['idcuentacobrar'] = $cliente['idcuentacobrar'];
            $factura['idcuentapagar'] = $cliente['idcuentapagar'];

        }
        return $facturas;
    }

    /**
     * Carga los datos para contabilizar asi como los pases del comprobante
     * @param $arrIdFactura
     * @return array
     */
    public function loadDataContabilizar($arrIdFactura)
    {
        $rpse = array();
        $periodoActual = $this->integrator->contabilidad->getPeriodoSubsistema($this->global->Estructura->idestructura, 7);
        $dataFecha = $this->integrator->contabilidad->getFechaSubsistema($this->global->Estructura->idestructura, 7);

        $objBasicParam = new ConfParametrosbasicosModel();
        $basicParams = $objBasicParam->load();

        $rpse['referencia'] = 'Contabilizando facturas (';
        $rpse['detalle'] = 'Contabilizando facturas (';
        $rpse['fechaemision'] = $dataFecha[0]['fecha'];
        $rpse['idsubsistema'] = 7;
        $rpse['idperiodo'] = $periodoActual[0]['idperiodo'];

        $facturas = $this->getFacturaByArrId($arrIdFactura);

        $rpse['pases'] = array();

        foreach ($facturas as $k => $factura) {
            $rpse['referencia'] .= ($k == 0 ? '' : ', ') . $factura['numero'];
            $rpse['detalle'] .= ($k == 0 ? '' : ', ') . $factura['numero'];

            //Pase por el debito de la cuenta del usuario
            $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($factura['idcuentacobrar']));
            $paseDebito['readOnly'] = true;
            $paseDebito['idpase'] = 0;
            $paseDebito['idcuenta'] = $factura['idcuentacobrar'];
            $paseDebito['codigodocumento'] = $factura['numero'];
            $paseDebito['concatcta'] = $dataCta[0]['concat'];
            $paseDebito['denominacion'] = $dataCta[0]['denominacion'];
            $paseDebito['debito'] = $factura['importe'];
            $paseDebito['credito'] = 0.00;

            $rpse['pases'][] = $paseDebito;


            //Pase por el credito para el Flete
            if (!empty($basicParams['data']['idcuentaflete'])) {
                if (!empty($factura['flete'])) {
                    $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($basicParams['data']['idcuentaflete']));
                    $paseFlete['readOnly'] = true;
                    $paseFlete['idpase'] = 0;
                    $paseFlete['idcuenta'] = $basicParams['data']['idcuentaflete'];
                    $paseFlete['codigodocumento'] = $factura['numero'];
                    $paseFlete['concatcta'] = $dataCta[0]['concat'];
                    $paseFlete['denominacion'] = $dataCta[0]['denominacion'];
                    $paseFlete['debito'] = 0.00;
                    $paseFlete['credito'] = $factura['flete'];

                    $rpse['pases'][] = $paseFlete;
                }
            }

            //Pase por el credito para el IVA
            if (!empty($basicParams['data']['idcuentaiva'])) {
                if (!empty($factura['impuesto'])) {
                    $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($basicParams['data']['idcuentaiva']));
                    $paseIva['readOnly'] = true;
                    $paseIva['idpase'] = 0;
                    $paseIva['idcuenta'] = $basicParams['data']['idcuentaiva'];
                    $paseIva['codigodocumento'] = $factura['numero'];
                    $paseIva['concatcta'] = $dataCta[0]['concat'];
                    $paseIva['denominacion'] = $dataCta[0]['denominacion'];
                    $paseIva['debito'] = 0.00;
                    $paseIva['credito'] = $factura['impuesto'];

                    $rpse['pases'][] = $paseIva;
                }
            }

            //Pase por el credito para la cuenta de venta
            if (!empty($basicParams['data']['idcuentaventa'])) {
                if (!empty($factura['impuesto'])) {
                    $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($basicParams['data']['idcuentaventa']));
                    $credito = $factura['importe'];

                    if ($factura['flete'] > 0 && !empty($basicParams['data']['idcuentaflete'])) {
                        $credito -= $factura['flete'];
                    }
                    if ($factura['impuesto'] > 0) {
                        $credito -= $factura['impuesto'];
                    }

                    $paseVenta['readOnly'] = true;
                    $paseVenta['idpase'] = 0;
                    $paseVenta['idcuenta'] = $basicParams['data']['idcuentaventa'];
                    $paseVenta['codigodocumento'] = $factura['numero'];
                    $paseVenta['concatcta'] = $dataCta[0]['concat'];
                    $paseVenta['denominacion'] = $dataCta[0]['denominacion'];
                    $paseVenta['debito'] = 0.00;
                    $paseVenta['credito'] = $credito;

                    $rpse['pases'][] = $paseVenta;
                }
            }
        }

        $rpse['referencia'] .= ')';
        $rpse['detalle'] .= ')';

        return $rpse;
    }

    /**
     * Adiciona los comprobantes de facturacion.
     * @param Array $argparams
     * @return boolean true if ocurred, false if failure
     */
    public function saveComprobante($argparams)
    {
        try {
            $periodoActual = $this->integrator->contabilidad->getPeriodoSubsistema($this->global->Estructura->idestructura, 7);
            $arrIdFactura = json_decode($argparams['arrIdFactura']);

            $facturas = $this->getFacturaByArrId($arrIdFactura);

            $save = $this->integrator->contabilidad->saveComprobante($argparams);

            $id = explode("'id':", $save);
            $num = explode("'nocomp':", $save);
            $numco = array();
            if (count($id) > 1) {
                $id = substr($id[1], 0, -1);
            }
            if (count($num) > 1) {
                $num = substr($num[1], 0, -1);
                $numco = explode(',', $num);
            }
            if (is_numeric($id)) {
                foreach ($facturas as $factura) {
                    $params['numero'] = $factura['numero'];
                    $params['fechaemision'] = $factura['fecha'];
                    $params['importe'] = $factura['importe'];
                    $params['idsubsistema'] = 7; //Subsistema de facturacion
                    $params['estadodocumento'] = 4;
                    $params['estado'] = 1;
                    $params['idterminodepago'] = $factura['idterminodepago'];
                    $params['tipo'] = 1; //Tipo cuenta por cobrar
                    $params['idtipodocumento'] = 1; //Tipo de documento FACTURA
                    $params['idestructura'] = $this->global->Estructura->idestructura;
                    $params['idperiodo'] = $periodoActual[0]['idperiodo'];
                    $params['idclientesproveedores'] = $factura['idcliente'];
                    $params['idcomprobante'] = $id;
                    $params['nocomprobante'] = $numco[0];

                    //Registra la cuentas por cobrar
                    $this->integrator->contabilidad->adicionarObligacion($params);
                }

                //Actualiza el estado de la factura a Contabilizado
                $this->cambiaEstadoFactura($arrIdFactura, 1);

                return "{'success':true, 'codMsg':1,'mensaje':'Factura contabilizada satisfactoriamente'}";
            } else {
                return "{'success':false, 'codMsg':3,'mensaje':'Ha ocurrido un error al registrar el comprobante'}";
            }
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3,'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    /**
     * Actualiza el estado de la factura
     * @param array $arrIdFactura
     * @param $estado
     * @throws ZendExt_Exception
     */
    public function cambiaEstadoFactura(array $arrIdFactura, $estado)
    {
        try {
            if (!empty($arrIdFactura)) {
                $q = Doctrine_Query::create()
                    ->update('DatFactura')
                    ->set('estado', '?', $estado)
                    ->whereIn('idfactura', $arrIdFactura);
                $q->execute();
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}