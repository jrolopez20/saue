<?php

class ClasificadoresModel extends ZendExt_Model
{

    public function __construct()
    {
        parent::ZendExt_Model();
    }

    /**
     * Carga los clasificadores de la entidad.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function load($post)
    {
        $estructura = $this->global->Estructura->idestructura;

        $domain = new NomClasificadorprodserv();

        $conditions = array('idestructura = ?');
        $params = array($estructura);

        if (!empty($post['node']) && is_numeric($post['node'])) {
            $conditions[] = 'idpadre = ? AND idclasificador <> idpadre';
            $params[] = $post['node'];
        } else {
            $conditions[] = 'idclasificador = idpadre';
        }

        $results = $domain->GetByCondition(implode(' AND ', $conditions), $params, TRUE);
        if (count($results)) {
            foreach ($results as &$clasif) {
                $clasif['icon'] = ($clasif['tipo'] == 1) ? '/logistica/facturacion/views/images/product.png'
                    : '/logistica/facturacion/views/images/service.png';
            }
        }

        return $results;
//        return $domain->GetByCondition(implode(' AND ', $conditions), $params, TRUE);
    }

    /**
     * Inserta y modifica los clasificadores de la entidad.
     *
     * @param array $post Arreglo con los datos de la clasificacion
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function save($post)
    {
        $estructura = $this->global->Estructura->idestructura;

        /* Si se esta modificando */
        $objeto = (!empty($post['idclasificador']) && is_numeric($post['idclasificador'])) ?
            NomClasificadorprodserv::GetById($post['idclasificador']) : new NomClasificadorprodserv();

        $objeto->idpadre = !empty($post['idpadre']) ? $post['idpadre'] : NULL;
        $objeto->concat = $post['concat'];
        $objeto->denominacion = $post['denominacion'];
        $objeto->codigo = $post['codigo'];
        $objeto->tipo = $post['tipo'];
        $objeto->idestructura = $estructura;
        $objeto->activo = !empty($post['activo']) ? $post['activo'] : $objeto->activo;
        $objeto->descripcion = !empty($post['descripcion']) ? $post['descripcion'] : NULL;
        $objeto->nivel = !empty($post['nivel']) ? $post['nivel'] : 1;

        $idclasificador = $objeto->persist();

        $flag = is_numeric($idclasificador) ? 'true' : 'false';
        $code = is_numeric($idclasificador) ? '1' : '3';
        $msg = is_numeric($idclasificador) ?
            (!empty($post['idclasificador']) ? 'perfil.etiquetas.msgModOK' : 'perfil.etiquetas.msgAddOK') :
            (!empty($post['idclasificador']) ? 'perfil.etiquetas.msgModFail' : 'perfil.etiquetas.msgAddFail');
        $plus = empty($post['idclasificador']) ? ', idgenerado:' . $idclasificador : '';

        return "{'success':$flag, 'codMsg': $code, 'mensaje': $msg $plus}";
    }

    /**
     * Activa | Desactiva uno o mas clasificadores de la entidad.
     *
     * @param array $post Arreglo con los ids
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function changestate($post)
    {
        $arrayids = json_decode($post['ids']);
        $response = array('code' => 1, 'errors' => array());

        $result = NomClasificadorprodserv::ChangeActive($arrayids);

        return json_encode($response);
    }

    /**
     * Elimina uno o mas clasificadores de la entidad.
     *
     * @param array $post Arreglo con los ids a eliminar
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function delete($post)
    {
        $arrayids = json_decode($post['ids']);
        $response = array('code' => 1, 'errors' => array());

        foreach ($arrayids as $id) {
//            $clasificacion = NomClasificadorprodserv::GetById($id);

            if (is_numeric($id)) {
                $responseAux = array('code' => 1, 'errors' => array());
                $this->deleteCascade($id, new NomClasificadorprodserv(), $responseAux);
                if (count($responseAux['errors'])) {
                    $response['code'] = $responseAux['code'];
                    $response['errors'] = array_merge($response['errors'], $responseAux['errors']);
                }
            }
        }

        return json_encode($response);
    }

    /**
     * Funcion auxiliar utilizada para eliminar un clasificador y toda su descendencia.
     *
     * @param int $idclasificador Identidicador del clasificador padre a eliminar
     * @param NomClasificadorprodserv $domainModel objeto de tipo modelo de dominio
     * @param array $response Arreglo que almacena y retorna (por referencia) el resultado de eliminar cada clasificador de la rama
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function deleteCascade($idclasificador, $domainModel, &$response)
    {
        $children = $domainModel->GetByCondition('idpadre = ? AND idpadre <> idclasificador', array($idclasificador), TRIE);

        if (!$children || empty($children)) {
            $this->erase($idclasificador, $domainModel, $response);
        } else {
            foreach ($children as $child) {
                $this->deleteCascade($child['idclasificador'], $domainModel, $response);
            }
            if (!count($response['errors'])) { // si todos los hijos fueron eliminados
                $this->erase($idclasificador, $domainModel, $response);
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar utilizada para eliminar un unico clasificador.
     *
     * @param int $idclasificador Identidicador del clasificador a eliminar
     * @param NomClasificadorprodserv $domainModel objeto de tipo modelo de dominio
     * @param array $response Arreglo que almacena y retorna (por referencia) el resultado de eliminar cada clasificador de la rama
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function erase($idclasificador, $domainModel, &$response)
    {
        $clasificador = NomClasificadorprodserv::GetById($idclasificador);
        if ($clasificador) { // verificando que existe el clasificador en la BD
            $result = NomClasificadorprodserv::EraseSQL($idclasificador);
            if ($result === -1) {
                $response['code'] = -3;
                $response['errors'][] = $idclasificador;
            }
        }

        return $response;
    }

}

