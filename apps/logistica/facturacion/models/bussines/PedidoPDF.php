<?php

class PedidoPDF extends ZendExt_Model
{

    private $pdf;

    private $pedidos;
    private $empresa;

    public function __construct($arrIdPedido)
    {
        parent::ZendExt_Model();

//        $this->idpedido = $idpedido;
        $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('Sistema de facturación');
        $this->pdf->SetTitle('PEDIDO');
        $this->pdf->SetSubject('Nota de pedido');
        $this->pdf->SetKeywords('pedido');
        $this->pdf->setPrintHeader(false);
//        $this->pdf->setPrintFooter(true);

        $this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', '9'));
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //Ejecuta la carga de los datos para poder exportar el pedido
        $this->loadData($arrIdPedido);

        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->pdf->SetMargins(5, 5, 5);

        // set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


        //Ejecuta los metodos que conforman el documento
        $this->generateDoc();
    }

    private function loadData($arrIdPedido)
    {
        //Carga los datos de la empresa
        $this->empresa = $this->integrator->configuracion->getDatosEmpresa($this->global->Estructura->idestructura);

        //Carga los datos del pedido
        $objDomain = new DatPedidoModel();
        $this->pedidos = $objDomain->getPedidoImpresion($arrIdPedido);
    }

    /**
     * Muestra el pedido en formato listo para impresion
     */
    public function show()
    {
        // Close and output PDF document
        ob_clean();
        $this->pdf->Output("Pedido.pdf", 'I');
    }

    private function generateDoc()
    {
        try {

            foreach ($this->pedidos as $pedido) {
//                print('<pre>');
//                print_r($pedido);
//                die;
                $this->pdf->AddPage();

                $temp = explode('data:image/jpg;base64,', $this->empresa['logo']);
                $imgdata = base64_decode($temp[1]);

                // The '@' character is used to indicate that follows an image data stream and not an image file name
                $this->pdf->Image('@' . $imgdata, 158, 4, 50, 35, '', '', '', false, 300, '', false, false, 0, 'RTL');

                $this->pdf->SetFont('helvetica', 'B', 11);
                $this->pdf->Cell(null, null, strtoupper($this->empresa['nombre']), 0, 1);
                $this->pdf->Cell(null, null, strtoupper($this->empresa['razonsocial']), 0, 1);
                $this->pdf->ln(2);
                $this->pdf->SetFont('helvetica', null, 9);
                $this->pdf->MultiCell(null, null, strtoupper($this->empresa['direccion']), 0, 'L', false, 1);

                if ($this->empresa['correo']) {
                    $this->pdf->Cell(null, null, $this->empresa['correo'], 0, 1);
                }

                if ($this->empresa['telefono']) {
                    $this->pdf->Cell(null, null, strtoupper($this->empresa['telefono']), 0, 1);
                }

                $this->pdf->ln(5);
                $this->pdf->Cell(null, null, "NOTA DE PEDIDO # {$pedido['numero']}", 0, 0);

                $this->pdf->Cell(null, null, "FECHA: {$pedido['fecha']}", 0, 1, 'R');
                $this->pdf->ln(2);
                $this->pdf->writeHTML('<hr/>');
                $this->pdf->Cell(15, null, 'Cliente:', 0, 0, 'R');
                $this->pdf->Cell(null, null, $pedido['cliente'], 0, 1);
                $this->pdf->Cell(15, null, 'Dirección:', 0, 0, 'R');
                $this->pdf->Cell(null, null, $pedido['direccion'], 0, 1);
                if ($pedido['telefono']) {
                    $this->pdf->Cell(15, null, 'Teléfonos:', 0, 0, 'R');
                    $this->pdf->Cell(null, null, $pedido['telefono'], 0, 1);
                }
                if ($pedido['email']) {
                    $this->pdf->Cell(15, null, 'Correo:', 0, 0, 'R');
                    $this->pdf->Cell(null, null, $pedido['email'], 0, 1);
                }
                $this->pdf->ln(5);

                //Cabecera de la tabla
                $this->pdf->Cell(20, 6, 'Código', 'TB', 0);
                $this->pdf->Cell(85, 6, 'Denominación', 'TB', 0);
                $this->pdf->Cell(17, 6, 'Cantidad', 'TB', 0, 'C');
                $this->pdf->Cell(20, 6, 'Precio', 'TB', 0, 'C');
                $this->pdf->Cell(20, 6, 'Descuento', 'TB', 0, 'C');
                $this->pdf->Cell(15, 6, 'IVA', 'TB', 0, 'C');
                $this->pdf->Cell(23, 6, 'Importe', 'TB', 1, 'C');

//                subtotal += Ext.util.Format.round(record.get('cantidad') * record.get('preciounitario'), 2);
                $subtotal = 0;
                $descuento = 0;
                $impuesto = 0;
                $importe = 0;

                //Verifica si el pedido lleva flete y lo adiciona como un producto mas
                if (!empty($pedido['flete']) && $pedido['flete'] > 0) {
                    $flete = array(
                        'cantidad' => 1,
                        'descuento' => null,
                        'NomProductoservicio' => array(
                            'codigo' => 'Flete',
                            'denominacion' => 'Flete',
                            'preciounitario' => $pedido['flete'],
                            'iva' => null
                        )
                    );

                    array_unshift($pedido['DatPedidoProducto'], $flete);
                }



                //Contenido de la tabla
                foreach ($pedido['DatPedidoProducto'] as $item) {
                    $this->pdf->Cell(20, 6, $item['NomProductoservicio']['codigo']);
                    $this->pdf->Cell(85, 6, $item['NomProductoservicio']['denominacion']);
                    $this->pdf->Cell(17, 6, $item['cantidad'], 0, 0, 'R');
                    $this->pdf->Cell(20, 6, $item['NomProductoservicio']['preciounitario'], 0, 0, 'R');
                    $this->pdf->Cell(20, 6, $item['descuento'], 0, 0, 'R');
                    $this->pdf->Cell(15, 6, $item['NomProductoservicio']['iva'], 0, 0, 'R');
                    $importeProducto = $this->calculaImporteProducto(
                        $item['cantidad'], $item['NomProductoservicio']['preciounitario'], $item['descuento'],
                        $item['NomProductoservicio']['iva']
                    );
                    $this->pdf->Cell(23, 6, $importeProducto, 0, 1, 'R');

                    $subtotal += round($item['cantidad'] * $item['NomProductoservicio']['preciounitario'], 2);

                    if ($item['descuento'] > 0) {
                        $descuento += round($item['cantidad'] * $item['NomProductoservicio']['preciounitario'] * $item['descuento'] / 100, 2);
                    }

                    if ($item['NomProductoservicio']['iva'] > 0) {
                        $impuesto += round($item['cantidad'] * $item['NomProductoservicio']['preciounitario'] * $item['NomProductoservicio']['iva'] / 100, 2);
                    }

                    $importe += $importeProducto;
                }

                $this->pdf->writeHTML('<hr/>');


                $this->pdf->Cell(175, null, 'SUBTOTAL:', 0, 0, 'R');
                $this->pdf->Cell(null, null, $subtotal, 0, 1, 'R');
                $this->pdf->Cell(175, null, 'DESCUENTO:', 0, 0, 'R');
                $this->pdf->Cell(null, null, $descuento, 0, 1, 'R');
                $this->pdf->Cell(175, null, 'IVA 12%:', 0, 0, 'R');
                $this->pdf->Cell(null, null, $impuesto, 0, 1, 'R');
                $this->pdf->Cell(175, null, 'TOTAL:', 0, 0, 'R');
                $this->pdf->Cell(null, null, $importe, 0, 1, 'R');

                $importeLetras = $this->integrator->comun->valorEnLetras($importe, '');
                $this->pdf->Cell(null, null, 'Son: ' . $importeLetras, 0, 1);
                $this->pdf->ln(2);
                $this->pdf->Cell(null, null, 'Observaciones:', 0, 1);
                $this->pdf->Cell(null, 6, 'ESTOS PRECIOS ESTAN SUJETOS A CAMBIOS', 'B', 1);

                $html = <<<EOF
<table cellspacing="5" style="width:100%;text-align:center;">
    <tr>
        <td>
            <p>Elaborado por</p>
            --------------------
            <br/>
        </td>
        <td>
            <p>Aprobado por</p>
            --------------------
            <br/>
        </td>
        <td>
            <p>Recibido por</p>
            --------------------
            <br/>
        </td>
    </tr>
</table>
EOF;

                $this->pdf->writeHTML($html);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Calcula el importe para un producto
     * @param $cantidad
     * @param $precio
     * @param $descuento
     * @param $iva
     * @return float Importe
     */
    private function calculaImporteProducto($cantidad, $precio, $descuento, $iva)
    {
        $porcientoDescuento = round(($precio * $descuento) / 100, 2);
        $porcientoIva = round(($precio * $iva) / 100, 2);
        return round(($precio - $porcientoDescuento + $porcientoIva) * $cantidad, 2);
    }

} 