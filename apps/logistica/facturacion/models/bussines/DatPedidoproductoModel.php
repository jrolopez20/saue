<?php

class DatPedidoproductoModel extends ZendExt_Model
{

    public function __construct()
    {
        parent::ZendExt_Model();
    }

    public function eliminarProductos($idpedido)
    {
        try {
            $query = new Doctrine_Query();
            $rows_afected = $query->delete()
                ->from('DatPedidoProducto')
                ->where('idpedido = ?', $idpedido)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

}