<?php

class PoductoServicioModel extends ZendExt_Model
{

    public function __construct()
    {
        parent::ZendExt_Model();
    }

    /**
     * Carga los productos y servicios de la entidad.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function load($post)
    {
//        $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í", "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
        $estructura = $this->global->Estructura->idestructura;

        $conditions = array("idestructura = ?");
        $params = array("$estructura");

        if (!empty($post['idclasificador'])) {
            $conditions[] = 'idclasificador = ?';
            $params[] = $post['idclasificador'];
        }


        /* Obtener los productos o servicios */
        $data = NomProductoservicio::GetByCondition(implode(" AND ", $conditions), $params, TRUE);

        $paging = array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );

        return $paging;
    }

    /**
     * Inserta y modifica los productos y/o servicios de la entidad.
     *
     * @param array $post Arreglo con los datos del producto y/o sevicio
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function save($post)
    {
        $estructura = $this->global->Estructura->idestructura;

        /* VERIFICACION PARA VER SI EXISTE EL ELEMENTO */
        $condExist = array('lower(codigo) = ? OR lower(denominacion) = ?');
        $params =  array(strtolower($post['codigo']), strtolower($post['denominacion']));

        if(!empty($post['codigobarra'])){
            $condExist[] = 'lower(codigobarra) = ?';
            $params[] = strtolower($post['codigobarra']);
        }

        $condExist = '('.implode(' OR ', $condExist).')';

        // si se esta modificando se busca cualquier otro que no sea el mismo
        if(!empty($post['idproductoservicio'])){
            $condExist .= ' AND idproductoservicio <> ?';
            $params[] = $post['idproductoservicio'];
        }

        $exist = NomProductoservicio::GetByCondition($condExist, $params, TRUE);

        if(count($exist)){
            return "{'success':false, 'codMsg': 3, 'mensaje': perfil.etiquetas.msgAlreadyExist}";
        }
        /* FIN VERIFICACION */

        /* Si se esta modificando */
        $objeto = (!empty($post['idproductoservicio']) && is_numeric($post['idproductoservicio'])) ?
            NomProductoservicio::GetById($post['idproductoservicio']) : new NomProductoservicio();

        $objeto->codigo = $post['codigo'];
        $objeto->denominacion = $post['denominacion'];
        $objeto->foto = !empty($post['foto']) ? $post['foto'] : NULL;
        $objeto->idunidadmedida = !empty($post['idunidadmedida']) ? $post['idunidadmedida'] : NULL;
        $objeto->stock = $post['stock'];
        $objeto->preciounitario = $post['preciounitario'];
        $objeto->iva = !empty($post['iva']) ? $post['iva'] : NULL;
        $objeto->codigobarra = !empty($post['codigobarra']) ? $post['codigobarra'] : NULL;
        $objeto->activo = !empty($post['activo']) ? $post['activo'] : $objeto->activo;
        $objeto->tipo = $post['tipo'];
        $objeto->idestructura = $estructura;
        $objeto->idclasificador = $post['idclasificador'];

        $idproductoservicio = $objeto->persist();

        $flag = is_numeric($idproductoservicio) ? 'true' : 'false';
        $code = is_numeric($idproductoservicio) ? '1' : '3';
        $msg = is_numeric($idproductoservicio) ?
            (!empty($post['idproductoservicio']) ? 'perfil.etiquetas.msgModOK' : 'perfil.etiquetas.msgAddOK') :
            (!empty($post['idproductoservicio']) ? 'perfil.etiquetas.msgModFail' : 'perfil.etiquetas.msgAddFail');
        $plus = empty($post['idproductoservicio']) ? ', idgenerado:' . $idproductoservicio : '';

        return "{'success':$flag, 'codMsg': $code, 'mensaje': $msg $plus}";
    }

    /**
     * Activa | Desactiva uno o mas producto o servicio.
     *
     * @param array $post Arreglo con los ids
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function changestate($post)
    {
        $arrayids = json_decode($post['ids']);
        $response = array('code' => 1, 'errors' => array());

        $result = NomProductoservicio::ChangeActive($arrayids);

        return json_encode($response);
    }

    /**
     * Elimina uno o mas productos o servicios.
     *
     * @param array $post Arreglo con los ids de los productos y servicios a eliminar
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function delete($post)
    {
        $arrayids = json_decode($post['ids']);
        $response = array('code' => 1, 'errors' => array());

        foreach ($arrayids as $id) {

            if (is_numeric($id)) {
                $prodserv = NomProductoservicio::GetById($id);

                if ($prodserv) { // verificando que existe el producto en la BD
                    $result = NomProductoservicio::EraseSQL($id);

                    if ($result === -1) {
                        $response['code'] = -3;
                        $response['errors'][] = $id;
                    }
                }
            }
        }

        return json_encode($response);
    }
}

