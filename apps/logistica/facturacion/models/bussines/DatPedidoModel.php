<?php

class DatPedidoModel extends ZendExt_Model
{
    private $domain;

    public function __construct()
    {
        parent::ZendExt_Model();
        $this->domain = new DatPedido();
    }

    /**
     * Elimina un pedido dado su identificador
     * @param $idpedido
     * @throws Doctrine_Exception
     * @throws Exception
     * @throws ZendExt_Exception
     */
    public function eliminarPedido($idpedido)
    {
        try {
            $q = new Doctrine_Query();
            $q->delete()
                ->from('DatPedido')
                ->where('idpedido=?', $idpedido)
                ->execute();
            return "{'success':true, 'codMsg':1, 'mensaje': 'Pedido eliminado satisfactoriamente'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) { //Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'El pedido se encuentra en uso.'}";
            } else {
                return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
            }
        }
    }

    public function getTerminosPago($limit, $start, $query)
    {
        $r['data'] = $this->integrator->configuracion->getTerminosdepago(
            $this->global->Estructura->idestructura,
            $start, $limit, $query);
        return $r;
    }

    public function getClientes($limit, $start, $query)
    {
        $r['data'] = $this->integrator->configuracion->getClientes(
            $this->global->Estructura->idestructura,
            $start, $limit, $query);
        return $r;
    }

    public function guardarPedido($numero, $fecha, $idcliente, $idterminodepago, $items, $flete, $idpedido = null)
    {
        try {
            $obj = new DatPedido();
            if ($idpedido) {
                $obj = $obj->getById($idpedido);

                //Elimina los productos que ya existan
                $objProductos = new DatPedidoproductoModel();
                $objProductos->eliminarProductos($idpedido);
            }

            $obj->numero = $numero;
            $obj->fecha = $fecha;
            $obj->idusuario = $this->global->Perfil->idusuario;
            $obj->idcliente = $idcliente;
            $obj->idterminodepago = $idterminodepago;
            $obj->flete = empty($flete) ? 0.00 : $flete;

            foreach ($items as $k => $item) {
                $obj->DatPedidoProducto[$k]->idproductoservicio = $item->idproductoservicio;
                $obj->DatPedidoProducto[$k]->cantidad = $item->cantidad;
                $obj->DatPedidoProducto[$k]->descuento = $item->descuento;
            }
            $obj->save();

            $basicParam = new ConfParametrosbasicosModel();
            $basicParam->setUltimonumpedido($this->global->Estructura->idestructura, $numero + 1);
            return "{'success':true, 'codMsg':1, 'mensaje': 'Pedido guardado satisfactoriamente'}";
        } catch (Doctrine_Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }

    }

    public function getPedidos($limit, $start, $filtros)
    {
        $pedidos = $this->domain->GetAll($limit, $start, $filtros);
        foreach ($pedidos as &$pedido) {
            $pedido['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($pedido['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($pedido['idcliente']);
            $pedido['cliente'] = $cliente['nombre'];
        }
        return $pedidos;
    }

    public function getPedido($idpedido)
    {
        $r = $this->domain->getPedidoProductos($idpedido);
        return $r[0];
    }

    public function getPedidoImpresion($arrIdPedido)
    {
        $pedidos = $this->domain->getPedidoProductos($arrIdPedido);

        foreach ($pedidos as &$pedido) {
            $pedido['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($pedido['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($pedido['idcliente']);
            $pedido['cliente'] = $cliente['nombre'];
            $pedido['provincia'] = $cliente['provincia'];
            $pedido['direccion'] = $cliente['direccion'];
            $pedido['telefono'] = $cliente['telefono'];
            $pedido['email'] = $cliente['email'];
        }
        return $pedidos;
    }
}