<?php

class ConfParametrosbasicosModel extends ZendExt_Model
{

    public function __construct()
    {
        parent::ZendExt_Model();
    }

    /**
     * Carga los parametros basicos de la entidad.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function load()
    {
        $estructura = $this->global->Estructura->idestructura;
        $response = array('success' => TRUE, 'data' => array());

        $data = ConfParametrosbasicos::GetByCondition('idestructura = ?', array($estructura), TRUE);
        if (count($data)) {
            if (is_numeric($data[0]['idcuentadescuento'])) {
                $cuentadescuento = $this->integrator->contabilidad->getCuentasPorID(array($data[0]['idcuentadescuento']));
                if (count($cuentadescuento)) {
                    $data[0]['cuentadescuento'] = $cuentadescuento[0]['text'];
                }
            }

            if (is_numeric($data[0]['idcuentaflete'])) {
                $cuentaflete = $this->integrator->contabilidad->getCuentasPorID(array($data[0]['idcuentaflete']));
                if (count($cuentaflete)) {
                    $data[0]['cuentaflete'] = $cuentaflete[0]['text'];
                }
            }

            if (is_numeric($data[0]['idcuentaventa'])) {
                $cuentaventa = $this->integrator->contabilidad->getCuentasPorID(array($data[0]['idcuentaventa']));
                if (count($cuentaventa)) {
                    $data[0]['cuentaventa'] = $cuentaventa[0]['text'];
                }
            }

            if (is_numeric($data[0]['idcuentadevolucion'])) {
                $cuentadevolucion = $this->integrator->contabilidad->getCuentasPorID(array($data[0]['idcuentadevolucion']));
                if (count($cuentadevolucion)) {
                    $data[0]['cuentadevolucion'] = $cuentadevolucion[0]['text'];
                }
            }

            if (is_numeric($data[0]['idcuentaiva'])) {
                $cuentaiva = $this->integrator->contabilidad->getCuentasPorID(array($data[0]['idcuentaiva']));
                if (count($cuentaiva)) {
                    $data[0]['cuentaiva'] = $cuentaiva[0]['text'];
                }
            }
            $response['data'] = $data[0];
        }
        return $response;
    }

    /**
     * Inserta y modifica los parametros basicos de la entidad.
     *
     * @param array $post Arreglo con los datos del cliente o del proveedor
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function save($post)
    {
        $estructura = $this->global->Estructura->idestructura;

        /* Si se esta modificando */
        $objeto = (!empty($post['idparametro']) && is_numeric($post['idparametro'])) ?
            ConfParametrosbasicos::GetById($post['idparametro']) : new ConfParametrosbasicos();

        $objeto->ultimonumfactura = !empty($post['ultimonumfactura']) ? $post['ultimonumfactura'] : NULL;
        $objeto->ultimonumpedido = !empty($post['ultimonumpedido']) ? $post['ultimonumpedido'] : NULL;
        $objeto->ultimonumnotaventa = !empty($post['ultimonumnotaventa']) ? $post['ultimonumnotaventa'] : NULL;
        $objeto->ultimonumdevolucion = !empty($post['ultimonumdevolucion']) ? $post['ultimonumdevolucion'] : NULL;
        $objeto->contabilizartrans = !empty($post['contabilizartrans']) ? $post['contabilizartrans'] : 0;
        $objeto->idcuentadescuento = !empty($post['idcuentadescuento']) ? $post['idcuentadescuento'] : NULL;
        $objeto->idcuentaflete = !empty($post['idcuentaflete']) ? $post['idcuentaflete'] : NULL;
        $objeto->idcuentaventa = !empty($post['idcuentaventa']) ? $post['idcuentaventa'] : NULL;
        $objeto->idcuentadevolucion = !empty($post['idcuentadevolucion']) ? $post['idcuentadevolucion'] : NULL;
        $objeto->ivavigente = !empty($post['ivavigente']) ? $post['ivavigente'] : NULL;
        $objeto->idcuentaiva = !empty($post['idcuentaiva']) ? $post['idcuentaiva'] : NULL;
        $objeto->idestructura = $estructura;

        $idparametro = $objeto->persist();

        if (!is_numeric($idparametro)) {
//            return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgAddFAIL}";
            return "{'success':false, 'codMsg':3, 'mensaje': 'Los par&aacute;metros b&aacute;sicos no pudieron ser adicionados'}";
        }

        return "{'success':true, 'codMsg':1, 'mensaje': perfil.etiquetas.lbSuccess , idgenerado: $idparametro}";
    }

    public function setUltimonumpedido($idestructura, $val)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('ConfParametrosbasicos')
                ->set('ultimonumpedido', $val)
                ->where('idestructura = ?', $idestructura)
                ->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Número de pedido modificado satisfactoriamente'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    public function setUltimonumfactura($idestructura, $val)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('ConfParametrosbasicos')
                ->set('ultimonumfactura', $val)
                ->where('idestructura = ?', $idestructura)
                ->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Número de factura modificado satisfactoriamente'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    public function setUltimonumdevolucion($idestructura, $val)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('ConfParametrosbasicos')
                ->set('ultimonumdevolucion', $val)
                ->where('idestructura = ?', $idestructura)
                ->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Número de devolución modificado satisfactoriamente'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }
    }

}