<?php

class DatVendedorModel extends ZendExt_Model
{
    public function __construct()
    {
        parent::ZendExt_Model();
    }

    public function addVendedor($params)
    {
        $objVendedor = new DatVendedor();

        $objVendedor->codigo = $params->codigo;
        $objVendedor->idpersona = $params->idpersona;
        $objVendedor->idestructura = $this->global->Estructura->idestructura;

        $objVendedor->save();
        return "{'success':true, 'codMsg':1, 'mensaje':'Adicionado', 'idvendedor':'" . $objVendedor->idvendedor . "'}";
    }

    public function deleteVendedor($idvendedor)
    {
        try {
            $query = Doctrine_Query::create();
            $query->delete()
                ->from('DatVendedor')
                ->where('idvendedor = ?', $idvendedor)
                ->execute();

            return "{'success':true, 'codMsg':1, 'mensaje':'Eliminado'}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Tipo de contacto en uso'}";
            } else {
                return $e->getMessage();
            }
        }
    }

    public function updateVendedor($params)
    {
        try {
            $query = Doctrine_Query::create()
                ->update('DatVendedor');

            if($params->codigo) {
                $query->set('codigo', $params->codigo);
            }
            if($params->idpersona) {
                $query->set('idpersona', $params->idpersona);
            }

            $query->where('idvendedor = ?', $params->idvendedor);
            $query->execute();
            return "{'success':true, 'codMsg':1, 'mensaje':'Actulizado'}";
        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . " '}";
        }
    }

}