<?php

class DatDevolucionModel extends ZendExt_Model
{

    private $domain;

    public function __construct()
    {
        parent::ZendExt_Model();
        $this->domain = new DatDevolucion();
    }

    public function getDevoluciones($limit, $start, $filtros)
    {
        $devoluciones = $this->domain->GetAll($limit, $start, $filtros);
        foreach ($devoluciones as &$devolucion) {
            $devolucion['usuario'] = ZendExt_IoC::getInstance()->seguridad->nombreUsuario($devolucion['idusuario']);
            $cliente = ZendExt_IoC::getInstance()->configuracion->getClienteProvById($devolucion['idcliente']);
            $devolucion['cliente'] = $cliente['nombre'];
        }
        return $devoluciones;
    }

    public function guardarDevolucion($idfactura, $numero, $fecha, $idcliente, $comentario, $importe, $impuesto, $items)
    {
        try {
            $obj = new DatDevolucion();

            //Datos de la factura
            $obj->idfactura = $idfactura;
            $obj->numero = $numero;
            $obj->fecha = $fecha;
            $obj->idcliente = $idcliente;
            $obj->comentario = $comentario;
            $obj->importe = $importe;
            $obj->impuesto = $impuesto;
            $obj->idusuario = $this->global->Perfil->idusuario;

            //Datos de los productos
            foreach ($items as $k => $item) {
                $obj->DatDevolucionProducto[$k]->idproductoservicio = $item->idproductoservicio;
                $obj->DatDevolucionProducto[$k]->cantidad = $item->cantidad;
                $obj->DatDevolucionProducto[$k]->descuento = $item->descuento;
                $obj->DatDevolucionProducto[$k]->preciounitario = $item->preciounitario;
                $obj->DatDevolucionProducto[$k]->iva = $item->iva;
                $obj->DatDevolucionProducto[$k]->importe = $item->importe;
            }

            $obj->save();
            $basicParam = new ConfParametrosbasicosModel();
            $basicParam->setUltimonumdevolucion($this->global->Estructura->idestructura, (int)$numero + 1);
            return "{'success':true, 'codMsg':1, 'mensaje': 'Factura guardada satisfactoriamente'}";
        } catch (Doctrine_Exception $e) {
            return "{'success':false, 'codMsg':3, 'mensaje':'" . $e->getMessage() . "'}";
        }

    }

    public function loadDataContabilizar($iddevolucion)
    {
        $rpse = array();
        $devolucion = $this->domain->GetAll(null, null, null, $iddevolucion);
        $devolucion = $devolucion[0];

        $facturaModel = new DatFacturaModel();
        $factura = $facturaModel->getFacturaByArrId(array($devolucion['idfactura']));
        $factura = $factura[0];

        if ($factura['estado'] == 0) {
            $rpse['code'] = 1;//Factura sin contabilizar
            //Verificar que la factura que no este contabilizada disminuye los productos que se devuelvan


        } else if ($factura['estado'] == 1) {
            //Cuando la factura esta contabilizada
            $rpse['code'] = 2; //Factura contabilizada

            $periodoActual = $this->integrator->contabilidad->getPeriodoSubsistema($this->global->Estructura->idestructura, 7);
            $dataFecha = $this->integrator->contabilidad->getFechaSubsistema($this->global->Estructura->idestructura, 7);

            $objBasicParam = new ConfParametrosbasicosModel();
            $basicParams = $objBasicParam->load();

            $rpse['referencia'] = 'Contabilizando devolución (' . $devolucion['numero'] . ')';
            $rpse['detalle'] = 'Contabilizando devolución (' . $devolucion['numero'] . ')';
            $rpse['fechaemision'] = $dataFecha[0]['fecha'];
            $rpse['idsubsistema'] = 7;
            $rpse['idperiodo'] = $periodoActual[0]['idperiodo'];

            $rpse['pases'] = array();

            //Pase por el credito de la cuenta del usuario
            if (empty($factura['idcuentacobrar'])) {
                throw new Exception('Debe configurar una cuenta para el cliente');
            } else {
                $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($factura['idcuentacobrar']));
                $paseCliente['readOnly'] = true;
                $paseCliente['idpase'] = 0;
                $paseCliente['idcuenta'] = $factura['idcuentacobrar'];
                $paseCliente['codigodocumento'] = $devolucion['numero'];
                $paseCliente['concatcta'] = $dataCta[0]['concat'];
                $paseCliente['denominacion'] = $dataCta[0]['denominacion'];
                $paseCliente['debito'] = 0.00;
                $paseCliente['credito'] = $devolucion['importe'];

                $rpse['pases'][] = $paseCliente;
            }


            //Pase por el debe para la cuenta de devolucion
            if (empty($basicParams['data']['idcuentadevolucion'])) {
                throw new Exception('Debe configurar una cuenta para las devoluciones');
            } else {
                $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($basicParams['data']['idcuentadevolucion']));
                $paseFlete['readOnly'] = true;
                $paseFlete['idpase'] = 0;
                $paseFlete['idcuenta'] = $basicParams['data']['idcuentadevolucion'];
                $paseFlete['codigodocumento'] = $devolucion['numero'];
                $paseFlete['concatcta'] = $dataCta[0]['concat'];
                $paseFlete['denominacion'] = $dataCta[0]['denominacion'];
                $paseFlete['debito'] = $devolucion['importe'] - $devolucion['impuesto'];
                $paseFlete['credito'] = 0.00;

                $rpse['pases'][] = $paseFlete;
            }

            //Pase por el debe para el IVA
            if (!empty($basicParams['data']['idcuentaiva'])) {
                if (!empty($devolucion['impuesto'])) {
                    $dataCta = $this->integrator->contabilidad->getCuentasPorID(array($basicParams['data']['idcuentaiva']));
                    $paseIva['readOnly'] = true;
                    $paseIva['idpase'] = 0;
                    $paseIva['idcuenta'] = $basicParams['data']['idcuentaiva'];
                    $paseIva['codigodocumento'] = $devolucion['numero'];
                    $paseIva['concatcta'] = $dataCta[0]['concat'];
                    $paseIva['denominacion'] = $dataCta[0]['denominacion'];
                    $paseIva['debito'] = $devolucion['impuesto'];
                    $paseIva['credito'] = 0.00;

                    $rpse['pases'][] = $paseIva;
                }
            }
        }
        return $rpse;
    }

    public function saveComprobante($argparams)
    {
        try {
            $datDevolucion = new DatDevolucion();
            $datDevolucion = $datDevolucion->getById($argparams['iddevolucion']);

            $datFactura = new DatFactura();
            $datFactura = $datFactura->getById($datDevolucion->idfactura);

            //Determina si se registra la cuenta por pagar de la devolucion si se ha liquidado algo en la factura
            //Busca las cuentas por cobrar con el numero escojido y que sea una factura
            $obligacion = $this->integrator->contabilidad->obtenerObligacion(1, $datFactura->numero, 1);

            if ((float)$obligacion[0]['liquidado'] == 0) {
                $save = $this->integrator->contabilidad->saveComprobante($argparams);

                $id = explode("'id':", $save);
                $num = explode("'nocomp':", $save);
                $numco = array();
                if (count($id) > 1) {
                    $id = substr($id[1], 0, -1);
                }
                if (count($num) > 1) {
                    $num = substr($num[1], 0, -1);
                    $numco = explode(',', $num);
                }

                if (is_numeric($id)) {

                    //Actualiza el estado de la factura a Contabilizado
                    $this->cambiaEstadoDevolucion(array($argparams['iddevolucion']), 1);

                    return "{'success':true, 'codMsg':1,'mensaje':'Devolución contabilizada satisfactoriamente'}";
                } else {
                    return "{'success':false, 'codMsg':3,'mensaje':'Ha ocurrido un error al registrar el comprobante'}";
                }
            } else {
                //Caso cuando se ha liquidado parte o toda la factura
            }

        } catch (Exception $e) {
            return "{'success':false, 'codMsg':3,'mensaje':'" . $e->getMessage() . "'}";
        }
    }

    /**
     * Actualiza el estado de la devolucion
     * @param array $arrIdDevolucion
     * @param $estado
     * @throws ZendExt_Exception
     */
    public function cambiaEstadoDevolucion(array $arrIdDevolucion, $estado)
    {
        try {
            if (!empty($arrIdFactura)) {
                $q = Doctrine_Query::create()
                    ->update('DatDevolucion')
                    ->set('estado', '?', $estado)
                    ->whereIn('iddevolucion', $arrIdDevolucion);
                $q->execute();
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}

