<?php

class DatPedidoProducto extends BaseDatPedidoProducto
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomProductoservicio', array('local' => 'idproductoservicio', 'foreign' => 'idproductoservicio'));
        $this->hasOne('DatPedido', array('local' => 'idpedido', 'foreign' => 'idpedido'));
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->idpedidoproducto;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('DatPedidoProducto')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatPedidoProducto')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idpedidoproducto, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatPedidoProducto')->find($idpedidoproducto);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatPedidoProducto')
                ->whereIn('idpedidoproducto', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatPedidoProducto')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function Erase($idpedidoproducto)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatPedidoProducto')
                ->where("idpedidoproducto = $idpedidoproducto")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($idpedidoproducto)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.dat_pedido_producto WHERE idpedidoproducto = ' . $idpedidoproducto);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatPedidoProducto')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.dat_pedido_producto WHERE ' . $conditions);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

    public function EraseByIdPedidoProducto($idpedidoproducto)
    {
        $array = (is_array($idpedidoproducto)) ? $idpedidoproducto : array($idpedidoproducto);
        return $this->EraseByConditionSQL('idpedidoproducto IN( ' . implode(',', $array)) . ')';
    }
}