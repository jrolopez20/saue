<?php

class NomProductoservicio extends BaseNomProductoservicio
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatPedidoProducto', array('local' => 'idproductoservicio', 'foreign' => 'idproductoservicio'));
        $this->hasMany('DatFacturaProducto', array('local' => 'idproductoservicio', 'foreign' => 'idproductoservicio'));
        $this->hasMany ('DatDevolucionProducto', array ('local' => 'idproductoservicio', 'foreign' => 'idproductoservicio'));
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->idproductoservicio;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    /**
     * Busca los productos y servicios de una entidad
     * @param $idestructura
     * @param $limit
     * @param $start
     * @param $filter
     * @param null $activo
     * @return array|Doctrine_Exception|Exception
     */
    public function getProductoServicio($idestructura, $limit, $start, $filter, $activo = null)
    {
        try {
            $query = Doctrine_Query::create();
            $query->from('NomProductoservicio')
                ->where('idestructura = ?', $idestructura);

            if (!empty($filter)) {
                $query->addWhere("codigo = '$filter' OR codigobarra = '$filter' OR denominacion ilike '%$filter%'");
            }

            if (!empty($activo)) {
                $query->addWhere('activo = ?', $activo);
            }

            if ($limit) {
                $query->limit($limit);
            }

            if ($start) {
                $query->offset($start);
            }
            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    /**
     * Ontiene la cantidad total de productos y servicios de una entidad
     * @param $idestructura
     * @param $filter
     * @param null $activo
     * @return Doctrine_Exception|Exception
     */
    public function cantProductoServicio($idestructura, $filter, $activo = null)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('COUNT(idproductoservicio) as cantidad')
                ->from('NomProductoservicio')
                ->where('idestructura = ?', $idestructura);

            if (!empty($filter)) {
                $query->addWhere("codigo = ? OR codigobarra = ? OR denominacion ilike ?", $filter);
            }

            if (!empty($activo)) {
                $query->addWhere('activo = ?', $activo);
            }

            return $query->fetchOne()->cantidad;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }


    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('NomProductoservicio')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomProductoservicio')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idproductoservicio, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('NomProductoservicio')->find($idproductoservicio);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomProductoservicio')
                ->whereIn('idproductoservicio', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomProductoservicio')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function Erase($idproductoservicio)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomProductoservicio')
                ->where("idproductoservicio = $idproductoservicio")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($idproductoservicio)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_productoservicio WHERE idproductoservicio = ' . $idproductoservicio);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                throw $e;
//                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomProductoservicio')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_productoservicio WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

    public function EraseByIdProdServ($idproductoservicio)
    {
        $array = (is_array($idproductoservicio)) ? $idproductoservicio : array($idproductoservicio);

        return $this->EraseByConditionSQL('idproductoservicio IN( ' . implode(',', $array)) . ')';
    }

    static public function ChangeActive($idproductoservicio)
    {
        $prodservicios = is_array($idproductoservicio) ? implode(',', $idproductoservicio) : $idproductoservicio;
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('UPDATE mod_facturacion.nom_productoservicio SET activo = ' .
                'CASE WHEN activo = 1 THEN 0 ELSE 1 END WHERE idproductoservicio IN(' . $prodservicios . ')');

            return TRUE;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }
}

