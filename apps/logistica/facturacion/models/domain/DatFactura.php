<?php

class DatFactura extends BaseDatFactura
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatFacturaProducto', array('local' => 'idfactura', 'foreign' => 'idfactura'));
        $this->hasOne('NomTerminosPago', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
        $this->hasMany('DatFpagocheque', array('local' => 'idfactura', 'foreign' => 'idfactura'));
        $this->hasMany('DatFpagoefectivo', array('local' => 'idfactura', 'foreign' => 'idfactura'));
        $this->hasMany('DatFpagotarjeta', array('local' => 'idfactura', 'foreign' => 'idfactura'));
    }

    public function getById($idfactura, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatFactura')->find($idfactura);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetAll($limit, $start, $filtros)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('f.idfactura, f.idcliente, f.idusuario, f.fecha, f.idterminodepago, f.numero,
                    f.flete, tp.nombre as terminodepago, f.idvendedor, f.importe, f.impuesto, f.estado')
                ->from('DatFactura f')
                ->leftJoin('f.NomTerminosPago tp');

            if (!empty($filtros) && !empty($filtros[0]->value)) {
                $query->where('f.numero = ?', $filtros[0]->value);
            }

            if ($limit) {
                $query->limit($limit);
            }

            if ($start) {
                $query->offset($start);
            }

            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

    public function GetTotalCount($filtros)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('count(idfactura) as cantidad')
                ->from('DatFactura');

            if (!empty($filtros) && !empty($filtros[0]->value)) {
                $query->where('numero = ?', $filtros[0]->value);
            }

            return $query->fetchOne()->cantidad;
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

    public function getFacturaByNumero($numero)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('f.idfactura, f.idcliente, f.idusuario, f.fecha, f.idterminodepago, f.numero, f.flete,
                    f.idvendedor, tp.nombre as terminodepago, fp.idproductoservicio, fp.cantidad, fp.descuento,
                    ps.codigo, ps.denominacion, ps.preciounitario, ps.iva, f.importe, f.impuesto, f.estado')
                ->from('DatFactura f')
                ->leftJoin('f.NomTerminosPago tp')
                ->leftJoin('f.DatFacturaProducto fp')
                ->leftJoin('fp.NomProductoservicio ps')
                ->where('f.numero = ?', $numero);

            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

    public function getFacturaProductos($idfactura = null, $numero = null)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('f.*, pp.*, nps.codigo, nps.denominacion, nps.preciounitario, nps.iva')
                ->from('DatFactura f')
                ->leftJoin('f.DatFacturaProducto pp')
                ->leftJoin('pp.NomProductoservicio nps');

            if (!empty($idfactura)) {
                if (is_array($idfactura)) {
                    $idfactura = implode(',', $idfactura);
                    $query->where("f.idfactura IN ($idfactura)");
                } else {
                    $query->addWhere('f.idfactura = ?', $idfactura);
                }
            }

            if ($numero) {
                $query->addWhere('f.numero = ?', $numero);
            }

            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

}