<?php

class DatPedido extends BaseDatPedido
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatPedidoProducto', array('local' => 'idpedido', 'foreign' => 'idpedido'));
        $this->hasOne('NomTerminosPago', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
    }

    public function getById($idpedido, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatPedido')->find($idpedido);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetAll($limit, $start, $filtros)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('p.idpedido, p.idcliente, p.idusuario, p.fecha, p.idterminodepago, p.estado, p.numero,
                    p.flete, tp.nombre as terminodepago')
                ->from('DatPedido p')
                ->leftJoin('p.NomTerminosPago tp');

            if (!empty($filtros) && !empty($filtros[0]->value)) {
                $query->where('p.numero = ?', $filtros[0]->value);
            }

            if ($limit) {
                $query->limit($limit);
            }

            if ($start) {
                $query->offset($start);
            }

            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

    public function getPedidoProductos($idpedido = null, $numero = null)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('p.*, pp.*, nps.codigo, nps.denominacion, nps.preciounitario, nps.iva')
                ->from('DatPedido p')
                ->leftJoin('p.DatPedidoProducto pp')
                ->leftJoin('pp.NomProductoservicio nps');

            if (!empty($idpedido)) {
                if (is_array($idpedido)) {
                    $idpedido = implode(',', $idpedido);
                    $query->where("p.idpedido IN ($idpedido)");
                } else {
                    $query->addWhere('p.idpedido = ?', $idpedido);
                }
            }

            if ($numero) {
                $query->addWhere('p.numero = ?', $numero);
            }

            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

}