<?php

class NomClasificadorprodserv extends BaseNomClasificadorprodserv
{

    public function setUp()
    {
        parent:: setUp();
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->idclasificador;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->select("idclasificador as id, idclasificador, CONCAT(CONCAT(concat ,' '), denominacion) as text, CONCAT(CONCAT(concat ,' '),
        denominacion) as qtip, ( (n.ordender - n.ordenizq) = 1) as leaf, codigo, concat, denominacion,
        idpadre, ordender, ordenizq, nivel, activo, idestructura")
                ->from('NomClasificadorprodserv')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->select("idclasificador as id, idclasificador, CONCAT(CONCAT(concat ,' '), denominacion) as text, CONCAT(CONCAT(concat ,' '),
        denominacion) as qtip, ( (ordender - ordenizq) = 1) as leaf, codigo, concat, denominacion,
        idpadre, ordender, ordenizq, nivel, activo, idestructura")
                ->from('NomClasificadorprodserv')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetById($idclasificador, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('NomClasificadorprodserv')->find($idclasificador);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->select("idclasificador as id, idclasificador, CONCAT(CONCAT(concat ,' '), denominacion) as text, CONCAT(CONCAT(concat ,' '),
        denominacion) as qtip, ( (ordender - ordenizq) = 1) as leaf, codigo, concat, denominacion,
        idpadre, ordender, ordenizq, nivel, activo, idestructura")
                ->from('NomClasificadorprodserv')
                ->whereIn('idclasificador', $arrIds)
                ->orderBy('tipo')
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->select("idclasificador as id, idclasificador, CONCAT(CONCAT(concat ,' '), denominacion) as text, CONCAT(CONCAT(concat ,' '),
        denominacion) as qtip, ( (ordender - ordenizq) = 1) as leaf, codigo, concat, denominacion,
        idpadre, ordender, ordenizq, nivel, activo, idestructura, tipo, idarbol, descripcion")
                ->from('NomClasificadorprodserv')
                ->where("$condition", $params)
                ->orderBy('tipo')
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function Erase($idclasificador)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomClasificadorprodserv')
                ->where("idclasificador = $idclasificador")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                throw $e;
            }
        }
    }

    static public function EraseSQL($idclasificador)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_clasificadorprodserv WHERE idclasificador = ' . $idclasificador);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                throw $e;
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomClasificadorprodserv')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_clasificadorprodserv WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

    public function EraseByIdClasificador($idclasificador)
    {
        $array = (is_array($idclasificador)) ? $idclasificador : array($idclasificador);

        return $this->EraseByConditionSQL('idclasificador IN( ' . implode(',', $array)) . ')';
    }

    static public function ChangeActive($idclasificador)
    {
        $clasificaciones = is_array($idclasificador) ? implode(',', $idclasificador) : $idclasificador;
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('UPDATE mod_facturacion.nom_clasificadorprodserv SET activo = ' .
                'CASE WHEN activo = 1 THEN 0 ELSE 1 END WHERE idclasificador IN(' . $clasificaciones . ')');

            return TRUE;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }
}

