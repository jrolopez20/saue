<?php

class ConfParametrosbasicos extends BaseConfParametrosbasicos
{

    public function setUp()
    {
        parent::setUp();
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->idparametro;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('ConfParametrosbasicos')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('ConfParametrosbasicos')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idparametro, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('ConfParametrosbasicos')->find($idparametro);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('ConfParametrosbasicos')
                ->whereIn('idparametro', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('ConfParametrosbasicos')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            echo "{'success':false, 'codMsg':3, 'mensaje':'". $e->getMessage()."'}";
        }
    }

    static public function Erase($idparametro)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('ConfParametrosbasicos')
                ->where("idparametro = $idparametro")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3, 'mensaje':'Cargo en uso'}";
            } else {
                throw $e;
            }
        }
    }

    static public function EraseSQL($idparametro)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.conf_parametrosbasicos WHERE idparametro = '.$idparametro);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('ConfParametrosbasicos')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.conf_parametrosbasicos WHERE ' . $conditions);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  0;
            } else {
                return $e->getMessage();
            }
        }
    }

    public function EraseByIdParametro($idparametro)
    {
        $array = (is_array($idparametro)) ? $idparametro : array($idparametro);
        return $this->EraseByConditionSQL('idparametro IN( ' . implode(',', $array)) . ')';
    }

}