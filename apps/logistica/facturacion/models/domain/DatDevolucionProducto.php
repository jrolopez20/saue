<?php

class DatDevolucionProducto extends BaseDatDevolucionProducto
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('NomProductoservicio', array ('local' => 'idproductoservicio', 'foreign' => 'idproductoservicio'));
        $this->hasOne ('DatDevolucion', array ('local' => 'iddevolucion', 'foreign' => 'iddevolucion'));
    }

}