<?php

class DatFacturaProducto extends BaseDatFacturaProducto
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('DatFactura', array ('local' => 'idfactura', 'foreign' => 'idfactura'));
        $this->hasOne ('NomProductoservicio', array ('local' => 'idproductoservicio', 'foreign' => 'idproductoservicio'));
    }

}