<?php

class DatVendedor extends BaseDatVendedor
{

    public function setUp()
    {
        parent:: setUp();
    }

    public function listVendedor($limit = 25, $start = 0, $filters = null, $idestructura)
    {
        $sql = "SELECT v.idvendedor, v.idpersona, v.codigo, (p.nombre || ' ' || p.apellidos) as fullname, p.foto "
            . "FROM mod_facturacion.dat_vendedor v "
            . "INNER JOIN mod_persona.dat_persona p on (p.idpersona = v.idpersona) "
            . "WHERE v.idestructura = $idestructura ";

        if (!empty($filters)) {
            if (is_array($filters)) {
                $sql .= " AND (v.codigo = '{$filters[0]->value}' OR p.nombre ilike '%{$filters[0]->value}%' OR p.apellidos ilike '%{$filters[0]->value}%') ";
            } else {
                $sql .= " AND (v.codigo = '{$filters}' OR p.nombre ilike '%{$filters}%' OR p.apellidos ilike '%{$filters}%') ";
            }
        }

        if ($limit) {
            $sql .= " LIMIT {$limit}";
        }

        if ($start) {
            $sql .= " OFFSET {$start}";
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchAll($sql);
    }

    public function countVendedor($filters = null, $idestructura)
    {

        $sql = "SELECT COUNT(v.idvendedor) as cantidad FROM mod_facturacion.dat_vendedor v "
            . "INNER JOIN mod_persona.dat_persona p on (p.idpersona = v.idpersona) "
            . "WHERE v.idestructura = $idestructura ";

        if (!empty($filters)) {
            if (is_array($filters)) {
                $sql .= " AND (v.codigo = '{$filters[0]->value}' OR p.nombre ilike '%{$filters[0]->value}%' OR p.apellidos ilike '%{$filters[0]->value}%') ";
            } else {
                $sql .= " AND (v.codigo = '{$filters}' OR p.nombre ilike '%{$filters}%' OR p.apellidos ilike '%{$filters}%') ";
            }
        }

        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        return $connection->fetchOne($sql)->cantidad;
    }
}