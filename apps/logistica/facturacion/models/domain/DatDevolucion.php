<?php

class DatDevolucion extends BaseDatDevolucion
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('DatDevolucionProducto', array('local' => 'iddevolucion', 'foreign' => 'iddevolucion'));
    }

    public function getById($iddevolucion, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatDevolucion')->find($iddevolucion);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetAll($limit = null, $start = null, $filtros = null, $iddevolucion = null)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('d.iddevolucion, d.idcliente, d.idusuario, d.fecha, d.numero, d.estado, d.importe,
                d.impuesto, d.idfactura')
                ->from('DatDevolucion d');

            if (!empty($filtros) && !empty($filtros[0]->value)) {
                $query->where('d.numero = ?', $filtros[0]->value);
            }

            if(!empty($iddevolucion)){
                $query->where('d.iddevolucion = ?', $iddevolucion);
            }

            if ($limit) {
                $query->limit($limit);
            }

            if ($start) {
                $query->offset($start);
            }

            return $query->fetchArray();
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

    public function GetTotalCount($filtros)
    {
        try {
            $query = Doctrine_Query::create();
            $query->select('count(iddevolucion) as cantidad')
                ->from('DatDevolucion');

            if (!empty($filtros) && !empty($filtros[0]->value)) {
                $query->where('numero = ?', $filtros[0]->value);
            }

            return $query->fetchOne()->cantidad;
        } catch (Doctrine_Exception $e) {
            die($e->getMessage());
        }
    }

}