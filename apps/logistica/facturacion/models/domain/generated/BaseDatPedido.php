<?php

abstract class BaseDatPedido extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_pedido');
        $this->hasColumn('idpedido', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_datpedido'));
        $this->hasColumn('idcliente', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('numero', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('flete', 'numeric', null, array ('notnull' => false,'primary' => false));

    }

}