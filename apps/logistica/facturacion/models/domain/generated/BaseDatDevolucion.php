<?php

abstract class BaseDatDevolucion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_devolucion');
        $this->hasColumn('iddevolucion', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_facturacion.sec_devolucion'));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('numero', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcliente', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('comentario', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('importe', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('impuesto', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idfactura', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}