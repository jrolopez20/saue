<?php

abstract class BaseNomClasificadorprodserv extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.nom_clasificadorprodserv');
        $this->hasColumn('idclasificador', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_clasificadorprodserv'));
        $this->hasColumn('idpadre', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('concat', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('tipo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('activo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('nivel', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('ordenizq', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('ordender', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idarbol', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array ('notnull' => false,'primary' => false));
    }


}

