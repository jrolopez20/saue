<?php

abstract class BaseDatDevolucionProducto extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_devolucion_producto');
        $this->hasColumn('iddevolucionproducto', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_facturacion.sec_devolucionproducto'));
        $this->hasColumn('iddevolucion', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idproductoservicio', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('cantidad', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('descuento', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('preciounitario', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('iva', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('importe', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}