<?php

abstract class BaseNomProductoservicio extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.nom_productoservicio');
        $this->hasColumn('idproductoservicio', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_productoservicio'));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('foto', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idunidadmedida', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('stock', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('preciounitario', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('activo', 'numeric', null, array ('notnull' => true,'primary' => false, 'default' => 1));
        $this->hasColumn('codigobarra', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('iva', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idclasificador', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('tipo', 'numeric', null, array ('notnull' => true,'primary' => false));
    }


}

