<?php

abstract class BaseDatFacturaProducto extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_factura_producto');
        $this->hasColumn('idfacturaproducto', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_facturaproducto'));
        $this->hasColumn('idfactura', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idproductoservicio', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cantidad', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('descuento', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}