<?php

abstract class BaseDatFpagoefectivo extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_fpagoefectivo');
        $this->hasColumn('idfpagoefectivo', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_fpagoefectivo'));
        $this->hasColumn('idfactura', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('efectivo', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cambio', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}