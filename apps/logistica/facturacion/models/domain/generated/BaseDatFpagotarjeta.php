<?php

abstract class BaseDatFpagotarjeta extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_fpagotarjeta');
        $this->hasColumn('idfpagotarjeta', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_fpagotarjeta'));
        $this->hasColumn('idfactura', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('numerotarjeta', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('monto', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}