<?php

abstract class BaseDatFactura extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_factura');
        $this->hasColumn('idfactura', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_factura'));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idcliente', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('numero', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('flete', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idvendedor', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('importe', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('impuesto', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}