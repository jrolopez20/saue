<?php

abstract class BaseDatPedidoProducto extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_pedido_producto');
        $this->hasColumn('idpedidoproducto', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_pedidoproducto'));
        $this->hasColumn('idpedido', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idproductoservicio', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cantidad', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('descuento', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}