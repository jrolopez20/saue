<?php

abstract class BaseDatVendedor extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_vendedor');
        $this->hasColumn('idvendedor', 'numeric', null, array ('notnull' => false,'primary' => true, 'default' => '', 'sequence' => 'mod_facturacion.sec_vendedor'));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}