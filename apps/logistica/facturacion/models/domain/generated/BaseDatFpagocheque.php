<?php

abstract class BaseDatFpagocheque extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_fpagocheque');
        $this->hasColumn('idfpagocheque', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_facturacion.sec_fpagocheque'));
        $this->hasColumn('numerocheque', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fechacheque', 'date', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idbanco', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('postfechado', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('telefono', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idfactura', 'numeric', null, array ('notnull' => true,'primary' => false));
    }

}