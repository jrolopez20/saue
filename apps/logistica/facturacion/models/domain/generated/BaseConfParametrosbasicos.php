<?php

abstract class BaseConfParametrosbasicos extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.conf_parametrosbasicos');
        $this->hasColumn('idparametro', 'numeric', null, array ('notnull' => true,'primary' => true, 'sequence' => 'mod_facturacion.sec_parametrosbasicos'));
        $this->hasColumn('ultimonumfactura', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('ultimonumpedido', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('ultimonumnotaventa', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('ultimonumdevolucion', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('contabilizartrans', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcuentadescuento', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcuentaflete', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcuentaventa', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcuentadevolucion', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('ivavigente', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idcuentaiva', 'numeric', null, array ('notnull' => false,'primary' => false));
    }

}