<?php

class EmpresaService extends ZendExt_Model
{

    public function EmpresaService()
    {
        parent::ZendExt_Model();
    }

    public function getDatosEmpresa($idestructura = 0)
    {
        $empresa = new DatEmpresa();
        return $empresa->getEmpresa((!$idestructura) ? $this->global->Estructura->idestructura : $idestructura);
    }

}
