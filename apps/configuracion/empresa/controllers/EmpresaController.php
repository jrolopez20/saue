<?php

/**
 * Clase controladora para configurar la empresa
 *
 * @author Team Delta
 * @package Datos Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class EmpresaController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new DatEmpresaModel();
        parent::init();
    }


    public function empresaAction()
    {
        $this->render();
    }

    public function loadAction()
    {
        $empresa = new DatEmpresa();

        $r = $empresa->getEmpresa($this->global->Estructura->idestructura);

        if(empty($r)){
            $r['idestructura'] = $this->global->Estructura->idestructura;
            $r['nombre'] = $this->global->Estructura->denominacion;
        }

        echo json_encode($r);
    }

    public function saveAction()
    {
        $idestructura = $this->_request->getPost('idestructura');
        $nombre = $this->_request->getPost('nombre');
        $razonsocial = $this->_request->getPost('razonsocial');
        $ruc = $this->_request->getPost('ruc');
        $direccion = $this->_request->getPost('direccion');
        $telefono = $this->_request->getPost('telefono');
        $correo = $this->_request->getPost('correo');
        $idpersona = $this->_request->getPost('idpersona');
        $logo = $this->_request->getPost('logo');

        $r = $this->model->saveEmpresa(
            $idestructura, $nombre, $razonsocial, $ruc, $direccion, $telefono, $correo, $idpersona, $logo
        );

        echo $r;
    }

}