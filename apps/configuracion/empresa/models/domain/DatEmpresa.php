<?php

class DatEmpresa extends BaseDatEmpresa
{

    public function setUp()
    {
        parent:: setUp();
    }

    public function getById($idestructura, $toArray = false)
    {
        try {
            $result = Doctrine::getTable('DatEmpresa')->find($idestructura);
            return ($toArray && $result) ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function getEmpresa($idestructura)
    {
        try {
            $sql = "SELECT e.*, (p.nombre|| ' '|| p.apellidos) as representante FROM mod_maestro.dat_empresa e
                LEFT JOIN mod_persona.dat_persona p ON (e.idpersona = p.idpersona)
                WHERE e.idestructura = {$idestructura}";
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $r = $connection->fetchAll($sql);
            $empresa = $r[0];
            $empresa['srcLogo'] = '/configuracion/empresa/views/images/logo/' . $empresa['idestructura'] . '.png';
            return $empresa;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

}
