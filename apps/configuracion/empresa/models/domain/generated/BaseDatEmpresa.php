<?php

abstract class BaseDatEmpresa extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_empresa');
        $this->hasColumn('idestructura', 'numeric', null, array ('notnull' => true,'primary' => true));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('razonsocial', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('ruc', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('direccion', 'character varying', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('correo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('telefono', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpersona', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('logo', 'character varying', null, array ('notnull' => true,'primary' => false));
    }

}