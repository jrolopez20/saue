<?php

class DatEmpresaModel extends ZendExt_Model
{

    public function DatEmpresaModel()
    {
        parent::ZendExt_Model();
    }

    public function saveEmpresa(
    $idestructura, $nombre, $razonsocial, $ruc, $direccion, $telefono, $correo, $idpersona, $logo
    )
    {
        if (empty($idestructura)) {
            $idestructura = $this->global->Estructura->idestructura;
        }

        $empresa = new DatEmpresa();
        $objEmpresa = $empresa->getById($idestructura);
        if (!$objEmpresa) {
            $objEmpresa = new DatEmpresa();
        }
        $objEmpresa->idestructura = $idestructura;
        $objEmpresa->nombre = $nombre;
        $objEmpresa->razonsocial = $razonsocial;
        $objEmpresa->ruc = $ruc;
        $objEmpresa->direccion = $direccion;
        $objEmpresa->telefono = $telefono;
        $objEmpresa->correo = $correo;
        $objEmpresa->idpersona = $idpersona;
        $objEmpresa->logo = $logo;
        $objEmpresa->save();
        
        $this->generateFisicalLogo($idestructura, $logo);

        return "{'success':true, 'codMsg':1,'mensaje': 'Datos de la empresa guardados satisfactoriamente', 'idestructura':{$idestructura}}";
    }

    public function generateFisicalLogo($idestructura, $Base64Img)
    {
        list(, $Base64Img) = explode(';', $Base64Img);
        list(, $Base64Img) = explode(',', $Base64Img);
        $Base64Img = base64_decode($Base64Img);
        file_put_contents('views/images/logo/'.$idestructura .'.png', $Base64Img);
    }

}
