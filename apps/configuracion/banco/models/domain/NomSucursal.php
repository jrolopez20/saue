<?php

class NomSucursal extends BaseNomSucursal
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomBanco', array('local' => 'idbanco', 'foreign' => 'idbanco'));
    }


    public function findById($idsucursal)
    {
        return Doctrine::getTable('NomSucursal')->find($idsucursal);
    }

    public function getSucursales($idbanco, $limit, $start, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idsucursal, idbanco, numero, direccion')
            ->from('NomSucursal');

        if ($idbanco) {
            $query->addWhere('idbanco = ?', $idbanco);
        }

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if($k > 0){
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }
        return $query->fetchArray();
    }

    public function getCantSucursales($idbanco, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idsucursal) as cantidad')
            ->from('NomSucursal');

        if ($idbanco) {
            $query->addWhere('idbanco = ?', $idbanco);
        }

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if($k > 0){
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }

        return $query->fetchOne()->cantidad;
    }

}