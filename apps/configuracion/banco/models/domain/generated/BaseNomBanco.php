<?php

abstract class BaseNomBanco extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_banco');
        $this->hasColumn('idbanco', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_nombanco'));
        $this->hasColumn('codigo', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('abreviatura', 'character varying', null, array ('notnull' => true,'primary' => false));
    }

}