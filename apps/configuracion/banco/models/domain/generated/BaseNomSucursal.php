<?php

abstract class BaseNomSucursal extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_sucursal');
        $this->hasColumn('idsucursal', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_nomsucursal'));
        $this->hasColumn('numero', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('direccion', 'character varying', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idbanco', 'numeric', null, array ('notnull' => false,'primary' => false));
    }


}

