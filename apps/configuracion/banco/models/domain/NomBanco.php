<?php

class NomBanco extends BaseNomBanco
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('NomSucursal', array('local' => 'idbanco', 'foreign' => 'idbanco'));
    }

    public function findById($idbanco)
    {
        return Doctrine::getTable('NomBanco')->find($idbanco);
    }

    public function getBancos($limit, $start, $filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('idbanco, codigo, abreviatura, nombre')
            ->from('NomBanco');

        if ($limit) {
            $query->limit($limit);
        }

        if ($start) {
            $query->limit($start);
        }

        if (!empty($filters)) {
            $sql = '';
            if (is_array($filters)) {
                foreach ($filters as $k => $filter) {
                    if ($k > 0) {
                        $sql .= ' OR ';
                    }
                    $sql .= "{$filter->property} ilike '%{$filter->value}%'";
                }
            } else if (is_string($filters)) {
                    $sql = "codigo={$filters} OR abreviatura={$filters} OR ilike '%{$filters}%'";
            }
            $query->addWhere($sql);
        }
        return $query->fetchArray();
    }

    public function getCantBancos($filters = null)
    {
        $query = Doctrine_Query::create()
            ->select('COUNT(idbanco) as cantidad')
            ->from('NomBanco');

        if (is_array($filters) && !empty($filters)) {
            $sql = '';
            foreach ($filters as $k => $filter) {
                if ($k > 0) {
                    $sql .= ' OR ';
                }
                $sql .= "{$filter->property} ilike '%{$filter->value}%'";
            }
            $query->addWhere($sql);
        }
        return $query->fetchOne()->cantidad;
    }

}