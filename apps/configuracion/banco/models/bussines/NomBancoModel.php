<?php

class NomBancoModel extends ZendExt_Model
{
    public function NomBancoModel()
    {
        parent::ZendExt_Model();
    }

    public function updateBanco($idbanco = null, $codigo = '', $abreviatura = '', $nombre = '')
    {
        try {
            $objDomain = new NomBanco();
            if (!empty($idbanco)) {
                $objDomain = $objDomain->findById($idbanco);
            }
            $objDomain->codigo = $codigo;
            $objDomain->abreviatura = $abreviatura;
            $objDomain->nombre = $nombre;
            $objDomain->save();

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgBancoGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function deleteBanco($idbanco)
    {
        try {
            $objDomain = new NomBanco();
            $objContenido = $objDomain->findById($idbanco);
            $objContenido->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgBancoEliminado}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgBancoEnUso}";
            } else {
                return $e->getMessage();
            }
        }
    }
}