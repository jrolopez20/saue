<?php

class NomSucursalModel extends ZendExt_Model
{
    public function NomBancoModel()
    {
        parent::ZendExt_Model();
    }

    public function updateSucursal($idsucursal = null, $idbanco = null, $numero = '', $direccion = '')
    {
        try {
            $objDomain = new NomSucursal();
            if (!empty($idsucursal)) {
                $objDomain = $objDomain->findById($idsucursal);
            }
            $objDomain->idbanco = $idbanco;
            $objDomain->numero = $numero;
            $objDomain->direccion = $direccion;
            $objDomain->save();

            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgSucursalGuardada}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function deleteSucursal($idsucursal)
    {
        try {
            $objDomain = new NomSucursal();
            $objContenido = $objDomain->findById($idsucursal);
            $objContenido->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgSucursalEliminada}";
        } catch (Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':1,'mensaje':perfil.etiquetas.msgSucursalEnUso}";
            } else {
                return $e->getMessage();
            }
        }
    }

}

