<?php

/**
 * Clase controladora para gestionar los bancos y sucursales
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class BancosucursalController extends ZendExt_Controller_Secure
{
    public function init()
    {
        parent::init();
    }

    public function bancosucursalAction()
    {
        $this->render();
    }

    public function loadBancosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filters = json_decode($this->_request->getPost('filtros'));

        $objBanco = new NomBanco();
        $r['datos'] = $objBanco->getBancos($limit, $start, $filters);
        $r['cantidad'] = $objBanco->getCantBancos($filters);
        echo json_encode($r);
    }

    public function loadSucursalesAction()
    {
        $idbanco = $this->_request->getPost('idbanco');
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filters = json_decode($this->_request->getPost('filtros'));

        $objSucursal = new NomSucursal();
        $r['datos'] = $objSucursal->getSucursales($idbanco, $limit, $start, $filters);
        $r['cantidad'] = $objSucursal->getCantSucursales($idbanco, $filters);
        echo json_encode($r);
    }

    public function updateBancoAction()
    {
        $idbanco = $this->_request->getPost('idbanco');
        $codigo = $this->_request->getPost('codigo');
        $nombre = $this->_request->getPost('nombre');
        $abreviatura = $this->_request->getPost('abreviatura');

        $objModel = new NomBancoModel();
        $r = $objModel->updateBanco($idbanco, $codigo, $abreviatura, $nombre);
        echo $r;
    }

    public function updateSucursalAction()
    {
        $idsucursal = $this->_request->getPost('idsucursal');
        $idbanco = $this->_request->getPost('idbanco');
        $numero = $this->_request->getPost('numero');
        $direccion = $this->_request->getPost('direccion');

        $objModel = new NomSucursalModel();
        $r = $objModel->updateSucursal($idsucursal, $idbanco, $numero, $direccion);
        echo $r;
    }

    public function deleteBancoAction()
    {
        $idbanco = $this->_request->getPost('idbanco');

        $objModel = new NomBancoModel();
        $r = $objModel->deleteBanco($idbanco);
        echo $r;
    }

    public function deleteSucursalAction()
    {
        $idsucursal = $this->_request->getPost('idsucursal');

        $objModel = new NomSucursalModel();
        $r = $objModel->deleteSucursal($idsucursal);
        echo $r;
    }


}