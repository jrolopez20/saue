<?php

class BancoSucursalService
{
    public function getBancos($limit, $start, $filters)
    {
        $objDomain = new NomBanco();
        return $objDomain->getBancos($limit, $start, $filters);
    }

    public function getSucursales($idbanco, $limit, $start, $filters)
    {
        $objDomain = new NomSucursal();
        return $objDomain->getSucursales($idbanco, $limit, $start, $filters);
    }

} 