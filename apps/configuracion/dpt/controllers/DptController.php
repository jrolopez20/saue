<?php

/**
 * Clase controladora para gestionar el DPT
 * 
 * @author Team Delta
 * @package Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class DptController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new NomDptModel();
    }

    public function dptAction()
    {
        $this->render();
    }

    /**
     * call this model action
     */
    public function loadDptAction()
    {
        echo json_encode($this->model->loadDpt($this->_request->getPost()));
    }

    /**
     * call this model action
     */
    public function listDataPaisAction()
    {
        echo json_encode($this->model->listDataPais());
    }

    /**
     * call this model action
     */
    public function updateDptAction()
    {
        echo $this->model->updateDpt($this->_request->getPost());
    }

    /**
     * call this model action
     */
    public function deleteDptAction()
    {
        echo $this->model->deleteDpt($this->_request->getPost());
    }

}
