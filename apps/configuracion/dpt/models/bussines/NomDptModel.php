<?php

/**
 * Clase modelo para configurar los Dpt.
 * 
 * @author Team Delta
 * @package Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomDptModel extends ZendExt_Model
{

    public function NomDptModel()
    {
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los dpt por nivel
     * @param Array $argparams (node)
     * @return Array Dpt
     */
    public function loadDpt($argparams)
    {
        $NomDpt = new NomDpt();
        return $NomDpt->loadDpt((is_numeric($argparams['node']) ? $argparams['node'] : null), $argparams['idpais']);
    }

    /**
     * Obtiene el listado de paises
     * @return Array listado de paises
     */
    public function listDataPais()
    {
        return $this->integrator->configuracion->getPaises();
    }

    /**
     * Adiciona o modifica el DPT
     * @param Array $argparams (iddpt,idpadre,codigo,abreviatura,denominacion)
     * @return boolean true if ocurred, false if failure
     */
    public function updateDpt($argparams)
    {
        if ($argparams['iddpt']) {
            return $this->Actualizar($argparams);
        } else {
            return $this->Insertar($argparams);
        }
    }

    /**
     * Adiciona el DPT
     * @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function Insertar($argparams)
    {
        $objDpt = new NomDpt();
        $objDpt->codigo = $argparams['codigo'];
        $objDpt->abreviatura = $argparams['abreviatura'];
        $objDpt->denominacion = $argparams['denominacion'];
        $objDpt->idpadre = ($argparams['idpadre']) ? $argparams['idpadre'] : null;
        $objDpt->idpais = $argparams['idpais'];
        try {
            $objDpt->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Modifica el DPT
     * @param Array $argparams 
     * @return boolean true if ocurred, false if failure
     */
    public function Actualizar($argparams)
    {
        $objDpt = Doctrine::getTable('NomDpt')->find($argparams['iddpt']);
        $objDpt->codigo = $argparams['codigo'];
        $objDpt->abreviatura = $argparams['abreviatura'];
        $objDpt->denominacion = $argparams['denominacion'];
        try {
            $objDpt->save();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgGuardado}";
        } catch (Doctrine_Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Elimina el DPT
     * @param Integer $argparams (node)
     * @return boolean true if ocurred, false if failure
     */
    public function deleteDpt($argparams)
    {
        try {
            $objDpt = Doctrine::getTable('NomDpt')->find($argparams['iddpt']);
            $objDpt->delete();
            return "{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.msgEliminado}";
        } catch (Exception $exc) {
            if ($exc->getCode() == 23503) {//Code for foreignKeyViolation
                return "{'success':false, 'codMsg':3,'mensaje':perfil.etiquetas.msgGrupoUso}";
            } else {
                echo $exc->getTraceAsString();
            }
        }
    }

}
