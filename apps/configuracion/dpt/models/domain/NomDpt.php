<?php

/**
 * Clase dominio para configurar los Dpt.
 *
 * @author Team Delta
 * @package Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class NomDpt extends BaseNomDpt
{

    public function setUp()
    {
        parent:: setUp();
    }

    /**
     * Lista los datos de los Dpt
     * @param Integer $idDpt identificador del nodo padre
     * @param Integer $idPais identificador del pais
     * @return Array Listado de los Dpt
     */
    public function loadDpt($idDpt, $idPais)
    {
        $query = Doctrine_Query::create();
        $where = "n.idpais = $idPais AND ";
        $where .= ($idDpt) ? "n.idpadre = $idDpt AND n.idpadre <> n.iddpt" : "n.idpadre = n.iddpt";

        return $query->select("iddpt as id, CONCAT(CONCAT(codigo ,' '), denominacion) text, CONCAT(CONCAT(codigo ,' '),
        denominacion) as qtip, ( (n.ordender - n.ordenizq) = 1) as leaf, codigo, abreviatura, denominacion,
        iddpt, idpadre, true as expanded")
                        ->from('NomDpt n')
                        ->where($where)
                        ->orderby('n.codigo')
                        ->setHydrationMode(Doctrine::HYDRATE_ARRAY)
                        ->execute();
    }

    /**
     * Obtiene el clasificador de paises
     * @param Integer $idPais
     * @return Array listado de paises
     */
    public function getPaises($idPais = null)
    {
        $objDoctrine = Doctrine_Manager::getInstance();
        $connection = $objDoctrine->getCurrentConnection();
        $where = ($idPais) ? "WHERE np.idpais = " . $idPais : '';
        return $connection->fetchAll("SELECT np.* FROM mod_nomencladores.nom_pais np " . $where);
    }

}
