<?php

abstract class BaseNomDpt extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_nomencladores.nom_dpt');
        $this->hasColumn('iddpt', 'numeric', null,
                array('notnull' => false, 'primary' => true, 'sequence' => 'mod_nomencladores.sec_iddpt'));
        $this->hasColumn('idpadre', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('codigo', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('abreviatura', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ordenizq', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ordender', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idarbol', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idpais', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}
