<?php

class DptService
{

    /**
     * Obtiene el arbol de Dpt
     * @param Array $node
     * @return Array arbol de Dpt
     */
    public function getDpt($node, $idpais)
    {
        $NomDpt = new NomDpt();
        return $NomDpt->loadDpt((is_numeric($node) ? $node : null), $idpais);
    }

    /**
     * Obtiene el clasificador de paises
     * @param Integer $idPais
     * @return Array listado de paises
     */
    public function getPaises($idPais)
    {
        $NomDpt = new NomDpt();
        return $NomDpt->getPaises($idPais);
    }

}
