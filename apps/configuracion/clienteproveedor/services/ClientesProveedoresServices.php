<?php

class ClientesProveedoresServices
{

    /**
     * Obtiene los clientes de una entidad.
     *
     * @param int $estructura
     * @param string $filter
     * @param string $limit
     * @param string $filter
     * @return Array Clientes
     */
    public function getClientes($estructura, $start = 0, $limit = null, $filter = 0)
    {
        $where = "idestructura = $estructura AND  tipo IN(1,3)";
        if($filter != 0 && !empty($filter)){
            $filter = strtolower($filter);
            $where .= ' AND (lower(codigo) ILIKE \'%'.$filter.'%\' OR lower(nombre) ILIKE \'%'.$filter.'%\'
            OR lower(descripcion) ILIKE \'%'.$filter.'%\')';
        }
//        print_r(func_get_args());die;
        if(!is_null($limit) && $limit){
            return NomClientesproveedores::GetByConditionSQL($where, $start, $limit);
        }
        return NomClientesproveedores::GetByConditionSQL($where);
    }

    /**
     * Obtiene los provedores de una entidad.
     *
     * @param int $estructura
     * * @param string $filter
     * @return Array Proveedores
     */
    public function getProveedores($estructura, $filter = 0)
    {
        $where = "idestructura = $estructura AND tipo IN(2,3)";
        if(!is_null($filter)){
            $filter = strtolower($filter);
            $where .= ' AND (lower(codigo) ILIKE \'%'.$filter.'%\' OR lower(nombre) ILIKE \'%'.$filter.'%\'
            OR lower(descripcion) ILIKE \'%'.$filter.'%\')';
        }
        return NomClientesproveedores::GetByConditionSQL($where);
    }

    /**
     * Obtiene los clientes y provedores de una entidad.
     *
     * @param int $estructura
     * @return Array Clientes y Proveedores
     */
    public function getClientesProveedores($estructura, $query = null)
    {
        $where = "idestructura = $estructura";
        if(!is_null($query)){
            $query = strtolower($query);
            $where .= ' AND (lower(codigo) ILIKE \'%'.$query.'%\' OR lower(nombre) ILIKE \'%'.$query.'%\'
            OR lower(descripcion) ILIKE \'%'.$query.'%\')';
        }
        return NomClientesproveedores::GetByConditionSQL($where);
    }

    /**
     * Obtiene un cliente o provedores por un id.
     *
     * @param int $id
     * @return Array Cliente o Proveedors
     */
    public function getClienteProvById($id)
    {
        $res = NomClientesproveedores::GetByConditionSQL("idclientesproveedores = $id");
        if(is_array($res) && count($res)){
            return $res[0];
        }

        return $res;
    }

}
