<?php

class NomContacto extends BaseNomContacto
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasOne ('NomClientesproveedores', array ('local' => 'idclientesproveedores', 'foreign' => 'idclientesproveedores'));
    }

    public function persist()
    {
        try {

            $this->save();

            return $this->idcontacto;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomContacto')->execute();
            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomContacto')
                ->offset($offset)
                ->limit($limit)
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idcontacto, $toArray = false)
    {
        try {
            $result = Doctrine::getTable('NomContacto')->find($idcontacto);
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomContacto')
                ->whereIn("idcontacto", $arrIds)
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByCondition($condition, $params, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomContacto')
                ->where("$condition", $params)
//            print_r($result->getSqlQuery());die;
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function Erase($idcontacto)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomContacto')
                ->where("idcontacto=?", array($idcontacto))
                ->execute();
            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseSQL($idcontacto)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.nom_contacto WHERE idcontacto = ' . $idcontacto);
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomContacto')
                ->where("$conditions", $params)
                ->execute();
            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.nom_contacto WHERE ' . $conditions);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  0;
            } else {
                return $e->getMessage();
            }
        }
    }

}

