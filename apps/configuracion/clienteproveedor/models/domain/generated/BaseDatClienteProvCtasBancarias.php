<?php

abstract class BaseDatClienteProvCtasBancarias extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_clienteprov_ctasbancarias');
        $this->hasColumn('idcuentacliente', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_datclienteprov'));
        $this->hasColumn('banco', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('numerocuenta', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('predeterminada', 'numeric', null, array('notnull' => true, 'default' => 0,'primary' => false));
        $this->hasColumn('idclienteproveedor', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}

