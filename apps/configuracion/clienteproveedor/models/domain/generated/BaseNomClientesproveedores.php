<?php

abstract class BaseNomClientesproveedores extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_clientesproveedores');
        $this->hasColumn('idclientesproveedores', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_nomclientes'));
        $this->hasColumn('codigo', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('tipo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('telefono', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idpais', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('provincia', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('direccion', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('codpostal', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('sitioweb', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('movil', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fax', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idcuentacobrar', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idcuentapagar', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('creditoconcedido', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idempresa', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('foto', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ci', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('email', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('isempresa', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}

