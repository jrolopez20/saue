<?php

abstract class BaseNomContacto extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_contacto');
        $this->hasColumn('idcontacto', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_maestro.sec_nomcontacto'));
        $this->hasColumn('cargo', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('foto', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('email', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('telefono', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('movil', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('direccionempresa', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idpais', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('provincia', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('direccionpersonal', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('codpostal', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idclientesprovedores', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('ci', 'character varying', null, array('notnull' => true, 'primary' => false));
    }

}