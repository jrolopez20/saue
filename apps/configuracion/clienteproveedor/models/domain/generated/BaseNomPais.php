<?php

abstract class BaseNomPais extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_nomencladores.nom_pais');
        $this->hasColumn('idpais', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_nomencladores.sec_idpais'));
        $this->hasColumn('nombrepais', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('codigopais', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('siglas', 'character varying', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('nacionalidad', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}

