<?php

class DatClienteProvCtasBancarias extends BaseDatClienteProvCtasBancarias
{

    public function setUp()
    {
        parent :: setUp ();
    }

    public function persist()
    {
        try {
            $this->save();
            return $this->idcuentacliente;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatClienteProvCtasBancarias')->orderBy('numerocuenta')->execute();
            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatClienteProvCtasBancarias')
                ->offset($offset)
                ->limit($limit)
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idcuentacliente, $toArray = false)
    {
        try {
            $result = Doctrine::getTable('DatClienteProvCtasBancarias')->find($idcuentacliente);
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatClienteProvCtasBancarias')
                ->whereIn("idcuentacliente", $arrIds)
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByCondition($condition, $params, $toArray = false)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatClienteProvCtasBancarias')
                ->where("$condition", $params)
                ->execute();
            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function Erase($idcuentacliente)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatClienteProvCtasBancarias')
                ->where("idcuentacliente = ?", array($idcuentacliente))
                ->execute();
            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }


    static public function EraseSQL($idcuentacliente)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.dat_clienteprov_ctasbancarias WHERE idcuentacliente =' . $idcuentacliente);
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatClienteProvCtasBancarias')
                ->where("$conditions", $params)
                ->execute();
            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.dat_clienteprov_ctasbancarias WHERE ' . $conditions);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  0;
            } else {
                return $e->getMessage();
            }
        }
    }

}

