<?php

class NomClientesproveedores extends BaseNomClientesproveedores
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomClientesproveedores', array('local' => 'idempresa', 'foreign' => 'idclientesproveedores'));
        $this->hasMany('NomClientesproveedores', array('local' => 'idclientesproveedores', 'foreign' => 'idempresa'));
        $this->hasMany('BaseNomContacto', array('local' => 'idclientesproveedores', 'foreign' => 'idclientesproveedores'));
    }

    public function persist()
    {
        try {
//            $a = Doctrine_Manager::getInstance();
//            $va = $a->getCurrentConnection();

            $this->save();
//
//            $va->commit();
//            $va->beginTransaction();

            return $this->idclientesproveedores;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomClientesproveedores')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomClientesproveedores')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idclientesproveedores, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('NomClientesproveedores')->find($idclientesproveedores);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomClientesproveedores')
                ->whereIn('idclientesproveedores', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomClientesproveedores')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    /**
     * @param $conditions
     * @return array
     */
    static public function GetByConditionSQL($conditions, $start = NULL, $limit = NULL, $loadfoto = true)
    {
        $atributos = 'cp.idclientesproveedores, cp.codigo, cp.nombre, cp.descripcion, cp.tipo, cp.telefono, cp.idpais, cp.direccion, cp.codpostal,'
            . 'cp.sitioweb, cp.movil, cp.fax, cp.idcuentacobrar, cp.idcuentapagar, cp.creditoconcedido, cp.idempresa, cp.idestructura, cp.email,'
            . 'cp.isempresa, cp.provincia, cp.ci';

        if($loadfoto){
            $atributos .= ', cp.foto';
        }

        $select = $atributos.', (CASE WHEN cp.idempresa IS NOT NULL THEN
	                (SELECT e.nombre FROM mod_maestro.nom_clientesproveedores e WHERE cp.idempresa = e.idclientesproveedores)
	                ELSE null END) as empresa'
            . ', (CASE WHEN cp.idcuentacobrar IS NOT NULL THEN
	                (SELECT nc.concatcta ||\' \'|| nc.denominacion FROM mod_contabilidad.nom_cuenta nc WHERE nc.idcuenta = cp.idcuentacobrar)
	                ELSE null END) as cuentacobrar'
            . ', (CASE WHEN cp.idcuentapagar IS NOT NULL THEN
	                (SELECT nc1.concatcta ||\' \'|| nc1.denominacion FROM mod_contabilidad.nom_cuenta nc1 WHERE nc1.idcuenta = cp.idcuentapagar)
	                ELSE null END) as cuentapagar'
            . ', (CASE WHEN cp.idpais IS NOT NULL THEN
	                (SELECT np.nombrepais ||\' (\'|| np.siglas ||\')\' FROM mod_nomencladores.nom_pais np WHERE cp.idpais = np.idpais)
	                ELSE \' \' END) as pais';

        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $start = !is_null($start) ? 'OFFSET ' . $start : '';
            $limit = !is_null($limit) ? 'LIMIT ' . $limit : '';
            $data_return = $connection->fetchAll('SELECT ' . $select . ' FROM mod_maestro.nom_clientesproveedores cp '
                . "WHERE $conditions ORDER BY cp.nombre ASC $start $limit;");
        } catch (Doctrine_Exception $e) {
            throw $e;
        }

        return $data_return;
    }

    static public function Erase($idclientesproveedores)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomClientesproveedores')
                ->where("idclientesproveedores = $idclientesproveedores")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($idclientesproveedores)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.nom_clientesproveedores WHERE idclientesproveedores = ' . $idclientesproveedores);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomClientesproveedores')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.nom_clientesproveedores WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

}

