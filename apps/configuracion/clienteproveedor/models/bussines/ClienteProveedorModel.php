<?php

/**
 * Clase modelo que implementa el negocio concerniente a la gestión de
 * clientes y proveedores.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class ClienteProveedorModel extends ZendExt_Model
{

    private $domain;

    public function ClienteProveedorModel()
    {
        $this->domain = new NomClientesproveedores();
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los clientes o proveedores de la entidad.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * @param boolean $comun en caso de tomar valor true se cargan los campos
     * contables comunes para todos los subsistemas
     * @throws Doctrine_Exception
     * @return mixed Arreglo con los Campos contables
     */
    public function load($post)
    {
        $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í", "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
        $estructura = $this->global->Estructura->idestructura;

        $conditions = array("idestructura = $estructura");

        if (!empty($post['filter'])) {
            $filter = $post['filter'];
            $conditions[] = "(lower(nombre) ILIKE lower('%$filter%') OR lower(descripcion) ILIKE lower('%$filter%'))";
        }

        /* Obtener los clientes o proveedores */
        $conditions = implode(" AND ", $conditions);
        $data = NomClientesproveedores::GetByConditionSQL($conditions);

        if (count($data)) {
            foreach ($data as &$clienteprov) {
                /* Obtener los contactos paralas empresas */

                $clienteprov['contactos'] = ($clienteprov['isempresa'] == 1) ?
                    NomContacto::GetByCondition('idclientesprovedores = ?', array($clienteprov['idclientesproveedores']), TRUE)
                    : array();

                if (!empty($post['filter'])) {
                    $clienteprov['nombre'] = str_replace(strtr($filter, $utf8), "<span style=\"background-color: rgb(255, 255, 0);\">" . strtr($filter, $utf8) . "</span>", strtr($clienteprov['nombre'], $utf8));
                    $clienteprov['descripcion'] = str_replace(strtr($filter, $utf8), "<span style=\"background-color: rgb(255, 255, 0);\">" . strtr($filter, $utf8) . "</span>", strtr($clienteprov['descripcion'], $utf8));
                }
            }
        }

        $paging = array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );

        return $paging;
    }


    /**
     * Eliminar uno o clientes o proveedores.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * (almacena los ids de los clientes o provedores en el índice 'ids')
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function delete($post)
    {
        $response = array('errors' => array(), 'success' => array());
        if (!empty($post['ids'])) {
            $ids = json_decode(stripslashes($post['ids']));
            $count = count($ids);

            $implode = implode(',', $ids);
            /* Borrar las dependencas */
            NomContacto::EraseByConditionSQL("idclientesprovedores IN($implode)");
            DatClienteProvCtasBancarias::EraseByConditionSQL("idclienteproveedor IN($implode)");

            foreach ($ids as $idclientesproveedores) {
                $result = NomClientesproveedores::EraseSQL($idclientesproveedores);
                if (!$result) {
                    /* An error was ocurred */
                    //No se ha podido eliminar el cliente o proveedor
                    $response['errors'][] = array('code' => 'DELCP03', 'id' => $idclientesproveedores);
                    $response['success'] = TRUE;
                    $response['codMsg'] = 3;
                    $response['mensaje'] = $count > 1 ? 'perfil.etiquetas.msgDelFAILInUSE' : 'perfil.etiquetas.msgDelFAILInUSE';
                } else {
//                    $response['success'] = TRUE;
//                    $response['codMsg'] = 1;
                    $msg = $count > 1 ? 'perfil.etiquetas.msgDelOKP' : 'perfil.etiquetas.msgDelOK';
                    return "{'success':true, 'codMsg':1, 'mensaje': $msg}";
                }
            }
        } else {
            /* No data to delete */
            $response['errors'][] = array('code' => 'DELCP02');
            return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgDelNoData}";
        }

        return $response;
    }

    /**
     * Inserta y modofica un cliente o proveedor.
     *
     * @param array $post Arreglo con los datos del cliente o del proveedor
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function save($post)
    {
        $response = array('errors' => array(), 'success' => array());

        if (!empty($post['nombre']) && !empty($post['codigoidentificacion'])
            && (!empty($post['cliente']) || !empty($post['proveedor']))
        ) {
            $flagmodify = FALSE;
            $estructura = $this->global->Estructura->idestructura;

            if (!empty($post['idclienteproveedor']) && is_numeric($post['idclienteproveedor'])) {
                /* Si se esta modificando */
                $objeto = NomClientesproveedores::GetById($post['idclienteproveedor']);
                $flagmodify = TRUE;
            } else {
                $objeto = new NomClientesproveedores();
            }

            $dataCont = json_decode(stripslashes($post['contabilidad']));
            $dataContacto = json_decode(stripslashes($post['contactos']));

            if (!$flagmodify) {
                $alreadyExist = $this->alreadyExist($post, $estructura);
                if ($alreadyExist) {
                    /* Ya existe el cliente o proveedor */
                    $response['success'] = FALSE;
                    $response['errors'][] = array('code' => 'CP01', 'ci' => $post['codigoidentificacion']);

                    return $response;
                }
            } else {
                /* Eliminar los contactos que a eliminar */
                if (!empty($dataCont->deletedAccounts)) {
                    $this->delBankAccount($dataCont->deletedAccounts);
                }
                /* Eliminar los contactos que a eliminar */
                if (!empty($dataContacto->deletedAccounts)) {
                    $this->delContact($dataContacto->deletedContacts);
                }
            }

            $tipo = 0;
            if (!empty($post['cliente'])) {
                $tipo += 1;
            }
            if (!empty($post['proveedor'])) {
                $tipo += 2;
            }

            $objeto->nombre = $post['nombre'];
            $objeto->tipo = $tipo;
            $objeto->ci = $post['codigoidentificacion'];
            $objeto->codigo = $post['codigoidentificacion'];
            $objeto->descripcion = !empty($post['descripcion']) ? $post['descripcion'] : NULL;
            $objeto->telefono = !empty($post['telefono']) ? $post['telefono'] : NULL;
            $objeto->idpais = !empty($post['idpais']) ? $post['idpais'] : NULL;
            $objeto->provincia = !empty($post['idprovincia']) ? $post['idprovincia'] : NULL;
            $objeto->direccion = !empty($post['direccion']) ? $post['direccion'] : NULL;
            $objeto->codpostal = !empty($post['codigopostal']) ? $post['codigopostal'] : NULL;
            $objeto->sitioweb = !empty($post['sitioweb']) ? $post['sitioweb'] : NULL;
            $objeto->movil = !empty($post['movil']) ? $post['movil'] : NULL;
            $objeto->fax = !empty($post['fax']) ? $post['fax'] : NULL;
            $objeto->idcuentacobrar = !empty($dataCont->idcuentacobrar) ? $dataCont->idcuentacobrar : NULL;
            $objeto->idcuentapagar = !empty($dataCont->idcuentapagar) ? $dataCont->idcuentapagar : NULL;
            $objeto->creditoconcedido = !empty($dataCont->credito) ? $dataCont->credito : NULL;
            $objeto->idempresa = !empty($post['idempresa']) && is_numeric($post['idempresa']) ? $post['idempresa'] : NULL;
            $objeto->isempresa = !empty($post['empresa']) ? 1 : NULL;
            $objeto->idestructura = $estructura;
            $objeto->foto = !empty($post['foto']) ? $post['foto'] : NULL;

            $idclientesproveedores = $objeto->persist();

            if (!is_numeric($idclientesproveedores)) {
                /* Ocurrio un error al insertar el cliente o proveedor */
//                $response['success'] = FALSE;
//                $response['errors'][] = array('code' => 'CP02', 'nombre' => $post['nombre']);
                return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgAddFAIL}";
            } else {
                /* agregando las cuentas bancarias */
                if (!empty($dataCont->modifiedAccounts)) {
                    $resBA = $this->addBankAccount($dataCont->modifiedAccounts, $idclientesproveedores);
                    $response['errors'] = array_merge($response['errors'], $resBA['errors']);
                }
                if (!empty($post['empresa']) && !empty($dataContacto->modifiedContacts)) {
                    $res = $this->addContacts($dataContacto->modifiedContacts, $idclientesproveedores);
                    $response['errors'] = array_merge($response['errors'], $res['errors']);
                }
//                $response['success'][] = array('nombre' => $post['nombre']);
                if ($tipo == 1) {
                    $mensaje = $flagmodify ? 'perfil.etiquetas.msgModCOK' : 'perfil.etiquetas.msgAddCOK';
                } elseif ($tipo == 2) {
                    $mensaje = $flagmodify ? 'perfil.etiquetas.msgModPOK' : 'perfil.etiquetas.msgAddPOK';
                } else {
                    $mensaje = $flagmodify ? 'perfil.etiquetas.msgModCPOK' : 'perfil.etiquetas.msgAddCPOK';
                }

                return "{'success':true, 'codMsg':1, 'mensaje': $mensaje}";
            }
        } else {
            /* Faltan datos para adicionar el campo contable */
//            $response['success'] = false;
//            $response['errors'][] = array('code' => 'CP03');
            return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgAddNoData}";
        }

        return $response;
    }

    /**
     * Obtiene los clientes y/o proveedores ue son compannias..
     *
     * @param array $post Arreglo con los datos de la peticion
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getCompanies($post)
    {
        $estructura = $this->global->Estructura->idestructura;
        $type = !empty($post['type']) ? (is_array($post['type']) ? $post['type'] : array($post['type'])) : array(1, 2, 3);
        $conditions = 'idestructura = ? AND isempresa = 1 AND tipo IN(' . implode(',', $type) . ')';
        $params = array($estructura);

        $data = $this->domain->GetByCondition($conditions, $params, TRUE);

        return array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );
    }

    /**
     * Obtiene los clientes y/o proveedores ue son compannias.
     *
     * @param array $post Arreglo con los datos de la peticion
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getCountries()
    {
        $data = NomPais::GetAll();

        return array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );
    }

    /**
     * Obtiene el arbol de cuentas.
     *
     * @param array $post Arreglo con los datos de la peticion
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getAccounts($post = NULL)
    {
        $filter = !empty($post['query']) ? $post['query'] : -1;
        $data = $this->integrator->contabilidad->getCuentasHojas($filter);

        return array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );
    }

    /**
     * Obtiene todas las cuents bancarias.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getBankAccounts()
    {
        $estructura = $this->global->Estructura->idestructura;
        $data = $this->integrator->banco->getCuentasBancarias($estructura);

        return array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );
    }

    /**
     * Codifica una imagen subida al servidor a base64.
     *
     * @param string $source Cadena con la ruta a la imagen
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function uploadedImg2Base64($source)
    {
        $response = array('base64img' => -1);
        if ($_FILES['photoname']["error"] == 0) {
            $fp = fopen($source, "r");
            $contents = file_get_contents($source);
            $base64 = base64_encode($contents);
            fclose($fp);
            $response = array('base64img' => "$base64", 'success' => TRUE);
        }

        return $response;
    }

    /**
     * Funcion auxiliar que registra uno o mas contactos para un cliente.
     *
     * @param array $contactos Arreglo con los datos del contacto
     * (pueder ser uno o mas)
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function addContacts($contactos, $idclientesprovedores)
    {
        $response = array('errors' => array(), 'success' => array());

        if (empty($contactos)) {
            $response['errors'][] = array('code' => 'CTO0');

            return $response;
        }

        foreach ($contactos as $contacto) {

            if (!empty($contacto->nombre) && !empty($contacto->codigocontacto)) {
                $flagmodify = FALSE;
                if (!empty($contacto->idcontacto) && is_numeric($contacto->idcontacto)) {
                    $objeto = NomContacto::GetById($contacto->idcontacto);
                    $flagmodify = TRUE;
                } else {
                    $objeto = new NomContacto();
                }

                if (!$flagmodify) {
                    $condition = 'ci = ?';
                    $params = array($contacto->codigocontacto);
                    $check = NomContacto::GetByCondition($condition, $params, TRUE);
                    if ($check) {
                        /* Ya existe un cliente */
                        $response['errors'][] = array('code' => 'CTO1', 'nombre' => $contacto->nombre);

                        return $response;
                    }
                }

                $objeto->nombre = $contacto->nombre;
                $objeto->ci = $contacto->codigocontacto;
                $objeto->cargo = !empty($contacto->cargo) ? $contacto->cargo : NULL;
                $objeto->email = !empty($contacto->email) ? $contacto->email : NULL;
                $objeto->telefono = !empty($contacto->telefono) ? $contacto->telefono : NULL;
                $objeto->movil = !empty($contacto->movil) ? $contacto->movil : NULL;
                $objeto->direccionempresa = ($contacto->direccionempresa) ? $contacto->direccionempresa : NULL;
                $objeto->idpais = !empty($contacto->idpais) ? $contacto->idpais : NULL;
                $objeto->provincia = !empty($contacto->provincia) ? $contacto->provincia : NULL;
                $objeto->direccionpersonal = !empty($contacto->direccion) ? $contacto->direccion : NULL;
                $objeto->idclientesprovedores = $idclientesprovedores;
                $objeto->codpostal = !empty($contacto->codigopostal) ? $contacto->codigopostal : NULL;
                $objeto->foto = !empty($contacto->foto) ? $contacto->foto : NULL;

                $idcontacto = $objeto->persist();

                if (!is_numeric($idcontacto)) {
                    /* an error was ocurred */
                    $response['errors'][] = array('code' => 'CTO2', 'nombre' => $contacto->nombre);
                }
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar que registra uno o mas cuentas bancarias para un cliente o proveedor.
     *
     * @param array $cuentas Arreglo con los datos de las cuentas
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function addBankAccount($cuentas, $idclientesprovedores)
    {
        $response = array('errors' => array(), 'success' => array());
        if (empty($cuentas)) {
            $response['errors'][] = array('code' => 'BA0');

            return $response;
        }

        foreach ($cuentas as $cuenta) {
            $flagmodify = FALSE;
            /* para cada cuenta */
            if (!empty($cuenta->idcuentacliente) && is_numeric($cuenta->idcuentacliente)) {
                /* Si se esta modificando */
                $objeto = DatClienteProvCtasBancarias::GetById($cuenta->idcuentacliente);
                $flagmodify = TRUE;
            } else {
                $objeto = new DatClienteProvCtasBancarias();
            }

            if (!$flagmodify) {
                $conditions = '(lower(numerocuenta) ILIKE lower(\'%' . $cuenta->numerocuenta . '%\') AND idclienteproveedor =' . $idclientesprovedores . ')';
                $alreadyExist = DatClienteProvCtasBancarias::GetByCondition($conditions, array(), TRUE);
                if ($alreadyExist) {
                    /* Ya existe el cliente o proveedor */
                    $response['errors'][] = array('code' => 'BA1', 'nombre' => $cuenta->numerocuenta);

                    return $response;
                }
            }

            $objeto->numerocuenta = !empty($cuenta->numerocuenta) ? $cuenta->numerocuenta : NULL;
            $objeto->banco = !empty($cuenta->banco) ? $cuenta->banco : NULL;
            $objeto->predeterminada = !empty($cuenta->predeterminada) ? $cuenta->predeterminada : NULL;
            $objeto->idclienteproveedor = $idclientesprovedores;

            $idcuentacliente = $objeto->persist();

            if (!is_numeric($idcuentacliente)) {
                $response['errors'][] = array('code' => 'BA2', 'nombre' => $cuenta->numerocuenta);
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar que elimina uno o mas cuentas bancarias de un cliente o proveedor.
     *
     * @param array $idcuenta Arreglo con los ids de las cuentas bancarias
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function delBankAccount($idcuenta)
    {
        $ids = (is_array($idcuenta)) ? $idcuenta : array($idcuenta);
        $condition = 'idcuentacliente IN(' . implode(',', $ids) . ')';
//        $response = DatClienteProvCtasBancarias::EraseByCondition($condition, array());
        $response = DatClienteProvCtasBancarias::EraseByConditionSQL($condition);

        return $response;
    }

    /**
     * Funcion auxiliar que elimina uno o mas cuentas bancarias de un cliente o proveedor.
     *
     * @param array $idcontacto Arreglo con los ids de las cuentas bancarias
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function delContact($idcontacto)
    {
        $ids = (is_array($idcontacto)) ? $idcontacto : array($idcontacto);
        $condition = 'idcontacto IN(' . implode(',', $ids) . ')';
//        $response = NomContacto::EraseByCondition($condition, array());
        $response = NomContacto::EraseByConditionSQL($condition);

        return $response;
    }

    /**
     * Funcion auxiliar que verifica si ua existe el cliente o proveedor
     * que se esta intentando registrar.
     *
     * @param array $post Arreglo con los datos de la petición.
     * @param int $estructura Arreglo con los datos de la petición.
     * @return boolean TRUE o FALSO si existe o no.
     */
    private function alreadyExist($post, $estructura)
    {
        if (!empty($post['idempresa']) && is_numeric($post['idempresa'])) {
//            $condition = 'idestructura = ? AND (codigo = ? OR nombre ILIKE lower(\'%'.$post['nombre'].'%\'))';
            $condition = 'idestructura = ? AND (codigo = ? OR nombre = ?)';
            $params = array($estructura, $post['codigo'], $post['nombre']);
        } else {
            $condition = 'idestructura = ? AND (codigo = ? OR ci = ?)';
            $params = array($estructura, $post['codigoidentificacion'], $post['codigoidentificacion']);
        }
        $check = NomClientesproveedores::GetByCondition($condition, $params, TRUE);

        return count($check) > 0;
    }

}
