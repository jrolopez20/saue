<?php

/**
 * Clase controladora para gestionar los clieste y proveedores
 *
 * @author Team Delta
 * @package Datos Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class GestclienteproveedorController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new ClienteProveedorModel();
        parent::init();
    }

    /**
     * Carga la interfaz correspondiente a la gestion de clientes
     */
//    public function gestclienteAction()
    public function gestclienteproveedorAction()
    {
        $this->view->type = '1';
        $this->render();
    }

    /**
     * Carga la interfaz correspondiente a la gestion de proveedores
     */
    public function gestproveedorAction()
    {
//        $this->view->type = '2';
        $this->render();
    }

    /**
     * Carga los clientes y proveedores.
     */
    public function loadAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->load($post);
        echo json_encode($data);
    }

    /**
     * Registra un cliente o proveedor.
     */
    public function saveAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->save($post);
        echo ($data);
    }

    /**
     * Eliminar uno o varios clientes y proveedores.
     */
    public function deleteAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->delete($post);
        echo (is_array($data)) ? json_encode($data) : $data;
    }

    /**
     * Codifica una imagen subida al servidor a base64
     * @return mixed Arreglo con la respuesta del servidor (el indice 'base64img' almacena
     * la imagen codificada a base4 o -1 si no existe el fichero en la ruta especificada)
     */
    public function getPictureAction()
    {
        $dir_file = $_FILES["photoname"]["tmp_name"];
        $response = array('base64img' => -1);
        if (file_exists($dir_file))
        {
            $response = $this->model->uploadedImg2Base64($dir_file);
        }
        echo json_encode($response);
    }

    /**
     * Obtiene los clientes y/o proveedores ue son compannias.
     *
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getCompaniesAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->getCompanies($post);
        echo json_encode($data);
    }

    /**
     * Obtiene los paises.
     *
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getCountriesAction()
    {
        $data = $this->model->getCountries();
        echo json_encode($data);
    }

    /**
     * Obtiene el arbol de cuenta.
     *
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getAccountsAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->getAccounts($post);
        echo json_encode($data);
    }

    /**
     * Obtiene todas las cuents bancarias.
     *
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getBankAccountsAction()
    {
        $data = $this->model->getBankAccounts();
        echo json_encode($data);
    }
}