<?php

class TerminesdepagoService
{

    /**
     * Obtiene las Cuentas por su id.
     *
     * @param Array $arrids
     * @return Array Terminos de pago
     */
    public function getTerminosdepago($idestructura, $start = 0, $limit = null,$filter = 0)
    {
        $condition = 'idestructura = ?';
        $params = array($idestructura);
        if($filter != 0 && !empty($filter)){
            $filter = strtolower($filter);
            $condition .= ' AND (lower(nombre) ILIKE \'%'.$filter.'%\')';
        }
        $model = new NomTerminosPago();
        $res = $model->GetByCondition($condition, $params, TRUE);

        if(empty($res)){
            return $res;
        }

        if(!is_null($limit) && $limit){
            return array_splice($res,$start, $limit);
        }

        return $res;
    }
}
