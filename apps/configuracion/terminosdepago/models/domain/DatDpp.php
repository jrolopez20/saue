<?php

class DatDpp extends BaseDatDpp
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomTerminosPago', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->iddpp;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('DatDpp')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatDpp')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($iddpp, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatDpp')->find($iddpp);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatDpp')
                ->whereIn('iddpp', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatDpp')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function Erase($iddpp)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatDpp')
                ->where("iddpp = $iddpp")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($iddpp)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.dat_dpp WHERE iddpp = '.$iddpp);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatDpp')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.dat_dpp WHERE ' . $conditions);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  0;
            } else {
                return $e->getMessage();
            }
        }
    }

    public function EraseByIdTermino($idterminopago)
    {
        $array = (is_array($idterminopago)) ? $idterminopago : array($idterminopago);
        return $this->EraseByConditionSQL('idterminodepago IN( ' . implode(',', $array)) . ')';
    }

}