<?php

abstract class BaseDatMora extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_mora');
        $this->hasColumn('idmora', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_facturacion.sec_mora'));
        $this->hasColumn('porciento', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('dias', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('aclaraciones', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}

