<?php

abstract class BaseDatDescuentosVenta extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_descuentosventa');
        $this->hasColumn('iddescuentosventa', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_facturacion.sec_descventa'));
        $this->hasColumn('porciento', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('min', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('aclaraciones', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}

