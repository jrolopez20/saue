<?php

abstract class BaseNomTerminosPago extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.nom_terminosdepago');
        $this->hasColumn('idterminodepago', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_facturacion.sec_nomterminosdepago'));
        $this->hasColumn('aplazos', 'numeric', null, array('notnull' => false, 'primary' => false, 'default' => 0));
        $this->hasColumn('diafijo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('plazo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('fechavencimiento', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descventa', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('dpp', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idumplazo', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('nombre', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('predeterminado', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}

