<?php

abstract class BaseDatCuotasTerminosdePago extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_cuotas_terminosdepago');
        $this->hasColumn('idcuota', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_facturacion.sec_cuotas'));
        $this->hasColumn('idterminoaplicable', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('porciento', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('aclaraciones', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}

