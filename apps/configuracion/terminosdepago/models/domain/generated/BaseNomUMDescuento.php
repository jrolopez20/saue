<?php

abstract class BaseNomUMDescuento extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.nom_umdescuentos');
        $this->hasColumn('idumdescuento', 'numeric', null, array('notnull' => true, 'primary' => true));
        $this->hasColumn('umdescuento', 'character varying', null, array('notnull' => false, 'primary' => false));
    }

}

