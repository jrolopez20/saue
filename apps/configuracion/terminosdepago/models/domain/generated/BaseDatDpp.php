<?php

abstract class BaseDatDpp extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_facturacion.dat_dpp');
        $this->hasColumn('iddpp', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_facturacion.sec_dpp'));
        $this->hasColumn('porciento', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('dias', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('aclaraciones', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idterminodepago', 'numeric', null, array('notnull' => true, 'primary' => false));
    }

}

