<?php

class NomUMPlazo extends BaseNomUMPlazo
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomTerminosPago', array('local' => 'idumplazo', 'foreign' => 'idumplazo'));
    }

    public function persist()
    {
        try {
            $this->save();

            return $this->idumplazo;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->select("idumplazo, denominacion")->from('NomUMPlazo')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomUMPlazo')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idumplazo, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('NomUMPlazo')->find($idumplazo);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomUMPlazo')
                ->whereIn('idumplazo', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomUMPlazo')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function Erase($idumplazo)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomUMPlazo')
                ->where("idterminodepago = $idumplazo")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }


    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomUMPlazo')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_umplazo WHERE ' . $conditions);
            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return  0;
            } else {
                throw $e;
            }
        }
    }

}