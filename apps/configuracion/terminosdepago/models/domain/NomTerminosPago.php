<?php

class NomTerminosPago extends BaseNomTerminosPago
{

    public function setUp()
    {
        parent:: setUp();
//        $this->hasOne('NomClientesproveedores', array('local' => 'idempresa', 'foreign' => 'idclientesproveedores'));
        $this->hasMany('DatCuotas', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
        $this->hasMany('DatDescuentosVenta', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
        $this->hasMany('DatDpp', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
        $this->hasMany('DatMora', array('local' => 'idterminodepago', 'foreign' => 'idterminodepago'));
        $this->hasMany('NomUMPlazo', array('local' => 'idumplazo', 'foreign' => 'idumplazo'));
    }

    public function persist()
    {
        try {
//            $a = Doctrine_Manager::getInstance();
//            $va = $a->getCurrentConnection();

            $this->save();

//            $va->commit();
//            $va->beginTransaction();

            return $this->idterminodepago;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('NomTerminosPago')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomTerminosPago')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idterminodepago, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('NomTerminosPago')->find($idterminodepago);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomTerminosPago')
                ->whereIn('idterminodepago', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomTerminosPago')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    /**
     * @param $conditions
     * @return array
     */
    static public function GetByConditionSQL($conditions)
    {
        $select = 'tp.*,um.denominacion as umplazo';

        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $data_return = $connection->fetchAll('SELECT ' . $select . ' FROM mod_facturacion.nom_terminosdepago tp '
                . 'LEFT JOIN mod_facturacion.nom_umplazo um ON tp.idumplazo = um.idumplazo '
                . "WHERE $conditions ORDER BY tp.fechavencimiento ASC;");
        } catch (Doctrine_Exception $e) {
            throw $e;
        }

        return $data_return;
    }

    static public function Erase($idterminodepago)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomTerminosPago')
                ->where("idterminodepago = $idterminodepago")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($idterminodepago)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_terminosdepago WHERE idterminodepago = ' . $idterminodepago);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomTerminosPago')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_facturacion.nom_terminosdepago WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function UpdateSQL($fields, $conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('UPDATE mod_facturacion.nom_terminosdepago SET '.implode(',',$fields).' WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

}