<?php

/**
 * Clase modelo que implementa el negocio concerniente a la gestión de
 * terminos de pago.
 *
 * @author Team Delta
 * @package Contabilidad
 * @copyright Ecotec
 * @version 1.0-0
 */
class TerminosdepagoModel extends ZendExt_Model
{

    private $domain;

    public function __construct()
    {
        $this->domain = new NomTerminosPago();
        parent::ZendExt_Model();
    }

    /**
     * Obtiene los terminos de pago de la entidad.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * @param boolean $onlySelectables Boleano que indica si cargar solo los terminos que no han vencido o
     * no tienen fecha de vencimiento definida
     * @throws Doctrine_Exception
     * @return mixed Arreglo con los Campos contables
     */
    public function load($post, $onlySelectables = FALSE)
    {
        $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í", "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
        $estructura = $this->global->Estructura->idestructura;

        $conditions = array("idestructura = $estructura");

        if (!empty($post['filter'])) {
            $filter = $post['filter'];
            $conditions[] = "(lower(nombre) ILIKE lower('%$filter%'))";
        }

        if ($onlySelectables) {
            $conditions[] = "(fechavencimiento >= NOW() or fechavencimiento IS NULL)";
        }

        /* Obtener los terminos de pago */
        $conditions = implode(" AND ", $conditions);
        $data = NomTerminosPago::GetByConditionSQL($conditions);

        if (count($data) && !$onlySelectables) {
            foreach ($data as &$termino) {
                $termino['cuotas'] = DatCuotas::GetByCondition('idterminodepago = ?', array($termino['idterminodepago']), TRUE);
                $termino['moras'] = DatMora::GetByCondition('idterminodepago = ?', array($termino['idterminodepago']), TRUE);
                $termino['descuentosxpp'] = DatDpp::GetByCondition('idterminodepago = ?', array($termino['idterminodepago']), TRUE);
                $termino['descuentosventa'] = DatDescuentosVenta::GetByCondition('idterminodepago = ?', array($termino['idterminodepago']), TRUE);
//                $termino['hasaclaraciones'] = false;


                if (!empty($post['filter'])) {
                    $termino['nombre'] = str_replace(strtr($filter, $utf8), "<span style=\"background-color: rgb(255, 255, 0);\">" . strtr($filter, $utf8) . "</span>", strtr($termino['nombre'], $utf8));
                }
            }
        }

        $paging = array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );

        return $paging;
    }

    /**
     * Obtiene los terminos de pago de la entidad que no han vencido o no tienen fecha de vencimiento definida.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con los Campos contables
     */
    public function loadSelectablesTerms()
    {

    }


    /**
     * Eliminar uno o terminos de pago.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * (almacena los ids de los terminos de pago en el índice 'ids')
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function delete($post)
    {
        $response = array('errors' => array(), 'success' => array());
        if (!empty($post['ids'])) {
            $ids = json_decode(stripslashes($post['ids']));
            $count = count($ids);

            $implode = implode(',', $ids);
            /* Borrar las dependencas */
            $modelMora = new DatMora();
            $modelMora->EraseByConditionSQL("idterminodepago IN($implode)");
            $modelDpp = new DatDpp();
            $modelDpp->EraseByConditionSQL("idterminodepago IN($implode)");
            $modelDescV = new DatDescuentosVenta();
            $modelDescV->EraseByConditionSQL("idterminodepago IN($implode)");
            $modelCuotas = new DatCuotas();
            $modelCuotas->EraseByConditionSQL("idterminodepago IN($implode)");

            foreach ($ids as $idterminodepago) {
                $result = NomTerminosPago::EraseSQL($idterminodepago);
                if (!$result) {
                    /* An error was ocurred */
                    //No se ha podido eliminar el termino de pago
                    $response['errors'][] = array('code' => 'DELTP03', 'id' => $idterminodepago);
                    $response['success'] = TRUE;
                    $response['codMsg'] = 3;
                    $response['mensaje'] = $count > 1 ? 'perfil.etiquetas.msgDelFAILInUSEP' : 'perfil.etiquetas.msgDelFAILInUSE';
                } else {
//                    $response['success'] = TRUE;
//                    $response['codMsg'] = 1;
                    $msg = $count > 1 ? 'perfil.etiquetas.msgDelOKP' : 'perfil.etiquetas.msgDelOK';

                    return "{'success':true, 'codMsg':1, 'mensaje': $msg}";
                }
            }
        } else {
            /* No data to delete */
            $response['errors'][] = array('code' => 'DELCP02');

            return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgDelNoData}";
        }

        return $response;
    }

    /**
     * Inserta y modofica un termino de pago.
     *
     * @param array $post Arreglo con los datos del cliente o del proveedor
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function save($post)
    {
        $response = array('errors' => array(), 'success' => array());

        if (!empty($post['nombre'])) {
            $flagmodify = FALSE;
            $estructura = $this->global->Estructura->idestructura;

            if (!empty($post['idterminodepago']) && is_numeric($post['idterminodepago'])) {
                /* Si se esta modificando */
                $objeto = NomTerminosPago::GetById($post['idterminodepago']);
                $flagmodify = TRUE;
            } else {
                $objeto = new NomTerminosPago();
            }

            /* Si tiene Mora */
            if (!empty($post['arraymora'])) {
                $dataMora = json_decode(stripslashes($post['arraymora']));
            }

            if (!empty($post['arraydpp'])) {
                $dataDpp = json_decode(stripslashes($post['arraydpp']));
            }

            if (!empty($post['arraydescv'])) {
                $dataDescV = json_decode(stripslashes($post['arraydescv']));
            }

            if (!empty($post['arraycuotas'])) {
                $dataCuotas = json_decode(stripslashes($post['arraycuotas']));
            }

            if (!$flagmodify) {
                $alreadyExist = $this->alreadyExist($post, $estructura);
                if ($alreadyExist) {
                    /* Ya existe el termino de pago */
                    return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgAddAlreadyExist}";
                }
            } else {
                /* Eliminar las moras que fueron eliminadas */
                if (!empty($dataMora->deleted)) {
                    $this->delMoras($dataMora->deleted);
                }
                /* Eliminar los dpp que fueron eliminados */
                if (!empty($dataDpp->deleted)) {
                    $this->delDpp($dataDpp->deleted);
                }
                /* Eliminar los descuentos en venta que fueron eliminados */
                if (!empty($dataDescV->deleted)) {
                    $this->delDescV($dataDescV->deleted);
                }
                /* Eliminar las cuotas que fueron eliminados */
                if (!empty($dataCuotas->deleted)) {
                    $this->delCuotas($dataCuotas->deleted);
                }
            }

            $objeto->nombre = $post['nombre'];
            $objeto->aplazos = ($post['tipopago'] == 1) ? 1 : 0;
            $objeto->diafijo = ($post['tipopago'] == 2) ? 1 : 0;
            $objeto->plazo = ($post['tipopago'] == 3 && !empty($post['plazo'])) ? $post['plazo'] : NULL;
            $objeto->fechavencimiento = ($post['tipopago'] == 2 && !empty($post['diafijo'])) ? $post['diafijo'] : NULL;
            $objeto->descventa = (!empty($post['descventa'])) ? $post['descventa'] : NULL;
            $objeto->dpp = (!empty($post['dpp'])) ? $post['dpp'] : NULL;
            $objeto->idumplazo = (!empty($post['idumplazo'])) ? $post['idumplazo'] : NULL;
            $objeto->predeterminado = 0;
            $objeto->idestructura = $estructura;

            $idterminodepago = $objeto->persist();

            if (!is_numeric($idterminodepago)) {
                /* Ocurrio un error al insertar el termino de pago */
                return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgAddFAIL}";
            } else {
                /* agregando las moras */
                if (!empty($dataMora->modified)) {
                    $resBA = $this->addMora($dataMora->modified, $idterminodepago);
                    $response['errors'] = array_merge($response['errors'], $resBA['errors']);
                }
                /* agregando los dpp */
                if (!empty($dataDpp->modified)) {
                    $resBA = $this->addDpp($dataDpp->modified, $idterminodepago);
                    $response['errors'] = array_merge($response['errors'], $resBA['errors']);
                }
                /* agregando las cuotas */
                if (!empty($dataCuotas->modified)) {
                    $resBA = $this->addCuotas($dataCuotas->modified, $idterminodepago);
                    $response['errors'] = array_merge($response['errors'], $resBA['errors']);
                }
                /* agregando los descuentos en venta */
                if (!empty($dataDescV->modified)) {
                    $resBA = $this->addDescV($dataDescV->modified, $idterminodepago);
                    $response['errors'] = array_merge($response['errors'], $resBA['errors']);
                }

                $mensaje = $flagmodify ? 'perfil.etiquetas.msgModOK' : 'perfil.etiquetas.msgAddOK';


                return "{'success':true, 'codMsg':1, 'mensaje': $mensaje}";
            }
        } else {
            /* Faltan datos para adicionar el campo contable */
//            $response['success'] = false;
//            $response['errors'][] = array('code' => 'CP03');
            return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgAddNoData}";
        }

        return $response;
    }

    /**
     * Obtiene las unidades de medida para los plazos.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getUMedidaPlazo($post)
    {
        $data = NomUMPlazo::GetAll();

        return array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );
    }

    /**
     * Funcion auxiliar que registra uno o mas moras para un termino de pago.
     *
     * @param array $moras Arreglo con los datos de las moras
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function addMora($moras, $idterminodepago)
    {
        $response = array('errors' => array(), 'success' => array());
        if (empty($moras)) {
            $response['errors'][] = array('code' => 'MOR0');

            return $response;
        }

        foreach ($moras as $mora) {
            $flagmodify = FALSE;
            /* para cada mora */
            if (!empty($mora->idmora) && is_numeric($mora->idmora)) {
                /* Si se esta modificando */
                $objeto = DatMora::GetById($mora->idmora);
                $flagmodify = TRUE;
            } else {
                $objeto = new DatMora();
            }

//            if (!$flagmodify) {
//                $conditions = 'porciento = ' . $mora->porciento . ' AND dias =' . $mora->dias . ' AND idterminodepago =' . $idterminodepago;
//                $alreadyExist = DatMora::GetByCondition($conditions, array(), TRUE);
//                if ($alreadyExist) {
//                    /* Ya existe el termino de pago */
//                    $response['errors'][] = array('code' => 'BA1', 'nombre' => $mora->numerocuenta);
//
//                    return $response;
//                }
//            }

            $objeto->porciento = $mora->porciento;
            $objeto->dias = $mora->dias;
            $objeto->aclaraciones = $mora->aclaraciones;
            $objeto->idterminodepago = $idterminodepago;

            $idmora = $objeto->persist();

            if (!is_numeric($idmora)) {
                $response['errors'][] = array('code' => 'MOR2', 'id' => $idmora);
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar que registra uno o mas dpp para un termino de pago.
     *
     * @param array $dpps Arreglo con los datos de los dpp
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function addDpp($dpps, $idterminodepago)
    {
        $response = array('errors' => array(), 'success' => array());
        if (empty($dpps)) {
            $response['errors'][] = array('code' => 'DPP0');

            return $response;
        }

        foreach ($dpps as $dpp) {
            $flagmodify = FALSE;
            /* para cada dpp */
            if (!empty($dpp->iddpp) && is_numeric($dpp->iddpp)) {
                /* Si se esta modificando */
                $objeto = DatDpp::GetById($dpp->iddpp);
                $flagmodify = TRUE;
            } else {
                $objeto = new DatDpp();
            }

            $objeto->porciento = $dpp->porciento;
            $objeto->dias = is_numeric($dpp->dias) ? $dpp->dias : NULL;
            $objeto->aclaraciones = $dpp->aclaraciones;
            $objeto->idterminodepago = $idterminodepago;

            $iddpp = $objeto->persist();

            if (!is_numeric($iddpp)) {
                $response['errors'][] = array('code' => 'DPP2', 'id' => $iddpp);
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar que registra uno o mas cuotas para un termino de pago.
     *
     * @param array $cuotas Arreglo con los datos de las cuotas
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function addCuotas($cuotas, $idterminodepago)
    {
        $response = array('errors' => array(), 'success' => array());
        if (empty($cuotas)) {
            $response['errors'][] = array('code' => 'CUO0');

            return $response;
        }

        foreach ($cuotas as $cuota) {
            $flagmodify = FALSE;
            /* para cada cuota */
            if (!empty($cuota->idcuota) && is_numeric($cuota->idcuota)) {
                /* Si se esta modificando */
                $objeto = DatCuotas::GetById($cuota->idcuota);
                $flagmodify = TRUE;
            } else {
                $objeto = new DatCuotas();
            }

            $objeto->idterminoaplicable = $cuota->idterminoaplicable;
            $objeto->porciento = $cuota->porciento;
            $objeto->aclaraciones = $cuota->aclaraciones;
//            $objeto->fechalimite = $cuota->fechalimite;
            $objeto->idterminodepago = $idterminodepago;

            $idcuota = $objeto->persist();

            if (!is_numeric($idcuota)) {
                $response['errors'][] = array('code' => 'CUO2', 'id' => idcuota);
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar que registra uno o mas cuotas para un termino de pago.
     *
     * @param array $descuentos Arreglo con los datos de las cuotas
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function addDescV($descuentos, $idterminodepago)
    {
        $response = array('errors' => array(), 'success' => array());
        if (empty($descuentos)) {
            $response['errors'][] = array('code' => 'DES0');

            return $response;
        }

        foreach ($descuentos as $descuento) {
            $flagmodify = FALSE;
            /* para cada cuota */
            if (!empty($descuento->iddescuentosventa) && is_numeric($descuento->iddescuentosventa)) {
                /* Si se esta modificando */
                $objeto = DatDescuentosVenta::GetById($descuento->iddescuentosventa);
                $flagmodify = TRUE;
            } else {
                $objeto = new DatDescuentosVenta();
            }

            $objeto->porciento = $descuento->porciento;
            $objeto->min = $descuento->min;
            $objeto->aclaraciones = $descuento->aclaraciones;
            $objeto->idterminodepago = $idterminodepago;

            $iddescuentosventa = $objeto->persist();

            if (!is_numeric($iddescuentosventa)) {
                $response['errors'][] = array('code' => 'CUO2', 'id' => idcuota);
            }
        }

        return $response;
    }

    /**
     * Funcion auxiliar que elimina uno o mas moras de un termino de pago.
     *
     * @param array $idmoras Arreglo con los ids de las moras
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function delMoras($idmoras)
    {
        $ids = (is_array($idmoras)) ? $idmoras : array($idmoras);
        $condition = 'idmora IN(' . implode(',', $ids) . ')';
        $response = DatMora::EraseByConditionSQL($condition);

        return $response;
    }

    /**
     * Funcion auxiliar que elimina uno o mas dpp de un termino de pago.
     *
     * @param array $ids Arreglo con los ids de los dpp
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function delDpp($ids)
    {
        $ids = (is_array($ids)) ? $ids : array($ids);
        $condition = 'iddpp IN(' . implode(',', $ids) . ')';
        $response = DatDpp::EraseByConditionSQL($condition);

        return $response;
    }

    /**
     * Funcion auxiliar que elimina uno o mas descuentos en venta de un termino de pago.
     *
     * @param array $ids Arreglo con los ids de los descuentos en venta
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function delDescV($ids)
    {
        $ids = (is_array($ids)) ? $ids : array($ids);
        $condition = 'iddescuentosventa IN(' . implode(',', $ids) . ')';
        $response = DatDescuentosVenta::EraseByConditionSQL($condition);

        return $response;
    }

    /**
     * Funcion auxiliar que elimina uno o mas dpp de un termino de pago.
     *
     * @param array $ids Arreglo con los ids de los dpp
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function delCuotas($ids)
    {
        $ids = (is_array($ids)) ? $ids : array($ids);
        $condition = 'idcuota IN(' . implode(',', $ids) . ')';
        $response = DatCuotas::EraseByConditionSQL($condition);

        return $response;
    }

    /**
     * Funcion auxiliar que verifica si ua existe el termino de pago
     * que se esta intentando registrar.
     *
     * @param array $post Arreglo con los datos de la petición.
     * @param int $estructura identificador de la entidad loagueada.
     * @return boolean TRUE o FALSO si existe o no.
     */
    private function alreadyExist($post, $estructura)
    {
        $condition = 'idestructura = ? AND nombre = ?';
        $params = array($estructura, $post['nombre']);

        $check = NomTerminosPago::GetByCondition($condition, $params, TRUE);

        return count($check) > 0;
    }

    /**
     * Funcion que establece termino de pago como predeterminado
     *
     * @param array $post Arreglo con los datos de la petición.
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function setDefault($post)
    {
        $estructura = $this->global->Estructura->idestructura;
        $precondition = 'idestructura = ' . $estructura . ' AND predeterminado = 1';
        $preconditionfields = array('predeterminado = 0');
        $condition = 'idterminodepago = ' . $post['id'];
        $conditionfields = array('predeterminado = 1');

        NomTerminosPago::UpdateSQL($preconditionfields, $precondition);
        NomTerminosPago::UpdateSQL($conditionfields, $condition);

        return "{'success':true, 'codMsg':1, 'mensaje': perfil.etiquetas.msgDefaultOK}";

    }

}
