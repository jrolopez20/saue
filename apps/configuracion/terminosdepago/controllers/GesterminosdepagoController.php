<?php

/**
 * Clase controladora para gestionar los terminos de pago
 *
 * @author Team Delta
 * @package Datos Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class GesterminosdepagoController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new TerminosdepagoModel();
        parent::init();
    }

    /**
     * Carga la interfaz principal
     */
    public function gesterminosdepagoAction()
    {
        $this->render();
    }

    /**
     * Carga los terminos de pago.
     */
    public function loadAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->load($post);
        echo json_encode($data);
    }

    /**
     * Carga los terminos de pago de la entidad que no han vencido o no tienen fecha de vencimiento definida.
     */
    public function loadSelectablesAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->load($post, TRUE);
        echo json_encode($data);
    }

    /**
     * Registra y modifica un termino de pago.
     */
    public function saveAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->save($post);
        echo ($data);
    }

    /**
     * Eliminar uno o varios terminos de pago.
     */
    public function deleteAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->delete($post);
        echo (is_array($data)) ? json_encode($data) : $data;
    }


    /**
     * Obtiene los paises.
     *
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function getUMedidaPlazoAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->getUMedidaPlazo($post);
        echo json_encode($data);
    }

    /**
     * Obtiene los paises.
     *
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function setDefaultAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->setDefault($post);
        echo (is_array($data)) ? json_encode($data) : $data;
    }
}