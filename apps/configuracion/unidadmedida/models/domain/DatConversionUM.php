<?php

class DatConversionUM extends BaseDatConversionUM
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasOne('NomUnidadMedida', array('local' => 'idunidadmedida', 'foreign' => 'idunidadmedida'));
    }

    public function persist()
    {
        try {
            $a = Doctrine_Manager::getInstance();
            $va = $a->getCurrentConnection();

            $this->save();

            $va->commit();
            $va->beginTransaction();

            return $this->idconversion;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('DatConversionUM')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatConversionUM')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idconversion, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('DatConversionUM')->find($idconversion);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatConversionUM')
                ->whereIn('idconversion', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('DatConversionUM')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }


    static public function Erase($idconversion)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatConversionUM')
                ->where("idconversion = $idconversion")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($idconversion)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.dat_conversionum WHERE idconversion = ' . $idconversion);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('DatConversionUM')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.dat_conversionum WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function UpdateSQL($fields, $conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('UPDATE mod_maestro.dat_conversionum SET '.implode(',',$fields).' WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

}