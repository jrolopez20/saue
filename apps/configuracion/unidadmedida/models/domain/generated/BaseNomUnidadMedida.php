<?php

abstract class BaseNomUnidadMedida extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_unidadmedida');
        $this->hasColumn('idunidadmedida', 'numeric', null,array('notnull' => true, 'primary' => true, 'sequence' => 'mod_maestro.sec_nomunidadmedida'));
        $this->hasColumn('denominacion', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('simbolo', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idmagnitud', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('basica', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idestructura', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('autogenerada', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}

