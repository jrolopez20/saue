<?php

abstract class BaseNomPrefijosum extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.nom_prefijosum');
        $this->hasColumn('idprefijo', 'numeric', null,array('notnull' => true, 'primary' => true));
        $this->hasColumn('prefijo', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('simbolo', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('escalacorta', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('escalalarga', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('equivalencia', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('activo', 'numeric', null, array('notnull' => false, 'primary' => false));
    }

}

