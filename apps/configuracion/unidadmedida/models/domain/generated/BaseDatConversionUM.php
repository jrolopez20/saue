<?php

abstract class BaseDatConversionUM extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_maestro.dat_conversionum');
        $this->hasColumn('idconversion', 'numeric', null, array('notnull' => true, 'primary' => true, 'sequence' => 'mod_maestro.sec_conversionum'));
        $this->hasColumn('idunidadmedida', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('factorconversion', 'numeric', null, array('notnull' => true, 'primary' => false));

    }

}

