<?php

class NomMagnitud extends BaseNomMagnitud
{

    public function setUp()
    {
        parent:: setUp();
        $this->hasMany('NomUnidadMedida', array('local' => 'idmagnitud', 'foreign' => 'idmagnitud'));
    }

    public function persist()
    {
        try {
//            $a = Doctrine_Manager::getInstance();
//            $va = $a->getCurrentConnection();

            $this->save();

//            $va->commit();
//            $va->beginTransaction();

            return $this->idmagnitud;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function GetAll()
    {
        try {
            /* Aqui falta la parte de los joins para obtener la informacion adicional de la otras tablas */
            $query = Doctrine_Query::create();
            $result = $query->from('NomMagnitud')->execute();

            return $result->toArray();
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByPage($offset, $limit, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomMagnitud')
                ->offset($offset)
                ->limit($limit)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetById($idmagnitud, $toArray = FALSE)
    {
        try {
            $result = Doctrine::getTable('NomMagnitud')->find($idmagnitud);

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByIds($arrIds, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomMagnitud')
                ->whereIn('idmagnitud', $arrIds)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function GetByCondition($condition, $params, $toArray = FALSE)
    {
        try {
            $query = Doctrine_Query::create();
            $result = $query->from('NomMagnitud')
                ->where("$condition", $params)
                ->execute();

            return $toArray ? $result->toArray() : $result;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }


    static public function Erase($idmagnitud)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomMagnitud')
                ->where("idmagnitud = $idmagnitud")
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

    static public function EraseSQL($idmagnitud)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.nom_magnitud WHERE idmagnitud = ' . $idmagnitud);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function EraseByCondition($conditions, $params)
    {
        try {
            $query = Doctrine_Query::create();
            $rows_afected = $query->delete('NomMagnitud')
                ->where("$conditions", $params)
                ->execute();

            return $rows_afected > 0;
        } catch (Doctrine_Exception $e) {
            return $e;
        }
    }

    static public function EraseByConditionSQL($conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('DELETE FROM mod_maestro.nom_magnitud WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return 0;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function UpdateSQL($fields, $conditions)
    {
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('UPDATE mod_maestro.nom_magnitud SET ' . implode(',', $fields) . ' WHERE ' . $conditions);

            return TRUE;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == 23503) {//Code for foreignKeyViolation
                return -1;
            } else {
                return $e->getMessage();
            }
        }
    }

    static public function ChangeActive($idmagnitud)
    {
        $magnitudes = is_array($idmagnitud) ? implode(',', $idmagnitud) : $idmagnitud;
        try {
            $objDoctrine = Doctrine_Manager::getInstance();
            $connection = $objDoctrine->getCurrentConnection();
            $connection->execute('UPDATE mod_maestro.nom_magnitud SET activo = ' .
                'CASE WHEN activo = 1 THEN 0 ELSE 1 END WHERE idmagnitud IN(' . $magnitudes . ')');

            return TRUE;
        } catch (Doctrine_Exception $e) {
            throw $e;
        }
    }

}