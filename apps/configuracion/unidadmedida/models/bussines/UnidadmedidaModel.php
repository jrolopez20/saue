<?php

/**
 * Clase modelo que implementa el negocio concerniente a la gestión de
 * terminos de pago.
 *
 * @author Team Delta
 * @package Datos maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class UnidadmedidaModel extends ZendExt_Model
{

    private $domain;

    public function __construct()
    {
        $this->domain = new NomUnidadMedida();
        $this->domainmagnitud = new NomMagnitud();
        $this->domainprefijos = new NomPrefijosum();
        parent::ZendExt_Model();
    }

    /**
     * Obtiene las unidades de medida de una magnitud.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * @throws Doctrine_Exception
     * @return mixed Arreglo con las unidades de medida para una magnitud fisica
     */
    public function load($post)
    {
        $estructura = $this->global->Estructura->idestructura;
        $utf8 = array("à" => "À", "è" => "È", "ì" => "Ì", "ò" => "Ò", "ù" => "Ù", "á" => "Á", "é" => "É", "í" => "Í", "ó" => "Ó", "ú" => "Ú", "â" => "Â", "ê" => "Ê", "î" => "Î", "ô" => "Ô", "û" => "Û", "ç" => "Ç");
        $data = array();

        if (!empty($post['idmagnitud'])) {
            $conditions = 'idmagnitud = ? AND (autogenerada = ? OR idestructura = ?)';
            $params = array($post['idmagnitud'], 1, $estructura);
            $data = $this->domain->GetByCondition($conditions, $params, TRUE);

            /* Obtener los factores de conversion */
            foreach($data as $key => $um){
                $factorconversion = DatConversionUM::GetByCondition('idunidadmedida = ?', array($um['idunidadmedida']), TRUE);
                if(!empty($factorconversion)){
                    $data[$key] = array_merge($um, $factorconversion[0]);
                }
            }
        }

        $paging = array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );

        return $paging;
    }

    /**
     * Obtiene las magnitudes fisicas.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function loadMagnitudes()
    {
        $estructura = $this->global->Estructura->idestructura;

        $conditions = 'standard = ? OR idestructura = ?';
        $params = array(1, $estructura);

        $data = $this->domainmagnitud->GetByCondition($conditions, $params, TRUE);

        $paging = array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );

        return $paging;
    }

    /**
     * Eliminar una o varias unidades de medida.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * (almacena los ids de las unidades de medida en el índice 'ids')
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function delete($post)
    {
        $response = array('errors' => array(), 'success' => array());
        if (!empty($post['ids'])) {
            $ids = json_decode(stripslashes($post['ids']));
            $count = count($ids);

            $implode = implode(',', $ids);
        } else {
            /* No data to delete */
            $response['errors'][] = array('code' => 'DELCP02');

            return "{'success':false, 'codMsg':3, 'mensaje': perfil.etiquetas.msgDelNoData}";
        }

        return $response;
    }

    /**
     * Inserta y modofica una magnitud.
     *
     * @param array $post Arreglo con los datos de la magnitud
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function saveMagnitud($post)
    {
        $estructura = $this->global->Estructura->idestructura;

        /* VERIFICACION PARA VER SI EXISTE EL ELEMENTO */
        $condExist = '(lower(denominacion) = ? AND (standard = ? OR idestructura = ?))';
        $params = array(strtolower($post['denominacion']), 1, $estructura);

        // si se esta modificando se busca cualquier otro que no sea el mismo
        if (!empty($post['idmagnitud'])) {
            $condExist .= ' AND idmagnitud <> ?';
            $params[] = $post['idmagnitud'];
        }

        $exist = NomMagnitud::GetByCondition($condExist, $params, TRUE);

        if (count($exist)) {
            return "{'success':false, 'codMsg': 3, 'mensaje': perfil.etiquetas.msgAlreadyExistM}";
        }
        /* FIN VERIFICACION */

        /* Si se esta modificando */
        $objeto = (!empty($post['idmagnitud']) && is_numeric($post['idmagnitud'])) ?
            NomMagnitud::GetById($post['idmagnitud']) : new NomMagnitud();

        $objeto->denominacion = $post['denominacion'];
        $objeto->idestructura = $estructura;
        $objeto->descripcion = !empty($post['descripcion']) ? $post['descripcion'] : NULL;
        $objeto->activo = !empty($post['activo']) ? $post['activo'] : $objeto->activo;

        $idmagnitud = $objeto->persist();

        $flag = is_numeric($idmagnitud) ? 'true' : 'false';
        $code = is_numeric($idmagnitud) ? '1' : '3';
        $msg = is_numeric($idmagnitud) ?
            (!empty($post['idmagnitud']) ? 'perfil.etiquetas.msgModOKM' : 'perfil.etiquetas.msgAddOKM') :
            (!empty($post['idmagnitud']) ? 'perfil.etiquetas.msgModFailM' : 'perfil.etiquetas.msgAddFailM');
        $plus = empty($post['idmagnitud']) ? ', idgenerado:' . $idmagnitud : '';

        return "{'success':$flag, 'codMsg': $code, 'mensaje': $msg $plus}";
    }

    /**
     * Inserta y modofica una unidad de medida.
     *
     * @param array $post Arreglo con los datos de la unidad de medida
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function save($post, &$idautogenerada, $flagautogenerate = false)
    {
        $estructura = $this->global->Estructura->idestructura;
//        print_r($post);die;
        $flagmodify = !empty($post['idunidadmedida']) ? TRUE : FALSE;
        // FALTA LA VERIFICACION SI EXISTE EL TIPO

        /* Si se esta modificando */
        $objeto = (!empty($post['idunidadmedida']) && is_numeric($post['idunidadmedida'])) ?
            NomUnidadMedida::GetById($post['idunidadmedida']) : new NomUnidadMedida();

        $objeto->denominacion = $post['denominacion'];
        $objeto->simbolo = $post['simbolo'];
        $objeto->descripcion = !empty($post['descripcion']) ? $post['descripcion'] : NULL;
        $objeto->idestructura = $estructura;
        $objeto->idmagnitud = $post['idmagnitud'];
        $objeto->basica = (!empty($post['basica']) && $post['basica'] == 'on') ? 1 : 0;
        $objeto->autogenerada = $post['autogenerada'] ? 1 : 0;

        $idunidadmedida = $objeto->persist();

        if(!$flagautogenerate){
            /* CREAR EL FACTOR DE CONVERSION PARA ESTA UNIDAD */
            $conversion = !empty($post['idconversion']) && is_numeric($post['idconversion']) ?
                DatConversionUM::GetById($post['idconversion']) : new DatConversionUM();

            $conversion->idunidadmedida = $idunidadmedida;
            /* Si viene el umfactor  es porque es en base a otra unidad de medida */
            $conversion->factorconversion = empty($post['umfactor']) ? $post['factorconversion'] :
                $post['umfactor'] * $post['factorconversion'];

            $idconversion = $conversion->persist();
        }

        /* ESTO SOLO SE EJECUTA PARA AUTOGENERAR UM */
        if (!empty($post['idprefijos']) && is_numeric($idunidadmedida)) {
            $response = array('conversiones' => array(), 'autogenerados' => array());
            $this->generateUM($post, $idunidadmedida, $response);
        }

        $flag = is_numeric($idunidadmedida) ? 'true' : 'false';
        $code = is_numeric($idunidadmedida) ? '1' : '3';
        $msg = is_numeric($idunidadmedida) ?
            (!empty($post['idunidadmedida']) ? 'perfil.etiquetas.msgModOKUM' : 'perfil.etiquetas.msgAddOKUM') :
            (!empty($post['idunidadmedida']) ? 'perfil.etiquetas.msgModFailUM' : 'perfil.etiquetas.msgAddFailUM');
        $plus = empty($post['idunidadmedida']) ? ', idgenerado:' . $idunidadmedida : '';

        if(is_numeric($idunidadmedida)){
            $idautogenerada = $idunidadmedida;
        }

        if (!empty($post['conversiones'])) {
            $post['conversiones'] = json_decode(stripcslashes($post['conversiones']));
            // hacer una funcionalidad aqui que inserte las conversiones
        }

        return "{'success':$flag, 'codMsg': $code, 'mensaje': $msg $plus}";
    }

    /**
     * Funcionalidad que autogenera unidades de medida de acuerdo ala seleccionde prefijos
     *
     * @param array $post Arreglo con los datos de peticion inicial
     * @param int $idunidadmedida Id de la unidad de medida basica a partir de la cual se autogereran
     * los multiplos y submultiplos
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    private function generateUM($post, $idunidadmedida, &$response)
    {

        $post['idprefijos'] = json_decode($post['idprefijos']);
        foreach ($post['idprefijos'] as $idprefijo) {
            $newPost = array();
            $prefijo = NomPrefijosum::GetById($idprefijo, TRUE);
            if (!empty($prefijo)) {
                /* CREANDO LA NUEVA UNIDAD */
                $newPost['denominacion'] = ucfirst($prefijo['prefijo']) . strtolower($post['denominacion']);
                $newPost['descripcion'] = 'Unidad de medida generada automaticamente';
                $newPost['simbolo'] = $prefijo['simbolo'] . $post['simbolo'];
                $newPost['idmagnitud'] = $post['idmagnitud'];
                $newPost['autogenerada'] = 1;

                $this->save($newPost, $idautoum , TRUE); // se le pasa true par aindicar que es una un autogenerada

                /* AKI INSERTAR EL FACTOR DE CONVERSION */
                if (is_numeric($idautoum)) {
                    $conversion = new DatConversionUM();
                    $conversion->idunidadmedida = $idautoum;
                    $conversion->factorconversion = $prefijo['equivalencia'];

                    $idconversion = $conversion->persist();

                    if(!is_numeric($idconversion)){
                        $response['errors']['conversiones'][] = $idautoum;
                    }
                }else{
                    $response['errors']['autogenerados'][] = $idautoum;
                }
            }
        }

    }

    /**
     * Activa | Desactiva uno o mas magnitudes.
     *
     * @param array $post Arreglo con los ids
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function changestateUM($post)
    {
        $arrayids = json_decode($post['ids']);
        $response = array('code' => 1, 'errors' => array());

        $result = NomMagnitud::ChangeActive($arrayids);

        return json_encode($response);
    }

    /**
     * Eliminar una o varias magnitudes.
     *
     * @param array $post Arreglo con los datos enviados en la peticion
     * (almacena los ids de las magnitudes en el índice 'ids')
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function deleteMagnitud($post)
    {
        $arrayids = json_decode($post['ids']);
        $estructura = $this->global->Estructura->idestructura;
        $response = array('code' => 1, 'errors' => array());

        foreach ($arrayids as $id) {

            if (is_numeric($id)) {
                $magnitud = NomMagnitud::GetById($id);

                if ($magnitud) { // verificando que existe el producto en la BD
                    $result = NomMagnitud::EraseSQL($id);

                    if ($result === -1) {
                        $response['code'] = -3;
                        $response['errors'][] = $id;
                    }
                }
            }
        }

        /* ELIMINAR TODAS LAS UNIDADES DE MEDIDA DE ESTA MAGNITUD */
        NomUnidadMedida::EraseByConditionSQL('idmagnitud IN(' . implode(',', $arrayids) . ') AND idestructura = ' . $estructura);

        return json_encode($response);
    }

    /**
     * Carga todos los prefijo del Sistema Internacional de UM.
     *
     * @throws Doctrine_Exception
     * @return mixed Arreglo con la respuesta del servidor
     */
    public function loadPrefijos()
    {
        $data = NomPrefijosum::GetAll();

        $paging = array(
            'success' => TRUE,
            'total' => count($data),
            'data' => $data
        );

        return $paging;
    }
}
