<?php

/**
 * Clase controladora para gestionar los terminos de pago
 *
 * @author Team Delta
 * @package Datos Maestros
 * @copyright Ecotec
 * @version 1.0-0
 */
class GestunidadmedidaController extends ZendExt_Controller_Secure
{
    private $model;

    public function init()
    {
        $this->model = new UnidadmedidaModel();
        parent::init();
    }

    /**
     * Carga la interfaz principal
     */
    public function gestunidadmedidaAction()
    {
        $this->render();
    }

    /**
     * Carga las unidades de medida.
     */
    public function loadAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->load($post);
        echo json_encode($data);
    }
    /**
     * Carga las unidades de medida.
     */
    public function loadMagnitudesAction()
    {
        $data = $this->model->loadMagnitudes();
        echo json_encode($data);
    }

    /**
     * Registra y modifica una magnitud.
     */
    public function saveMagnitudAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->saveMagnitud($post);
        echo ($data);
    }
    /**
     * Registra y modifica una unidad de medida.
     */
    public function saveAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->save($post);
        echo ($data);
    }

    /**
     * Eliminar uno o varias unidades de medida.
     */
    public function deleteAction()
    {
        $post = $this->_request->getPost();
        $data = $this->model->delete($post);
        echo (is_array($data)) ? json_encode($data) : $data;
    }

    /**
     * call this model action
     */
    public function changestateUMAction()
    {
        echo $this->model->changestateUM($this->_request->getPost());
    }

    /**
     * Eliminar uno o varias magnitudes.
     */
    public function deleteMagnitudAction()
    {
        echo $this->model->deleteMagnitud($this->_request->getPost());
    }

    /**
     * Carga los prefijo del SI.
     */
    public function loadPrefijosAction()
    {
        echo json_encode($this->model->loadPrefijos());
    }
}