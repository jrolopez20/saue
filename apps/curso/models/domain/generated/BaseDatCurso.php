<?php

abstract class BaseDatCurso extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_curso');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('par_curs', 'integer', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('cupo', 'integer', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idprofesor', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idhorario', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idaula', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idmateria', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idperiododocente', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idfacultad', 'numeric', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('idcurso', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_curso'));
    }


}

