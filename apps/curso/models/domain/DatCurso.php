<?php

class DatCurso extends BaseDatCurso
{

    public function setUp()
    {
        parent :: setUp();
    }

    static public function cargarCursos($limit, $start, $filtros)
    {

        $query = Doctrine_Query::create();

        $sql = "SELECT
                    c.*,

                    a.descripcion as aula_descripcion,
                    m.descripcion as materia_descripcion,
                    m.codmateria,
                    p.nombre as profesor_nombre,
                    p.apellidos as profesor_apellidos,
                    h.descripcion as horario_descripcion--,
                    --(SELECT
                    --      COUNT (dat_alumno.idalumno)
                    --    FROM
                    --      mod_saue.dat_alumno
                    --    WHERE
                    --      dat_alumno.idcurso = c.idcurso) as n_alumnos
                FROM
                    mod_saue.dat_curso c
                    INNER JOIN mod_saue.dat_aula a ON c.idaula=a.idaula
                    INNER JOIN mod_saue.dat_materia m ON c.idmateria=m.idmateria
                    INNER JOIN mod_saue.dat_profesor p ON c.idprofesor=p.idprofesor
                    INNER JOIN mod_saue.dat_horario h ON c.idhorario=h.idhorario";

        $params = array();
        if ($filtros) {
            $hasWhere = false;
            foreach ($filtros as $filtro) {
                if (!$hasWhere)
                    $sql .= "\nWHERE ";
                else
                    $sql .= "\nAND ";
                $sql .= "c.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        };

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }
    
    static public function cargarCursosService($limit, $start, $filtros)
    {

        $query = Doctrine_Query::create();

        $sql = "SELECT
                    c.*,
                    a.descripcion as aula_descripcion,
                    m.descripcion ,
                    m.codmateria,
                    p.nombre as profesor_nombre,
                    p.apellidos as profesor_apellidos,
                    h.descripcion as horario_descripcion--,
                    --(SELECT
                    --      COUNT (dat_alumno.idalumno)
                    --    FROM
                    --      mod_saue.dat_alumno
                    --    WHERE
                    --      dat_alumno.idcurso = c.idcurso) as n_alumnos
                FROM
                    mod_saue.dat_curso c
                    INNER JOIN mod_saue.dat_aula a ON c.idaula=a.idaula
                    INNER JOIN mod_saue.dat_materia m ON c.idmateria=m.idmateria
                    INNER JOIN mod_saue.dat_profesor p ON c.idprofesor=p.idprofesor
                    INNER JOIN mod_saue.dat_horario h ON c.idhorario=h.idhorario
                    WHERE c.estado = true";

        $params = array();
        
        if ($filtros) {
                $sql .= " AND ". self::createStringFilter($filtros);
        }

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public  function createStringFilter($filtros){
            $str = "";
            foreach ($filtros as $key) {
                if($key->property=='idperiododocente')
                    $str .=" (c.".$key->property." = " . $key->value . ") AND ";
                    else
                    $str .=" (lower(m.".$key->property.") LIKE '%" . strtolower($key->value) . "%') OR  ";
            }
            return substr( $str ,0, -4);
        }

    static public function countCursosService($filtros)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT (c.idcurso)
                FROM
                    mod_saue.dat_curso c
                    INNER JOIN mod_saue.dat_aula a ON c.idaula=a.idaula
                    INNER JOIN mod_saue.dat_materia m ON c.idmateria=m.idmateria
                    INNER JOIN mod_saue.dat_profesor p ON c.idprofesor=p.idprofesor
                    INNER JOIN mod_saue.dat_horario h ON c.idhorario=h.idhorario
                    WHERE c.estado = true";

        $params = array();
        
        if ($filtros) {
                $sql .= " AND (". self::createStringFilter($filtros).") ";
        }

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetch();

        return $result['count'];
    }

    static public function countCursos($filtros)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT (c.idcurso)
                FROM
                    mod_saue.dat_curso c
                    INNER JOIN mod_saue.dat_aula a ON c.idaula=a.idaula
                    INNER JOIN mod_saue.dat_materia m ON c.idmateria=m.idmateria
                    INNER JOIN mod_saue.dat_profesor p ON c.idprofesor=p.idprofesor
                    INNER JOIN mod_saue.dat_horario h ON c.idhorario=h.idhorario";

        $params = array();
        if ($filtros) {
            $hasWhere = false;
            foreach ($filtros as $filtro) {
                if (!$hasWhere)
                    $sql .= "\nWHERE ";
                else
                    $sql .= "\nAND ";
                $sql .= "c.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        };

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetch();

        return $result['count'];
    }
}

