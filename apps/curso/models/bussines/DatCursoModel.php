<?php

class DatCursoModel extends ZendExt_Model
{
    public function DatCarreraModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarCursos($limit = 20, $start = 0, $filtros = null)
    {
        return DatCurso::cargarCursos($limit, $start, $filtros);
    }

    public function countCursos($filtros = null)
    {
        try {
            return DatCurso::countCursos($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarFacultades($limit, $start)
    {
        try {
            return $this->integrator->metadatos->ListadoEstructurasT($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAnno($limit, $start)
    {
        try {
            return $this->integrator->calendar->getAnnos();

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarPeriodos($anno, $limit, $start)
    {
        try {
            return $this->integrator->calendar->cargarPeriodoAnno($anno);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarHorarios($limit, $start)
    {
        try {
            return $this->integrator->calendar->cargarHorarios($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAulas($limit, $start)
    {
        try {
            return $this->integrator->calendar->cargarAulas($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMaterias($limit, $start)
    {
        try {
            return $this->integrator->materia->cargarMaterias($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarProfesores($limit, $start, $filtros)
    {
        try {
            return $this->integrator->profesor->cargarProfesores($limit, $start, $filtros);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAlumnos($limit, $start)
    {
        try {
            return array(
                array(
                    'idalumno' => 1,
                    'codigo' => 'ALU001',
                    'nombre' => 'JUAN MANUEL',
                    'facultad' => 'FACULTAD 1',
                    'nota' => '80',
                    'faltas' => '3',
                    'detalles' => '******* DETALLES *******'
                ),
                array(
                    'idalumno' => 2,
                    'codigo' => 'ALU002',
                    'nombre' => 'LUIS ALBERTO',
                    'facultad' => 'FACULTAD 1',
                    'nota' => '95',
                    'faltas' => '0',
                    'detalles' => '******* DETALLES *******'
                ),
                array(
                    'idalumno' => 3,
                    'codigo' => 'ALU003',
                    'nombre' => 'JULIO CESAR',
                    'facultad' => 'FACULTAD 2',
                    'nota' => '89',
                    'faltas' => '1',
                    'detalles' => '******* DETALLES *******'
                ),
                array(
                    'idalumno' => 4,
                    'codigo' => 'ALU004',
                    'nombre' => 'ROSENDO JAVIER',
                    'facultad' => 'FACULTAD 1',
                    'nota' => '98',
                    'faltas' => '0',
                    'detalles' => '******* DETALLES *******'
                )
            );
            //$this->integrator->;

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarCurso($idmateria, $idaula, $idhorario, $idprofesor, $idperiododocente, $idfacultad, $cupo, $par_curs, $estado, $usuario,
                                  $fecha)
    {
        try {
            $curso = new DatCurso();
            $curso->idmateria = $idmateria;
            $curso->idaula = $idaula;
            $curso->idhorario = $idhorario;
            $curso->idprofesor = $idprofesor;
            $curso->idperiododocente = $idperiododocente;
            $curso->idfacultad = $idfacultad;
            $curso->cupo = $cupo;
            $curso->par_curs = $par_curs;
            $curso->estado = $estado;
            $curso->idusuario = $usuario;
            $curso->fecha = $fecha;
            $curso->save();
            return $curso->idcurso;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == '23505' && strstr($e->getMessage(), 'uq_profesor_periododocente_horario'))
                return 'profesor_registrado';
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarCurso($idcurso, $idmateria, $idaula, $idhorario, $idprofesor, $idperiododocente,
                                   $idfacultad, $cupo, $par_curs, $estado, $usuario, $fecha)
    {
        try {
            $curso = Doctrine::getTable("DatCurso")->find($idcurso);
            $curso->idmateria = $idmateria;
            $curso->idaula = $idaula;
            $curso->idhorario = $idhorario;
            $curso->idprofesor = $idprofesor;
            $curso->idperiododocente = $idperiododocente;
            $curso->idfacultad = $idfacultad;
            $curso->cupo = $cupo;
            $curso->par_curs = $par_curs;
            $curso->estado = $estado;
            $curso->idusuario = $usuario;
            $curso->fecha = $fecha;
            $curso->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if ($e->getCode() == '23505' && strstr($e->getMessage(), 'uq_profesor_periododocente_horario'))
                return 'profesor_registrado';
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarCurso($idcurso)
    {
        try {
            $curso = Doctrine::getTable("DatCurso")->find($idcurso);
            $curso->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

