<?php

class CursoProxyService
{
    public function loadCursos($limit, $start, $filtros = null)
    {

        return DatCurso::cargarCursosService($limit, $start, $filtros);
    }

    public function countCursos($filtros)
    {
    	
        return DatCurso::countCursosService($filtros);
    }
} 