<?php

class GestcursosController extends ZendExt_Controller_Secure
{

    public function init()
    {
        parent::init();
        $this->model = new DatCursoModel();
    }

    public function gestcursosAction()
    {
        $this->render();
    }

    public function cargarCursosAction()
    {
        //obtener todos los cursos
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $cursos = $this->model->cargarCursos($limit, $start, $filtros);
        //ver esto en todas las cargas, si realmente hace falta.
        $cant = $this->model->countCursos($filtros);

        $result = array('cantidad' => $cant, 'datos' => $cursos);
        echo json_encode($result);
    }

    public function cargarAnnosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $anno = $this->model->cargarAnno($limit, $start);

        echo json_encode(array('datos' => $anno));
    }

    public function cargarPeriodosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $anno = $this->_request->getPost('anno');

        $periodos = $this->model->cargarPeriodos($anno, $limit, $start);

        echo json_encode(array('datos' => $periodos));
    }

    public function cargarHorariosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $horarios = $this->model->cargarHorarios($limit, $start);

        echo json_encode(array('datos' => $horarios));
    }

    public function cargarAulasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $aulas = $this->model->cargarAulas($limit, $start);

        echo json_encode(array('datos' => $aulas));
    }

    public function cargarMateriasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $materias = $this->model->cargarMaterias($limit, $start);

        echo json_encode(array('datos' => $materias));
    }

    public function cargarAlumnosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $alumnos = $this->model->cargarAlumnos($limit, $start);

        echo json_encode(array('datos' => $alumnos));
    }

    public function cargarProfesoresAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $profesores = $this->model->cargarProfesores($limit, $start, $filtros);

        echo json_encode(array('datos' => $profesores));
    }

    public function cargarFacultadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $facultades = $this->model->cargarFacultades($limit, $start);

        echo json_encode(array('datos' => $facultades));
    }

    public function insertarCursosAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idmateria = $datos->idmateria;
        $idaula = $datos->idaula;
        $idhorario = $datos->idhorario;
        $idprofesor = $datos->idprofesor;
        $idperiododocente = $datos->idperiododocente;
        $idfacultad = $datos->idfacultad;
        $cupo = $datos->cupo;
        $par_curs = $datos->par_curs;
        $estado = $datos->estado;

        $result = $this->model->insertarCurso($idmateria, $idaula, $idhorario, $idprofesor, $idperiododocente, $idfacultad, $cupo, $par_curs, $estado, $usuario,
            $fecha);
        if ($result === 'profesor_registrado')
            echo("{'failure': true,'codMsg':3,'mensaje':perfil.etiquetas.lbMsgInfProfesorExistente}");
        else
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarCursosAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idcurso = $datos->idcurso;
        $idmateria = $datos->idmateria;
        $idaula = $datos->idaula;
        $idhorario = $datos->idhorario;
        $idprofesor = $datos->idprofesor;
        $idperiododocente = $datos->idperiododocente;
        $idfacultad = $datos->idfacultad;
        $cupo = $datos->cupo;
        $par_curs = $datos->par_curs;
        $estado = $datos->estado;

        $result = $this->model->modificarCurso($idcurso, $idmateria, $idaula, $idhorario, $idprofesor, $idperiododocente, $idfacultad, $cupo, $par_curs, $estado, $usuario,
            $fecha);
        if ($result === 'profesor_registrado')
            echo("{'failure': true,'codMsg':3,'mensaje':perfil.etiquetas.lbMsgInfProfesorExistente}");
        else
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarCursosAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idcurso = $datos->idcurso;

        if ($this->model->eliminarCurso($idcurso))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'idcurso':$idcurso,'success': true,'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfEliminar}");
    }
}

?>