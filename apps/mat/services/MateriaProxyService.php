<?php


class MateriaProxyService
{
    /**
     * loadCanton
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function countPEMT()
    {
        $pemt = new PensumMateriaEnfasisTipomateriaModel();
        return $pemt->countPEMTService();
    }

    /**
     * loadCanton
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadPEMT($limit, $start)
    {
        $pemt = new PensumMateriaEnfasisTipomateriaModel();
        return $pemt->cargarPEMTService($limit, $start);
    }

    public function countMaterias()
    {
        return DatMateria::countMateriasService();
    }

    public function loadMaterias($limit, $start, $filtros = null)
    {
        $DatMateriaModel = new DatMateriaModel();
        return $DatMateriaModel->cargarMaterias($limit, $start, $filtros);
    }

    public function countAreas()
    {
        return DatArea::countAreasService();
    }

    public function loadAreas($limit, $start, $filtros = null)
    {
        return DatArea::cargarAreas($limit, $start, $filtros);
    }

    public function loadAreasGenerales($limit, $start)
    {
        return DatArea::cargarAreasGenerales($limit, $start);
    }

    public function loadMensiones($limit, $start)
    {

        return DatMension:: cargarMesionService($limit, $start);
    }
    public function countMensiones()
    {
        return DatMension:: contarMesionService();
    }
} 
