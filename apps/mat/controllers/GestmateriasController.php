<?php

/**
 * Componente para gestionar los sistemas.
 *
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestmateriasController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatMateriaModel();
    }

    public function gestmateriasAction()
    {
        $this->render();
    }

    public function cargarMateriasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $materias = $this->model->cargarMaterias($limit, $start, $filtros);
        $cant = $this->model->countMaterias($filtros);

        $result = array('success' => true, 'cantidad' => $cant, 'datos' => $materias);
        echo json_encode($result);
    }

    public function cargarIdiomasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $idiomas = $this->model->cargarIdiomas($limit, $start);

        //$result = array('success' => true, 'cantidad' => $cant, 'datos' => $materias);
        echo json_encode(array('datos'=> $idiomas));
    }

    public function insertarMateriasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $codmateria = $datos->codmateria;
        $descripcion = $datos->descripcion;
        $traduccion = $datos->traduccion;
        $min_nota_materia = $datos->min_nota_materia;
        $obligatoria = $datos->obligatoria;
        $idtipomateria = $datos->idtipomateria;
        $ididioma = $datos->ididioma;
        $nivel = $datos->nivel;
        $estado = $datos->estado;

        if ($this->model->insertarMateria($codmateria, $descripcion, $traduccion, $min_nota_materia,
            $obligatoria, $estado, $idtipomateria, $ididioma, $nivel, $usuario, $fecha)) {
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
            //echo json_encode($result);
        }
    }

    public function cargarCarrerasAction()
    {
        //obtener todas las carreras
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        //$idfacultad = $this->getPerfil()->
        $carreras = $this->model->cargarCarreras($limit, $start);

        $result = array('datos' => $carreras);
        echo json_encode($result);
    }

    public function cargarCreditosAction()
    {
        //obtener todas las areas
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idmateria = $this->_request->getPost('idmateria');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $creditos = $this->model->cargarCreditos($idmateria, $limit, $start, $filtros);

        $result = array('datos' => $creditos);
        echo json_encode($result);
    }

    public function gestionarCreditosAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);

        $idmateriacredito = $datos->idmateriacredito;
        $idmateria = $datos->idmateria;
        $idpensum = $datos->idpensum;
        $creditos = $datos->creditos;

        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        if($this->model->gestionarCreditos($idmateriacredito, $idmateria, $idpensum, $creditos, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfCreditosAsig}");
    }

    public function modificarMateriasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idmateria = $datos->idmateria;
        $codmateria = $datos->codmateria;
        $descripcion = $datos->descripcion;
        $traduccion = $datos->traduccion;
        $min_nota_materia = $datos->min_nota_materia;
        $obligatoria = $datos->obligatoria;
        $idtipomateria = $datos->idtipomateria;
        $ididioma = $datos->ididioma;
        $nivel = $datos->nivel;
        $estado = $datos->estado;

        if($this->model->modificarMateria($idmateria, $codmateria, $descripcion, $traduccion, $min_nota_materia,
            $obligatoria, $estado, $idtipomateria, $ididioma, $nivel, $usuario, $fecha))
        //devolver los datos, hay que agregar la etiqueta
        echo("{'success': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarMateriasAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idmateria = $datos->idmateria;

        if ($this->model->eliminarMateria($idmateria))
            echo "{'success': true, 'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>