<?php

/**
 * Componente para gestionar los sistemas.
 *
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestprerequisitosController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatPreRequisitosMateriaModel();
    }

    public function gestprerequisitosAction()
    {
        $this->render();
    }

    public function cargarPrerequisitosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idmateria = $this->_request->getPost('idmateria');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $prerequisitos = $this->model->cargarPreRequisitosMateria($idmateria, $limit, $start, $filtros);
        $cant = $this->model->countPreRequisitosMateria($idmateria, $filtros);

        $result = array('cantidad' => $cant, 'datos' => $prerequisitos);
        echo json_encode($result);
    }

    public function cargarMateriasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $materias = $this->model->cargarMaterias($limit, $start, $filtros);
        $cant = $this->model->countMaterias($filtros);

        $result = array('cantidad' => $cant, 'datos' => $materias);
        echo json_encode($result);
    }

    public function cargarMateriasParaPreRequisitosAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idmateria = $this->getRequest()->getPost('idmateria');

        $materiaspre = $this->model->cargarMateriasParaPreRequisitos($idmateria, $limit, $start);
        $cant = $this->model->countMateriasParaPreRequisitos($idmateria, $limit, $start);

        $result = array('cantidad' => $cant, 'datos' => $materiaspre);
        echo json_encode($result);
    }

    public function insertarPrerequisitosAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);

        if (!is_array($datos)) {
            $idmateria = $datos->idmateria;
            $idmateriapre = $datos->idmateriapre;
            $estado = true; //$datos->estado;

            if ($this->model->insertarPreRequisitosMateria($idmateria, $idmateriapre, $estado, $usuario, $fecha))
                //devolver los datos, hay que agregar la etiqueta
                echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
        }else{
            foreach ($datos as $dato){
                $idmateria = $dato->idmateria;
                $idmateriapre = $dato->idmateriapre;
                $estado = true; //$datos->estado;

                $this->model->insertarPreRequisitosMateria($idmateria, $idmateriapre, $estado, $usuario, $fecha);
            }
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionarM}");
        }
    }

    /*public function modificarPrerequisitosAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $idmateria = $this->_request->getPost('idmateria');
        $idmateriapre = $this->_request->getPost('idmateriapre');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        $idpre = $this->_request->getPost('idpensum');
        $this->model->modificarPreRequisitosMateria($usuario, $idpre, $idmateria, $idmateriapre, $estado, $fecha);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }*/

    public function eliminarPrerequisitosAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idpre_requisito = $datos->idpre_requisito;

        if ($this->model->eliminarPreRequisitosMateria($idpre_requisito))
            echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>