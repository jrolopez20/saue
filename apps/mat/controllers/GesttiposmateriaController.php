<?php

/**
 * Componente para gestionar los sistemas.
 *
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GesttiposmateriaController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatTipoMateriaModel();
    }

    public function gesttiposmateriaAction()
    {
        $this->render();
    }

    public function cargarTiposMateriaAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = $this->_request->getPost('filtros');

        $tiposmateria = $this->model->cargarTiposMateria($limit, $start, $filtros);
        $cant = $this->model->countTiposMateria($filtros);
        $result = array('cantidad' => $cant, 'datos' => $tiposmateria);
        echo json_encode($result);
    }

    public function insertarTiposMateriaAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion_tipo_materia;
        $estado = $datos->estado;

        if ($this->model->insertarTipoMateria($descripcion, $estado, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarTiposMateriaAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idtipomateria = $datos->idtipomateria;
        $descripcion = $datos->descripcion_tipo_materia;
        $estado = $datos->estado;

        if ($this->model->modificarTipoMateria($idtipomateria, $descripcion, $estado, $usuario, $fecha))
            //devolver los datos, hay que agregar la etiqueta
            echo("{'success': true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarTiposMateriaAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idtipomateria = $datos->idtipomateria;

        if ($this->model->eliminarTipoMateria($idtipomateria))
            echo "{'success': true, 'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>