<?php

/**
 * Componente para gestionar los sistemas.
 *
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestareasController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatAreaModel();
    }

    public function gestareasAction()
    {
        $this->render();
    }

    public function cargarAreasAction()
    {
        //obtener todas las areas
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $areas = $this->model->cargarAreas($limit, $start, $filtros);
        $cant = $this->model->countAreas($filtros);

        $result = array('cantidad' => $cant, 'datos' => $areas);
        echo json_encode($result);
    }

    public function cargarAreasGeneralesAction()
    {
        //obtener todas las areas
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $areas_generales = $this->model->cargarAreasGenerales($limit, $start);

        //$result = array('cantidad' => $cant, 'datos' => $areas);
        echo json_encode($areas_generales);
    }

    public function insertarAreasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $descripcion = $datos->descripcion_area;
        $idareageneral = $datos->idareageneral;
        $estado = $datos->estado;

        $this->model->insertarArea($idareageneral, $descripcion, $estado, $usuario, $fecha);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'success':true, 'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarAreasAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idarea = $datos->idarea;
        $descripcion = $datos->descripcion_area;
        $idareageneral = $datos->idareageneral;
        $estado = $datos->estado;

        $this->model->modificarArea($idarea, $idareageneral, $descripcion, $estado, $usuario, $fecha);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'success':true, 'codMsg':1, 'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarAreasAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idArea = $datos->idarea; //$this->_request->getPost('idarea');
        $this->model->eliminarArea($idArea);
        echo "{'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>