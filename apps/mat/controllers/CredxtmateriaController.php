<?php

class CredxtmateriaController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatCredxMateriaModel();
    }

    public function credxtmateriaAction()
    {

        $this->render();
    }

    public function cargarCredxMateriaAction()
    {
        //obtener todos los créditos asociados a un tipo de carrera
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idfacultad = $this->_request->getPost('idfacultad');
        $idcarrera = $this->_request->getPost('idcarrera');
        $idenfasis = $this->_request->getPost('idenfasis');
        $filtros = $this->_request->getPost('filtros');

        $credxmateria = $this->model->cargarCredxMateria($idfacultad, $limit, $start, $filtros);
        //ver esto en todas las cargas, si realmente hace falta.
        $cant = $this->model->countCredxMateria($idfacultad, $filtros);

        $result = array('cantidad' => $cant, 'datos' => $credxmateria);
        echo json_encode($result);
    }

    public function cargarFacultadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $facultades = $this->model->cargarFacultades($limit, $start);

        echo json_encode($facultades);
    }
}

?>
