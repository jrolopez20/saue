<?php

class GestmatxpensumController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatPensumMateriaEnfasisTipomateriaModel();
    }

    public function gestmatxpensumAction()
    {
        $this->render();
    }

    public function cargarFacultadesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $facultades = $this->model->cargarFacultades($limit, $start);

        echo json_encode(array('datos'=>$facultades));
    }

    /*public function cargarTiposMateriaAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $tiposmateria = $this->model->cargarTiposMateria($limit, $start);
        $cant = $this->model->countTiposMateria();
        $result = array('cantidad' => $cant, 'datos' => $tiposmateria);
        echo json_encode($result);
    }*/

    public function cargarCarrerasAction()
    {
        //obtener todas las carreras
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idfacultad = $this->_request->getPost('idfacultad');

        $carreras = $this->model->cargarCarreras($idfacultad, $limit, $start);

        echo json_encode(array('datos' => $carreras));
    }

    public function cargarEnfasisAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');
        $enfasis = $this->model->cargarEnfasis($idcarrera, $limit, $start);
        /*$cant = $this->model->countEnfasis($idfacultad);
        $result = array('cantidad' => $cant, 'datos' => $enfasis);*/
        echo json_encode(array('datos' => $enfasis));
    }

    public function cargarPensumAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idcarrera = $this->_request->getPost('idcarrera');

        $pensums = $this->model->cargarPensums($idcarrera, $limit, $start);

        //$result = array('cantidad' => $cant, 'datos' => $pensums);
        echo json_encode(array('datos' => $pensums));
    }

    public function cargarAreasGeneralesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $areas_gnerales = $this->model->cargarAreasGenerales($limit, $start);

        //$result = array('cantidad' => $cant, 'datos' => $pensums);
        echo json_encode(array('datos' => $areas_gnerales));
    }

    public function cargarAreasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idenfasis = $this->_request->getPost('idenfasis');
        $idpensum = $this->_request->getPost('idpensum');
        $idareageneral = $this->_request->getPost('idareageneral');

        $areas = $this->model->cargarAreas($idenfasis, $idpensum, $idareageneral, $limit, $start);

        //$result = array('cantidad' => $cant, 'datos' => $pensums);
        echo json_encode(array('datos' => $areas));
    }

    public function cargarMateriasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $idpensum = $this->_request->getPost('idpensum');
        $idenfasis = $this->_request->getPost('idenfasis');
        $idarea = $this->_request->getPost('idarea');
        $ids = $this->model->obtenerIdsMaterias($idpensum, $idenfasis, $idarea, $limit, $start);
        $filtros = json_decode($this->_request->getPost('filtros'));

        $materias = $this->model->cargarMaterias($ids, $idpensum, $limit, $start, $filtros);
        $cant = $this->model->countMaterias($ids, $filtros);

        $result = array('cantidad' => $cant, 'datos' => $materias);
        echo json_encode($result);
    }

    public function cargarMatxPensumAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $idpensum = $this->_request->getPost('idpensum');
        $idenfasis = $this->_request->getPost('idenfasis');
        $idarea = $this->_request->getPost('idarea');
        //$idmateria = $this->_request->getPost('idmateria');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $matxPen = $this->model->cargarMatxPensum($idpensum, $idenfasis, $idarea, $limit, $start, $filtros);
        $cant = $this->model->countMateriaxPensum($idpensum, $idenfasis, $idarea, $filtros);

        $result = array('cantidad' => $cant, 'datos' => $matxPen);
        echo json_encode($result);
    }

    public function insertarMatxPensumAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);

        if (!is_array($datos)) {
            $idmateria = $datos->idmateria;
            $idenfasis = $datos->idenfasis;
            $idpensum = $datos->idpensum;
            $idarea = $datos->idarea;
            $creditos = $datos->creditos;
            $estado = $datos->estado;

            $this->model->insertarMatxPensum($idmateria, $idenfasis, $idpensum, $idarea, $creditos, $estado, $usuario, $fecha);
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
        }
        else{
            foreach ($datos as $dato){
                $idmateria = $dato->idmateria;
                $idenfasis = $dato->idenfasis;
                $idpensum = $dato->idpensum;
                $idarea = $dato->idarea;
                $creditos = $dato->creditos;
                $estado = $dato->estado;

                $this->model->insertarMatxPensum($idmateria, $idenfasis, $idpensum, $idarea, $creditos, $estado, $usuario, $fecha);
            }
            //devolver los datos, hay que agregar la etiqueta
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionarM}");
        }
    }

    /*public function modificarMatxPensumAction() {
        $usuario = $this->global->Perfil->idusuario;
        $idmateria = $this->_request->getPost('idmateria');
        $idenfasis = $this->_request->getPost('idenfasis');
        $idpensum = $this->_request->getPost('idpensum');
        $idarea = $this->_request->getPost('idarea');
        $creditos = $this->_request->getPost('creditos');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        $idmatxpensum = $this->_request->getPost('idmatxpensum');
        $this->model->modificarMatxPensum($usuario, $idmatxpensum, $idmateria, $idenfasis, $idpensum, $idarea, $creditos, $estado, $fecha);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }*/

    public function eliminarMatxPensumAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);

        $idpensumenfasismateriatipo = $datos->idpensumenfasismateriatipo;
        if ($this->model->eliminarMatxPensum($idpensumenfasismateriatipo))
            echo "{'success': true, 'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}

?>