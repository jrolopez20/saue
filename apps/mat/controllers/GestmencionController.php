<?php

class GestmencionController extends ZendExt_Controller_Secure
{

    private $model;

    public function init()
    {
        parent::init();
        $this->model = new DatMateriaMencionModel();
    }

    public function gestmencionAction()
    {
        $this->render();
    }

    public function cargarMatxMencionAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idmencion = $this->_request->getPost('idmencion');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $materiasxmencion = $this->model->cargarMateriasxMencion($idmencion, $limit, $start, $filtros);
        $cant = $this->model->countMateriasxMencion($idmencion, $filtros);

        $result = array('cantidad' => $cant, 'datos' => $materiasxmencion);
        echo json_encode($result);
    }

    public function cargarMencionesAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');

        $menciones = $this->model->cargarMenciones($limit, $start);

        $result = array('datos' => $menciones);
        echo json_encode($result);
    }

    public function cargarMateriasAction()
    {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idmencion = $this->_request->getPost('idmencion');
        $filtros = json_decode($this->_request->getPost('filtros'));

        $materias = $this->model->cargarMaterias($idmencion, $limit, $start, $filtros);
        $cantidad = $this->model->countMaterias($idmencion, $filtros);

        $result = array('cantidad' => $cantidad, 'datos' => $materias);
        echo json_encode($result);
    }

    public function insertarMatxMencionAction()
    {
        $usuario = $this->global->Perfil->idusuario;
        $fecha = date('Y-m-d H:i');

        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        $idmencion = $datos->idmencion;
        $idmateria = $datos->idmateria;
        $estado = $datos->estado;

        if ($this->model->insertarMatxMencion($idmencion, $idmateria, $estado, $usuario, $fecha))
            echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function eliminarMatxMencionAction()
    {
        $datos = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);

        $idmencion = $datos->idmencion;
        $idmateria = $datos->idmateria;

        if ($this->model->eliminarMatxMencion($idmencion, $idmateria))
            echo "{'success': true, 'codMsg':1,'mensaje': perfil.etiquetas.lbMsgInfEliminar}";
    }

}