<?php

class DatMateriaMencionModel extends ZendExt_Model
{

    public function DatMateriaMencionModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarMateriasxMencion($idmencion, $limit = 20, $start = 0, $filtros = null)
    {
        try {
            return DatMencionMateria::cargarMateriasxMencion($idmencion, $limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMateriasxMencion($idmencion, $filtros = null)
    {
        try {
            return DatMencionMateria::countMateriasxMencion($idmencion, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMenciones($limit = 20, $start = 0)
    {
        try {
            return DatMencionMateria::cargarMenciones($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function obtenerIdsMaterias($idmencion)
    {
        $ids_materias = DatMencionMateria::obtenerIdsMateriasRegistradas($idmencion);
        $ids = array();
        foreach ($ids_materias as $ids_materia) {
            $ids[] = $ids_materia['idmateria'];
        }
        return $ids;
    }

    public function cargarMaterias($idmencion, $limit = 20, $start = 0, $filtros = null)
    {
        try {
            $ids = $this->obtenerIdsMaterias($idmencion);
            return DatMencionMateria::cargarMaterias($ids, $limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMaterias($idmencion, $filtros = null)
    {
        try {
            $ids = $this->obtenerIdsMaterias($idmencion);
            return DatMencionMateria::countMaterias($ids, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarMatxMencion($idmencion, $idmateria, $estado, $usuario, $fecha)
    {
        try {
            $prereqmateria = new DatMencionMateria();
            $prereqmateria->idmateria = $idmateria;
            $prereqmateria->idmencion = $idmencion;
            $prereqmateria->idusuario = $usuario;
            $prereqmateria->estado = $estado;
            $prereqmateria->fecha = $fecha;
            $prereqmateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarMatxMencion($idmencion, $idmateria)
    {
        try {
            $DatMencionMateria = Doctrine::getTable("DatMencionMateria")->find(array($idmencion, $idmateria));
            $DatMencionMateria->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

