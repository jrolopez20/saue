<?php

class DatTipoMateriaModel extends ZendExt_Model
{
    public function DatTipoMateriaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarTiposMateria($limit, $start, $filtros)
    {
        return DatTipoMateria::cargarTiposMateria($limit, $start, $filtros);
    }

    public function countTiposMateria($filtros)
    {
        try {
            return DatTipoMateria::countTiposMateria($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarTipoMateria($descripcion, $estado, $usuario, $fecha)
    {
        try {
            $tipomateria = new DatTipoMateria();
            $tipomateria->idusuario = $usuario;
            $tipomateria->descripcion = $descripcion;
            $tipomateria->estado = $estado;
            $tipomateria->fecha = $fecha;
            $tipomateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTipoMateria($idtipomateria, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $tipomateria = Doctrine::getTable("DatTipoMateria")->find($idtipomateria);
            $tipomateria->idusuario = $usuario;
            $tipomateria->descripcion = $descripcion;
            $tipomateria->estado = $estado;
            $tipomateria->fecha = $fecha;
            $tipomateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarTipoMateria($idtipomateria)
    {
        try {
            $tipomateria = Doctrine::getTable("DatTipoMateria")->find($idtipomateria);
            $tipomateria->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

