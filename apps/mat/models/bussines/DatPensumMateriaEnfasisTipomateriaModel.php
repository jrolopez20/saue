<?php

class DatPensumMateriaEnfasisTipomateriaModel extends ZendExt_Model
{

    public function DatPensumMateriaEnfasisTipomateriaModel()
    {
        parent::ZendExt_Model();
    }

    public function obtenerIdsMaterias($idpensum, $idenfasis, $idarea, $limit, $start)
    {
        $ids_materias = DatPensumMateriaEnfasisTipomateria::cargarPEMT($idpensum, $idenfasis, $idarea, $limit, $start);
        $ids = array();
        foreach ($ids_materias as $ids_materia) {
            $ids[] = $ids_materia['idmateria'];
        }
        return $ids;
    }

    public function cargarMaterias($ids = array(), $idpensum, $limit = 20, $start = 0, $filtros = null)
    {
        try {
            $materias = DatPensumMateriaEnfasisTipomateria::cargarMaterias($ids, $limit, $start, $filtros);
            for($i=0; $i<count($materias); $i++){
                $credito = DatMateriaCredito::cargarCreditos($materias[$i]['idmateria'], $idpensum, $limit, $start);
                $materias[$i]['creditos'] = $credito[0]['creditos'];
            }
            return $materias;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarAreasGenerales($limit = 20, $start = 0)
    {
        try {
            return DatPensumMateriaEnfasisTipomateria::cargarAreasGenerales($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }


    public function cargarAreas($idenfasis, $idpensum, $idareageneral, $limit = 20, $start = 0)
    {
        try {
            return DatPensumMateriaEnfasisTipomateria::cargarAreas($idenfasis, $idpensum, $idareageneral, $limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMaterias($ids = array(), $filtros = null)
    {
        try {
            return DatPensumMateriaEnfasisTipomateria::countMaterias($ids, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarFacultades($limit, $start)
    {
        try {
            return $this->integrator->metadatos->ListadoEstructurasT($limit, $start);

        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMatxPensum($idpensum, $idenfasis, $idarea, $limit, $start, $filtros)
    {
        try {
            return DatPensumMateriaEnfasisTipomateria::cargarPEMT($idpensum, $idenfasis, $idarea, $limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMateriaxPensum($idpensum, $idenfasis, $idarea, $limit, $start, $filtros)
    {
        try {
            return DatPensumMateriaEnfasisTipomateria::countPEMT($idpensum, $idenfasis, $idarea, $limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }


    public function insertarMatxPensum($idmateria, $idenfasis, $idpensum, $idarea, $creditos, $estado, $usuario, $fecha)
    {
        try {
            $prereqmateria = new DatPensumMateriaEnfasisTipomateria();
            $prereqmateria->idmateria = $idmateria;
            $prereqmateria->idpensum = $idpensum;
            $prereqmateria->idenfasis = $idenfasis;
            $prereqmateria->idarea = $idarea;
            $prereqmateria->creditos = $creditos;
            $prereqmateria->idusuario = $usuario;
            $prereqmateria->estado = $estado;
            $prereqmateria->fecha = $fecha;
            $prereqmateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarPMETM($idpensumenfasismateriatipo, $idpensum, $idmateria, $idenfasis, $creditos, $idarea, $usuario, $descripcion, $estado, $fecha)
    {
        try {
            $prereqmateria = Doctrine::getTable("DatPensumMateriaEnfasisTipomateria")->find($idpensumenfasismateriatipo);
            $prereqmateria->idmateria = $idmateria;
            $prereqmateria->idpensum = $idpensum;
            $prereqmateria->idenfasis = $idenfasis;
            $prereqmateria->idarea = $idarea;
            $prereqmateria->creditos = $creditos;
            $prereqmateria->idusuario = $usuario;
            $prereqmateria->estado = $estado;
            $prereqmateria->fecha = $fecha;
            $prereqmateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }


    public function eliminarMatxPensum($idpensumenfasismateriatipo)
    {
        try {
            $DatPensumMateriaEnfasisTipomateria = Doctrine::getTable("DatPensumMateriaEnfasisTipomateria")->find($idpensumenfasismateriatipo);
            $DatPensumMateriaEnfasisTipomateria->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarCarreras($idfacultad, $limit, $start)
    {
        try {
            $filtro = new stdClass();
            $filtro->property = 'idfacultad';
            $filtro->value = $idfacultad;
            $filtros = array($filtro);

            $carreras = $this->integrator->facultad->cargarCarreras($limit, $start, $filtros);
            return $carreras;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarEnfasis($idcarrera, $limit, $start)
    {
        try {
            $carreras = $this->integrator->facultad->cargarEnfasis($idcarrera, $limit, $start);
            return $carreras;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarPensums($idcarrera, $limit, $start)
    {
        try {
            $filtro = new stdClass();
            $filtro->property = 'idcarrera';
            $filtro->value = $idcarrera;
            $filtros = array($filtro);

            $pensums = $this->integrator->facultad->cargarPensum($limit, $start, $filtros); //DatPensumMateriaEnfasisTipomateria::cargarPensums($idcarrera, $limit, $start);
            return $pensums;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

