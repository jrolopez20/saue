<?php

class DatPreRequisitosMateriaModel extends ZendExt_Model
{
    public function DatPreRequisitosMateriaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarMaterias($limit = 20, $start = 0, $filtros = null)
    {
        try {
            return DatPreRequisitosMateria::cargarMaterias($limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMaterias($filtros = null)
    {
        try {
            return DatPreRequisitosMateria::countMaterias($filtros = null);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarPreRequisitosMateria($idmateria, $limit = 20, $start = 0, $filtros = null)
    {
        try {
            return DatPreRequisitosMateria::cargarPreRequisitosMateria($idmateria, $limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    
    public function countPreRequisitosMateria($idmateria, $filtros = null)
    {
        try {
            return DatPreRequisitosMateria::countPreRequisitosMateria($idmateria, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMateriasParaPreRequisitos($idmateria, $limit = 20, $start = 0)
    {
        try {
            $materias_pre = DatPreRequisitosMateria::cargarPreRequisitosMateria($idmateria, $limit, $start);
            $ids_materias_pre = array($idmateria);
            foreach($materias_pre as $materia_pre){
                $ids_materias_pre[] = $materia_pre['idmateriapre'];
            }
            return DatPreRequisitosMateria::cargarMateriasParaPreRequisitos($ids_materias_pre, $limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMateriasParaPreRequisitos($idmateria, $limit = 20, $start = 0)
    {
        try {
            $materias_pre = DatPreRequisitosMateria::cargarPreRequisitosMateria($idmateria, $limit, $start);
            $ids_materias_pre = array($idmateria);
            foreach($materias_pre as $materia_pre){
                $ids_materias_pre[] = $materia_pre['idmateriapre'];
            }
            return DatPreRequisitosMateria::countMateriasParaPreRequisitos($ids_materias_pre);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    

    public function insertarPreRequisitosMateria($idmateria, $idmateriapre, $estado, $usuario, $fecha)
    {
        try {
            $prereqmateria = new DatPreRequisitosMateria();
            $prereqmateria->idmateria = $idmateria;
            $prereqmateria->idmateriapre = $idmateriapre;
            $prereqmateria->idusuario = $usuario;
            $prereqmateria->estado = $estado;
            $prereqmateria->fecha = $fecha;
            $prereqmateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

 

    /*public function modificarPreRequisitosMateria($idmateria,$idmateriapre,$usuario, $descripcion, $estado, $fecha)
    {
        try {
            $prereqmateria = Doctrine::getTable("PreRequisitosMateria")->find($idmateria);
            
            $prereqmateria->idmateriapre = $idmateriapre;
            $prereqmateria->idusuario = $usuario;
            $prereqmateria->descripcion = $descripcion;
            $prereqmateria->estado = $estado;
            $prereqmateria->fecha = $fecha;
            $prereqmateria->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }*/

    public function eliminarPreRequisitosMateria($idpre_requisito)
    {
        try {
            $prereqmateria = Doctrine::getTable("DatPreRequisitosMateria")->find($idpre_requisito);
            $prereqmateria->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}

