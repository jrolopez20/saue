<?php

class DatAreaModel extends ZendExt_Model
{

    public function DatAreaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarAreas($limit = 20, $start = 0, $filtros = null)
    {
        return DatArea::cargarAreas($limit, $start, $filtros);
    }

    public function cargarAreasGenerales($limit = 20, $start = 0)
    {
        return DatArea::cargarAreasGenerales($limit, $start);
    }

    public function countAreas($filtros = null)
    {
        try {
            return DatArea::countAreas($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarArea($idareageneral, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $area = new DatArea();
            $area->idusuario = $usuario;
            $area->descripcion = $descripcion;
            $area->idareageneral = $idareageneral;
            $area->estado = $estado;
            $area->fecha = $fecha;
            $area->save();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarArea($idarea, $idareageneral, $descripcion, $estado, $usuario, $fecha)
    {
        try {
            $area = Doctrine::getTable("DatArea")->find($idarea);
            $area->idarea = $idarea;
            $area->idusuario = $usuario;
            $area->descripcion = $descripcion;
            $area->idareageneral = $idareageneral;
            $area->estado = $estado;
            $area->fecha = $fecha;
            $area->save();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarArea($idarea)
    {
        try {
            $area = Doctrine::getTable("DatArea")->find($idarea);
            $area->delete();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
