<?php

class DatMateriaModel extends ZendExt_Model
{
    public function DatMateriaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarMaterias($limit = 20, $start = 0, $filtros = null)
    {
        try {
            return DatMateria::cargarMaterias($limit, $start, $filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMaterias($filtros = null)
    {
        try {
            return DatMateria::countMaterias($filtros);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarIdiomas($limit = 20, $start = 0)
    {
        try {
            return $this->integrator->nomencladores->cargarIdiomas($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarCarreras($limit = 20, $start = 0)
    {
        try {
            return $this->integrator->facultad->cargarCarreras($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarCreditos($idmateria, $limit = 20, $start = 0, $filtros = null)
    {
        try {
            $creditos = array();
            if(!$filtros)
                return $creditos;
            $pensums = $this->integrator->facultad->cargarPensum($limit, $start, $filtros);
            foreach($pensums as $pensum){
                $credito = DatMateriaCredito::cargarCreditos($idmateria, $pensum['idpensum'], $limit, $start);
                if($credito)
                    $creditos[]=$credito[0];
                else
                    $creditos[]=$pensum;
            }
            return $creditos;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function cargarMateriasXAreas($idmateria, $limit = 20, $start = 0)
    {

        try {
            return DatMateria::cargarMateriasXArea($idmateria, $limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarMateria($codmateria, $descripcion, $traduccion, $min_nota_materia, $obligatoria,
                                    $estado, $idtipomateria, $ididioma, $nivel, $usuario, $fecha)
    {
        try {
            $materia = new DatMateria();

            /*if (isset($idarea))
                $materia->idarea = $idarea;*/
            if (isset($ididioma))
                $materia->ididioma = $ididioma;
            if (isset($nivel))
                $materia->nivel = $nivel;
            $materia->traduccion = $traduccion;
            $materia->codmateria = $codmateria;
            $materia->idusuario = $usuario;
            $materia->min_nota_materia = $min_nota_materia;
            $materia->obligatoria = $obligatoria;
            $materia->descripcion = $descripcion;
            $materia->idtipomateria = $idtipomateria;
            $materia->estado = $estado;
            $materia->fecha = $fecha;
            $materia->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function gestionarCreditos($idmateriacredito, $idmateria, $idpensum, $creditos, $usuario, $fecha)
    {
        if($idmateriacredito == 0 )
            return $this->insertarCreditos($idmateria, $idpensum, $creditos, true, $usuario, $fecha);
        return $this->modificarCreditos($idmateriacredito, $creditos, $usuario, $fecha);
    }

    public function insertarCreditos($idmateria, $idpensum, $creditos, $estado, $usuario, $fecha)
    {
        try {
            $materia_credito = new DatMateriaCredito();
            $materia_credito->idmateria = $idmateria;
            $materia_credito->idpensum = $idpensum;
            $materia_credito->creditos = $creditos;
            $materia_credito->idusuario = $usuario;
            $materia_credito->estado = $estado;
            $materia_credito->fecha = $fecha;
            $materia_credito->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarCreditos($idmateriacredito, $creditos, $usuario, $fecha)
    {
        try {
            $materia_credito = Doctrine::getTable("DatMateriaCredito")->find($idmateriacredito);
            $materia_credito->creditos = $creditos;
            $materia_credito->idusuario = $usuario;
            //$materia_credito->estado = $estado;
            $materia_credito->fecha = $fecha;
            $materia_credito->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function countMaateriasXArea($idmateria)
    {
        try {
            return DatMateria::countMateriasXArea($idmateria);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarMateria($idmateria, $codmateria, $descripcion, $traduccion, $min_nota_materia,
                                     $obligatoria, $estado, $idtipomateria, $ididioma, $nivel, $usuario, $fecha)
    {
        try {
            $materia = Doctrine::getTable("DatMateria")->find($idmateria);

            /*if (isset($idarea))
                $materia->idarea = $idarea;*/
            if (isset($ididioma))
                $materia->ididioma = $ididioma;
            if (isset($nivel))
                $materia->nivel = $nivel;
            $materia->traduccion = $traduccion;
            $materia->codmateria = $codmateria;
            $materia->idusuario = $usuario;
            $materia->min_nota_materia = $min_nota_materia;
            $materia->obligatoria = $obligatoria;
            $materia->descripcion = $descripcion;
            $materia->idtipomateria = $idtipomateria;
            $materia->estado = $estado;
            $materia->fecha = $fecha;
            $materia->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarMateria($idmateria)
    {
        try {
            $materia = Doctrine::getTable("DatMateria")->find($idmateria);
            $materia->delete();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
