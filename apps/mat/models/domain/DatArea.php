<?php

class DatArea extends BaseDatArea
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatMateria', array('local' => 'idarea', 'foreign' => 'idarea'));
        $this->hasOne('NomAreaGeneral', array('local' => 'idareageneral', 'foreign' => 'idareageneral'));
    }

    static public function cargarAreas($limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();

        $query->from("DatArea a")
            ->innerJoin("a.NomAreaGeneral");

        if ($filtros) {
            foreach ($filtros as $filtro) {
                if (strstr($filtro->property, 'descripcion')) {
                    $query->addWhere("LOWER(a.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                } else {
                    $query->addWhere("a.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray(true);
    }

    static public function cargarAreasGenerales($limit, $start)
    {
        $query = Doctrine_Query::create();

        $result = $query->from("NomAreaGeneral")
            ->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function countAreas($filtros)
    {
        $query = Doctrine_Query::create();

        $query->from("DatArea a")
            ->innerJoin("a.NomAreaGeneral nag");

        if ($filtros) {
            foreach ($filtros as $filtro) {
                if (strstr($filtro->property, 'descripcion')) {
                    $query->addWhere("LOWER(a.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                } else {
                    $query->addWhere("a.$filtro->property=?", $filtro->value);
                }
            }
        }

        $result = $query->execute();

        return $result->count();
    }

}

