<?php

abstract class BaseDatTipoMateria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_tipo_materia');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array ('notnull' => true,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => true,'primary' => false, 'default' => true));
        $this->hasColumn('idtipomateria', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_tipo_materia'));
    }


}

