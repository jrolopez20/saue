<?php

abstract class BaseDatPensumMateriaEnfasisTipomateria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_pensum_materia_enfasis_tipomateria');
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => true,'primary' => false, 'default' => true));
        $this->hasColumn('creditos', 'double precision', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idmateria', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idarea', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idenfasis', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpensum', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idpensumenfasismateriatipo', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_pensum_materia_enfasis_tipomateria'));
    }


}

