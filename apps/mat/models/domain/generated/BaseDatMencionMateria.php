<?php

abstract class BaseDatMencionMateria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_mencion_materia');
        $this->hasColumn('idmencion', 'numeric', 19, array ('notnull' => true,'primary' => true));
        $this->hasColumn('idmateria', 'numeric', 19, array ('notnull' => true,'primary' => true));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => true,'primary' => false, 'default' => true));
        $this->hasColumn('idusuario', 'numeric', 19, array ('notnull' => true,'primary' => false));
    }


}

