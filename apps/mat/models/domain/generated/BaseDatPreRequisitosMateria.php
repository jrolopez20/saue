<?php

abstract class BaseDatPreRequisitosMateria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_pre_requisitos_materia');
        $this->hasColumn('idpre_requisito', 'numeric', null, array('notnull' => true, 'primary' => true));
        $this->hasColumn('idmateria', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idmateriapre', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => true,'primary' => false, 'default' => ''));
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => true,'primary' => false, 'default' => true));
    }


}

