<?php

abstract class BaseDatMateriaCredito extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_materia_credito');
        $this->hasColumn('idmateriacredito', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_saue.seq_dat_materia_credito'));
        $this->hasColumn('idmateria', 'numeric', 19, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idpensum', 'numeric', 19, array('notnull' => true, 'primary' => false));
        $this->hasColumn('creditos', 'real', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fecha', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array('notnull' => false, 'primary' => false, 'default' => true));
    }


}

