<?php

abstract class BaseDatMateria extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_materia');
        $this->hasColumn('estado', 'boolean', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('obligatoria', 'boolean', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('ididioma', 'numeric', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('min_nota_materia', 'integer', null, array('notnull' => true, 'primary' => false));
        //$this->hasColumn('creditos', 'real', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idtipomateria', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('traduccion', 'text', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('fecha', 'date', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('codmateria', 'character varying', null, array('notnull' => true, 'primary' => false));
        $this->hasColumn('idmateria', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_saue.seq_materia'));
        $this->hasColumn('nivel', 'integer', null, array('notnull' => false));
    }


}

