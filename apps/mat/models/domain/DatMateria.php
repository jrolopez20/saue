<?php

class DatMateria extends BaseDatMateria
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasOne('DatIdioma', array('local' => 'ididioma', 'foreign' => 'ididioma'));
        $this->hasOne('DatArea', array('local' => 'idarea', 'foreign' => 'idarea'));
        $this->hasMany('DatPreRequisitosMateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
        $this->hasMany('DatPreRequisitosMateria', array('local' => 'idmateria', 'foreign' => 'idmateriapre'));
        $this->hasMany('DatPensumMateriaEnfasisTipomateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
        //$this->hasMany('DatMateriaConvalidada', array('local' => 'idmateria', 'foreign' => 'idmateria'));
        //$this->hasMany('DatMateriaFacultadCarreraEnfasis', array('local' => 'idmateria', 'foreign' => 'idmateria'));
        //$this->hasMany('DatMensionMateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
    }

    static public function cargarMaterias($limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT
                    m.*,
                    (m.ididioma IS NOT NULL) as have_idioma--,

                    --a.idarea,
                    --a.descripcion as descripcion_area
                FROM
                    mod_saue.dat_materia m
                    --INNER JOIN mod_saue.dat_area a ON a.idarea=m.idarea";

        $params = array();
        if ($filtros) {
            $sql .= "\nWHERE ";
            $hasWhere = false;
            $sqlSubWhere = "";
            foreach ($filtros as $filtro) {

                if ($filtro->property == 'codmateria' || $filtro->property == 'descripcion') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, "\n     OR LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                    continue;
                }

                if ($hasWhere)
                    $sql .= "\nAND ";
                $sql .= "m.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        }
        if ($sqlSubWhere) {
            if ($hasWhere)
                $sql .= "\nAND ";
            $sql .= $sqlSubWhere;
        }

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countMaterias($filtros)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT (m.idmateria)
                FROM
                    mod_saue.dat_materia m
                    --INNER JOIN mod_saue.dat_area a ON a.idarea=m.idarea";

        if ($filtros) {
            $sql .= "\nWHERE ";
            $hasWhere = false;
            $params = array();
            $sqlSubWhere = "";
            foreach ($filtros as $filtro) {

                if ($filtro->property == 'codmateria' || $filtro->property == 'descripcion') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, "\n     OR LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                    continue;
                }

                if ($hasWhere)
                    $sql .= "\nAND ";
                $sql .= "m.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        }
        if ($sqlSubWhere) {
            if ($hasWhere)
                $sql .= "\nAND ";
            $sql .= $sqlSubWhere;
        }

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetch();

        return $result['count'];
    }

    static public function cargarMateriasXArea($idarea, $limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMateria m")
            ->where("m.idarea=?", $idarea)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }

    static public function countMateriasXArea($idarea)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMateria m")
            ->where("m.idarea=?", $idarea)
            ->execute();
        return $result->count();
    }

    static public function eliminarMateria($idmateria)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatMateria m")
            ->where("m.idmateria=?", $idmateria)
            ->execute();
        return $result->count();
    }


}

