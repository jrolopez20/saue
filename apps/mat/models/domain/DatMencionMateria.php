<?php

class DatMencionMateria extends BaseDatMencionMateria
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasOne('DaMateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
        //$this->hasOne('DatMencion', array('local' => 'idmencion', 'foreign' => 'idmencion'));
    }

    static public function cargarMateriasxMencion($idmencion, $limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();

        $params = array();

        $sql = "SELECT
                    mm.*,
                    ma.codmateria,
                    ma.descripcion
                FROM
                    mod_saue.dat_mencion_materia mm
                    INNER JOIN mod_saue.dat_mencion me ON me.idmencion=mm.idmencion
                    INNER JOIN mod_saue.dat_materia ma ON ma.idmateria=mm.idmateria
                WHERE
                    mm.idmencion = ?";
        $params[] = $idmencion;

        if ($filtros) {
            $sqlSubWhere = "";
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'codmateria' || $filtro->property == 'descripcion') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(mm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, "\n     OR LOWER(mm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                    continue;
                }
                $sql .= "\nAND ";
                $sql .= "mm.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        }
        if ($sqlSubWhere) {
            if ($hasWhere)
                $sql .= "\nAND ";
            $sql .= $sqlSubWhere;
        }

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countMateriasxMencion($idmencion, $filtros)
    {
        $query = Doctrine_Query::create();

        $params = array();

        $sql = "SELECT COUNT (mm.idmencion)
                FROM
                    mod_saue.dat_mencion_materia mm
                    INNER JOIN mod_saue.dat_mencion me ON me.idmencion=mm.idmencion
                    INNER JOIN mod_saue.dat_materia ma ON ma.idmateria=mm.idmateria
                WHERE
                    mm.idmencion = ?";
        $params[] = $idmencion;

        if ($filtros) {
            $sqlSubWhere = "";
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'codmateria' || $filtro->property == 'descripcion') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(mm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, "\n     OR LOWER(mm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                    continue;
                }
                $sql .= "\nAND ";
                $sql .= "mm.$filtro->property = ?";
                $params[] = $filtro->value;
                $hasWhere = true;
            }
        }
        if ($sqlSubWhere) {
            if ($hasWhere)
                $sql .= "\nAND ";
            $sql .= $sqlSubWhere;
        }

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetch();

        return $result['count'];
    }

    static public function cargarMenciones($limit, $start)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT
                    m.idmencion,
                    m.descripcion
                FROM
                    mod_saue.dat_mencion m
                WHERE
                    m.estado = ?";
        $params[] = true;

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function obtenerIdsMateriasRegistradas($idmencion)
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->select("mm.idmateria")
            ->from("DatMencionMateria mm")
            ->where("mm.idmencion = ?", $idmencion)
            ->execute();

        return $result->toArray();
    }

    static public function cargarMaterias($ids, $limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatMateria m")
            ->where("m.estado = ?", true)
            ->whereNotIn('m.idmateria', $ids);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'descripcion' || $filtro->property == 'codmateria') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("m.$filtro->property=?", $filtro->value);
                }
            }
        }
        if ($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

    static public function countMaterias($ids, $filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatMateria m")
            ->where("m.estado = ?", true)
            ->whereNotIn('m.idmateria', $ids);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'descripcion' || $filtro->property == 'codmateria') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("m.$filtro->property=?", $filtro->value);
                }
            }
        }
        if ($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->execute()
            ->count();

        return $result;
    }

}

