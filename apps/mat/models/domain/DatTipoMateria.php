<?php

class DatTipoMateria extends BaseDatTipoMateria
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasMany ('DatPensumMateriaEnfasisTipomateria', array ('local' => 'idtipomateria', 'foreign' => 'idtipomateria'));
    }

    static public function cargarTiposMateria($limit = 20, $start = 0,  $filtros = null)
    {
        $query = Doctrine_Query::create();

        $query->from("DatTipoMateria tm");
        if (isset($filtro))
            $query->where("lower(tm.descripcion) LIKE '%".strtolower($filtro)."%'");
        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray();
    }

        static public function countTiposMateria($filtro = null)
    {
        $query = Doctrine_Query::create();

        $query->from("DatTipoMateria tm");
        if (isset($filtro))
            $query->where("lower(tm.descripcion) LIKE '%".strtolower($filtro)."%'");
        $result = $query->execute();

        return $result->count();
    }



    static public function eliminarTipoMateria($idtipomateria)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatTipoMateria m")
            ->where("m.idtipomateria=?", $idtipomateria)
            ->execute();
        return $result->count();
    }


}

