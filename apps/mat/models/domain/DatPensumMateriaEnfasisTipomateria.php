<?php

class DatPensumMateriaEnfasisTipomateria extends BaseDatPensumMateriaEnfasisTipomateria
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasOne('DatPensum', array('local' => 'idpensum', 'foreign' => 'idpensum'));
        //$this->hasOne('DatEnfasis', array('local' => 'idenfasis', 'foreign' => 'idenfasis'));
        $this->hasOne('DatMateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
    }

    static public function cargarPEMT($idpensum, $idenfasis, $idarea, $limit = 20, $start = 0, $filtros = null)
    {
        $hasWhere = false;
        $params = array();
        $query = Doctrine_Query::create();

        $sql = "SELECT
                    p.*,
                    m.codmateria as codmateria,
                    m.descripcion as descripcion
                FROM
                    mod_saue.dat_pensum_materia_enfasis_tipomateria p
                    INNER JOIN mod_saue.dat_pensum pe ON pe.idpensum=p.idpensum
                    INNER JOIN mod_saue.dat_enfasis e ON e.idenfasis=p.idenfasis
                    INNER JOIN mod_saue.dat_materia m ON m.idmateria=p.idmateria
                    INNER JOIN mod_saue.dat_area a ON a.idarea=p.idarea";

        if (isset($idpensum)) {
            $sql .= "\nWHERE p.idpensum = ?";
            $hasWhere = true;
            $params[] = $idpensum;
        };
        if (isset($idenfasis)) {
            if ($hasWhere)
                $sql .= "\nAND p.idenfasis = ?";
            else
                $sql .= "\nWHERE p.idenfasis = ?";
            $hasWhere = true;
            $params[] = $idenfasis;
        };
        if (isset($idarea)) {
            if ($hasWhere)
                $sql .= "\nAND p.idarea = ?";
            else
                $sql .= "\nWHERE p.idarea = ?";
            $hasWhere = true;
            $params[] = $idarea;
        }; /*
        if ($filtros) {
            if ($hasWhere)
                $sql .= "\nAND lower(m.descripcion) LIKE '%" . strtolower($filtro) . "%'";
            else
                $sql .= "\nWHERE lower(m.descripcion) LIKE '%" . strtolower($filtro) . "%'";
            $hasWhere = true;
        }*/

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countPEMT($idpensum, $idenfasis, $idarea, $filtros = null)
    {
        $hasWhere = false;
        $params = array();
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT (p.idpensumenfasismateriatipo)
                FROM
                    mod_saue.dat_pensum_materia_enfasis_tipomateria p
                    INNER JOIN mod_saue.dat_pensum pe ON pe.idpensum=p.idpensum
                    INNER JOIN mod_saue.dat_enfasis e ON e.idenfasis=p.idenfasis
                    INNER JOIN mod_saue.dat_materia m ON m.idmateria=p.idmateria
                    INNER JOIN mod_saue.dat_area a ON a.idarea=p.idarea";

        if (isset($idpensum)) {
            $sql .= "\nWHERE p.idpensum = ?";
            $hasWhere = true;
            $params[] = $idpensum;
        };
        if (isset($idenfasis)) {
            if ($hasWhere)
                $sql .= "\nAND p.idenfasis = ?";
            else
                $sql .= "\nWHERE p.idenfasis = ?";
            $hasWhere = true;
            $params[] = $idenfasis;
        };
        if (isset($idarea)) {
            if ($hasWhere)
                $sql .= "\nAND p.idarea = ?";
            else
                $sql .= "\nWHERE p.idarea = ?";
            $hasWhere = true;
            $params[] = $idarea;
        };
        /*if ($filtros) {
            if ($hasWhere)
                $sql .= "\nAND lower(m.descripcion) LIKE '%" . strtolower($filtro) . "%'";
            else
                $sql .= "\nWHERE lower(m.descripcion) LIKE '%" . strtolower($filtro) . "%'";
            $hasWhere = true;
        }*/

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetch();

        return $result['count'];
    }

    static public function cargarAreasGenerales($limit, $start)
    {
        $query = Doctrine_Query::create();

        $result = $query
            ->from("NomAreaGeneral nag")
            ->limit($limit)
            ->offset($start)
            ->execute()
            ->toArray();

        return $result;
    }

    static public function cargarAreas($idenfasis, $idpensum, $idareageneral, $limit, $start)
    {
        $query = Doctrine_Query::create();

        $params = array();

        $sql = "SELECT
                    ac.*,

                    a.descripcion
                FROM
                    mod_saue.dat_area_credito ac
                    INNER JOIN mod_saue.dat_enfasis e ON e.idenfasis=ac.idenfasis
                    INNER JOIN mod_saue.dat_pensum p ON p.idpensum=ac.idpensum
                    INNER JOIN mod_saue.nom_area_general ag ON ag.idareageneral=ac.idareageneral
                    INNER JOIN mod_saue.dat_area a ON a.idarea=ac.idarea";

        $hasWhere = false;
        if (isset($idenfasis)) {
            $sql .= "\nWHERE ac.idenfasis = ?";
            $params[] = $idenfasis;
            $hasWhere = true;
        }
        if (isset($idpensum)) {
            if ($hasWhere)
                $sql .= "\nAND ac.idpensum = ?";
            else
                $sql .= "\nWHERE ac.idpensum = ?";
            $params[] = $idpensum;
            $hasWhere = true;
        }
        if (isset($idareageneral)) {
            if ($hasWhere)
                $sql .= "\nAND ac.idareageneral = ?";
            else
                $sql .= "\nWHERE ac.idareageneral = ?";
            $params[] = $idareageneral;
        }

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function cargarMaterias($ids, $limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatMateria m")
            ->where("m.estado = ?", true)
            ->whereNotIn('m.idmateria', $ids);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'descripcion' || $filtro->property == 'codmateria') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("m.$filtro->property=?", $filtro->value);
                }
            }
        }
        if ($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->limit($limit)
            ->offset($start)
            ->execute()
            ->toArray();

        return $result;
    }

    static public function countMaterias($ids, $filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatMateria m")
            ->where("m.estado = ?", true)
            ->whereNotIn('m.idmateria', $ids);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property == 'descripcion' || $filtro->property == 'codmateria') {
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(m.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("m.$filtro->property=?", $filtro->value);
                }
            }
        }
        if ($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->execute()
            ->count();

        return $result;
    }

}

