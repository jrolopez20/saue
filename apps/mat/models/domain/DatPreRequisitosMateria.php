<?php

class DatPreRequisitosMateria extends BaseDatPreRequisitosMateria
{

    static public function cargarMaterias($limit, $start, $filtros){
        $query = Doctrine_Query::create();

        $query
            ->from("DatMateria dm")
            ->where("dm.estado = ?", true);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property ==  'descripcion' || $filtro->property == 'codmateria') {
                    //$query->addWhere("LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("dm.$filtro->property=?", $filtro->value);
                }
            }
        }
        if($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray(true);
    }

    static public function countMaterias($filtros){
        $query = Doctrine_Query::create();
        $query
            ->from("DatMateria dm")
            ->where("dm.estado=?", true);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property ==  'descripcion' || $filtro->property == 'codmateria') {
                    //$query->addWhere("LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("dm.$filtro->property=?", $filtro->value);
                }
            }
        }
        if($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->execute();

        return $result->count();
    }

    static public function cargarPreRequisitosMateria($idmateria, $limit, $start, $filtros)
    {
        $query = Doctrine_Query::create();

        $query
            ->from("DatPreRequisitosMateria prm")
            ->innerJoin("prm.DatMateria dm")
            ->where("prm.idmateria=?", $idmateria);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property ==  'descripcion' || $filtro->property == 'codmateria') {
                    //$query->addWhere("LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("dm.$filtro->property=?", $filtro->value);
                }
            }
        }
        if($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->limit($limit)
            ->offset($start)
            ->execute();

        return $result->toArray(true);
    }

    static public function countPreRequisitosMateria($idmateria, $filtros)
    {
        $query = Doctrine_Query::create();
        $query
            ->from("DatPreRequisitosMateria prm")
            ->innerJoin("prm.DatMateria dm")
            ->where("prm.idmateria=?", $idmateria);

        $sqlSubWhere = "";
        if ($filtros) {
            foreach ($filtros as $filtro) {
                if ($filtro->property ==  'descripcion' || $filtro->property == 'codmateria') {
                    //$query->addWhere("LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%'");
                    if (!$sqlSubWhere) {
                        $sqlSubWhere .= "(LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')";
                    } else
                        $sqlSubWhere = substr_replace($sqlSubWhere, " OR LOWER(dm.$filtro->property) LIKE '%" . strtolower($filtro->value) . "%')", strlen($sqlSubWhere) - 1);
                } else {
                    $query->addWhere("dm.$filtro->property=?", $filtro->value);
                }
            }
        }
        if($sqlSubWhere)
            $query->addWhere($sqlSubWhere);

        $result = $query->execute();

        return $result->count();
    }

    /*static public function eliminarPreRequisitosMateria($idmateria)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatPreRequisitosMateria prm")
            ->where("prm.idmateriapre=?", $idmateria)
            ->execute();
        return $result->count();
    }*/

    static public function cargarMateriasParaPreRequisitos($ids_materias_pre, $limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMateria m")
            ->whereNotIn("m.idmateria", $ids_materias_pre)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();
    }

    static public function countMateriasParaPreRequisitos($ids_materias_pre)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatMateria m")
            ->whereNotIn("m.idmateria", $ids_materias_pre)
            ->execute();
        return $result->count();
    }

    public function setUp()
    {
        parent :: setUp();
        $this->hasOne('DatMateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
        $this->hasOne('DatMateria', array('local' => 'idmateriapre', 'foreign' => 'idmateria'));
    }
}

