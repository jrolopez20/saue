<?php

class DatMateriaCredito extends BaseDatMateriaCredito
{

    public function setUp()
    {
        parent :: setUp();
        //$this->hasOne('DatPensum', array('local' => 'idpensum', 'foreign' => 'idpensum'));
        $this->hasMany('DatMateria', array('local' => 'idmateria', 'foreign' => 'idmateria'));
    }

    static public function cargarCreditos($idmateria, $idpensum, $limit, $start)
    {
        $query = Doctrine_Query::create();

        $params = array();

        $sql = "SELECT
                    mc.*,

                    p.descripcion
                FROM
                    mod_saue.dat_materia_credito mc
                    INNER JOIN mod_saue.dat_materia m ON m.idmateria=mc.idmateria
                    INNER JOIN mod_saue.dat_pensum p ON p.idpensum=mc.idpensum
                WHERE
                    mc.idmateria = ? AND mc.idpensum = ?";

        $params[] = $idmateria;
        $params[] = $idpensum;

        $sql .= "\nLIMIT ? OFFSET ?";
        $params[] = $limit;
        $params[] = $start;

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll();

        return $result;
    }

    static public function countCreditos($idmateria)
    {
        $query = Doctrine_Query::create();

        $sql = "SELECT COUNT (mc.idmateriacredito)
                FROM
                    mod_saue.dat_materia_credito mc
                    INNER JOIN mod_saue.dat_materia m ON m.idmateria=mc.idmateria
                    INNER JOIN mod_saue.dat_pensum p ON p.idpensum=mc.idpensum
                WHERE
                    mc.idmateria = ?";
        $params = array($idmateria);

        $stmt = $query
            ->getConnection()
            ->prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetch();

        return $result['count'];
    }


}

