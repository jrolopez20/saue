<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestcolegiosController extends ZendExt_Controller_Secure
{

  private $model;

    public function init() {
        parent::init();
        $this->model = new DatColegiosModel();
    }

    public function gestcolegiosAction()
    {
        $this->render();
    }

     public function cargarColegiosAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $estadosCiviles = $this->model->cargarColegios($limit, $start);
        $cant = $this->model->countColegio();
        $result = array('cantidad' => $cant, 'datos' => $estadosCiviles);
        echo json_encode($result);
    }

    public function insertarColegioAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarColegio($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarColegioAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idcolegio = $this->_request->getPost('idcolegio');
        $this->model->modificarColegio($idcolegio,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarColegioAction() {
        $idcolegio = $this->_request->getPost('idcolegio');
        $this->model->eliminarColegio($idcolegio);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>