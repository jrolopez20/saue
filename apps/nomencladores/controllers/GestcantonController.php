<?php

class GestcantonController extends ZendExt_Controller_Secure
{
    private $model;

    public function init() {
        parent::init();
        $this->model = new DatCantonModel();
    }


    public function gestcantonAction()
    {
        $this->render();
    }

public function cargarCantonAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $cantons = $this->model->cargarCanton($limit, $start);
        $cant = $this->model->countCanton();
        $result = array('cantidad' => $cant, 'datos' => $cantons);
        echo json_encode($result);
    }

    public function insertarCantonAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarCanton($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarCantonAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idcanton = $this->_request->getPost('idcanton');
        $this->model->modificarCanton($idcanton,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarCantonAction() {
        $idcanton = $this->_request->getPost('idcanton');
        $this->model->eliminarCanton($idcanton);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }
}

?>