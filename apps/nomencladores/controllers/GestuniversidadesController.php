<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestuniversidadesController extends ZendExt_Controller_Secure
{

    private $model;

    public function init() {
        parent::init();
        $this->model = new DatUniversidadModel();
    }
    public function gestuniversidadesAction()
    {
        $this->render();
    }
public function cargarUniversidadAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $universidades = $this->model->cargarUniversidades($limit, $start);
        $cant = $this->model->countUniversidades();
        $result = array('cantidad' => $cant, 'datos' => $universidades);
        echo json_encode($result);
    }

    public function insertarUniversidadAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarUniversidad($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarUniversidadAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $this->model->modificarUniversidad($iduniversidad,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarUniversidadAction() {
        $iduniversidad = $this->_request->getPost('iduniversidad');
        $this->model->eliminarUniversidad($iduniversidad);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>