<?php

class GesttpasantiaController extends ZendExt_Controller_Secure
{
 private $model;

    public function init() {
        parent::init();
        $this->model = new DatTipoPracticaModel();
    }

    public function gesttpasantiaAction()
    {
        $this->render();
    }
public function cargarTPasantiaAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $tpasantias = $this->model->cargarTPasantias($limit, $start);
        $cant = $this->model->countTPasantias();
        $result = array('cantidad' => $cant, 'datos' => $tpasantias);
        echo json_encode($result);
    }

    public function insertarTPasantiaAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarTPasantia($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarTPasantiaAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idtipopractica = $this->_request->getPost('idtipopractica');
        $this->model->modificarTPasantia($idtipopractica,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarTPasantiaAction() {
        $idtipopractica = $this->_request->getPost('idtipopractica');
        $this->model->eliminarTPasantia($idtipopractica);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>