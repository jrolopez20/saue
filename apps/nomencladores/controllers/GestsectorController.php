<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestsectorController extends ZendExt_Controller_Secure {

    private $model;

    public function init() {
        parent::init();
        $this->model = new DatSectorDelaciudadModel();
    }

    public function gestsectorAction() {
        $this->render();
    }

    public function cargarSectorAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $sectores = $this->model->cargarSectores($limit, $start);
        $cant = $this->model->countSectores();
        $result = array('cantidad' => $cant, 'datos' => $sectores);
//        echo ('{"datos":[{"idSector":"1","descripcion":"Casado"},{"idSector":"2","descripcion":"Soltero"},{"idSector":"3","descripcion":"Viudo"}]}');
        echo json_encode($result);
    }

    public function insertarSectorAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $fecha = date('Y-m-d H:i');
        $this->model->insertarSector($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarSectorAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $idSector = $this->_request->getPost('idsectorciudad');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->modificarSector($idSector, $fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarSectorAction() {
        $idSector = $this->_request->getPost('idSector');
        $this->model->eliminarSector($idSector);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>