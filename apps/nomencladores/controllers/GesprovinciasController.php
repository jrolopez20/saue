<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GesprovinciasController extends ZendExt_Controller_Secure {

    private $model;

    public function init() {
        parent::init();
        $this->model = new DatProvinciaModel();
    }

    public function gesprovinciasAction() {
        $this->render();
    }

    public function cargarProvinciaAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $provincias = $this->model->cargarProvincias($limit, $start);
        $cant = $this->model->countProvincias();
        $result = array('cantidad' => $cant, 'datos' => $provincias);
//        echo ('{"datos":[{"idprovincia":"1","descripcion":"Casado"},{"idprovincia":"2","descripcion":"Soltero"},{"idprovincia":"3","descripcion":"Viudo"}]}');
        echo json_encode($result);
    }

    public function insertarProvinciaAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarProvincia($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarProvinciaAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $idprovincia=$this->_request->getPost('idprovincia');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->modificarProvincia($idprovincia, $fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarProvinciaAction() {
        $idprovincia = $this->_request->getPost('idprovincia');
        $this->model->eliminarProvincia($idprovincia);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>