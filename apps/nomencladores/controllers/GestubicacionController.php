<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestubicacionController extends ZendExt_Controller_Secure
{
    private $model;

    public function init() {
        parent::init();
        $this->model = new DatUbicacionModel();
    }


    public function gestubicacionAction()
    {
        $this->render();
    }

public function cargarUbicacionAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $ubicacions = $this->model->cargarUbicacion($limit, $start);
        $cant = $this->model->countUbicacion();
        $result = array('cantidad' => $cant, 'datos' => $ubicacions);
        echo json_encode($result);
    }

    public function insertarUbicacionAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarUbicacion($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarUbicacionAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idubicacion = $this->_request->getPost('idubicacion');
        $this->model->modificarUbicacion($idubicacion,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarUbicacionAction() {
        $idubicacion = $this->_request->getPost('idubicacion');
        $this->model->eliminarUbicacion($idubicacion);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }
}

?>