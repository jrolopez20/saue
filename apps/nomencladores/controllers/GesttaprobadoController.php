<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GesttaprobadoController extends ZendExt_Controller_Secure
{
    private $model;

    public function init() {
        parent::init();
        $this->model = new DatTipoaprobadoModel();
    }


    public function gesttaprobadoAction()
    {
        $this->render();
    }

public function cargarTipoaprobadoAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $tipoaprobados = $this->model->cargarTipoaprobado($limit, $start);
        $cant = $this->model->countTipoaprobado();
        $result = array('cantidad' => $cant, 'datos' => $tipoaprobados);
        echo json_encode($result);
    }

    public function insertarTipoaprobadoAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarTipoaprobado($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarTipoaprobadoAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idtipoaprobado = $this->_request->getPost('idtipoaprobado');
        $this->model->modificarTipoaprobado($idtipoaprobado,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarTipoaprobadoAction() {
        $idtipoaprobado = $this->_request->getPost('idtipoaprobado');
        $this->model->eliminarTipoaprobado($idtipoaprobado);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }
}

?>