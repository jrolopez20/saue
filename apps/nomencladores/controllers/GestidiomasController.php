<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestidiomasController extends ZendExt_Controller_Secure
{

 	private $model;

    public function init() {
        parent::init();
        $this->model = new DatIdiomaModel();
    }
    public function gestidiomasAction()
    {
        $this->render();
    }
   public function cargarIdiomaAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $idiomas = $this->model->cargarIdiomas($limit, $start);
        $cant = $this->model->countIdiomas();
        $result = array('cantidad' => $cant, 'datos' => $idiomas);
//        echo ('{"datos":[{"ididioma":"1","descripcion":"Casado"},{"ididioma":"2","descripcion":"Soltero"},{"ididioma":"3","descripcion":"Viudo"}]}');
        echo json_encode($result);
    }

    public function insertarIdiomaAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $niveles = $this->_request->getPost('nivel');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarIdioma($fecha, $usuario, $descripcion, $niveles, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarIdiomaAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $niveles = $this->_request->getPost('nivel');
        $ididioma=$this->_request->getPost('ididioma');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->modificarIdioma($ididioma, $fecha, $usuario, $descripcion, $niveles, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarIdiomaAction() {
        $ididioma = $this->_request->getPost('ididioma');
        $this->model->eliminarIdioma($ididioma);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>