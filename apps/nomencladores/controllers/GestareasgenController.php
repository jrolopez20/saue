<?php

class GestareasgenController extends ZendExt_Controller_Secure
{
    private $model;

    public function init() {
        parent::init();
        $this->model = new DatAreasgenModel();
    }


    public function gestareasgenAction()
    {
        $this->render();
    }

public function cargarAreasgenAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $areasgens = $this->model->cargarAreasgen($limit, $start);
        $cant = $this->model->countAreasgen();
        $result = array('cantidad' => $cant, 'datos' => $areasgens);
        echo json_encode($result);
    }

    public function insertarAreasgenAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarAreasgen($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarAreasgenAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idareasgen = $this->_request->getPost('idareasgen');
        $this->model->modificarAreasgen($idareasgen,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarAreasgenAction() {
        $idareasgen = $this->_request->getPost('idareasgen');
        $this->model->eliminarAreasgen($idareasgen);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }
}

?>