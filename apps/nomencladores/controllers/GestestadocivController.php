<?php

/**
 * Componente para gestinar los sistemas.
 * 
 * @package SAUXE_v2.3
 * @Copyright UCI
 * @Author Esta clase fue generada automáticamente
 * @Version 3.0-0
 */
class GestestadocivController extends ZendExt_Controller_Secure {

    private $model;

    public function init() {
        parent::init();
        $this->model = new DatEstadocivilModel();
    }

    public function gestestadocivAction() {
        $this->render();
    }

    public function cargarEstadoCivilAction() {
        $limit = $this->_request->getPost('limit');
        $start = $this->_request->getPost('start');
        $estadosCiviles = $this->model->cargarEstadocivil($limit, $start);
        $cant = $this->model->countEstadocivil();
        $result = array('cantidad' => $cant, 'datos' => $estadosCiviles);
//        echo ('{"datos":[{"idestadocivil":"1","descripcion":"Casado"},{"idestadocivil":"2","descripcion":"Soltero"},{"idestadocivil":"3","descripcion":"Viudo"}]}');
        echo json_encode($result);
    }

    public function insertarEstadoCivilAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $this->model->insertarEstadocivil($fecha, $usuario, $descripcion, $estado);
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfAdicionar}");
    }

    public function modificarEstadoCivilAction() {
        $usuario = $this->global->Perfil->idusuario;
        $descripcion = $this->_request->getPost('descripcion');
        $fecha = date('Y-m-d H:i');
        $estado = $this->_request->getPost('estado');
        $fecha = date('Y-m-d H:i');
        if (!$estado) {
            $estado = false;
        } else {
            $estado = true;
        }
        $idestadocivil = $this->_request->getPost('idestadocivil');
        $this->model->modificarEstadocivil($idestadocivil,$fecha, $usuario, $descripcion, $estado);
        //devolver los datos, hay que agregar la etiqueta
        echo("{'codMsg':1,'mensaje':perfil.etiquetas.lbMsgInfModificar}");
    }

    public function eliminarEstadoCivilAction() {
        $idestadocivil = $this->_request->getPost('idestadocivil');
        $this->model->eliminarEstadocivil($idestadocivil);
        echo"{'codMsg':1,'mensaje': perfil.etiquetas.MsgInfEliminar}";
    }

}

?>