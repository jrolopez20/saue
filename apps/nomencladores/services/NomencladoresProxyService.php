<?php


class NomencladoresProxyService
{
    /**
     * loadCanton
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadCanton($limit, $start)
    {
        $canton = new DatCantonModel();
        return $canton->cargarCantonService($limit, $start);
    } 
    /**
     * loadTipoaprobado
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadTipoaprobado($limit, $start)
    {
        $tipoaprobado = new DatTipoaprobadoModel();
        return $tipoaprobado->cargarTipoaprobadoService($limit, $start);
    } 

    /**
     * countTipoaprobado
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countTipoaprobados()
    {
        $Tipoaprobado = new DatTipoaprobadoModel();
        return $Tipoaprobado->countTipoaprobadoService();
    }
    /**
     * loadAreasgen
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadAreasgen($limit, $start)
    {
        $areasgen = new DatAreasgenModel();
        return $areasgen->cargarAreasgenService($limit, $start);
    }  

    /**
     * countAreasgen
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countAreasgen()
    {
        $Areasgen = new DatAreasgenModel();
        return $Areasgen->countAreasgenService();
    }
    /**
     * loadUbicacion
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadUbicacion($limit, $start)
    {
        $ubicacion = new DatUbicacionModel();
        return $ubicacion->cargarUbicacionService($limit, $start);
    }
    /**
     * countUbicacion
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countUbicacion()
    {
        $Ubicacion = new DatUbicacionModel();
        return $Ubicacion->countUbicacionService();
    }
    /**
     * loadColegios
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadColegios($limit, $start)
    {
        $colegios = new DatColegiosModel();
        return $colegios->cargarColegiosService($limit, $start);
    }

    /**
     * loadEstadosCivil
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadEstadosCivil($limit, $start)
    {
        $estadocivil = new DatEstadocivilModel();
        return $estadocivil->cargarEstadocivil($limit, $start);
    }

    /**
     * loadProvincias
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadProvincias($limit, $start)
    {
        $provincias = new DatProvinciaModel();
        return $provincias->cargarProvinciasService($limit, $start);
    }

    /**
     * loadSectoresCiudad
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadSectoresCiudad($limit, $start)
    {
        $sectciudad = new DatSectorDelaciudadModel();
        return $sectciudad->cargarSectores($limit, $start);

    }

    /**
     * loadUniversidades
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadUniversidades($limit, $start)
    {
        $universidad = new DatUniversidadModel();
        return $universidad->cargarUniversidadesService($limit, $start);
    }

    /**
     * loadIdiomas
     * Brinda el servicio de carga de registros de base de datos
     *
     * @param int $limit - Cantidad de registros a cargar
     * @param int $start - Inicio de la los registros a cargar
     * @return array - Registros .
     */
    public function loadIdiomas($limit, $start)
    {
        $idioma = new DatIdiomaModel();
        return $idioma->cargarIdiomas($limit, $start);
    }

    /**
     * countCanton
     * Brinda el servicio de carga de registros de base de datos
     * @return numero - Registros .
     */
    public function countCanton()
    {
        $canton = new DatCantonModel();
        return $canton->countCantonService();
    }

    /**
     * countColegios
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countColegios()
    {
        $colegios = new DatColegiosModel();
        return $colegios->countColegiosService();
    }

    /**
     * countEstadosCivil
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countEstadosCivil()
    {
        $estadocivil = new DatEstadocivilModel();
        return $estadocivil->countEstadocivil();
    }

    /**
     * countProvincias
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countProvincias()
    {
        $provincias = new DatProvinciaModel();
        return $provincias->countProvinciasService();
    }

    /**
     * countSectoresCiudad
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countSectoresCiudad()
    {
        $sectciudad = new DatSectorDelaciudadModel();

        return $sectciudad->countSectores();
    }

    /**
     * countUniversidades
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countUniversidades()
    {
        $universidad = new DatUniversidadModel();
        return $universidad->countUniversidadesService();
    }

    /**
     * countIdiomas
     * Brinda el servicio de carga de registros de base de datos
     *
     * @return numero - Registros .
     */
    public function countIdiomas()
    {
        $idioma = new DatIdiomaModel();
        return $idioma->countIdiomas();
    }
} 
