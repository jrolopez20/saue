<?php

class DatColegiosModel extends ZendExt_Model
{

    public function DatColegioModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarColegios($limit, $start)
    {
        try {
            return DatColegios::cargarColegios($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countColegio()
    {

        try {
            return DatColegios::countColegios();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
public function cargarColegiosService($limit, $start)
    {
        try {
            return DatColegios::cargarColegiosService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countColegiosService()
    {

        try {
            return DatColegios::countColegiosService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarColegio( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $colegio = new DatColegios();
            $colegio->fecha = $fecha;
            $colegio->idusuario = $idusuario;
            $colegio->descripcion = $descripcion;
            $colegio->estado = $estado;
            $colegio->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarColegio($idcolegio,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $colegio = Doctrine::getTable("DatColegios")->find($idcolegio);
            $colegio->fecha = $fecha;
            $colegio->idusuario = $idusuario;
            $colegio->descripcion = $descripcion;
            $colegio->estado = $estado;
            $colegio->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarColegio($idcolegio)
    {

        try {
            return DatColegios::eliminarColegio($idcolegio);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

