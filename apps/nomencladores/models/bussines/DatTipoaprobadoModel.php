<?php

class DatTipoaprobadoModel extends ZendExt_Model
{

    public function DatTipoaprobadoModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarTipoaprobado($limit, $start)
    {
        try {
            return DatTipoaprobado::cargarTipoaprobado($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countTipoaprobado()
    {

        try {
            return DatTipoaprobado::countTipoaprobado();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
 public function cargarTipoaprobadoService($limit, $start)
    {
        try {
            return DatTipoaprobado::cargarTipoaprobadoService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countTipoaprobadoService()
    {

        try {
            return DatTipoaprobado::countTipoaprobadoService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarTipoaprobado( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $tipoaprobado = new DatTipoaprobado();
            $tipoaprobado->fecha = $fecha;
            $tipoaprobado->idusuario = $idusuario;
            $tipoaprobado->descripcion = $descripcion;
            $tipoaprobado->estado = $estado;
            $tipoaprobado->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTipoaprobado($idtipoaprobado,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $tipoaprobado = Doctrine::getTable("DatTipoaprobado")->find($idtipoaprobado);
            $tipoaprobado->fecha = $fecha;
            $tipoaprobado->idusuario = $idusuario;
            $tipoaprobado->descripcion = $descripcion;
            $tipoaprobado->estado = $estado;
            $tipoaprobado->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarTipoaprobado($idtipoaprobado)
    {

        try {
            return DatTipoaprobado::eliminarTipoaprobado($idtipoaprobado);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

