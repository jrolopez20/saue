<?php

class DatUbicacionModel extends ZendExt_Model
{

    public function DatUbicacionModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarUbicacion($limit, $start)
    {
        try {
            return DatUbicacion::cargarUbicacion($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countUbicacion()
    {

        try {
            return DatUbicacion::countUbicacion();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
 public function cargarUbicacionService($limit, $start)
    {
        try {
            return DatUbicacion::cargarUbicacionService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countUbicacionService()
    {

        try {
            return DatUbicacion::countUbicacionService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarUbicacion( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $ubicacion = new DatUbicacion();
            $ubicacion->fecha = $fecha;
            $ubicacion->idusuario = $idusuario;
            $ubicacion->descripcion = $descripcion;
            $ubicacion->estado = $estado;
            $ubicacion->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarUbicacion($idubicacion,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $ubicacion = Doctrine::getTable("DatUbicacion")->find($idubicacion);
            $ubicacion->fecha = $fecha;
            $ubicacion->idusuario = $idusuario;
            $ubicacion->descripcion = $descripcion;
            $ubicacion->estado = $estado;
            $ubicacion->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarUbicacion($idubicacion)
    {

        try {
            return DatUbicacion::eliminarUbicacion($idubicacion);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

