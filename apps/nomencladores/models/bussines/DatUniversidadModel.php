<?php

class DatUniversidadModel extends ZendExt_Model
{

    public function DatUniversidadModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarUniversidades($limit, $start)
    {
        try {
            return DatUniversidad::cargarUniversidades($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countUniversidades()
    {

        try {
            return DatUniversidad::countUniversidades();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
public function cargarUniversidadesService($limit, $start)
    {
        try {
            return DatUniversidad::cargarUniversidadesService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countUniversidadesService()
    {

        try {
            return DatUniversidad::countUniversidadesService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarUniversidad( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $universidad = new DatUniversidad();
            $universidad->fecha = $fecha;
            $universidad->idusuario = $idusuario;
            $universidad->descripcion = $descripcion;
            $universidad->estado = $estado;
            $universidad->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarUniversidad($iduniversidad,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $universidad = Doctrine::getTable("DatUniversidad")->find($iduniversidad);
            $universidad->fecha = $fecha;
            $universidad->idusuario = $idusuario;
            $universidad->descripcion = $descripcion;
            $universidad->estado = $estado;
            $universidad->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarUniversidad($iduniversidad)
    {

        try {
            return DatUniversidad::eliminarUniversidad($iduniversidad);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }


    }

}
