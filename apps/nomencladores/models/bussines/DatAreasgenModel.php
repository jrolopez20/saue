<?php

class DatAreasgenModel extends ZendExt_Model
{

    public function DatAreasgenModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarAreasgen($limit, $start)
    {
        try {
            return DatAreasgen::cargarAreasgen($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countAreasgen()
    {

        try {
            return DatAreasgen::countAreasgen();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
 public function cargarAreasgenService($limit, $start)
    {
        try {
            return DatAreasgen::cargarAreasgenService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countAreasgenService()
    {

        try {
            return DatAreasgen::countAreasgenService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarAreasgen( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $areageneral = new DatAreasgen();
            $areageneral->fecha = $fecha;
            $areageneral->idusuario = $idusuario;
            $areageneral->descripcion = $descripcion;
            $areageneral->estado = $estado;
            $areageneral->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarAreasgen($idareageneral,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $areageneral = Doctrine::getTable("DatAreasgen")->find($idareageneral);
            $areageneral->fecha = $fecha;
            $areageneral->idusuario = $idusuario;
            $areageneral->descripcion = $descripcion;
            $areageneral->estado = $estado;
            $areageneral->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarAreasgen($idareageneral)
    {

        try {
            return DatAreasgen::eliminarAreasgen($idareageneral);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

