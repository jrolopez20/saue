<?php

class DatTipoPracticaModel extends ZendExt_Model
{
 public function DatTipoPracticaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarTPasantias($limit, $start)
    {
        try {
            return DatTipoPractica::cargarTPasantias($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countTPasantias()
    {

        try {
            return DatTipoPractica::countTPasantias();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
public function cargarTPasantiasService($limit, $start)
    {
        try {
            return DatTipoPractica::cargarTPasantiasService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countTPasantiasService()
    {

        try {
            return DatTipoPractica::countTPasantiasService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarTPasantia( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $universidad = new DatTipoPractica();
            $universidad->fecha = $fecha;
            $universidad->idusuario = $idusuario;
            $universidad->descripcion = $descripcion;
            $universidad->estado = $estado;
            $universidad->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarTPasantia($idtipopractica,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $universidad = Doctrine::getTable("DatTipoPractica")->find($idtipopractica);
            $universidad->fecha = $fecha;
            $universidad->idusuario = $idusuario;
            $universidad->descripcion = $descripcion;
            $universidad->estado = $estado;
            $universidad->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarTPasantia($idtipopractica)
    {

        try {
            return DatTipoPractica::eliminarTPasantia($idtipopractica);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }


    }

}

