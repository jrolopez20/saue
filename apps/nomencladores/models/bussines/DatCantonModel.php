<?php

class DatCantonModel extends ZendExt_Model
{

    public function DatCantonModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarCanton($limit, $start)
    {
        try {
            return DatCanton::cargarCanton($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countCanton()
    {

        try {
            return DatCanton::countCanton();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
 public function cargarCantonService($limit, $start)
    {
        try {
            return DatCanton::cargarCantonService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countCantonService()
    {

        try {
            return DatCanton::countCantonService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarCanton( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $canton = new DatCanton();
            $canton->fecha = $fecha;
            $canton->idusuario = $idusuario;
            $canton->descripcion = $descripcion;
            $canton->estado = $estado;
            $canton->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarCanton($idcanton,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $canton = Doctrine::getTable("DatCanton")->find($idcanton);
            $canton->fecha = $fecha;
            $canton->idusuario = $idusuario;
            $canton->descripcion = $descripcion;
            $canton->estado = $estado;
            $canton->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarCanton($idcanton)
    {

        try {
            return DatCanton::eliminarCanton($idcanton);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
}

