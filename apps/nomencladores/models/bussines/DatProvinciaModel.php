<?php

class DatProvinciaModel extends ZendExt_Model
{

    public function DatProvinciaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarProvincias($limit, $start)
    {
        try {
            return DatProvincia::cargarProvincias($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countProvincias()
    {

        try {
            return DatProvincia::countProvincias();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function cargarProvinciasService($limit, $start)
    {
        try {
            return DatProvincia::cargarProvinciasService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countProvinciasService()
    {

        try {
            return DatProvincia::countProvinciasService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
    public function insertarProvincia( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $provincia = new DatProvincia();
            $provincia->fecha = $fecha;
            $provincia->idusuario = $idusuario;
            $provincia->descripcion = $descripcion;
            $provincia->estado = $estado;
            $provincia->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarProvincia($idprovincia,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $provincia = Doctrine::getTable("DatProvincia")->find($idprovincia);
            $provincia->fecha = $fecha;
            $provincia->idusuario = $idusuario;
            $provincia->descripcion = $descripcion;
            $provincia->estado = $estado;
            $provincia->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarProvincia($idprovincia)
    {

        try {
            return DatProvincia::eliminarProvincia($idprovincia);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }


}

}