<?php

class DatSectorDelaciudadModel extends ZendExt_Model
{

    public function DatSectorDelaciudadModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarSectores($limit, $start)
    {
        try {
            return DatSectorDelaciudad::cargarSectoresDelaciudad($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countSectores()
    {

        try {
            return DatSectorDelaciudad::countSectoresDelaciudad();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }
 public function cargarSectorDelaciudadService($limit, $start)
    {
        try {
            return DatSectorDelaciudad::cargarSectoresDelaciudadService($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countSectoresService()
    {

        try {
            return DatSectorDelaciudad::countSectoresDelaciudadService();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarSector($fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $sectciudad = new DatSectorDelaciudad();
            $sectciudad->fecha = $fecha;
            $sectciudad->idusuario = $idusuario;
            $sectciudad->descripcion = $descripcion;
            $sectciudad->estado = $estado;
            $sectciudad->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarSector($idsectorciudad,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $sectciudad = Doctrine::getTable("DatSectorDelaciudad")->find($idsectorciudad);
            $sectciudad->fecha = $fecha;
            $sectciudad->idusuario = $idusuario;
            $sectciudad->descripcion = $descripcion;
            $sectciudad->estado = $estado;
            $sectciudad->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarSector($idsectorciudad)
    {

        try {
            return DatSectorDelaciudad::eliminarSector($idsectorciudad);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }


    }

}
