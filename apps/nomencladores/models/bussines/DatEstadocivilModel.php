<?php

class DatEstadocivilModel extends ZendExt_Model
{
    public function DatEstadocivilModel()
    {
        parent::ZendExt_Model();
    }
    
    public function cargarEstadocivil($limit, $start)
    {
        try {
            return DatEstadocivil::cargarEstadocivil($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countEstadocivil()
    {

        try {
            return DatEstadocivil::countEstadoCivil();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarEstadocivil( $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $estadocivil = new DatEstadocivil();
            $estadocivil->fecha = $fecha;
            $estadocivil->idusuario = $idusuario;
            $estadocivil->descripcion = $descripcion;
            $estadocivil->estado = $estado;
            $estadocivil->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarEstadocivil($idestadocivil,  $fecha, $idusuario, $descripcion,$estado)
    {

        try {
            $estadocivil = Doctrine::getTable("DatEstadocivil")->find($idestadocivil);
            $estadocivil->fecha = $fecha;
            $estadocivil->idusuario = $idusuario;
            $estadocivil->descripcion = $descripcion;
            $estadocivil->estado = $estado;
            $estadocivil->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarEstadocivil($idestadocivil)
    {
        try {
            return DatEstadocivil::eliminarEstadoCivil($idestadocivil);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

}

