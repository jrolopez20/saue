<?php

class DatIdiomaModel extends ZendExt_Model
{

   public function DatIdiomaModel()
    {
        parent::ZendExt_Model();
    }

    public function cargarIdiomas($limit, $start)
    {
        try {
            return DatIdioma::cargarIdiomas($limit, $start);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }

    }

    public function countIdiomas()
    {

        try {
            return DatIdioma::countIdiomas();
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function insertarIdioma( $fecha, $idusuario, $descripcion, $nivel, $estado)
    {

        try {
            $idioma = new DatIdioma();
            $idioma->fecha = $fecha;
            $idioma->idusuario = $idusuario;
            $idioma->descripcion = $descripcion;
            $idioma->estado = $estado;
            $idioma->nivel = $nivel;
            $idioma->save();

            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function modificarIdioma($ididioma,  $fecha, $idusuario, $descripcion, $nivel, $estado)
    {

        try {
            $idioma = Doctrine::getTable("DatIdioma")->find($ididioma);
            $idioma->fecha = $fecha;
            $idioma->idusuario = $idusuario;
            $idioma->descripcion = $descripcion;
            $idioma->estado = $estado;
            $idioma->nivel = $nivel;
            $idioma->save();
            return true;
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
    }

    public function eliminarIdioma($ididioma)
    {

        try {
            return DatIdioma::eliminarIdioma($ididioma);
        } catch (Doctrine_Exception $e) {
            if (DEBUG_SAUE)
                echo(__FILE__ . ' ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }


}
}

