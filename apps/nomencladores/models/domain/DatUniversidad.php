<?php

class DatUniversidad extends BaseDatUniversidad
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'iduniversidad', 'foreign' => 'iduniversidad'));
    }


    static public function cargarUniversidades($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUniversidad u")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countUniversidades()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUniversidad u")
            ->execute();
        return $result->count();

    }

 static public function cargarUniversidadesService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUniversidad u")
            ->where("u.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countUniversidadesService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUniversidad u")
            ->where("u.estado=?",true)
            ->execute();
        return $result->count();

    }

    static public function eliminarUniversidad($iduniversidad)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatUniversidad u")
            ->where("u.iduniversidad=?", $iduniversidad)
            ->execute();
        return true;
    }



}

