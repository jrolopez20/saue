<?php

abstract class BaseDatTipoaprobado extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_tipoaprobado');
        $this->hasColumn('estado', 'boolean', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('fecha', 'date', null, array ('notnull' => false,'primary' => false, 'default' => ''));
        $this->hasColumn('idusuario', 'numeric', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array ('notnull' => false,'primary' => false));
        $this->hasColumn('idtipoaprobado', 'numeric', null, array ('notnull' => false,'primary' => true, 'sequence' => 'mod_saue.seq_dat_tipoaprobado'));
    }


}

