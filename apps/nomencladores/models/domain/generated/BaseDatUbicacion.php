<?php

abstract class BaseDatUbicacion extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_ubicacion');
        $this->hasColumn('fecha', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idubicacion', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_saue.seq_dat_ubicacion'));
    }


}

