<?php

abstract class BaseDatAreasgen extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.nom_area_general');
        $this->hasColumn('fecha', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idareageneral', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_saue.seq_nom_area_gen'));
    }


}

