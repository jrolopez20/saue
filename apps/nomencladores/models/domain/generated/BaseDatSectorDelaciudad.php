<?php

abstract class BaseDatSectorDelaciudad extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('mod_saue.dat_sector_delaciudad');
        $this->hasColumn('fecha', 'date', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idusuario', 'numeric', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('estado', 'boolean', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('descripcion', 'text', null, array('notnull' => false, 'primary' => false));
        $this->hasColumn('idsectorciudad', 'numeric', null, array('notnull' => false, 'primary' => true, 'sequence' => 'mod_saue.seq_dat_sector_delaciudad'));
    }


}

