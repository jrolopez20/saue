<?php

class DatTipoPractica extends BaseDatTipoPractica
{

   public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatPractica', array('local' => 'idtipopractica', 'foreign' => 'idtipopractica'));
    }


    static public function cargarTPasantias($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoPractica u")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countTPasantias()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoPractica u")
            ->execute();
        return $result->count();

    }

 static public function cargarTPasantiasService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoPractica u")
            ->where("u.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countTPasantiasService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoPractica u")
            ->where("u.estado=?",true)
            ->execute();
        return $result->count();

    }

    static public function eliminarTPasantia($idtipopractica)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatTipoPractica u")
            ->where("u.idtipopractica=?", $idtipopractica)
            ->execute();
        return true;
    }



}

