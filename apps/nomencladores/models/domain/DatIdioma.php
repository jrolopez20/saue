<?php

class DatIdioma extends BaseDatIdioma
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasMany ('DatMateria', array ('local' => 'ididioma', 'foreign' => 'ididioma'));
       $this->hasMany ('DatAlumnoIdioma', array ('local' => 'ididioma', 'foreign' => 'ididioma'));
    }

 static public function cargarIdiomas($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatIdioma i")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countIdiomas()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatIdioma i")
            ->execute();
        return $result->count();

    }

    static public function eliminarIdioma($ididioma)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatIdioma i")
            ->where("i.ididioma=?", $ididioma)
            ->execute();
        return true;
    }
}

