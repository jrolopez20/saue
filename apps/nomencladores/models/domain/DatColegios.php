<?php

class DatColegios extends BaseDatColegios
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idcolegio', 'foreign' => 'idcolegio'));
    }
    static public function cargarColegios($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatColegios c")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countColegios()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatColegios c")
            ->execute();
        return $result->count();

    }
 static public function cargarColegiosService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatColegios c")
            ->where("c.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countColegiosService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatColegios c")
            ->where("c.estado=?",true)
            ->execute();
        return $result->count();

    }

    static public function eliminarColegio($idcolegio)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatColegios c")
            ->where("c.idcolegio=?", $idcolegio)
            ->execute();
        return true;
    }

}

