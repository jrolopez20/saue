<?php

class DatAreasgen extends BaseDatAreasgen
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idareageneral', 'foreign' => 'idareageneral'));
    }

    static public function cargarAreasgen($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAreasgen c")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countAreasgenService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAreasgen c")
            ->where("c.estado=?",true)
            ->execute();
        return $result->count();

    }
 static public function cargarAreasgenService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAreasgen c")            
            ->where("c.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countAreasgen()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatAreasgen c")
            ->execute();
        return $result->count();

    }

    static public function eliminarAreasgen($idareageneral)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatAreasgen c")
            ->where("c.idareageneral=?", $idareageneral)
            ->execute();
        return true;
    }

}



