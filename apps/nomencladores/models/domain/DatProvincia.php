<?php

class DatProvincia extends BaseDatProvincia
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idprovincia', 'foreign' => 'idprovincia'));
    }

    static public function cargarProvincias($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatProvincia p")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countProvincias()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatProvincia p")
            ->execute();
        return $result->count();

    }
    
     static public function cargarProvinciasService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatProvincia p")
            ->limit($limit)
            ->where("p.estado=?",true)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countProvinciasService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatProvincia p")
            ->where("p.estado=?",true)
            ->execute();
        return $result->count();

    }

    static public function eliminarProvincia($idprovincia)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatProvincia p")
            ->where("p.idprovincia=?", $idprovincia)
            ->execute();
        return true;
    }

}

