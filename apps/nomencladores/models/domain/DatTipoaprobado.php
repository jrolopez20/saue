<?php

class DatTipoaprobado extends BaseDatTipoaprobado
{

    public function setUp()
    {
        parent :: setUp ();
        $this->hasMany ('DatAlumnoMateDeta', array ('local' => 'idtipoaprobado', 'foreign' => 'idtipoaprobado'));
    }

    static public function cargarTipoaprobado($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoaprobado c")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countTipoaprobadoService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoaprobado c")
            ->where("c.estado=?",true)
            ->execute();
        return $result->count();

    }
 static public function cargarTipoaprobadoService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoaprobado c")            
            ->where("c.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countTipoaprobado()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatTipoaprobado c")
            ->execute();
        return $result->count();

    }

    static public function eliminarTipoaprobado($idtipoaprobado)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatTipoaprobado c")
            ->where("c.idtipoaprobado=?", $idtipoaprobado)
            ->execute();
        return true;
    }

}



