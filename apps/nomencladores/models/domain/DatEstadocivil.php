<?php

class DatEstadocivil extends BaseDatEstadocivil
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idestadocivil', 'foreign' => 'idestadocivil'));
        $this->hasMany('DatProfesor', array('local' => 'idestadocivil', 'foreign' => 'idestadocivil'));
    }
    static public function cargarEstadocivil($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatEstadocivil ec")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countEstadocivil()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatEstadocivil ec")
            ->execute();
        return $result->count();

    }
static public function cargarEstadocivilService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatEstadocivil ec")
            ->where("ec.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countEstadocivilService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatEstadocivil ec")
            ->where("ec.estado=?",true)
            ->execute();
        return $result->count();

    }

    static public function eliminarEstadoCivil($idestadocivil)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatEstadocivil ec")
            ->where("ec.idestadocivil=?", $idestadocivil)
            ->execute();
        return true;
    }

}

