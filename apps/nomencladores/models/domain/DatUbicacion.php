<?php

class DatUbicacion extends BaseDatUbicacion
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idubicacion', 'foreign' => 'idubicacion'));
    }

    static public function cargarUbicacion($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUbicacion c")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countUbicacionService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUbicacion c")
            ->where("c.estado=?",true)
            ->execute();
        return $result->count();

    }
 static public function cargarUbicacionService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUbicacion c")            
            ->where("c.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countUbicacion()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatUbicacion c")
            ->execute();
        return $result->count();

    }

    static public function eliminarUbicacion($idubicacion)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatUbicacion c")
            ->where("c.idubicacion=?", $idubicacion)
            ->execute();
        return true;
    }

}



