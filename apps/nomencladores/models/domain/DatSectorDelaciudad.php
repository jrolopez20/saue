<?php

class DatSectorDelaciudad extends BaseDatSectorDelaciudad
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idsectorciudad', 'foreign' => 'idsectorciudad'));
    }

    static public function cargarSectoresDelaciudad($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatSectorDelaciudad s")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countSectoresDelaciudad()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatSectorDelaciudad s")
            ->execute();
        return $result->count();

    }
    
     static public function cargarSectoresDelaciudadService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatSectorDelaciudad s")
            ->where("s.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countSectoresDelaciudadService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatSectorDelaciudad s")
            ->where("s.estado=?",true)
            ->execute();
        return $result->count();

    }

    static public function eliminarSector($idsectorciudad)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatSectorDelaciudad s")
            ->execute();
        return true;
    }

}
