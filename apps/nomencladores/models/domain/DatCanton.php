<?php

class DatCanton extends BaseDatCanton
{

    public function setUp()
    {
        parent :: setUp();
        $this->hasMany('DatEstudiante', array('local' => 'idcanton', 'foreign' => 'idcanton'));
    }

    static public function cargarCanton($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatCanton c")
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countCantonService()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatCanton c")
            ->where("c.estado=?",true)
            ->execute();
        return $result->count();

    }
 static public function cargarCantonService($limit, $start)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatCanton c")            
            ->where("c.estado=?",true)
            ->limit($limit)
            ->offset($start)
            ->execute();
        return $result->toArray();

    }

    static public function countCanton()
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->from("DatCanton c")
            ->execute();
        return $result->count();

    }

    static public function eliminarCanton($idcanton)
    {
        $query = Doctrine_Query::create();
        $result = $query
            ->delete()
            ->from("DatCanton c")
            ->where("c.idcanton=?", $idcanton)
            ->execute();
        return true;
    }

}



