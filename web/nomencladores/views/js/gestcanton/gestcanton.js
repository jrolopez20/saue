var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestcanton', function() {
    cargarInterfaz();
});
Ext.QuickTips.init();
function cargarInterfaz() {
    var btnAdicionargestcanton = Ext.create('Ext.Button', {
        id: 'btnAgrgestcanton',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormgestcanton('add');
        }
    });
    var btnModificargestcanton = Ext.create('Ext.Button', {
        id: 'btnModgestcanton',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormgestcanton('mod');
        }
    });
    var btnEliminargestcanton = Ext.create('Ext.Button', {
        id: 'btnEligestcanton',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function() {
            Ext.MessageBox.show({
                title: 'Eliminar',
                msg: 'Desea eliminar el elemento?',
                buttons: Ext.MessageBox.YESNO,
                icon: Ext.MessageBox.QUESTION,
                fn: eliminarCanton
            });
        }
    });
    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);
    var winAdicionargestcanton;
    var winModificargestcanton;
    var formgestcanton = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 350,
        fieldDefaults: {
            msgTarget: 'side',
            labelWidth: 75
        },
        defaults: {
            anchor: '100%'
        },
        items: [{xtype: 'textfield',
                fieldLabel: 'Cantón:',
                name: 'descripcion',
                anchor: '100%',
                allowBlank: false
            }, {xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                checked: true
            },
            {xtype: 'hidden',
                name: 'idcanton'
            }]
    });
    function mostFormgestcanton(opcion) {
        switch (opcion) {
            case 'add':
                {
                    winAdicionargestcanton = Ext.create('Ext.Window', {
                        title: 'Adicionar cantón',
                        closeAction: 'hide',
                        width: 300,
                        height: 150,
                        x: 220,
                        y: 100,
                        constrain: true,
                        layout: 'fit',
                        buttons: [{
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function() {
                                    winAdicionargestcanton.hide();
                                }
                            }, {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                handler: function() {
                                    adicionarCanton("apl");
                                }
                            }, {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function() {
                                    adicionarCanton("aceptar");
                                }
                            }
                        ]
                    });
                    winAdicionargestcanton.add(formgestcanton);
                    winAdicionargestcanton.show();
                    winAdicionargestcanton.on('hide', function() {
                        formgestcanton.getForm().reset();
                    }, this);
                }
                break;
            case 'mod':
                {
                    winModificargestcanton = Ext.create('Ext.Window', {
                        title: 'Modificar cantón',
                        closeAction: 'hide',
                        width: 300,
                        height: 150,
                        x: 220,
                        y: 100,
                        constrain: true,
                        layout: 'fit',
                        buttons: [{
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function() {
                                    winModificargestcanton.hide();
                                }
                            }, {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                handler: function() {
                                    modificarCanton("apl");
                                }
                            }, {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function() {
                                    modificarCanton("aceptar");
                                }
                            }
                        ]
                    });
                    winModificargestcanton.add(formgestcanton);
                    winModificargestcanton.show();
                    winModificargestcanton.on('hide', function() {
                        formgestcanton.getForm().reset();
                    }, this);
                    formgestcanton.getForm().loadRecord(sm.getLastSelected());
                }
                break;
        }
    }
    var stGpgestcanton = Ext.create('Ext.data.ArrayStore', {
        fields: [{
                name: "idcanton"
            }, {
                name: 'descripcion'
            }, {
                name: 'estado'
            }],
        proxy: {
            type: 'ajax',
            url: 'cargarCanton',
            reader: {
                type: 'json',
                id: 'idcanton',
                root: 'datos'
            }
        }
    });
             var search = Ext.create ('Ext.form.field.Trigger', {
                store:stGpgestcanton,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                width: 400,
                fieldLabel: perfil.etiquetas.lbBtnBuscar,
                labelWidth: 40,
                onTrigger1Click: function () {
                    var me = this;

                    if (me.hasSearch) {
                        me.setValue('');
                        me.store.clearFilter();
                        me.hasSearch = false;
                        me.triggerCell.item(0).setDisplayed(false);
                        me.updateLayout();
                    }
                },

                onTrigger2Click: function () {
                    var me = this,
                        value = me.getValue();

                    if (value != null) {
                        me.store.clearFilter();
                        me.store.filter({filterFn: function(item) { return item.get("descripcion").toLowerCase().match(me.getValue().toLowerCase());}});
                        me.hasSearch = true;
                        me.triggerCell.item(0).setDisplayed(true);
                        me.updateLayout();
                    }
            }
        });
     search.on('afterrender', function () {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                );
     search.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                f.onTrigger2Click();
            }
        });
    var Gpgestcanton = Ext.create('Ext.grid.Panel', {
        store: stGpgestcanton,
        stateId: 'stateGrid',
        columnLines: true,
        viewConfig:{
            getRowClass: function(record, rowIndex, rowParams, store){
                if (record.data.estado == false)
                    return 'FilaRoja';
            }
        },
        columns: [{
                hidden:true,
                dataIndex: 'idcanton'
            }, {hidden:true,
                dataIndex: 'estado'
            }, {
                header: 'Cantón',
                flex: 1,
                sortable: true,
                dataIndex: 'descripcion'
            }],
        region: 'center',
        tbar: [btnAdicionargestcanton, btnModificargestcanton, btnEliminargestcanton, "|", search]
    });
    var sm = Gpgestcanton.getSelectionModel();
    sm.on('selectionchange', function(sel, selectedRecord) {
        if (selectedRecord.length) {
            btnModificargestcanton.enable();
            btnEliminargestcanton.enable();
        } else {
            btnModificargestcanton.disable();
            btnEliminargestcanton.disable();
        }
    });
    stGpgestcanton.load();
    function adicionarCanton(apl) {
        //si es la opción de aplicar
        if (formgestcanton.getForm().isValid()) {
            formgestcanton.getForm().submit({
                url: 'insertarCanton',
                waitMsg: perfil.etiquetas.lbMsgRegistrando,
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        formgestcanton.getForm().reset();
                        stGpgestcanton.reload();
                        if (apl === "aceptar")
                            winAdicionargestcanton.hide();
                    }


                }
            });
        }
    }
    function modificarCanton(apl) {
        //si es la opción de aplicar
        if (formgestcanton.getForm().isValid()) {
            formgestcanton.getForm().submit({
                url: 'modificarCanton',
                waitMsg: perfil.etiquetas.lbMsgModificando,
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        stGpgestcanton.reload();
                        if (apl === "aceptar")
                            winModificargestcanton.hide();
                    }

                }
            });
        }
    }
    function eliminarCanton(buttonId) {
        if (buttonId === "yes") {
            Ext.Ajax.request({
                url: 'eliminarCanton',
                method: 'POST',
                params: {idcanton: sm.getLastSelected().data.idcanton},
                callback: function(options, success, response) {
                    responseData = Ext.decode(response.responseText);
                    if (responseData.codMsg === 1) {
                        stGpgestcanton.reload();
                        sm.deselect();
                    }
                }
            });
        }
    }
    var general = Ext.create('Ext.panel.Panel', {layout: 'border', items: [Gpgestcanton]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});
}