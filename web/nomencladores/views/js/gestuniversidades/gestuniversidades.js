var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestuniversidades', function() {
    cargarInterfaz();
});
Ext.QuickTips.init();
function cargarInterfaz() {
    var btnAdicionargestuniversidades = Ext.create('Ext.Button', {
        id: 'btnAgrgestuniversidades',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormgestuniversidades('add');
        }
    });
    var btnModificargestuniversidades = Ext.create('Ext.Button', {
        id: 'btnModgestuniversidades',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormgestuniversidades('mod');
        }
    });
    var btnEliminargestuniversidades = Ext.create('Ext.Button', {
        id: 'btnEligestuniversidades',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function() {
            Ext.MessageBox.show({
                title: 'Eliminar',
                msg: 'Desea eliminar el elemento?',
                buttons: Ext.MessageBox.YESNO,
                icon: Ext.MessageBox.QUESTION,
                fn: eliminarUniversidad
            });
        }
    });
    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);
    var winAdicionargestuniversidades;
    var winModificargestuniversidades;
    var formgestuniversidades = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 350,
        fieldDefaults: {
            msgTarget: 'side',
            labelWidth: 75
        },
        defaults: {
            anchor: '100%'
        },
        items: [{xtype: 'textfield',
                fieldLabel: 'Universidad:',
                name: 'descripcion',
                anchor: '100%',
                allowBlank: false
            }, {xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                checked: true
            },
            {xtype: 'hidden',
                name: 'iduniversidad'
            }]
    });
    function mostFormgestuniversidades(opcion) {
        switch (opcion) {
            case 'add':
                {
                    winAdicionargestuniversidades = Ext.create('Ext.Window', {
                        title: 'Adicionar universidad',
                        closeAction: 'hide',
                        width: 300,
                        height: 150,
                        x: 220,
                        y: 100,
                        constrain: true,
                        layout: 'fit',
                        buttons: [{
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function() {
                                    winAdicionargestuniversidades.hide();
                                }
                            }, {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                handler: function() {
                                    adicionarUniversidad("apl");
                                }
                            }, {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function() {
                                    adicionarUniversidad("aceptar");
                                }
                            }
                        ]
                    });
                    winAdicionargestuniversidades.add(formgestuniversidades);
                    winAdicionargestuniversidades.show();
                    winAdicionargestuniversidades.on('hide', function() {
                        formgestuniversidades.getForm().reset();
                    }, this);
                }
                break;
            case 'mod':
                {
                    winModificargestuniversidades = Ext.create('Ext.Window', {
                        title: 'Modificar universidad',
                        closeAction: 'hide',
                        width: 300,
                        height: 150,
                        x: 220,
                        y: 100,
                        constrain: true,
                        layout: 'fit',
                        buttons: [{
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function() {
                                    winModificargestuniversidades.hide();
                                }
                            }, {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                handler: function() {
                                    modificarUniversidad("apl");
                                }
                            }, {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function() {
                                    modificarUniversidad("aceptar");
                                }
                            }
                        ]
                    });
                    winModificargestuniversidades.add(formgestuniversidades);
                    winModificargestuniversidades.show();
                    winModificargestuniversidades.on('hide', function() {
                        formgestuniversidades.getForm().reset();
                    }, this);
                    formgestuniversidades.getForm().loadRecord(sm.getLastSelected());
                }
                break;
        }
    }
    var stGpgestuniversidades = Ext.create('Ext.data.ArrayStore', {
        fields: [{
                name: "iduniversidad"
            }, {
                name: 'descripcion'
            }, {
                name: 'estado'
            }],
        proxy: {
            type: 'ajax',
            url: 'cargarUniversidad',
            reader: {
                type: 'json',
                id: 'iduniversidad',
                root: 'datos'
            }
        }
    });
             var search = Ext.create ('Ext.form.field.Trigger', {
                store:stGpgestuniversidades,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                width: 400,
                fieldLabel: perfil.etiquetas.lbBtnBuscar,
                labelWidth: 40,
                onTrigger1Click: function () {
                    var me = this;

                    if (me.hasSearch) {
                        me.setValue('');
                        me.store.clearFilter();
                        me.hasSearch = false;
                        me.triggerCell.item(0).setDisplayed(false);
                        me.updateLayout();
                    }
                },

                onTrigger2Click: function () {
                    var me = this,
                        value = me.getValue();

                    if (value != null) {
                        me.store.clearFilter();
                        me.store.filter({filterFn: function(item) { return item.get("descripcion").toLowerCase().match(me.getValue().toLowerCase());}});
                        me.hasSearch = true;
                        me.triggerCell.item(0).setDisplayed(true);
                        me.updateLayout();
                    }
            }
        });
     search.on('afterrender', function () {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                );
     search.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                f.onTrigger2Click();
            }
        });
    var Gpgestuniversidades = Ext.create('Ext.grid.Panel', {
        store: stGpgestuniversidades,
        stateId: 'stateGrid',
        columnLines: true,
        viewConfig:{
            getRowClass: function(record, rowIndex, rowParams, store){
                if (record.data.estado == false)
                    return 'FilaRoja';
            }
        },
        columns: [{
                hidden:true,
                dataIndex: 'iduniversidad'
            }, {hidden:true,
                dataIndex: 'estado'
            }, {
                header: 'Universidad',
                flex: 1,
                sortable: true,
                dataIndex: 'descripcion'
            }],
        region: 'center',
        tbar: [btnAdicionargestuniversidades, btnModificargestuniversidades, btnEliminargestuniversidades, "|", search]
    });
    var sm = Gpgestuniversidades.getSelectionModel();
    sm.on('selectionchange', function(sel, selectedRecord) {
        if (selectedRecord.length) {
            btnModificargestuniversidades.enable();
            btnEliminargestuniversidades.enable();
        } else {
            btnModificargestuniversidades.disable();
            btnEliminargestuniversidades.disable();
        }
    });
    stGpgestuniversidades.load();
    function adicionarUniversidad(apl) {
        //si es la opción de aplicar
        if (formgestuniversidades.getForm().isValid()) {
            formgestuniversidades.getForm().submit({
                url: 'insertarUniversidad',
                waitMsg: perfil.etiquetas.lbMsgRegistrando,
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        formgestuniversidades.getForm().reset();
                        stGpgestuniversidades.reload();
                        if (apl === "aceptar")
                            winAdicionargestuniversidades.hide();
                    }


                }
            });
        }
    }
    function modificarUniversidad(apl) {
        //si es la opción de aplicar
        if (formgestuniversidades.getForm().isValid()) {
            formgestuniversidades.getForm().submit({
                url: 'modificarUniversidad',
                waitMsg: perfil.etiquetas.lbMsgModificando,
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        stGpgestuniversidades.reload();
                        if (apl === "aceptar")
                            winModificargestuniversidades.hide();
                    }

                }
            });
        }
    }
    function eliminarUniversidad(buttonId) {
        if (buttonId === "yes") {
            Ext.Ajax.request({
                url: 'eliminarUniversidad',
                method: 'POST',
                params: {iduniversidad: sm.getLastSelected().data.iduniversidad},
                callback: function(options, success, response) {
                    responseData = Ext.decode(response.responseText);
                    if (responseData.codMsg === 1) {
                        stGpgestuniversidades.reload();
                        sm.deselect();
                    }
                }
            });
        }
    }
    var general = Ext.create('Ext.panel.Panel', {layout: 'border', items: [Gpgestuniversidades]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});
}