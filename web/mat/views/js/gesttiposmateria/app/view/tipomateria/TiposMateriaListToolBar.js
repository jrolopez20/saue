Ext.define('GestTiposMateria.view.tipomateria.TiposMateriaListToolBar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.tiposmaterialisttbar',

    initComponent: function () {

        this.items = [
            {
                id: 'idBtnAddTipoMateria',
                text: perfil.etiquetas.lbBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                id: 'idBtnUpdTipoMateria',
                text: perfil.etiquetas.lbBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'modificar',
                disabled: true
            },
            {
                id: 'idBtnDelTipoMateria',
                text: perfil.etiquetas.lbBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'eliminar',
                disabled: true
            }
        ];

        this.callParent(arguments);
    }
});