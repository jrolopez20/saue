Ext.define('GestMatxPensum.view.pensum.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.pensum_combo',

    fieldLabel: 'Pensum',//Etiqueta
    emptyText: '--Seleccione--',//perfil.etiquetas.lbEmpCombo,
    editable: false,
    store: Ext.create('GestMatxPensum.store.Pensums'),
    queryMode: 'local',
    name: 'idpensum',
    displayField: 'descripcion',
    valueField: 'idpensum',
    allowBlank: false,
    disabled: true
});