Ext.define('GestMatxPensum.view.enfasi.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.enfasi_combo',

    id: 'idMatxPensumEnfasisCombo',
    fieldLabel: 'Enfasis',//Etiqueta
    emptyText: '--Seleccione--',//perfil.etiquetas.lbEmpCombo,
    editable: false,
    store: Ext.create('GestMatxPensum.store.Enfasis'),
    queryMode: 'local',
    name: 'idenfasis',
    displayField: 'descripcion',
    valueField: 'idenfasis',
    allowBlank: false,
    /*listConfig: {
        loadingText: 'Cargando énfasis...',//Etiquetas
        emptyText: 'No existen énfasis.'//Etiquetas
    },
    pageSize: 10,*/
    disabled: true
});