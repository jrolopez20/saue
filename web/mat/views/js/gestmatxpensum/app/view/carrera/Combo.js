Ext.define('GestMatxPensum.view.carrera.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.carrera_combo',

    id: 'idMatxPensumCarrerasCombo',
    fieldLabel: 'Carrera',//perfil.etiquetas.lbHdrDescripcionCarrera,
    emptyText: '--Seleccione--',//perfil.etiquetas.lbEmpCombo,
    allowBlank: false,
    editable: false,
    store: Ext.create('GestMatxPensum.store.Carreras'),
    queryMode: 'local',
    name: 'idcarrera',
    displayField: 'descripcion',
    valueField: 'idcarrera',
    /*anchor: '95%',
    listConfig: {
        loadingText: 'Cargando áreas...',//Etiquetas
        emptyText: 'No existen áreas.'//Etiquetas
    },
    pageSize: 10,*/
    disabled: true
});