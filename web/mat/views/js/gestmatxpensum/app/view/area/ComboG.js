Ext.define('GestMatxPensum.view.area.ComboG', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.area_general_combo',

    fieldLabel: 'Área general',//Etiqueta
    emptyText: '--Seleccione--',//perfil.etiquetas.lbEmpCombo,
    editable: false,
    store: Ext.create('GestMatxPensum.store.AreasGenerales'),
    queryMode: 'local',
    name: 'idareageneral',
    displayField: 'descripcion',
    valueField: 'idareageneral',
    allowBlank: false
});