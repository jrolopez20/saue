Ext.define('GestMatxPensum.view.area.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.area_combo',

    //id: 'idMateriasAreasCombo',
    fieldLabel: 'Área',//perfil.etiquetas.lbHdrDescripcionArea,
    emptyText: '--Seleccione--',//perfil.etiquetas.lbEmpCombo,
    editable: false,
    allowBlank: false,
    store: 'GestMatxPensum.store.Areas',
    queryMode: 'local',
    name: 'idarea',
    displayField: 'descripcion',
    valueField: 'idarea',
    disabled: true
});
