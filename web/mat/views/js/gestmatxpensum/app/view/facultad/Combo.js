Ext.define('GestMatxPensum.view.facultad.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.facultad_combo',

    fieldLabel: 'Facultad',//Etiqueta
    emptyText: '--Seleccione--',//perfil.etiquetas.lbEmpCombo,
    editable: false,
    store: Ext.create('GestMatxPensum.store.Facultades'),
    queryMode: 'local',
    name: 'idfacultad',
    displayField: 'denominacion',
    valueField: 'idfacultad',
    allowBlank: false
});