Ext.define('GestMatxPensum.store.AreasGenerales', {
    extend: 'Ext.data.Store',
    model: 'GestMatxPensum.model.AreaGeneral',

    autoLoad: true,
    pageSize: 5,
    proxy: {
        type: 'rest',
        url: 'cargarAreasGenerales',
        actionMethods: { //Esta Linea es necesaria para el metodo de llamada POST o GET
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad'
        }
    }
});