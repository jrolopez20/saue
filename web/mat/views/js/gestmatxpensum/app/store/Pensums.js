Ext.define('GestMatxPensum.store.Pensums', {
    extend: 'Ext.data.Store',
    model: 'GestMatxPensum.model.Pensum',

    //autoLoad: true,
    pageSize: 25,
    /*data:[
        {'idcarrera':1, 'descripcion': 'Carrera 1'},
        {'idcarrera':2, 'descripcion': 'Carrera 2'},
        {'idcarrera':3, 'descripcion': 'Carrera 3'},
        {'idcarrera':4, 'descripcion': 'Carrera 4'},
        {'idcarrera':5, 'descripcion': 'Carrera 5'},
        {'idcarrera':6, 'descripcion': 'Carrera 6'}
    ]*/
    proxy: {
        type: 'rest',
        url: 'cargarPensum',
        actionMethods: { //Esta Linea es necesaria para el metodo de llamada POST o GET
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad'
        }
    }
});