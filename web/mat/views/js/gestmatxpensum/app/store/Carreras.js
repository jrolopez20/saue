Ext.define('GestMatxPensum.store.Carreras', {
    extend: 'Ext.data.Store',
    model: 'GestMatxPensum.model.Carrera',

    //autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'rest',
        url: 'cargarCarreras',
        actionMethods: { //Esta Linea es necesaria para el metodo de llamada POST o GET
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad'
        }
    }
});