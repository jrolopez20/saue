Ext.define('GestMatxPensum.store.MateriasxPensum', {
    extend: 'Ext.data.Store',
    model: 'GestMatxPensum.model.MateriaxPensum',

    //autoLoad: true,
    pageSize: 20,
    //autoSync: true,
    /*data:[
     {'idmateria':1, 'codmateria':'MAT001', 'descripcion': 'Materia 1', 'credito': 4.8},
     {'idmateria':2, 'codmateria':'MAT002', 'descripcion': 'Materia 2', 'credito': 4.8},
     {'idmateria':3, 'codmateria':'MAT003', 'descripcion': 'Materia 3', 'credito': 4.8},
     {'idmateria':4, 'codmateria':'MAT004', 'descripcion': 'Materia 4', 'credito': 4.8},
     {'idmateria':5, 'codmateria':'MAT005', 'descripcion': 'Materia 5', 'credito': 4.8},
     {'idmateria':6, 'codmateria':'MAT006', 'descripcion': 'Materia 6', 'credito': 4.8}
     ]*/
    proxy: {
        type: 'ajax',
        api: {
            read: 'cargarMatxPensum',
            create: 'insertarMatxPensum',
            update: 'modificarMatxPensum',
            destroy: 'eliminarMatxPensum'
        },
        actionMethods: { //Esta Linea es necesaria para el metodo de llamada POST o GET
            read: 'POST',
            create: 'POST',
            update: 'POST',
            destroy: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});