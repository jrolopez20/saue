Ext.define('GestMateriaxMencion.model.Mencion', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idmencion', type: 'int', convert: null},
        {name: 'descripcion', type: 'string'}
    ]
})