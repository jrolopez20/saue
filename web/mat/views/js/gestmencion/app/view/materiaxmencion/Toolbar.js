Ext.define('GestMateriaxMencion.view.materiaxmencion.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.materiaxmencion_tbar',

    initComponent: function () {
        var me = this;

        me.items = [
            {
                text: 'Eliminar',//perfil.etiquetas.lbBtnBuscar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'eliminar',
                disabled: true
            }
        ];

        me.callParent(arguments);
    }
});