Ext.define('GestMaterias.view.idioma.FieldSet', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.materia_idioma_fieldset',
    checkboxToggle: true,
    checkboxName: 'have_idioma',
    collapsed: true,
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        padding: '5'
    },

    initComponent: function(){
        var me = this;

        me.title = perfil.etiquetas.lbCmpMateriaIdioma;

        me.items = [
            {
                xtype: 'materia_idioma_combo'
            },
            {
                xtype: 'materia_idioma_nivel'
            }
        ];

        me.callParent(arguments);
    }
});