Ext.define('GestMaterias.view.materia.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.materiaedit',

    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 400,

    initComponent: function () {
        var me = this;

        me.items =
            {
                xtype: 'form',
                border: 0,
                fieldDefaults: {
                    anchor: '100%',
                    msgTarget: 'side'
                },
                defaults: {
                    padding: '5'
                },
                flex: 1,
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idmateria'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: perfil.etiquetas.lbCmpCodMateria,
                        name: 'codmateria',
                        allowBlank: false,
                        blankText: 'Campo requerido'
                    },
                    {
                        xtype: 'textareafield',
                        fieldLabel: perfil.etiquetas.lbCmpDescripcion,
                        name: 'descripcion',
                        allowBlank: false,
                        blankText: 'Campo requerido'
                    },
                    {
                        xtype: 'textareafield',
                        fieldLabel: perfil.etiquetas.lbCmpTraduccion,
                        name: 'traduccion',
                        allowBlank: false,
                        blankText: 'Campo requerido'
                    },
                    {
                        xtype: 'numberfield',
                        fieldLabel: perfil.etiquetas.lbCmpNotaMinima,
                        name: 'min_nota_materia',
                        allowBlank: false,
                        blankText: 'Campo requerido',
                        value: 70,
                        minValue: 1,
                        allowDecimals: false
                    },
                    {
                        id: 'idobligatoria',
                        name: 'obligatoria',
                        xtype: 'checkbox',
                        fieldLabel: perfil.etiquetas.lbCmpObligatoria,
                        checked: true,
                        inputValue: true,
                        uncheckedValue: false
                    },
                    {
                        xtype: 'tipo_materia_combo'
                    },
                    {
                        xtype: 'materia_idioma_fieldset'
                    },/*
                    {
                     id: 'idMateriasAreasCombo',
                     xtype: 'combo',
                     fieldLabel: perfil.etiquetas.lbHdrDescripcionArea,
                     emptyText: perfil.etiquetas.lbEmpCombo,
                     editable: false,
                     allowBlank: false,
                     store: 'GestMaterias.store.Areas',
                     queryMode: 'local',
                     name: 'descripcion_area',
                     displayField: 'descripcion_area',
                     valueField: 'idarea'
                     xtype: 'materia_area_combo'
                     },*/
                    {
                        id: 'idestado_materia',
                        name: 'estado',
                        xtype: 'checkbox',
                        fieldLabel: perfil.etiquetas.lbHdrEstado,
                        checked: true,
                        inputValue: true,
                        uncheckedValue: false
                    }
                ]
            }
        ;

        me.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                id: 'idBtnAplicar',
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAplicar,
                action: 'aplicar'
            },
            {
                id: 'idBtnAceptar',
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                action: 'aceptar'
            }
        ];

        me.callParent(arguments);
    }
})