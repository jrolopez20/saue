Ext.define('GestAreas.view.area_general.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.area_general_combo',

    fieldLabel: 'Area Gen',//Etiqueta
    editable: false,
    store: Ext.create('GestAreas.store.AreasGenerales'),
    queryMode: 'local',
    name: 'idareageneral',
    displayField: 'descripcion_area_general',
    valueField: 'idareageneral',
    allowBlank: false
});