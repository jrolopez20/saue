Ext.define('GestAreas.view.area.ComboG', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.area_general_combo',

    fieldLabel: 'Área general',//Etiqueta
    editable: false,
    store: Ext.create('GestAreas.store.AreasGenerales'),
    queryMode: 'local',
    name: 'descripcion_area_general',
    displayField: 'descripcion_area_general',
    valueField: 'descripcion_area_general',
    allowBlank: false
});