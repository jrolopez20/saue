var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestprofesores', function() {
    cargarInterfaz();
});
var winAdicionargestprofesores;
var winModificargestprofesores;
var formgestprofesores;
Ext.QuickTips.init();
function cargarInterfaz() {
    
    var stcmbSexo = Ext.data.SimpleStore({
        fields: ["sexo"],
        data: [{"sexo": "Femenino"}, {"sexo": "Masculino"}]
    });

    Ext.define('EstadoCivil', {
        extend: 'Ext.data.Model',
        fields: ['idestadocivil', 'descripcion']
    });
    Ext.define('Historial', {
        extend: 'Ext.data.Model',
        fields: ['idcurso', 'materia', 'periodo', 'horario', 'local']
    });

    var stcmbEstadocivil = Ext.create('Ext.data.Store', {
        model: 'EstadoCivil',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarEstadoCivil',
            reader: {
                type: 'json',
                id: 'idestadocivil',
                root: 'datos'
            }
        }
    });
    var stHistorial = Ext.create('Ext.data.Store', {
        model: 'Historial',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: 'cargarHistorial',
            reader: {
                type: 'json',
                id: 'idcurso',
                root: 'datos'
            },
            actionMethods: { //Esta Linea es necesaria para el metodo de llamada POST o GET
                    read: 'POST',
            },
        }
    });
    stHistorial.on('beforeload', function (store) {
            store.getProxy().extraParams = {
                "idprofesor": sm.getLastSelected().data.idprofesor,
                    }  
    });
    var btnAdicionargestprofesores = Ext.create('Ext.Button', {
        id: 'btnAgrgestprofesores',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormgestprofesores('add');
        }
    });
    var btnModificargestprofesores = Ext.create('Ext.Button', {
        id: 'btnModgestprofesores',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormgestprofesores('mod');
        }
    });
    var btnEliminargestprofesores = Ext.create('Ext.Button', {
        id: 'btnEligestprofesores',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function() {
            Ext.MessageBox.show({
                title: 'Eliminar',
                msg: 'Desea eliminar el elemento?',
                buttons: Ext.MessageBox.YESNO,
                icon: Ext.MessageBox.QUESTION,
                fn: eliminarProfesor
            });
        }
    });
    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    formgestprofesores = new Ext.FormPanel({
        frame: true,
        bodyStyle: 'padding:5px auto 0px',
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [{
                xtype: 'tabpanel',
                plain: true,
                activeTab: 0,
                id: 'tabadd',
                items: [{
                        title: 'Información personal',
                        layout: 'column',
                        border: false,
                        items: [
                            {
                                defaults: {width: '100%'},
                                columnWidth: .5,
                                border: false,
                                bodyStyle: 'padding:5px 0 5px 5px',
                                items: [{
                                        xtype: 'textfield',
                                        fieldLabel: 'Apellidos',
                                        name: 'apellidos',
                                        anchor: '100%',
                                        allowBlank: false
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nombre',
                                        name: 'nombre',
                                        anchor: '100%',
                                        allowBlank: false
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: 'Correo',
                                        name: 'correo',
                                        anchor: '100%',
                                        allowBlank: false
                                    }, {
                                        xtype: 'fieldset',
                                        title: 'Identificación',
                                        defaults: {width: '100%'},
                                        items: [{
                                                xtype: 'radiogroup',
                                                id: 'radio',
                                                columns: 2,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Cédula', name: 'rb', inputValue: 'cedula', checked: true},
                                                    {boxLabel: 'Pasaporte', name: 'rb', inputValue: 'pasaporte'}
                                                ]
                                            }, {
                                                id: 'cedpas',
                                                xtype: 'textfield',
                                                name: 'cedpas',
                                                allowBlank: false
                                            }]
                                    }
                                ]
                            }, {
                                defaults: {width: 180},
                                columnWidth: .5,
                                border: false,
                                bodyStyle: 'padding:5px',
                                items: [{
                                        xtype: 'combobox',
                                        fieldLabel: 'Sexo',
                                        name: 'sexo',
                                        editable: false,
                                        forceSelection: true,
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        selectOnFocus: true,
                                        emptyText: "Seleccione el sexo",
                                        store: stcmbSexo,
                                        queryMode: 'local',
                                        displayField: 'sexo',
                                        valueField: 'sexo'
                                    }, {
                                        xtype: 'combobox',
                                        fieldLabel: 'Estado civil',
                                        name: 'idestadocivil',
                                        editable: false,
                                        forceSelection: true,
                                        typeAhead: true,
                                        queryMode: 'local',
                                        triggerAction: 'all',
                                        store: stcmbEstadocivil,
                                        displayField: 'descripcion',
                                        valueField: 'idestadocivil'
                                    }, {
                                        xtype: 'datefield',
                                        name: "fecha_nacimiento",
                                        fieldLabel: 'Fecha de nacimiento',
                                        maxValue: new Date(),
                                        allowBlank: false,
                                        editable: false
                                    }, {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Activado:',
                                        name: 'estado',
                                        labelAlign: 'left',
                                        style: {
                                            marginTop: '10px'
                                        },
                                        checked: true
                                    }]
                            }
                        ]
                    }, {
                        title: 'Datos adicionales',
                        bodyStyle: 'padding:5px',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'Especialización',
                                name: 'especializacion',
                                width: '70%',
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Instrucción',
                                name: 'instruccion',
                                width: '70%'
                            }, {
                                xtype: 'textareafield',
                                name: 'domicilio',
                                fieldLabel: 'Domicilio',
                                width: '100%'
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Teléfono',
                                name: 'telefono',
                                width: '50%'
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Celular',
                                name: 'celular',
                                width: '50%'
                            }]
                    }],
                listeners: {
                    beforetabchange: function(tabs, newTab, oldTab) {
                        return formgestprofesores.getForm().isValid();
                    }
                }
            }]
    });
formgestprofesores2 = new Ext.FormPanel({
        frame: true,
        bodyStyle: 'padding:5px auto 0px',
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },                       
        items: [{
                xtype: 'tabpanel',
                plain: true,
                activeTab: 0,
                id:'tabmod', 
                height: 320,
                items: [{
                        title: 'Información personal',
                        layout: 'column',
                        border: false,
                        items: [
                            {
                                defaults: {width: '100%'},
                                columnWidth: .5,
                                border: false,
                                bodyStyle: 'padding:5px 0 5px 5px',
                                items: [{
                                        xtype: 'textfield',
                                        fieldLabel: 'Apellidos',
                                        name: 'apellidos',
                                        anchor: '100%',
                                        allowBlank: false
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nombre',
                                        name: 'nombre',
                                        anchor: '100%',
                                        allowBlank: false
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: 'Correo',
                                        name: 'correo',
                                        anchor: '100%',
                                        allowBlank: false
                                    }, {
                                        xtype: 'fieldset',
                                        title: 'Identificación',
                                        defaults: {width: '100%'},
                                        items: [{
                                                xtype: 'radiogroup',
                                                id: 'radio2',
                                                columns: 2,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Cédula', name: 'rb', inputValue: 'cedula', checked: true},
                                                    {boxLabel: 'Pasaporte', name: 'rb', inputValue: 'pasaporte'}
                                                ]
                                            }, {
                                                id: 'cedpas2',
                                                xtype: 'textfield',
                                                name: 'cedpas',
                                                allowBlank: false
                                            }]
                                    }
                                ]
                            }, {
                                defaults: {width: 180},
                                columnWidth: .5,
                                border: false,
                                bodyStyle: 'padding:5px',
                                items: [{
                                        xtype: 'combobox',
                                        fieldLabel: 'Sexo',
                                        name: 'sexo',
                                        editable: false,
                                        forceSelection: true,
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        selectOnFocus: true,
                                        emptyText: "Seleccione el sexo",
                                        store: stcmbSexo,
                                        queryMode: 'local',
                                        displayField: 'sexo',
                                        valueField: 'sexo'
                                    }, {
                                        xtype: 'combobox',
                                        fieldLabel: 'Estado civil',
                                        name: 'idestadocivil',
                                        editable: false,
                                        forceSelection: true,
                                        emptyText: "Seleccione el estado civil",
                                        typeAhead: true,
                                        queryMode: 'local',
                                        triggerAction: 'all',
                                        store: stcmbEstadocivil,
                                        displayField: 'descripcion',
                                        valueField: 'idestadocivil'
                                    }, {
                                        xtype: 'datefield',
                                        name: "fecha_nacimiento",
                                        fieldLabel: 'Fecha de nacimiento',
                                        maxValue: new Date(),
                                        allowBlank: false,
                                        editable: false
                                    }, {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Activado:',
                                        name: 'estado',
                                        labelAlign: 'left',
                                        style: {
                                            marginTop: '10px'
                                        },
                                        checked: true
                                    }]
                            }
                        ]
                    }, {
                        title: 'Datos adicionales',
                        bodyStyle: 'padding:5px',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'Especialización',
                                name: 'especializacion',
                                width: '70%',
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Instrucción',
                                name: 'instruccion',
                                width: '70%'
                            }, {
                                xtype: 'textareafield',
                                name: 'domicilio',
                                fieldLabel: 'Domicilio',
                                width: '100%'
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Teléfono',
                                name: 'telefono',
                                width: '50%'
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Celular',
                                name: 'celular',
                                width: '50%'
                            }]
                    }, {
                        title: 'Historial laboral',
                        bodyStyle: 'padding:5px',
                        flex: 1,
                        layout:'anchor',
                        items: [{
                                xtype:'gridpanel',
                                anchor: '100%, 100%',
                                viewConfig:{
                                        getRowClass: function(record, rowIndex, rowParams, store){
                                            if (record.data.periodo.match("2014"))
                                                return 'FilaVerde';
                                        }
                                    },
                                store: stHistorial,
                                columns: [
                                    { text: 'Materia',  dataIndex: 'materia' },
                                    { text: 'Periodo', dataIndex: 'periodo'},
                                    { text: 'Horario', dataIndex: 'horario' , flex: 1 },
                                    { text: 'Local', dataIndex: 'local'}
                                    ],
                                dockedItems: [{
                                    xtype: 'pagingtoolbar',
                                    store: stHistorial,
                                    dock: 'bottom',
                                }],
                            }]
                    }],
                listeners: {
                    beforetabchange: function(tabs, newTab, oldTab) {
                        return formgestprofesores2.getForm().isValid();
                    }
                }
            }]
    });
    function mostFormgestprofesores(opcion) {
        switch (opcion) {
            case 'add':
                {
                    if (!winAdicionargestprofesores) {
                        winAdicionargestprofesores = new Ext.Window({
                            title: 'Adicionar profesor',
                            closeAction: 'hide',
                            width: 450,
                            height: 400,
                            x: 220,
                            y: 100,
                            items:[formgestprofesores],
                            constrain: true,
                            layout: 'fit',
                            buttons: [{
                                    text: 'Cancelar',
                                    icon: perfil.dirImg + 'cancelar.png',
                                    handler: function() {
                                        winAdicionargestprofesores.hide();
                                    }
                                }, {
                                    text: 'Aplicar',
                                    icon: perfil.dirImg + 'aplicar.png',
                                    handler: function() {
                                        adicionarProfesor("apl",1);
                                    }
                                }, {
                                    text: 'Aceptar',
                                    icon: perfil.dirImg + 'aceptar.png',
                                    handler: function() {
                                        adicionarProfesor("aceptar",1);
                                    }
                                }
                            ]
                        });
                    }
                    winAdicionargestprofesores.doLayout();
                    winAdicionargestprofesores.show();
                    formgestprofesores.getForm().reset();
                    Ext.getCmp('tabadd').suspendEvents();
                    Ext.getCmp('tabadd').setActiveTab(0);
                    Ext.getCmp('tabadd').resumeEvents();
                }
                break;
            case 'mod':
                {
                    if (!winModificargestprofesores) {
                        winModificargestprofesores = new Ext.Window({
                            title: 'Modificar profesor',
                            closeAction: 'hide',
                            width: 500,
                            height: 400,
                            x: 220,
                            y: 100,
                            items:[formgestprofesores2],
                            constrain: true,
                            layout: 'fit',
                            buttons: [{
                                    text: 'Cancelar',
                                    icon: perfil.dirImg + 'cancelar.png',
                                    handler: function() {
                                        winModificargestprofesores.hide();
                                    }
                                }, {
                                    text: 'Aplicar',
                                    icon: perfil.dirImg + 'aplicar.png',
                                    handler: function() {
                                        modificarProfesor("apl",2);
                                    }
                                }, {
                                    text: 'Aceptar',
                                    icon: perfil.dirImg + 'aceptar.png',
                                    handler: function() {
                                        modificarProfesor("aceptar",2);
                                    }
                                }
                            ]
                        });
                    }
                    winModificargestprofesores.doLayout();
                    winModificargestprofesores.show();
                    stHistorial.load();
                    Ext.getCmp('tabmod').suspendEvents();
                    Ext.getCmp('tabmod').setActiveTab(0);
                    Ext.getCmp('tabmod').resumeEvents();
                    formgestprofesores2.getForm().loadRecord(sm.getLastSelected());
                    if (sm.getLastSelected().data.cedula) {
                        Ext.getCmp('radio2').getComponent(0).setValue(true);
                        Ext.getCmp('cedpas2').setValue(sm.getLastSelected().data.cedula);
                    } else {
                        Ext.getCmp('radio2').getComponent(1).setValue(true);
                        Ext.getCmp('cedpas2').setValue(sm.getLastSelected().data.pasaporte);
                    }
                }
                break;
        }
    }

    var stGpgestprofesores = Ext.create('Ext.data.ArrayStore', {
        fields: [{
                name: 'idprofesor'
            }, {
                name: 'nombre'
            }, {
                name: 'apellidos'
            }, {
                name: 'sexo'
            }, {
                name: 'estado'
            }, {
                name: 'estadocivil'
            }, {
                name: 'idestadocivil'
            }, {
                name: 'especializacion'
            }, {
                name: 'instruccion'
            }, {
                name: 'correo'
            }, {
                name: 'pasaporte'
            }, {
                name: 'cedula'
            }, {
                name: 'fecha_nacimiento'
            }, {
                name: 'domicilio'
            }, {
                name: 'telefono'
            }, {
                name: 'celular'
            }],
            pageSize:25,
        proxy: {
            remoteSort: true,
            type: 'ajax',
            url: 'cargarProfesores',
            reader: {
                type: 'json',
                id: 'idprofesor',
                totalProperty: "cantidad",
                root: 'datos'
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    var Gpgestprofesores = Ext.create('Ext.grid.Panel', {
        store: stGpgestprofesores,
        stateful: true,
        stateId: 'stateGrid',
        columns: [{
                hidden: true,
                dataIndex: 'idprofesor'
            }, {
                hidden: true,
                dataIndex: 'idestadocivil'
            }, {
                text: 'Apellidos',
                flex: 1,
                dataIndex: 'apellidos'
            }, {
                text: 'Nombre',
                flex: 1,
                dataIndex: 'nombre'
            }, {
                text: 'Sexo',
                flex: 1, dataIndex: 'sexo'
            }, {
                text: 'Estado civil',
                flex: 1,
                dataIndex: 'estadocivil'
            }, {
                text: 'Especialización',
                flex: 1,
                dataIndex: 'especializacion'
            }, {
                text: 'Instrucción',
                flex: 1,
                dataIndex: 'instruccion'
            }, {
                text: 'Correo',
                flex: 1,
                dataIndex: 'correo'
            }, {
                text: 'Pasaporte',
                flex: 1,
                dataIndex: 'pasaporte'
            }, {
                text: 'Cédula',
                flex: 1,
                dataIndex: 'cedula'
            }, {
                text: 'Fecha de nacimiento',
                flex: 1,
                dataIndex: 'fecha_nacimiento'
            }, {
                hidden: true,
                dataIndex: 'estado'
            }, {
                hidden: true,
                dataIndex: 'domicilio'
            }, {
                hidden: true,
                dataIndex: 'telefono'
            }, {
                hidden: true,
                dataIndex: 'celular'
            }],
        dockedItems: [{
                xtype: 'pagingtoolbar',
                store: stGpgestprofesores,
                dock: 'bottom',
            }],
        viewConfig:{
            getRowClass: function(record, rowIndex, rowParams, store){
                if (record.data.estado == false)
                    return 'FilaRoja';
            }
        },
        region: 'center',
        tbar: [btnAdicionargestprofesores, btnModificargestprofesores, btnEliminargestprofesores,
         {
                xtype: 'searchfield',
                store: stGpgestprofesores,
                width: 400,
                fieldLabel: perfil.etiquetas.lbBtnBuscar,
                labelWidth: 40,
                filterPropertysNames: ['nombre', 'apellidos'],
            }
           /* {xtype: 'textfield', labelWidth: 45, id: 'buscar'}, {xtype: 'button',
                text: perfil.etiquetas.lbBtnBuscar,
                icon: perfil.dirImg + 'buscar.png',
                iconCls: 'btn',
                handler: function() {
                    stGpgestprofesores.clearFilter();
                    if (Ext.getCmp('buscar').getValue() !== "") {
                        stGpgestprofesores.filter({
                            filterFn: function(item) {
                                exp = new RegExp(Ext.getCmp('buscar').getValue().toLowerCase());
                                return exp.test(item.get('apellidos').toLowerCase()) || exp.test(item.get('nombre').toLowerCase());
                            }
                        });
                    }
                }}*/
        ]});
    var txtBuscarProfesor = new Ext.form.TextField();
    var sm = Gpgestprofesores.getSelectionModel();
    sm.setSelectionMode('MULTI');
    sm.on('selectionchange', function(sel, selectedRecord) {
        if (selectedRecord.length === 1) {
            btnModificargestprofesores.enable();
            btnEliminargestprofesores.enable();
        } else if (selectedRecord.length > 1) {
            btnModificargestprofesores.disable();
            btnEliminargestprofesores.enable();
        } else {
            btnModificargestprofesores.disable();
            btnEliminargestprofesores.disable();
        }
    });
    stGpgestprofesores.load();
    function adicionarProfesor(apl, oper) {
        if(oper == 1)
            win = formgestprofesores;
        else
            win = formgestprofesores2;
        //si es la opción de aplicar
        if (win.getForm().isValid()) {
            win.getForm().submit({
                url: 'insertarProfesor',
                waitMsg: perfil.etiquetas.lbMsgRegistrandoProfesor,
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        win.getForm().reset();
                        stGpgestprofesores.reload();
                        if (apl === "aceptar")
                            winAdicionargestprofesores.hide();
                    }


                }
            });
        }
    }
    function modificarProfesor(apl,oper) {
      if(oper == 1)
            win = formgestprofesores;
        else
            win = formgestprofesores2;
        if (win.getForm().isValid()) {
            win.getForm().submit({
                url: 'modificarProfesor',
                params: {
                    idprofesor: sm.getLastSelected().data.idprofesor},
                waitMsg: perfil.etiquetas.lbMsgModificandoProfesor,
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        stGpgestprofesores.reload();
                        if (apl === "aceptar")
                            winModificargestprofesores.hide();
                    }

                }
            });
        }
    }
    function eliminarProfesor(buttonId) {
        if (buttonId === "yes") {
            var arrProfesoresElim = sm.getSelection();
            var arrProfElim = [];
            for (var i = 0; i < arrProfesoresElim.length; i++) {
                arrProfElim.push(arrProfesoresElim[i].data.idprofesor);
            }
            Ext.Ajax.request({
                url: 'eliminarProfesor',
                method: 'POST',
                params: {ArrayProfDel: Ext.encode(arrProfElim)},
                callback: function(options, success, response) {
                    responseData = Ext.decode(response.responseText);
                    if (responseData.codMsg === 1) {
                        stGpgestprofesores.reload();
                        sm.deselect();
                    }
                }
            });
        }
    }
    var general = Ext.create('Ext.panel.Panel', {layout: 'border', items: [Gpgestprofesores]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});
}