Ext.define('Ext.ux.form.SearchComboField', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.searchcombofield',

    editable: true,
    typeAhead: true,
    forceSelection: true,
    trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
    trigger2Cls: Ext.baseCSSPrefix + 'x-form-arrow-trigger',
    queryMode: 'local',
    hasSearch: false,
    filterParamName: 'filtros',

    initComponent: function () {
        var me = this;

        me.callParent(arguments);

        if (Ext.isString(me.storeToFilter)){
            me.storeToFilter = Ext.data.StoreManager.lookup(me.storeToFilter);
        }

        // We're going to use filtering
        me.storeToFilter.remoteFilter = true;

        // If the Store has not been *configured* with a filterParam property, then use our filter parameter name
        if (!me.storeToFilter.proxy.hasOwnProperty('filterParam')) {
            me.storeToFilter.proxy.filterParam = me.filterParamName;
        };

        if (me.filterPropertysNames == null) {
            me.filterPropertysNames = [me.valueField];
        };

        //Creando los filtros por cada filterPropertysNames definido
        me.filtros = Ext.create('Ext.util.MixedCollection');

        me.on(
            {
                select: me.onSelect
            }
        );
    },

    afterRender: function () {
        var me = this;
        me.callParent();
        me.triggerCell.item(0).setDisplayed(false);
        me.updateLayout();
    },

    onTrigger1Click: function () {
        var me = this;

        if (me.hasSearch) {
            me.setValue('');
            //me.store.clearFilter();
            me.filtros.each(
                function (filtro) {
                    me.storeToFilter.filters.eachKey(
                        function (key, filter) {
                            if (filtro.id === key) {
                                me.storeToFilter.filters.remove(filter);
                            }
                        }
                    )
                }
            )
            /*me.filtros.each(
             function (filtro) {
             console.log();
             me.storeToFilter.removeFilter(filtro);
             }
             )*/
            me.storeToFilter.load();
            me.hasSearch = false;
            me.triggerCell.item(0).setDisplayed(false);
            me.updateLayout();
        }
    },

    onTrigger2Click: function () {
        this.expand();
    },

    onSelect: function (combo) {
        var me = this,
            value = combo.getValue();

        if (value != null) {

            if (me.filterPropertysNames.length > 0) {
                for (var i = 0; i < me.filterPropertysNames.length; i++) {
                    me.filtros.add(i, Ext.create('Ext.util.Filter', {
                        id: 'id' + me.filterPropertysNames[i],
                        property: me.filterPropertysNames[i],
                        value: value
                    }))
                }
            }

            // Param name is ignored here since we use custom encoding in the proxy.
            // id is used by the Store to replace any previous filter
            me.filtros.each(
                function (filtro, index, len) {
                    me.storeToFilter.addFilter(
                        [
                            filtro
                        ], (index == len - 1)
                    );
                }
            )

            me.hasSearch = true;
            me.triggerCell.item(0).setDisplayed(true);
            me.updateLayout();
        }
    }
})