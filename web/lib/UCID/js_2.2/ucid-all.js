/*
 * Componente portal de aplicaciones
 *
 * Elemento base del portal donde se colocan funciones generales.
 *
 * @author Dionisdel Ponce Santana
 * @package Portal
 * @subpackage Portal
 * @copyright UCID-ERP Cuba
 * @version 1.0-0
 */

if (development == false && typeof(console) == "object" && console.info.toString().indexOf('[native code]').toString() == '-1') {
    alert('FIREBUG INSTALADO.\nPara acceder al sistema debe desinstalarlo.');
    window.location = 'about:blank';
}

var UCID = new Object();
var winError, winConfirmDetalles;
var acciones_reportes = [];
var perfil = null;
if (window.parent.UCID.portal)
    perfil = window.parent.UCID.portal.perfil; else perfil = {};
UCID.portal = new Object();

if (Ext) {
    Ext.BLANK_IMAGE_URL = '/lib/ExtJS/temas/default/images/s.gif';


    //Ext.WindowMgr.zseed = 50000;
    if (window.parent.UCID.portal.dir_ext_css) {
        var dir_icon_class = window.parent.UCID.portal.dir_ext_css + 'css/icon-clases.css';
        var existLink = false;
        var linkArr = document.getElementsByTagName("link");
        var port = '';
        if (window.location.port)
            port = ':' + window.location.port;
        var dirHostIconCSS = window.location.protocol + '//' + window.location.host + port + dir_icon_class;
        var dirHostnameIconCSS = window.location.protocol + '//' + window.location.hostname + port + dir_icon_class;
        for (var i = 0; i < linkArr.length; i++) {
            if (linkArr[i].href == dirHostIconCSS || linkArr[i].href == dirHostnameIconCSS) {
                existLink = true;
                break;
            }
        }
        if (!existLink)
            importarCSS(dir_icon_class);
    }

    Ext.Ajax.on('requestcomplete', function (conn, response, options) {
        var respText = response.responseText;
        var respXML = response.responseXML;
        if (respText && !respXML) {
            var respObj = Ext.decode(respText);
            if (respObj.codMsg && respObj.codMsg >= 1 && respObj.codMsg <= 4) {
                Ext.getBody().unmask();
                mostrarMensaje(respObj.codMsg, respObj.mensaje, null, respObj.detalles);
            }

        }
    });

    UCID.portal.cargarEtiquetas = function (vistaCU, fn) {
        Ext.Ajax.request({
            url: 'cargaretiquetas',
            method: 'POST',
            params: {
                vista: vistaCU
            },
            callback: function (options, success, response) {
                if (success) {
                    var etiquetas = Ext.decode(response.responseText);
                    if (!perfil) {
                        perfil = {};
                    }
                    perfil.etiquetas = etiquetas;
                    var codMsg = perfil.etiquetas.codMsg;
                    if (!codMsg)
                        fn(perfil, etiquetas);
                }
            }
        });
    }

    Ext.ns('Zeolides.Utils.NotificationBox');
    Zeolides.Utils.NotificationBox.initStyles = function () {
        var styleLeaf = document.createElement('style');
        styleLeaf.innerHTML = "" +
            ".msg .x-box-mc {font-size:14px;} " +
            'div#imgOk{background: url("/lib/UCID/images/ok.png") no-repeat left; width: 50px; height: 50px; float: left; margin: -25px 0px 0px -25px;}' +
            "#msg-div {position:absolute;left:35%;top:10px;width:300px;z-index:20000;} " +
            "#msg-div .msg {border-radius: 8px;-moz-border-radius: 8px;background: #F6F6F6;border: 2px solid #3764A0;margin-top: 10px;padding: 10px 15px;color: #04408c;box-shadow: 2px 2px 15px #3764A0;} " +
            "#msg-div .msg h3 {margin: 0 0 8px;font-weight: bold;font-size: 12px;} " +
            "#msg-div .msg p {margin: 0;} ";
        document.body.appendChild(styleLeaf);
    }();

    Zeolides.Utils.NotificationBox.show = function (message) {
        var msgCt = Ext.DomHelper.insertFirst(document.body, {id: 'msg-div'}, true);
        var msg = Ext.DomHelper.append(msgCt, '<div class="msg"><div id="imgOk"></div><h3>Informaci&oacute;n</h3><p>' + message + '</p></div>', true);
        msg.slideIn('t').pause(3).ghost("t", {/*duration: 4, */remove: true});
    };

    // Funcion para mostrar un mensaje
    mostrarMensaje = function (tipo, msg, fn, detalle) {
        var buttons = new Array(Ext.MessageBox.OK, Ext.MessageBox.OKCANCEL, Ext.MessageBox.OK);
        var title = new Array('Informaci&oacute;n', 'Confirmaci&oacute;n', 'Error');
        var icons = new Array(Ext.MessageBox.INFO, Ext.MessageBox.QUESTION, Ext.MessageBox.ERROR);
        //con switch por si surgen nuevos tipos de mensajes
        //para los de tipo 4 y 5 se puede hacer con IF-ELSE con menos codigo en caso de que no vayan a surgir nuevos tipos de mensajes
        switch (tipo) {
            case 4:
                if (!winError) {
                    winError = new Ext.Window({
                        title: ' Error ',
                        modal: true,
                        width: 500,
                        minWidth: 500,
                        buttonAlign: 'center',
                        autoHeight: true,
                        bodyBorder: false,
                        layout: 'fit',
                        defaults: {
                            frame: true,
                            border: false
                        },
                        items: new Ext.Panel({
                            frame: true,
                            monitorResize: true,
                            autoHeight: true,
                            layout: 'column',
                            items: [
                                {
                                    columnWidth: .15,
                                    items: {
                                        style: 'margin:2px 0px 0px 2px;',
                                        html: '<div id="pIconoERROR" class="iconoERROR"></div>'
                                    }
                                },
                                {
                                    columnWidth: .75,
                                    items: {
                                        xtype: 'panel',
                                        autoHeight: true,
                                        html: '<div id="pMSG">' + msg + '</div>'
                                    }
                                },
                                {
                                    columnWidth: 1,
                                    items: new Ext.form.FieldSet({
                                        id: 'ucid.winerror',
                                        autoScroll: true,
                                        layout: 'form',
                                        title: ' Detalles : ',
                                        autoWidth: true,
                                        html: '<div id="pDetalle"><pre>' + detalle + '</pre></div>',
                                        height: 150,
                                        collapsed: true,
                                        collapsible: true
                                    })
                                }
                            ]
                        }),
                        buttons: [
                            {
                                text: 'Aceptar',
                                handler: function () {
                                    winError.hide();
                                    if (typeof(fn) == 'function')fn();
                                }
                            }
                        ]
                    });
                }
                Ext.getCmp('ucid.winerror').collapse();
                winError.doLayout(true);
                winError.show();
                document.getElementById("pMSG").innerHTML = msg;
                document.getElementById("pDetalle").innerHTML = detalle;
                break;
            case 5:
                if (winConfirmDetalles)winConfirmDetalles.destroy();
                winConfirmDetalles = new Ext.Window({
                    title: ' Confirmaci&oacute;n ',
                    modal: true,
                    width: 400,
                    minWidth: 300,
                    buttonAlign: 'center',
                    autoHeight: true,
                    closeAction: 'hide',
                    bodyBorder: false,
                    layout: 'fit',
                    defaults: {
                        frame: true,
                        border: false
                    },
                    items: new Ext.Panel({
                        frame: true,
                        monitorResize: true,
                        autoHeight: true,
                        layout: 'column',
                        items: [
                            {
                                columnWidth: .2,
                                items: {
                                    style: 'margin:2px 0px 0px 2px;',
                                    html: '<div id="iconoCONFIRM" class="iconoCONFIRM></div>'
                                }
                            },
                            {
                                columnWidth: .7,
                                items: {
                                    xtype: 'panel',
                                    autoHeight: true,
                                    html: '<div id="pMSGcd">' + msg + '</div>'
                                }
                            },
                            {
                                columnWidth: 1,
                                items: new Ext.form.FieldSet({
                                    autoScroll: true,
                                    layout: 'form',
                                    title: ' Detalles : ',
                                    autoWidth: true,
                                    html: '<div id="cDetalle">' + detalle + '</div>',
                                    autoHeight: true,
                                    collapsed: true,
                                    collapsible: true
                                })
                            },
                            {
                                columnWidth: 1,
                                items: {
                                    xtype: 'panel',
                                    autoHeight: true,
                                    html: '<div>Oprima <b>Cancelar</b> para abortar o <b>Aceptar</b> para proseguir con la operaci&oacute;n</div>'
                                }
                            }
                        ]
                    }),
                    buttons: [
                        {
                            text: 'Cancelar',
                            handler: function () {
                                winConfirmDetalles.hide();
                            }
                        },
                        {
                            text: 'Aceptar',
                            handler: function () {
                                winConfirmDetalles.hide();
                                if (typeof(fn) == 'function')fn();
                            }
                        }
                    ]
                });
                winConfirmDetalles.show(Ext.getBody());
                document.getElementById("pMSGcd").innerHTML = msg;
                document.getElementById("cDetalle").innerHTML = detalle;
                break;
            case 3:
                Ext.MessageBox.show({
                    title: title[tipo - 1],
                    msg: msg,
                    buttons: buttons[tipo - 1],
                    icon: icons[tipo - 1],
                    fn: fn
                });
                break;
            default:
                if (fn !== null && (typeof(fn) == 'function' || typeof(fn) == 'object'))
                    Ext.MessageBox.show({
                        title: title[tipo - 1],
                        msg: msg,
                        buttons: buttons[tipo - 1],
                        icon: icons[tipo - 1],
                        fn: fn
                    });
                else
                    Zeolides.Utils.NotificationBox.show(msg);
        }
    }

    Ext.ns('Zeolides.Utils.MessageBox');
    Zeolides.Utils.MessageBox.show = mostrarMensaje;

    Ext.ns('UCID.acciones');
    UCID.acciones = {};
    UCID.portal.cargarAcciones = function (idFuncionalidad, fn) {
        Ext.Ajax.request({
            url: 'cargaracciones',
            method: 'POST',
            params: {
                idfuncionalidad: idFuncionalidad
            },
            callback: function (options, success, response) {
                if (success) {
                    UCID.acciones[idFuncionalidad] = {};
                    acciones_reportes = Ext.decode(response.responseText);
                    var codMsg = acciones_reportes.codMsg;
                    if (!codMsg) {
                        for (i in acciones_reportes) {
                            if (i != 'remove' && Ext.getCmp(acciones_reportes[i].abreviatura)) {
                                UCID.acciones[idFuncionalidad][acciones_reportes[i].abreviatura] = true;
                                if (Ext.getCmp(acciones_reportes[i].abreviatura).getXType() == 'datefield' || Ext.getCmp(acciones_reportes[i].abreviatura).getXType() == 'textfield')
                                    Ext.getCmp(acciones_reportes[i].abreviatura).enable();
                                else
                                    Ext.getCmp(acciones_reportes[i].abreviatura).show();
                            }
                        }
                        if (fn)
                            fn();
                    }
                }
            }
        });
    }

    UCID.portal.getAccionesById = function (idFuncionalidad, identidad, fn) {
        Ext.Ajax.request({
            url: 'cargaraccionesbyid',
            method: 'POST',
            params: {
                idfuncionalidad: idFuncionalidad,
                identidad: identidad

            },
            callback: function (options, success, response) {
                if (success) {
                    UCID.acciones[idFuncionalidad] = {};
                    acciones_reportes = Ext.decode(response.responseText);
                    var codMsg = acciones_reportes.codMsg;
                    if (!codMsg) {
                        for (i in acciones_reportes) {
                            if (i != 'remove' && Ext.getCmp(acciones_reportes[i].abreviatura)) {
                                UCID.acciones[idFuncionalidad][acciones_reportes[i].abreviatura] = true;
                                if (Ext.getCmp(acciones_reportes[i].abreviatura).getXType() == 'datefield' || Ext.getCmp(acciones_reportes[i].abreviatura).getXType() == 'textfield')
                                    Ext.getCmp(acciones_reportes[i].abreviatura).enable();
                                else
                                    Ext.getCmp(acciones_reportes[i].abreviatura).show();
                            }
                        }
                        if (fn)
                            fn();
                    }
                }
            }
        });
    }

    /*------------------------------------------------------------------------*/
    /*-------------Otra variante para el uso de IFRAME*/
    UCID.portal.cargarAccionesXUri = function (uri, fn) {
        Ext.Ajax.request({
            url: 'cargarAccionesXUri',
            method: 'POST',
            params: {
                uri: uri
            },
            callback: function (options, success, response) {
                if (success) {
                    acciones_reportes = Ext.decode(response.responseText);
                    var codMsg = acciones_reportes.codMsg;
                    if (!codMsg) {
                        for (i in acciones_reportes) {
                            if (i != 'remove' && Ext.getCmp(acciones_reportes[i].abreviatura)) {
                                if (Ext.getCmp(acciones_reportes[i].abreviatura).getXType() == 'datefield' || Ext.getCmp(acciones_reportes[i].abreviatura).getXType() == 'textfield')
                                    Ext.getCmp(acciones_reportes[i].abreviatura).enable();
                                else
                                    Ext.getCmp(acciones_reportes[i].abreviatura).show();
                            }
                        }
                        if (fn)
                            fn();
                    }
                }
            }
        });
    }

    UCID.portal.getAccionesReportes = function () {
        return acciones_reportes;
    }
    Ext.Ajax.on('requestexception', function (conn, response, options) {
        Ext.Ajax.suspendEvents();
        var answer = Ext.decode(response.responseText);
        Ext.Msg.show({
            title: 'Error',
            msg: answer.msg,
            buttons: Ext.MessageBox.OK,
            fn: function () {
                var wCfg = 'titlebar=yes,status=yes,resizable=no,width=' + screen.width + ',height=' + screen.height;
                window.open('/portal/index.php/portal/login', 'portaLogin', wCfg);
            },
            icon: Ext.MessageBox.ERROR
        });
    }, this);
}//Endofif


Ext.override(Ext.Button, {
    initComponent: Ext.Button.prototype.initComponent.createInterceptor(function () {
        if (this.seguridad) {
            this.hidden = true;
        }
    }, this)
});
