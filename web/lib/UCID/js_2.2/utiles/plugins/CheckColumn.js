Ext.grid.CheckColumn = function(config){
    this.EVENT_MANAGER = new Ext.util.Observable();
    this.EVENT_MANAGER.addEvents({
        "checkchange"	:true
    });
    Ext.apply(this, config);
    if(!this.id){
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },
    onMouseDown : function(e, t){
        if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1 && !this.disabled){            
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            var record = this.grid.store.getAt(index);            
            record.set(this.dataIndex, !record.data[this.dataIndex]);
            this.grid.getSelectionModel().selectRecords([record]);
            this.EVENT_MANAGER.fireEvent('checkchange',record,record.data[this.dataIndex]);            
        }
    },

    on:function(evt_name,fn,scope){
        this.EVENT_MANAGER.on(evt_name,fn,scope);
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
    }
};