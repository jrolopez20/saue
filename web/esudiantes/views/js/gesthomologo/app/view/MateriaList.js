Ext.define('GestHom.view.MateriaList', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.materialist',
    id:'materialist',
    //title: perfil.etiquetas.lbTtlLista,
    store: 'GestHom.store.Materias',
    // selModel: selModel = Ext.create('Ext.selection.RowModel', {
    //     id: 'idSelectionNotaGrid',
    //     mode: 'SINGLE'
    // }),
    columns: [
        { dataIndex: 'idalumno', hidden: true, hideable: false},
        { dataIndex: 'idpd', hidden: true},
        { dataIndex: 'idmateriahomo', hidden: true},
        { dataIndex: 'iduniversidad', hidden: true},
        { dataIndex: 'idmateria', hidden: true},
        { header: 'Código', dataIndex: 'codigo'},
        { header: 'Materia', dataIndex: 'descripcion', flex: 1},
        { header: 'Cod. homologada'},
        { header: 'Materia homologada',flex: 1},
        { header: 'Nota',flex: 1},
    ],
    columnLines : true, 
    
    selType: 'checkboxmodel',

    initComponent: function () {
        var me = this;

       // me.selModel.on("selectionchange", me.manejarBotones, me);
        me.bbar = [Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            id:"paginator",
            store: me.store
        }),
        "->",
        {
            xtype:'button',
            text:'Homologar',
            action:'homologar',
            icon: perfil.dirImg + 'avanzada.png',
            iconCls: 'btn',
        }
        ];


        me.callParent(arguments);
    },

});