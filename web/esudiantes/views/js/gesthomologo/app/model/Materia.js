Ext.define('GestHom.model.Materia', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idmateriahomo', type: 'int'},
        {name: 'idmateria', type: 'int'},
        {name: 'descripcion',   type: 'string'},
        {name: 'codigo', mapping:'codmateria',    type: 'string'},
        {name: 'materiahomo',   type: 'string'}
    ]
})