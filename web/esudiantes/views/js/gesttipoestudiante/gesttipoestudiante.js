var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gesttipoestudiante', function () {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

function cargarInterfaz() {
    ////------------ Botones ------------////
    var btnAdicionarTipoEstudiante = Ext.create('Ext.Button', {
        id: 'btnAdicionarTipoEstudiante',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormTipoEstudiante('add');
        }
    });
    var btnModificarTipoEstudiante = Ext.create('Ext.Button', {
        id: 'btnModificarTipoEstudiante',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormTipoEstudiante('mod');
        }
    });
    var btnEliminarTipoEstudiante = Ext.create('Ext.Button', {
        id: 'btnEliminarTipoEstudiante',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarTipoEstudiante();
        }
    });

    var txtBuscarDocRequerido = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'desctipoestudiante',
        enableKeyEvents: true
    });

    txtBuscarDocRequerido.on('keyup', function (tf) {
        if (tf.getValue()) {
            stGpTipoEstudiante.clearFilter();
            stGpTipoEstudiante.filter('descripcion', tf.getValue());
        } else
            stGpTipoEstudiante.clearFilter();
    }, this)

    var btnBuscarTipoEstudiante = Ext.create('Ext.Button', {
        id: 'btnBuscarTipoEstudiante',
        disabled: true,
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });

    var separador = new Ext.menu.Separator();

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpTipoEstudiante = new Ext.data.Store({
        fields: [
            {
                name: 'idtipoalumno'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'fecha'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'estado'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarTipoEstudiantes',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarTipoEstudiante.setDisabled(smodel.getCount() == 0);
                btnEliminarTipoEstudiante.enable(smodel.getCount() > 0);
            }
        }
    });

    var GpTipoEstudiante = new Ext.grid.GridPanel({
        store: stGpTipoEstudiante,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'idtipoalumno',
                flex: 1,
                dataIndex: 'idtipoalumno',
                hidden: true
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            }
        ],
        region: 'center',
        tbar: [btnAdicionarTipoEstudiante, btnModificarTipoEstudiante, btnEliminarTipoEstudiante, separador, txtBuscarDocRequerido, btnBuscarTipoEstudiante],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux99',
            store: stGpTipoEstudiante,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });
    stGpTipoEstudiante.load();

    var general = Ext.create('Ext.panel.Panel', { layout: 'border', items: [GpTipoEstudiante]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarTipoEstudiante;
    var winModificarTipoEstudiante;

    var formTipoEstudiante = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Descripción',
                name: 'descripcion',
                anchor: '96%',
                id: 'descripcion',
                allowBlank: false,
                maskRe: /[A-Z a-z]/,
                tabIndex: 1
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true
            }
        ]
    });

    function mostFormTipoEstudiante(opcion) {
        switch (opcion) {
            case 'add':
            {
                winAdicionarTipoEstudiante = Ext.create('Ext.Window', {
                    title: perfil.etiquetas.lbTitVentanaTitI,
                    closeAction: 'hide',
                    width: 300,
                    height: 200,
                    modal: true,
                    constrain: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            tabIndex: 13,
                            handler: function () {
                                winAdicionarTipoEstudiante.hide();
                            }
                        },
                        {
                            text: 'Aplicar',
                            icon: perfil.dirImg + 'aplicar.png',
                            tabIndex: 12,
                            handler: function () {
                                AdicionarTipoEstudiante('apl');
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            tabIndex: 11,
                            handler: function () {
                                AdicionarTipoEstudiante();
                                winAdicionarTipoEstudiante.hide();
                            }
                        }
                    ]
                });
                formTipoEstudiante.getForm().reset();
                winAdicionarTipoEstudiante.add(formTipoEstudiante);
                winAdicionarTipoEstudiante.show();
            }
                break;
            case 'mod':
            {
                winModificarTipoEstudiante = Ext.create('Ext.Window', {
                    title: perfil.etiquetas.lbTitVentanaTitII,
                    closeAction: 'hide',
                    width: 300,
                    height: 200,
                    constrain: true,
                    modal: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            handler: function () {
                                winModificarTipoEstudiante.hide();
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            handler: function () {
                                ModificarTipoEstudiante();
                                winModificarTipoEstudiante.hide();
                            }
                        }
                    ]
                });

                formTipoEstudiante.getForm().reset();
                winModificarTipoEstudiante.add(formTipoEstudiante);
                winModificarTipoEstudiante.doLayout();
                winModificarTipoEstudiante.show();
                formTipoEstudiante.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }


    //funciones

    function AdicionarTipoEstudiante(apl) {
        if (formTipoEstudiante.getForm().isValid()) {
            formTipoEstudiante.getForm().submit({
                url: 'insertarTipoEstudiante',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formTipoEstudiante.getForm().reset();
                        stGpTipoEstudiante.load();
                        if (!apl)
                            winAdicionarTipoEstudiante.hide();
                        sm.clearSelections();
                        btnModificarTipoEstudiante.disable();
                        btnEliminarTipoEstudiante.disable();
                    }
                }
            });
        }
    }

    function ModificarTipoEstudiante() {
        if (formTipoEstudiante.getForm().isValid()) {
            formTipoEstudiante.getForm().submit({
                url: 'modificarTipoEstudiante',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idtipoestudiante: sm.getSelection()[0].raw.idtipoalumno
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarTipoEstudiante.hide();
                        stGpTipoEstudiante.load();
                        btnEliminarTipoEstudiante.disable();
                        btnModificarTipoEstudiante.disable();
                    }
                }
            });
        }
    }

    function EliminarTipoEstudiante() {
        mostrarMensaje(2, '¿Desea eliminar este tipo de estudiante?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando tipo de estudiante...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.idtipoalumno);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarTipoEstudiante',
                    method: 'POST',
                    params: {
                        idtipoestudiantes: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpTipoEstudiante.load()
                            btnModificarTipoEstudiante.disable();
                            btnEliminarTipoEstudiante.disable();
                        }
                    }
                });
            }
        }
    }
}
