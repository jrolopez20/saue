Ext.define('GestConv.view.StudentInfo', {
    extend: 'Ext.form.Panel',
    alias: 'widget.studentinfo',
    layout: 'column',
    border:0,
    items: [{
            xtype:'fieldset',
            columnWidth: 3/4,
            padding: '5 0 5 5',
            height:'100%',
            layout: 'vbox',
            title: 'Información del estudiante',
            items:[
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'C&oacute;digo',
                        id: 'studentCodigo',
                        value: '-'
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'Nombre',
                        id: 'studentNombre',
                        value: '-'
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'Facultad',
                        id: 'studentFacultad',
                        value: '-'
                    }, {
                        xtype: 'displayfield',
                        hidden: true,
                        fieldLabel: 'Alumno',
                        id: 'idalumno'
                    }
            ]
        },{
            columnWidth: 1/4,
            padding: '35 0 0 35',
            border:0,
            items:[
                {
                xtype:'button',
                id:'buscarEstudiantes',
                rowspan: 3,
                iconAlign:'top',
                text: "Buscar estudiante",
                icon: perfil.dirImg + 'buscar.png',
                iconCls: 'btn',
                action: 'buscar',
                         },
                    ],

                }
            ],

     initComponent: function () {
        var me = this;
        me.callParent(arguments);
    },
});