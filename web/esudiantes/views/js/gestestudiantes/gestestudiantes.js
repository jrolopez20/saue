var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestestudiantes', function() {
    cargarInterfaz();
});
////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();
function cargarInterfaz() {

    ////------------ Botones ------------////
    var btnAdicionarEstudiante = Ext.create('Ext.Button', {
        id: 'btnAgrEstudiante',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormEstudiante('add');
        }
    });
    var btnModificarEstudiante = Ext.create('Ext.Button', {
        id: 'btnModEstudiante',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormEstudiante('mod');
        }
    });
    var btnEliminarEstudiante = Ext.create('Ext.Button', {
        id: 'btnEliEstudiante',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function() {
            EliminarEstudiante();
        }
    });
    var txtBuscarEstudiante = new Ext.form.TextField({
        fieldLabel: 'Apellidos',
        labelWidth: 45,
        id: 'estudiante',
        enableKeyEvents: true
    });
    txtBuscarEstudiante.on('keyup', function(tf) {
        if (tf.getValue()) {
            stGpEstudiante.clearFilter();
            stGpEstudiante.filter('apellidos', tf.getValue());
        } else
            stGpEstudiante.clearFilter();
    }, this);
    var btnBuscarEstudiante = Ext.create('Ext.Button', {
        id: 'btnBuscarEstudiante',
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });
    var separador = new Ext.menu.Separator();
    //botones para la gestion de estudios
    var btnAdicionarEstudio = Ext.create('Ext.Button', {
        id: 'btnAdicionarEstudio',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormEstudio('add');
        }
    });
    var btnModificarEstudio = Ext.create('Ext.Button', {
        id: 'btnModificarEstudio',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormEstudio('mod');
        }
    });
    var btnEliminarEstudio = Ext.create('Ext.Button', {
        id: 'btnEliminarEstudio',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarEstudio();
        }
    });

    //cargar acciones permitidas
    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de estudiantes ------------////
    var stGpEstudiante = new Ext.data.Store({
        fields: [
            {
                name: 'alumno'
            },
            {
                name: 'id_alumnodatos'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'idestadocivil'
            },
            {
                name: 'idprovincia'
            },
            {
                name: 'idtipoalumno'
            },
            {
                name: 'idsectorciudad'
            },
            {
                name: 'iduniversidad'
            },
            {
                name: 'idcanton'
            },
            {
                name: 'idcolegio'
            },
            {
                name: 'idpensumenfasismateriatipo'
            },
            {
                name: 'idenfasis'
            },
            {
                name: 'idprovinciauniv'
            },
            {
                name: 'idprovinciacoleg'
            },
            {
                name: 'idcarrera'
            },
            {
                name: 'idestructura'
            },
            {
                name: 'nombre'
            },
            {
                name: 'apellidos'
            },
            {
                name: 'fecha'
            },
            {
                name: 'estadocivil'
            },
            {
                name: 'sexo'
            },
            {
                name: 'provincia'
            },
            {
                name: 'tipoalumno'
            },
            {
                name: 'sectorciudad'
            },
            {
                name: 'universidad'
            },
            {
                name: 'canton'
            },
            {
                name: 'colegio'
            },
            {
                name: 'codigo'
            },
            {
                name: 'fec_nacimiento'
            },
            {
                name: 'domicilio'
            },
            {
                name: 'telefono'
            },
            {
                name: 'celular'
            },
            {
                name: 'lugar_nacimiento'
            },
            {
                name: 'nacionalidad'
            },
            {
                name: 'pais'
            },
            {
                name: 'religion'
            },
            {
                name: 'e_mail'
            },
            {
                name: 'e_mail2'
            },
            {
                name: 'cedpas'
            },
            {
                name: 'enfasis'
            },
            {
                name: 'provColeg'
            },
            {
                name: 'provUniv'
            },
            {
                name: 'observaciones'
            },
            {
                name: 'cant_hijos'
            },
            {
                name: 'estado'
            },
            {
                name: 'carrera'
            },
            {
                name: 'denominacion'
            },
            {
                name: 'empresa_trab'
            },
            {
                name: 'cargo_empresa'
            },
            {
                name: 'ciudad_trabajo'
            },
            {
                name: 'direccion_trab'
            },
            {
                name: 'telefono_trab'
            },
            {
                name: 'nombre_padre'
            },
            {
                name: 'apellidos_padre'
            },
            {
                name: 'direccion_padre'
            },
            {
                name: 'telefono_padre'
            },
            {
                name: 'profesion_padre'
            },
            {
                name: 'cargo_padre'
            },
            {
                name: 'empresa_padre'
            },
            {
                name: 'nombre_madre'
            },
            {
                name: 'apellidos_madre'
            },
            {
                name: 'direccion_madre'
            },
            {
                name: 'telefono_madre'
            },
            {
                name: 'profesion_madre'
            },
            {
                name: 'cargo_madre'
            },
            {
                name: 'empresa_madre'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarEstudiantes',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid de estudiantes ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function(smodel, rowIndex, keepExisting, record) {
                btnModificarEstudiante.setDisabled(smodel.getCount() == 0);
                btnEliminarEstudiante.enable(smodel.getCount() > 0);
            }
        }
    });

    //grid de estudiantes
    var GpEstudiante = new Ext.grid.GridPanel({
        store: stGpEstudiante,
        frame: true,
        region: 'east',
        selModel: sm,
        columns: [
            {
                text: 'alumno',
                flex: 1,
                dataIndex: 'alumno',
                hidden: true
            },
            {
                text: 'id_alumnodatos',
                flex: 1,
                dataIndex: 'id_alumnodatos',
                hidden: true
            },
            {
                text: 'idusuario',
                flex: 1,
                dataIndex: 'idusuario',
                hidden: true
            },
            {
                text: 'idestadocivil',
                flex: 1,
                dataIndex: 'idestadocivil',
                hidden: true
            },
            {
                text: 'idprovincia',
                flex: 1,
                dataIndex: 'idprovincia',
                hidden: true
            },
            {
                text: 'idtipoalumno',
                flex: 1,
                dataIndex: 'idtipoalumno',
                hidden: true
            },
            {
                text: 'idsectorciudad',
                flex: 1,
                dataIndex: 'idsectorciudad',
                hidden: true
            },
            {
                text: 'iduniversidad',
                flex: 1,
                dataIndex: 'iduniversidad',
                hidden: true
            },
            {
                text: 'idcanton',
                flex: 1,
                dataIndex: 'idcanton',
                hidden: true
            },
            {
                text: 'idcolegio',
                flex: 1,
                dataIndex: 'idcolegio',
                hidden: true
            },
            {
                text: 'idpensumenfasismateriatipo',
                flex: 1,
                dataIndex: 'idpensumenfasismateriatipo',
                hidden: true
            },
            {
                text: 'idusuarioasig',
                flex: 1,
                dataIndex: 'idusuarioasig',
                hidden: true
            },
            {
                text: 'idEnfasis',
                flex: 1,
                dataIndex: 'idenfasis',
                hidden: true
            },
            {
                text: 'idcarrera',
                flex: 1,
                dataIndex: 'idcarrera',
                hidden: true
            },
            {
                text: 'idestructura',
                flex: 1,
                dataIndex: 'idestructura',
                hidden: true
            },
            {
                text: 'id_alumnodatos',
                flex: 1,
                dataIndex: 'id_alumnodatos',
                hidden: true
            },
            {
                text: 'Apellidos',
                flex: 1,
                dataIndex: 'apellidos'
            },
            {
                text: 'Nombre',
                flex: 1,
                dataIndex: 'nombre'
            },
            {
                text: 'Fecha',
                flex: 1,
                dataIndex: 'fecha',
                hidden: true
            },
            {
                text: 'Estado civil',
                flex: 1,
                dataIndex: 'estadocivil'
            },
            {
                text: 'Sexo',
                flex: 1,
                dataIndex: 'sexo'
            },
            {
                text: 'Provincia',
                flex: 1,
                dataIndex: 'provincia'
            },
            {
                text: 'Tipo alumno',
                flex: 1,
                dataIndex: 'tipoalumno'
            },
            {
                text: 'Sector ciudad',
                flex: 1,
                dataIndex: 'sectorciudad',
                hidden: true
            },
            {
                text: 'Universidad',
                flex: 1,
                dataIndex: 'universidad',
                hidden: true
            },
            {
                text: 'Colegio',
                flex: 1,
                dataIndex: 'colegio',
                hidden: true
            },
            {
                text: 'Fecha nacimiento',
                flex: 1,
                dataIndex: 'fec_nacimiento',
                type: 'date'
            },
            {
                text: 'Domicilio',
                flex: 1,
                dataIndex: 'domicilio',
                hidden: true
            },
            {
                text: 'Teléfono',
                flex: 1,
                dataIndex: 'telefono',
                hidden: true
            },
            {
                text: 'Celular',
                flex: 1,
                dataIndex: 'celular',
                hidden: true
            },
            {
                text: 'Lugar nacimiento',
                flex: 1,
                dataIndex: 'lugar_nacimiento',
                hidden: true
            },
            {
                text: 'Nacionalidad',
                flex: 1,
                dataIndex: 'nacionalidad',
                hidden: true
            },
            {
                text: 'País',
                flex: 1,
                dataIndex: 'pais',
                hidden: true
            },
            {
                text: 'Religión',
                flex: 1,
                dataIndex: 'religion',
                hidden: true
            },
            {
                text: 'Correo',
                flex: 1,
                dataIndex: 'e_mail',
                hidden: true
            },
            {
                text: 'Correo (alternativo)',
                flex: 1,
                dataIndex: 'e_mail2',
                hidden: true
            },
            {
                text: 'Cedula/Pasaporte',
                flex: 1,
                dataIndex: 'cedpas',
                hidden: true
            },
            {
                text: 'Enfasis',
                flex: 1,
                dataIndex: 'enfasis',
                hidden: true
            },
            {
                text: 'Carrera',
                flex: 1,
                dataIndex: 'carrera',
                hidden: true
            },
            {
                text: 'denominacion',
                flex: 1,
                dataIndex: 'denominacion',
                hidden: true
            },
            {
                text: 'Estado',
                flex: 1,
                dataIndex: 'estado',
                hidden: true
            },
            {
                text: 'Empresa',
                flex: 1,
                dataIndex: 'empresa_trab',
                hidden: true
            },
            {
                text: 'Cargo',
                flex: 1,
                dataIndex: 'cargo_empresa',
                hidden: true
            },
            {
                text: 'Ciudad',
                flex: 1,
                dataIndex: 'ciudad_trabajo',
                hidden: true
            },
            {
                text: 'Direccion',
                flex: 1,
                dataIndex: 'direccion_trab',
                hidden: true
            },
            {
                text: 'Teléfono',
                flex: 1,
                dataIndex: 'telefono_trab',
                hidden: true
            },
            {
                text: 'Nombre padre',
                flex: 1,
                dataIndex: 'nombre_padre',
                hidden: true
            },
            {
                text: 'Apellidos padre',
                flex: 1,
                dataIndex: 'apellidos_padre',
                hidden: true
            },
            {
                text: 'Dirección padre',
                flex: 1,
                dataIndex: 'direccion_padre',
                hidden: true
            },
            {
                text: 'Teléfono padre',
                flex: 1,
                dataIndex: 'telefono_padre',
                hidden: true
            },
            {
                text: 'Profesión padre',
                flex: 1,
                dataIndex: 'profesion_padre',
                hidden: true
            },
            {
                text: 'Cargo padre',
                flex: 1,
                dataIndex: 'cargo_padre',
                hidden: true
            },
            {
                text: 'Empresa padre',
                flex: 1,
                dataIndex: 'empresa_padre',
                hidden: true
            },
            {
                text: 'Nombre madre',
                flex: 1,
                dataIndex: 'nombre_madre',
                hidden: true
            },
            {
                text: 'Apellidos madre',
                flex: 1,
                dataIndex: 'apellidos_madre',
                hidden: true
            },
            {
                text: 'Dirección madre',
                flex: 1,
                dataIndex: 'direccion_madre',
                hidden: true
            },
            {
                text: 'Teléfono madre',
                flex: 1,
                dataIndex: 'telefono_madre',
                hidden: true
            },
            {
                text: 'Profesión madre',
                flex: 1,
                dataIndex: 'profesion_madre',
                hidden: true
            },
            {
                text: 'Cargo madre',
                flex: 1,
                dataIndex: 'cargo_madre',
                hidden: true
            },
            {
                text: 'Empresa madre',
                flex: 1,
                dataIndex: 'empresa_madre',
                hidden: true
            }
        ],
        tbar: [btnAdicionarEstudiante, btnModificarEstudiante, btnEliminarEstudiante, separador, txtBuscarEstudiante, btnBuscarEstudiante],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux99',
            store: stGpEstudiante,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });

    //paneles para el grid de estudiante y para los reportes
    var pnlEstudiante = Ext.create('Ext.Panel', {
        id: 'pnlEstudiante',
        frame: true,
        layout: 'fit',
        region: 'center',
        width: 500,
        items: GpEstudiante
    });
    var pnlReportesEstudiante = Ext.create('Ext.Panel', {
        id: 'pnlReportesEstudiante',
        frame: true,
        //layout: 'fit',
        region: 'east',
        items: [
            {
                xtype: 'button',
                text: 'Certificado IECE',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Cuadro de promedios',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Récord académico',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Récord académico Inglés',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Notas y faltas',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Consejería académica',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Materias aprobadas',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Materias convalidadas',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Materias homologadas',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Materias Validadas',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Materias Validadas Inst.',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            },
            {
                xtype: 'button',
                text: 'Estado de cuenta',
                width: '100%',
                renderTo: Ext.getBody(),
                handler: function() {
                    alert('You clicked the button!');
                }
            }
        ]
    });
    var pnl1 = Ext.create('Ext.Panel', {
        title: 'Reportes',
        id: 'reportes_Est',
        frame: true,
        layout: 'fit',
        region: 'east',
        collapsed: true,
        collapsible: true,
        width: 200,
        items: [pnlReportesEstudiante]
    });

    //elementos del formulario
    var stcmbEstadocivil = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idestadocivil'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarEstadoCivil',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbEstadocivil = Ext.create('Ext.form.field.ComboBox', {
        id: 'idestadocivil',
        name: 'idestadocivil',
        fieldLabel: 'Estado civil',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbEstadocivil,
        //emptyText: '--seleccione--',
        queryMode: 'remote',
        typeAhead: true,
        displayField: 'descripcion',
        valueField: 'idestadocivil',
        style: {
            marginTop: '10px'
        }
    });
    var stcmbProvincia = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idprovincia'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarProvincias',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbProvinciaC = Ext.create('Ext.form.field.ComboBox', {
        id: 'idprovinciacoleg',
        name: 'idprovinciacoleg',
        fieldLabel: 'Provincia',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbProvincia,
        //emptyText: '--seleccione--',
        queryMode: 'remote',
        typeAhead: true,
        displayField: 'descripcion',
        valueField: 'idprovincia',
        allowBlank: false,
        style: {
            marginTop: '6px'
        },
        tabIndex:18
    });
    var cmbProvinciaUni = Ext.create('Ext.form.field.ComboBox', {
        id: 'idprovinciauniv',
        name: 'idprovinciauniv',
        fieldLabel: 'Provincia',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbProvincia,
        //emptyText: '--seleccione--',
        queryMode: 'remote',
        typeAhead: true,
        displayField: 'descripcion',
        valueField: 'idprovincia',
        allowBlank: false,
        style: {
            marginTop: '6px'
        },
        tabIndex:20
    });
    var cmbProvinciaAdic = Ext.create('Ext.form.field.ComboBox', {
        id: 'idprovinciaAdic',
        name: 'idprovincia',
        fieldLabel: 'Provincia',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbProvincia,
        //emptyText: '--seleccione--',
        queryMode: 'remote',
        typeAhead: true,
        displayField: 'descripcion',
        valueField: 'idprovincia',
        style: {
            marginTop: '10px'
        }
    });
    var stcmbTipoestudiante = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idtipoalumno'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '../gesttipoestudiante/cargarTipoEstudiantes',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbTipoestudiante = Ext.create('Ext.form.field.ComboBox', {
        id: 'idtipoalumno',
        name: 'idtipoalumno',
        fieldLabel: 'Tipo alumno',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbTipoestudiante,
        //emptyText: '--seleccione--',
        queryMode: 'remote',
        typeAhead: true,
        allowBlank: false,
        displayField: 'descripcion',
        valueField: 'idtipoalumno',
        style: {
            marginTop: '11px'
        },
        tabIndex:5
    });
    var stcmbEnfasis = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idenfasis'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarEnfasis',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbEnfasis = Ext.create('Ext.form.field.ComboBox', {
        id: 'idenfasis',
        name: 'idenfasis',
        fieldLabel: 'Énfasis',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbEnfasis,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idenfasis',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        disabled: true,
        style: {
            marginTop: '11px'
        },
        tabIndex:23
    });
    var stcmbCarreras = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idcarrera'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarCarreras',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbCarreras = Ext.create('Ext.form.field.ComboBox', {
        id: 'idcarrera',
        name: 'idcarrera',
        fieldLabel: 'Carrera',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbCarreras,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idcarrera',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        disabled: true,
        style: {
            marginTop: '11px'
        },
        tabIndex:22
    });
    var stcmbFacultades = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idestructura'
            },
            {
                name: 'denominacion'
            },
            {
                name: 'abreviatura'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarFacultades',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbFacultades = Ext.create('Ext.form.field.ComboBox', {
        id: 'idestructura',
        name: 'idestructura',
        fieldLabel: 'Facultad',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbFacultades,
        //emptyText: '--seleccione--',
        displayField: 'denominacion',
        valueField: 'idestructura',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        tabIndex:21
    });
    var cmbPeriodo = Ext.create('Ext.form.field.ComboBox', {
        id: 'idperiodo',
        name: 'idperiodo',
        fieldLabel: 'Periodo',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        //store: stcmbFacultades,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idperiodo',
        queryMode: 'local',
        typeAhead: true,
        //allowBlank: false,
        style: {
            marginTop: '11px'
        },
        tabIndex:8
    });
    var stcmbSector = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idsectorciudad'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarSectores',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbSector = Ext.create('Ext.form.field.ComboBox', {
        id: 'idsectorciudad',
        name: 'idsectorciudad',
        fieldLabel: 'Sector',
        editable: false,
        anchor: '95%',
        labelWidth: 130,
        store: stcmbSector,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idsectorciudad',
        queryMode: 'remote',
        typeAhead: true,
        allowBlank: false,
        style: {
            marginTop: '6px'
        },
        tabIndex:12
    });
    var stcmbPensum = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idpensum'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        //autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarPensums',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbPensum = Ext.create('Ext.form.field.ComboBox', {
        id: 'idpensum',
        name: 'idpensum',
        fieldLabel: 'Pensum',
        editable: true,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbPensum,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idpensum',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        disabled: true,
        style: {
            marginTop: '11px'
        },
        tabIndex:24
    });
    var stcmbColegios = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idcolegio'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarColegios',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbColegio = Ext.create('Ext.form.field.ComboBox', {
        id: 'idcolegio',
        name: 'idcolegio',
        fieldLabel: 'Colegio',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbColegios,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idcolegio',
        queryMode: 'remote',
        typeAhead: true,
        allowBlank: false,
        tabIndex:16
    });
    var stcmbUniversidad = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'iduniversidad'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarUniversidades',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbUniversidad = Ext.create('Ext.form.field.ComboBox', {
        id: 'iduniversidad',
        name: 'iduniversidad',
        fieldLabel: 'Universidad',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbUniversidad,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'iduniversidad',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        style: {
            marginTop: '6px'
        },
        tabIndex:19
    });
    cmbFacultades.on('change', function() {
        stcmbCarreras.load({params: {idfacultad: cmbFacultades.getValue()}});
        cmbCarreras.enable();
    });
    cmbCarreras.on('change', function() {
        stcmbEnfasis.load({params: {idcarrera: cmbCarreras.getValue()}});
        stcmbPensum.load({params: {idcarrera: cmbCarreras.getValue()}});
        cmbPensum.enable();
        cmbEnfasis.enable();
    });
    var stcmbAnno = Ext.data.SimpleStore({
        fields: ["anno"],
        data: [
            {"anno": '1999'},
            {"anno": '2000'},
            {"anno": '2001'},
            {"anno": '2002'},
            {"anno": '2003'},
            {"anno": '2004'},
            {"anno": '2005'},
            {"anno": '2006'},
            {"anno": '2007'},
            {"anno": '2008'},
            {"anno": '2009'},
            {"anno": '2010'},
            {"anno": '2011'},
            {"anno": '2012'},
            {"anno": '2013'},
            {"anno": '2014'}
        ]
    });

    //documentos requeridos
    ////------------ Store del Grid de documentos requeridos para un estudiante ------------////
    var stGpDocRequeridoEstud = new Ext.data.Store({
        fields: [
            {
                name: 'iddocumentorequerido'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'fecha'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'estado'
            },
            {
                name: 'checked',
                mapping: 'checked'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: '../gestdocxestudiante/cargarDocRequeridos',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid de documentos requeridos para un estudiante ------------////
    var smDocRequeridosEstud = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function(smodel, rowIndex, keepExisting, record) {
                btnEliminarDocRequerido.enable(smodel.getCount() > 0);
            }
        }
    });

    //grid de documentos requeridos segun el estudiante que tengo seleccionado, para el adicionar no carga nada este grid
    var GpDocRequeridoEstud = new Ext.grid.GridPanel({
        store: stGpDocRequeridoEstud,
        frame: true,
        layout: 'fit',
        selModel: smDocRequeridosEstud,
        columns: [
            {
                text: 'iddocumentorequerido',
                flex: 1,
                dataIndex: 'iddocumentorequerido',
                hidden: true
            },
            {
                text: '',
                dataIndex: 'checked',
                xtype: 'checkcolumn',
                width: 25
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            }
        ],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux7',
            store: stGpDocRequeridoEstud,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });

    //menciones
    ////------------ Store del Grid de menciones de un estudiante ------------////
    var stGpMencionesEstud = new Ext.data.Store({
        fields: [
            {
                name: 'idmension'
            },
            {
                name: 'idfacultad'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'fecha'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'estado'
            },
            {
                name: 'facultad'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarMenciones',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid de menciones de un estudiante------------////
    var smMencionesEstud = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function(smodel, rowIndex, keepExisting, record) {
                //btnEliminarDocRequerido.enable(smodel.getCount() > 0);
            }
        }
    });

    //grid de menciones segun el estudiante que tengo seleccionado, para el adicionar no carga nada este grid
    var GpMencionesEstud = new Ext.grid.GridPanel({
        store: stGpMencionesEstud,
        frame: true,
        layout: 'fit',
        selModel: smMencionesEstud,
        columns: [
            {
                text: 'idmension',
                flex: 1,
                dataIndex: 'idmension',
                hidden: true
            },
            {
                text: 'idfacultad',
                flex: 1,
                dataIndex: 'idfacultad',
                hidden: true
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            },
            {
                text: 'Facultad',
                flex: 1,
                dataIndex: 'facultad'
            }
        ],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux10',
            store: stGpMencionesEstud,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });

////------------ Store del Grid de estudios ------------////
    var stGpEstudio = new Ext.data.Store({
        fields: [
            {
                name: 'idalumno'
            },
            {
                name: 'idfacultad'
            },
            {
                name: 'idcarrera'
            },
            {
                name: 'idenfasis'
            },
            {
                name: 'facultad'
            },
            {
                name: 'enfasis'
            },
            {
                name: 'carrera'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarEstudios',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var smEstudios = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarEstudio.setDisabled(smodel.getCount() == 0);
                btnEliminarEstudio.setDisabled(smodel.getCount() == 0);
            }
        }
    });

    //---------------grid de estudios-----------------------////
    var GpEstudios = new Ext.grid.GridPanel({
        store: stGpEstudio,
        frame: true,
        region: 'center',
        selModel: smEstudios,
        columns: [
            {
                text: 'idalumno',
                flex: 1,
                dataIndex: 'idalumno',
                hidden: true
            },
            {
                text: 'idfacultad',
                flex: 1,
                dataIndex: 'idfacultad',
                hidden: true
            },{
                text: 'idcarrera',
                flex: 1,
                dataIndex: 'idcarrera',
                hidden: true
            },{
                text: 'idenfasis',
                flex: 1,
                dataIndex: 'idenfasis',
                hidden: true
            },
            {
                text: 'Facultad',
                flex: 1,
                dataIndex: 'facultad'
            },{
                text: 'Carrera',
                flex: 1,
                dataIndex: 'carrera'
            },{
                text: 'Énfasis',
                flex: 1,
                dataIndex: 'enfasis'
            }
        ],
        tbar: [btnAdicionarEstudio, btnModificarEstudio, btnEliminarEstudio],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux65',
            store: stGpEstudio,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });

    //formulario estudiantes
    var winAdicionarEstudiante;
    var winModificarEstudiante;
    var formEstudiantes = Ext.create('Ext.form.Panel', {
        frame: true,
        layout: 'fit',
        bodyStyle: 'padding:5px auto 0px',
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'tabpanel',
                plain: true,
                id: 'tbpnlEstudiantes',
                layout: 'fit',
                activeTab: 0,
                listeners: {
                    beforetabchange: function(tabs, newTab, oldTab) {
                        return formEstudiantes.getForm().isValid();
                    }
                },
                items: [
                    {
                        title: 'Información principal',
                        layout: 'column',
                        id: 'tbInfPrinc',
                        items: [
                            {
                                //primera columa
                                defaults: {width: '100%'},
                                columnWidth: .54,
                                border: false,
                                bodyStyle: 'padding:5px 0 5px 5px',
                                items: [
                                    {
                                        xtype: 'fieldset',
                                        title: '<b>' + 'GENERALES' + '</b>',
                                        border: 1,
                                        style: {
                                            borderColor: 'black',
                                            borderStyle: 'solid'
                                        },
                                        defaults: {width: '100%'},
                                        items: [
                                            {
                                                xtype: 'container',
                                                anchor: '100%',
                                                layout: 'column',
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .5,
                                                        layout: 'anchor',
                                                        items: [
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Nombre (s)',
                                                                name: 'nombre',
                                                                anchor: '95%',
                                                                allowBlank: false,
                                                                maskRe: /[A-Z a-z]/,
                                                                tabIndex:1
                                                            },
                                                            {
                                                                xtype: 'container',
                                                                anchor: '100%',
                                                                layout: 'column',
                                                                items: [
                                                                    {
                                                                        xtype: 'container',
                                                                        columnWidth: .4,
                                                                        layout: 'anchor',
                                                                        items: [
                                                                            {
                                                                                id: 'sexo',
                                                                                xtype: 'combo',
                                                                                fieldLabel: 'Sexo',
                                                                                editable: false,
                                                                                allowBlank: false,
                                                                                store: Ext.create('Ext.data.Store', {
                                                                                    fields: ['idsexo', 'dsexo'],
                                                                                    data: [
                                                                                        {"idsexo": 1, "dsexo": 'Masculino'},
                                                                                        {"idsexo": 2, "dsexo": 'Femenino'}
                                                                                    ]
                                                                                }),
                                                                                queryMode: 'local',
                                                                                name: 'sexo',
                                                                                displayField: 'dsexo',
                                                                                valueField: 'dsexo',
                                                                                width: 80,
                                                                                style: {
                                                                                    marginTop: '6px'
                                                                                },
                                                                                tabIndex:3
                                                                            }
                                                                        ]
                                                                    },
                                                                    {
                                                                        xtype: 'container',
                                                                        columnWidth: .55,
                                                                        layout: 'anchor',
                                                                        items: [
                                                                            {
                                                                                xtype: 'datefield',
                                                                                name: "fec_nacimiento",
                                                                                fieldLabel: 'Fecha nacimiento',
                                                                                maxValue: new Date(),
                                                                                allowBlank: false,
                                                                                width: 123,
                                                                                editable: false,
                                                                                style: {
                                                                                    marginTop: '6px'
                                                                                },
                                                                                tabIndex:4
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                xtype: 'container',
                                                                anchor: '100%',
                                                                layout: 'column',
                                                                items: [
                                                                    {
                                                                        xtype: 'container',
                                                                        columnWidth: .4,
                                                                        layout: 'anchor',
                                                                        items: [
                                                                            {
                                                                                xtype: 'combo',
                                                                                store: stcmbAnno,
                                                                                fieldLabel: 'Año',
                                                                                name: 'anno',
                                                                                //emptyText: '--seleccione--',
                                                                                width: 80,
                                                                                id: 'anno',
                                                                                queryMode: 'local',
                                                                                valueField: 'anno',
                                                                                //allowBlank: false,
                                                                                displayField: 'anno',
                                                                                style: {
                                                                                    marginTop: '6px'
                                                                                },
                                                                                tabIndex:6
                                                                            }
                                                                        ]
                                                                    },
                                                                    {
                                                                        xtype: 'container',
                                                                        columnWidth: .55,
                                                                        layout: 'anchor',
                                                                        items: [
                                                                            {
                                                                                xtype: 'datefield',
                                                                                name: "fec_grad",
                                                                                fieldLabel: 'Fecha graduación',
                                                                                minValue: new Date(),
                                                                                //allowBlank: false,
                                                                                width: 124,
                                                                                editable: false,
                                                                                style: {
                                                                                    marginTop: '6px'
                                                                                },
                                                                                tabIndex:7
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                xtype: 'radiogroup',
                                                                id: 'radio',
                                                                columns: 2,
                                                                vertical: true,
                                                                items: [
                                                                    {id: 'radio2', boxLabel: 'Cédula', name: 'rb', inputValue: 'cedula', checked: true},
                                                                    {id: 'cedpas2', boxLabel: 'Pasaporte', name: 'rb', inputValue: 'pasaporte'}],
                                                                style: {
                                                                    marginTop: '16px'
                                                                },
                                                                tabIndex:9
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .5,
                                                        layout: 'anchor',
                                                        items: [
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Apellidos',
                                                                name: 'apellidos',
                                                                anchor: '100%',
                                                                allowBlank: false,
                                                                maskRe: /[A-Z a-z]/,
                                                                tabIndex:2
                                                            },
                                                            cmbTipoestudiante,
                                                            cmbPeriodo,
                                                            {
                                                                id: 'cedpas',
                                                                xtype: 'textfield',
                                                                name: 'cedpas',
                                                                allowBlank: false,
                                                                anchor: '100%',
                                                                style: {
                                                                    marginTop: '28px'
                                                                },
                                                                tabIndex:10
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'fieldset',
                                        title: '<b>' + 'CONTACTO' + '</b>',
                                        border: 1,
                                        style: {
                                            borderColor: 'black',
                                            borderStyle: 'solid'
                                        },
                                        defaults: {width: '100%'},
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Dirección',
                                                name: 'domicilio',
                                                anchor: '100%',
                                                id: 'domicilio',
                                                allowBlank: false,
                                                tabIndex:11
                                            },
                                            {
                                                xtype: 'container',
                                                anchor: '100%',
                                                layout: 'column',
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .6,
                                                        layout: 'anchor',
                                                        items: [
                                                            cmbSector
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .4,
                                                        layout: 'anchor',
                                                        items: [
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Correo I',
                                                                name: 'e_mail',
                                                                anchor: '100%',
                                                                id: 'e_mail',
                                                                style: {
                                                                    marginTop: '6px'
                                                                },
                                                                tabIndex:13
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                anchor: '100%',
                                                layout: 'column',
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .5,
                                                        layout: 'anchor',
                                                        items: [
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Teléfono casa',
                                                                name: 'telefono',
                                                                anchor: '90%',
                                                                id: 'telefono',
                                                                style: {
                                                                    marginTop: '6px'
                                                                },
                                                                tabIndex:14
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .5,
                                                        layout: 'anchor',
                                                        items: [
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Teléfono celular',
                                                                name: 'celular',
                                                                anchor: '100%',
                                                                id: 'celular',
                                                                style: {
                                                                    marginTop: '6px'
                                                                },
                                                                tabIndex:15
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Observaciones',
                                        name: 'observ',
                                        anchor: '100%',
                                        id: 'observ',
                                        tabIndex:25
                                    }
                                ]
                            },
                            {
                                //segunda columa
                                defaults: {width: '100%'},
                                columnWidth: .45,
                                border: false,
                                bodyStyle: 'padding:5px 0 5px 10px',
                                items: [
                                    {
                                        xtype: 'fieldset',
                                        title: '<b>' + 'PROCEDENCIA' + '</b>',
                                        border: 1,
                                        style: {
                                            borderColor: 'black',
                                            borderStyle: 'solid'
                                        },
                                        defaults: {width: '100%'},
                                        items: [
                                            cmbColegio,
                                            {
                                                xtype: 'container',
                                                anchor: '100%',
                                                layout: 'column',
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .4,
                                                        layout: 'anchor',
                                                        items: [
                                                            {
                                                                xtype: 'combo',
                                                                store: stcmbAnno,
                                                                fieldLabel: 'Año',
                                                                name: 'annoC',
                                                                //emptyText: '--seleccione--',
                                                                anchor: '90%',
                                                                id: 'annoC',
                                                                queryMode: 'local',
                                                                valueField: 'anno',
                                                                //allowBlank: false,
                                                                displayField: 'anno',
                                                                style: {
                                                                    marginTop: '6px'
                                                                },
                                                                tabIndex:17
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .6,
                                                        layout: 'anchor',
                                                        items: [
                                                            cmbProvinciaC
                                                        ]
                                                    }
                                                ]
                                            },
                                            cmbUniversidad,
                                            {
                                                xtype: 'container',
                                                anchor: '100%',
                                                layout: 'column',
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        columnWidth: .6,
                                                        layout: 'anchor',
                                                        items: [
                                                            cmbProvinciaUni
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'fieldset',
                                        title: '<b>' + 'UBICACIÓN DOCENTE' + '</b>',
                                        border: 1,
                                        style: {
                                            borderColor: 'black',
                                            borderStyle: 'solid'
                                        },
                                        defaults: {width: '100%'},
                                        items: [cmbFacultades, cmbCarreras, cmbEnfasis, cmbPensum]
                                    },
                                    {
                                        xtype: 'checkbox',
                                        fieldLabel: 'Activado:',
                                        name: 'estado',
                                        labelAlign: 'left',
                                        style: {
                                            marginTop: '6px'
                                        },
                                        checked: true,
                                        tabIndex:26
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Información adicional',
                        bodyStyle: 'padding:5px',
                        id: 'tbInfAdic',
                        items: [
                            {
                                xtype: 'container',
                                anchor: '100%',
                                layout: 'column',
                                items: [
                                    {
                                        defaults: {width: '100%'},
                                        columnWidth: .33,
                                        border: false,
                                        bodyStyle: 'padding:15px 0 10px 5px',
                                        items: [
                                            {
                                                xtype: 'fieldset',
                                                title: '<b>' + 'GENERALES' + '</b>',
                                                border: 1,
                                                style: {
                                                    borderColor: 'black',
                                                    borderStyle: 'solid'
                                                },
                                                defaults: {width: '100%'},
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Correo (alternativo)',
                                                        name: 'e_mail2',
                                                        anchor: '100%',
                                                        id: 'e_mail2'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Religión',
                                                        name: 'religion',
                                                        anchor: '100%',
                                                        id: 'religion',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    },
                                                    cmbEstadocivil,
                                                    {
                                                        xtype: 'numberfield',
                                                        fieldLabel: 'Hijos',
                                                        name: 'hijos',
                                                        anchor: '50%',
                                                        id: 'hijos',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        defaults: {width: '100%'},
                                        columnWidth: .33,
                                        border: false,
                                        bodyStyle: 'padding:15px 0 10px 5px',
                                        items: [
                                            {
                                                xtype: 'fieldset',
                                                title: '<b>' + 'CONTACTO' + '</b>',
                                                border: 1,
                                                style: {
                                                    borderColor: 'black',
                                                    borderStyle: 'solid'
                                                },
                                                defaults: {width: '90%'},
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Lugar de nacimiento',
                                                        name: 'lugar_nacimiento',
                                                        anchor: '100%',
                                                        id: 'lugar_nacimiento'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nacionalidad',
                                                        name: 'nacionalidad',
                                                        anchor: '100%',
                                                        id: 'nacionalidad',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'País',
                                                        name: 'pais',
                                                        anchor: '100%',
                                                        id: 'pais',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    },
                                                    cmbProvinciaAdic
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        defaults: {width: '100%'},
                                        columnWidth: .34,
                                        border: false,
                                        bodyStyle: 'padding:15px 0 10px 5px',
                                        items: [
                                            {
                                                xtype: 'fieldset',
                                                title: '<b>' + 'UBICACIÓN LABORAL' + '</b>',
                                                border: 1,
                                                style: {
                                                    borderColor: 'black',
                                                    borderStyle: 'solid'
                                                },
                                                defaults: {width: '90%'},
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Empresa',
                                                        name: 'empresa_trab',
                                                        anchor: '100%',
                                                        id: 'empresa'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Dirección empresa',
                                                        name: 'direccion_trab',
                                                        anchor: '100%',
                                                        id: 'direccion_trab',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Teléfono empresa',
                                                        name: 'telefono_trab',
                                                        anchor: '100%',
                                                        id: 'telefono_trab',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Cargo',
                                                        name: 'cargo_empresa',
                                                        anchor: '100%',
                                                        id: 'cargo',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Ciudad',
                                                        name: 'ciudad_trabajo',
                                                        anchor: '100%',
                                                        id: 'ciudad',
                                                        style: {
                                                            marginTop: '10px'
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }]
                            }
                        ]
                    },
                    {
                        title: 'Datos familiares',
                        bodyStyle: 'padding:5px',
                        id: 'tbDatosFam',
                        items: [{
                                xtype: 'container',
                                anchor: '100%',
                                layout: 'column',
                                items: [
                                    {
                                        defaults: {width: '100%'},
                                        columnWidth: .46,
                                        border: false,
                                        bodyStyle: 'padding:15px 0 5px 70px',
                                        items: [
                                            {
                                                xtype: 'fieldset',
                                                title: '<b>' + 'DATOS DEL PADRE' + '</b>',
                                                border: 1,
                                                style: {
                                                    borderColor: 'black',
                                                    borderStyle: 'solid'
                                                },
                                                defaults: {width: '100%'},
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nombre (s)',
                                                        name: 'nombre_padre',
                                                        anchor: '100%',
                                                        maskRe: /[A-Z a-z]/
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Apellidos',
                                                        name: 'apellidos_padre',
                                                        anchor: '100%',
                                                        maskRe: /[A-Z a-z]/
                                                    },
                                                    {
                                                        xtype: 'textarea',
                                                        fieldLabel: 'Dirección',
                                                        name: 'direccion_padre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Teléfono',
                                                        name: 'telefono_padre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Profesión',
                                                        name: 'profesion_padre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Cargo',
                                                        name: 'cargo_padre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Empresa',
                                                        name: 'empresa_padre',
                                                        anchor: '100%'
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        defaults: {width: '100%'},
                                        columnWidth: .46,
                                        border: false,
                                        bodyStyle: 'padding:15px 0 20px 70px',
                                        items: [
                                            {
                                                xtype: 'fieldset',
                                                title: '<b>' + 'DATOS DE LA MADRE' + '</b>',
                                                border: 1,
                                                style: {
                                                    borderColor: 'black',
                                                    borderStyle: 'solid'
                                                },
                                                defaults: {width: '90%'},
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nombre (s)',
                                                        name: 'nombre_madre',
                                                        anchor: '100%',
                                                        maskRe: /[A-Z a-z]/
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Apellidos',
                                                        name: 'apellidos_madre',
                                                        anchor: '100%',
                                                        maskRe: /[A-Z a-z]/
                                                    },
                                                    {
                                                        xtype: 'textarea',
                                                        fieldLabel: 'Dirección',
                                                        name: 'direccion_madre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Teléfono',
                                                        name: 'telefono_madre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Profesión',
                                                        name: 'profesion_madre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Cargo',
                                                        name: 'cargo_madre',
                                                        anchor: '100%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Empresa',
                                                        name: 'empresa_madre',
                                                        anchor: '100%'
                                                    }
                                                ]
                                            }
                                        ]
                                    }]
                            }]
                    },
                    {
                        title: 'Documentos requeridos',
                        layout: 'fit',
                        bodyStyle: 'padding:5px',
                        id: 'tbDocReq',
                        items: [
                            GpDocRequeridoEstud
                        ]
                    },
                    {
                        title: 'Menciones',
                        layout: 'fit',
                        bodyStyle: 'padding:5px',
                        id: 'tbMenciones',
                        items: [GpMencionesEstud]
                    },
                    {
                        title: 'Estudios',
                        bodyStyle: 'padding:5px',
                        layout: 'fit',
                        id: 'tbEstudios',
                        items: [GpEstudios]
                    },
                    {
                        title: 'Programas internacionales',
                        bodyStyle: 'padding:5px',
                        items: []
                    }
                ]}
        ]
    });

    //formulario para estudios
    var stcmbEnfasisE = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idenfasis'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarEnfasis',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbEnfasisE = Ext.create('Ext.form.field.ComboBox', {
       // id: 'idenfasis',
        name: 'idenfasis',
        fieldLabel: 'Énfasis',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbEnfasisE,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idenfasis',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        disabled: true,
        style: {
            marginTop: '11px'
        },
        tabIndex:23
    });
    var stcmbCarrerasE = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idcarrera'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarCarreras',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbCarrerasE = Ext.create('Ext.form.field.ComboBox', {
        //id: 'idcarrera',
        name: 'idcarrera',
        fieldLabel: 'Carrera',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbCarrerasE,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idcarrera',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        disabled: true,
        style: {
            marginTop: '11px'
        },
        tabIndex:22
    });
    var stcmbFacultadesE = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idestructura'
            },
            {
                name: 'denominacion'
            },
            {
                name: 'abreviatura'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarFacultades',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbFacultadesE = Ext.create('Ext.form.field.ComboBox', {
        //id: 'idestructura',
        name: 'idestructura',
        fieldLabel: 'Facultad',
        editable: false,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbFacultadesE,
        //emptyText: '--seleccione--',
        displayField: 'denominacion',
        valueField: 'idestructura',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        tabIndex:21
    });
    var stcmbPensumE = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idpensum'
            },
            {
                name: 'descripcion'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        //autoLoad: true,
        proxy: {
            type: 'ajax',
            url: 'cargarPensums',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var cmbPensumE = Ext.create('Ext.form.field.ComboBox', {
        //id: 'idpensum',
        name: 'idpensum',
        fieldLabel: 'Pensum',
        editable: true,
        anchor: '100%',
        labelWidth: 130,
        store: stcmbPensumE,
        //emptyText: '--seleccione--',
        displayField: 'descripcion',
        valueField: 'idpensum',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        disabled: true,
        style: {
            marginTop: '11px'
        },
        tabIndex:24
    });

    var formEstudios = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            cmbFacultadesE,
            cmbCarrerasE,
            cmbEnfasisE,
            cmbPensumE,
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true
            }
        ]
    });

    //ventana para mostrar formulario de estudiantes
    function mostFormEstudiante(opcion) {
        switch (opcion) {
            case 'add':
                {
                    stGpDocRequeridoEstud.load({params: {idalumno: 0}});

                    if (!winAdicionarEstudiante) {
                        winAdicionarEstudiante = Ext.create('Ext.Window', {
                            title: perfil.etiquetas.lbTitVentanaTitI,
                            closeAction: 'close',
                            modal: true,
                            width: 900,
                            height: 600,
                            constrain: true,
                            resizable: false,
                            items: formEstudiantes,
                            layout: 'fit',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    icon: perfil.dirImg + 'cancelar.png',
                                    handler: function() {
                                        cmbCarreras.disable();
                                        cmbEnfasis.disable();
                                        cmbPensum.disable();
                                        winAdicionarEstudiante.close();
                                        formEstudiantes.getForm().reset();
                                    }
                                },
                                {
                                    text: 'Aplicar',
                                    icon: perfil.dirImg + 'aplicar.png',
                                    handler: function() {
                                        AdicionarEstudiante('apl');
                                        cmbCarreras.disable();
                                        cmbEnfasis.disable();
                                        cmbPensum.disable();
                                    }
                                },
                                {
                                    text: 'Aceptar',
                                    icon: perfil.dirImg + 'aceptar.png',
                                    handler: function() {
                                        AdicionarEstudiante();
                                        cmbCarreras.disable();
                                        cmbEnfasis.disable();
                                        cmbPensum.disable();
                                    }
                                }
                            ]
                        });
                    }
                    winAdicionarEstudiante.add(formEstudiantes);
                    winAdicionarEstudiante.doLayout();
                    winAdicionarEstudiante.show();
                    formEstudiantes.getForm().reset();
                    Ext.getCmp('tbpnlEstudiantes').suspendEvents();
                    Ext.getCmp('tbpnlEstudiantes').setActiveTab(0);
                    Ext.getCmp('tbpnlEstudiantes').resumeEvents();
                }
                break;
            case 'mod':
                {
                    stGpDocRequeridoEstud.load({params: {idalumno: sm.getSelection()[0].raw.alumno}});

                    if (!winModificarEstudiante) {
                        winModificarEstudiante = Ext.create('Ext.Window', {
                            title: perfil.etiquetas.lbTitVentanaTitII,
                            closeAction: 'close',
                            width: 900,
                            height: 600,
                            constrain: true,
                            modal: true,
                            resizable: false,
                            layout: 'fit',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    icon: perfil.dirImg + 'cancelar.png',
                                    handler: function() {
                                        cmbCarreras.disable();
                                        cmbEnfasis.disable();
                                        cmbPensum.disable();
                                        winModificarEstudiante.close();
                                        formEstudiantes.getForm().reset();
                                    }
                                },
                                {
                                    text: 'Aceptar',
                                    icon: perfil.dirImg + 'aceptar.png',
                                    handler: function() {
                                        ModificarEstudiante();
                                        cmbCarreras.disable();
                                        cmbEnfasis.disable();
                                        cmbPensum.disable();
                                    }
                                }
                            ]
                        });
                    }
                    winModificarEstudiante.add(formEstudiantes);
                    winModificarEstudiante.doLayout();
                    winModificarEstudiante.show();
                    formEstudiantes.getForm().reset();
                    Ext.getCmp('tbpnlEstudiantes').suspendEvents();
                    Ext.getCmp('tbpnlEstudiantes').setActiveTab(0);
                    Ext.getCmp('tbpnlEstudiantes').resumeEvents();
                    formEstudiantes.getForm().loadRecord(sm.getLastSelected());
                }
                break;
        }
    }

    //ventana para mostrar formulario de estudios
    var winAdicionarEstudio;
    var winModificarEstudio;
    function mostFormEstudio(opcion) {
        switch (opcion) {
            case 'add':
            {
                winAdicionarEstudio = Ext.create('Ext.Window', {
                    title: 'Adicionar estudio',
                    closeAction: 'close',
                    width: 350,
                    height: 310,
                    modal: true,
                    constrain: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            tabIndex: 13,
                            handler: function () {
                                winAdicionarEstudio.close();
                            }
                        },
                        {
                            text: 'Aplicar',
                            icon: perfil.dirImg + 'aplicar.png',
                            tabIndex: 12,
                            handler: function () {
                                AdicionarEstudio('apl');
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            tabIndex: 11,
                            handler: function () {
                                AdicionarEstudio();
                                winAdicionarEstudio.close();
                            }
                        }
                    ]
                });
                formEstudios.getForm().reset();
                winAdicionarEstudio.add(formEstudios);
                winAdicionarEstudio.show();
            }
                break;
            case 'mod':
            {
                winModificarEstudio = Ext.create('Ext.Window', {
                    title: 'Modificar estudio',
                    closeAction: 'close',
                    width: 350,
                    height: 310,
                    constrain: true,
                    modal: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            handler: function () {
                                winModificarEstudio.close();
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            handler: function () {
                                ModificarEstudio();
                                winModificarEstudio.close();
                            }
                        }
                    ]
                });

                formEstudios.getForm().reset();
                winModificarEstudio.add(formEstudios);
                winModificarEstudio.doLayout();
                winModificarEstudio.show();
                formEstudios.getForm().loadRecord(smEstudios.getLastSelected());
            }
                break;
        }
    };

    //funciones
    function AdicionarEstudiante(apl) {
        if (formEstudiantes.getForm().isValid()) {
            var items = stGpDocRequeridoEstud.data.items;
            var iddocsAdd = new Array();

            for (var i = 0; i < items.length; i++) {
                if(items[i]['dirty']){
                    if(items[i].data['checked']){
                        iddocsAdd.push(items[i].data['iddocumentorequerido']);
                    }
                }
            }
            formEstudiantes.getForm().submit({
                url: 'insertarEstudiante',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                params: {idenfasis: Ext.getCmp('idenfasis').getValue(),
                    idcarrera: Ext.getCmp('idcarrera').getValue(),
                    iddocsAdd: Ext.JSON.encode(iddocsAdd)
                },

                failure: function(form, action) {
                    if (action.result.codMsg !== 3) {
                        formEstudiantes.getForm().reset();
                        stGpEstudiante.load();
                        if (!apl)
                            winAdicionarEstudiante.close();
                        sm.clearSelections();
                        btnModificarEstudiante.disable();
                        btnEliminarEstudiante.disable();
                    }
                }
            });
        } else
            mostrarMensaje(3, perfil.etiquetas.lbMsgErrorEnCamops);
    }
    function ModificarEstudiante() {
        if (formEstudiantes.getForm().isValid()) {
            var items = stGpDocRequeridoEstud.data.items;
            var iddocsAdd = new Array();
            var iddocsDel = new Array();

            for (var i = 0; i < items.length; i++) {
                if(items[i]['dirty']){
                    if(items[i].data['checked']){
                        iddocsAdd.push(items[i].data['iddocumentorequerido']);
                    }else{
                        iddocsDel.push(items[i].data['iddocumentorequerido']);
                    }
                }
            }

            formEstudiantes.getForm().submit({
                url: 'modificarEstudiante',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idalumno: sm.getSelection()[0].raw.alumno,
                    idenfasis: Ext.getCmp('idenfasis').getValue(),
                    idcarrera: Ext.getCmp('idcarrera').getValue(),
                    id_alumnodatos: sm.getSelection()[0].raw.id_alumnodatos,
                    iddocsAdd: Ext.JSON.encode(iddocsAdd),
                    iddocsDel: Ext.JSON.encode(iddocsDel)
                },
                failure: function(form, action) {
                    if (action.result.codMsg === 1) {
                        winModificarEstudiante.close();
                        stGpEstudiante.load();
                        btnModificarEstudiante.disable();
                        btnEliminarEstudiante.disable();
                    }
                }
            });
        } else
            mostrarMensaje(3, perfil.etiquetas.lbMsgErrorEnCamops);
    }
    function EliminarEstudiante() {
        mostrarMensaje(2, '¿Desea eliminar este estudiante?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando estudiante...'
        });
        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.alumno);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarEstudiante',
                    method: 'POST',
                    params: {
                        idalumnos: Ext.JSON.encode(ids)
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpEstudiante.load();
                            btnModificarEstudiante.disable();
                            btnEliminarEstudiante.disable();
                        }
                    }
                });
            }
        }
    }

    function AdicionarEstudio(apl) {
        if (formEstudios.getForm().isValid()) {
            formEstudios.getForm().submit({
                url: 'insertarEstudio',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarEstMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formEstudios.getForm().reset();
                        stGpEstudio.load();
                        if (!apl)
                            winAdicionarEstudio.close();
                        smEstudios.clearSelections();
                        btnModificarEstudio.disable();
                        btnEliminarEstudio.disable();
                    }
                }
            });
        }
    }
    function ModificarEstudio() {
        if (formEstudios.getForm().isValid()) {
            formEstudios.getForm().submit({
                url: 'modificarEstudio',
                waitMsg: perfil.etiquetas.lbMsgFunModificarEstMsg,
                params: {
                    idestudio: smEstudios.getSelection()[0].raw.idestudio
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarEstudio.close();
                        stGpEstudio.load();
                        btnModificarEstudio.disable();
                        btnEliminarEstudio.disable();
                    }
                }
            });
        }
    }
    function EliminarEstudio() {
        mostrarMensaje(2, '¿Desea eliminar este estudio?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando estudio...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < smEstudios.getCount(); i++) {
                    ids.push(smEstudios.getSelection()[i].raw.idestudio);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarEstudio',
                    method: 'POST',
                    params: {
                        idestudios: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            smEstudios.clearSelections();
                            stGpEstudio.load();
                            btnModificarEstudio.disable();
                            btnEliminarEstudio.disable();
                        }
                    }
                });
            }
        }
    }

    var general = Ext.create('Ext.panel.Panel', {layout: 'border', items: [pnlEstudiante, pnl1]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});
}
