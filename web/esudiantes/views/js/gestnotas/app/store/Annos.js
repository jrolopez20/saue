Ext.define('GestNotas.store.Annos', {
    extend: 'Ext.data.Store',
    model: 'GestNotas.model.Annos',

    autoLoad: true,
    storeId: 'idStoreAnnos',
    proxy: {
        type: 'ajax',
        api: {
            read: '../gestconvalidaciones/cargarAnnos'
        },
        actionMethods: {
            read: 'POST',
        },
        reader: {
            root: 'datos',
        }
    }
});