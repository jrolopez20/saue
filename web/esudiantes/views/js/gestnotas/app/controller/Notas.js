Ext.define('GestNotas.controller.Notas', {
    extend: 'Ext.app.Controller',

    views: [
        'GestNotas.view.nota.NotaList',
        'GestNotas.view.nota.AlumnoList',
        'GestNotas.view.nota.WindowSearch',
        'GestNotas.view.nota.CursoList',
        'GestNotas.view.nota.NotaListToolBar'
    ],
    stores: ['GestNotas.store.Notas', 'GestNotas.store.Alumnos', 'GestNotas.store.Cursos','GestNotas.store.Periodos','GestNotas.store.Annos'],
    models: ['GestNotas.model.Notas','GestNotas.model.Alumnos', 'GestNotas.model.Cursos','GestNotas.model.Periodos','GestNotas.model.Annos' ],
    refs: [
        {ref: 'list', selector: 'notalist'},
        {ref: 'alist', selector: 'alumnolist'},
        {ref: 'clist', selector: 'cursolist'}
    ],

    init: function () {
        this.control({
            'notalisttbar button[action=buscar]': {
                click: this.buscar
            },
            'notalisttbar combobox[id=periodoList]': {
                afterrender: this.comboEvents
            },
            'notalisttbar combobox[id=anno]': {
                change: this.updateStore,
                afterrender: this.updateStore
            },
            'notalisttbar textfield[id=buscar]': {
                keyup: this.fastSearch,
            },
            'windowsearch button[action=aceptar]': {
                click: this.cargarNotas
            },
            'cursolist' : {
                 afterrender: this.loadStoreCursos
            },
            'notalist': {
                afterrender: this.saveChangesEvent
            }

        });
    },
    updateStore: function (combo, nv) {
        Ext.getCmp('periodoList').getStore().load({params: {anno: Ext.getCmp('anno').getValue()}});
    },
    loadStoreCursos: function(grid, eOp){
        grid.getStore().clearFilter(true);
        grid.getStore().addFilter({'property':'idperiododocente', 'value':Ext.getCmp('periodoList').getValue()});
    },
    buscar: function (button) {
        if(Ext.getCmp('periodoList').getValue()){
            if(Ext.getCmp('radio').getValue().rb == 'alumnolist'){
                var view = Ext.widget('windowsearch',{who:'alumnolist', height:400});
            } else {
                var view = Ext.widget('windowsearch',{who:'cursolist', width:600, height:400});
            }
       } else {
        mostrarMensaje(1, perfil.etiquetas.lbMsgSelCombo);
       }
    },
    mostrarError: function(type){
        mostrarMensaje(1, perfil.etiquetas.lbMsgError + " " + type + ".")
    },
    saveChangesEvent: function(grid, eOp){
        var me = this;
        grid.getPlugin('cellplugin').on('edit', function(editor, e) {
        e.grid.getStore().sync();
        });
        grid.getStore().on('beforeload', function (store) {
            store.getProxy().extraParams = {type: me.typo, idt: me.idt, idpd:Ext.getCmp('periodoList').getValue()};
            });
    },
    comboEvents: function(combo, eOp){
        combo.getStore().on('beforeload', function (store) {    
            store.getProxy().extraParams = {anno: Ext.getCmp('anno').getValue()};
            });
        combo.getStore().on('load', function (store) {
            if(store.count()>0)
                combo.select(store.getAt(0).data.idperiododocente);
            });
    },
    cargarNotas: function(button){
        var win = button.up('window');
        var me = this;
        if(button.list == 'alumnolist'){
            record = me.getAlist().getSelectionModel().getSelection()[0];
            console.log(record);
           if (record){
                    me.getList().reconfigure(me.getList().getStore(),[
                        { dataIndex: 'idalumno', hidden: true, hideable: false},
                        { dataIndex: 'idmateria', hidden: true, hideable: false},
                        { dataIndex: 'idperiododocente', hidden: true, hideable: false},
                        { header: 'Código', dataIndex: 'codmateria'},
                        { header: 'Materia', dataIndex: 'materia', flex: 1},
                        { header: 'Facultad', dataIndex: 'facultad'},
                        { header: 'Nota 1', dataIndex: 'nota1', editor: 'numberfield'},
                        { header: 'Nota 2', dataIndex: 'nota2', editor: 'numberfield'},
                        { header: 'Nota 3', dataIndex: 'nota3', editor: 'numberfield'},
                        { header: 'Nota 4', dataIndex: 'nota4', editor: 'numberfield'},
                        { header: 'Promedio', dataIndex: 'prom',
                            renderer: function(value, md, record){
                                 return (record.data.nota1+record.data.nota2+record.data.nota3+record.data.nota4)/4;
                                }}
                    ]);
                    me.typo = 'alumno';
                me.idt = record.data.idalumno;
                me.getList().getStore().load();
                me.getList().setTitle("Nombre: "+record.data.nombre + " " + record.data.apellidos + "| C&oacute;digo: " + record.data.codigo);
                win.close();
             } else {
                 me.mostrarError(perfil.etiquetas.lbMsgEst);
             }
        } else {
            record = me.getClist().getSelectionModel().getSelection()[0];
            if(record){
                    me.getList().reconfigure( me.getList().getStore(),[
                        { dataIndex: 'idnota', hidden: true, hideable: false},
                        { dataIndex: 'idalumno', hidden:true, hideable: false},
                        { header: 'Código', dataIndex: 'codigo'},
                        { header: 'Nombre', dataIndex: 'nombre', flex: 1},
                        { header: 'Facultad', dataIndex: 'facultad'},
                        { header: 'Nota 1', dataIndex: 'nota1', editor: 'numberfield'},
                        { header: 'Nota 2', dataIndex: 'nota2', editor: 'numberfield'},
                        { header: 'Nota 3', dataIndex: 'nota3', editor: 'numberfield'},
                        { header: 'Nota 4', dataIndex: 'nota4', editor: 'numberfield'},
                        { header: 'Promedio', dataIndex: 'prom',
                            renderer: function(value, md, record){
                                return (record.data.nota1+record.data.nota2+record.data.nota3+record.data.nota4)/4;
                                }}
                    ]);
                me.typo = 'curso';
                me.idt = record.data.idmateria;
                me.getList().getStore().load();
                 me.getList().setTitle("Materia: "+ record.data.codmateria + "-" + record.data.materia + "| Profesor: " + record.data.profesor+ "| Horario: " + record.data.horario);
                win.close();
          } else {
            me.mostrarError(perfil.etiquetas.lbMsgCur);
            }
        }
        

    }
});
