Ext.define('GestNotas.model.Notas', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idpd', type: 'int'},
        {name: 'idmateria', type: 'int'},
        {name: 'idalumno', type: 'int'},
        {name: 'codigo', type: 'int'},
        {name: 'materia', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'codmateria', type: 'string'},
        {name: 'facultad', type: 'string'},
        {name: 'nota1',  type: 'float'},
        {name: 'nota2',  type: 'float'},
        {name: 'nota3',  type: 'float'},
        {name: 'nota4',  type: 'float'},
        {name: 'prom',  type: 'float'}
    ]
}) 