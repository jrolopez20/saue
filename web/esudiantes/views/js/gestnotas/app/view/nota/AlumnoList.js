Ext.define('GestNotas.view.nota.AlumnoList', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.alumnolist',

    store: 'GestNotas.store.Alumnos',

    selModel: Ext.create('Ext.selection.RowModel', {
        id: 'idSelectionAlummnoGrid',
        mode: 'SINGLE'
    }),
   
    columns: [
        { dataIndex: 'idalumno', hidden: true, hideable: false},
        { header: 'Código', dataIndex: 'codigo'},
        { header: 'Nombre', dataIndex: 'nombre', flex: 1},
        { header: 'Apellidos', dataIndex: 'apellidos', flex: 1 },
    ],

    initComponent: function () {
        var me = this;
        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.callParent(arguments);
    },

});