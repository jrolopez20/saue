Ext.define('GestNotas.view.nota.NotaListToolBar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.notalisttbar',
    store:'GestNotas.store.Periodos',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'fieldset',
                width: 280,
                height: 60,
                border: 0,
                style: {paddingTop: '0px'},
                items: [{   
                    xtype:'combobox',
                    id: 'anno',
                    fieldLabel: 'Año',
                    queryMode:'local',
                    valueField: 'anno',
                    displayField: 'anno',
                    store:'GestNotas.store.Annos',
                    value:'2014',

                },  {   
                    xtype:'combobox',
                    id: 'periodoList',
                    forceSelection: true,
                    autoSelect : true,
                    emptyText : '--seleccione--',
                    fieldLabel: 'Periodo',
                    queryMode: 'local',
                    valueField: 'idperiododocente',
                    displayField: 'descripcion',
                    allowBlank:false,
                    store:me.store
                }]
            },{
                xtype: 'fieldset',
                width: 180,
                title: 'Tipo de ingreso',
                style: {top:'10px !important'},
                items: [{
                        xtype: 'radiogroup',
                        id: 'radio',
                        columns: 2,
                        vertical: true,
                        items: [
                            {boxLabel: 'Por alumno', name: 'rb', inputValue: 'alumnolist', checked: true},
                            {boxLabel: 'Por curso', name: 'rb', inputValue: 'cursolist'}
                        ]
                    }]
            },{
                id: 'idBtnSrhNota',
                rowspan: 3,
                iconAlign: 'top',
                text: perfil.etiquetas.lbBtnBuscar,
                icon: perfil.dirImg + 'buscar.png',
                iconCls: 'btn',
                action: 'buscar',
                style: {marginLeft:'5px'}

            },'->',{                
                id: 'idBtnImprimir',
                rowspan: 3,
                iconAlign: 'top',
                text: perfil.etiquetas.lbBtnImprimir,
                icon: perfil.dirImg + 'imprimir.png',
                iconCls: 'btn',
                action: 'imprimir'}
        ];

        this.callParent(arguments);
    }
});