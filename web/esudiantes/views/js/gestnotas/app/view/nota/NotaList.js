Ext.define('GestNotas.view.nota.NotaList', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.notalist',

    //title: perfil.etiquetas.lbTtlLista,

    store: 'GestNotas.store.Notas',

    // selModel: selModel = Ext.create('Ext.selection.RowModel', {
    //     id: 'idSelectionNotaGrid',
    //     mode: 'SINGLE'
    // }),
    columns: [
        { dataIndex: 'idalumno', hidden: true, hideable: false},
        { dataIndex: 'idpd', hidden: true},
        { dataIndex: 'idmateria', hidden: true},
        { header: 'Código', dataIndex: 'codmateria'},
        { header: 'Materia', dataIndex: 'materia', flex: 1},
        { header: 'Facultad', dataIndex: 'facultad'},
        { header: 'Nota 1', dataIndex: 'nota1'},
        { header: 'Nota 2', dataIndex: 'nota2'},
        { header: 'Nota 3', dataIndex: 'nota3'},
        { header: 'Nota 4', dataIndex: 'nota4'},
        { header: 'Promedio', dataIndex: 'prom'}
    ],
    
    selType: 'cellmodel',

    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            pluginId: 'cellplugin'
        })
    ],
    initComponent: function () {
        var me = this;

       // me.selModel.on("selectionchange", me.manejarBotones, me);

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });


        me.callParent(arguments);
    },

});