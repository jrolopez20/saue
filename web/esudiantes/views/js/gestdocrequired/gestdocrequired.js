var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestdocrequired', function() {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

function cargarInterfaz() {
    ////------------ Botones ------------////
    var btnAdicionarDocRequerido = Ext.create('Ext.Button', {
        id: 'btnAgrDocRequerido',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormDocRequerido('add');
        }
    });
    var btnModificarDocRequerido = Ext.create('Ext.Button', {
        id: 'btnModDocRequerido',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function() {
            mostFormDocRequerido('mod');
        }
    });
    var btnEliminarDocRequerido = Ext.create('Ext.Button', {
        id: 'btnEliDocRequerido',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function() {
            EliminarDocRequerido();
        }
    });

    var txtBuscarDocRequerido = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'docrequerido',
        enableKeyEvents: true
    });

    txtBuscarDocRequerido.on('keyup', function(tf) {
        if (tf.getValue()) {
            stGpDocRequerido.clearFilter();
            stGpDocRequerido.filter('descripcion', tf.getValue());
        } else
            stGpDocRequerido.clearFilter();
    }, this)

    var btnBuscarDocRequerido = Ext.create('Ext.Button', {
        id: 'btnBuscarDocRequerido',
        disabled: true,
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });

    var separador = new Ext.menu.Separator();

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpDocRequerido = new Ext.data.Store({
        fields: [
            {
                name: 'iddocumentorequerido'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'fecha'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'estado'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarDocRequerido',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function(smodel, rowIndex, keepExisting, record) {
                btnModificarDocRequerido.setDisabled(smodel.getCount() == 0);
                btnEliminarDocRequerido.enable(smodel.getCount() > 0);
            }
        }
    });

    var GpDocRequerido = new Ext.grid.GridPanel({
        store: stGpDocRequerido,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'iddocumentorequerido',
                flex: 1,
                dataIndex: 'iddocumentorequerido',
                hidden: true
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            }
        ],
        region: 'center',
                tbar: [btnAdicionarDocRequerido, btnModificarDocRequerido, btnEliminarDocRequerido, separador, txtBuscarDocRequerido, btnBuscarDocRequerido],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux99',
            store: stGpDocRequerido,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });
    stGpDocRequerido.load();

    var general = Ext.create('Ext.panel.Panel', {layout: 'border', items: [GpDocRequerido]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarDocRequerido;
    var winModificarDocRequerido;

    var formDocRequerido = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Descripción',
                name: 'descripcion',
                anchor: '96%',
                id: 'descripcion',
                allowBlank: false,
                //maskRe: /[A-Z a-z]/,
                tabIndex: 1
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '6px'
                },
                checked: true
            }
        ]
    });

    function mostFormDocRequerido(opcion) {
        switch (opcion) {
            case 'add':
                {
                    winAdicionarDocRequerido = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitI,
                        closeAction: 'hide',
                        width: 300,
                        height: 200,
                        modal: true,
                        constrain: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                tabIndex: 13,
                                handler: function() {
                                    winAdicionarDocRequerido.hide();
                                }
                            },
                            {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                tabIndex: 12,
                                handler: function() {
                                    AdicionarDocRequerido('apl');
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                tabIndex: 11,
                                handler: function() {
                                    AdicionarDocRequerido();
                                    winAdicionarDocRequerido.hide();
                                }
                            }
                        ]
                    });
                    formDocRequerido.getForm().reset();
                    winAdicionarDocRequerido.add(formDocRequerido);
                    winAdicionarDocRequerido.show();
                }
                break;
            case 'mod':
                {
                    winModificarDocRequerido = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitII,
                        closeAction: 'hide',
                        width: 300,
                        height: 200,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function() {
                                    winModificarDocRequerido.hide();
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function() {
                                    ModificarDocRequerido();
                                    winModificarDocRequerido.hide();
                                }
                            }
                        ]
                    });

                    formDocRequerido.getForm().reset();
                    winModificarDocRequerido.add(formDocRequerido);
                    winModificarDocRequerido.doLayout();
                    winModificarDocRequerido.show();
                    formDocRequerido.getForm().loadRecord(sm.getLastSelected());
                }
                break;
        }
    }


    //funciones

    function AdicionarDocRequerido(apl) {
        if (formDocRequerido.getForm().isValid()) {
            formDocRequerido.getForm().submit({
                url: 'insertarDocRequerido',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function(form, action) {
                    if (action.result.codMsg != 3) {
                        formDocRequerido.getForm().reset();
                        stGpDocRequerido.load();
                        if (!apl)
                            winAdicionarDocRequerido.hide();
                        sm.clearSelections();
                        btnModificarDocRequerido.disable();
                        btnEliminarDocRequerido.disable();
                    }
                }
            });
        }
    }

    function ModificarDocRequerido() {
        if (formDocRequerido.getForm().isValid()) {
            formDocRequerido.getForm().submit({
                url: 'modificarDocRequerido',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    iddocumentorequerido: sm.getSelection()[0].raw.iddocumentorequerido
                },
                failure: function(form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarDocRequerido.hide();
                        stGpDocRequerido.load();
                        btnEliminarDocRequerido.disable();
                        btnModificarDocRequerido.disable();
                    }
                }
            });
        }
    }

    function EliminarDocRequerido() {
        mostrarMensaje(2, '¿Desea eliminar este documento requerido?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando documento requerido...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.iddocumentorequerido);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'EliminarDocRequerido',
                    method: 'POST',
                    params: {
                        iddocumentorequeridos: Ext.JSON.encode(ids)
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpDocRequerido.load()
                            btnModificarDocRequerido.disable();
                            btnEliminarDocRequerido.disable();
                        }
                    }
                });
            }
        }
    }
}