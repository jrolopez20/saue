Ext.define('CredxArea.view.carrera.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.credxarea_carrera_combo',

    fieldLabel: perfil.etiquetas.lbCmpArea,
    editable: false,
    store: 'CredxArea.store.Carreras',
    queryMode: 'local',
    name: 'idcarrera',
    valueField: 'idcarrera',
    displayField: 'descripcion',

    initComponent: function(){
        var me = this;

        me.fieldLabel = perfil.etiquetas.lbCmpArea;
        me.emptyText = perfil.etiquetas.lbEmpCombo;

        this.callParent(arguments);
    }
});