Ext.define('GestEnfasis.controller.Enfasis', {
    extend: 'Ext.app.Controller',

    views: [
        'GestEnfasis.view.enfasi.EnfasiList',
        'GestEnfasis.view.enfasi.EnfasiEdit',
        'GestEnfasis.view.enfasi.EnfasiListToolBar',
        'GestEnfasis.view.enfasi.EnfasiListPagingToolBar',
        'GestEnfasis.view.facultad.Combo',
        'GestEnfasis.view.carrera.Combo'

    ],
    stores: [
        'GestEnfasis.store.Enfasis',
        'GestEnfasis.store.Carreras',
        'GestEnfasis.store.Facultades'
    ],

    models: [
        'GestEnfasis.model.Enfasi',
        'GestEnfasis.model.Carrera'
    ],

    refs: [
        {ref: 'list', selector: 'enfasilist'},
        {ref: 'tbar', selector: 'enfasilisttbar'},
        {ref: 'enfasis_carrera_combo', selector: 'enfasis_carrera_combo'}
    ],

    init: function () {
        var me = this;

        me.control({

            'enfasilist': {
                selectionchange: me.activarBotones
            },
            'enfasis_facultad_combo': {
                select: me.cargarCarreras
            },
            'enfasilisttbar button[action=adicionar]': {
                click: me.adicionarEnfasi
            },
            'enfasilisttbar button[action=modificar]': {
                click: me.modificarEnfasi
            },
            'enfasilisttbar button[action=eliminar]': {
                click: me.eliminarEnfasi
            },
            'enfasiedit button[action=aceptar]': {
                click: me.guardarEnfasi
            },
            'enfasiedit button[action=aplicar]': {
                click: me.guardarEnfasi
            }
        });
    },

    cargarCarreras: function (combo) {
        var me = this,
            enfasis_carrera_combo = me.getEnfasis_carrera_combo();

        enfasis_carrera_combo.reset();
        enfasis_carrera_combo.getStore().load(
            {
                params: { idfacultad: combo.getValue() }
            }
        )
    },

    activarBotones: function (store, selected) {
        var me = this,
            tbar = me.getTbar();
        tbar.down('button[action=modificar]').setDisabled(selected.length === 0);
        tbar.down('button[action=eliminar]').setDisabled(selected.length === 0);
    },

    adicionarEnfasi: function (button) {
        var view = Ext.widget('enfasiedit');
        view.setTitle(perfil.etiquetas.lbTtlAdicionar);
    },

    modificarEnfasi: function (button) {
        var view = Ext.widget('enfasiedit'),
            record = this.getList().getSelectionModel().getLastSelected();

        view.setTitle(perfil.etiquetas.lbTtlModificar);

        view.down('enfasis_facultad_combo').disable();
        view.down('form').loadRecord(record);
    },

    eliminarEnfasi: function (button) {
        var me = this,
            record = me.getList().getSelectionModel().getLastSelected(),
            store = me.getList().getStore();

        mostrarMensaje(
            2,
            perfil.etiquetas.lbMsgConfEliminar + " '" + record.get('descripcion_enfasi') + "'",
            function (btn, text) {
                if (btn == 'ok') {
                    store.remove(record);
                    me.sincronizarStore(me.getList(), store);
                }
            }
        )
    },

    guardarEnfasi: function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            me = this;

        if (form.getForm().isValid()) {
            var record = form.getRecord(),
                values = form.getValues(),
                combo = form.down('#idEnfasisCarrerasCombo');

            values.descripcion_carrera = combo.getRawValue();

            //modificando
            if (record){
                record.set(values);
            }
            //insertando
            else {
                me.getList().getStore().add(values);
            }

            if (button.action === 'aceptar')
                win.close();

            me.sincronizarStore(me.getList(), me.getList().getStore());
        }
    },

    sincronizarStore: function (grid, store) {
        store.sync({
            //scope: this,
            success: function (batch) {
                if (batch.operations[0].action == "create") {
                    //var idcarrera = Ext.decode(batch.operations[0].response.responseText).idcarrera;
                    //store.last().set('idcarrera', idcarrera);

                    grid.down('enfasilistpbar').moveLast();
                    //grid.down('enfasilisttbar').updateInfo();
                } else if (batch.operations[0].action == "destroy") {
                    if(store.count() > 0)
                        grid.down('enfasilistpbar').doRefresh();//me.getList().down('enfasilisttbar').doRefresh();
                    else
                        grid.down('enfasilistpbar').movePrevious();
                }
            },
            failure: function () {
                grid.down('enfasilistpbar').doRefresh();
            }
        });
    }
});