Ext.define('GestCarreras.view.carrera.CarreraList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.carreralist',

    store: Ext.create('GestCarreras.store.Carreras'),

    initComponent: function () {
        var me = this;

        me.sm = Ext.create('Ext.selection.RowModel', {
            id: 'idSelectionCarreraGrid',
            mode: 'SINGLE',
            pruneRemoved: false
        });

        me.bbar = Ext.create('GestCarreras.view.carrera.CarreraListPagingToolBar');

        me.tbar = Ext.create('GestCarreras.view.carrera.CarreraListToolBar');

        me.viewConfig = {
            getRowClass: function(record){
                if (record.get('estado') === false)
                    return 'FilaRoja';
            }
        };

        me.columns = [
            { dataIndex: 'idcarrera', hidden: true, hideable: false},
            { dataIndex: 'estado', hidden: true, hideable: false },
            { dataIndex: 'idfacultad', hidden: true, hideable: false },
            { header: perfil.etiquetas.lbHdrDescripcion, dataIndex: 'descripcion_carrera', flex: 1}
        ];

        me.callParent(arguments);
    }
});