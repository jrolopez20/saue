Ext.define('Maestro.store.Clasificadores', {
    extend: 'Ext.data.TreeStore',
    model: 'Maestro.model.Clasificadores',

    autoLoad: false,
    folderSort: false,
    proxy: {
        type: 'ajax',
        url: '/logistica/facturacion/index.php/clasificadores/load',
        actionMethods: {
            read: 'POST'
        }
    }
});