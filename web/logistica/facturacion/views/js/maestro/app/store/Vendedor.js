Ext.define('Maestro.store.Vendedor', {
    extend: 'Ext.data.Store',
    model: 'Maestro.model.Vendedor',

    pageSize: 25,
    autoLoad: false,
    autoSync: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '../vendedor/read',
            create: '../vendedor/create',
            update: '../vendedor/update',
            destroy: '../vendedor/destroy'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false,
            root: 'data'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    },
    listeners: {
        beforesync: function (options, eOpts) {
            if (options.create) {
                return options.create[0].isValid();
            }
        },
        write: function (proxy, operation) {
            var response = Ext.decode(operation.response.responseText)
            proxy.suspendAutoSync();
            operation.records[0].set('idvendedor', response.idvendedor);
            proxy.resumeAutoSync();
        }
    }
});