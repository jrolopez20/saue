Ext.define('Maestro.store.Productoservicio', {
    extend: 'Ext.data.Store',
    model: 'Maestro.model.Productoservicio', // Definir aqui uno para los productos

    storeId: 'stProductoservicio',
    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: '/logistica/facturacion/index.php/productoservicio/load',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    }
});