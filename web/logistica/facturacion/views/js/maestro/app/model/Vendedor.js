Ext.define('Maestro.model.Vendedor', {
    extend: 'Ext.data.Model',
    idProperty: 'idvendedor',
    fields: [
        {name: 'idvendedor'},
        {name: 'codigo', type: 'string'},
        {name: 'fullname', type: 'string'},
        {name: 'numid'},
        {name: 'foto'},
        {name: 'idpersona'}
    ],

    validations: [
        {type: 'presence', field: 'codigo'},
        {type: 'length', field: 'codigo', min: 3}
    ]
});