Ext.define('Maestro.model.Clasificadores', {
    extend: 'Ext.data.Model',
    idProperty: 'idclasificador',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idclasificador', type: 'int'},
        {name: 'idpadre', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'descripcion', type: 'string'},
        {name: 'codigo'},
        {name: 'concat'},
        {name: 'tipo', type: 'int'},
        {name: 'idestructura', type: 'int'},
        {name: 'activo', type: 'int'},
        {name: 'nivel', type: 'int'},
        {name: 'idparteformato', type: 'int'},
        {name: 'leaf', type: 'bool'},
        {name: 'text', type: 'string'}
    ],
    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});