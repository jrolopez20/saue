Ext.define('Maestro.model.Productoservicio', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idproductoservicio', type: 'int'},
        {name: 'codigo', type: 'string'},
        {name: 'denominacion', type: 'string'},
        {name: 'foto', type: 'string'},
        {name: 'idunidadmedida', type: 'int'},
        {name: 'stock', type: 'int'},
        {name: 'preciounitario', type: 'float'},
        {name: 'activo', type: 'int'},
        {name: 'idestructura', type: 'int'},
        {name: 'codigobarra', type: 'string'},
        {name: 'iva', type: 'float'},
        {name: 'idclasificador', type: 'int'},
        {name: 'tipo', type: 'int'},
        {name: 'qtip', type: 'string'},
    ]
});