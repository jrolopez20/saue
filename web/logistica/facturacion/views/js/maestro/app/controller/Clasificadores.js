Ext.define('Maestro.controller.Clasificadores', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.clasificadorprodserv.EditClasificador',
        'Maestro.view.clasificadorprodserv.TreeGrid',
    ],
    stores: [
        'Maestro.store.Clasificadores'
    ],
    models: [
        'Maestro.model.Clasificadores'
    ],
    refs: [
        {ref: 'clasificadorprodserv_treegrid', selector: 'clasificadorprodserv_treegrid'},
        {ref: 'editclasificador', selector: 'editclasificador'}
    ],
    init: function () {
        var me = this;

        this.listen({
            component: {
                'clasificadorprodserv_treegrid button[action=adicionar]': {
                    click: me.onAdd
                },
                'clasificadorprodserv_treegrid button[action=modificar]': {
                    click: me.onMod
                },
                'clasificadorprodserv_treegrid button[action=eliminar]': {
                    click: me.onDelete
                },
                'clasificadorprodserv_treegrid button[action=reload]': {
                    click: me.onReload
                },
                'clasificadorprodserv_treegrid button[action=activar]': {
                    click: me.onActivate
                },
                'clasificadorprodserv_treegrid': {
                    //itemclick: me.onNodeCLick,
                    selectionchange: me.onSelectionChange
                },
                'editclasificador button[action=aceptar], editclasificador button[action=aplicar]': {
                    click: me.save
                },
                'editclasificador button[action=cancelar]': {
                    click: me.onReload
                }
            }
        });
    },

    onAdd: function (btn) {
        var me = this,
            win = Ext.widget('editclasificador'),
            form = win.down('form');
        win.setTitle(perfil.etiquetas.titleWinAdd);
        win.down('button[action=aplicar]').show();

        var selected = me.getClasificadorprodserv_treegrid().getSelectionModel().getLastSelected();
        if (selected) {
            form.getForm().findField('tipo').setValue(selected.get('tipo'));
            win.down('combobox[name="tipo"]').setReadOnly(true);
        }
    },

    onMod: function (btn) {
        var me = this,
            win = Ext.widget('editclasificador'),
            form = win.down('form');
        var record = me.getClasificadorprodserv_treegrid().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },

    onReload: function (btn) {
        var me = this,
            tree = me.getClasificadorprodserv_treegrid();

        tree.getStore().load();
    },
    onActivate: function (btn) {
        var me = this,
            tree = me.getClasificadorprodserv_treegrid(),
            selection = tree.getSelectionModel().getSelection(),
            ids = [];

        selection.every(function (record) {
            ids.push(record.get('idclasificador'));
            return true;
        });

        var myMask = new Ext.LoadMask(tree, {msg: perfil.etiquetas.loading});
        myMask.show();
        Ext.Ajax.request({
            url: '/logistica/facturacion/index.php/clasificadores/changestate',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                mostrarMensaje(1, perfil.etiquetas.msgOK);
                tree.getStore().load();
            }
        });
    },

    //onNodeCLick: function (tree, record, items, index, e, eOpt) {
    //},

    onSelectionChange: function (selModel, records, eOpts) {

        var me = this,
            tree = me.getClasificadorprodserv_treegrid(),
            hasSelection = tree.getSelectionModel().hasSelection();
        // aki hacer esto mismo para la seleccion
        tree.down('button[action=modificar]').setDisabled(!hasSelection);
        tree.down('button[action=eliminar]').setDisabled(!hasSelection);
        tree.down('button[action=activar]').setDisabled(!hasSelection);

        if (records.length) {
            var type = records[0].get('activo');
            records.every(function (record) {
                if (record.get('activo') !== type) {
                    type = 2;
                    return false;
                } else {
                    return true;
                }
            });

            if (type === 2) {
                tree.down('button[action=activar]').setIcon(perfil.dirImg + 'banderarojaverde.png');
                tree.down('button[action=activar]').setText(perfil.etiquetas.lbBtnActivarDesactivar);
                tree.down('button[action=activar]').setTooltip(perfil.etiquetas.ttpBtnActivarDesactivar);
            } else if (type === 1) {
                tree.down('button[action=activar]').setIcon(perfil.dirImg + 'banderaroja.png');
                tree.down('button[action=activar]').setText(perfil.etiquetas.lbBtnDesactivar);
                tree.down('button[action=activar]').setTooltip(perfil.etiquetas.ttpBtnDesactivar);
            } else {
                tree.down('button[action=activar]').setIcon(perfil.dirImg + 'banderaverde.png');
                tree.down('button[action=activar]').setText(perfil.etiquetas.lbBtnActivar);
                tree.down('button[action=activar]').setTooltip(perfil.etiquetas.ttpBtnActivar);
            }
        }
    },

    save: function (btn) {
        var me = this,
            concat = '',
            idpadre = null,
            nivel = 1,
            form = btn.up('window').down('form').getForm(),
            win = btn.up('window');

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: perfil.etiquetas.lbGuardando});
            myMask.show();

            var selected = me.getClasificadorprodserv_treegrid().getSelectionModel().getLastSelected();

            //Caso de adicionar
            if (Ext.isEmpty(form.findField('idclasificador').getValue())) {
                if (selected) {
                    concat = selected.get('concat') + '.';
                    idpadre = selected.get('idclasificador');
                    nivel += parseInt(selected.get('nivel'));
                    form.findField('idpadre').setValue(idpadre);
                }
            } else {
                //Caso de modificar
                if (selected.get('idclasificador') != selected.get('idpadre')) {
                    var classPadre = me.getClasificadorprodserv_treegrid().getStore().getNodeById(selected.get('idpadre'));
                    idpadre = selected.get('idpadre');
                    concat = classPadre.get('concat') + '.';
                }
                nivel = selected.get('nivel');
            }

            form.findField('nivel').setValue(nivel);
            concat += form.findField('codigo').getValue();

            form.submit({
                clientValidation: true,
                submitEmptyText: false,
                //url: 'updateGrupoContable',
                params: {
                    //idpadre: idpadre,
                    concat: concat
                },
                success: function (form, action) {
                    myMask.hide();
                    //mostrarMensaje()
                    if (btn.action === 'aceptar') {
                        win.close();
                        if (selected) {
                            me.getClasificadorprodserv_treegrid().getStore().reload({params: {node: idpadre}});
                            //me.getClasificadorprodserv_treegrid().expandNode(selected);
                        } else {
                            me.getClasificadorprodserv_treegrid().getStore().load();
                        }
                    } else {
                        form.findField('codigo').reset();
                        form.findField('denominacion').reset();
                        form.findField('descripcion').reset();
                        //form.reset();
                    }
                },
                failure: function (form, action) {
                    myMask.hide();
                    switch (action.failureType) {
                        case Ext.form.action.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Fallido', 'Los campos del formulario no debe ser enviados con valores no v&aacute;lidos');
                            break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Fallido', 'Comunicacion Ajax fallida');
                            break;
                        case Ext.form.action.Action.SERVER_INVALID:
                            Ext.Msg.alert('Fallido', action.result.mensaje);
                    }
                }
            });
        }
    },

    onDelete: function (btn) {
        var me = this,
            tree = btn.up('treepanel'),
            selection = tree.getSelectionModel().getSelection();

        if (selection.length) {
            var msg = selection.length > 1 ?
                perfil.etiquetas.msgConfirmDelP :
                perfil.etiquetas.msgConfirmDel
            Ext.Msg.confirm(perfil.etiquetas.lbConfirm, msg,
                function (btn) {
                    if (btn === 'yes') {
                        var ids = [];
                        selection.every(function (record) {
                            ids.push(record.get('idclasificador'));
                            return true;
                        });
                        me.delete(ids, me);
                    }
                });
        }
    },

    delete: function (ids, me) {
        var tree = me.getClasificadorprodserv_treegrid();
        var myMask = new Ext.LoadMask(tree, {msg: perfil.etiquetas.loading});
        myMask.show();
        Ext.Ajax.request({
            url: '/logistica/facturacion/index.php/clasificadores/delete',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                if (responseData.errors && responseData.errors.length > 0) {
                    mostrarMensaje(3, perfil.etiquetas.msgConfirmDelFail);
                } else {
                    mostrarMensaje(1, perfil.etiquetas.msgConfirmDelOk);
                }
                tree.getStore().load();
            }
        });
    }

});