Ext.define('Maestro.controller.Vendedor', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.vendedor.Grid'
    ],
    stores: [
        'Maestro.store.Vendedor',
        'Maestro.store.Persona'
    ],
    models: [
        'Maestro.model.Vendedor'
    ],
    refs: [
        {ref: 'vendedor_grid', selector: 'vendedor_grid'}
    ],
    init: function () {
        var me = this;

        me.control({
            'vendedor_grid button[action=adicionar]': {
                click: me.onAddClick
            }
        });

    },

    onAddClick: function (btn) {
        var rec = new Maestro.model.Vendedor({
            codigo: '',
            nombre: ''
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_vendedor');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});