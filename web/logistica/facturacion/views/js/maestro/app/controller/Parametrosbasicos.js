Ext.define('Maestro.controller.Parametrosbasicos', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.parametrosbasicos.Form',
        'Maestro.view.parametrosbasicos.CuentaEdit'
    ],
    stores: [
        //'Maestro.store.Cargo'
    ],
    models: [
        //'Maestro.model.Cargo'
    ],
    refs: [
        {ref: 'form_parametrosbasicos', selector: 'form_parametrosbasicos'},
        {ref: 'cuenta_edit', selector: 'cuenta_edit'}
    ],
    init: function () {
        var me = this;

        this.listen({
            component: {
                'form_parametrosbasicos': {
                    beforerender: me.loadParametros
                },
                'form_parametrosbasicos trigger[xtype="trigger"]': {
                    beforerender: me.setOnTriggerClick
                },
                'form_parametrosbasicos button[action=save]': {
                    click: me.onSaveClick
                },
                'form_parametrosbasicos checkbox[name=contabilizartrans]': {
                    change: me.onChangeContabTrans
                },
                'cuenta_edit button[action=aceptar]': {
                    click: me.setCuenta
                }
            }
        });
    },

    onSaveClick: function () {
        var me = this,
            form = this.getForm_parametrosbasicos().getForm();

        if (form.isValid()) {
            console.log('valid');
            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Guardando configuración de parámetros básicos...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                success: function (form, action) {
                    myMask.hide();
                    form.findField('idparametro').setValue(action.result.idgenerado);
                },
                failure: function (form, action) {
                    myMask.hide();
                    switch (action.failureType) {
                        case Ext.form.action.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Fallido', 'Los campos del formulario no debe ser enviados con valores no v&aacute;lidos');
                            break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Fallido', 'Comunicacion Ajax fallida');
                            break;
                        case Ext.form.action.Action.SERVER_INVALID:
                            Ext.Msg.alert('Fallido', action.result.mensaje);
                    }
                }
            });
        }
    },

    loadParametros: function (form, eOpts) {
        form.getForm().load({
            url: '/logistica/facturacion/index.php/parametrosbasicos/load',
            failure: function (form, action) {
                Ext.Msg.alert("Carga fallida", action.result.errorMessage);
            }
        });
    },

    onChangeContabTrans: function (field, newValue, oldValue) {
        if (newValue) {

        }
    },

    setOnTriggerClick: function (triggerfield, eOpts) {
        var me = this
        triggerfield.onTriggerClick = function () {
            me.createCmpCuenta.apply(me, [triggerfield])
        };
    },

    createCmpCuenta: function (triggerfield) {
        var win = Ext.widget('cuenta_edit');
        win.triggerfield = triggerfield;
        win.setTitle(perfil.etiquetas.lbWindCuenta);
    },

    setCuenta: function (btn) {
        var me = this,
            form = me.getForm_parametrosbasicos().getForm(),
            win = btn.up('cuenta_edit'),
            tree = win.down('treepanel'),
            selectedRecord = tree.getSelectionModel().getLastSelected();

        var hidden = form.findField('id' + win.triggerfield.name);
        if(hidden){
            hidden.setValue(selectedRecord.get('idcuenta'));
        }
        win.triggerfield.setValue(selectedRecord.get('text'));
        win.close();
    }

});