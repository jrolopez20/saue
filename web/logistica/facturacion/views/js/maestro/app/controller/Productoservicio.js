Ext.define('Maestro.controller.Productoservicio', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.productoservicio.GestionProdServicio',
        'Maestro.view.productoservicio.TreeClasificadores',
        'Maestro.view.productoservicio.Grid',
        'Maestro.view.productoservicio.Edit'
    ],
    stores: [
        'Maestro.store.Clasificadores',
        'Maestro.store.Productoservicio'
    ],
    models: [
        'Maestro.model.Clasificadores',
        'Maestro.model.Productoservicio'
    ],
    refs: [
        {ref: 'gestprodservicio', selector: 'gestprodservicio'},
        {ref: 'treeclasificadores', selector: 'treeclasificadores'},
        {ref: 'produtoservicio_grid', selector: 'produtoservicio_grid'},
        {ref: 'editproductoservicio', selector: 'editproductoservicio'}
    ],
    init: function () {
        var me = this;
        this.etiquetas = perfil.etiquetas;
        this.listen({
            component: {
                'treeclasificadores button[action=reload]': {
                    click: me.onReloadClas
                },
                'treeclasificadores': {
                    beforeselect: me.onBeforeSelectClas,
                    select: me.onSelectClas,
                    deselect: me.onClasDeselect
                },
                'produtoservicio_grid': {
                    disable: me.onDisableGrid,
                    selectionchange: me.onGridSelectionChange
                },
                'produtoservicio_grid button[action=reload]': {
                    click: me.onReloadGrid
                },
                'produtoservicio_grid button[action=adicionar]': {
                    click: me.onAdd
                },
                'produtoservicio_grid button[action=modificar]': {
                    click: me.onMod
                },
                'produtoservicio_grid button[action=eliminar]': {
                    click: me.onDelete
                },
                'produtoservicio_grid button[action=activar]': {
                    click: me.onActivate
                },
                'editproductoservicio button[action=aceptar], editproductoservicio button[action=aplicar]': {
                    click: me.save
                },
                'editproductoservicio button[action=cancelar]': {
                    click: me.onReloadGrid
                }
            }
        });
    },

    onReloadClas: function (btn) {
        var me = this,
            tree = btn.up('treepanel');

        me.getProdutoservicio_grid().setDisabled(true);
        //tree = me.getClasificadorprodserv_treegrid();

        tree.getStore().load();
    },
    onBeforeSelectClas: function (rowModel, record, index, eOpts) {
        var me = this,
            grid = me.getProdutoservicio_grid();

        return record.get('leaf');
    },
    onSelectClas: function (rowModel, record, index, eOpts) {
        var me = this,
            grid = me.getProdutoservicio_grid(),
            tree = me.getTreeclasificadores(),
            clasificador = tree.getSelectionModel().getLastSelected();


        grid.setDisabled(false);

        //grid.getStore().load({
        //    params: {idclasificador: clasificador.get('idclasificador')}
        //});
        grid.getStore().getProxy().extraParams = {idclasificador: clasificador.get('idclasificador')};
        grid.getStore().load();

    },
    onGridSelectionChange: function (selModel, records, eOpts) {
        var me = this,
            grid = me.getProdutoservicio_grid(),
            hasSelection = grid.getSelectionModel().hasSelection();

        grid.down('button[action="modificar"]').setDisabled(!hasSelection);
        grid.down('button[action="eliminar"]').setDisabled(!hasSelection);
        grid.down('button[action="activar"]').setDisabled(!hasSelection);

        if (records.length) {
            var type = records[0].get('activo');
            records.every(function (record) {
                if (record.get('activo') !== type) {
                    type = 2;
                    return false;
                } else {
                    return true;
                }
            });

            if (type === 2) {
                grid.down('button[action="activar"]').setIcon(perfil.dirImg + 'banderarojaverde.png');
                grid.down('button[action="activar"]').setText(perfil.etiquetas.lbBtnActivarDesactivar);
                grid.down('button[action="activar"]').setTooltip(perfil.etiquetas.ttpBtnActivarDesactivar);
            } else if (type === 1) {
                grid.down('button[action="activar"]').setIcon(perfil.dirImg + 'banderaroja.png');
                grid.down('button[action="activar"]').setText(perfil.etiquetas.lbBtnDesactivar);
                grid.down('button[action="activar"]').setTooltip(perfil.etiquetas.ttpBtnDesactivar);
            } else {
                grid.down('button[action="activar"]').setIcon(perfil.dirImg + 'banderaverde.png');
                grid.down('button[action="activar"]').setText(perfil.etiquetas.lbBtnActivar);
                grid.down('button[action="activar"]').setTooltip(perfil.etiquetas.ttpBtnActivar);
            }
        }
    },
    onDisableGrid: function (cmp, eOpts) {
        var me = this,
            grid = me.getProdutoservicio_grid();

        grid.getStore().removeAll();
    },

    onClasDeselect: function (selModel, record, index, eOpts) {
        var me = this,
            tree = me.getTreeclasificadores(),
            grid = me.getProdutoservicio_grid();

        grid.setDisabled(!tree.getSelectionModel().hasSelection());
    },

    onReloadGrid: function (btn) {
        var me = this,
            grid = me.getProdutoservicio_grid();
        //grid = btn.up('produtoservicio_grid'); Esto lo comente porque esta misma funcionalidad se llama en el cancelar del add

        grid.getStore().reload();
    },

    onAdd: function (btn) {
        var me = this,
            tree = me.getTreeclasificadores();

        if (tree.getSelectionModel().hasSelection()) {
            var win = Ext.widget('editproductoservicio'),
                form = win.down('form');
            win.setTitle(perfil.etiquetas.titleWinAdd);
            win.down('button[action=aplicar]').show();

            var selected = tree.getSelectionModel().getLastSelected();
            if (selected) {
                var tipo = selected.get('tipo');
                form.getForm().findField('tipo').setValue(tipo);
                win.down('combobox[name="tipo"]').setReadOnly(true);

                if(tipo == 2){ // si es un servicio oculta el codigo de barras y la disponiblidad
                    form.getForm().findField('codigobarra').hide();
                    form.getForm().findField('stock').hide();
                }
            }
        }
    },

    onMod: function (btn) {
        var me = this,
            grid = me.getProdutoservicio_grid();


        if (grid.getSelectionModel().hasSelection()) {
            var win = Ext.widget('editproductoservicio'),
                form = win.down('form');
            win.setTitle(perfil.etiquetas.titleWinMod);
            var record = grid.getSelectionModel().getLastSelected();

            if(record.get('tipo') == 2){ // si es un servicio oculta el codigo de barras y la disponiblidad
                form.getForm().findField('codigobarra').hide();
                form.getForm().findField('stock').hide();
            }

            var foto = record.get('foto');
            if (!Ext.isEmpty(foto)) {
                form.down('image[name=foto]').setSrc(foto);
            }

            form.loadRecord(record);
        }
    },

    save: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm(),
            tree = me.getTreeclasificadores(),
            clasificador = tree.getSelectionModel().getLastSelected();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: me.etiquetas.lbGuardando});
            myMask.show();

            form.submit({
                submitEmptyText: false,
                params: {
                    foto: win.down('form').down('image[name=foto]').src,
                    idclasificador: clasificador.get('idclasificador')
                },
                success: function (response, action) {
                    // En action.result viene el objeto que se devuelve en el controller
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getProdutoservicio_grid().getStore().reload();
                    }
                    else {
                        //antes de resetaer el formulario se capturar el tipo y setearlo nuevamente una vez reseteados todos los campos
                        var cbxtipo = form.findField('tipo'),
                            tipo = cbxtipo.getValue();

                        win.down('form').down('image[name=foto]').setSrc(win.DEFAULT_IMAGE);
                        form.reset();

                        cbxtipo.setValue(tipo);
                    }
                },
                failure: function (response, action) {
                    myMask.hide();
                    switch (action.failureType) {
                        case Ext.form.action.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Fallido', 'Los campos del formulario no debe ser enviados con valores no v&aacute;lidos');
                            break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Fallido', 'Comunicacion Ajax fallida');
                            break;
                        case Ext.form.action.Action.SERVER_INVALID:
                            Ext.Msg.alert('Fallido', action.result.mensaje);
                    }
                }
            });
        }
    },

    onActivate: function (btn) {
        //var me = this,
        var grid = btn.up('gridpanel'),
            selection = grid.getSelectionModel().getSelection(),
            ids = [];

        selection.every(function (record) {
            ids.push(record.get('idproductoservicio'));
            return true;
        });

        var myMask = new Ext.LoadMask(grid, {msg: perfil.etiquetas.loading});
        myMask.show();
        Ext.Ajax.request({
            url: '/logistica/facturacion/index.php/productoservicio/changestate',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                mostrarMensaje(1, perfil.etiquetas.msgOK);
                grid.getStore().reload();
            }
        });
    },

    onDelete: function (btn) {
        var me = this,
            grid = btn.up('gridpanel'),
            selection = grid.getSelectionModel().getSelection(),
            ids = [];

        if (selection.length) {
            var msg = selection.length > 1 ?
                perfil.etiquetas.msgConfirmDelP :
                perfil.etiquetas.msgConfirmDel
            Ext.Msg.confirm(perfil.etiquetas.lbConfirm, msg,
                function (btn) {
                    if (btn === 'yes') {
                        var ids = [];
                        selection.every(function (record) {
                            ids.push(record.get('idproductoservicio'));
                            return true;
                        });
                        me.delete(ids, me);
                    }
                });
        }
    },

    delete: function (ids, me) {
        var grid = me.getProdutoservicio_grid();
        var myMask = new Ext.LoadMask(grid, {msg: perfil.etiquetas.loading});
        myMask.show();
        Ext.Ajax.request({
            url: '/logistica/facturacion/index.php/productoservicio/delete',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                if (responseData.errors && responseData.errors.length > 0) {
                    mostrarMensaje(3, perfil.etiquetas.msgConfirmDelFail);
                } else {
                    mostrarMensaje(1, perfil.etiquetas.msgConfirmDelOk);
                }
                grid.getStore().reload();
            }
        });
    }

});