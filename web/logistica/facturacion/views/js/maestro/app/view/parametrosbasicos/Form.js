Ext.define('Maestro.view.parametrosbasicos.Form', {
    extend: 'Ext.form.Panel',
    alias: 'widget.form_parametrosbasicos',
    url: '/logistica/facturacion/index.php/parametrosbasicos/save',
    title: 'Pármetros básicos',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    bodyPadding: 10,
    fieldDefaults: {
        labelAlign: 'top'
    },
    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'hidden',
                name: 'idparametro'
            },
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaultType: 'textfield',
                fieldDefaults: {
                    labelAlign: 'top',
                    maskRe: /^[0-9]+$/,
                    regex: /^[0-9]+$/
                },
                items: [
                    {
                        flex: 1,
                        name: 'ultimonumfactura',
                        fieldLabel: 'Ultimo Num. factura',
                        allowBlank: false,
                        margins: '0 5 0 0'
                    }, {
                        flex: 1,
                        name: 'ultimonumpedido',
                        fieldLabel: 'Ultimo Num. pedido',
                        margins: '0 5 0 0'
                    }, {
                        flex: 1,
                        name: 'ultimonumnotaventa',
                        fieldLabel: 'Ultimo Num. nota de venta',
                        margins: '0 5 0 0'
                    }, {
                        flex: 1,
                        name: 'ultimonumdevolucion',
                        fieldLabel: 'Ultimo Num. devolución'
                    }
                ]
            },
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                fieldDefaults: {
                    labelAlign: 'top'
                },
                items: [
                    {
                        xtype: 'numberfield',
                        name: 'ivavigente',
                        fieldLabel: 'IVA vigente',
                        margins: '0 5 0 0',
                        width: 120,
                        minValue: 0,
                        decimalSeparator: '.',
                        fieldStyle: 'text-align:right'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idcuentaiva'
                    }, {
                        xtype: 'trigger',
                        name: 'cuentaiva',
                        fieldLabel: 'Cuenta de IVA',
                        editable: false,
                        triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                        emptyText: 'Seleccione una cuenta...',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'checkbox',
                name: 'contabilizartrans',
                boxLabel: 'Contabilizar transacciones',
                inputValue: '1'
            },
            {
                xtype: 'container',
                anchor: '100%',
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    flex: 1,
                    layout: 'anchor',
                    margins: '0 5 0 0',
                    items: [
                        {
                            xtype: 'hidden',
                            name: 'idcuentadescuento'
                        }, {
                            xtype: 'trigger',
                            name: 'cuentadescuento',
                            fieldLabel: 'Cuenta de descuento',
                            editable: false,
                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                            emptyText: 'Seleccione una cuenta...',
                            anchor: '100%'
                        },{
                            xtype: 'hidden',
                            name: 'idcuentaventa'
                        },{
                            xtype: 'trigger',
                            name: 'cuentaventa',
                            fieldLabel: 'Cuenta de venta',
                            editable: false,
                            triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                            emptyText: 'Seleccione una cuenta...',
                            anchor: '100%'
                        }]
                }, {
                    xtype: 'container',
                    flex: 1,
                    layout: 'anchor',
                    items: [{
                        xtype: 'hidden',
                        name: 'idcuentaflete'
                    }, {
                        xtype: 'trigger',
                        name: 'cuentaflete',
                        fieldLabel: 'Cuenta de flete',
                        anchor: '100%',
                        editable: false,
                        triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                        emptyText: 'Seleccione una cuenta...'
                    }, {
                        xtype: 'hidden',
                        name: 'idcuentadevolucion'
                    }, {
                        xtype: 'trigger',
                        name: 'cuentadevolucion',
                        fieldLabel: 'Cuenta de devolucion',
                        anchor: '100%',
                        editable: false,
                        triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                        emptyText: 'Seleccione una cuenta...'
                    }]
                }]
            }
        ];
        this.buttons = [
            {
                icon: perfil.dirImg + 'guardar.png',
                iconCls: 'btn',
                //text: perfil.etiquetas.lbBtnGuardar,
                text: 'Guardar',
                action: 'save'
            }
        ];
        this.callParent(arguments);
    }
});