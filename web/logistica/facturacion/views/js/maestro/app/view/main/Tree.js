Ext.define('Maestro.view.main.Tree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.main_tree',

    title: 'Conceptos',
    rootVisible: false,
    plugins: [{
        ptype: 'treefilter',
        allowParentFolders: true
    }],
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            store: new Ext.data.TreeStore({
                proxy: {
                    type: 'ajax',
                    url: '/logistica/facturacion/views/js/maestro/data/concepts.json'
                },
                folderSort: true,
                //sorters: [{
                //    property: 'text',
                //    direction: 'ASC'
                //}]
            }),
            tbar: [{
                xtype: 'trigger',
                emptyText: 'Buscar...',
                width: 240,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function () {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function (f, e) {
                        var me = this,
                            tree = me.up('treepanel');
                        if (e.getKey() == e.ENTER) {
                            tree.getStore().load({
                                params: {
                                    filter: f.getValue()
                                }
                            });
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    },
                    change: function (field, newVal) {
                        var tree = field.up('treepanel');
                        tree.filter(newVal, 'text');
                        if (newVal) {
                            this.triggerCell.item(0).setDisplayed(true);
                        } else {
                            this.triggerCell.item(0).setDisplayed(false);
                        }
                        this.updateLayout();
                    }, buffer: 250
                },

                onTrigger1Click: function () {
                    this.reset();
                    this.triggerCell.item(0).setDisplayed(false);
                    this.updateLayout();
                    this.focus();
                },

                onTrigger2Click: function () {
                    var newVal = this.getValue(),
                        tree = this.up('treepanel');

                    tree.filter(newVal, 'text');
                    if (newVal) {
                        this.triggerCell.item(0).setDisplayed(true);
                    } else {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                    this.updateLayout();
                }
            }]
        });

        this.callParent(arguments);
    },
    onExpandAllClick: function () {
        var me = this,
            toolbar = me.down('toolbar');

        me.getEl().mask('Expanding tree...');
        toolbar.disable();

        this.expandAll(function () {
            me.getEl().unmask();
            toolbar.enable();
        });
    },

    onCollapseAllClick: function () {
        var toolbar = this.down('toolbar');

        toolbar.disable();
        this.collapseAll(function () {
            toolbar.enable();
        });
    }
});