Ext.define('Maestro.view.productoservicio.TreeClasificadores', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.treeclasificadores',
    region: 'center',

    rootVisible: false,
    useArrows: true,
    store: 'Maestro.store.Clasificadores',

    initComponent: function () {
        var me = this;
        this.etiquetas = perfil.etiquetas;

        me.bbar = [
            '->',
            {
                text: me.etiquetas.lbBtnReload,
                tooltip: me.etiquetas.lbBtnReload,
                icon: perfil.dirImg + 'actualizar.png',
                iconCls: 'btn',
                action: 'reload'
            }
        ]

        this.callParent(arguments);
    }

});