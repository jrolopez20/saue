Ext.define('Maestro.view.clasificadorprodserv.EditClasificador', {
    extend: 'Ext.window.Window',
    alias: 'widget.editclasificador',
    layout: 'fit',
    url: '/logistica/facturacion/index.php/clasificadores/',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 450,
    initComponent: function () {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: this.url + 'save',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%'
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                //{
                //    xtype: 'combobox',
                //    name: 'idformato',
                //    fieldLabel: 'Seleccione el formato',
                //    store: 'GrupoContable.store.Formato',
                //    queryMode: 'local',
                //    displayField: 'nombre',
                //    valueField: 'idformato',
                //    hidden: true,
                //    disabled:true,
                //    editable: false,
                //    allowBlank: false
                //},
                {
                    xtype: 'hidden',
                    name: 'idclasificador'
                },
                {
                    xtype: 'hidden',
                    name: 'idpadre'
                },
                {
                    xtype: 'hidden',
                    name: 'nivel'
                },
                {
                    xtype: 'hidden',
                    name: 'activo',
                    value: '1'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [
                        {
                            flex: 1,
                            xtype: 'textfield',
                            maskRe: /^[0-9a-zA-Z_]+$/,
                            regex: /^[0-9a-zA-Z_]+$/,
                            name: 'codigo',
                            fieldLabel: perfil.etiquetas.lbCodigo,
                            allowBlank: false
                        }, {
                            xtype: 'combobox',
                            name: 'tipo',
                            margin: '0 0 0 5',
                            fieldLabel: 'Tipo',
                            store: Ext.create('Ext.data.Store', {
                                fields: ['idtipo', 'nombre'],
                                data : [
                                    {"idtipo": 1, "nombre":"Producto"},
                                    {"idtipo": 2, "nombre":"Servicio"},
                                ]
                            }),
                            flex: 1,
                            queryMode: 'local',
                            displayField: 'nombre',
                            valueField: 'idtipo',
                            editable: false,
                            allowBlank: false
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'denominacion',
                    fieldLabel: perfil.etiquetas.lbDenominacion,
                    maxLength: 255,
                    allowBlank: false
                },
                {
                    xtype: 'textarea',
                    name: 'descripcion',
                    fieldLabel: perfil.etiquetas.lbDescripcion,
                    maxLength: 255
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});