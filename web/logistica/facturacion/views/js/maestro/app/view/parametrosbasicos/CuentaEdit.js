Ext.define('Maestro.view.parametrosbasicos.CuentaEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.cuenta_edit',
    layout: 'border',
    modal: true,
    resizable: true,
    maximizable: true,
    autoShow: true,
    width: 800,
    height: 500,
    initComponent: function () {

        this.items = [{
            xtype: 'cuenta_tree',
            idnaturaleza: [8030,8031],
            anySelection: false,
            region: 'center',
            margins: '5 5 5 5'
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});