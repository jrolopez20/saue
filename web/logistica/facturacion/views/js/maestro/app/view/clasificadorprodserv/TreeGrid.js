Ext.define('Maestro.view.clasificadorprodserv.TreeGrid', {
    extend: 'Ext.tree.Panel',
    store: 'Maestro.store.Clasificadores',
    alias: 'widget.clasificadorprodserv_treegrid',

    title: 'Categor&iacute;as de productos y servicios',
    useArrows: true,
    rootVisible: false,
    multiSelect: true,
    plugins: [{
        ptype: 'treefilter',
        allowParentFolders: true
    }],
    initComponent: function () {
        var me = this;
        this.etiquetas = perfil.etiquetas;

        me.columns = [
            {
                xtype: 'treecolumn',
                text: 'Denominaci&oacute;n',
                flex: 2,
                sortable: true,
                dataIndex: 'text'
            },
            {
                text: 'Activo',
                dataIndex: 'activo',
                align: 'center',
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    var dir = (value == 1) ? '/logistica/facturacion/views/images/active.png'
                        : '/logistica/facturacion/views/images/inactive.png';
                    return '<img height="16" width="16" src="' + dir +'" />'
                }
                //flex: 1,
                //stopSelection: true,
                //xtype: 'checkcolumn',
                //disabled: true,
            }

        ];

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar clasificador de productos o servicios',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                tooltip: 'Modificar clasificador de productos o servicios',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar clasificador de productos o servicios',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, {
                text: perfil.etiquetas.lbBtnDesactivar,
                tooltip: perfil.etiquetas.ttpBtnDesactivar,
                icon: perfil.dirImg + 'banderaroja.png',
                iconCls: 'btn',
                disabled: true,
                action: 'activar'
            }, {
                text: perfil.etiquetas.lbBtnReload,
                tooltip: perfil.etiquetas.ttpBtnReload,
                icon: perfil.dirImg + 'actualizar.png',
                iconCls: 'btn',
                action: 'reload'
            }, '->',
            {
                xtype: 'trigger',
                emptyText: 'Buscar...',
                width: 250,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function () {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function (f, e) {
                        var me = this,
                            tree = me.up('treepanel');
                        if (e.getKey() == e.ENTER) {
                            tree.getStore().load({
                                params: {
                                    filter: f.getValue()
                                }
                            });
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    },
                    change: function (field, newVal) {
                        var tree = field.up('treepanel');
                        tree.filter(newVal, 'concat');
                        if (newVal) {
                            this.triggerCell.item(0).setDisplayed(true);
                        } else {
                            this.triggerCell.item(0).setDisplayed(false);
                        }
                        this.updateLayout();
                    }, buffer: 250
                },

                onTrigger1Click: function () {
                    this.reset();
                    this.triggerCell.item(0).setDisplayed(false);
                    this.updateLayout();
                    this.focus();
                },

                onTrigger2Click: function () {
                    var newVal = this.getValue(),
                        tree = this.up('treepanel');

                    tree.filter(newVal, 'concat');
                    if (newVal) {
                        this.triggerCell.item(0).setDisplayed(true);
                    } else {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                    this.updateLayout();
                }
            }
        ];

        this.callParent(arguments);
    }
});