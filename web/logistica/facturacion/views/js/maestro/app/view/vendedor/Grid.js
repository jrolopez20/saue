Ext.define('Maestro.view.vendedor.Grid', {
    extend: 'Ext.grid.Panel',
    store: 'Maestro.store.Vendedor',
    alias: 'widget.vendedor_grid',
    title: 'Vendedores',
    selType: 'rowmodel',
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            pluginId: 'rowEditor_vendedor',
            clicksToEdit: 2,
            errorSummary: false,
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        context.grid.getStore().remove(context.record);
                    }
                }
            }
        })
    ],
    initComponent: function () {
        var me = this;

        me.columns = [
            {
                text: 'Código',
                dataIndex: 'codigo',
                width: 90,
                field: {
                    xtype: 'textfield',
                    allowBlank: false,
                    maxLength: 255,
                    minLength: 3
                }
            },
            {
                text: 'Nombre',
                dataIndex: 'fullname',
                width: 400,
                field: {
                    xtype: 'combo',
                    store: 'Maestro.store.Persona',
                    name: 'fullname',
                    allowBlank: false,
                    valueField: 'fullname',
                    displayField: 'fullname',
                    queryMode: 'remote',
                    typeAhead: false,
                    pageSize: 20,
                    listeners: {
                        select: function(cb, records){
                            me.getSelectionModel().getLastSelected().set('idpersona', records[0].get('idpersona'));
                        }
                    },
                    listConfig: {
                        getInnerTpl: function() {
                            return '<div data-qtip="<img src={foto} width=60 height=60 />">{numid} {fullname}</div>';
                        }
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: 'Eliminar vendedor',
                    handler: function (grid, rowIndex) {
                        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el vendedor seleccionado?',
                            function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().removeAt(rowIndex);
                                }
                            }
                        );
                    }
                }]
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar vendedor',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            '->',
            {
                xtype: 'searchfield',
                store: 'Maestro.store.Vendedor',
                emptyText: 'Buscar...',
                width: 250,
                filterPropertysNames: ['filtro']
            }
        ];

        this.callParent(arguments);
        this.getStore().load();
    }
});