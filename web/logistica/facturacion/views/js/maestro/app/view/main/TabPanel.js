Ext.define('Maestro.view.main.TabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.main_tabpanel',

    activeTab: 0,
    items: [
        {
            title: 'Inicio',
            layout: 'fit',
            html: '<div class="empty-info-area">Seleccione algún concepto de la izquierda para gestionar su contenido</div>'
        }
    ],
    initComponent: function () {
        var me = this;


        this.callParent(arguments);
    }
});