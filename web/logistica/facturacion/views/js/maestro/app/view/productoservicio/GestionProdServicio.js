Ext.define('Maestro.view.productoservicio.GestionProdServicio', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.gestprodservicio',
    layout: 'border',

    initComponent: function () {
        var me = this;
        this.etiquetas = perfil.etiquetas;
        this.title = this.etiquetas.lbProdServ,

        me.items = [
            {
                region: 'west',
                title: this.etiquetas.lbCategorias,
                margin: '5 5 5 5',
                width: 250,
                layout: 'border',
                items: [
                    {
                        xtype: 'treeclasificadores'
                    }
                ],
                collapsible: true
            },
            {
                region: 'center',
                xtype: 'produtoservicio_grid',
                margin: '5 5 5 0',
                layout: 'border'
            }
        ]

        this.callParent(arguments);
    }
});