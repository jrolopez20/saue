var perfil = window.parent.UCID.portal.perfil;
Ext.application({
    name: 'Pedido',
    enableQuickTips: true,
    paths: {
        'Pedido': '../../views/js/pedido/app'
    },
    controllers: [
        'Pedido'
    ],
    launch: function () {
        UCID.portal.cargarEtiquetas('pedido', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [{
                    region: 'center',
                    xtype: 'grid_pedido',
                    margin: '5 5 5 5'
                }]
            });
        });
    }
});