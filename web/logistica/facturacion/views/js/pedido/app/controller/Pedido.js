Ext.define('Pedido.controller.Pedido', {
    extend: 'Ext.app.Controller',

    views: [
        'Pedido.view.Grid',
        'Pedido.view.Edit'
    ],
    stores: [
        'Pedido.store.Pedido',
        'Pedido.store.TerminoPago',
        'Pedido.store.Cliente'
    ],
    models: [
        'Pedido.model.Pedido'
    ],
    refs: [
        {ref: 'grid_pedido', selector: 'grid_pedido'},
        {ref: 'view_productoservicio', selector: 'view_productoservicio'},
        {ref: 'edit_pedido', selector: 'edit_pedido'},
        {ref: 'grid_itemcart', selector: 'grid_itemcart'}
    ],

    editWindow: null,

    init: function () {
        var me = this;

        me.control({
            'grid_pedido': {
                selectionchange: me.onPedidoSelectionChange
            },
            'grid_pedido button[action=adicionar]': {
                click: me.onAddClick
            },
            'grid_pedido button[action=modificar]': {
                click: me.onModClick
            },
            'grid_pedido button[action=eliminar]': {
                click: me.onDelClick
            },
            'grid_pedido button[action=imprimir]': {
                click: me.onPrintClick
            },
            'view_productoservicio dataview': {
                itemdblclick: me.onProductoServicioDblClick
            },
            'edit_pedido numberfield[name=flete]': {
                change: me.updateTotalInfo
            },
            'edit_pedido button[action=aplicar]': {
                click: me.savePedido
            },
            'edit_pedido button[action=aceptar]': {
                click: me.savePedido
            },
            'grid_itemcart': {
                addItem: me.updateArticleCartRecord,
                updateItem: me.updateArticleCartRecord,
                datachangedItem: me.updateTotalInfo,
                onItemDroped: me.onItemDroped
            }
        });

    },

    onPedidoSelectionChange: function () {
        var me = this,
            grid = me.getGrid_pedido();
        grid.down('button[action=modificar]').disable();
        grid.down('button[action=eliminar]').disable();
        grid.down('button[action=imprimir]').disable();

        if (grid.getSelectionModel().hasSelection()) {
            if(grid.getSelectionModel().getCount( ) == 1){
                grid.down('button[action=modificar]').enable();
                grid.down('button[action=eliminar]').enable();
            }
            grid.down('button[action=imprimir]').enable();
        }
    },

    onAddClick: function (btn) {
        var me = this;
        if (!me.editWindow) {
            me.editWindow = Ext.widget('edit_pedido');
        }
        me.editWindow.show();
        me.editWindow.maximize();

        var myMask = new Ext.LoadMask(me.editWindow, {msg: 'Cargando último número de pedido...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'getUltimoNumPedido',
            method: 'POST',
            callback: function (options, success, response) {
                myMask.hide();
                var rpse = Ext.decode(response.responseText);
                if (rpse.ultimonumpedido) {
                    me.resetEditWindow();

                    me.editWindow.setTitle('Adicionar pedido');
                    me.editWindow.down('button[action=aplicar]').show();
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('numero').setValue(rpse.ultimonumpedido);
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('display_numero').setValue(rpse.ultimonumpedido);
                }
            }
        });
    },

    onModClick: function (btn) {
        var me = this,
            grid = me.getGrid_pedido();

        if (grid.getSelectionModel().hasSelection()) {
            if (!me.editWindow) {
                me.editWindow = Ext.widget('edit_pedido');
            }
            me.editWindow.show();
            me.editWindow.maximize();
            me.resetEditWindow();

            var myMask = new Ext.LoadMask(me.editWindow, {msg: 'Cargando datos del pedido...'});
            myMask.show();
            var stDetallePedido = me.getGrid_itemcart().getStore(),
                recordPedido = grid.getSelectionModel().getLastSelected();
            Ext.Ajax.request({
                url: 'getDetallePedido',
                method: 'POST',
                params: {
                    idpedido: recordPedido.get('idpedido')
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);

                    Ext.each(rpse.DatPedidoProducto, function (producto) {
                        stDetallePedido.add({
                            idproductoservicio: producto.idproductoservicio,
                            codigo: producto.NomProductoservicio.codigo,
                            denominacion: producto.NomProductoservicio.denominacion,
                            preciounitario: producto.NomProductoservicio.preciounitario,
                            cantidad: producto.cantidad,
                            descuento: producto.descuento,
                            iva: producto.NomProductoservicio.iva
                        });
                    });

                    me.editWindow.setTitle('Modificar pedido');
                    me.editWindow.down('button[action=aplicar]').hide();
                    me.editWindow.down('form[itemId=basicData]').getForm().setValues(recordPedido.getData());
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('display_numero').setValue(recordPedido.get('numero'));
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('idcliente').setValue(rpse.idcliente);
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('idterminodepago').setValue(rpse.idterminodepago);
                }
            });
        }
    },

    savePedido: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form[itemId=basicData]').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando datos del pedido...'});
            myMask.show();
            var recItems = me.getGrid_itemcart().getStore().getRange();
            var items = new Array();
            Ext.each(recItems, function (rec) {
                items.push({
                    idproductoservicio: rec.get('idproductoservicio'),
                    cantidad: rec.get('cantidad'),
                    descuento: rec.get('descuento'),
                    iva: rec.get('iva'),
                    importe: rec.get('importe')
                });
            });

            form.submit({
                submitEmptyText: false,
                params: {
                    items: Ext.encode(items)
                },
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getGrid_pedido().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    onDelClick: function (btn) {
        var me = this,
            grid = me.getGrid_pedido();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el pedido seleccionado?', function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando pedido...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deletePedido',
                        method: 'POST',
                        params: {
                            idpedido: record.get('idpedido')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.down('button[action=modificar]').disable();
                                grid.down('button[action=eliminar]').disable();
                                grid.getStore().reload();
                            }
                        }
                    });
                }
            });
        }
    },

    onPrintClick: function (btn) {
        var me = this,
            smPedido = me.getGrid_pedido().getSelectionModel();
        if (smPedido.hasSelection()) {
            var records = smPedido.getSelection();

            var arrIdPedido = new Array();

            Ext.each(records, function (record) {
                arrIdPedido.push(record.get('idpedido'));
            });

            var params = Ext.urlEncode({
                arrIdPedido: Ext.encode(arrIdPedido)
            });

            var win = new Ext.Window({
                title: 'Pedido',
                width: 800,
                height: 500,
                maskDisabled: false,
                animCollapse: false,
                maximizable: true,
                constrainHeader: true,
                border: false,
                modal: true,
                items: {
                    xtype: 'component',
                    autoEl: {
                        tag: 'iframe',
                        style: 'height: 100%; width: 100%; border: none',
                        src: document.location.protocol + '//' + document.location.host +
                        '/logistica/facturacion/index.php/pedido/imprimirPedido?' + params
                    }
                }
            }).show();
        }
    },

    resetEditWindow: function (win) {
        this.editWindow.down('form[itemId=basicData]').getForm().reset();
        this.editWindow.down('form[itemId=formTotals]').getForm().reset();
        this.getGrid_itemcart().getStore().removeAll();
    },

    /**
     * Adiciona un nuevo articulo al carrito al hacer doble click
     * @param view
     * @param record
     */
    onProductoServicioDblClick: function (view, record) {
        this.addCartItem(record);
    },

    /**
     * Adiciona un nuevo articulo al carrito al arrastrar y soltar
     * @param target
     * @param dd
     * @param e
     * @param data
     */
    onItemDroped: function (target, dd, e, data) {
        this.addCartItem(data.record);
    },

    /**
     * Adiciona un record a la carrito
     * @param record
     */
    addCartItem: function (record) {
        var gpDetallePedido = this.getGrid_itemcart(),
            stDetallePedido = gpDetallePedido.getStore();

        var index = stDetallePedido.find('idproductoservicio', record.get('idproductoservicio'));
        if (index == -1) {
            stDetallePedido.insert(0, {
                idproductoservicio: record.get('idproductoservicio'),
                codigo: record.get('codigo'),
                denominacion: record.get('denominacion'),
                preciounitario: record.get('preciounitario'),
                cantidad: 1,
                descuento: 0.00,
                iva: record.get('iva')
            });
            gpDetallePedido.getSelectionModel().selectRange(0, 0);
        } else {
            var articleRec = stDetallePedido.getAt(index);
            articleRec.set('cantidad', articleRec.get('cantidad') + 1);
            gpDetallePedido.getSelectionModel().select(articleRec);
        }
    },

    updateArticleCartRecord: function (st, record, operation, e) {
        if (Ext.isArray(record)) {
            record = record[0];
        }

        var porcientoDescuento = Ext.util.Format.round((record.get('preciounitario') * record.get('descuento') / 100), 2);
        var porcientoIva = Ext.util.Format.round((record.get('preciounitario') * record.get('iva') / 100), 2);
        var importe = Ext.util.Format.round(
            (record.get('preciounitario') - porcientoDescuento + porcientoIva) * record.get('cantidad'), 2
        );

        //Modifica el importe
        record.set('importe', importe);

        this.updateTotalInfo();
    },

    updateTotalInfo: function () {
        var stArticlesCart = this.getGrid_itemcart().getStore();
        var records = stArticlesCart.getRange(),
            subtotal = 0,
            descuento = 0,
            impuesto = 0,
            importe = 0,
            totalArticles = 0,
            flete = this.getEdit_pedido().down('numberfield[name=flete]').getValue();

        var formTotals = this.getEdit_pedido().down('form[itemId=formTotals]');


        Ext.each(records, function (record) {
            // Subtotal
            subtotal += Ext.util.Format.round(record.get('cantidad') * record.get('preciounitario'), 2);

            // Descuento
            if (record.get('descuento') > 0) {
                var porcientoDescuento = (record.get('preciounitario') * record.get('cantidad')) * record.get('descuento') / 100;
                descuento += Ext.util.Format.round(porcientoDescuento, 2);
            }

            //IVA 12%
            if (record.get('iva') > 0) {
                impuesto += Ext.util.Format.round(
                    (record.get('cantidad') * record.get('preciounitario') * record.get('iva') / 100), 2
                );
            }

            //Cantidad total de articulos
            totalArticles += record.get('cantidad');

            //VALOR TOTAL
            importe += Ext.util.Format.round(
                (record.get('importe')), 2
            );
        });

        var objTotals = {
            subtotal: Ext.util.Format.round(subtotal, 2),
            descuento: Ext.util.Format.round(descuento, 2),
            impuesto: Ext.util.Format.round(impuesto, 2),
            importe: Ext.util.Format.round(importe + flete, 2),
            totalArticles: Ext.util.Format.round(totalArticles, 2)
        };

//        //Actualiza el formulario de los totales con los nuevos datos
        formTotals.getForm().setValues(objTotals);
    }
});