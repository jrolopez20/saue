Ext.define('Pedido.model.Pedido', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idpedido', type: 'int'},
        {name: 'numero', type: 'int'},
        {name: 'fecha', type: 'date'},
        {name: 'idcliente', type: 'int'},
        {name: 'cliente', type: 'string'},
        {name: 'idusuario', type: 'int'},
        {name: 'usuario', type: 'string'},
        {name: 'idterminodepago', type: 'int'},
        {name: 'terminodepago', type: 'string', mapping: 'NomTerminosPago.terminodepago'},
        {name: 'flete', type: 'int'},
        {name: 'estado', type: 'int'}
    ]
});