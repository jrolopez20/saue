Ext.define('Pedido.store.Pedido', {
    extend: 'Ext.data.Store',
    model: 'Pedido.model.Pedido',

    pageSize: 25,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'getPedidos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});