Ext.define('Pedido.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_pedido',

    store: 'Pedido.store.Pedido',

    title: 'Listado de pedidos',
    selModel: {
        mode : 'MULTI'
    },
    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar pedido',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                tooltip: 'Modificar pedido',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'modificar',
                disabled: true
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar pedido',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'eliminar',
                disabled: true
            },
            {
                text: 'Imprimir',
                tooltip: 'Imprimir pedido',
                icon: perfil.dirImg + 'imprimir.png',
                iconCls: 'btn',
                action: 'imprimir',
                disabled: true
            },
            '->',
            {
                xtype: 'searchfield',
                store: 'Pedido.store.Pedido',
                emptyText: 'Buscar...',
                width: 250,
                maskRe: /^[0-9]+$/,
                regex: /^[0-9]+$/,
                filterPropertysNames: ['numero']
            }
        ];

        me.columns = [
            {
                header: 'Numero',
                dataIndex: 'numero',
                width: 110
            },
            {
                header: 'Fecha',
                dataIndex: 'fecha',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            },
            {
                header: 'Cliente',
                dataIndex: 'cliente',
                flex: 1
            },
            {
                header: 'Términos de pago',
                dataIndex: 'terminodepago',
                flex: 1
            },
            {
                header: 'Usuario',
                dataIndex: 'usuario',
                width: 110
            },
            {
                header: 'Estado',
                dataIndex: 'estado',
                width: 80,
                renderer: function (v) {
                    if (v) {
                        return '<b>Abierto</b>';
                    } else {
                        return '<b>Cerrado</b>';
                    }
                }
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});