Ext.define('ItemCart', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_itemcart',

    initComponent: function () {
        var me = this;

        me.store = Ext.create('Ext.data.Store', {
            extend: 'Ext.data.Store',
            fields: [
                {
                    name: 'idproductoservicio', type: 'int'
                },
                {
                    name: 'codigo'
                },
                {
                    name: 'denominacion', type: 'string'
                },
                {
                    name: 'preciounitario', type: 'float'
                },
                {
                    name: 'cantidad', type: 'int'
                },
                {
                    name: 'descuento'
                },
                {
                    name: 'iva'
                },
                {
                    name: 'importe'
                }
            ],
            listeners: {
                add: function (st, record, operation, e) {
                    me.fireEvent('addItem', st, record, operation, e);
                },
                update: function (st, record, operation, e) {
                    me.fireEvent('updateItem', st, record, operation, e);
                },
                datachanged: function () {
                    me.fireEvent('datachangedItem', arguments);
                }
            }
        });

        this.plugins = [
            Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1
            })
        ];

        me.columns = [
            {
                text: 'Código',
                width: 110,
                sortable: true,
                dataIndex: 'codigo'
            },
            {
                text: 'Denominación',
                flex: 1,
                sortable: true,
                dataIndex: 'denominacion'
            },
            {
                text: 'Cantidad',
                width: 90,
                sortable: true,
                dataIndex: 'cantidad',
                renderer: me.showCantidad,
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    minValue: 1,
                    maxValue: 99999999,
                    fieldStyle: 'text-align:right',
                    listeners: {
                        change: function (f, newValue, oldValue, evt) {
                            me.fireEvent('onCantItemChange', me.getSelectionModel().getLastSelected(), f, newValue, oldValue, evt);
                        }
                    }
                }
            },
            {
                text: 'Precio',
                tooltip: 'Valor unitario',
                width: 80,
                sortable: true,
                dataIndex: 'preciounitario',
                renderer: me.showMoney
            },
            {
                text: '% Descuento',
                width: 85,
                sortable: true,
                dataIndex: 'descuento',
                renderer: me.showPercent,
                editor: {
                    xtype: 'numberfield',
                    value: 0,
                    minValue: 0,
                    maxValue: 99.99,
                    decimalSeparator: '.',
                    fieldStyle: 'text-align:right'
                }
            },
            {
                text: 'IVA',
                width: 75,
                sortable: true,
                dataIndex: 'iva',
                renderer: me.showPercent
            },
            {
                text: 'Importe',
                tooltip: 'Importe total',
                width: 120,
                sortable: true,
                dataIndex: 'importe',
                renderer: me.showMoney
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [
                    {
                        icon: perfil.dirImg + 'eliminar.png',
                        iconCls: 'btn',
                        tooltip: 'Eliminar elemento',
                        handler: function (grid, rowIndex) {
                            grid.getStore().removeAt(rowIndex);
                        }
                    }
                ]
            }
        ];

        this.callParent(arguments);
    },
    renderAlignRight: function (v) {
        return '<div style="text-align:right">' + v + '</div>'
    },
    showCantidad: function (val) {
        var r = null;
        if (val >= 0) {
            r = '<div style="color:black; font-weight: bold; text-align: right;">' + val + '</div>';
        } else if (val < 0) {
            r = '<div style="color:red; font-weight: bold; text-align: right;">' + val + '</div>';
        }
        return r;
    },
    showMoney: function (val) {
        var r = null;
        if (val >= 0) {
            r = '<div style="color:green; text-align: right;">$' + val + '</div>';
        } else if (val < 0) {
            r = '<div style="color:red; text-align: right;">$' + val + '</div>';
        }
        return r;
    },
    showPercent: function (val) {
        var r = '';
        if (Ext.isNumeric(val)) {
            if (val >= 0) {
                r = '<div style="color:green; text-align: right;">' + val + '%</div>';
            } else if (val < 0) {
                r = '<div style="color:red; text-align: right;">' + val + '%</div>';
            }
        }
        return r;
    }
});