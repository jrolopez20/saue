Ext.define('Devolucion.controller.Devolucion', {
    extend: 'Ext.app.Controller',

    views: [
        'Devolucion.view.Grid',
        'Devolucion.view.Edit'
    ],
    stores: [
        'Devolucion.store.Devolucion'
    ],
    models: [
        'Devolucion.model.Devolucion'
    ],
    refs: [
        {ref: 'grid_devolucion', selector: 'grid_devolucion'},
        {ref: 'edit_devolucion', selector: 'edit_devolucion'},
        {ref: 'grid_itemcart', selector: 'grid_itemcart'},
        {ref: 'comprobante', selector: 'comprobante'}
    ],

    editWindow: null,

    init: function () {
        var me = this;

        me.control({
            'grid_devolucion': {
                selectionchange: me.onDevolucionSelectionChange
            },
            'grid_devolucion button[action=adicionar]': {
                click: me.onAddClick
            },
            'grid_devolucion button[action=contabilizar]': {
                click: me.onContabilizarClick
            },
            'grid_itemcart': {
                addItem: me.updateArticleCartRecord,
                updateItem: me.updateArticleCartRecord,
                datachangedItem: me.updateTotalInfo,
                onCantItemChange: me.onCantItemChange
            },
            'edit_devolucion button[action=aplicar]': {
                click: me.saveDevolucion
            },
            'edit_devolucion button[action=aceptar]': {
                click: me.saveDevolucion
            },
            'edit_devolucion triggerfield[name=numfactura]': {
                onTriggerClick: me.onSearchFactura,
                specialkey: me.onSearchFacturaEnter
            },
            'edit_devolucion grid[itemId=grid_facturaproducto]': {
                itemdblclick: me.onProductoServicioDblClick
            },
            'comprobante button[action=aceptar]': {
                click: me.onAceptComprobante
            }
        });

    },

    onDevolucionSelectionChange: function () {
        var me = this,
            grid = me.getGrid_devolucion();
        grid.down('button[action=imprimir]').disable();
        grid.down('button[action=contabilizar]').disable();

        if (grid.getSelectionModel().hasSelection()) {
            var record = grid.getSelectionModel().getLastSelected();
            //Verifica que no exista ninguna factura seleccionada que ya este contabilizada
            if (record.get('estado') == 0) {
                grid.down('button[action=contabilizar]').enable();
            }
            grid.down('button[action=imprimir]').enable();
        }
    },

    onAddClick: function () {
        var me = this;
        if (!me.editWindow) {
            me.editWindow = Ext.widget('edit_devolucion');
        }
        me.editWindow.show();
        me.editWindow.maximize();

        var myMask = new Ext.LoadMask(me.editWindow, {msg: 'Cargando último número de devolucion...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'getUltimoNumDevolucion',
            method: 'POST',
            callback: function (options, success, response) {
                myMask.hide();
                var rpse = Ext.decode(response.responseText);
                if (rpse.ultimonumdevolucion) {
                    me.resetEditWindow();

                    me.editWindow.setTitle('Adicionar devolución');
                    me.editWindow.down('button[action=aplicar]').show();
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('numero').setValue(rpse.ultimonumdevolucion);
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('display_numero').setValue(rpse.ultimonumdevolucion);
                }
            }
        });
    },

    onContabilizarClick: function (btn) {
        var me = this,
            smDevolucion = me.getGrid_devolucion().getSelectionModel();
        if (smDevolucion.hasSelection()) {
            var myMask = new Ext.LoadMask(me.getGrid_devolucion(), {msg: 'Contabilizando devolución...'});
            myMask.show();

            Ext.Ajax.request({
                url: 'loadDataContabilizar',
                method: 'POST',
                params: {
                    iddevolucion: smDevolucion.getLastSelected().get('iddevolucion')
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);

                    if (rpse.code == 2) {
                        var win = Ext.widget('comprobante', {
                            title: 'Contabilizar devolución'
                        });
                        win.stTipoDiario.on('beforeload', function (st, operation, Opts) {
                            operation.params.idsubsistema = 7;
                        });
                        win.down('form').getForm().setValues(rpse);
                        win.down('grid').getStore().loadData(rpse.pases);
                    }
                }
            });

        }
    },

    resetEditWindow: function (win) {
        this.editWindow.down('form[itemId=basicData]').getForm().reset();
        this.editWindow.down('form[itemId=formTotals]').getForm().reset();
        this.editWindow.down('grid[itemId=grid_facturaproducto]').getStore().removeAll();
        this.getGrid_itemcart().getStore().removeAll();
    },

    /**
     * Adiciona un nuevo articulo al carro de devoluciones al hacer doble click
     * @param view
     * @param record
     */
    onProductoServicioDblClick: function (view, record) {
        this.addCartItem(record);
    },

    /**
     * Adiciona un record a la carrito
     * @param record
     */
    addCartItem: function (record) {
        var me = this,
            gpDetalleDevolucion = this.getGrid_itemcart(),
            stDetalleDevolucion = gpDetalleDevolucion.getStore();

        if(record.get('cantidad') > 0){
            var index = stDetalleDevolucion.find('idproductoservicio', record.get('idproductoservicio'));
            if (index == -1) {
                stDetalleDevolucion.insert(0, {
                    idproductoservicio: record.get('idproductoservicio'),
                    codigo: record.get('codigo'),
                    denominacion: record.get('denominacion'),
                    preciounitario: record.get('preciounitario'),
                    cantidad: 1,
                    descuento: 0.00,
                    iva: record.get('iva')
                });
                gpDetalleDevolucion.getSelectionModel().selectRange(0, 0);

            } else {
                var articleRec = stDetalleDevolucion.getAt(index);
                articleRec.set('cantidad', articleRec.get('cantidad') + 1);
                gpDetalleDevolucion.getSelectionModel().select(articleRec);
            }

            //Disminuye la disponiblidad de productos en la factura
            record.set('cantidad', record.get('cantidad') - 1);
        }
    },


    updateArticleCartRecord: function (st, record, operation, e) {
        if (Ext.isArray(record)) {
            record = record[0];
        }

        var porcientoDescuento = Ext.util.Format.round((record.get('preciounitario') * record.get('descuento') / 100), 2);
        var porcientoIva = Ext.util.Format.round((record.get('preciounitario') * record.get('iva') / 100), 2);
        var importe = Ext.util.Format.round(
            (record.get('preciounitario') - porcientoDescuento + porcientoIva) * record.get('cantidad'), 2
        );

        //Modifica el importe
        record.set('importe', importe);

        this.updateTotalInfo();
    },

    updateTotalInfo: function () {
        var stArticlesCart = this.getGrid_itemcart().getStore();
        var records = stArticlesCart.getRange(),
            subtotal = 0,
            descuento = 0,
            impuesto = 0,
            importe = 0,
            totalArticles = 0;

        var formTotals = this.getEdit_devolucion().down('form[itemId=formTotals]');


        Ext.each(records, function (record) {
            // Subtotal
            subtotal += Ext.util.Format.round(record.get('cantidad') * record.get('preciounitario'), 2);

            // Descuento
            if (record.get('descuento') > 0) {
                var porcientoDescuento = (record.get('preciounitario') * record.get('cantidad')) * record.get('descuento') / 100;
                descuento += Ext.util.Format.round(porcientoDescuento, 2);
            }

            //IVA 12%
            if (record.get('iva') > 0) {
                impuesto += Ext.util.Format.round(
                    (record.get('cantidad') * record.get('preciounitario') * record.get('iva') / 100), 2
                );
            }

            //Cantidad total de articulos
            totalArticles += record.get('cantidad');

            //VALOR TOTAL
            importe += Ext.util.Format.round(
                (record.get('importe')), 2
            );
        });

        var objTotals = {
            subtotal: Ext.util.Format.round(subtotal, 2),
            descuento: Ext.util.Format.round(descuento, 2),
            impuesto: Ext.util.Format.round(impuesto, 2),
            importe: Ext.util.Format.round(importe, 2),
            totalArticles: Ext.util.Format.round(totalArticles, 2)
        };

//        //Actualiza el formulario de los totales con los nuevos datos
        formTotals.getForm().setValues(objTotals);
    },

    saveDevolucion: function (btn) {
        var me = this,
            form = me.editWindow.down('form[itemId=basicData]').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.editWindow, {msg: 'Guardando datos del pedido...'});
            //myMask.show();

            if (me.editWindow.down('form[itemId=basicData]').getForm().isValid()) {
                if (me.editWindow.down('displayfield[name=importe]').getValue() > 0) {
                    var recItems = me.getGrid_itemcart().getStore().getRange();
                    var items = new Array();
                    Ext.each(recItems, function (rec) {
                        items.push({
                            idproductoservicio: rec.get('idproductoservicio'),
                            cantidad: rec.get('cantidad'),
                            preciounitario: rec.get('preciounitario'),
                            descuento: rec.get('descuento'),
                            iva: rec.get('iva'),
                            importe: rec.get('importe')
                        });
                    });

                    form.submit({
                        submitEmptyText: false,
                        params: {
                            items: Ext.encode(items),
                            importe: me.editWindow.down('displayfield[name=importe]').getValue(),
                            impuesto: me.editWindow.down('displayfield[name=impuesto]').getValue()
                        },
                        success: function (response) {
                            myMask.hide();
                            if (btn.action === 'aceptar') {
                                me.editWindow.close();
                                me.getGrid_devolucion().getStore().load();
                            }
                            else {
                                form.reset();
                            }
                        },
                        failure: function (response, opts) {
                            myMask.hide();
                        }
                    });
                } else {
                    me.editWindow.down('form[itemId=formTotals]').getEl().highlight();
                }
            }
        }
    },

    /**
     * Ejecuta el metodo de buscar articulo cuando se presiona ENTER en el campo de busqueda
     * @param f
     * @param e
     */
    onSearchFacturaEnter: function (f, e) {
        if (e.getKey() == e.ENTER) {
            this.onSearchFactura(f);
        }
    },

    /**
     * Busca el articulo en la base de datos y limpia el campo de busqueda
     * @param f
     */
    onSearchFactura: function (f) {
        var me = this;
        if (f.isValid()) {
            var myMask = new Ext.LoadMask(f.up('window'), {msg: 'Cargando datos de la factura...'});
            myMask.show();
            var numFactura = f.getValue();
            Ext.Ajax.request({
                url: 'getFactura',
                method: 'POST',
                params: {
                    numFactura: numFactura
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);
                    if (rpse) {
                        var numdevolucion = me.editWindow.down('hidden[name=numero]').getValue();
                        me.resetEditWindow();
                        var stFacturaProducto = me.editWindow.down('grid[itemId=grid_facturaproducto]').getStore();

                        Ext.each(rpse.DatFacturaProducto, function (producto) {
                            stFacturaProducto.add({
                                idproductoservicio: producto.idproductoservicio,
                                codigo: producto.NomProductoservicio.codigo,
                                denominacion: producto.NomProductoservicio.denominacion,
                                preciounitario: producto.NomProductoservicio.preciounitario,
                                cantidad: producto.cantidad,
                                descuento: producto.descuento,
                                iva: producto.NomProductoservicio.iva
                            });
                        });

                        me.editWindow.down('form[itemId=basicData]').getForm().setValues({
                            numero: numdevolucion,
                            display_numero: numdevolucion,
                            numfactura: rpse.numero,
                            idfactura: rpse.idfactura,
                            cliente: rpse.cliente,
                            idcliente: rpse.idcliente
                        });

                    } else {
                        Ext.MessageBox.show({
                            title: 'Error',
                            msg: 'No existe ninguna factura con ese número.',
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            });
        }
    },

    onCantItemChange: function (record, f, newValue, oldValue, evt) {
        var producto = this.editWindow.down('grid[itemId=grid_facturaproducto]').getStore().findRecord('idproductoservicio', record.get('idproductoservicio'));
        if (!Ext.isEmpty(producto)) {
            var stock = producto.get('cantidad');
            if (newValue > stock) {
                f.setValue(oldValue);
            } else {
                producto.set('cantidad', stock + (oldValue - newValue));
            }
        }
    },


    onAceptComprobante: function (btn) {
        var me = this,
            win = btn.up('window'),
            gridDevolucion = this.getGrid_devolucion();

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea generar el comprobante para la devolución seleccionada?', function (btn) {
                if (btn == 'yes') {
                    var params = win.down('form').getForm().getValues();

                    params.iddevolucion = gridDevolucion.getSelectionModel().getLastSelected().get('iddevolucion');

                    var pases = new Array();
                    var recordsPases = win.down('grid').getStore().getRange();

                    var sumDebito = 0;
                    var sumCredito = 0;
                    Ext.each(recordsPases, function (pase) {
                        sumDebito += pase.get('debito');
                        sumCredito += pase.get('credito');

                        pases.push({
                            codigodocumento: pase.get('codigodocumento'),
                            concatcta: pase.get('concatcta'),
                            credito: pase.get('credito'),
                            debito: pase.get('debito'),
                            denominacion: pase.get('denominacion'),
                            idcuenta: pase.get('idcuenta'),
                            idpase: pase.get('idpase')
                        });
                    });
                    params.pases = Ext.encode(pases)

                    if ((sumDebito - sumCredito) != 0) {
                        Ext.Msg.show({
                            title: perfil.etiquetas.lnInfo,
                            msg: 'No se puede generar un comprobante con errores.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                        return;
                    }

                    if (win.extraParams) {
                        Ext.apply(params, win.extraParams);
                    }
                    var myMask = new Ext.LoadMask(win, {msg: 'Generando...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'saveComprobante',
                        method: 'POST',
                        params: params,
                        callback: function (options, success, response) {
                            var rpse = Ext.decode(response.responseText);
                            myMask.hide();
                            gridDevolucion.getStore().reload();
                            win.close();
                        }
                    });
                }
            }
        );
    }

});