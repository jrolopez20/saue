Ext.define('Devolucion.model.Devolucion', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'iddevolucion', type: 'int'},
        {name: 'numero', type: 'int'},
        {name: 'fecha'},
        {name: 'idcliente', type: 'int'},
        {name: 'cliente', type: 'string'},
        {name: 'idusuario', type: 'int'},
        {name: 'usuario', type: 'string'},
        {name: 'idterminopago', type: 'int'},
        {name: 'terminodepago', type: 'string', mapping: 'NomTerminosPago.terminodepago'},
        {name: 'idvendedor', type: 'int'},
        {name: 'estado', type: 'int'},
        {name: 'importe', type: 'float'},
        {name: 'impuesto', type: 'float'}
    ]
});