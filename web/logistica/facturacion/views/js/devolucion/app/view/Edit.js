Ext.define('Devolucion.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_devolucion',
    layout: 'border',
    title: 'Adicionar devolucion',
    modal: true,
    width: 900,
    height: 600,
    closeAction: 'hide',

    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'north',
                url: 'guardarDevolucion',
                itemId: 'basicData',
                margins: '5 5 5 5',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 70
                },
                bodyPadding: '5 5 0',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'iddevolucion'
                    },
                    {
                        xtype: 'hidden',
                        name: 'numero'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idcliente'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idfactura'
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'displayfield',
                                name: 'display_numero',
                                fieldLabel: 'No. devolución',
                                width: 250,
                                labelAlign: 'left',
                                labelWidth: 130,
                                value: '',
                                fieldStyle: 'color:rgba(0, 7, 20, 0.83); font-size: 18px;',
                                labelStyle: 'color:rgba(0, 7, 20, 0.83); font-size: 18px;'
                            },
                            {
                                xtype: 'displayfield',
                                flex: 1
                            },
                            {
                                xtype: 'datefield',
                                name: 'fecha',
                                fieldLabel: 'Fecha',
                                labelAlign: 'left',
                                allowBlank: false,
                                value: new Date()
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'triggerfield',
                                fieldLabel: 'No. factura',
                                name: 'numfactura',
                                emptyText: 'Buscar factura',
                                maxLength: 50,
                                width: 150,
                                enableKeyEvents: true,
                                allowBlank: false,
                                triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                margins: '0 5 0 0',
                                onTriggerClick: function () {
                                    this.fireEvent('onTriggerClick', this);
                                }
                            },
                            {
                                xtype: 'textfield',
                                name: 'cliente',
                                fieldLabel: 'Cliente',
                                readOnly: true,
                                margins: '0 5 0 0',
                                width: 300
                            },
                            {
                                xtype: 'textfield',
                                name: 'comentario',
                                fieldLabel: 'Comentario',
                                maxLength: 255,
                                flex: 1
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'grid',
                region: 'west',
                title: 'Productos de la factura',
                itemId: 'grid_facturaproducto',
                width: 480,
                minWidth: 300,
                split: true,
                margins: '0 0 5 5',
                multiSelect: true,
                store: Ext.create('Ext.data.Store', {
                    extend: 'Ext.data.Store',
                    fields: [
                        {
                            name: 'idproductoservicio', type: 'int'
                        },
                        {
                            name: 'codigo'
                        },
                        {
                            name: 'denominacion'
                        },
                        {
                            name: 'preciounitario'
                        },
                        {
                            name: 'cantidad'
                        },
                        {
                            name: 'descuento'
                        },
                        {
                            name: 'iva'
                        }
                    ]
                }),
                columns: [
                    {
                        text: 'Codigo',
                        dataIndex: 'codigo',
                        width: 100
                    },
                    {
                        text: 'Denominacion',
                        dataIndex: 'denominacion',
                        flex: 1
                    },
                    {
                        text: 'Cantidad',
                        dataIndex: 'cantidad',
                        width: 80
                    }
                ],
                viewConfig: {
                    plugins: {
                        ptype: 'gridviewdragdrop',
                        dragGroup: 'group1'
                    }
                }
            },
            {
                region: 'center',
                margins: '0 5 5 0',
                layout: 'border',
                border: false,
                items: [
                    {
                        xtype: 'grid_itemcart',
                        iconCls: 'cart',
                        title: 'Detalle de la devolución',
                        region: 'center',
                        emptyText: '<div style="text-align:center;font-size: 14px;font-style:italic;padding:50px;">' +
                        'Arrastrar y soltar o doble click sobre un elemento para adicionarlo.</div>',
                        viewConfig: {
                            plugins: {
                                ptype: 'gridviewdragdrop',
                                dropGroup: 'group1',
                                enableDrag: false
                            },
                            listeners: {
                                drop: function(node, data, dropRec, dropPosition) {
                                    var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
                                    console.info('Drag from left to right', 'Dropped ' + data.records[0].get('name') + dropOn);
                                }
                            }
                        }
                    },
                    {
                        xtype: 'form',
                        itemId: 'formTotals',
                        region: 'south',
                        fieldDefaults: {
                            labelAlign: 'top',
                            msgTarget: 'side',
                            anchor: '100%',
                            labelWidth: 70
                        },
                        bodyPadding: '5 10 10',
                        margins: '5 0 0 0',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'displayfield',
                                flex: 1
                            },
                            {
                                xtype: 'displayfield',
                                name: 'totalArticles',
                                hideLabel: true,
                                fieldStyle: 'text-align:center;font-size:51px;color:black;margin: 15 0px;',
                                width: 80,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                flex: 1
                            },
                            {
                                xtype: 'displayfield',
                                name: 'subtotal',
                                fieldLabel: 'Subtotal',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:22px; color: green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 110,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                name: 'descuento',
                                fieldLabel: '(-) Descuento',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:22px; color: green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 110,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                name: 'impuesto',
                                fieldLabel: '(+) Impuesto',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:22px; color: green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 110,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                name: 'importe',
                                fieldLabel: '(=) Total',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:25px; color: green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 130,
                                value: '0'
                            }
                        ]
                    }
                ]
            }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.hide
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});