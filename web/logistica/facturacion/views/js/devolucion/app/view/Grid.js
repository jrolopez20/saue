Ext.define('Devolucion.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_devolucion',

    store: 'Devolucion.store.Devolucion',

    title: 'Listado de devoluciones',
    allowDeselect: true,
    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar devolución',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Imprimir',
                tooltip: 'Imprimir devolución',
                icon: perfil.dirImg + 'imprimir.png',
                iconCls: 'btn',
                action: 'imprimir',
                disabled: true,
                hidden: true
            },
            {
                text: 'Contabilizar',
                tooltip: 'Contabilizar factura(s)',
                icon: perfil.dirImg + 'contabilizar.png',
                iconCls: 'btn',
                action: 'contabilizar',
                disabled: true
            },
            '->',
            {
                xtype: 'searchfield',
                store: 'Devolucion.store.Devolucion',
                emptyText: 'Buscar...',
                width: 250,
                maskRe: /^[0-9]+$/,
                regex: /^[0-9]+$/,
                filterPropertysNames: ['numero']
            }
        ];

        me.columns = [
            {
                header: 'Número',
                dataIndex: 'numero',
                width: 110
            },
            {
                header: 'Fecha',
                dataIndex: 'fecha',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            },
            {
                header: 'Importe',
                dataIndex: 'importe',
                width: 110,
                renderer: me.showImporte
            },
            {
                header: 'Cliente',
                dataIndex: 'cliente',
                flex: 1
            },
            {
                header: 'Usuario',
                dataIndex: 'usuario',
                width: 110
            },
            {
                header: 'Estado',
                dataIndex: 'estado',
                width: 110,
                renderer: me.showStatus
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    },
    showImporte: function (v) {
        return '<div style="text-align: right; font-weight: bold">' + v + '</div>';
    },
    showStatus: function (v) {
        if (v == 1) {
            return '<div style="color: #006400">Contabilizado</div>';
        } else {
            return 'Sin contabilizar';
        }
    }

});