var perfil = window.parent.UCID.portal.perfil;
Ext.application({
    name: 'Devolucion',
    enableQuickTips: true,
    paths: {
        'Devolucion': '../../views/js/devolucion/app'
    },
    controllers: [
        'Devolucion'
    ],
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [{
                region: 'center',
                xtype: 'grid_devolucion',
                margin: '5 5 5 5'
            }]
        });
    }
});