Ext.define('Factura.store.TerminoPago', {
    extend: 'Ext.data.Store',


    fields: [
        {name: 'idterminodepago'},
        {name: 'nombre'},
        {name: 'plazo'}
    ],

    pageSize: 25,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'getTerminosPago',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});