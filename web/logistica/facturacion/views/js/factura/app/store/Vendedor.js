Ext.define('Factura.store.Vendedor', {
    extend: 'Ext.data.Store',

    fields: [
        {name: 'idvendedor'},
        {name: 'codigo'},
        {name: 'fullname'},
        {name: 'foto'}
    ],

    pageSize: 25,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'getVendedores',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});