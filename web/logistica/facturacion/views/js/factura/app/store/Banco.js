Ext.define('Factura.store.Banco', {
    extend: 'Ext.data.Store',

    fields: [
        {name: 'idbanco'},
        {name: 'codigo'},
        {name: 'abreviatura'},
        {name: 'nombre'}
    ],
    pageSize: 25,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'getBancos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});