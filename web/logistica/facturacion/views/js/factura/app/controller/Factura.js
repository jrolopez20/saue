Ext.define('Factura.controller.Factura', {
    extend: 'Ext.app.Controller',

    views: [
        'Factura.view.Grid',
        'Factura.view.Edit',
        'Factura.view.DetalleFacturaGrid',
        'Factura.view.FormaPago'
    ],
    stores: [
        'Factura.store.Factura',
        'Factura.store.Cliente',
        'Factura.store.TerminoPago',
        'Factura.store.Banco',
        'Factura.store.Vendedor'
    ],
    models: [
        'Factura.model.Factura'
    ],
    refs: [
        {ref: 'grid_factura', selector: 'grid_factura'},
        {ref: 'edit_factura', selector: 'edit_factura'},
        {ref: 'grid_itemcart', selector: 'grid_itemcart'},
        {ref: 'edit_formapago', selector: 'edit_formapago'},
        {ref: 'comprobante', selector: 'comprobante'}
    ],

    editWindow: null,
    editFormaPago: null,

    init: function () {
        var me = this;

        me.control({
            'grid_factura': {
                selectionchange: me.onFacturaSelectionChange
            },
            'grid_factura button[action=adicionar]': {
                click: me.onAddClick
            },
            'grid_factura button[action=eliminar]': {
                click: me.onDelClick
            },
            'grid_factura button[action=imprimir]': {
                click: me.onPrintClick
            },
            'grid_factura button[action=contabilizar]': {
                click: me.onContabilizarClick
            },
            'grid_factura button[action=anular]': {
                click: me.onAnularClick
            },
            'grid_itemcart': {
                addItem: me.updateArticleCartRecord,
                updateItem: me.updateArticleCartRecord,
                datachangedItem: me.updateTotalInfo,
                onItemDroped: me.onItemDroped
            },
            'view_productoservicio dataview': {
                itemdblclick: me.onProductoServicioDblClick
            },
            'edit_factura numberfield[name=flete]': {
                change: me.updateTotalInfo
            },
            'edit_factura triggerfield[name=numpedido]': {
                onTriggerClick: me.onSearchPedido,
                specialkey: me.onSearchPedidoEnter
            },
            'edit_factura button[action=aceptar]': {
                click: me.onAceptFacturaClick
            },
            'edit_formapago dataview[itemId=formapagoview]': {
                itemdblclick: me.onFormaPagoDblClick
            },
            'edit_formapago button[action=cancelar_formapago]': {
                click: me.onBackFormaPagoClick
            },
            'edit_formapago button[action=aceptar]': {
                click: me.saveFactura
            },
            'comprobante button[action=aceptar]': {
                click: me.onAceptComprobante
            }
        });
    },

    onFacturaSelectionChange: function () {
        var me = this,
            grid = me.getGrid_factura();
        grid.down('button[action=imprimir]').disable();
        grid.down('button[action=contabilizar]').disable();
        grid.down('button[action=anular]').disable();

        if (grid.getSelectionModel().hasSelection()) {
            var records = grid.getSelectionModel().getSelection();
            var contabilizar = true;
            var anular = true;
            //Determina si debo habilitar el boton de Contabilizar
            Ext.each(records, function (record) {
                //Verifica que no exista ninguna factura seleccionada que ya este contabilizada
                if (record.get('estado') != 0) {
                    contabilizar = false;
                    anular = false;
                }
            });
            grid.down('button[action=imprimir]').enable();

            if (contabilizar) {
                grid.down('button[action=contabilizar]').enable();
            }

            if (anular) {
                grid.down('button[action=anular]').enable();
            }
        }
    },

    onAddClick: function (btn) {
        var me = this;
        if (!me.editWindow) {
            me.editWindow = Ext.widget('edit_factura');
        }
        me.editWindow.show();
        me.editWindow.maximize();

        var myMask = new Ext.LoadMask(me.editWindow, {msg: 'Cargando último número de factura...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'getUltimoNumFactura',
            method: 'POST',
            callback: function (options, success, response) {
                myMask.hide();
                var rpse = Ext.decode(response.responseText);
                if (rpse.ultimonumfactura) {
                    me.resetEditWindow();
                    me.editWindow.setTitle('Adicionar factura');
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('numero').setValue(rpse.ultimonumfactura);
                    me.editWindow.down('form[itemId=basicData]').getForm().findField('display_numero').setValue(rpse.ultimonumfactura);
                }
            }
        });
    },

    onDelClick: function (btn) {
        var me = this,
            grid = me.getGrid_factura();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar la factura seleccionada?', function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando factura...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteFactura',
                        method: 'POST',
                        params: {
                            idfactura: record.get('idfactura')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                //grid.down('button[action=modificar]').disable();
                                //grid.down('button[action=eliminar]').disable();
                                grid.down('button[action=imprimir]').disable();
                                grid.getStore().reload();
                            }
                        }
                    });
                }
            });
        }
    },

    onPrintClick: function (btn) {
        var me = this,
            smPedido = me.getGrid_factura().getSelectionModel();
        if (smPedido.hasSelection()) {
            var records = smPedido.getSelection();

            var arrIdFactura = new Array();

            Ext.each(records, function (record) {
                arrIdFactura.push(record.get('idfactura'));
            });

            var params = Ext.urlEncode({
                arrIdPedido: Ext.encode(arrIdFactura)
            });

            var win = new Ext.Window({
                title: 'Factura',
                width: 800,
                height: 500,
                maskDisabled: false,
                animCollapse: false,
                maximizable: true,
                constrainHeader: true,
                border: false,
                modal: true,
                items: {
                    xtype: 'component',
                    autoEl: {
                        tag: 'iframe',
                        style: 'height: 100%; width: 100%; border: none',
                        src: document.location.protocol + '//' + document.location.host +
                        '/logistica/facturacion/index.php/factura/imprimirFactura?' + params
                    }
                }
            }).show();
        }
    },

    onContabilizarClick: function (btn) {
        var me = this,
            smFactura = me.getGrid_factura().getSelectionModel();
        if (smFactura.hasSelection()) {
            var records = smFactura.getSelection();

            var arrIdFactura = new Array();

            Ext.each(records, function (record) {
                arrIdFactura.push(record.get('idfactura'));
            });

            var myMask = new Ext.LoadMask(me.getGrid_factura(), {msg: 'Contabilizando facturas...'});
            myMask.show();

            Ext.Ajax.request({
                url: 'loadDataContabilizar',
                method: 'POST',
                params: {
                    arrIdFactura: Ext.encode(arrIdFactura)
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);

                    var win = Ext.widget('comprobante', {
                        title: 'Contabilizar'
                    });
                    win.stTipoDiario.on('beforeload', function (st, operation, Opts) {
                        operation.params.idsubsistema = 7;
                    });
                    win.down('form').getForm().setValues(rpse);
                    win.down('grid').getStore().loadData(rpse.pases);
                }
            });
        }
    },

    onAnularClick: function (btn) {
        var me = this,
            smFactura = me.getGrid_factura().getSelectionModel();

        if (smFactura.hasSelection()) {
            var records = smFactura.getSelection();
            var arrIdFactura = new Array();

            Ext.each(records, function (record) {
                if (record.get('estado') == 0) {
                    arrIdFactura.push(record.get('idfactura'));
                }
            });

            var myMask = new Ext.LoadMask(me.getGrid_factura(), {msg: 'Anulando factura(s)...'});
            myMask.show();

            Ext.Ajax.request({
                url: 'anularFactura',
                method: 'POST',
                params: {
                    arrIdFactura: Ext.encode(arrIdFactura)
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);
                    me.getGrid_factura().getStore().reload();
                }
            });
        }
    },

    onAceptComprobante: function (btn) {
        var me = this,
            win = btn.up('window'),
            gridFactura = this.getGrid_factura();

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea generar el comprobante para la(s) factura(s) seleccionada(s)?', function (btn) {
                if (btn == 'yes') {
                    var params = win.down('form').getForm().getValues();
                    var arrIdFactura = new Array();
                    var facturasSeleccionadas = gridFactura.getSelectionModel().getSelection();

                    Ext.each(facturasSeleccionadas, function (record) {
                        arrIdFactura.push(record.get('idfactura'));
                    });
                    params.arrIdFactura = Ext.encode(arrIdFactura);

                    var pases = new Array();
                    var recordsPases = win.down('grid').getStore().getRange();

                    var sumDebito = 0;
                    var sumCredito = 0;
                    Ext.each(recordsPases, function (pase) {
                        sumDebito += pase.get('debito');
                        sumCredito += pase.get('credito');

                        pases.push({
                            codigodocumento: pase.get('codigodocumento'),
                            concatcta: pase.get('concatcta'),
                            credito: pase.get('credito'),
                            debito: pase.get('debito'),
                            denominacion: pase.get('denominacion'),
                            idcuenta: pase.get('idcuenta'),
                            idpase: pase.get('idpase')
                        });
                    });
                    params.pases = Ext.encode(pases)

                    if ((sumDebito - sumCredito) != 0) {
                        Ext.Msg.show({
                            title: perfil.etiquetas.lnInfo,
                            msg: 'No se puede generar un comprobante con errores.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                        return;
                    }

                    if (win.extraParams) {
                        Ext.apply(params, win.extraParams);
                    }
                    var myMask = new Ext.LoadMask(win, {msg: 'Generando...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'saveComprobante',
                        method: 'POST',
                        params: params,
                        callback: function (options, success, response) {
                            var rpse = Ext.decode(response.responseText);
                            myMask.hide();
                            gridFactura.getStore().reload();
                            win.close();
                        }
                    });
                }
            }
        );
    },

    resetEditWindow: function (all) {
        this.editWindow.down('form[itemId=basicData]').getForm().reset();
        this.editWindow.down('form[itemId=formTotals]').getForm().reset();
        this.getGrid_itemcart().getStore().removeAll();
    },

    /**
     * Adiciona un nuevo articulo al carrito al hacer doble click
     * @param view
     * @param record
     */
    onProductoServicioDblClick: function (view, record) {
        this.addCartItem(record);
    },

    /**
     * Adiciona un nuevo articulo al carrito al arrastrar y soltar
     * @param target
     * @param dd
     * @param e
     * @param data
     */
    onItemDroped: function (target, dd, e, data) {
        this.addCartItem(data.record);
    },

    /**
     * Adiciona un record a la carrito
     * @param record
     */
    addCartItem: function (record) {
        var gpDetallePedido = this.getGrid_itemcart(),
            stDetallePedido = gpDetallePedido.getStore();

        var index = stDetallePedido.find('idproductoservicio', record.get('idproductoservicio'));
        if (index == -1) {
            stDetallePedido.insert(0, {
                idproductoservicio: record.get('idproductoservicio'),
                codigo: record.get('codigo'),
                denominacion: record.get('denominacion'),
                preciounitario: record.get('preciounitario'),
                cantidad: 1,
                descuento: 0.00,
                iva: record.get('iva')
            });
            gpDetallePedido.getSelectionModel().selectRange(0, 0);
        } else {
            var articleRec = stDetallePedido.getAt(index);
            articleRec.set('cantidad', articleRec.get('cantidad') + 1);
            gpDetallePedido.getSelectionModel().select(articleRec);
        }
    },

    onFormaPagoDblClick: function (view, record, item, index) {
        var pFormaPago = this.getEdit_formapago().down('panel[itemId=cardFormaPago]');
        pFormaPago.getLayout().setActiveItem(record.data.code);
    },

    onBackFormaPagoClick: function (btn) {
        var pFormaPago = this.getEdit_formapago().down('panel[itemId=cardFormaPago]');
        pFormaPago.getLayout().setActiveItem(0);
    },

    updateArticleCartRecord: function (st, record, operation, e) {
        if (Ext.isArray(record)) {
            record = record[0];
        }

        var porcientoDescuento = Ext.util.Format.round((record.get('preciounitario') * record.get('descuento') / 100), 2);
        var porcientoIva = Ext.util.Format.round((record.get('preciounitario') * record.get('iva') / 100), 2);
        var importe = Ext.util.Format.round(
            (record.get('preciounitario') - porcientoDescuento + porcientoIva) * record.get('cantidad'), 2
        );

        //Modifica el importe
        record.set('importe', importe);

        this.updateTotalInfo();
    },

    updateTotalInfo: function () {
        var stArticlesCart = this.getGrid_itemcart().getStore();
        var records = stArticlesCart.getRange(),
            subtotal = 0,
            descuento = 0,
            impuesto = 0,
            importe = 0,
            totalArticles = 0,
            flete = this.getEdit_factura().down('numberfield[name=flete]').getValue();

        var formTotals = this.getEdit_factura().down('form[itemId=formTotals]');

        Ext.each(records, function (record) {
            // Subtotal
            subtotal += Ext.util.Format.round(record.get('cantidad') * record.get('preciounitario'), 2);

            // Descuento
            if (record.get('descuento') > 0) {
                var porcientoDescuento = (record.get('preciounitario') * record.get('cantidad')) * record.get('descuento') / 100;
                descuento += Ext.util.Format.round(porcientoDescuento, 2);
            }

            //IVA 12%
            if (record.get('iva') > 0) {
                impuesto += Ext.util.Format.round(
                    (record.get('cantidad') * record.get('preciounitario') * record.get('iva') / 100), 2
                );
            }

            //Cantidad total de articulos
            totalArticles += record.get('cantidad');

            //VALOR TOTAL
            importe += Ext.util.Format.round(
                (record.get('importe')), 2
            );
        });

        var objTotals = {
            subtotal: Ext.util.Format.round(subtotal, 2),
            descuento: Ext.util.Format.round(descuento, 2),
            impuesto: Ext.util.Format.round(impuesto, 2),
            importe: Ext.util.Format.round(importe + flete, 2),
            totalArticles: Ext.util.Format.round(totalArticles, 2)
        };

//        //Actualiza el formulario de los totales con los nuevos datos
        formTotals.getForm().setValues(objTotals);
    },

    /**
     * Ejecuta el metodo de buscar articulo cuando se presiona ENTER en el campo de busqueda
     * @param f
     * @param e
     */
    onSearchPedidoEnter: function (f, e) {
        if (e.getKey() == e.ENTER) {
            this.onSearchPedido(f);
        }
    },

    /**
     * Busca el articulo en la base de datos y limpia el campo de busqueda
     * @param f
     */
    onSearchPedido: function (f) {
        var me = this;
        var myMask = new Ext.LoadMask(f.up('window'), {msg: 'Cargando datos del pedido...'});
        myMask.show();
        var numPedido = f.getValue();
        Ext.Ajax.request({
            url: 'getDetallePedido',
            method: 'POST',
            params: {
                numero: numPedido
            },
            callback: function (options, success, response) {
                myMask.hide();
                var rpse = Ext.decode(response.responseText);
                if (rpse) {
                    var numeroFactura = me.editWindow.down('hidden[name=numero]').getValue();
                    me.resetEditWindow();
                    //var win = this.getEdit_factura();
                    var stDetalleFactura = me.getGrid_itemcart().getStore();
                    Ext.each(rpse.DatPedidoProducto, function (producto) {
                        stDetalleFactura.add({
                            idproductoservicio: producto.idproductoservicio,
                            codigo: producto.NomProductoservicio.codigo,
                            denominacion: producto.NomProductoservicio.denominacion,
                            preciounitario: producto.NomProductoservicio.preciounitario,
                            cantidad: producto.cantidad,
                            descuento: producto.descuento,
                            iva: producto.NomProductoservicio.iva
                        });
                    });

                    //me.editWindow.down('form[itemId=basicData]').getForm().findField('idcliente').setValue(rpse.idcliente);
                    //me.editWindow.down('form[itemId=basicData]').getForm().findField('idterminodepago').setValue(rpse.idterminodepago);
                    me.editWindow.down('hidden[name=numero]').setValue(numeroFactura);
                    me.editWindow.down('displayfield[name=display_numero]').setValue(numeroFactura);
                    me.editWindow.down('triggerfield[name=numpedido]').setValue(numPedido);
                    me.editWindow.down('combobox[name=idcliente]').setValue(rpse.idcliente);
                    me.editWindow.down('combobox[name=idterminodepago]').setValue(rpse.idterminodepago);
                    me.editWindow.down('numberfield[name=flete]').setValue(rpse.flete);
                } else {
                    Ext.MessageBox.show({
                        title: 'Error',
                        msg: 'No existe ningún pedido con ese número.',
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        });
    },

    /**
     * Muestra la ventana para seleccionar la forma de pago
     * @param btn
     */
    onAceptFacturaClick: function (btn) {
        var me = this;
        if (me.editWindow.down('form[itemId=basicData]').getForm().isValid()) {
            if (me.editWindow.down('displayfield[name=importe]').getValue() > 0) {
                if (!me.editFormaPago) {
                    me.editFormaPago = Ext.widget('edit_formapago');
                }
                me.editFormaPago.show();

                var numero = me.getEdit_factura().down('hidden[name=numero]').getValue();
                var importe = me.getEdit_factura().down('displayfield[name=importe]').getValue();

                me.editFormaPago.down('form[itemId=factura_info]').getForm().setValues({
                    numero: numero,
                    importe: importe
                });
                //Activa el panel para seleccionar la forma de pago
                me.editFormaPago.down('panel[itemId=cardFormaPago]').getLayout().setActiveItem(0);
            } else {
                me.editWindow.down('form[itemId=formTotals]').getEl().highlight();
            }
        }
    },

    /**
     * Guarda los datos de la factura
     * @param btn
     */
    saveFactura: function (btn) {
        var me = this;
        //Datos basicos de la factura
        var basicData = me.editWindow.down('form[itemId=basicData]').getForm().getValues();
        var totalData = me.editWindow.down('form[itemId=formTotals]').getForm().getValues();

        //Items de la factura pueden ser productos o servicios
        var recItems = me.getGrid_itemcart().getStore().getRange();
        var items = new Array();
        Ext.each(recItems, function (rec) {
            items.push(rec.getData());
        });

        //Formas de pago de la factura
        var pFormaPago = me.getEdit_formapago().down('panel[itemId=cardFormaPago]').getLayout().getActiveItem();
        if (pFormaPago.getXType() == 'form' || pFormaPago.getXType() == 'gridpanel') {
            var formapago = null;
            if (pFormaPago.getXType() == 'form') {
                var form = pFormaPago.getForm();
                if (form.isValid()) {
                    formapago = form.getValues();
                } else {
                    return;
                }
            } else {
                recTarjetas = pFormaPago.getStore().getRange();
                var tarjetas = new Array();
                Ext.each(recTarjetas, function (rec) {
                    if (rec.get('monto') != 0 && rec.get('numerotarjeta') != '') {
                        tarjetas.push(rec.getData());
                    }
                });
                formapago = {
                    type: 'tarjeta',
                    tarjetas: tarjetas
                }
            }
            var myMask = new Ext.LoadMask(me.editWindow, {msg: 'Registrando factura...'});
            myMask.show();
            Ext.Ajax.request({
                url: 'saveFactura',
                params: {
                    basicData: Ext.encode(basicData),
                    totalData: Ext.encode(totalData),
                    items: Ext.encode(items),
                    formapago: Ext.encode(formapago)
                },
                method: 'POST',
                callback: function (options, success, response) {
                    myMask.hide();
                    me.getEdit_formapago().hide();
                    me.editWindow.hide();
                    me.getGrid_factura().getStore().reload();
                }
            });
        } else {
            Ext.Msg.show({
                title: 'Alerta',
                msg: 'Debe selecionar una forma de pago',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
        }
    }

});