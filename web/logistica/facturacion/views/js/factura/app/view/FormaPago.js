Ext.define('Factura.view.FormaPago', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_formapago',
    layout: 'border',
    title: 'Seleccione la forma de pago',
    modal: true,
    width: 400,
    height: 520,
    closeAction: 'hide',

    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                itemId: 'factura_info',
                margins: '5 5 5 5',
                region: 'north',
                bodypadding: '10px',
                bodyPadding: '5 10',
                frame: true,
                fieldDefaults: {
                    labelWidth: 150
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Factura No',
                        name: 'numero',
                        flex: 1,
                        labelStyle: 'font-size:15px;',
                        fieldStyle: 'font-size:16px;'
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Valor de la factura',
                        name: 'importe',
                        value: '0.00',
                        flex: 1,
                        labelStyle: 'font-size:15px;',
                        fieldStyle: 'font-size:18;color:green;'
                    }
                ]
            },
            {
                xtype: 'panel',
                itemId: 'cardFormaPago',
                layout: 'card',
                region: 'center',
                margins: '0 5 5 5',
                items: [
                    {
                        xtype: 'dataview',
                        cls: 'img-chooser-view',
                        padding: '5',
                        itemId: 'formapagoview',
                        store: Ext.create('Ext.data.Store', {
                            fields: [
                                {name: 'name', type: 'string'},
                                {name: 'code', type: 'int'},
                                {name: 'image', type: 'string'}
                            ],
                            data: [
                                {name: 'Efectivo', code: '1', image: 'cash.jpg'},
                                {name: 'Cheque', code: '2', image: 'check.jpg'},
                                {name: 'Tarjeta', code: '3', image: 'credit-card.jpg'}
                            ]
                        }),
                        tpl: [
                            '<tpl for=".">',
                            '<div class="thumb-wrap">',
                            '<div class="thumb">',
                            '<img width="100" height="100" src="../../views/images/{image}" />',
                            '</div>',
                            '<span>{name}</span>',
                            '</div>',
                            '</tpl>',
                            '<div class="fpago_description">Doble click para seleccionar</div>'
                        ],
                        overItemCls: 'x-view-over',
                        itemSelector: 'div.thumb-wrap',
                        singleSelect: true,
                        autoScroll: true
                    },
                    {
                        xtype: 'form',
                        frame: true,
                        itemId: 'fp_efectivo',
                        fieldDefaults: {
                            labelAlign: 'left',
                            msgTarget: 'side',
                            anchor: '100%',
                            labelWidth: 130
                        },
                        bodyPadding: '10 5 0',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'hidden',
                                name: 'type',
                                value: 'efectivo'
                            },
                            {
                                xtype: 'displayfield',
                                margins: '10 0 15 0',
                                fieldStyle: 'text-align:center;font-size: 26',
                                value: 'Efectivo'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Valor de la factura',
                                name: 'valor_factura',
                                readOnly: true,
                                fieldStyle: 'text-align:right;'
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: 'Valor en efectivo',
                                name: 'efectivo',
                                allowBlank: false,
                                allowDecimals: true,
                                decimalSeparator: '.',
                                allowNegative: false,
                                fieldStyle: 'text-align:right;',
                                listeners: {
                                    change: function (f, v) {
                                        var form = f.up('form').getForm();
                                        var valorFactura = form.findField('valor_factura').getValue();
                                        var dplFieldCambio = form.findField('cambio');
                                        var c = v - valorFactura;
                                        if (c < 0) {
                                            dplFieldCambio.setFieldStyle('text-align:right;color:red;');
                                            me.down('button[action=aceptar]').disable();
                                        } else {
                                            dplFieldCambio.setFieldStyle('text-align:right;color:black;');
                                            me.down('button[action=aceptar]').enable();
                                        }
                                        dplFieldCambio.setValue(Ext.util.Format.round(c, 2));
                                    }
                                }
                            },
                            {
                                xtype: 'displayfield',
                                fieldLabel: 'Cambio',
                                name: 'cambio',
                                submitValue: true,
                                margins: '10 0 0 0',
                                fieldStyle: 'text-align:right;font-size:20px;color:black;',
                                labelStyle: 'font-size:20px;color:black;',
                                value: '0.00'
                            }
                        ],
                        buttons: [{
                            icon: perfil.dirImg + 'anterior.png',
                            iconCls: 'btn',
                            text: 'Atrás',
                            tooltip: 'Seleccionar otra forma de pago',
                            action: 'cancelar_formapago'
                        }],
                        listeners: {
                            show: function (cmp) {
                                var form = cmp.getForm();
                                form.reset();
                                form.findField('valor_factura').setValue(cmp.up('window').down('displayfield[name=importe]').getValue());
                            }
                        }
                    },
                    {
                        xtype: 'form',
                        frame: true,
                        itemId: 'fp_cheque',
                        fieldDefaults: {
                            labelAlign: 'top',
                            msgTarget: 'side',
                            anchor: '100%',
                            labelWidth: 130
                        },
                        bodyPadding: '10 5 0',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'hidden',
                                name: 'type',
                                value: 'cheque'
                            },
                            {
                                xtype: 'displayfield',
                                margins: '10 0 15 0',
                                fieldStyle: 'text-align:center;font-size: 26',
                                value: 'Cheque'
                            },
                            {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                fieldDefaults: {
                                    labelAlign: 'top'
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. cheque',
                                        name: 'numerocheque',
                                        allowBlank: false,
                                        flex: 1,
                                        margins: '0 5 0 0'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Fecha del cheque',
                                        name: 'fechacheque',
                                        allowBlank: false,
                                        flex: 1
                                    }
                                ]
                            },
                            {
                                xtype: 'combo',
                                store: 'Factura.store.Banco',
                                name: 'idbanco',
                                fieldLabel: 'Banco',
                                allowBlank: false,
                                valueField: 'idbanco',
                                displayField: 'nombre',
                                typeAhead: false,
                                forceSelection: true,
                                pageSize: 20,
                                listConfig: {
                                    getInnerTpl: function () {
                                        return '<div data-qtip="{codigo} {nombre} ({abreviatura})">{codigo} ({nombre})</div>';
                                    }
                                }
                            },
                            {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                fieldDefaults: {
                                    labelAlign: 'top'
                                },
                                items: [
                                    {
                                        xtype: 'radiogroup',
                                        fieldLabel: 'Postfechado?',
                                        flex: 1,
                                        layout: {
                                            autoFlex: false
                                        },
                                        defaults: {
                                            name: 'postfechado'
                                        },
                                        items: [{
                                            inputValue: '1',
                                            boxLabel: 'SI',
                                            checked: true,
                                            margin: '0 10 0 0'
                                        }, {
                                            inputValue: '0',
                                            boxLabel: 'NO'
                                        }]
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Teléfono',
                                        name: 'telefono',
                                        flex: 2
                                    }
                                ]
                            }
                        ],
                        buttons: [{
                            icon: perfil.dirImg + 'anterior.png',
                            iconCls: 'btn',
                            text: 'Atrás',
                            tooltip: 'Cancelar esta forma de pago',
                            action: 'cancelar_formapago'
                        }],
                        listeners: {
                            show: function (cmp) {
                                cmp.getForm().reset();
                                me.down('button[action=aceptar]').enable();
                            }
                        }
                    },
                    {
                        xtype: 'grid',
                        itemId: 'fp_tarjeta',
                        store: Ext.create('Ext.data.Store', {
                            fields: [
                                {name: 'numerotarjeta', type: 'string'},
                                {name: 'monto', type: 'float'}
                            ]
                        }),
                        plugins: [new Ext.grid.plugin.RowEditing({
                            pluginId: 'rowEditTarjeta',
                            clicksToEdit: 1,
                            errorSummary: false,
                            listeners: {
                                cancelEdit: function (rowEditing, context) {
                                    // Canceling editing of a locally added, unsaved record: remove it
                                    if (context.record.phantom) {
                                        context.grid.getStore().remove(context.record);
                                    }
                                }
                            }
                        })],
                        selModel: {
                            selType: 'rowmodel'
                        },
                        features: [{
                            ftype: 'summary'
                        }],
                        tbar: [{
                            text: 'Agregar tarjeta',
                            icon: perfil.dirImg + 'adicionar.png',
                            iconCls: 'btn',
                            scope: this,
                            handler: function (btn) {
                                var st = btn.up('grid').getStore(),
                                    records = st.getRange(),
                                    flag = true;
                                Ext.each(records, function (rec) {
                                    if (rec.get('numerotarjeta') == '' || rec.get('monto') == '') {
                                        flag = false
                                    }
                                });
                                if (flag) {
                                    var roweditor = btn.up('grid').getPlugin('rowEditTarjeta');
                                    roweditor.cancelEdit();
                                    var newRecords = st.add({numerotarjeta: '', monto: '0.00'});
                                    roweditor.startEdit(newRecords[0], 0);
                                }
                            }
                        }],
                        columns: [{
                            header: 'Numero tarjeta',
                            dataIndex: 'numerotarjeta',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false
                            },
                            summaryRenderer: function (value, summaryData, dataIndex) {
                                return '<div style="text-align:right;font-size:12px"><b>Total</b></div>';
                            }
                        }, {
                            header: 'Monto',
                            dataIndex: 'monto',
                            width: 140,
                            editor: {
                                xtype: 'numberfield',
                                allowBlank: false,
                                allowDecimals: true,
                                allowNegative: false,
                                decimalSeparator: '.'
                            },
                            summaryType: 'sum',
                            summaryRenderer: function (value, summaryData, dataIndex) {
                                var color = 'black',
                                    msg = 'La suma coincide con el valor de la factura';
                                if (value != me.down('displayfield[name=importe]').getValue()) {
                                    color = 'red';
                                    msg = 'La suma no coincide con el valor de la factura';
                                    me.down('button[action=aceptar]').disable();
                                } else {
                                    me.down('button[action=aceptar]').enable();
                                }
                                return Ext.String.format('<b style="color:{1};font-size:12px" data-qtip="{2}">{0}</b>', value, color, msg);
                            }
                        }],
                        buttons: [{
                            icon: perfil.dirImg + 'anterior.png',
                            iconCls: 'btn',
                            text: 'Atrás',
                            tooltip: 'Cancelar esta forma de pago',
                            action: 'cancelar_formapago'
                        }],
                        listeners: {
                            show: function (cmp) {
                                cmp.getStore().removeAll();
                            }
                        }
                    }
                ]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                tooltip: 'Cerrar la ventana sin guardar los cambios',
                action: 'cancelar',
                scope: this,
                handler: this.hide
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                tooltip: 'Guardar factura',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});