Ext.define('Factura.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_factura',
    layout: 'border',
    title: 'Adicionar factura',
    modal: true,
    width: 900,
    height: 600,
    closeAction: 'hide',

    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'north',
                itemId: 'basicData',
                url: 'guardarFactura',
                margins: '5 5 5 5',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 70
                },
                bodyPadding: '5 5 0',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idfactura'
                    },
                    {
                        xtype: 'hidden',
                        name: 'numero'
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'displayfield',
                                name: 'display_numero',
                                fieldLabel: 'No. factura',
                                width: 250,
                                labelAlign: 'left',
                                labelWidth: 110,
                                value: '',
                                fieldStyle: 'color:rgba(0, 7, 20, 0.83); font-size: 18px;',
                                labelStyle: 'color:rgba(0, 7, 20, 0.83); font-size: 18px;'
                            },
                            {
                                xtype: 'displayfield',
                                flex: 1
                            },
                            {
                                xtype: 'datefield',
                                name: 'fecha',
                                fieldLabel: 'Fecha',
                                labelAlign: 'left',
                                allowBlank: false,
                                value: new Date()
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'triggerfield',
                                fieldLabel: 'No. pedido',
                                name: 'numpedido',
                                emptyText: 'Buscar pedido',
                                maxLength: 50,
                                width: 110,
                                enableKeyEvents: true,
                                triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                margins: '0 5 0 0',
                                onTriggerClick: function () {
                                    this.fireEvent('onTriggerClick', this);
                                }
                            },
                            {
                                xtype: 'combo',
                                store: 'Factura.store.Cliente',
                                name: 'idcliente',
                                fieldLabel: 'Cliente',
                                flex: 1,
                                allowBlank: false,
                                valueField: 'idclientesproveedores',
                                displayField: 'nombre',
                                typeAhead: false,
                                pageSize: 20,
                                margins: '0 5 0 0'
                            },
                            {
                                xtype: 'combo',
                                store: 'Factura.store.TerminoPago',
                                name: 'idterminodepago',
                                fieldLabel: 'Término de pago',
                                flex: 1,
                                allowBlank: false,
                                valueField: 'idterminodepago',
                                displayField: 'nombre',
                                typeAhead: false,
                                pageSize: 20,
                                margins: '0 5 0 0'
                            },
                            {
                                xtype: 'combo',
                                store: 'Factura.store.Vendedor',
                                name: 'idvendedor',
                                fieldLabel: 'Vendedor',
                                flex: 1,
                                allowBlank: false,
                                valueField: 'idvendedor',
                                displayField: 'fullname',
                                typeAhead: false,
                                pageSize: 20,
                                margins: '0 5 0 0',
                                listConfig: {
                                    getInnerTpl: function () {
                                        return '<div data-qtip="<img src={foto} width=60 height=60 />"><b>({codigo}</b>) {fullname}</div>';
                                    }
                                }
                            },
                            {
                                xtype: 'numberfield',
                                name: 'flete',
                                fieldLabel: 'Flete',
                                width: 100,
                                value: 0,
                                minValue: 0,
                                decimalSeparator: '.',
                                fieldStyle: 'text-align:right'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'view_productoservicio',
                region: 'west',
                width: 415,
                minWidth: 200,
                split: true,
                margins: '0 0 5 5'
            },
            {
                region: 'center',
                margins: '0 5 5 0',
                layout: 'border',
                border: false,
                items: [
                    {
                        xtype: 'grid_itemcart',
                        iconCls: 'cart',
                        title: 'Detalle de la factura',
                        region: 'center',
                        emptyText: '<div style="text-align:center;font-size: 14px;font-style:italic;padding:50px;">' +
                        'Arrastrar y soltar o doble click sobre un elemento para adicionarlo.' +
                        '<div><img src="../../views/images/shoppingcart.png" width="140" height="140" ' +
                        'style="border: 1px solid #f5f5f5;border-radius: 150px; margin-top: 20px"/></div></div>',
                        viewConfig: {
                            listeners: {
                                render: function (v) {
                                    var gridView = v,
                                        grid = gridView.up('gridpanel');

                                    grid.dropZone = Ext.create('Ext.dd.DropZone', v.el, {
                                        getTargetFromEvent: function (e) {
                                            return v.el;
                                        },
                                        onNodeOver: function (target, dd, e, data) {
                                            return Ext.dd.DropZone.prototype.dropAllowed;
                                        },
                                        onNodeEnter: function (target, dd, e, data) {
                                            Ext.fly(target).addCls('cart-target-hover');
                                        },
                                        onNodeOut: function (target, dd, e, data) {
                                            Ext.fly(target).removeCls('cart-target-hover');
                                        },
                                        onNodeDrop: function (target, dd, e, data) {
                                            me.down('grid_itemcart').fireEvent('onItemDroped', target, dd, e, data);
                                        }
                                    });
                                }
                            }
                        }
                    },
                    {
                        xtype: 'form',
                        itemId: 'formTotals',
                        region: 'south',
                        fieldDefaults: {
                            labelAlign: 'top',
                            msgTarget: 'side',
                            anchor: '100%',
                            labelWidth: 70
                        },
                        bodyPadding: '5 10 10',
                        margins: '5 0 0 0',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'displayfield',
                                flex: 1
                            },
                            {
                                xtype: 'displayfield',
                                name: 'totalArticles',
                                hideLabel: true,
                                fieldStyle: 'text-align:center;font-size:51px;color:black;margin: 15 0px;',
                                width: 80,
                                submitValue: true,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                flex: 1
                            },
                            {
                                xtype: 'displayfield',
                                name: 'subtotal',
                                fieldLabel: 'Subtotal',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right;font-size:22px;color:green;',
                                labelStyle: 'text-align:center;margin-bottom:5px;',
                                width: 110,
                                submitValue: true,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                name: 'descuento',
                                fieldLabel: '(-) Descuento',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:22px;color:green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 110,
                                submitValue: true,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                name: 'impuesto',
                                fieldLabel: '(+) Impuesto',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:22px; color: green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 110,
                                submitValue: true,
                                value: '0'
                            },
                            {
                                xtype: 'displayfield',
                                name: 'importe',
                                fieldLabel: '(=) Total',
                                margins: '0 15 0 0',
                                fieldStyle: 'text-align:right; font-size:25px; color: green;',
                                labelStyle: 'text-align:center; margin-bottom:5px;',
                                width: 130,
                                submitValue: true,
                                value: '0'
                            }
                        ]
                    }
                ]
            }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                tooltip: 'Cerrar la ventana sin guardar los cambios',
                action: 'cancelar',
                scope: this,
                handler: this.hide
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});