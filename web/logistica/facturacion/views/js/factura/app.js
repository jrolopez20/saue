var perfil = window.parent.UCID.portal.perfil;
Ext.application({
    name: 'Factura',
    enableQuickTips: true,
    paths: {
        'Factura': '../../views/js/factura/app'
    },
    controllers: [
        'Factura'
    ],
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [{
                region: 'center',
                xtype: 'grid_factura',
                margin: '5 5 5 5'
            }]
        });
    }
});