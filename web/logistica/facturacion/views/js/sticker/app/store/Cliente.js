Ext.define('Sticker.store.Cliente', {
    extend: 'Ext.data.Store',

    fields: [
        {name: 'idclientesproveedores'},
        {name: 'nombre'},
        {name: 'codigo'},
        {name: 'tipo'},
        {name: 'telefono'},
        {name: 'movil'},
        {name: 'pais'},
        {name: 'provincia'},
        {name: 'direccion'},
        {name: 'codpostal'}
    ],

    pageSize: 25,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'getClientes',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});