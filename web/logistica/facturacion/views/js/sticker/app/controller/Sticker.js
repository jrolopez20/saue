Ext.define('Sticker.controller.Sticker', {
    extend: 'Ext.app.Controller',

    views: [
        'Sticker.view.StickerViewer'
    ],
    stores: [
        'Sticker.store.Cliente'
    ],
    refs: [
        {ref: 'sticker_viewer', selector: 'sticker_viewer'}
    ],

    init: function () {
        var me = this;

        me.control({
            'sticker_viewer button[action=generar]': {
                click: me.onGenerarClick
            }
        });

    },
    onGenerarClick: function (btn) {
        var me = this,
            form = btn.up('form').getForm(),
            params = form.getValues(),
            cliente = me.getStickerStoreClienteStore().findRecord('idclientesproveedores', params.idcliente),
            doc = new jsPDF('p', 'mm', 'a5');

        doc.setProperties({
            title: 'Listado de Stickers',
            author: nombreEmpresa.toUpperCase(),
            keywords: 'sticker',
            creator: 'Sistema SAUE'
        });

        doc.setFont("helvetica");

        if (form.isValid()) {

            for (var i = 0; i < params.cantidad; i++) {
                if (i != 0) {
                    doc.addPage();
                }
                doc.setFontType('bold');
                doc.setFontSize(18);
                doc.text(10, 20, cliente.get('nombre').toUpperCase());

                doc.setFontType('normal');
                doc.setFontSize(14);
                doc.text(10, 30, [
                    cliente.get('direccion').toUpperCase(),
                    cliente.get('provincia').toUpperCase()
                ]);
                //doc.text(10, 40, cliente.get('provincia').toUpperCase());

                doc.setFontSize(20);
                doc.setFontType('bold');
                doc.text(50, 60, 'EMBALAJE');
                doc.setFontSize(14);
                doc.setFontType('normal');
                doc.text(65, 70, 'DE');
                doc.setFontSize(50);
                doc.setFontType('bold');
                doc.text(( i < 10 ? 45 : 35), 85, (i + 1) + '');
                doc.text(85, 85, params.cantidad + '');

                doc.rect(10, 100, 130, 40);
                doc.setFontSize(13);
                doc.setFontType('normal');
                doc.text(65, 105, 'Remite:');
                doc.setFontType('bold');
                doc.text(15, 115, nombreEmpresa.toUpperCase());

                doc.setFontSize(11);
                doc.setFontType('normal');
                doc.text(15, 125, [direccionEmpresa,telefonoEmpresa]);
                //doc.text(20, 125, direccionEmpresa);
                //doc.text(20, 135, telefonoEmpresa);
            }

            var data = doc.output('dataurlstring', {});
            Ext.get('iframe_viewer').dom.src = data;

        }
    }

});