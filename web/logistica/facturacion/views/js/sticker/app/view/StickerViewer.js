Ext.define('Sticker.view.StickerViewer', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.sticker_viewer',

    layout: 'border',
    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'north',
                title: 'Parámetros',
                itemId: 'parameter_form',
                margins: '5 5 5 5',
                fieldDefaults: {
                    //labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 55
                },
                bodyPadding: '10px',
                layout: {
                    type: 'hbox'
                },
                frame: true,
                items: [
                    {
                        xtype: 'numberfield',
                        name: 'cantidad',
                        fieldLabel: '# Cajas',
                        width: 180,
                        value: 1,
                        minValue: 1,
                        maxValue: 300,
                        allowBlank: false,
                        allowDecimals: false,
                        margins: '0 5 0 0'
                    },
                    {
                        xtype: 'combo',
                        store: 'Sticker.store.Cliente',
                        name: 'idcliente',
                        fieldLabel: 'Cliente',
                        width: 500,
                        allowBlank: false,
                        valueField: 'idclientesproveedores',
                        displayField: 'nombre',
                        typeAhead: false,
                        pageSize: 20,
                        margins: '0 5 0 0'
                    },
                    {
                        xtype: 'button',
                        icon: perfil.dirImg + 'generarconfiguracion.png',
                        iconCls: 'btn',
                        action: 'generar',
                        text: 'Generar',
                        tooltip: 'Generar stickers',
                        formBind: true
                    }
                ]
            },
            {
                xtype: 'panel',
                region: 'center',
                itemId: 'viewer',
                title: 'Visor de stickers',
                margins: '0 5 5 5',
                items: {
                    xtype: 'component',
                    id: 'iframe_viewer',
                    html: 'hola muindo',
                    autoEl: {
                        tag: 'iframe',
                        style: 'height: 100%; width: 100%; border: none'
                    }
                }
            }
        ];
        this.callParent(arguments);
    }
});