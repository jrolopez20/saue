var perfil = window.parent.UCID.portal.perfil;
Ext.application({
    name: 'Sticker',
    enableQuickTips: true,
    paths: {
        'Sticker': '../../views/js/sticker/app'
    },
    controllers: [
        'Sticker'
    ],
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [{
                region: 'center',
                xtype: 'sticker_viewer',
                margin: '5 5 5 5'
            }]
        });
    }
});