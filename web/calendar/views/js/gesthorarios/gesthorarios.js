var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gesthorarios', function () {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

function cargarInterfaz() {

    ////------------ Botones ------------////
    var btnAdicionarHorario = Ext.create('Ext.Button', {
        id: 'btnAdicionarHorario',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormHorario('add');
        }
    });
    var btnModificarHorario = Ext.create('Ext.Button', {
        id: 'btnModificarHorario',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormHorario('mod');
        }
    });
    var btnEliminarHorario = Ext.create('Ext.Button', {
        id: 'btnEliminarHorario',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarHorario();
        }
    });

    var txtBuscarHorario = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'txtBuscarHorario',
        enableKeyEvents: true
    });

    txtBuscarHorario.on('keyup', function (tf) {
        if (tf.getValue()) {
            stGpHorario.clearFilter();
            stGpHorario.filter({filterFn: function(item) { return item.get("descripcion").toLowerCase().match(tf.getValue().toLowerCase());}});
        } else
            stGpHorario.clearFilter();
    }, this)

    var btnBuscarHorario = Ext.create('Ext.Button', {
        id: 'btnBuscarHorario',
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });

    var separador = new Ext.menu.Separator();

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpHorario = new Ext.data.Store({
        fields: [
            {
                name: 'idhorario'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'estado'
            },
            {
                name: 'dias'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarHorarios',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarHorario.setDisabled(smodel.getCount() == 0);
                btnEliminarHorario.enable(smodel.getCount() > 0);
            }
        }
    });

    var GpHorario = new Ext.grid.GridPanel({
        store: stGpHorario,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'Código horario',
                flex: 1,
                dataIndex: 'idhorario'
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            },
            {
                text: 'Días',
                flex: 1,
                dataIndex: 'dias'
            }
        ],
        region: 'center',
        tbar: [btnAdicionarHorario, btnModificarHorario, btnEliminarHorario, separador, txtBuscarHorario, btnBuscarHorario],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux77',
            store: stGpHorario,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });
    stGpHorario.load();

    var general = Ext.create('Ext.panel.Panel', { layout: 'border', items: [GpHorario]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarHorario;
    var winModificarHorario;

    var formHorario = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Descripción',
                name: 'descripcion',
                anchor: '96%',
                id: 'descricpion',
                allowBlank: false,
                tabIndex: 1
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Días',
                value: '0',
                name: 'dias',
                anchor: '96%',
                id: 'dias',
                //allowBlank: false,
                maskRe: /^([1-9]|1[0-9])/,
                maxlength: 2,
                tabIndex: 1
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true,
                inputValue: 'true'
            }
        ]
    });

    function mostFormHorario(opcion) {
        switch (opcion) {
            case 'add':
            {
                if (!winAdicionarHorario) {
                    winAdicionarHorario = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitI,
                        closeAction: 'hide',
                        width: 300,
                        height: 200,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                tabIndex: 13,
                                handler: function () {
                                    winAdicionarHorario.hide();
                                }
                            },
                            {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                tabIndex: 12,
                                handler: function () {
                                    AdicionarHorario('apl');
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                tabIndex: 11,
                                handler: function () {
                                    AdicionarHorario();
                                    winAdicionarHorario.hide();
                                }
                            }
                        ]
                    });
                }
                formHorario.getForm().reset();
                winAdicionarHorario.add(formHorario);
                winAdicionarHorario.doLayout();
                winAdicionarHorario.show();
            }
                break;
            case 'mod':
            {
                if (!winModificarHorario)
                    winModificarHorario = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitII,
                        closeAction: 'hide',
                        width: 300,
                        height: 200,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function () {
                                    winModificarHorario.hide();
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function () {
                                    ModificarHorario();
                                    winModificarHorario.hide();
                                }
                            }
                        ]
                    });
                formHorario.getForm().reset();
                winModificarHorario.add(formHorario);
                winModificarHorario.doLayout();
                winModificarHorario.show();
                formHorario.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }


    //funciones

    function AdicionarHorario(apl) {
        if (formHorario.getForm().isValid()) {
            formHorario.getForm().submit({
                url: 'insertarHorario',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formHorario.getForm().reset();
                        stGpHorario.load();
                        if (!apl)
                            winAdicionarLocal.hide();
                        sm.clearSelections();
                        btnModificarHorario.disable();
                        btnEliminarHorario.disable();
                    }
                }
            });
        }
    }

    function ModificarHorario() {
        if (formHorario.getForm().isValid()) {
            formHorario.getForm().submit({
                url: 'modificarHorario',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idhorario: sm.getLastSelected().data.idhorario
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarHorario.hide();
                        stGpHorario.load();
                    }
                }
            });
        }
    }

    function EliminarHorario() {
        mostrarMensaje(2, '¿Desea eliminar este horario?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando horario...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.idhorario);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarHorario',
                    method: 'POST',
                    params: {
                        idhorarios: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpHorario.load()
                            btnModificarHorario.disable();
                            btnEliminarHorario.disable();
                        }
                    }
                });
            }
        }
    }
}