var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gesttipoperiodo', function () {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

function cargarInterfaz() {
    ////------------ Botones ------------////
    var btnAdicionarTipoPeriodo = Ext.create('Ext.Button', {
        id: 'btnAdicionarTipoPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormTipoPeriodo('add');
        }
    });
    var btnModificarTipoPeriodo = Ext.create('Ext.Button', {
        id: 'btnModificarTipoPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormTipoPeriodo('mod');
        }
    });
    var btnEliminarTipoPeriodo = Ext.create('Ext.Button', {
        id: 'btnEliminarTipoPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarTipoPeriodo();
        }
    });

    var txtBuscarTipoPeriodo = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'txtBuscarTipoPeriodo',
        enableKeyEvents: true
    });

    txtBuscarTipoPeriodo.on('keyup', function (tf) {
        if (tf.getValue()) {
            stGpTipoPeriodo.clearFilter();
            stGpTipoPeriodo.filter('descripcion', tf.getValue());
        } else
            stGpTipoPeriodo.clearFilter();
    }, this)

    var btnBuscarTipoPeriodo = Ext.create('Ext.Button', {
        id: 'btnBuscarTipoPeriodo',
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });

    var separador = new Ext.menu.Separator();

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpTipoPeriodo = new Ext.data.Store({
        fields: [
            {
                name: 'idtipo_periododocente'
            },
            {
                name: 'tipoperiodo'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'estado'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarTipoPeriodos',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarTipoPeriodo.setDisabled(smodel.getCount() == 0);
                btnEliminarTipoPeriodo.setDisabled(smodel.getCount() == 0);
            }
        }
    });

    var GpTipoPeriodo = new Ext.grid.GridPanel({
        store: stGpTipoPeriodo,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'idtipo_periododocente',
                flex: 1,
                dataIndex: 'idtipo_periododocente',
                hidden: true
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'tipoperiodo'
            }
        ],
        region: 'center',
        tbar: [btnAdicionarTipoPeriodo, btnModificarTipoPeriodo, btnEliminarTipoPeriodo, separador, txtBuscarTipoPeriodo, btnBuscarTipoPeriodo],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux55',
            store: stGpTipoPeriodo,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        }),
        viewConfig:{
            getRowClass: function(record, rowIndex, rowParams, store){
                if (record.data.estado == false)
                    return 'FilaRoja';
            }
        }
    });
    stGpTipoPeriodo.load();

    var general = Ext.create('Ext.panel.Panel', { layout: 'border', items: [GpTipoPeriodo]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarTipoPeriodo;
    var winModificarTipoPeriodo;

    var formTipoPeriodo = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Descripción',
                name: 'tipoperiodo',
                anchor: '96%',
                id: 'tipoperiodo',
                allowBlank: false,
                maskRe: /[A-Z a-z]/,
                tabIndex: 1
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true
            }
        ]
    });

    function mostFormTipoPeriodo(opcion) {
        switch (opcion) {
            case 'add':
            {
                winAdicionarTipoPeriodo = Ext.create('Ext.Window', {
                    title: perfil.etiquetas.lbTitVentanaTitI,
                    closeAction: 'hide',
                    width: 300,
                    height: 200,
                    modal:true,
                    constrain: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            tabIndex: 13,
                            handler: function () {
                                winAdicionarTipoPeriodo.hide();
                            }
                        },
                        {
                            text: 'Aplicar',
                            icon: perfil.dirImg + 'aplicar.png',
                            tabIndex: 12,
                            handler: function () {
                                AdicionarTipoPeriodo('apl');
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            tabIndex: 11,
                            handler: function () {
                                AdicionarTipoPeriodo();
                            }
                        }
                    ]
                });
                formTipoPeriodo.getForm().reset();
                winAdicionarTipoPeriodo.add(formTipoPeriodo);
                winAdicionarTipoPeriodo.show();
            }
                break;
            case 'mod':
            {
                winModificarTipoPeriodo = Ext.create('Ext.Window', {
                    title: perfil.etiquetas.lbTitVentanaTitII,
                    closeAction: 'hide',
                    width: 300,
                    height: 200,
                    constrain: true,
                    modal:true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            handler: function () {
                                winModificarTipoPeriodo.hide();
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            handler: function () {
                                ModificarTipoPeriodo();
                            }
                        }
                    ]
                });
                formTipoPeriodo.getForm().reset();
                winModificarTipoPeriodo.add(formTipoPeriodo);
                winModificarTipoPeriodo.doLayout();
                winModificarTipoPeriodo.show();
                formTipoPeriodo.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }


    //funciones

    function AdicionarTipoPeriodo(apl) {
        if (formTipoPeriodo.getForm().isValid()) {
            formTipoPeriodo.getForm().submit({
                url: 'insertarTipoPeriodo',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formTipoPeriodo.getForm().reset();
                        stGpTipoPeriodo.load();
                        if (!apl)
                            winAdicionarTipoPeriodo.hide();
                        sm.clearSelections();
                        btnModificarTipoPeriodo.disable();
                        btnEliminarTipoPeriodo.disable();
                    }
                }
            });
        }else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function ModificarTipoPeriodo() {
        if (formTipoPeriodo.getForm().isValid()) {
            formTipoPeriodo.getForm().submit({
                url: 'modificarTipoPeriodo',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idtipoperiododocente: sm.getSelection()[0].raw.idtipo_periododocente
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarTipoPeriodo.hide();
                        stGpTipoPeriodo.load();
                        btnModificarTipoPeriodo.disable();
                        btnEliminarTipoPeriodo.disable();
                    }
                }
            });
        }else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function EliminarTipoPeriodo() {
        mostrarMensaje(2, '¿Desea eliminar este tipo de periodo docente?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando tipo de periodo docente...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.idtipo_periododocente);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarTipoPeriodo',
                    method: 'POST',
                    params: {
                        idtipoperiododocentes: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpTipoPeriodo.load()
                            btnModificarTipoPeriodo.disable();
                            btnEliminarTipoPeriodo.disable();
                        }
                    }
                });
            }
        }
    }
}