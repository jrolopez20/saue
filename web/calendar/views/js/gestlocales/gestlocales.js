var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestlocales', function () {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

function cargarInterfaz() {
    ////------------ Botones ------------////
    var btnAdicionarLocal = Ext.create('Ext.Button', {
        id: 'btnAdicionarLocal',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormLocal('add');
        }
    });
    var btnModificarLocal = Ext.create('Ext.Button', {
        id: 'btnModificarLocal',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormLocal('mod');
        }
    });
    var btnEliminarLocal = Ext.create('Ext.Button', {
        id: 'btnEliminarLocal',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarLocal();
        }
    });

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpLocal = new Ext.data.Store({
        fields: [
            {
                name: 'idaula'
            },
            {
                name: 'idtipo_aula'
            },
            {
                name: 'idubicacion'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'ubicacion'
            },
            {
                name: 'tipolocal'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'estado'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarLocalesByD',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarLocal.setDisabled(smodel.getCount() == 0);
                btnEliminarLocal.enable(smodel.getCount() > 0);
            }
        }
    });

    var GpLocal = new Ext.grid.GridPanel({
        store: stGpLocal,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'idaula',
                flex: 1,
                dataIndex: 'idaula',
                hidden: true
            },
            {
                text: 'idtipo_aula',
                flex: 1,
                dataIndex: 'idtipo_aula',
                hidden: true
            },
            {
                text: 'idubicacion',
                flex: 1,
                dataIndex: 'idubicacion',
                hidden: true
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            },
            {
                text: 'Tipo Local',
                flex: 1,
                dataIndex: 'tipolocal'
            },
            {
                text: 'Ubicación',
                flex: 1,
                dataIndex: 'ubicacion'
            }
        ],
        region: 'center',
        tbar: [btnAdicionarLocal, btnModificarLocal, btnEliminarLocal,
            {xtype: 'searchfield',
                store: stGpLocal,
                anchor: '100%',
                //width: 400,
                fieldLabel: perfil.etiquetas.lbBtnBuscar,
                labelWidth: 40,
                filterPropertysNames: ['descripcion']
            }],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux55',
            store: stGpLocal,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        }),
        viewConfig: {
            getRowClass: function (record, rowIndex, rowParams, store) {
                if (record.data.estado == false)
                    return 'FilaRoja';
            }
        }
    });
    stGpLocal.load();

    var general = Ext.create('Ext.panel.Panel', { layout: 'border', items: [GpLocal]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarLocal;
    var winModificarLocal;

    //elementos del formulario
    var stcmbTipoLocal = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idtipo_aula'
            },
            {
                name: 'descripcion'
            }
        ],
        proxy: {
            type: 'ajax',
            url: '../gesttipolocales/cargarTipoLocalesA',
            reader: {
                type: 'json',
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    stcmbTipoLocal.load();

    var cmbTipoLocal = Ext.create('Ext.form.field.ComboBox', {
        id: 'idtipo_aula',
        name: 'idtipolocal',
        fieldLabel: 'Tipo local',
        editable: false,
        displayField: 'descripcion',
        valueField: 'idtipo_aula',
        hiddenName: 'idtipo_aula',
        valueName: 'idtipo_aula',
        anchor: '96%',
        labelWidth: 130,
        store: stcmbTipoLocal,
        emptyText: '--seleccione--',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        tabIndex: 3
    });

    var stcmbUbicacion = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idubicacion'
            },
            {
                name: 'descripcion'
            }
        ],
        proxy: {
            type: 'ajax',
            url: 'cargarUbicacionA',
            reader: {
                type: 'json',
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    stcmbUbicacion.load();

    var cmbUbicacion = Ext.create('Ext.form.field.ComboBox', {
        id: 'idubicacion',
        name: 'idubicacion',
        fieldLabel: 'Ubicación',
        editable: false,
        displayField: 'descripcion',
        valueField: 'idubicacion',
        anchor: '96%',
        labelWidth: 130,
        store: stcmbUbicacion,
        emptyText: '--seleccione--',
        queryMode: 'local',
        typeAhead: true,
        allowBlank: false,
        tabIndex: 3
    });

    var formLocal = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Descripci&oacute;n',
                name: 'descripcion',
                anchor: '96%',
                id: 'descripcion',
                allowBlank: false,
                maskRe: /^[a-z0-9A-Z_\xE1\xE9\xED\xF3\xFA\xC1\xC9\xCD\xD3\xDA\xD1\xF1\x2E\x2D\s]+$/,
                tabIndex: 1
            },
            cmbTipoLocal,
            cmbUbicacion,
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true
            }
        ]
    });

    function mostFormLocal(opcion) {
        switch (opcion) {
            case 'add':
            {
                if (!winAdicionarLocal) {
                    winAdicionarLocal = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitI,
                        closeAction: 'hide',
                        width: 300,
                        height: 250,
                        constrain: true,
                        resizable: false,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                tabIndex: 13,
                                handler: function () {
                                    winAdicionarLocal.hide();
                                }
                            },
                            {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                tabIndex: 12,
                                handler: function () {
                                    AdicionarLocal('apl');
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                tabIndex: 11,
                                handler: function () {
                                    AdicionarLocal();
                                }
                            }
                        ]
                    });
                }
                winAdicionarLocal.add(formLocal);
                winAdicionarLocal.doLayout();
                winAdicionarLocal.show();
                formLocal.getForm().reset();
            }
                break;
            case 'mod':
            {
                if (!winModificarLocal)
                    winModificarLocal = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitII,
                        closeAction: 'hide',
                        width: 300,
                        height: 250,
                        constrain: true,
                        resizable: false,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function () {
                                    winModificarLocal.hide();
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function () {
                                    ModificarLocal();
                                }
                            }
                        ]
                    });
                winModificarLocal.add(formLocal);
                winModificarLocal.doLayout();
                winModificarLocal.show();
                formLocal.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }


    //funciones

    function AdicionarLocal(apl) {
        if (formLocal.getForm().isValid()) {
            formLocal.getForm().submit({
                url: 'insertarLocal',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formLocal.getForm().reset();
                        stGpLocal.load();
                        if (!apl)
                            winAdicionarLocal.hide();
                        sm.clearSelections();
                        btnModificarLocal.disable();
                        btnEliminarLocal.disable();
                    }
                }
            });
        } else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function ModificarLocal() {
        if (formLocal.getForm().isValid()) {
            formLocal.getForm().submit({
                url: 'modificarLocal',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idlocal: sm.getSelection()[0].raw.idaula
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarLocal.hide();
                        stGpLocal.load();
                        btnModificarLocal.disable();
                        btnEliminarLocal.disable();
                    }
                }
            });
        } else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function EliminarLocal() {
        mostrarMensaje(2, '¿Desea eliminar este local?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando local...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.idaula);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarLocal',
                    method: 'POST',
                    params: {
                        idlocales: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpLocal.load();
                            btnModificarLocal.disable();
                            btnEliminarLocal.disable();
                        }
                    }
                });
            }
        }
    }
}
