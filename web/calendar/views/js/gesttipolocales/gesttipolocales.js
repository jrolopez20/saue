var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gesttipolocales', function () {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();


function cargarInterfaz() {
    ////------------ Botones ------------////
    var btnAdicionarTipoLocal = Ext.create('Ext.Button', {
        id: 'btnAdicionarTipoLocal',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormTipoLocal('add');
        }
    });
    var btnModificarTipoLocal = Ext.create('Ext.Button', {
        id: 'btnModificarTipoLocal',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormTipoLocal('mod');
        }
    });
    var btnEliminarTipoLocal = Ext.create('Ext.Button', {
        id: 'btnEliminarTipoLocal',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarTipoLocal();
        }
    });

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpTipoLocal = new Ext.data.Store({
        fields: [
            {
                name: 'idtipo_aula'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'estado'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarTipoLocalesByD',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarTipoLocal.setDisabled(smodel.getCount() == 0);
                btnEliminarTipoLocal.enable(smodel.getCount() > 0);
            }
        }
    });

    var GpTipoLocal = new Ext.grid.GridPanel({
        store: stGpTipoLocal,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'idtipo_aula',
                flex: 1,
                dataIndex: 'idtipo_aula',
                hidden: true
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            }
        ],
        region: 'center',
        tbar: [btnAdicionarTipoLocal, btnModificarTipoLocal, btnEliminarTipoLocal,{
            xtype: 'searchfield',
            store: stGpTipoLocal,
            anchor: '100%',
            //width: 400,
            fieldLabel: perfil.etiquetas.lbBtnBuscar,
            labelWidth: 40,
            filterPropertysNames: ['descripcion']
        }],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux55',
            store: stGpTipoLocal,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        }),
        viewConfig:{
            getRowClass: function(record, rowIndex, rowParams, store){
                if (record.data.estado == false)
                    return 'FilaRoja';
            }
        }
    });
    stGpTipoLocal.load();

    var general = Ext.create('Ext.panel.Panel', { layout: 'border', items: [GpTipoLocal]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarTipoLocal;
    var winModificarTipoLocal;

    var formTipoLocal = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'left',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Descripción',
                name: 'descripcion',
                anchor: '96%',
                id: 'descricpion',
                allowBlank: false,
                maskRe: /[A-Z a-z]/,
                tabIndex: 1
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true
            }
        ]
    });

    function mostFormTipoLocal(opcion) {
        switch (opcion) {
            case 'add':
            {
                winAdicionarTipoLocal = Ext.create('Ext.Window', {
                    title: perfil.etiquetas.lbTitVentanaTitI,
                    closeAction: 'hide',
                    width: 300,
                    height: 150,
                    constrain: true,
                    modal: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            tabIndex: 13,
                            handler: function () {
                                winAdicionarTipoLocal.hide();
                            }
                        },
                        {
                            text: 'Aplicar',
                            icon: perfil.dirImg + 'aplicar.png',
                            tabIndex: 12,
                            handler: function () {
                                AdicionarTipoLocal('apl');
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            tabIndex: 11,
                            handler: function () {
                                AdicionarTipoLocal();
                            }
                        }
                    ]
                });
                formTipoLocal.getForm().reset();
                winAdicionarTipoLocal.add(formTipoLocal);
                winAdicionarTipoLocal.show();
            }
                break;
            case 'mod':
            {
                winModificarTipoLocal = Ext.create('Ext.Window', {
                    title: perfil.etiquetas.lbTitVentanaTitII,
                    closeAction: 'hide',
                    width: 300,
                    height: 150,
                    constrain: true,
                    modal: true,
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Cancelar',
                            icon: perfil.dirImg + 'cancelar.png',
                            handler: function () {
                                winModificarTipoLocal.hide();
                            }
                        },
                        {
                            text: 'Aceptar',
                            icon: perfil.dirImg + 'aceptar.png',
                            handler: function () {
                                ModificarTipoLocal();
                            }
                        }
                    ]
                });
                formTipoLocal.getForm().reset();
                winModificarTipoLocal.add(formTipoLocal);
                winModificarTipoLocal.doLayout();
                winModificarTipoLocal.show();
                formTipoLocal.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }


    //funciones

    function AdicionarTipoLocal(apl) {
        if (formTipoLocal.getForm().isValid()) {
            formTipoLocal.getForm().submit({
                url: 'insertarTipoLocal',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formTipoLocal.getForm().reset();
                        stGpTipoLocal.load();
                        if (!apl)
                            winAdicionarTipoLocal.hide();
                        sm.clearSelections();
                        btnModificarTipoLocal.disable();
                        btnEliminarTipoLocal.disable();
                    }
                }
            });
        }else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function ModificarTipoLocal() {
        if (formTipoLocal.getForm().isValid()) {
            formTipoLocal.getForm().submit({
                url: 'modificarTipoLocal',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idtipo_aula: sm.getLastSelected().data.idtipo_aula
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarTipoLocal.hide();
                        stGpTipoLocal.load();
                    }
                }
            });
        }else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function EliminarTipoLocal() {
        mostrarMensaje(2, '¿Desea eliminar este tipo de local?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando tipo de local...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.idtipo_aula);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'EliminarTipoLocal',
                    method: 'POST',
                    params: {
                        idtipo_locales: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpTipoLocal.load()
                            btnModificarTipoLocal.disable();
                            btnEliminarTipoLocal.disable();
                        }
                    }
                });
            }
        }
    }
}