var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gestperiodos', function () {
    cargarInterfaz();
});

////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

function cargarInterfaz() {

    ////------------ Botones ------------////
    var btnAdicionarPeriodo = Ext.create('Ext.Button', {
        id: 'btnAdicionarPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormPeriodo('add');
        }
    });

    var btnModificarPeriodo = Ext.create('Ext.Button', {
        id: 'btnModificarPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            mostFormPeriodo('mod');
        }
    });

    var btnEliminarPeriodo = Ext.create('Ext.Button', {
        id: 'btnEliminarPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarLocal();
        }
    });

    var txtBuscarPeriodo = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'txtBuscarPeriodo',
        enableKeyEvents: true
    });

    txtBuscarPeriodo.on('keyup', function (tf) {
        if (tf.getValue()) {
            stGpPeriodo.clearFilter();
            stGpPeriodo.filter('descripcion', tf.getValue());
        } else
            stGpPeriodo.clearFilter();
    }, this)

    var btnBuscarPeriodo = Ext.create('Ext.Button', {
        id: 'btnBuscarPeriodo',
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });

    var separador = new Ext.menu.Separator();

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);

    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stGpPeriodo = new Ext.data.Store({
        fields: [
            {
                name: 'idperiododocente'
            },
            {
                name: 'idtipo_periododocente'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'tipoperiodo'
            },
            {
                name: 'fecha'
            },
            {
                name: 'idusuario'
            },
            {
                name: 'estado'
            },
            {
                name: 'codperiodo'
            },
            {
                name: 'anno'
            },
            {
                name: 'letra'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarPeriodos',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificarPeriodo.setDisabled(smodel.getCount() == 0);
                btnEliminarPeriodo.setDisabled(smodel.getCount() == 0);
            }
        }
    });

    var GpPeriodo = new Ext.grid.GridPanel({
        store: stGpPeriodo,
        frame: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'idperiododocente',
                flex: 1,
                dataIndex: 'idperiododocente',
                hidden: true
            },
            {
                text: 'idtipo_periododocente',
                flex: 1,
                dataIndex: 'idtipo_periododocente',
                hidden: true
            },
            {
                text: 'Código periodo',
                flex: 1,
                dataIndex: 'codperiodo'
            },
            {
                text: 'Descripción',
                flex: 1,
                dataIndex: 'descripcion'
            },
            {
                text: 'Tipo periodo',
                flex: 1,
                dataIndex: 'tipoperiodo'
            }
        ],
        region: 'center',
        tbar: [btnAdicionarPeriodo, btnModificarPeriodo, btnEliminarPeriodo, separador, txtBuscarPeriodo, btnBuscarPeriodo],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux55',
            store: stGpPeriodo,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });

    stGpPeriodo.load();

    var general = Ext.create('Ext.panel.Panel', { layout: 'border', items: [GpPeriodo]});
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: general});

    //formulario
    var winAdicionarPeriodo;
    var winModificarPeriodo;

    var stcmbTipoPeriodo = Ext.create('Ext.data.Store', {
        fields: [
            {
                name: 'idtipo_periododocente'
            },
            {
                name: 'tipoperiodo'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        autoLoad:true,
        proxy: {
            type: 'ajax',
            url: '../gesttipoperiodo/cargarTipoPeriodos',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });


    var stcmbAnno = Ext.data.SimpleStore({
        fields: ["anno"],
        data: [
            {"anno": '1999'},
            {"anno": '2000'},
            {"anno": '2001'},
            {"anno": '2002'},
            {"anno": '2003'},
            {"anno": '2004'},
            {"anno": '2005'},
            {"anno": '2006'},
            {"anno": '2007'},
            {"anno": '2008'},
            {"anno": '2009'},
            {"anno": '2010'},
            {"anno": '2011'},
            {"anno": '2012'},
            {"anno": '2013'},
            {"anno": '2014'}
        ]
    });
    var stcmbLetra = Ext.data.SimpleStore({
        fields: ["letra"],
        data: [
            {"letra": 'A'},
            {"letra": 'B'},
            {"letra": 'C'},
            {"letra": 'D'},
            {"letra": 'E'},
            {"letra": 'F'},
            {"letra": 'G'},
            {"letra": 'H'},
            {"letra": 'I'},
            {"letra": 'J'},
            {"letra": 'K'},
            {"letra": 'L'},
            {"letra": 'M'},
            {"letra": 'N'},
            {"letra": 'O'},
            {"letra": 'P'},
            {"letra": 'Q'},
            {"letra": 'R'},
            {"letra": 'S'},
            {"letra": 'T'},
            {"letra": 'U'},
            {"letra": 'V'},
            {"letra": 'W'},
            {"letra": 'X'},
            {"letra": 'Y'},
            {"letra": 'Z'}
        ]
    });
    var formPeriodo = Ext.create('Ext.form.Panel', {
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 150,
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                xtype: 'fieldset',
                title: 'Código',
                defaults: {width: '80%'},
                items: [
                    {
                        xtype: 'container',
                        layout: 'column',
                        items: [
                            {
                                xtype: 'container',
                                columnWidth: .8,
                                layout: 'anchor',
                                items: [
                                    {
                                        xtype: 'combo',
                                        store: stcmbAnno,
                                        fieldLabel: 'Año',
                                        name: 'anno',
                                        emptyText: '--seleccione--',
                                        width: 100,
                                        id: 'anno',
                                        queryMode: 'local',
                                        valueField: 'anno',
                                        allowBlank: false,
                                        displayField: 'anno'
                                    }
                                ]
                            },
                            {
                                xtype: 'combo',
                                fieldLabel: 'Letra',
                                queryMode: 'local',
                                store: stcmbLetra,
                                valueField: 'letra',
                                displayField: 'letra',
                                name: 'letra',
                                emptyText: '--seleccione--',
                                width: 100,
                                id: 'letra',
                                allowBlank: false
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Descripción',
                name: 'descripcion',
                anchor: '100%',
                id: 'descripcion',
                allowBlank: false
            },
            {
                xtype: 'combo',
                fieldLabel: 'Tipo periodo',
                name: 'idtipo_periododocente',
                emptyText: '--seleccione--',
                anchor: '100%',
                id: 'idtipo_periododocente',
                allowBlank: false,
                store: stcmbTipoPeriodo,
                queryMode: 'local',
                typeAhead: true,
                displayField: 'tipoperiodo',
                valueField: 'idtipo_periododocente'
            },
            {
                xtype: 'checkbox',
                fieldLabel: 'Activado:',
                name: 'estado',
                labelAlign: 'left',
                style: {
                    marginTop: '10px'
                },
                checked: true
            }
        ]
    });

    function mostFormPeriodo(opcion) {
        switch (opcion) {
            case 'add':
            {
                if (!winAdicionarPeriodo)
                    winAdicionarPeriodo = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitI,
                        closeAction: 'hide',
                        width: 300,
                        height: 300,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                tabIndex: 13,
                                handler: function () {
                                    winAdicionarPeriodo.hide();
                                }
                            },
                            {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                tabIndex: 12,
                                handler: function () {
                                    AdicionarPeriodo('apl');
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                tabIndex: 11,
                                handler: function () {
                                    AdicionarPeriodo();
                                }
                            }
                        ]
                    });
                formPeriodo.getForm().reset();
                winAdicionarPeriodo.add(formPeriodo);
                winAdicionarPeriodo.show();
            }
                break;
            case 'mod':
            {
                if (!winModificarPeriodo)
                    winModificarPeriodo = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitII,
                        closeAction: 'hide',
                        width: 300,
                        height: 300,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function () {
                                    winModificarPeriodo.hide();
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function () {
                                    ModificarPeriodo();
                                }
                            }
                        ]
                    });
                formPeriodo.getForm().reset();
                winModificarPeriodo.add(formPeriodo);
                winModificarPeriodo.doLayout();
                winModificarPeriodo.show();
                formPeriodo.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }

    //funciones

    function AdicionarPeriodo(apl) {
        if (formPeriodo.getForm().isValid()) {
            formPeriodo.getForm().submit({
                url: 'insertarPeriodo',
                waitMsg: perfil.etiquetas.lbMsgFunAdicionarMsg,
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        formPeriodo.getForm().reset();
                        stGpPeriodo.load();
                        if (!apl)
                            winAdicionarPeriodo.hide();
                        sm.clearSelections();
                        btnModificarPeriodo.disable();
                        btnEliminarPeriodo.disable();
                    }
                }
            });
        } else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function ModificarPeriodo() {
        if (formPeriodo.getForm().isValid()) {
            formPeriodo.getForm().submit({
                url: 'modificarPeriodo',
                waitMsg: perfil.etiquetas.lbMsgFunModificarMsg,
                params: {
                    idperiododocente: sm.getSelection()[0].raw.idperiododocente
                },
                failure: function (form, action) {
                    if (action.result.codMsg != 3) {
                        winModificarPeriodo.hide();
                        stGpPeriodo.load();
                        btnModificarPeriodo.disable();
                        btnEliminarPeriodo.disable();
                    }
                }
            });
        } else
            mostrarMensaje(3, 'Existen campos obligatorios vacíos.')
    }

    function EliminarLocal() {
        mostrarMensaje(2, '¿Desea eliminar este periodo docente?', eliminar);
        var delMask = new Ext.LoadMask(Ext.getBody(), {
            msg: 'Eliminando periodo docente...'
        });

        function eliminar(btnPresionado) {
            if (btnPresionado === 'ok') {
                var ids = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    ids.push(sm.getSelection()[i].raw.idperiododocente);
                }
                delMask.show();
                Ext.Ajax.request({
                    url: 'eliminarPeriodo',
                    method: 'POST',
                    params: {
                        idperiododocentes: Ext.JSON.encode(ids)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        delMask.disable();
                        delMask.hide();
                        if (responseData.codMsg === 1) {
                            sm.clearSelections();
                            stGpPeriodo.load();
                            btnModificarPeriodo.disable();
                            btnEliminarPeriodo.disable();
                        }
                    }
                });
            }
        }
    }
}