var perfil = window.parent.UCID.portal.perfil;
UCID.portal.cargarEtiquetas('gesthorarioxperiodo', function () {
    cargarInterfaz();
});
////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();
var winAdd, winMod;
function cargarInterfaz() {
    ////------------ Botones ------------////
    var btnAdicionar = Ext.create('Ext.Button', {
        id: 'btnAdicionar',
        text: '<b>' + perfil.etiquetas.lbBtnAdicionar + '</b>',
        icon: perfil.dirImg + 'adicionar.png',
        iconCls: 'btn',
        handler: function () {
            showWin('add');
        }
    });

    var btnModificar = Ext.create('Ext.Button', {
        id: 'btnModificar',
        text: '<b>' + perfil.etiquetas.lbBtnModificar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'modificar.png',
        iconCls: 'btn',
        handler: function () {
            showWin('mod');
        }
    });

    var btnEliminar = Ext.create('Ext.Button', {
        id: 'btnEliminar',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            eliminarHorarioXPeriodo();
        }
    });

    var txtBuscar = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'txtBuscar',
        enableKeyEvents: true
    });

    txtBuscar.on('keyup', function (tf) {
        if (tf.getValue()) {
            stgridHorario.clearFilter();
            stgridHorario.filter('horario', tf.getValue());
        } else
            stgridHorario.clearFilter();
    }, this)

    var btnBuscar = Ext.create('Ext.Button', {
        id: 'btnBuscar',
        disabled: true,
        icon: perfil.dirImg + 'buscar.png',
        iconCls: 'btn'
    });

    var btnRecargar = new Ext.Button({
        disabled: false,
        icon: perfil.dirImg + 'actualizar.png',
        iconCls: 'btn',
        text: perfil.etiquetas.lbBtnRecargar,
        handler: function () {
            stGestores.load();
        }
    });
    var separador = new Ext.menu.Separator();

    UCID.portal.cargarAcciones(window.parent.idFuncionalidad);
//
    var stPeriodo = Ext.create('Ext.data.TreeStore', {
        model: 'cargarPeriodos',
        idProperty: 'idperiododocente',
        proxy: {
            type: 'ajax',
            url: 'cargarPeriodos',
            actionMethods: { //Esta Linea es necesaria para el metodo de llamada POST o GET

                read: 'POST'
            },
            reader: {
                type: 'json'
            }
        }
    });
////------------ Arbol Periodos ------------////
    var arbolPeriodos = new Ext.tree.TreePanel({
        title: perfil.etiquetas.lbRootNode,
        collapsible: true,
        autoScroll: true,
        region: 'west',
        store: stPeriodo,
        root: {
            expandable: true,
            text: perfil.etiquetas.lbRootNode,
            expanded: true,
            id: '0'
        },
        split: true,
        width: '25%',
        bbar: ['->', btnRecargar]
    });
    var idperiodo;
    var nperiodo;
////--------------- Evento para habilitar botones -------------////
    arbolPeriodos.on('itemclick', function (a, node, item, index, e, eOpts) {
        btnEliminar.disable();
        btnModificar.disable();
        btnAdicionar.enable();
        grid.enable();
        stgridHorario.removeAll();
        idperiodo = node.data.id;
        nperiodo = node.data.text;
        grid.setTitle('Horarios en el periodo ' + nperiodo);
        stgridHorario.load({params: {start: 0, limit: 20, idperiodo: node.data.id}});
    }, this);
    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stgridHorario = new Ext.data.Store({
        fields: [
            {
                name: 'idhorario_periododocente'
            },
            {
                name: 'idperiododocente'
            },
            {
                name: 'idhorario'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'idprofesor'
            },
            {
                name: 'profesor'
            },
            {
                name: 'horas'
            },
            {
                name: 'semanas'
            },
            {
                name: 'maximas_falta'
            },
            {
                name: 'estado'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarHorariosXPeriodos',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });

    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var sm = new Ext.selection.RowModel({
        mode: 'MULTI',
        listeners: {
            selectionchange: function (smodel, rowIndex, keepExisting, record) {
                btnModificar.setDisabled(smodel.getCount() == 0);
                btnEliminar.enable(smodel.getCount() > 0);
            }
        }
    });

    var grid = new Ext.grid.GridPanel({
        store: stgridHorario,
        frame: true,
        disabled: true,
        region: 'center',
        selModel: sm,
        columns: [
            {
                text: 'idhorario_periododocente',
                flex: 1,
                dataIndex: 'idhorario_periododocente',
                hidden: true
            },
            {
                text: 'idperiododocente',
                flex: 1,
                dataIndex: 'idperiododocente',
                hidden: true
            },
            {
                text: 'idhorario',
                flex: 1,
                dataIndex: 'idhorario',
                hidden: true
            },
            {
                text: 'idprofesor',
                flex: 1,
                dataIndex: 'idprofesor',
                hidden: true
            },
            {
                text: 'Descripcion',
                flex: 1,
                dataIndex: 'descripcion'
            },
            {
                text: 'Profesor',
                flex: 1,
                dataIndex: 'profesor'
            },
            {
                text: 'Horas',
                flex: 1,
                dataIndex: 'horas'
            },
            {
                text: 'Semanas',
                flex: 1,
                dataIndex: 'semanas'
            },
            {
                text: 'M&aacute;ximas Faltas',
                flex: 1,
                dataIndex: 'maximas_falta'
            }
        ],
        region: 'center',
        tbar: [btnAdicionar, btnModificar, btnEliminar, separador, txtBuscar, btnBuscar],
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux55',
            store: stgridHorario,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });
    ////------------ Store del Grid de tipo de estudiantes ------------////
    var stgridAddHorario = new Ext.data.Store({
        fields: [
            {
                name: 'idhorario'
            },
            {
                name: 'descripcion'
            },
            {
                name: 'codigo'
            },
            {
                name: 'dias'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarHorarios',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    var stcmbProf = new Ext.data.Store({
        fields: [
            {
                name: 'idprofesor'
            },
            {
                name: 'nombre'
            }
        ],
        remoteSort: true,
        pageSize: 20,
        proxy: {
            type: 'ajax',
            url: 'cargarProfesores',
            reader: {
                totalProperty: "cantidad",
                root: "datos"
            },
            actionMethods: {
                read: 'POST'
            }
        }
    });
    stcmbProf.load();
    ////------------ Establesco modo de seleccion de grid (single) ------------////
    var smwin = new Ext.selection.RowModel({
        mode: 'MULTI'
    });

    var gridAddHor = new Ext.grid.GridPanel({
        store: stgridAddHorario,
        frame: true,
        height: '100%',
        selModel: smwin,
        columns: [
            {
                text: 'idhorario',
                flex: 1,
                dataIndex: 'idhorario_periododocente',
                hidden: true
            },
            {
                text: 'Codigo',
                flex: 1,
                dataIndex: 'codigo'
            },
            {
                text: 'Descripcion',
                flex: 1,
                dataIndex: 'descripcion'
            },
            {
                text: 'Dias',
                flex: 1,
                dataIndex: 'dias'
            }
        ],
        region: 'center',
        bbar: new Ext.PagingToolbar({
            id: 'ptbaux565',
            store: stgridAddHorario,
            displayInfo: true,
            displayMsg: perfil.etiquetas.lbMsgbbarI,
            emptyMsg: perfil.etiquetas.lbMsgbbarII
        })
    });
    var formHorXPer = new Ext.FormPanel({
        frame: true,
        height: '100%',
        bodyStyle: 'padding:5px auto 0px',
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },
        items: [
            {
                //  title: 'Información personal',
                layout: 'column',
                border: false,
                items: [
                    {
                        defaults: {width: '50%'},
                        columnWidth: .5,
                        border: false,
                        bodyStyle: 'padding:5px 0 5px 5px',
                        items: [
                            {
                                xtype: 'numberfield',
                                fieldLabel: 'Horas',
                                name: 'horas',
                                anchor: '95%',
                                allowBlank: false
                            },
                            {
                                xtype: 'textfield',
                                name: "semanas",
                                fieldLabel: 'Semanas',
                                allowBlank: false,
                                editable: false,
                                anchor: '95%'
                            },
                            {
                                xtype: 'checkbox',
                                fieldLabel: 'Activado:',
                                name: 'estado',
                                labelAlign: 'left',
                                style: {
                                    marginTop: '10px'
                                },
                                checked: true
                            }
                        ]
                    },
                    {
                        defaults: {width: '50%'},
                        columnWidth: .5,
                        border: false,
                        bodyStyle: 'padding:5px',
                        items: [
                            {
                                xtype: 'combobox',
                                fieldLabel: 'Profesor',
                                name: 'idprofesor',
                                editable: false,
                                anchor: '95%',
                                forceSelection: true,
                                typeAhead: true,
                                triggerAction: 'all',
                                selectOnFocus: true,
                                emptyText: "Seleccione",
                                store: stcmbProf,
                                queryMode: 'local',
                                displayField: 'nombre',
                                valueField: 'idprofesor'
                            },
                            {
                                xtype: 'textfield',
                                name: "maximas_falta",
                                fieldLabel: 'Máximas faltas',
                                allowBlank: false,
                                anchor: '95%',
                                editable: false
                            }
                        ]
                    }
                ]
            },
            gridAddHor
        ]
    });
// stgridHorario.load();

    var panel = new Ext.Panel({
        //  title: 'gesthorarioxperiodo',
        id: 'pepe',
        layout: 'border',
        renderTo: 'panel',
        items: [grid, arbolPeriodos]

    });
    var vpGestSistema = Ext.create('Ext.Viewport', {layout: 'fit', items: panel});
    var btnEliminarHorPeriodo = Ext.create('Ext.Button', {
        id: 'btnEliminarHorPeriodo',
        text: '<b>' + perfil.etiquetas.lbBtnEliminar + '</b>',
        disabled: true,
        icon: perfil.dirImg + 'eliminar.png',
        iconCls: 'btn',
        handler: function () {
            EliminarLocal();
        }
    });

    var txtBuscarHorPeriodo = new Ext.form.TextField({
        fieldLabel: 'Descripción',
        labelWidth: 80,
        anchor: '95%',
        id: 'txtBuscarHorPeriodo',
        enableKeyEvents: true
    });


    function showWin(opcion) {
        switch (opcion) {
            case 'add':
            {
                if (!winAdd) {
                    winAdd = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitI,
                        closeAction: 'hide',
                        width: 380,
                        height: 360,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                tabIndex: 13,
                                handler: function () {
                                    winAdd.hide();
                                }
                            },
                            {
                                text: 'Aplicar',
                                icon: perfil.dirImg + 'aplicar.png',
                                tabIndex: 12,
                                handler: function () {
                                    adicionarHorario('apl');
                                }
                            },
                            {
                                text: 'Adicionar',
                                icon: perfil.dirImg + 'aceptar.png',
                                tabIndex: 11,
                                handler: function () {
                                    adicionarHorario('aceptar');
                                    //   winAdd.hide();
                                }
                            }
                        ]
                    });
                }
                formHorXPer.getForm().reset();
                gridAddHor.setVisible(true);
                gridAddHor.setHeight(160)
                stgridAddHorario.load({params: {idperiodo: idperiodo}});
                winAdd.add(formHorXPer);
                winAdd.doLayout();
                winAdd.show();
            }
                break;
            case 'mod':
            {
                if (!winMod)
                    winMod = Ext.create('Ext.Window', {
                        title: perfil.etiquetas.lbTitVentanaTitII,
                        closeAction: 'hide',
                        width: 380,
                        height: 200,
                        constrain: true,
                        modal: true,
                        layout: 'fit',
                        buttons: [
                            {
                                text: 'Cancelar',
                                icon: perfil.dirImg + 'cancelar.png',
                                handler: function () {
                                    winMod.hide();
                                }
                            },
                            {
                                text: 'Aceptar',
                                icon: perfil.dirImg + 'aceptar.png',
                                handler: function () {
                                    modificarHorario();
                                    winMod.hide();
                                }
                            }
                        ]
                    });
                formHorXPer.getForm().reset();
                gridAddHor.setVisible(false);
                winMod.add(formHorXPer);
                winMod.doLayout();
                winMod.show();
                formHorXPer.getForm().loadRecord(sm.getLastSelected());
            }
                break;
        }
    }

    function adicionarHorario(apl) {
        var idhorarios = new Array();
        for (var i = 0; i < smwin.getCount(); i++) {
            idhorarios.push(smwin.getSelection()[i].raw.idhorario);
        }
        if (smwin.getCount() > 0) {
            if (formHorXPer.getForm().isValid()) {
                formHorXPer.getForm().submit({
                    url: 'insertarHorarioEnPeriodo',
                    params: {
                        idhorarios: Ext.JSON.encode(idhorarios),
                        idperiodo: idperiodo
                    },
                    waitMsg: perfil.etiquetas.lbMsgRegistrandoHorario,
                    failure: function (form, action) {
                        if (action.result.codMsg === 1) {
                            stgridHorario.load({params: {idperiodo: idperiodo}});
                            stgridAddHorario.load({params: {idperiodo: idperiodo}});
                            if (apl === "aceptar")
                                winAdd.hide();
                        }
                    }
                });
            }
        } else {
            mostrarMensaje(3, 'Seleccione un horario');
        }
    }

    function modificarHorario(apl) {
        if (formHorXPer.getForm().isValid()) {
            formHorXPer.getForm().submit({
                url: 'modificarHorarioEnPeriodo',
                params: {
                    idperiodo: idperiodo,
                    idhorario_periododocente: sm.getLastSelected().raw.idhorario_periododocente,
                    idhorario: sm.getLastSelected().raw.idhorario
                },
                waitMsg: perfil.etiquetas.lbMsgRegistrandoProfesor,
                failure: function (form, action) {
                    if (action.result.codMsg === 1) {
                        stgridHorario.load({params: {idperiodo: idperiodo}});
                        stgridAddHorario.load({params: {idperiodo: idperiodo}});
                    }
                }
            });
        }
    }

    function eliminarHorarioXPeriodo() {
        mostrarMensaje(2, perfil.etiquetas.lbMsgEliminarAsoc, elimina);

        function elimina(btnPresionado) {
            if (btnPresionado == 'ok') {
                var arrid = new Array();
                for (var i = 0; i < sm.getCount(); i++) {
                    arrid.push(sm.getSelection()[i].raw.idhorario_periododocente);
                }
                Ext.Ajax.request({
                    url: 'eliminarAsoc',
                    method: 'POST',
                    params: {
                        idhorarioxperiodo: Ext.JSON.encode(arrid)
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        if (responseData.codMsg == 1) {
                            stgridHorario.load({params: {start: 0, limit: 20, idperiodo: idperiodo}});
                            sm.clearSelections();
                            btnEliminar.disable();
                        }
                    }
                });
            }
        }
    }

}
