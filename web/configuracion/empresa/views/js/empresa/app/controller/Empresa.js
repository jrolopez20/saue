Ext.define('Empresa.controller.Empresa', {
    extend: 'Ext.app.Controller',
    views: [
        'Empresa.view.Edit',
        'InventarioPersona.view.persona.Grid',
        'InventarioPersona.view.persona.WinSearchPersona'
    ],
    stores: [
        'InventarioPersona.store.Personas'
    ],
    models: [
        'InventarioPersona.model.Persona'
    ],
    refs: [
        {ref: 'edit_empresa', selector: 'edit_empresa'},
        {ref: 'grid_persona', selector: 'grid_persona'},
        {ref: 'search_persona', selector: 'search_persona'}
    ],
    init: function () {
        var me = this;

        me.control({
            'edit_empresa': {
                render: me.onRenderForm
            },
            'edit_empresa button[action=save]': {
                click: me.save
            },
            'grid_persona': {
                itemdblclick: me.onDblClickEmpleado
            }
        });
    },

    onRenderForm: function (cmp) {
        var myMask = new Ext.LoadMask(cmp, {msg: 'Cargando datos de la empresa...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'load',
            method: 'POST',
            callback: function (options, success, response) {
                var responseData = Ext.decode(response.responseText);
                cmp.getForm().setValues(responseData);
                if (!Ext.isEmpty(responseData.logo)) {
                    cmp.down('image[name=logo]').setSrc(responseData.logo);
                }
                myMask.hide();
            }
        });
    },

    save: function (btn) {
        var me = this,
            logo = null,
            panel = btn.up('form'),
            form = panel.getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(panel, {msg: 'Guardando datos de la empresa...'});
            myMask.show();

            //Solo guarda el logo si este fue modificada
            if (panel.DEFAULT_IMAGE != panel.down('image[name=logo]').src) {
                logo = panel.down('image[name=logo]').src;
            }

            form.submit({
                params: {
                    logo: logo
                },
                submitEmptyText: false,
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    onDblClickEmpleado: function (grid, record) {
        var name = record.get('nombre') + ' ' + record.get('apellidos');
        this.getEdit_empresa().getForm().setValues({
            representante: name,
            idpersona: record.get('idpersona')
        });
        grid.up('window').close();
    }

});