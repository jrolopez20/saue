var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();
Ext.application({
    name: 'Empresa',
    enableQuickTips: true,
    paths: {
        'Empresa': '../../views/js/empresa/app',
        'InventarioPersona': '/rrhh/persona/views/js/inventariopersona/app'
    },
    controllers: ['Empresa'],
    launch: function () {
        UCID.portal.cargarEtiquetas('empresa', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'edit_empresa',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});