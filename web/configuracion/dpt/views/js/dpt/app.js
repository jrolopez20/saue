var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Dpt',
    enableQuickTips: true,
    paths: {
        'Dpt': '../../views/js/dpt/app'
    },
    controllers: ['Dpt'],
    launch: function() {
        UCID.portal.cargarEtiquetas('dpt', function() {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'west',
                        xtype: 'pais_grid',
                        margin: '5 0 5 5',
                        width: '40%'
                    },
                    {
                        region: 'center',
                        xtype: 'dpt_tree',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });

    }
});