Ext.define('Dpt.store.Pais', {
    extend: 'Ext.data.Store',
    model: 'Dpt.model.Pais',
    autoLoad: true,
    pageSize: 25,
    sorters: ['nombrepais'],
    proxy: {
        type: 'ajax',
        url: 'listDataPais',
        reader: {
            type: 'json',
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }

});