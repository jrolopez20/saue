Ext.define('Dpt.store.Dpt', {
    extend: 'Ext.data.TreeStore',
    model: 'Dpt.model.Dpt',
    folderSort: false,
    proxy: {
        type: 'ajax',
        url: 'loadDpt',
        actionMethods: {
            read: 'POST'
        }
    },
    root: {text: 'DPT'}


});