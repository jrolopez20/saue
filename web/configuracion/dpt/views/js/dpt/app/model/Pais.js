Ext.define('Dpt.model.Pais', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idpais', type: 'int'},
        {name: 'codigopais', type: 'string'},
        {name: 'siglas', type: 'string'},
        {name: 'nombrepais', type: 'string'}
    ]
});