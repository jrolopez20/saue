Ext.define('Dpt.model.Dpt', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'iddpt', type: 'int'
        },
        {
            name: 'idpais', type: 'int'
        },
        {
            name: 'codigo', type: 'string'
        },
        {
            name: 'abreviatura', type: 'string'
        },
        {
            name: 'denominacion', type: 'string'
        }
    ]
});