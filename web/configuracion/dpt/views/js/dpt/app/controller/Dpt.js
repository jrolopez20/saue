Ext.define('Dpt.controller.Dpt', {
    extend: 'Ext.app.Controller',
    views: [
        'Dpt.view.DptTree',
        'Dpt.view.DptEdit',
        'Dpt.view.PaisGrid'
    ],
    stores: [
        'Dpt.store.Dpt',
        'Dpt.store.Pais'
    ],
    models: [
        'Dpt.model.Dpt',
        'Dpt.model.Pais'
    ],
    refs: [
        {ref: 'dpt_tree', selector: 'dpt_tree'},
        {ref: 'dpt_edit', selector: 'dpt_edit'},
        {ref: 'pais_grid', selector: 'pais_grid'}
    ],
    init: function() {
        this.idpais = -1;
        var me = this;
        me.control({
            'pais_grid': {
                selectionchange: me.onPaisSelectionChange
            },
            'dpt_tree': {
                selectionchange: me.onDptSelectionChange
            },
            'dpt_tree button[action=adicionar]': {
                click: me.addDpt
            },
            'dpt_tree button[action=modificar]': {
                click: me.modDpt
            },
            'dpt_tree button[action=eliminar]': {
                click: me.delDpt
            },
            'dpt_tree button[action=reload]': {
                click: me.reloadDpt
            },
            'dpt_edit button[action=aceptar]': {
                click: me.saveDpt
            },
            'dpt_edit button[action=aplicar]': {
                click: me.saveDpt
            }
        });
        me.getDptStoreDptStore().on({
            beforeload: {fn: me.setearExtraParams, scope: this}
        });

    },
    setearExtraParams: function(store) {
        var me = this;
        if (me.idpais === -1) {
            return false;
        } else {
            var extraParams = {
                idpais: me.idpais
            };
            store.getProxy().extraParams = extraParams;
        }
    },
    onPaisSelectionChange: function(sm, arrSelectedRecord) {
        var me = this;
        me.disableButtons();
        console.log(me.getDpt_tree().getRootNode());
        if (sm.hasSelection()) {
            me.getDpt_tree().getRootNode().data.codigo = 'DPT: ' + me.getPais_grid().getSelectionModel().getLastSelected().get('nombrepais');
            me.idpais = me.getPais_grid().getSelectionModel().getLastSelected().get('idpais');
            me.getDpt_tree().getStore().load();
            me.enableButtons();
        }
    },
    onDptSelectionChange: function(sm) {
        var me = this;
        me.getDpt_tree().down('button[action=modificar]').disable();
        me.getDpt_tree().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            if (sm.getLastSelected().get('root') == false) {
                me.getDpt_tree().down('button[action=modificar]').enable();
                if (sm.getLastSelected().get('leaf') == true) {
                    me.getDpt_tree().down('button[action=eliminar]').enable();
                }
            }
        }
    },
    addDpt: function() {
        var me = this;
        var win = Ext.widget('dpt_edit');
        win.setTitle(perfil.etiquetas.ttpBtnAdd);
        win.down('button[action=aplicar]').show();
        var form = me.getDpt_edit().down('form').getForm();
        form.findField('idpais').setValue(me.getPais_grid().getSelectionModel().getLastSelected().get('idpais'));
        form.findField('codigo').enable();
        form.findField('abreviatura').enable();
        form.findField('denominacion').enable();
    },
    modDpt: function() {
        var win = Ext.widget('dpt_edit');
        win.setTitle(perfil.etiquetas.ttpBtnMod);
        win.down('button[action=aplicar]').hide();
        var form = this.getDpt_edit().down('form').getForm();
        var record = this.getDpt_tree().getSelectionModel().getLastSelected();
        form.loadRecord(record);
        form.findField('codigo').enable();
        form.findField('abreviatura').enable();
        form.findField('denominacion').enable();
    },
    delDpt: function(btn) {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelete, function(btn) {
            if (btn == 'yes') {
                var record = me.getDpt_tree().getSelectionModel().getLastSelected();

                var myMask = new Ext.LoadMask(me.getDpt_tree(), {msg: perfil.etiquetas.msgDeleting});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteDpt',
                    method: 'POST',
                    params: {
                        iddpt: record.get('iddpt')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getDpt_tree().down('button[action=modificar]').disable();
                        me.getDpt_tree().down('button[action=eliminar]').disable();
                        me.getDpt_tree().getStore().load();
                    }
                });
            }
        });
    },
    saveDpt: function(btn) {
        var me = this,
                idpadre = null;
        var form = btn.up('window').down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getDpt_edit(), {msg: perfil.etiquetas.msgGuardando});
            myMask.show();

            var selectedDpt = me.getDpt_tree().getSelectionModel().getLastSelected();
            //Caso de adicionar
            if (Ext.isEmpty(form.findField('iddpt').getValue())) {
                if (me.getDpt_tree().getSelectionModel().hasSelection()) {
                    idpadre = selectedDpt.get('iddpt');
                } else
                    idpadre = null;
            } else {
                //Caso de modificar
                if (selectedDpt.get('iddpt') != selectedDpt.get('idpadre')) {
                    idpadre = selectedDpt.get('idpadre');
                }
            }

            form.submit({
                clientValidation: true,
                url: 'updateDpt',
                params: {
                    idpadre: idpadre
                },
                success: function(response) {
                    myMask.hide();

                    if (btn.action === 'aceptar') {
                        me.getDpt_edit().close();
                        me.getDpt_tree().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    },
    reloadDpt: function(btn) {
        this.getDpt_tree().getStore().load();
    },
    disableButtons: function() {
        var me = this;
        me.getDpt_tree().down('button[action=adicionar]').disable();
        me.getDpt_tree().down('button[action=reload]').disable();
    },
    enableButtons: function() {
        var me = this;
        me.getDpt_tree().down('button[action=adicionar]').enable();
        me.getDpt_tree().down('button[action=reload]').enable();
    }

});