Ext.define('Dpt.view.DptEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.dpt_edit',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 450,
    initComponent: function() {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'saveConcepto',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%'
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'iddpt'
                },
                {
                    xtype: 'hidden',
                    name: 'idpais'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [{
                            flex: 1,
                            maskRe: /^[0-9]+$/,
                            regex: /^[0-9]+$/,
                            name: 'codigo',
                            fieldLabel: perfil.etiquetas.lbCodigo,
                            maxLength: 10,
                            allowBlank: false
                        },
                        {
                            flex: 1,
                            name: 'abreviatura',
                            fieldLabel: perfil.etiquetas.lbAbreviatura,
                            maxLength: 20,
                            allowBlank: false,
                            margins: '0 0 0 5'
                        }]
                },
                {
                    xtype: 'textarea',
                    name: 'denominacion',
                    allowBlank: false,
                    fieldLabel: perfil.etiquetas.lbDenominacion,
                    maxLength: 255
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});