Ext.define('Dpt.view.PaisGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pais_grid',
    store: 'Dpt.store.Pais',
//    plugins: [{
//            ptype: 'gridfilter'
//        }],
    initComponent: function() {
        var me = this;
        me.title = perfil.etiquetas.lbListadoPaises;
        me.selModel = Ext.create('Ext.selection.RowModel');

        me.columns = [
            {
                hidden: true,
                hideable: false,
                dataIndex: 'idpais',
                width: 100
            },
            {
                width: 100,
                header: perfil.etiquetas.lbCodigo,
                dataIndex: 'codigopais'
            },
            {
                width: 100,
                header: perfil.etiquetas.lbSigla,
                dataIndex: 'siglas'
            },
            {
                flex: 1,
                header: perfil.etiquetas.lbNombre,
                dataIndex: 'nombrepais'
            }
        ];


        me.tbar = [
            '->',
            {
                xtype: 'trigger',
                emptyText: 'Buscar...',
                width: 250,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function() {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function(f, e) {
                        var me = this,
                                grid = me.up('gridpanel');
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().filterBy(function(record) {
                                if (record.get('nombrepais').toLowerCase() === f.getValue().toLowerCase())
                                    return true;
                                if (f.getValue() === '')
                                    return true;
                            }, this);
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    },
                    change: function(field, newVal) {
                        var grid = field.up('gridpanel');
                        grid.getStore().filterBy(function(record) {
                            if (record.get('nombrepais').toLowerCase() === newVal.toLowerCase())
                                return true;
                            if (newVal === '')
                                return true;
                        }, this);
                        if (newVal) {
                            this.triggerCell.item(0).setDisplayed(true);
                        } else {
                            this.triggerCell.item(0).setDisplayed(false);
                        }
                        this.updateLayout();
                    }, buffer: 250
                },
                onTrigger1Click: function() {
                    this.reset();
                    this.triggerCell.item(0).setDisplayed(false);
                    this.updateLayout();
                    this.focus();
                },
                onTrigger2Click: function() {
                    var newVal = this.getValue(),
                            grid = this.up('gridpanel');

                    grid.filter(newVal, 'nombrepais');
                    if (newVal) {
                        this.triggerCell.item(0).setDisplayed(true);
                    } else {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                    this.updateLayout();
                }
            }
        ];

        this.callParent(arguments);
    }
});