Ext.define('Dpt.view.DptTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.dpt_tree',
    store: 'Dpt.store.Dpt',
    useArrows: true,
    allowDeselect: true,
    plugins: [{
            ptype: 'treefilter',
            allowParentFolders: true
        }],
    initComponent: function() {
        var me = this;

        me.columns = [
            {
                xtype: 'treecolumn',
                text: perfil.etiquetas.lbCodigo,
                width: 160,
                sortable: true,
                dataIndex: 'codigo'
            }, {
                text: perfil.etiquetas.lbAbreviatura,
                flex: 1,
                dataIndex: 'abreviatura',
                sortable: true
            }, {
                text: perfil.etiquetas.lbDenominacion,
                flex: 2,
                dataIndex: 'denominacion',
                sortable: true
            }
        ];

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdd,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnMod,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnDel,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            },
            {
                text: perfil.etiquetas.lbBtnReload,
                tooltip: perfil.etiquetas.ttpBtnReload,
                icon: perfil.dirImg + 'actualizar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'reload'
            }, '->',
            {
                xtype: 'trigger',
                emptyText: 'Buscar...',
                width: 250,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function() {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function(f, e) {
                        var me = this,
                                tree = me.up('treepanel');
                        if (e.getKey() == e.ENTER) {
                            tree.getStore().load({
                                params: {
                                    filter: f.getValue()
                                }
                            });
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    },
                    change: function(field, newVal) {
                        var tree = field.up('treepanel');
                        tree.filter(newVal, 'denominacion');
                        if (newVal) {
                            this.triggerCell.item(0).setDisplayed(true);
                        } else {
                            this.triggerCell.item(0).setDisplayed(false);
                        }
                        this.updateLayout();
                    }, buffer: 250
                },
                onTrigger1Click: function() {
                    this.reset();
                    this.triggerCell.item(0).setDisplayed(false);
                    this.updateLayout();
                    this.focus();
                },
                onTrigger2Click: function() {
                    var newVal = this.getValue(),
                            tree = this.up('treepanel');

                    tree.filter(newVal, 'denominacion');
                    if (newVal) {
                        this.triggerCell.item(0).setDisplayed(true);
                    } else {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                    this.updateLayout();
                }
            }
        ];
        this.callParent(arguments);
    }
});