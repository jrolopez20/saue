var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'TerminosDePago',

    enableQuickTips: true,

    paths: {
        'TerminosDePago': '../../views/js/gesterminosdepago/app'
    },

    controllers: ['TerminosDePago'],

    launch: function () {
        UCID.portal.cargarEtiquetas('gesterminosdepago', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'terminos_grid',
                        region: 'center',
                        margin: '5 5 0 5'
                    }
                ]
            });
        });

    }
});