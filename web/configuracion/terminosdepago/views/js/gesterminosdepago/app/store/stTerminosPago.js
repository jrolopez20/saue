Ext.define('TerminosDePago.store.stTerminosPago', {
    extend: 'Ext.data.Store',
    model: 'TerminosDePago.model.TerminosPago',

    storeId: 'idStTerminosPago',
    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: 'load',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    }
});