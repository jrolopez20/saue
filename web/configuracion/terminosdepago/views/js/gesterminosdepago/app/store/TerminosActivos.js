Ext.define('TerminosDePago.store.TerminosActivos', {
    extend: 'Ext.data.Store',
    model: 'TerminosDePago.model.TerminosPago',

    storeId: 'idStTerminosActivos',
    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: 'loadSelectables',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    }
});