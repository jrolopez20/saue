Ext.define('TerminosDePago.store.UMPlazo', {
    extend: 'Ext.data.Store',
    model: 'TerminosDePago.model.UMPlazo',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'getUMedidaPlazo',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    }
});