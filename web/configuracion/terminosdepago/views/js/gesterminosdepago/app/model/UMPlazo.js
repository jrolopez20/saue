Ext.define('TerminosDePago.model.UMPlazo', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idumplazo', type: 'int'},
        {name: 'denominacion', type: 'string'}
    ]
});