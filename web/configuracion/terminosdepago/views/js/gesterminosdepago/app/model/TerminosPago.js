Ext.define('TerminosDePago.model.TerminosPago', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idterminodepago', type: 'int'},
        {name: 'nombre', type: 'string'},
        {name: 'aplazos', type: 'int'},
        {name: 'diafijo', type: 'int'},
        {name: 'plazo', type: 'int'},
        {name: 'fechavencimiento', type: 'date'},
        {name: 'descventa', type: 'int'},
        {name: 'dpp', type: 'int'},
        {name: 'idumplazo', type: 'int'},
        {name: 'umplazo', type: 'string'},
        {name: 'idestructura', type: 'int'},
        {name: 'predeterminado', type: 'int'},
        {name: 'aclaraciones', type: 'string'},
        //{name: 'hasaclaraciones', type: 'boolean'},

        //estos son los que se obtienen de otras tablas
        {name: 'cuotas'},
        {name: 'moras'},
        {name: 'descuentosxpp'},
        {name: 'descuentosventa'}
    ]
});