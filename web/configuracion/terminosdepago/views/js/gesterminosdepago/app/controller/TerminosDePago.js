Ext.define('TerminosDePago.controller.TerminosDePago', {
    extend: 'Ext.app.Controller',
    views: [
        'TerminosDePago.view.Grid',
        'TerminosDePago.view.GridCuotas',
        'TerminosDePago.view.GridDescVenta',
        'TerminosDePago.view.GridDpp',
        'TerminosDePago.view.GridMora',
        'TerminosDePago.view.Edit'
    ],
    stores: [
        'TerminosDePago.store.stTerminosPago',
        'TerminosDePago.store.TerminosActivos',
        'TerminosDePago.store.UMPlazo'
    ],
    models: [
        'TerminosDePago.model.TerminosPago',
        'TerminosDePago.model.UMPlazo',
    ],
    refs: [
        {ref: 'terminos_grid', selector: 'terminos_grid'},
        {ref: 'grid_cuotas', selector: 'grid_cuotas'},
        {ref: 'grid_descventa', selector: 'grid_descventa'},
        {ref: 'grid_dpp', selector: 'grid_dpp'},
        {ref: 'grid_mora', selector: 'grid_mora'},
        {ref: 'edit_termino', selector: 'edit_termino'}
    ],
    init: function () {
        Ext.Date.dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        var me = this;
        this.listen({
            component: {
                'terminos_grid': {
                    selectionchange: me.onGridSelectionChange
                },
                'terminos_grid': {
                    expandbody: me.onGridExpandBody
                },
                'edit_termino': {
                    close: me.reloadTerms
                },
                'edit_termino radiofield[id=radioDiafijo]': {
                    change: me.onChangeDF
                },
                'edit_termino radiofield[id=radioAplazos]': {
                    change: me.toogleTabCuotas
                },
                'edit_termino radiofield[id=radioOtro]': {
                    change: me.onChangeOtro
                },
                'edit_termino checkboxfield[id=checkDescventa]': {
                    change: me.toogleTabDescV
                },
                'edit_termino checkboxfield[id=checkDpp]': {
                    change: me.toogleTabDpp
                },
                'edit_termino button[action=aceptar]': {
                    click: me.saveTermino
                },
                'edit_termino button[action=aplicar]': {
                    click: me.saveTermino
                },
                'terminos_grid button[action=adicionar]': {
                    click: me.addTermino
                },
                'terminos_grid button[action=modificar]': {
                    click: me.modTermino
                },
                'terminos_grid button[action=eliminar]': {
                    click: me.delTermino
                },
                'terminos_grid button[action=predeterminar]': {
                    click: me.setDefault
                },
                'grid_mora button[action=adicionar]': {
                    click: me.onAddMora
                },
                'grid_dpp button[action=adicionar]': {
                    click: me.onAddDpp
                },
                'grid_descventa button[action=adicionar]': {
                    click: me.onAddDescV
                },
                'grid_cuotas button[action=adicionar]': {
                    click: me.onAddCuotas
                },
                'grid_cuotas': {
                    renderingterms: me.onRenderingCuotaTerms
                }
            },
            store: {
                '#stgridmora': {
                    update: me.updateAclaraciones
                },
                '#stgriddpp': {
                    update: me.updateAclaraciones
                },
                '#stgriddescv': {
                    update: me.updateAclaraciones
                },
                '#stgridcuotas': {
                    update: me.updateAclaraciones
                }
            }
        });
    },

    updateAclaraciones: function (store, record, operation, eOpts) {
        var me = this;
        if (operation == Ext.data.Model.EDIT) {
            switch (store.storeId) {
                case 'stgridmora':
                    record.set('aclaraciones', perfil.etiquetas.msgAclaracionMora.replace('<%>', record.get('porciento')).replace('<#>', record.get('dias')));
                    break;
                case 'stgriddpp':
                    var aclaracion = undefined;
                    if (record.get('porciento') && Ext.isEmpty(record.get('dias'))) {
                        aclaracion = perfil.etiquetas.msgAclaracionDppCase1.replace('<%>', record.get('porciento'));
                    } else if (record.get('porciento') && record.get('dias')) {
                        aclaracion = perfil.etiquetas.msgAclaracionDppCase2.replace('<%>', record.get('porciento')).replace('<#days#>', record.get('dias'));
                    }
                    record.set('aclaraciones', aclaracion);
                    break;
                case 'stgriddescv':
                    record.set('aclaraciones', perfil.etiquetas.msgAclaracionDescV.replace('<%>', record.get('porciento')).replace('<#>', record.get('min')));
                    break;
                case 'stgridcuotas':
                    //console.log(me.getGrid_cuotas().down('gridcolumn').down('combobox'));
                    var cbxStore = me.getStore('TerminosDePago.store.TerminosActivos');
                    var recordaux = cbxStore.findRecord('idterminodepago', record.get('idterminoaplicable'));
                    record.set('aclaraciones', perfil.etiquetas.msgAclaracionCuotas.replace('<%>', record.get('porciento')).replace('<#term#>', recordaux.get('nombre')));
                    break;
            }
        }
    },

    /* esto es para renderizar los contactos */
    onGridExpandBody: function (rowNode, record, expanRow, eOpts) {
        var me = this,
            store = me.getTerminos_grid().getStore();

        if (!record.get('hasaclaraciones')) {
            record.set({hasaclaraciones: true});

            var aclaraciones = '<ol><li>';
            aclaraciones += (record.get('aplazos') == 1) ?
            perfil.etiquetas.msgFormaPagoAplazo.replace('<#>', record.get('cuotas').length) + '</li>' :
            perfil.etiquetas.msgFormaPagoOtro.replace('<#>', record.get('plazo')).replace('<#type#>', record.get('umplazo')) + '</li>';

            var dpp = record.get('descuentosxpp');
            if (dpp.length) {
                dpp.every(function (item) {
                    if (Ext.isEmpty(item.dias)) {
                        aclaraciones += '<li>' + perfil.etiquetas.msgAclaracionDppIndex1.replace('<#>', item.porciento) + '</li>';
                    } else {
                        aclaraciones += '<li>' + perfil.etiquetas.msgAclaracionDppIndex2.replace('<#>', item.porciento).replace('<#days#>', item.dias) + '</li>';
                    }
                    return true; // seguir con el siguiente elemento
                });
            }

            var moras = record.get('moras');
            if (moras.length) {
                moras.every(function (item) {
                    aclaraciones += '<li>' + perfil.etiquetas.msgAclaracionMoraIndex.replace('<#>', item.porciento).replace('<#days#>', item.dias) + '</li>';
                    return true; // seguir con el siguiente elemento
                });
            }

            var descv = record.get('descuentosventa');
            if (descv.length) {
                descv.every(function (item) {
                    aclaraciones += '<li>' + perfil.etiquetas.msgAclaracionDescVIndex.replace('<%>', item.porciento).replace('<#>', item.min) + '</li>';
                    return true; // seguir con el siguiente elemento
                });
            }

            var cuotas = record.get('cuotas');
            if (cuotas.length) {
                cuotas.every(function (item) {
                    var term = store.findRecord('idterminodepago', item.idterminoaplicable);
                    if(term){
                        var msg = perfil.etiquetas.msgAclaracionCuotas.replace('<%>', item.porciento).replace('<#term#>', term.get('nombre'));
                        aclaraciones += '<li>' + msg + '</li>';
                    }
                    return true; // seguir con el siguiente elemento
                });
            }

            var panel = Ext.create('Ext.panel.Panel', {
                title: 'Aclaraciones',
                html: aclaraciones + '</ol>'
            });

            if (!panel.rendered) {
                var a = Ext.fly(rowNode).getHeight();
                panel.render('detail' + record.get('idterminodepago'));
                Ext.fly(rowNode).setHeight(a + panel.getHeight()+ 25);
            }
        }
    },

    onChangeDF: function (radio, newValue, oldValue, eOpts) {
        var fieldset = radio.up('fieldset'), value = radio.getValue();
        fieldset.down('datefield[name=diafijo]').setDisabled(!value);

        if (!value) {
            fieldset.down('datefield[name=diafijo]').reset();
            if (!Ext.isIE6) {
                fieldset.down('datefield[name=diafijo]').el.animate({opacity: value ? 0.3 : 1});
            }
        } else {
            fieldset.down('datefield[name=diafijo]').setValue(new Date());
        }
    },

    onChangeOtro: function (radio, newValue, oldValue, eOpts) {
        var fieldset = radio.up('fieldset'), value = radio.getValue();

        fieldset.down('numberfield[name=plazo]').setDisabled(!value);
        fieldset.down('combobox[name=idumplazo]').setDisabled(!value);

        if (!value) {
            fieldset.down('numberfield[name=plazo]').reset();
            fieldset.down('combobox[name=idumplazo]').reset();

            if (!Ext.isIE6) {
                fieldset.down('numberfield[name=plazo]').el.animate({opacity: value ? 0.3 : 1});
                fieldset.down('combobox[name=idumplazo]').el.animate({opacity: value ? 0.3 : 1});
            }
        } else {
            radio.up('window').down('checkboxfield[id=checkDpp]').setValue(true);

            // se activa el fielset de descuentos
            radio.up('window').down('fieldset[itemId=chDescuentos]').setDisabled(false);
        }
    },

    toogleTabDpp: function (checkbox, newValue, oldValue, eOpts) {
        var me = this,
            tabpanel = checkbox.up('window').down('tabpanel'),
            value = checkbox.getValue();

        var activetab = tabpanel.getActiveTab(),
            tabdpp = tabpanel.getComponent('tabDpp');

        // resetar el store del grid de dpp
        me.resetGrid(tabdpp.down('grid_dpp').getStore());
        tabdpp.tab.hide();

        if (!value) {
            if (activetab.getItemId() == tabdpp.getItemId()) {
                tabpanel.setActiveTab(0); // se activa el tab de mora
            }
        } else {
            tabdpp.tab.show();
        }
    },

    toogleTabDescV: function (checkbox, newValue, oldValue, eOpts) {
        var me = this,
            tabpanel = checkbox.up('window').down('tabpanel'),
            value = checkbox.getValue();

        var activetab = tabpanel.getActiveTab(),
            tabdpp = tabpanel.getComponent('tabDescVenta');

        // resetar el store del grid de descuentos en ventas
        me.resetGrid(tabdpp.down('grid_descventa').getStore());
        tabdpp.tab.hide();

        if (!value) {
            if (activetab.getItemId() == tabdpp.getItemId()) {
                tabpanel.setActiveTab(0); // se activa el tab de mora
            }
        } else {
            tabdpp.tab.show();
        }
    },

    toogleTabCuotas: function (radio, newValue, oldValue, eOpts) {
        var me = this,
            tabpanel = radio.up('window').down('tabpanel'),
            value = radio.getValue();

        var activetab = tabpanel.getActiveTab(),
            tabcuotas = tabpanel.getComponent('tabCuotas'),
            tabmora = tabpanel.getComponent('tabMora');

        if (!value) {
            tabmora.tab.show(); // se muestra el tab de mora

            if (activetab.getItemId() == tabcuotas.getItemId()) {
                tabpanel.setActiveTab(0); // se activa el tab de mora
            }
            // resetar el store del grid de descuentos en ventas
            me.resetGrid(tabcuotas.down('grid_cuotas').getStore());
            tabcuotas.tab.hide();
        } else {
            // resetar el store del grid de mora
            me.resetGrid(tabmora.down('grid_mora').getStore());
            tabmora.tab.hide();

            // se dispara los eventos que ocultan los tab de descuentos
            radio.up('window').down('checkboxfield[id=checkDescventa]').setValue(false);
            radio.up('window').down('checkboxfield[id=checkDpp]').setValue(false);

            // se desactiva el fielset de descuentos
            radio.up('window').down('fieldset[itemId=chDescuentos]').setDisabled(true);

            me.resetGrid(tabcuotas.down('grid_cuotas').getStore());
            tabcuotas.tab.show();
            tabpanel.setActiveTab(3);
        }
    },

    onGridSelectionChange: function (sm) {
        var me = this;
        if (sm.hasSelection()) {
            me.getTerminos_grid().down('button[action=modificar]').enable();
            me.getTerminos_grid().down('button[action=eliminar]').enable();
        } else {
            me.getTerminos_grid().down('button[action=modificar]').disable();
            me.getTerminos_grid().down('button[action=eliminar]').disable();
        }
    },

    resetGrid: function (store) {
        store.removeAll();
    },

    onAddMora: function (btn) {
        var grid = btn.up('grid'),
            store = grid.getStore();
        var roweditor = grid.getPlugin('myRowEditor');
        roweditor.cancelEdit();
        var records = store.insert(store.getCount(), {
            porciento: '',
            dias: '',
            aclaraciones: ''
        });
        roweditor.startEdit(records[0], 0);
        roweditor.on({
            cancelEdit: function (rowEditing, context) {
                // Canceling editing of a locally added, unsaved record: remove it
                if (context.record.phantom) {
                    store.remove(context.record);
                }
            },
            //edit: function (editor, context, eOpts) {
            //    if (Ext.isNumeric(context.newValues.dias)) {
            //        grid.down('gridcolumn[dataIndex=dias]').getEditor().setMinValue(context.newValues.dias + 1);
            //    }
            //},
        });

        /* ESTO ES SOLO PARA EL CELLPLUGIN */
        //store.insert(store.getCount(), {
        //    porciento: '',
        //    dias: '',
        //    aclaraciones: ''
        //});
        //grid.getPlugin('cellplugin').startEditByPosition({row: store.getCount() - 1, column: 1});
    },

    onAddDpp: function (btn) {
        var grid = btn.up('grid'),
            store = grid.getStore();

        var roweditor = grid.getPlugin('myRowEditor');
        roweditor.cancelEdit();
        var records = store.insert(store.getCount(), {
            porciento: '0.01',
            dias: '',
            aclaraciones: ''
        });
        roweditor.startEdit(records[0], 0);
        roweditor.on({
            cancelEdit: function (rowEditing, context) {
                // Canceling editing of a locally added, unsaved record: remove it
                if (context.record.phantom) {
                    grid.getStore().remove(context.record);
                }
            }
            //edit: function (editor, context, eOpts) {
            //    if (Ext.isNumeric(context.newValues.dias)) {
            //        grid.down('gridcolumn[dataIndex=dias]').getEditor().setMinValue(context.newValues.dias + 1);
            //        //grid.down('gridcolumn[dataIndex=dias]').getEditor().allowBlank = false;
            //    }
            //}
        });
    },

    onAddDescV: function (btn) {
        var grid = btn.up('grid'),
            store = grid.getStore();

        var roweditor = grid.getPlugin('myRowEditor');
        roweditor.cancelEdit();
        var records = store.insert(store.getCount(), {
            porciento: '0.01',
            min: '',
            aclaraciones: ''
        });
        roweditor.startEdit(records[0], 0);
        roweditor.on({
            cancelEdit: function (rowEditing, context) {
                // Canceling editing of a locally added, unsaved record: remove it
                if (context.record.phantom) {
                    grid.getStore().remove(context.record);
                }
            }
            //edit: function (editor, context, eOpts) {
            //    if (Ext.isNumeric(context.newValues.min)) {
            //        grid.down('gridcolumn[dataIndex=min]').getEditor().setMinValue(context.newValues.min + 1);
            //    }
            //}
        });
    },

    onAddCuotas: function (btn) {
        var grid = btn.up('grid'),
            store = grid.getStore();

        var roweditor = grid.getPlugin('myRowEditor');
        roweditor.cancelEdit();
        var records = store.insert(store.getCount(), {
            idterminoaplicable: '',
            porciento: '',
            aclaraciones: ''
        });
        roweditor.startEdit(records[0], 0);
        roweditor.on({
            cancelEdit: function (rowEditing, context) {
                // Canceling editing of a locally added, unsaved record: remove it
                if (context.record.phantom) {
                    grid.getStore().remove(context.record);
                }
            }
            //edit: function (editor, context, eOpts) {
            //    var acumulado = store.sum('porciento');
            //    if (Ext.isNumeric(acumulado)) {
            //        grid.down('gridcolumn[dataIndex=porciento]').getEditor().setMaxValue(100 - acumulado);
            //    }
            //}
        });
    },

    delTermino: function (btn) {
        var me = this,
            sm = this.getTerminos_grid().getSelectionModel();
        if (sm.hasSelection()) {
            Ext.Msg.confirm(perfil.etiquetas.lbConfirm, perfil.etiquetas.msgConfirmDel,
                function (btn) {
                    if (btn === 'yes') {
                        var records = sm.getSelection();
                        var ids = [];
                        records.every(function (record) {
                            ids.push(record.get('idterminodepago'));
                        });
                        me.delete(ids, me);
                    }
                });
            /* Poner un mensaje de cnfirmacion */
        }
    },

    setDefault: function (btn) {
        var me = this,
            sm = this.getTerminos_grid().getSelectionModel();
        if (sm.hasSelection()) {
            var record = sm.getLastSelected();
            Ext.Msg.confirm(perfil.etiquetas.lbConfirm, perfil.etiquetas.msgConfirmDef.replace('<#name#>', record.get('nombre')),
                function (btn) {
                    if (btn === 'yes') {
                        me.setState(record.get('idterminodepago'));
                    }
                });
        }
    },

    setState: function (id) {
        var me = this,
            grid = me.getTerminos_grid();
        ;
        var myMask = new Ext.LoadMask(grid, {msg: 'Espere por favor ...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'setDefault',
            method: 'POST',
            params: {
                id: id
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                if (responseData.errors && responseData.errors.length > 0) {
                    alert('Construir el mensaje de error');
                }
                grid.getStore().reload();
            }
        });
    },

    delete: function (ids, me) {
        var grid = me.getTerminos_grid();
        var myMask = new Ext.LoadMask(grid, {msg: 'Espere por favor ...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'delete',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                if (responseData.errors && responseData.errors.length > 0) {
                    alert('Construir el mensaje de error');
                }

                grid.getStore().reload();

            }
        });
    },

    addTermino: function (btn) {
        var win = Ext.widget('edit_termino');
        win.setTitle(perfil.etiquetas.lbWinAddTermino);
        win.down('button[action=aplicar]').show();
    },

    modTermino: function (btn) {
        if (this.getTerminos_grid().getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_termino');
            win.setTitle(perfil.etiquetas.lbWinModTermino);
            win.down('button[action=aplicar]').hide();
            var form = win.down('form');
            var record = this.getTerminos_grid().getSelectionModel().getLastSelected();
            form.loadRecord(record);

            if(record.get('aplazos')){
                win.down('form').down('radiofield[id=radioAplazos]').setValue(true);
            }else{
                win.down('form').down('radiofield[id=radioOtro]').setValue(true);
            }

            /* seteando los valores a los grid */
            var moras = record.get('moras'),
                dpp = record.get('descuentosxpp'),
                descv = record.get('descuentosventa'),
                cuotas = record.get('cuotas');

            if(moras.length){
                win.down('grid_mora').getStore().loadRawData(moras);
            }
            if(dpp.length){
                win.down('grid_dpp').getStore().loadRawData(dpp);
            }
            if(descv.length){
                win.down('grid_descventa').getStore().loadRawData(descv);
            }
            if(cuotas.length){
                win.down('grid_cuotas').getStore().loadRawData(cuotas);
            }
        }
    },

    saveTermino: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm(),
            flagmodify = !Ext.isEmpty(form.findField('idterminodepago').getValue());
            //viewAdd = me.getEdit_termino();


        if (form.isValid()) {
            var tabpanel = win.down('tabpanel'),
                tabMora = tabpanel.getComponent('tabMora'),
                tabDpp = tabpanel.getComponent('tabDpp'),
                tabDescV = tabpanel.getComponent('tabDescVenta'),
                tabCuotas = tabpanel.getComponent('tabCuotas'),
                mora = {},
                dpp = {},
                descv = {},
                cuotas = {};

            /* Tomando los valores agregados o modificados en cada grid */
            if (!tabMora.tab.hidden) {
                var recordsmora = tabMora.down('grid_mora').getStore().getModifiedRecords(),
                    modifiedMora = new Array();
                Ext.each(recordsmora, function (record) {
                    modifiedMora.push(record.getData());
                });

                if (!modifiedMora.length && !flagmodify) {
                    mostrarMensaje(3, perfil.etiquetas.msgEmptyMora);
                    return;
                }
                mora.modified = modifiedMora;
            }

            if (!tabDpp.tab.hidden) {
                var recordsdpp = tabDpp.down('grid_dpp').getStore().getModifiedRecords(),
                    modifiedDpp = new Array();
                Ext.each(recordsdpp, function (record) {
                    modifiedDpp.push(record.getData());
                });
                if (!modifiedDpp.length && !flagmodify) {
                    mostrarMensaje(3, perfil.etiquetas.msgEmptyDpp);
                    return;
                }
                dpp.modified = modifiedDpp;
            }

            if (!tabDescV.tab.hidden) {
                var recordsdescV = tabDescV.down('grid_descventa').getStore().getModifiedRecords(),
                    modifiedDescV = new Array();
                Ext.each(recordsdescV, function (record) {
                    modifiedDescV.push(record.getData());
                });

                if (!modifiedDescV.length && !flagmodify) {
                    mostrarMensaje(3, perfil.etiquetas.msgEmptyDV);
                    return;
                }
                descv.modified = modifiedDescV;
            }

            if (!tabCuotas.tab.hidden) {
                var recordscuotas = tabCuotas.down('grid_cuotas').getStore().getModifiedRecords(),
                    modifiedCuotas = new Array();
                Ext.each(recordscuotas, function (record) {
                    modifiedCuotas.push(record.getData());
                });

                if (!modifiedCuotas.length && !flagmodify) {
                    mostrarMensaje(3, perfil.etiquetas.msgEmptyCuotas);
                    return;
                }
                cuotas.modified = modifiedCuotas;
            }

            //CUANDO SE VA A MODIFICAR
            if (flagmodify) {
                if (!tabMora.tab.hidden) {
                    mora.deleted = new Array();
                    Ext.each(tabMora.down('grid_mora').getStore().getRemovedRecords(), function (record) {
                        mora.deleted.push(record.get('idmora'));
                    });
                    if (!mora.deleted.length) {
                        delete mora.deleted;
                    }
                }

                if (!tabDpp.tab.hidden) {
                    dpp.deleted = new Array();
                    Ext.each(tabDpp.down('grid_dpp').getStore().getRemovedRecords(), function (record) {
                        dpp.deleted.push(record.get('iddpp'));
                    });
                    if (!dpp.deleted.length) {
                        delete dpp.deleted;
                    }
                }

                if (!tabDescV.tab.hidden) {
                    descv.deleted = new Array();
                    Ext.each(tabDescV.down('grid_descventa').getStore().getRemovedRecords(), function (record) {
                        descv.deleted.push(record.get('iddescuentosventa'));
                    });
                    if (!descv.deleted.length) {
                        delete descv.deleted;
                    }
                }

                if (!tabCuotas.tab.hidden) {
                    cuotas.deleted = new Array();
                    Ext.each(tabCuotas.down('grid_cuotas').getStore().getRemovedRecords(), function (record) {
                        cuotas.deleted.push(record.get('idcuota'));
                    });
                    if (!cuotas.deleted.length) {
                        delete cuotas.deleted;
                    }
                }
            }

            var extraParams = {};

            if (Object.keys(mora).length) {
                extraParams.arraymora = Ext.encode(mora);
            }
            if (Object.keys(dpp).length) {
                extraParams.arraydpp = Ext.encode(dpp);
            }
            if (Object.keys(cuotas).length) {
                extraParams.arraycuotas = Ext.encode(cuotas);
            }
            if (Object.keys(descv).length) {
                extraParams.arraydescv = Ext.encode(descv);
            }

            var myMask = new Ext.LoadMask(win, {msg: 'Guardando ...'});
            myMask.show();

            form.submit({
                submitEmptyText: false,
                params: extraParams,
                success: function (response, action) {
                    // En action.result viene el objeto que se devuelve en el controller
                    // AKI PONER UN MECANISMO PA MOSTRAR LOS ERRORES OCURRIDOS
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getTerminos_grid().getStore().load();
                    }
                    else {
                        me.resetEditors();
                        form.reset();
                        me.resetGrid(tabMora.down('grid_mora').getStore());
                        me.resetGrid(tabDpp.down('grid_dpp').getStore());
                        me.resetGrid(tabDescV.down('grid_descventa').getStore());
                        me.resetGrid(tabCuotas.down('grid_cuotas').getStore());
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    resetEditors: function () {
        var me = this,
            tabpanel = this.getEdit_termino().down('tabpanel'),
            tabMora = tabpanel.getComponent('tabMora'),
            tabDpp = tabpanel.getComponent('tabDpp'),
            tabDescV = tabpanel.getComponent('tabDescVenta'),
            tabCuotas = tabpanel.getComponent('tabCuotas');

        if (!tabMora.tab.hidden) {
            me.getGrid_mora().down('gridcolumn[dataIndex=dias]').getEditor().setMinValue(1);
        }
        if (!tabDpp.tab.hidden) {
            me.getGrid_dpp().down('gridcolumn[dataIndex=dias]').getEditor().setMinValue(1);
        }
        if (!tabDescV.tab.hidden) {
            me.getGrid_descventa().down('gridcolumn[dataIndex=min]').getEditor().setMinValue(1);
        }
        if (!tabCuotas.tab.hidden) {
            me.getGrid_cuotas().down('gridcolumn[dataIndex=porciento]').getEditor().setMaxValue(100);
        }
    },

    reloadTerms: function (panel, eOpts) {
        this.getTerminos_grid().getStore().reload();
    },

    showMsg: function (msg, title, icon, callback, buttons) {
        var me = this;
        Ext.Msg.show({
            title: (title) ? title : 'Información',
            msg: msg,
            buttons: buttons ? buttons : Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            multiline: true,
            fn: (callback) ? callback : Ext.emptyFn,
            //animateTarget: 'addAddressBtn',
            icon: icon ? icon : Ext.MessageBox.INFO,
            scope: me
        });
    }

});