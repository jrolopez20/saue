Ext.define('TerminosDePago.view.GridCuotas', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_cuotas',

    initComponent: function () {
        var me = this;
        //this.addEvents('renderingterms');
        //me.selType = 'rowmodel';

        //me.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        //    pluginId: 'cellplugin',
        //    clicksToEdit: 2
        //});

        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            clicksToEdit: 2,
            pluginId: 'myRowEditor',
            errorSummary : false
        });

        me.store = Ext.create('Ext.data.Store', {
            storeId: 'stgridcuotas',
            fields: ['idcuota', 'idterminoaplicable', 'porciento', 'aclaraciones']
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddCuotas,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }
        ];

        me.columns = [
            {xtype: 'rownumberer'},
            {
                header: 'Id',
                dataIndex: 'idcuota',
                hidden: true,
                hideable: false
            },
            {
                header: perfil.etiquetas.lbTermino,
                dataIndex: 'idterminoaplicable',
                editor: {
                    xtype: 'combobox',
                    store: 'TerminosDePago.store.TerminosActivos',
                    allowBlank: false,
                    queryMode: 'remote',
                    displayField: 'nombre',
                    valueField: 'idterminodepago',
                    editable: false,
                    //margins: '0 0 0 5',
                    emptyText: perfil.etiquetas.emptyTextCbx || 'Seleccione...',
                    validator: function(value){
                        var store = this.up('grid_cuotas').getStore();
                        if(store.find('idterminoaplicable',this.getValue()) != -1){
                            return 'Ya existe una cuota configurada para este t&eacute;rmino.'
                        }
                        return true;
                    }

                },
                //renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                //    var text = this.down('gridcolumn[dataIndex=idterminoaplicable]').getEditor().getStore().findRecord('idterminodepago', value).get('nombre');
                //    return text;
                //},
                flex: 1
            },
            {
                header: perfil.etiquetas.lbPorciento,
                dataIndex: 'porciento',
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    emptyText: '0.0',
                    allowDecimals: true,
                    maxValue: 100,
                    minValue: 0.01,
                    step: 0.01,
                    validator: function(value){
                        var store = this.up('grid_cuotas').getStore();
                        var suma = parseFloat(store.sum('porciento'));
                        if(suma + parseFloat(value) > 100){
                            return 'El m&aacute;ximo var permitido para este campo es ' + String(100.00 - suma)
                        }
                        return true;
                    }

                },
                flex: 1
            },
            {
                header: perfil.etiquetas.lbAclaraciones,
                dataIndex: 'aclaraciones',
                flex: 4
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: perfil.etiquetas.lbBtnEliminar,
                    handler: function (grid, rowIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }
                }]
            }
        ];

        //me.plugins = [me.cellEditing];
        me.plugins = [me.rowEditing];
        this.callParent(arguments);
    }
});