Ext.define('TerminosDePago.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_termino',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 653,
    height: 450,

    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'north',
                margins: '5 5 5 5',
                url: 'save',
                fieldDefaults: {
                    labelAlign: 'top',
                    //msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 70
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idterminodepago'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                items: [
                                    {
                                        xtype: 'fieldcontainer',
                                        layout: 'hbox',
                                        defaultType: 'textfield',
                                        anchor: '100%',
                                        items: [
                                            {
                                                flex: 1,
                                                name: 'nombre',
                                                fieldLabel: perfil.etiquetas.lbDenominacion || 'Denominaci&oacute;n',
                                                maxLength: 255,
                                                //margins: '0 5 0 0',
                                                allowBlank: false
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        anchor: '100%',
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype: 'fieldset',
                                                flex: 1,
                                                title: perfil.etiquetas.lbPlazo || 'Plazo',
                                                collapsible: false,
                                                anchor: '100%',
                                                //defaults: {anchor: '100%'},
                                                layout: 'hbox',
                                                items: {
                                                    xtype: 'container',
                                                    //margins: '20 0 0 0',
                                                    anchor: '100%',
                                                    layout: 'hbox',
                                                    items: [
                                                        {
                                                            xtype: 'fieldcontainer',
                                                            layout: 'hbox',
                                                            //margin: '0 0 0 5',
                                                            //flex: 2.5,
                                                            flex: 2,
                                                            defaultType: 'radiofield',
                                                            anchor: '100%',
                                                            items: [
                                                                {
                                                                    boxLabel: perfil.etiquetas.lbOtro,
                                                                    name: 'tipopago',
                                                                    inputValue: '3',
                                                                    id: 'radioOtro',
                                                                    margins: '0 0 10 0',
                                                                    checked: true
                                                                },
                                                                {
                                                                    xtype: 'numberfield',
                                                                    width: 100,
                                                                    //flex: 1,
                                                                    name: 'plazo',
                                                                    margins: '0 0 10 5',
                                                                    maxValue: 1000,
                                                                    minValue: 1,
                                                                    value: 30
                                                                },
                                                                {
                                                                    xtype: 'combobox',
                                                                    name: 'idumplazo',
                                                                    store: 'TerminosDePago.store.UMPlazo',
                                                                    queryMode: 'local',
                                                                    allowBlank: false,
                                                                    displayField: 'denominacion',
                                                                    valueField: 'idumplazo',
                                                                    value: '30',
                                                                    flex: 1.5,
                                                                    editable: false,
                                                                    margins: '0 0 10 5',
                                                                    forceSelection: true,
                                                                    autoSelect : true,
                                                                    typeAhead : true
                                                                    //emptyText: perfil.etiquetas.emptyTextCbx || 'Seleccione...'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            boxLabel: perfil.etiquetas.lbAPlazo,
                                                            //flex: 1,
                                                            flex: 1,
                                                            name: 'tipopago',
                                                            //margins: '0 0 10 0',
                                                            margins: '0 0 10 10',
                                                            inputValue: '1',
                                                            id: 'radioAplazos'
                                                        }
                                                        //{
                                                        //    xtype: 'fieldcontainer',
                                                        //    layout: 'hbox',
                                                        //    flex: 2,
                                                        //    defaultType: 'radiofield',
                                                        //    anchor: '100%',
                                                        //    margin: '0 0 0 15',
                                                        //    items: [
                                                        //        {
                                                        //            boxLabel: perfil.etiquetas.lbDiaFijo,
                                                        //            name: 'tipopago',
                                                        //            inputValue: '2',
                                                        //            margins: '0 0 10 0',
                                                        //            id: 'radioDiafijo'
                                                        //        },
                                                        //        {
                                                        //            xtype: 'datefield',
                                                        //            margins: '0 0 10 5',
                                                        //            disabled: true,
                                                        //            name: 'diafijo',
                                                        //            minValue: new Date()
                                                        //            //value: new Date()
                                                        //        }
                                                        //    ]
                                                        //},

                                                    ]
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        //margins: '20 0 0 0',
                        anchor: '100%',
                        layout: 'hbox',
                        items: [
                            {
                                xtype:'fieldset',
                                flex: 1,
                                title:  perfil.etiquetas.lbDescuentos || 'Descuentos',
                                collapsible: false,
                                itemId: 'chDescuentos',
                                defaultType: 'checkboxfield',
                                //defaults: {anchor: '100%'},
                                layout: 'hbox',
                                items: [
                                    {
                                        boxLabel: perfil.etiquetas.lbDpp + ' (DPP)',
                                        flex: 1,
                                        name: 'dpp',
                                        margins: '0 0 10 0',
                                        inputValue: '1',
                                        id: 'checkDpp',
                                        checked: true
                                    },
                                    {
                                        boxLabel: perfil.etiquetas.tabDescVenta,
                                        flex: 1,
                                        name: 'descventa',
                                        margins: '0 0 10 0',
                                        inputValue: '1',
                                        id: 'checkDescventa',
                                        checked: false
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'tabpanel',
                region: 'center',
                margins: '0 5 5 5',
                activeTab: 0,
                items: [
                    {
                        title: perfil.etiquetas.lbMora,
                        xtype: 'panel',
                        itemId: 'tabMora',
                        id: 'tabMora',
                        layout: 'fit',
                        items: {
                            xtype: 'grid_mora',
                            margins: '0 5 5 5'
                        }
                    },
                    {
                        title: perfil.etiquetas.lbDpp,
                        xtype: 'panel',
                        itemId: 'tabDpp',
                        id: 'tabDpp',
                        layout: 'fit',
                        items: [
                            {
                                xtype: 'grid_dpp',
                                margins: '0 5 5 5'
                            }
                        ]
                    },
                    {
                        title: perfil.etiquetas.tabDescVenta,
                        xtype: 'panel',
                        itemId: 'tabDescVenta',
                        id: 'tabDescVenta',
                        layout: 'fit',
                        hidden: true,
                        items: [
                            {
                                xtype: 'grid_descventa',
                                margins: '0 5 5 5'
                            }
                        ]
                    },
                    {
                        title: perfil.etiquetas.lbCuotas,
                        xtype: 'panel',
                        itemId: 'tabCuotas',
                        id: 'tabCuotas',
                        layout: 'fit',
                        hidden: true,
                        items: [
                            {
                                xtype: 'grid_cuotas',
                                margins: '0 5 5 5'
                            }
                        ]
                    }

                ]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];


        this.callParent(arguments);
    }
});