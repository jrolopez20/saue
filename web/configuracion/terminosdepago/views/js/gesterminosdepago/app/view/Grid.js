Ext.define('TerminosDePago.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.terminos_grid',

    title: 'Listado de t&eacute;rminos de pago',
    singleSelect: this.singleSelect || true,
    store: 'TerminosDePago.store.stTerminosPago',

    initComponent: function () {
        var me = this;
        this.addEvents('expandbody');

        if (me.singleSelect) {
            me.selModel = Ext.create('Ext.selection.RowModel');
        } else {
            me.selModel = Ext.create('Ext.selection.CheckboxModel');
        }

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar || 'Adicionar',
                tooltip: perfil.etiquetas.ttpBtnAdicionar || 'Adicionar cliente o proveedor',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnModificar || 'Modificar',
                tooltip: perfil.etiquetas.ttpBtnModificar || 'Modificar t&eacute;rmino de pago',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                //disabled: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar || 'Eliminar',
                tooltip: perfil.etiquetas.ttpBtnEliminar || 'Eliminar t&eacute;rmino de pago',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                //disabled: true,
                action: 'eliminar'
            },
            {
                text: perfil.etiquetas.lbBtnPredeterminar || 'Predeterminar',
                tooltip: perfil.etiquetas.ttpBtnPredeterminar || 'Establecer t&eacute;mino de pago como predeterminado',
                icon: perfil.dirImg + 'validaryaceptar.png',
                iconCls: 'btn',
                //disabled: true,
                action: 'predeterminar'
            }
        ];


        me.columns = [
            {
                header: perfil.etiquetas.lbNombre || 'Nombre',
                dataIndex: 'nombre',
                flex: 1,
                sortable: true
            },
            //{
            //    header: perfil.etiquetas.lbFechaVence || 'Fecha vencimiento',
            //    align: 'center',
            //    xtype:'datecolumn',
            //    dataIndex: 'fechavencimiento',
            //    flex: 1,
            //    format: 'd/m/Y'
            //    //renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
            //    //    return (value == 1) ? 'Si' : 'No';
            //    //}
            //},
            {
                header: perfil.etiquetas.lbDescuentosAplica || 'Descuentos que aplica',
                dataIndex: 'descventa',
                flex: 1,
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    var descuentos = new Array();
                    if (record.get('descventa')) {
                        descuentos.push((descuentos.length) ? perfil.etiquetas.tabDescVenta.toLowerCase() : perfil.etiquetas.tabDescVenta);
                    }
                    if (record.get('dpp')) {
                        descuentos.push((descuentos.length) ? perfil.etiquetas.lbDpp.toLowerCase() : perfil.etiquetas.lbDpp);
                    }

                    return (descuentos.length) ? descuentos.join(' y ') : perfil.etiquetas.lbNinguno;
                }
            },
            {
                header: perfil.etiquetas.lbCondicionesPlazo || 'Condiciones de plazo',
                dataIndex: 'aplazos',
                flex: 2,
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    metadata.tdAttr = 'style="white-space:normal !important"';
                    if (record.get('aplazos')) {
                        value = perfil.etiquetas.msgFormaPagoAplazo.replace('<#>', record.get('cuotas').length);
                    } else if (record.get('diafijo')) {
                        value = perfil.etiquetas.msgFormaPagoDiaFijo.replace('<#date#>', record.get('fechavencimiento'));
                    } else {
                        value = perfil.etiquetas.msgFormaPagoOtro.replace('<#>', record.get('plazo')).replace('<#type#>', record.get('umplazo'));
                    }
                    return value;
                }
            },
            {
                xtype: 'checkcolumn',
                header: perfil.etiquetas.lbPredeterminado || 'Predeterminado',
                dataIndex: 'predeterminado',
                width: 100
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        /********************/
        /*     PLUGINS      */
        /********************/
        me.enableLocking = true; //esto es necesario para ue pinche el rowespander

        var plugin = new Ext.grid.plugin.RowExpander({
            ptype: 'rowexpander',
            pluginId: 'rowexpander',
            /* Poner aqui todas los datos en letras de los descuentos, las moras y las cuotas */
            rowBodyTpl: new Ext.XTemplate(
                '<span id = \'detail{idterminodepago}\'></span>'
            )
        });
        me.plugins = [plugin];

        this.callParent(arguments);

        me.getView().on({
            expandbody: function (rowNode, record, expanRow, eOpts) {
                me.fireEvent('expandbody', rowNode, record, expanRow, eOpts);
            }
        });
    }
});