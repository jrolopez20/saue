Ext.define('TerminosDePago.view.GridDpp', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_dpp',

    initComponent: function () {
        var me = this;

        //me.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        //    pluginId: 'cellplugin',
        //    clicksToEdit: 2
        //});

        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            clicksToEdit: 2,
            pluginId: 'myRowEditor',
            errorSummary : false
        });

        me.store = Ext.create('Ext.data.Store', {
            storeId: 'stgriddpp',
            fields: ['iddpp', 'porciento', 'dias', 'aclaraciones'],
            clearRemovedOnLoad: false
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddDpp,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }
        ];

        me.columns = [
            {xtype: 'rownumberer'},
            {
                header: 'Id',
                dataIndex: 'iddpp',
                hidden: true,
                hideable: false
            },
            {
                header: perfil.etiquetas.lbPorciento,
                dataIndex: 'porciento',
                editor: {
                    xtype: 'numberfield',
                    emptyText: '0.0',
                    allowBlank: false,
                    allowDecimals: true,
                    maxValue: 100,
                    minValue: 0.01,
                    step: 0.01,
                    validator: function(value){
                        var store = this.up('grid_dpp').getStore();
                        var suma = parseFloat(store.sum('porciento'));
                        if(suma + parseFloat(value) > 100){
                            return 'El m&aacute;ximo var permitido para este campo es ' + String(100.00 - suma)
                        }
                        return true;
                    }
                },
                flex: 1
            },
            {
                header: perfil.etiquetas.lbAntesde,
                dataIndex: 'dias',
                editor: {
                    xtype: 'numberfield',
                    emptyText: '1',
                    allowBlank: true,
                    maxValue: 1000,
                    minValue: 1,
                    validator: function(value){
                        var store = this.up('grid_dpp').getStore();
                        if(store.find('dias',value) != -1){
                            return 'Ya existe un descuento con esta cantidad de d&iacute;as.'
                        }
                        return true;
                    }
                },
                flex: 1
            },
            {
                header: perfil.etiquetas.lbAclaraciones,
                dataIndex: 'aclaraciones',
                flex: 4
            },
            {
                xtype: 'actioncolumn',
                width:30,
                sortable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: perfil.etiquetas.lbBtnEliminar,
                    handler: function (grid, rowIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }
                }]
            }
        ];

        //me.plugins = [me.cellEditing];
        me.plugins = [me.rowEditing];
        this.callParent(arguments);
    }
});