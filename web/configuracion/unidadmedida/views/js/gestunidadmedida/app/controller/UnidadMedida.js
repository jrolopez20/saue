Ext.define('UnidadMedida.controller.UnidadMedida', {
    extend: 'Ext.app.Controller',
    views: [
        'UnidadMedida.view.MagnitudGrid',
        'UnidadMedida.view.UnidadMedidaGrid',
        'UnidadMedida.view.EditMagnitud',
        'UnidadMedida.view.EditUnidadMedida',
        'UnidadMedida.view.GridConversiones'
    ],
    stores: [
        'UnidadMedida.store.Magnitud',
        'UnidadMedida.store.UnidadMedida',
        'UnidadMedida.store.Prefijos'
    ],
    models: [
        'UnidadMedida.model.Magnitud',
        'UnidadMedida.model.UnidadMedida'
    ],
    refs: [
        {ref: 'magnitudgrid', selector: 'magnitudgrid'},
        {ref: 'unidadmedidagrid', selector: 'unidadmedidagrid'},
        {ref: 'editmagnitud', selector: 'editmagnitud'},
        {ref: 'editunidadmedida', selector: 'editunidadmedida'},
        {ref: 'gridconversiones', selector: 'gridconversiones'}
    ],
    init: function () {
        //Ext.Date.dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        var me = this;
        this.listen({
            component: {
                'magnitudgrid': {
                    selectionchange: me.onMGridSelectionChange
                },
                'magnitudgrid button[action=adicionar]': {
                    click: me.onAddMagnitud
                },
                'magnitudgrid button[action=modificar]': {
                    click: me.onModMagnitud
                },
                'magnitudgrid button[action=eliminar]': {
                    click: me.onDelMagnitud
                },
                'magnitudgrid button[action=activar]': {
                    click: me.onActivateMag
                },
                'editmagnitud form button[action=aceptar], editmagnitud form button[action=aplicar]': {
                    click: me.saveMagnitud
                },
                'editmagnitud': {
                    close: me.onReloadGridM
                },
                'unidadmedidagrid': {
                    disable: me.onDisableGridUM,
                    selectionchange: me.onUMGridSelectionChange
                },
                'unidadmedidagrid button[action=adicionar]': {
                    click: me.onAddUM
                },
                'unidadmedidagrid button[action=modificar]': {
                    click: me.onModUM
                },
                'checkbox[name=autogenerar]': {
                    change: me.onAutogenerate
                },
                'editunidadmedida': {
                    close: me.onReloadGridUM
                },
                'editunidadmedida button[action=aceptar], editunidadmedida button[action=aplicar]': {
                    click: me.saveUM
                },
                'checkbox[name=basica]': {
                    change: me.onCheckBasica
                },
                'checkbox[name=umbasefactor]': {
                    change: me.onCheckUMBasicaFactor
                }
            }
        });
    },

    onCheckBasica: function (chbx, newValue, oldValue, eOpts) {
        var form = chbx.up('form').getForm();

        var factorfield = form.findField('factorconversion'),
            cbxum = form.findField('umfactor'),
            checkbaseum = form.findField('umbasefactor'),
            checkautogenerar = form.findField('autogenerar'),
            cbxprefios = form.findField('prefijos');

        if (newValue) {
            factorfield.setValue(1)
        } else {
            factorfield.reset();
            checkautogenerar.reset();
            cbxprefios.reset();
        }

        factorfield.setReadOnly(newValue);

        checkbaseum.setDisabled(newValue);
        checkbaseum.reset();
        //chexkbaseum.setVisible(!newValue);
        cbxum.setDisabled(newValue);
        cbxum.setVisible(!newValue);
    },

    onCheckUMBasicaFactor: function (chbx, newValue, oldValue, eOpts) {
        var fieldset = chbx.up('fieldset'),
            cbxum = fieldset.down('combobox[name=umfactor]');

        cbxum.setDisabled(newValue);
        cbxum.setVisible(!newValue);
    },

    onAutogenerate: function (chbx, newValue, oldValue, eOpts) {
        var fieldset = chbx.up('fieldset');
        fieldset.down('combobox[name=prefijos]').setDisabled(!newValue);
    },

    onMGridSelectionChange: function (selModel, records, eOpts) {
        var me = this,
            grid = me.getMagnitudgrid(),
            gridum = me.getUnidadmedidagrid(),
            hasSelection = grid.getSelectionModel().hasSelection(),
            lastSelected = grid.getSelectionModel().getLastSelected();


        grid.down('button[action="modificar"]').setDisabled(lastSelected.get('standard') == 1);
        grid.down('button[action="activar"]').setDisabled(!hasSelection);

        //grid.down('button[action="eliminar"]').setDisabled(!hasSelection);

        if (records.length) {
            var type = records[0].get('activo'),
                standard = 1;
            records.every(function (record) {
                if (record.get('activo') !== type && type !== 2) {
                    type = 2;
                }
                if (record.get('standard') !== standard && standard !== 2) {
                    standard = 2;
                }
                return true;
            });

            grid.down('button[action="eliminar"]').setDisabled(standard !== 2);

            if (type === 2) {
                grid.down('button[action="activar"]').setIcon('/logistica/facturacion/views/images/active.png');
                grid.down('button[action="activar"]').setText(perfil.etiquetas.lbBtnActivarDesactivar);
                grid.down('button[action="activar"]').setTooltip(perfil.etiquetas.ttpBtnActivarDesactivar);
            } else if (type === 1) {
                grid.down('button[action="activar"]').setIcon('/logistica/facturacion/views/images/inactive.png');
                grid.down('button[action="activar"]').setText(perfil.etiquetas.lbBtnDesactivar);
                grid.down('button[action="activar"]').setTooltip(perfil.etiquetas.ttpBtnDesactivar);
            } else {
                grid.down('button[action="activar"]').setIcon('/logistica/facturacion/views/images/active.png');
                grid.down('button[action="activar"]').setText(perfil.etiquetas.lbBtnActivar);
                grid.down('button[action="activar"]').setTooltip(perfil.etiquetas.ttpBtnActivar);
            }

            gridum.setDisabled(false);
            gridum.getStore().getProxy().extraParams = {idmagnitud: lastSelected.get('idmagnitud')};
            gridum.getStore().load();
        } else {
            gridum.setDisabled(true);
        }
    },

    onUMGridSelectionChange: function (selModel, records, eOpts) {
        var me = this,
            grid = me.getUnidadmedidagrid(),
            hasSelection = grid.getSelectionModel().hasSelection();


        grid.down('button[action="modificar"]').setDisabled(!hasSelection);
        //grid.down('button[action="eliminar"]').setDisabled(!hasSelection);
    },

    onDisableGridUM: function (cmp, eOpts) {
        var me = this,
            grid = me.getUnidadmedidagrid();

        grid.getStore().removeAll();
    },

    onReloadGridM: function (btn) {
        var me = this,
            grid = me.getMagnitudgrid();

        grid.getStore().reload();
    },

    onReloadGridUM: function (btn) {
        var me = this,
            grid = me.getUnidadmedidagrid();

        grid.getStore().reload();
    },

    onAddMagnitud: function (btn) {
        var win = Ext.widget('editmagnitud');
        win.setTitle(perfil.etiquetas.lbWinAddM);
        win.down('button[action=aplicar]').show();
    },

    onModMagnitud: function (btn) {
        var me = this,
            grid = btn.up('magnitudgrid');


        if (grid.getSelectionModel().hasSelection()) {
            var win = Ext.widget('editmagnitud'),
                form = win.down('form');
            win.setTitle(perfil.etiquetas.lbWinModM);
            var record = grid.getSelectionModel().getLastSelected();

            form.loadRecord(record);
        }
    },

    onModUM: function (btn) {
        var me = this,
            grid = btn.up('unidadmedidagrid');

        if (grid.getSelectionModel().hasSelection()) {
            var win = Ext.widget('editunidadmedida'),
                form = win.down('form');
            win.setTitle(perfil.etiquetas.lbWinModUM);
            var record = grid.getSelectionModel().getLastSelected();

            form.down('checkboxfield[name=umbasefactor]').setValue(true);
            form.loadRecord(record);
        }
    },

    onActivateMag: function (btn) {
        //var me = this,
        var grid = btn.up('gridpanel'),
            selection = grid.getSelectionModel().getSelection(),
            ids = [];

        selection.every(function (record) {
            ids.push(record.get('idmagnitud'));
            return true;
        });

        var myMask = new Ext.LoadMask(grid, {msg: perfil.etiquetas.loading});
        myMask.show();
        Ext.Ajax.request({
            url: 'changestateUM',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                mostrarMensaje(1, perfil.etiquetas.msgOK);
                grid.getStore().reload();
            }
        });
    },

    onDelMagnitud: function (btn) {
        var me = this,
            grid = btn.up('gridpanel'),
            selection = grid.getSelectionModel().getSelection(),
            ids = [];

        if (selection.length) {
            var msg = selection.length > 1 ?
                perfil.etiquetas.msgConfirmDelP :
                perfil.etiquetas.msgConfirmDel
            Ext.Msg.confirm(perfil.etiquetas.lbConfirm, msg,
                function (btn) {
                    if (btn === 'yes') {
                        var ids = [];
                        selection.every(function (record) {
                            if (record.get('standard') == 0) {
                                ids.push(record.get('idmagnitud'));
                            }
                            return true;
                        });
                        me.deleteMagnitud(ids, me);
                    }
                });
        }
    },

    deleteMagnitud: function (ids, me) {
        var grid = me.getMagnitudgrid();
        var myMask = new Ext.LoadMask(grid, {msg: perfil.etiquetas.loading});
        myMask.show();
        Ext.Ajax.request({
            url: 'deleteMagnitud',
            method: 'POST',
            params: {
                ids: Ext.encode(ids)
            },
            callback: function (options, success, response) {
                myMask.hide();
                responseData = Ext.decode(response.responseText);
                if (responseData.errors && responseData.errors.length > 0) {
                    mostrarMensaje(3, perfil.etiquetas.msgConfirmDelFail);
                } else {
                    var msg =
                        mostrarMensaje(1, perfil.etiquetas.msgConfirmDelOk);
                }
                grid.getStore().reload();
            }
        });
    },

    saveMagnitud: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = btn.up('form').getForm(),
            grid = me.getMagnitudgrid();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: perfil.etiquetas.lbGuardando});
            myMask.show();

            form.submit({
                submitEmptyText: false,
                success: function (response, action) {
                    // En action.result viene el objeto que se devuelve en el controller
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                    } else {
                        form.reset();
                        grid.getStore().reload();
                    }
                },
                failure: function (response, action) {
                    myMask.hide();
                    switch (action.failureType) {
                        case Ext.form.action.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Fallido', 'Los campos del formulario no debe ser enviados con valores no v&aacute;lidos');
                            break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Fallido', 'Comunicaci&oacute;n Ajax fallida');
                            break;
                        case Ext.form.action.Action.SERVER_INVALID:
                            Ext.Msg.alert('Fallido', action.result.mensaje);
                    }
                }
            });
        }
    },

    onAddUM: function (btn) {
        var win = Ext.widget('editunidadmedida');
        win.setTitle(perfil.etiquetas.lbWinAddUM);
        win.down('button[action=aplicar]').show();
    },

    saveUM: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm(),
            grid = me.getUnidadmedidagrid(),
            flagmodify = !Ext.isEmpty(form.findField('idunidadmedida').getValue()),
            idmagnitud = me.getMagnitudgrid().getSelectionModel().getLastSelected().get('idmagnitud');

        if (form.isValid()) {
            //var tabpanel = win.down('tabpanel'),
            //    tabConversiones = tabpanel.getComponent('tabConversiones'),
            //    stConversiones = tabConversiones.down('gridconversiones').getStore();
            //
            //// Capturando las conversiones
            //if (!tabpanel.isHidden() && !tabConversiones.tab.hidden) {
            //    var recconversiones = stConversiones.getModifiedRecords(),
            //        modified = new Array();
            //    Ext.each(recconversiones, function (record) {
            //        modified.push(record.getData());
            //    });
            //}

            var prefijos = form.findField('prefijos').getValue(),
                extraParams = {idmagnitud: idmagnitud};

            if (!Ext.isEmpty(prefijos)) {
                extraParams.idprefijos = Ext.encode(prefijos);
            }
            //if (!Ext.isEmpty(modified)) {
            //    extraParams.conversiones = Ext.encode(modified);
            //}

            var myMask = new Ext.LoadMask(win, {msg: perfil.etiquetas.lbGuardando});
            myMask.show();

            form.submit({
                submitEmptyText: false,
                //waitMsg: 'Prueba',
                //waitTitle: 'Enviando',
                params: extraParams,
                success: function (response, action) {
                    // En action.result viene el objeto que se devuelve en el controller
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                    } else {
                        form.reset();
                        stConversiones.removeAll();
                        grid.getStore().reload();
                    }
                },
                failure: function (response, action) {
                    myMask.hide();
                    switch (action.failureType) {
                        case Ext.form.action.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Fallido', 'Los campos del formulario no debe ser enviados con valores no v&aacute;lidos');
                            break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Fallido', 'Comunicaci&oacute;n Ajax fallida');
                            break;
                        case Ext.form.action.Action.SERVER_INVALID:
                            Ext.Msg.alert('Fallido', action.result.mensaje);
                    }
                }
            });
        }
    },

    showMsg: function (msg, title, icon, callback, buttons) {
        var me = this;
        Ext.Msg.show({
            title: (title) ? title : 'Información',
            msg: msg,
            buttons: buttons ? buttons : Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            multiline: true,
            fn: (callback) ? callback : Ext.emptyFn,
            //animateTarget: 'addAddressBtn',
            icon: icon ? icon : Ext.MessageBox.INFO,
            scope: me
        });
    }

});