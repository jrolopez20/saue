Ext.define('UnidadMedida.model.UnidadMedida', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idunidadmedida', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'simbolo', type: 'string'},
        {name: 'descripcion', type: 'string'},
        {name: 'idmagnitud', type: 'int'},
        {name: 'basica', type: 'int'},
        {name: 'idestructura', type: 'int'},
        {name: 'autogenerada', type: 'int'},
        {name: 'idconversion', type: 'int'},
        {name: 'factorlocal', type: 'float'},
        {name: 'factorconversion', type: 'float'}
    ]
});