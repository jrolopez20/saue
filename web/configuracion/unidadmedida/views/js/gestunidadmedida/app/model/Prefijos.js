Ext.define('UnidadMedida.model.Prefijos', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idprefijo', type: 'int'},
        {name: 'prefijo', type: 'string'},
        {name: 'simbolo', type: 'string'},
        {name: 'escalacorta', type: 'string'},
        {name: 'escalalarga', type: 'string'},
        {name: 'equivalencia', type: 'float'},
        {name: 'activo', type: 'int'}
    ]
});