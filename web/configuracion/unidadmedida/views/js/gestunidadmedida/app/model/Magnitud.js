Ext.define('UnidadMedida.model.Magnitud', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idmagnitud', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'standard', type: 'int'},
        {name: 'idestructura', type: 'int'},
        {name: 'activo', type: 'int'}
    ]
});