Ext.define('UnidadMedida.store.UnidadMedida', {
    extend: 'Ext.data.Store',
    model: 'UnidadMedida.model.UnidadMedida',

    storeId: 'idStUnidadMedida',
    //autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: 'load',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    },
    sorters: [{
        property: 'factorconversion',
        direction: 'ASC'
    }]
});