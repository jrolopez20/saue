Ext.define('UnidadMedida.store.Magnitud', {
    extend: 'Ext.data.Store',
    model: 'UnidadMedida.model.Magnitud',

    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: 'loadMagnitudes',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    }
});