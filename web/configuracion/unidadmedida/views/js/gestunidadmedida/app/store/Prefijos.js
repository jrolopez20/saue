Ext.define('UnidadMedida.store.Prefijos', {
    extend: 'Ext.data.Store',
    model: 'UnidadMedida.model.Prefijos',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'loadPrefijos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        }
    }
});