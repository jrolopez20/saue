Ext.define('UnidadMedida.view.GridConversiones', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridconversiones',
    store: 'UnidadMedida.store.UnidadMedida',

    initComponent: function () {
        var me = this;
        me.selType = 'rowmodel';

        me.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            pluginId: 'cellplugin',
            clicksToEdit: 2
        });

        //me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        //    clicksToMoveEditor: 1,
        //    clicksToEdit: 2,
        //    pluginId: 'myRowEditor',
        //    errorSummary : false
        //});

        //me.store = Ext.create('Ext.data.Store', {
        //    storeId: 'stgridmora',
        //    fields: ['idmora', 'porciento', 'dias', 'aclaraciones']
        //});

        //me.tbar = [
        //    {
        //        text: perfil.etiquetas.lbBtnAdicionar,
        //        tooltip: perfil.etiquetas.ttpBtnAddMora,
        //        icon: perfil.dirImg + 'adicionar.png',
        //        iconCls: 'btn',
        //        action: 'adicionar'
        //    }
        //];

        me.columns = [
            {xtype: 'rownumberer'},
            {
                header: 'Id',
                dataIndex: 'idconversion',
                hidden: true,
                hideable: false
            },
            {
                header: 'IdUnidadMedida',
                dataIndex: 'idunidadmedida',
                hidden: true,
                hideable: false
            },
            {
                //header: perfil.etiquetas.lbPorciento,
                header: 'Unidad de medida',
                dataIndex: 'denominacion',
                flex: 1
            },
            {
                header: 'Factor de conversi&oacute;n',
                dataIndex: 'factorconversion',
                editor: {
                    xtype: 'numberfield',
                    //allowBlank: false,
                    emptyText: '0.0',
                    allowDecimals: true,
                    maxValue: 100,
                    minValue: 0.000000000000000000000001,
                    step: 1
                    //validator: function(value){
                    //    var store = this.up('grid_mora').getStore();
                    //    if(store.find('dias',value) != -1){
                    //        return 'Ya existe una mora con esta cantidad de d&iacute;as.'
                    //    }
                    //    return true;
                    //}
                },
                flex: 1
            },
            //{
            //    xtype: 'actioncolumn',
            //    width:30,
            //    sortable: false,
            //    items: [{
            //        icon: perfil.dirImg + 'eliminar.png',
            //        iconCls: 'btn',
            //        tooltip: perfil.etiquetas.lbBtnEliminar,
            //        handler: function (grid, rowIndex) {
            //            grid.getStore().removeAt(rowIndex);
            //        }
            //    }]
            //}
        ];

        me.plugins = [me.cellEditing];
        //me.plugins = [me.rowEditing];

        this.callParent(arguments);
    }
});