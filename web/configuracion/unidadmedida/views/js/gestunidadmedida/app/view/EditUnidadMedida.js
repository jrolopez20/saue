Ext.define('UnidadMedida.view.EditUnidadMedida', {
    extend: 'Ext.window.Window',
    alias: 'widget.editunidadmedida',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 500,
    height: 350,
    //height: 550,

    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'center',
                margins: '5 5 5 5',
                url: 'save',
                fieldDefaults: {
                    labelAlign: 'top',
                    //msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 70
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idunidadmedida'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idconversion'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                defaultType: 'textfield',
                                flex: 1,
                                items: [
                                    {
                                        flex: 1,
                                        name: 'denominacion',
                                        fieldLabel: perfil.etiquetas.lbDenominacion || 'Denominaci&oacute;n',
                                        maxLength: 255,
                                        margins: '0 5 0 0',
                                        allowBlank: false
                                    },
                                    {
                                        width: 60,
                                        name: 'simbolo',
                                        fieldLabel: perfil.etiquetas.lbSimbolo || 'S&iacute;mbolo',
                                        maxLength: 255,
                                        //margins: '0 5 0 0',
                                        allowBlank: false
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        margins: '10 0 0 0',
                        items: [
                            {
                                xtype: 'fieldset',
                                flex: 1,
                                title: '¿Unidad de medida b&aacute;sica?',
                                checkboxToggle: true,
                                collapsible: false,
                                collapsed: true,
                                checkboxName: 'basica',
                                layout: 'hbox',
                                items: {
                                    xtype: 'fieldcontainer',
                                    //margins: '20 0 0 0',
                                    flex: 1,
                                    anchor: '100%',
                                    layout: 'hbox',
                                    defaultType: 'checkboxfield',
                                    items: [
                                        {
                                            boxLabel: 'generar m&uacute;ltiplos y subm&uacute;ltiplos',
                                            name: 'autogenerar',
                                            margins: '0 0 8 0',
                                            inputValue: 1,
                                            checked: false
                                        },
                                        {
                                            flex: 1,
                                            xtype: 'combobox',
                                            name: 'prefijos',
                                            store: 'UnidadMedida.store.Prefijos',
                                            queryMode: 'remote',
                                            allowBlank: true,
                                            displayField: 'prefijo',
                                            valueField: 'idprefijo',
                                            editable: false,
                                            margins: '0 0 8 5',
                                            forceSelection: true,
                                            disabled: true,
                                            typeAhead: true,
                                            multiSelect: true,
                                            emptyText: perfil.etiquetas.emptyTextCbx || 'Seleccione...',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" data-qtip="{escalalarga} ({equivalencia})">{prefijo} ({simbolo})</div>',
                                                '</tpl>'
                                            )
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        margins: '5 0 0 0',
                        items: [
                            {
                                xtype: 'fieldset',
                                flex: 1,
                                title: 'Factor de conversi&oacute;n <span style="color: red;font:12px/14px tahoma,arial,verdana,sans-serif !important;">*</span>',
                                collapsible: false,
                                id: 'fsfactorconversion',
                                layout: 'hbox',
                                items: {
                                    xtype: 'fieldcontainer',
                                    //margins: '20 0 0 0',
                                    flex: 1,
                                    anchor: '100%',
                                    layout: 'hbox',
                                    defaultType: 'checkboxfield',
                                    items: [
                                        {
                                            boxLabel: 'en base a la unidad b&aacute;sica',
                                            name: 'umbasefactor',
                                            margins: '0 0 8 0',
                                            inputValue: 1,
                                            checked: false
                                        },
                                        {
                                            flex: 1,
                                            xtype: 'numberfield',
                                            name: 'factorconversion',
                                            emptyText: '0.0',
                                            margins: '0 0 8 5',
                                            allowDecimals: true,
                                            //maxValue: 100,
                                            minValue: 0.000000000000000000000001,
                                            step: 1,
                                            allowBlank: false
                                        },
                                        {
                                            flex: 1,
                                            xtype: 'combobox',
                                            name: 'umfactor',
                                            store: 'UnidadMedida.store.UnidadMedida',
                                            queryMode: 'local',
                                            allowBlank: true,
                                            displayField: 'denominacion',
                                            valueField: 'factorconversion',
                                            editable: false,
                                            margins: '0 0 8 5',
                                            forceSelection: true,
                                            //disabled: true,
                                            typeAhead: true,
                                            //multiSelect: true,
                                            emptyText: perfil.etiquetas.emptyTextCbx || 'Seleccione...',
                                            tpl: Ext.create('Ext.XTemplate',
                                                '<tpl for=".">',
                                                '<div class="x-boundlist-item" data-qtip="{denominacion}">{denominacion} ({simbolo})</div>',
                                                '</tpl>'
                                            ),
                                            validator: function(value){
                                                var fieldset = this.up('fieldset'),
                                                    checkbaseum = fieldset.down('checkbox[name=umbasefactor]');

                                                if(Ext.isEmpty(value) && !checkbaseum.getValue()){
                                                    return 'Este campo es obligatorio.'
                                                }
                                                return true;
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        //margins: '5 0',
                        items: [
                            {
                                flex: 1,
                                xtype: 'textarea',
                                name: 'descripcion',
                                fieldLabel: perfil.etiquetas.lbDescripcion || 'Descripci&oacute;n'
                            }
                        ]
                    }
                ]
            },
            //{
            //    xtype: 'tabpanel',
            //    region: 'center',
            //    margins: '0 5 5 5',
            //    activeTab: 0,
            //    items: [
            //        {
            //            title: perfil.etiquetas.lbConversiones,
            //            xtype: 'panel',
            //            itemId: 'tabConversiones',
            //            id: 'tabConversiones',
            //            layout: 'fit',
            //            items: {
            //                xtype: 'gridconversiones',
            //                margins: '0 5 5 5'
            //            }
            //        }
            //    ]
            //}
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAplicar,
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                action: 'aceptar'
            }
        ];


        this.callParent(arguments);
    }
});