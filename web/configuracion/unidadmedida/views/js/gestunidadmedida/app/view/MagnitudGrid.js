Ext.define('UnidadMedida.view.MagnitudGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.magnitudgrid',

    store: 'UnidadMedida.store.Magnitud',

    initComponent: function () {
        var me = this;
        me.title = perfil.etiquetas.lbMagnitudes;

        me.selModel = {mode: 'MULTI'};

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddM,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificarM,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminarM,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            },
            {
                text: perfil.etiquetas.lbBtnDesactivar,
                tooltip: perfil.etiquetas.ttpBtnDesactivar,
                icon: '/logistica/facturacion/views/images/inactive.png',
                //icon: perfil.dirImg + 'banderaroja.png',
                iconCls: 'btn',
                disabled: true,
                action: 'activar'
            },
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbActivo,
                tooltip: perfil.etiquetas.ttpActivo,
                dataIndex: 'activo',
                width: 40,
                align:'center',
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    var dir = (value == 1) ? '/logistica/facturacion/views/images/active.png'
                        : '/logistica/facturacion/views/images/inactive.png';
                    return '<img height="16" width="16" src="' + dir + '" />'
                }
            },
            {
                header: perfil.etiquetas.lbEstandar,
                tooltip: perfil.etiquetas.ttpEstandar,
                dataIndex: 'standard',
                width: 65,
                align:'center',
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    var dir = (value == 1) ? '/logistica/facturacion/views/images/active.png'
                        : '/logistica/facturacion/views/images/inactive.png';
                    return '<img height="16" width="16" src="' + dir + '" />'
                }
            },
            {
                header: perfil.etiquetas.lbDenominacion,
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});