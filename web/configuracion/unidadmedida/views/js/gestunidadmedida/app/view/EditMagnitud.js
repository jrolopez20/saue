Ext.define('UnidadMedida.view.EditMagnitud', {
    extend: 'Ext.window.Window',
    alias: 'widget.editmagnitud',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 380,
    height: 230,

    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'center',
                url: 'saveMagnitud',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 70
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idmagnitud'
                    },
                    {
                        xtype: 'hidden',
                        name: 'activo'
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        anchor: '100%',
                        items: [
                            {
                                flex: 1,
                                name: 'denominacion',
                                fieldLabel: perfil.etiquetas.lbDenominacion || 'Denominaci&oacute;n',
                                maxLength: 255,
                                allowBlank: false
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                flex: 1,
                                xtype: 'textarea',
                                name: 'descripcion',
                                fieldLabel: perfil.etiquetas.lbDescripcion || 'Descripci&oacute;n'
                            }
                        ]
                    }
                ],
                buttons: [
                    {
                        icon: perfil.dirImg + 'cancelar.png',
                        iconCls: 'btn',
                        text: perfil.etiquetas.lbBtnCancelar,
                        action: 'cancelar',
                        scope: this,
                        handler: this.close
                    },
                    {
                        icon: perfil.dirImg + 'aplicar.png',
                        iconCls: 'btn',
                        disabled: true,
                        formBind: true,
                        text: perfil.etiquetas.lbBtnAplicar,
                        action: 'aplicar'
                    },
                    {
                        icon: perfil.dirImg + 'aceptar.png',
                        iconCls: 'btn',
                        disabled: true,
                        formBind: true,
                        text: perfil.etiquetas.lbBtnAceptar,
                        action: 'aceptar'
                    }
                ]
            }];

        //this.buttons = [
        //    {
        //        icon: perfil.dirImg + 'cancelar.png',
        //        iconCls: 'btn',
        //        text: perfil.etiquetas.lbBtnCancelar,
        //        action: 'cancelar',
        //        scope: this,
        //        handler: this.close
        //    },
        //    {
        //        icon: perfil.dirImg + 'aplicar.png',
        //        iconCls: 'btn',
        //        text: perfil.etiquetas.lbBtnAplicar,
        //        action: 'aplicar'
        //    },
        //    {
        //        icon: perfil.dirImg + 'aceptar.png',
        //        iconCls: 'btn',
        //        text: perfil.etiquetas.lbBtnAceptar,
        //        action: 'aceptar'
        //    }
        //];


        this.callParent(arguments);
    }
});