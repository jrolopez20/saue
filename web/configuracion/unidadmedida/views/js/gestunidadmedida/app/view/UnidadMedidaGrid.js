Ext.define('UnidadMedida.view.UnidadMedidaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.unidadmedidagrid',

    store: 'UnidadMedida.store.UnidadMedida',

    initComponent: function () {
        var me = this;
        me.title = perfil.etiquetas.lbUnidadesMedida;

        me.selModel = {mode: 'MULTI'};

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddUM,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificarUM,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminarUM,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbUnidadBasica,
                dataIndex: 'basica',
                align:'center',
                width: 50,
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    var ret = (value == 1) ? '<img height="16" width="16" src="/logistica/facturacion/views/images/active.png"/>'
                        : '';
                    return ret;
                }
            },
            {
                header: perfil.etiquetas.lbAutogenerada,
                dataIndex: 'autogenerada',
                align:'center',
                width: 80,
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    var dir = (value == 1) ? '/logistica/facturacion/views/images/active.png'
                        : '/logistica/facturacion/views/images/inactive.png';
                    return '<img height="16" width="16" src="' + dir + '" />'
                }
            },
            {
                header: perfil.etiquetas.lbSimbolo,
                dataIndex: 'simbolo',
                align:'center',
                width: 50
            },
            {
                header: perfil.etiquetas.lbDenominacion,
                dataIndex: 'denominacion',
                flex: 3
            },
            {
                header: 'Factor de conversi&oacute;n',
                dataIndex: 'factorconversion',
                flex: 1
            }

        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});