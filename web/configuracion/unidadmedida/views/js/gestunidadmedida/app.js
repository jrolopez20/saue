var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'UnidadMedida',

    enableQuickTips: true,

    paths: {
        'UnidadMedida': '../../views/js/gestunidadmedida/app'
    },

    controllers: ['UnidadMedida'],

    launch: function () {
        UCID.portal.cargarEtiquetas('gestunidadmedida', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'magnitudgrid',
                        region: 'west',
                        //split: true,
                        margin: '5 5 5 5',
                        width: '35%'
                    },
                    {
                        xtype: 'unidadmedidagrid',
                        region: 'center',
                        margin: '5 5 5 0',
                        //collapsible:true,
                        disabled: true,
                        width: '65%'
                    }
                ]
            });
        });

    }
});