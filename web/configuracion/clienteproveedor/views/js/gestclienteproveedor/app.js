var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'ClientesProveedores',

    enableQuickTips: true,

    paths: {
        'ClientesProveedores': '../../views/js/gestclienteproveedor/app'
    },

    controllers: ['ClientesProveedores'],

    launch: function () {
        UCID.portal.cargarEtiquetas('gestclienteproveedor', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'clienteprov_grid',
                        region: 'center',
                        margin: '5 5 0 5'
                    }
                ]
            });
        });

    }
});