Ext.define('ClientesProveedores.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.clienteprov_grid',

    title: 'Listado de clientes y proveedores',
    singleSelect: this.singleSelect || true,
    store: 'ClientesProveedores.store.stClienteProv',

    initComponent: function () {
        var me = this;
        this.addEvents('expandbody');

        if (me.singleSelect) {
            me.selModel = Ext.create('Ext.selection.RowModel');
        } else {
            me.selModel = Ext.create('Ext.selection.CheckboxModel');
        }

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar cliente o proveedor',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            //{
            //    text: 'Modificar',
            //    tooltip: 'Modificar cliente o proveedor',
            //    icon: perfil.dirImg + 'modificar.png',
            //    iconCls: 'btn',
            //    disabled: true,
            //    action: 'modificar'
            //},
            {
                text: 'Eliminar',
                tooltip: 'Eliminar cliente o proveedor',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];


        me.columns = [
            //Ext.create('Ext.grid.RowNumberer'),
            //{
            //    header: 'Foto',
            //    dataIndex: 'base64img',
            //    width: 80,
            //    renderer: function(value,metaData,record,rowIndex,colIndex,store,view){
            //        return new Ext.Img({
            //            src: !Ext.isEmpty(value) ? 'data:image/jpg;base64,' + value : '',
            //            width: 60,
            //            height: 60,
            //            alt: 'FOTO',
            //            frame: true,
            //            region: center
            //        });
            //    }
            //},
            {
                header: 'C&oacute;digo',
                dataIndex: 'codigo'
                //width: 70
            },
            {
                header: 'N&uacute;mero de identificaci&oacute;n',
                dataIndex: 'ci',
                width: 130
            },
            {
                header: 'Nombre completo',
                dataIndex: 'nombre',
                flex: 1,
                sortable: true
            },
            {
                header: 'Tipo',
                dataIndex: 'tipo',
                width: 75,
                renderer: function (v) {
                    var r = '';
                    if (v == 1) {
                        r = 'Cliente';
                    } else if (v == 2) {
                        r = 'Proveedor';
                    } else {
                        r = 'Mixto'
                    }
                    return r;
                }
            },
            {
                header: 'Cuenta bancaria',
                dataIndex: 'cuentabancaria',
                flex: 1
                //width: 75
            },
            {
                header: 'Cuenta a cobrar',
                dataIndex: 'cuentacobrar',
                flex: 1
            },
            {
                header: 'Cuenta a pagar',
                dataIndex: 'cuentapagar',
                flex: 1
            },
            {
                header: '¿Es empresa?',
                dataIndex: 'isempresa',
                width: 85,
                align: 'center',
                renderer: function (value, metadata, record, rowIdex, colIndex, store, view) {
                    return (value == 1) ? 'Si' : 'No';
                }
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        /********************/
        /*     PLUGINS      */
        /********************/
        me.enableLocking = true; //esto es necesario para ue pinche el rowespander

        var plugin = new Ext.grid.plugin.RowExpander({
            ptype: 'rowexpander',
            pluginId: 'rowexpander',
            /* Poner la Foto aki en el row expander */
            rowBodyTpl: new Ext.XTemplate(
                '<table cellspacing="0" cellpadding="0" border="0" width="auto" style="padding-top: 15px">',
                '<tr>',
                '<td width="62" valign="top" height="62">',
                '<img valign="top" src=\'{foto}\' width="62" height="62" alt="FOTO">',
                '</td>',
                '<td valign="top" style="padding-left: 10px">',
                '<table cellspacing="0" cellpadding="0" border="0" style="font-size: 11px">',
                '<tr>',
                '<td valign="top" rowspan="3"><b>Direcci&oacute;n:</b></td>',
                '<td valign="top"><i>{direccion}</i></td>',
                '</tr>',
                '<tr>',
                '<td valign="top"><i>{provincia},</i> CP: <i>{codigopostal}</i></td>',
                '</tr>',
                '<tr>',
                '<td valign="top"><i>{pais}</i></td>',
                '</tr>',
                '<tr>',
                '<td valign="top"><b>Sitio web:</b></td>',
                '<td valign="top"><i>{sitioweb}</i></td>',
                '</tr>',
                '</table>',
                '</td>',
                '<td valign="top" style="padding-left: 20px">',
                '<table cellspacing="0" cellpadding="0" border="0" style="font-size: 11px">',
                '<tr>',
                '<td valign="top"><b>Tel&eacute;fono:</b></td>',
                '<td valign="top"><i>{telefono}</i></td>',
                '</tr>',
                '<tr>',
                '<td valign="top"><b>M&oacute;vil:</b></td>',
                '<td valign="top"><i>{movil}</i></td>',
                '</tr>',
                '<tr>',
                '<td valign="top"><b>Fax:</b></td>',
                '<td valign="top"><i>{fax}</i></td>',
                '</tr>',
                '<tr>',
                '<td valign="top"><b>Email:</b></td>',
                '<td valign="top"><i>{email}</i></td>',
                '</tr>',
                '</table>',
                '</td>',
                '</tr>',
                '</table>',
                '<p><b>Descripci&oacute;n:</b> <i>{descripcion}</i></p>',
                '<div id = \'ctos{idclientesproveedores}\'></div>'
            )
        });
        me.plugins = [plugin];

        this.callParent(arguments);

        me.getView().on({
            expandbody: function (rowNode, record, expanRow, eOpts) {
                me.fireEvent('expandbody', rowNode, record, expanRow, eOpts);
            }
        });
    }
});