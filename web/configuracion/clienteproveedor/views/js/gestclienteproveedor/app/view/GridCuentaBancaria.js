Ext.define('ClientesProveedores.view.GridCuentaBancaria', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_cuentabancaria',

    initComponent: function () {
        var me = this;

        //me.selType = 'rowmodel';

        me.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            pluginId: 'cellplugin',
            clicksToEdit: 1
        });

        //me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        //    clicksToMoveEditor: 1,
        //    clicksToEdit: 2,
        //    pluginId: 'myRowEditor'
        //});

        me.store = Ext.create('Ext.data.Store', {
            fields: ['idcuentacliente', 'numerocuenta', 'banco', 'predeterminada']
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddCuentaBanc,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbNoCuenta,
                dataIndex: 'numerocuenta',
                editor: {
                    allowBlank: false,
                    emptyText: 'No. de la cuenta bancaria'
                },
                flex: 1
            },
            {
                header: perfil.etiquetas.lbBanco,
                dataIndex: 'banco',
                editor: {
                    allowBlank: false,
                    emptyText: 'Nombre del banco'
                },
                flex: 1
            },
            {
                xtype: 'checkcolumn',
                header: perfil.etiquetas.lbPredeterminada,
                dataIndex: 'predeterminada',
                width: 110,
                editor: {
                    xtype: 'checkbox',
                    cls: 'x-grid-checkheader-editor'
                },
                stopSelection: false
            },
            {
                xtype: 'actioncolumn',
                width:30,
                sortable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: perfil.etiquetas.lbBtnEliminar,
                    handler: function (grid, rowIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }
                }]
            }
        ];

        me.plugins = [me.cellEditing];
        this.callParent(arguments);
    }
});