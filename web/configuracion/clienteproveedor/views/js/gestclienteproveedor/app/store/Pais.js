Ext.define('ClientesProveedores.store.Pais', {
    extend: 'Ext.data.Store',
    model: 'ClientesProveedores.model.Pais',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'getCountries',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});