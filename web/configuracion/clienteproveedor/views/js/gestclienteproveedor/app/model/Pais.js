Ext.define('ClientesProveedores.model.Pais', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idpais', type: 'int'},
        {name: 'nombrepais', type: 'string'},
        {name: 'siglas', type: 'string'}
    ]
});