Ext.define('BancoSucursal.view.GridSucursal', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_sucursal',
    store: 'BancoSucursal.store.Sucursal',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddSucursal,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModSucursal,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnDelSucursal,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, '->',
            {
                xtype: 'searchfield',
                store: 'BancoSucursal.store.Sucursal',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['numero', 'direccion']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbNumero,
                dataIndex: 'numero',
                width: 100
            },
            {
                header: perfil.etiquetas.lbDireccion,
                dataIndex: 'direccion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});