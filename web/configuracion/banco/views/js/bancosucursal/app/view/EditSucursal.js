Ext.define('BancoSucursal.view.EditSucursal', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_sucursal',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 450,
    initComponent: function () {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'updateSucursal',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'idsucursal'
                },
                {
                    xtype: 'hidden',
                    name: 'idbanco'
                },
                {
                    xtype: 'textfield',
                    name: 'numero',
                    fieldLabel: perfil.etiquetas.lbNumero,
                    maskRe: /^[0-9]+$/,
                    regex: /^[0-9]+$/,
                    maxLength: 30,
                    allowBlank: false
                },
                {
                    xtype: 'textarea',
                    name: 'direccion',
                    fieldLabel: perfil.etiquetas.lbDireccion,
                    maxLength: 255,
                    allowBlank: false
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});