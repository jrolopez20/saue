Ext.define('BancoSucursal.view.GridBanco', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_banco',
    store: 'BancoSucursal.store.Banco',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddBanco,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModBanco,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnDelBanco,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, '->',
            {
                xtype: 'searchfield',
                store: 'BancoSucursal.store.Banco',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['codigo', 'abreviatura', 'nombre']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbCodigo,
                dataIndex: 'codigo',
                width: 100
            },
            {
                header: perfil.etiquetas.lbAbrev,
                dataIndex: 'abreviatura',
                width: 150
            },
            {
                header: perfil.etiquetas.lbNombre,
                dataIndex: 'nombre',
                flex: 1
            },
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});