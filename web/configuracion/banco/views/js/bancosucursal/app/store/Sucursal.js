Ext.define('BancoSucursal.store.Sucursal', {
    extend: 'Ext.data.Store',
    model: 'BancoSucursal.model.Sucursal',

    autoLoad: false,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadSucursales'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});