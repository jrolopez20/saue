Ext.define('BancoSucursal.controller.BancoSucursal', {
    extend: 'Ext.app.Controller',
    views: [
        'BancoSucursal.view.GridBanco',
        'BancoSucursal.view.GridSucursal',
        'BancoSucursal.view.EditBanco',
        'BancoSucursal.view.EditSucursal'
    ],
    stores: [
        'BancoSucursal.store.Banco',
        'BancoSucursal.store.Sucursal'
    ],
    models: [
        'BancoSucursal.model.Banco',
        'BancoSucursal.model.Sucursal'
    ],
    refs: [
        {ref: 'grid_banco', selector: 'grid_banco'},
        {ref: 'grid_sucursal', selector: 'grid_sucursal'},
        {ref: 'edit_banco', selector: 'edit_banco'},
        {ref: 'edit_sucursal', selector: 'edit_sucursal'}
    ],
    init: function () {
        var me = this;
        me.control({
            'grid_banco': {
                selectionchange: me.onBancoSelectionChange
            },
            'grid_sucursal': {
                selectionchange: me.onSucursalSelectionChange
            },
            'grid_banco button[action=adicionar]': {
                click: me.addBanco
            },
            'grid_banco button[action=modificar]': {
                click: me.modBanco
            },
            'grid_banco button[action=eliminar]': {
                click: me.delBanco
            },
            'grid_sucursal button[action=adicionar]': {
                click: me.addSucursal
            },
            'grid_sucursal button[action=modificar]': {
                click: me.modSucursal
            },
            'grid_sucursal button[action=eliminar]': {
                click: me.delSucursal
            },
            'edit_banco button[action=aceptar]': {
                click: me.saveBanco
            },
            'edit_banco button[action=aplicar]': {
                click: me.saveBanco
            },
            'edit_sucursal button[action=aceptar]': {
                click: me.saveSucursal
            },
            'edit_sucursal button[action=aplicar]': {
                click: me.saveSucursal
            }
        });

        me.getBancoSucursalStoreSucursalStore().on(
            {
                beforeload: {fn: me.setearExtraParams, scope: this}
            }
        );
    },

    setearExtraParams: function (store) {
        var extraParams = {
            idbanco: this.getGrid_banco().getSelectionModel().getLastSelected().get('idbanco')
        };
        store.getProxy().extraParams = extraParams;
    },

    onBancoSelectionChange: function (sm) {
        var me = this;
        me.getGrid_banco().down('button[action=modificar]').disable();
        me.getGrid_banco().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getGrid_sucursal().enable();
            me.getGrid_sucursal().getStore().load();
            me.getGrid_banco().down('button[action=modificar]').enable();
            me.getGrid_banco().down('button[action=eliminar]').enable();
        }else{
            me.getGrid_sucursal().getStore().removeAll();
            me.getGrid_sucursal().disable();
        }
    },
    onSucursalSelectionChange: function (sm) {
        var me = this;
        me.getGrid_sucursal().down('button[action=modificar]').disable();
        me.getGrid_sucursal().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getGrid_sucursal().down('button[action=modificar]').enable();
            me.getGrid_sucursal().down('button[action=eliminar]').enable();
        }
    },
    addBanco: function () {
        var win = Ext.widget('edit_banco');
        win.setTitle(perfil.etiquetas.lbWinAddBanco);
        win.down('button[action=aplicar]').show();
    },
    modBanco: function () {
        if (this.getGrid_banco().getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_banco');
            win.setTitle(perfil.etiquetas.lbWinModBanco);
            win.down('button[action=aplicar]').hide();
            var form = win.down('form');
            var record = this.getGrid_banco().getSelectionModel().getLastSelected();
            form.loadRecord(record);
        }
    },
    delBanco: function () {
        var me = this,
            grid = me.getGrid_banco();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelBanco, function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando banco...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteBanco',
                        method: 'POST',
                        params: {
                            idbanco: record.get('idbanco')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.down('button[action=modificar]').disable();
                                grid.down('button[action=eliminar]').disable();
                                grid.getStore().load();
                            }
                        }
                    });
                }
            });
        }
    },
    addSucursal: function () {
        var win = Ext.widget('edit_sucursal');
        win.setTitle(perfil.etiquetas.lbWinAddSucursal);
        win.down('button[action=aplicar]').show();
        var form = win.down('form').getForm();
        form.findField('idbanco').setValue(this.getGrid_banco().getSelectionModel().getLastSelected().get('idbanco'));
    },
    modSucursal: function () {
        if (this.getGrid_sucursal().getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_sucursal');
            win.setTitle(perfil.etiquetas.lbWinModBanco);
            win.down('button[action=aplicar]').hide();
            var form = win.down('form');
            var record = this.getGrid_sucursal().getSelectionModel().getLastSelected();
            form.loadRecord(record);
            form.getForm().findField('idbanco').setValue(this.getGrid_banco().getSelectionModel().getLastSelected().get('idbanco'));
        }
    },
    delSucursal: function () {
        var me = this,
            grid = me.getGrid_sucursal();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelSucursal, function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando sucursal...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteSucursal',
                        method: 'POST',
                        params: {
                            idsucursal: record.get('idsucursal')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.down('button[action=modificar]').disable();
                                grid.down('button[action=eliminar]').disable();
                                grid.getStore().load();
                            }
                        }
                    });
                }
            });
        }
    },

    saveBanco: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando banco...'});
            myMask.show();

            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getGrid_banco().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    saveSucursal: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando sucursal...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getGrid_sucursal().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    }
});