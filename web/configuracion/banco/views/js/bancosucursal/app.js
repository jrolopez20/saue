var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'BancoSucursal',

    enableQuickTips: true,

    paths: {
        'BancoSucursal': '../../views/js/bancosucursal/app'
    },

    controllers: ['BancoSucursal'],

    launch: function () {
        UCID.portal.cargarEtiquetas('bancosucursal', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'grid_banco',
                        title: 'Listado de bancos',
                        region: 'center',
                        margin: '5 5 0 5',
                        width: '50%'
                    },
                    {
                        xtype: 'grid_sucursal',
                        title: 'Listado de sucursales por banco',
                        region: 'south',
                        height: '60%',
                        split: true,
                        disabled: true,
                        margin: '0 5 5 5'
                    }
                ]
            });
        });
    }
});