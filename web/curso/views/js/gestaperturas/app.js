var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'GestCarreras',

    enableQuickTips: true,

    paths: {
        'GestAperturas': '../../views/js/gestaperturas/app'
    },

    controllers: ['GestAperturas'],

    launch: function () {
        UCID.portal.cargarEtiquetas('gestaperturas', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'panel',
                        html: 'Hola Mundo Apertura!!!'
                    }
                ]
            });
        });

    }
});