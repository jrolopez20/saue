Ext.define('GestCursos.model.Alumno', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idalumno', type: 'int', convert: null},
        {name: 'codigo', type: 'int', convert: null},
        {name: 'nombre',  type: 'string'},
        {name: 'facultad',  type: 'string'},
        {name: 'nota',  type: 'int'},
        {name: 'faltas',  type: 'int'},
        {name: 'detalles',  type: 'string'}
    ]
})