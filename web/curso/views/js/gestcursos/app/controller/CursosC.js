Ext.define('GestCursos.controller.CursosC', {
    extend: 'Ext.app.Controller',

    views: [
        'GestCursos.view.curso.Grid',
        'GestCursos.view.curso.Toolbar',
        'GestCursos.view.curso.Edit',
        'GestCursos.view.profesor.Grid',
        'GestCursos.view.profesor.Search',
        'GestCursos.view.profesor.Toolbar',
        'GestCursos.view.alumno.Grid',
        'GestCursos.view.alumno.Search',
        'GestCursos.view.alumno.Toolbar'
    ],
    stores: [
        'GestCursos.store.Cursos',
        'GestCursos.store.Annos',
        'GestCursos.store.Periodos',
        'GestCursos.store.Horarios',
        'GestCursos.store.Profesores',
        'GestCursos.store.Alumnos',
        'GestCursos.store.Aulas',
        'GestCursos.store.Facultades',
        'GestCursos.store.Materias'
    ],

    models: [
        'GestCursos.model.Curso'
    ],

    refs: [
        {ref: 'curso_toolbar', selector: 'curso_toolbar'},
        {ref: 'curso_edit', selector: 'curso_edit'},
        {ref: 'curso_grid', selector: 'curso_grid'}
    ],

    init: function () {
        var me = this;

        me.control({

            'curso_grid': {
                selectionchange: me.manejarBotones
            },
            'curso_toolbar button[action=adicionar]': {
                click: me.adicionarCurso
            },
            'curso_toolbar button[action=modificar]': {
                click: me.modificarCurso
            },
            'curso_toolbar button[action=eliminar]': {
                click: me.eliminarCurso
            },
            'curso_toolbar button[action=mostrar_alumnos]': {
                click: me.mostrarAlumnos
            },
            'curso_toolbar combo[name=anno]': {
                select: function(combo){me.cargarPeriodos(combo, 'toolbar');}
            },
            'curso_profesor_search button[action=aceptar]': {
                click: me.asociarProfesor
            },
            'curso_profesor_search button[action=aplicar]': {
                click: me.asociarProfesor
            },
            'curso_edit button[action=aceptar]': {
                click: me.guardarCurso
            },
            'curso_edit combo[name=anno]': {
                select: function(combo){me.cargarPeriodos(combo, 'edit');}
            },
            'curso_edit button[action=aplicar]': {
                click: me.guardarCurso
            }
        });
    },

    cargarPeriodos: function(combo_anno, desde){
        var me = this,
            combo, periodos_store;

        if(desde === "toolbar"){
            combo = me.getCurso_toolbar().down('combo[name=idperiododocente]');
        }
        else if(desde === "edit"){
            combo = me.getCurso_edit().down('combo[name=idperiododocente]');
        }

        combo.reset();
        periodos_store = combo.getStore();

            periodos_store.load({
                params: {anno: combo_anno.getValue()}
            });
    },

    manejarBotones: function (store, selected) {
        var me = this,
            curso_toolbar = me.getCurso_toolbar();
        curso_toolbar.down('button[action=modificar]').setDisabled(selected.length === 0);
        curso_toolbar.down('button[action=eliminar]').setDisabled(selected.length === 0);
        curso_toolbar.down('button[action=mostrar_alumnos]').setDisabled(selected.length === 0);
    },

    adicionarCurso: function () {
        var window = Ext.widget('curso_edit');
        window.setTitle(perfil.etiquetas.lbTtlAdicionar);
    },

    modificarCurso: function () {
        var window = Ext.widget('curso_edit'),
            formulario = window.down('form'),
            me = this,
            record = me.getCurso_grid().getSelectionModel().getLastSelected(),
            curso_toolbar = me.getCurso_toolbar();

        window.setTitle(perfil.etiquetas.lbTtlModificar);

        //formulario.down('combo[name=anno]').disable();
        //formulario.down('combo[name=idperiododocente]').disable();

        formulario.down('combo[name=anno]').setValue(curso_toolbar.down('combo[name=anno]').getValue());
        formulario.down('combo[name=idperiododocente]').setValue(curso_toolbar.down('combo[name=idperiododocente]').getValue());

        formulario.loadRecord(record);
    },

    eliminarCurso: function () {
        var me = this,
            record = me.getCurso_grid().getSelectionModel().getLastSelected(),
            store = me.getCurso_grid().getStore();

        mostrarMensaje(
            2,
            perfil.etiquetas.lbMsgConfEliminar,
            function (btn, text) {
                if (btn == 'ok') {
                    store.remove(record);
                    me.sincronizarStore(me.getCurso_grid(), store);
                }
            }
        )
    },

    mostrarAlumnos: function () {
        var window = Ext.widget('curso_alumno_search'),
            alumno_grid = window.down('curso_alumno_grid'),
            alumno_toolbar = window.down('curso_alumno_toolbar'),
            me = this,
            curso_toolbar = me.getCurso_toolbar(),
            record = me.getCurso_grid().getSelectionModel().getLastSelected();

        //record.set('anno', curso_toolbar.down('combo[name=anno]').getValue());
        //record.set('periodo', curso_toolbar.down('combo[name=idperiododocente]').getRawValue());

        alumno_toolbar.down('form').loadRecord(record);

        alumno_toolbar.down('form').down('displayfield[name=anno]').setValue(curso_toolbar.down('combo[name=anno]').getValue());
        alumno_toolbar.down('form').down('displayfield[name=periodo]').setValue(curso_toolbar.down('combo[name=idperiododocente]').getRawValue());

        alumno_grid.getStore().load(
            {
                //idcurso: idcurso
            }
        );
    },

    asociarProfesor: function (button) {
        var window = button.up('curso_profesor_search'),
            curso_profesor_grid = window.down('curso_profesor_grid'),
            record = curso_profesor_grid.getSelectionModel().getLastSelected(),
            me = this,
            curso_edit = me.getCurso_edit();

        curso_edit.down('triggerfield[name=profesor_nombre_completo]').setValue(record.get('nombre') + ' ' + record.get('apellidos'));
        curso_edit.down('hidden[name=idprofesor]').setValue(record.get('idprofesor'));

        if (button.action === 'aceptar')
            window.close();
    },

    guardarCurso: function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            me = this,
            curso_edit = me.getCurso_edit();

        if (form.getForm().isValid()) {
            var record = form.getRecord(),
                values = form.getValues();

            //modificando
            if (record) {
                values.aula_descripcion = curso_edit.down('combo[name=idaula]').getRawValue();
                values.materia_descripcion = curso_edit.down('combo[name=idmateria]').getRawValue();
                values.horario_descripcion = curso_edit.down('combo[name=idhorario]').getRawValue();
                record.set(values);
            }
            //adicionando
            else {
                me.getCurso_grid().getStore().add(values);
            }

            me.sincronizarStore(me.getCurso_grid(), me.getCurso_grid().getStore());

            if (button.action === 'aceptar')
                win.close();
        }
    },

    sincronizarStore: function (grid, store) {
        store.sync({
            success: function (batch) {
                if (batch.operations[0].action == "create") {
                    //var idcarrera = Ext.decode(batch.operations[0].response.responseText).idcarrera;
                    //store.last().set('idcarrera', idcarrera);

                    grid.down('pagingtoolbar').moveLast();
                    //grid.down('carreralistpbar').updateInfo();
                } else if (batch.operations[0].action == "destroy") {
                    if (store.count() > 0)
                        grid.down('pagingtoolbar').doRefresh();//me.getList().down('carreralistpbar').doRefresh();
                    else
                        grid.down('pagingtoolbar').movePrevious();
                }
            },
            failure: function () {
                grid.down('pagingtoolbar').doRefresh();
            }
        });
    }
});