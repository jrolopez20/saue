Ext.define('GestCursos.view.curso.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.curso_grid',

    store: 'GestCursos.store.Cursos',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.CheckboxModel');

        me.bbar = [
            Ext.create('Ext.toolbar.Paging', {
                displayInfo: true,
                store: me.store
            }),
            '->',
            Ext.create('Ext.toolbar.Toolbar', {
                items:[
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Fecha de inicio',//Etiqueta
                        format: "D, d M Y"
                    },
                    {
                        text: 'Imprimir listado',
                        icon: perfil.dirImg + 'imprimir.png'
                    }
                ]
            })
        ];

        me.tbar = Ext.widget('curso_toolbar');

        me.viewConfig = {
            getRowClass: function(record){
                if (record.get('estado') === false)
                    return 'FilaRoja';
            }
        };

        me.columns = [
            {
                dataIndex: 'idcurso',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'idprofesor',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'idhorario',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'idaula',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'idmateria',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'idperiododocente',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'idfacultad',
                hidden: true,
                hideable: false
            },
            {
                dataIndex: 'cupo',
                hidden: true,
                hideable: false
            },
            {
                text: perfil.etiquetas.lbHdrAula,
                dataIndex: 'aula_descripcion'
            },
            {
                text: perfil.etiquetas.lbHdrMateria,
                dataIndex: 'materia_descripcion',
                flex: 1
            },
            {
                text: perfil.etiquetas.lbHdrProfesor,
                dataIndex: 'profesor_nombre_completo',
                flex: 1
            },
            {
                text: perfil.etiquetas.lbHdrHorario,
                dataIndex: 'horario_descripcion',
                flex: 1
            },
            {
                text: perfil.etiquetas.lbHdrNroAlum,
                dataIndex: 'n_alumnos'
            },
            {
                text: perfil.etiquetas.lbHdrPara,
                dataIndex: 'par_curs'
            },
            {
                text: perfil.etiquetas.lbHdrCupDisp,
                xtype:'templatecolumn',
                tpl:'{[values.cupo - values.n_alumnos]} / {cupo}'
            },
            {
                text: perfil.etiquetas.lbHdrEstado,
                dataIndex: 'estado',
                hidden: true,
                xtype: 'booleancolumn'
            }
        ];

        this.callParent(arguments);
    }
});