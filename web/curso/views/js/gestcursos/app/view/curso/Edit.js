Ext.define('GestCursos.view.curso.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.curso_edit',

    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 500,

    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                fieldDefaults: {
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 60
                },
                defaults: {
                    padding: '5'
                },
                defaultType: 'combo',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idcurso'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idprofesor'
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'combo',
                        items: [
                            {
                                fieldLabel: perfil.etiquetas.lbCmpAnno,
                                emptyText: '--Seleccione--',
                                width: 180,
                                name: 'anno',
                                valueField: 'anno',
                                displayField: 'anno',
                                store: 'GestCursos.store.Annos',
                                queryMode: 'local'
                            },
                            {
                                fieldLabel: perfil.etiquetas.lbCmpPeriodo,
                                emptyText: '--Seleccione--',
                                padding: '0 0 0 10',
                                flex: 1,
                                name: 'idperiododocente',
                                valueField: 'idperiododocente',
                                displayField: 'descripcion',
                                store: 'GestCursos.store.Periodos',
                                queryMode: 'local',
                                allowBlank: false
                            }
                        ]
                    },
                    {
                        fieldLabel: perfil.etiquetas.lbHdrHorario,
                        emptyText: '--Seleccione--',
                        name: 'idhorario',
                        valueField: 'idhorario',
                        displayField: 'horario_descripcion',
                        store: 'GestCursos.store.Horarios',
                        editable: false,
                        queryMode: 'local',
                        allowBlank: false
                    },
                    {
                        fieldLabel: perfil.etiquetas.lbHdrAula,
                        emptyText: '--Seleccione--',
                        name: 'idaula',
                        valueField: 'idaula',
                        displayField: 'descripcion',
                        store: 'GestCursos.store.Aulas',
                        editable: false,
                        queryMode: 'local',
                        allowBlank: false
                    },
                    {
                        fieldLabel: perfil.etiquetas.lbHdrMateria,
                        emptyText: '--Seleccione--',
                        name: 'idmateria',
                        valueField: 'idmateria',
                        displayField: 'descripcion',
                        store: 'GestCursos.store.Materias',
                        editable: false,
                        queryMode: 'local',
                        allowBlank: false
                    },
                    {
                        xtype: 'triggerfield',
                        fieldLabel: perfil.etiquetas.lbHdrProfesor,
                        emptyText: '--Seleccione--',
                        trigger1Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                        name: 'profesor_nombre_completo',
                        allowBlank: false,
                        onTrigger1Click: function () {
                            var window = Ext.create('GestCursos.view.profesor.Search');
                            window.down('curso_profesor_grid').getStore().load();
                        }
                    },
                    {
                        fieldLabel: perfil.etiquetas.lbCmpFacultad,
                        emptyText: '--Seleccione--',
                        name: 'idfacultad',
                        valueField: 'idfacultad',
                        displayField: 'denominacion',
                        store: 'GestCursos.store.Facultades',
                        editable: false,
                        queryMode: 'local',
                        allowBlank: false
                    },
                    {
                        xtype: 'fieldset',
                        title: perfil.etiquetas.lbCmpDetalles,
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'displayfield',
                                fieldLabel: perfil.etiquetas.lbCmpAlumnos,
                                name: 'n_alumnos',
                                flex: 1
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: perfil.etiquetas.lbCmpCupos,
                                name: 'cupo',
                                padding: '0 0 10 10',
                                flex: 2,
                                allowBlank: false
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: perfil.etiquetas.lbCmpParalelos,
                                name: 'par_curs',
                                padding: '0 0 10 10',
                                flex: 2,
                                allowBlank: false
                            }
                        ]
                    },
                    {
                        id: 'idestado_carrera',
                        xtype: 'checkbox',
                        fieldLabel: perfil.etiquetas.lbHdrEstado,
                        checked: true,
                        inputValue: true,
                        uncheckedValue: false,
                        name: 'estado'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAplicar,
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
})