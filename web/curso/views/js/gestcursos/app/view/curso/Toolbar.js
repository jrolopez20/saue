Ext.define('GestCursos.view.curso.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.curso_toolbar',

    initComponent: function () {
        var me = this;

        me.items = [
            {
                itemId: 'idBtnAddCurso',
                text: perfil.etiquetas.lbBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                itemId: 'idBtnUpdCurso',
                disabled: true,
                text: perfil.etiquetas.lbBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'modificar'
            },
            {
                itemId: 'idBtnDelCurso',
                disabled: true,
                text: perfil.etiquetas.lbBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'eliminar'
            },
             '-',
            {
                itemId: 'idBtnGetAlumnos',
                disabled: true,
                text: perfil.etiquetas.lbBtnAlumnos,
                icon: perfil.dirImg + 'usuario.png',
                iconCls: 'btn',
                action: 'mostrar_alumnos'
            },
            '-',
            {
                xtype: 'combo',
                store: 'GestCursos.store.Annos',
                emptyText: 'Filtrar por año',//Etiqueta
                name: 'anno',
                valueField: 'anno',
                displayField: 'anno',
                queryMode: 'local',
                width: 250,
                padding: '0 0 0 5',
                labelWidth: 40
            },
            {
                xtype: 'searchcombofield',
                store: Ext.create('GestCursos.store.Periodos'),
                emptyText: 'Filtrar por período',//Etiqueta
                name: 'idperiododocente',
                valueField: 'idperiododocente',
                displayField: 'descripcion',
                storeToFilter: 'GestCursos.store.Cursos',
                //filterPropertysNames: ['idperiododocente'],
                queryMode: 'local',
                width: 250,
                padding: '0 0 0 5',
                labelWidth: 40
            },
            {
                xtype: 'searchcombofield',
                store: 'GestCursos.store.Horarios',
                emptyText: 'Filtrar por horario',//Etiqueta
                name: 'idhorario',
                valueField: 'idhorario',
                displayField: 'horario_descripcion',
                storeToFilter: 'GestCursos.store.Cursos',
                //filterPropertysNames: ['idhorario'],
                queryMode: 'local',
                width: 250,
                padding: '0 0 0 5',
                labelWidth: 40
            }
        ];

        this.callParent(arguments);
    }
});