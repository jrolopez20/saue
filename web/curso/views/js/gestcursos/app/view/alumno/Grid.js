Ext.define('GestCursos.view.alumno.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.curso_alumno_grid',

    store: 'GestCursos.store.Alumnos',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.tbar = Ext.create('GestCursos.view.alumno.Toolbar');

        me.columns = [
            {
                dataIndex: 'idalumno',
                hidden: true,
                hideable: false
            },
            {
                text: 'Código alumno',//perfil.etiquetas.lbHdrApellidosProfesor,
                dataIndex: 'codigo'
            },
            {
                text: 'Nombre',//perfil.etiquetas.lbHdrApellidosProfesor,
                dataIndex: 'nombre',
                flex: 2
            },
            {
                text: 'Facultad',//perfil.etiquetas.lbHdrNombreProfesor,
                dataIndex: 'facultad'
            },
            {
                text: 'Nota',//perfil.etiquetas.lbHdrNombreProfesor,
                dataIndex: 'nota'
            },
            {
                text: 'Faltas',//perfil.etiquetas.lbHdrNombreProfesor,
                dataIndex: 'faltas'
            },
            {
                text: 'Detalles',//perfil.etiquetas.lbHdrNombreProfesor,
                dataIndex: 'detalles',
                flex: 1
            }
        ];

        this.callParent(arguments);
    }
});