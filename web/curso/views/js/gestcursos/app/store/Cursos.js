Ext.define('GestCursos.store.Cursos', {
    extend: 'Ext.data.Store',
    model: 'GestCursos.model.Curso',

    storeId: 'idCursosStoreCursos',
    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'cargarCursos',
            create: 'insertarCursos',
            update: 'modificarCursos',
            destroy: 'eliminarCursos'
        },
        actionMethods: {
            read: 'POST',
            create: 'POST',
            update: 'POST',
            destroy: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            idProperty: 'idcurso',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});