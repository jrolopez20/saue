var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();
Ext.application({
    name: 'Prestamo',
    enableQuickTips: true,
    paths: {
        'Prestamo': '../../views/js/prestamo/app',
        'InventarioPersona': '../../views/js/inventariopersona/app'
    },
    controllers: ['Prestamo'],
    launch: function () {
        UCID.portal.cargarEtiquetas('prestamo', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'center',
                        xtype: 'grid_prestamo',
                        margin: '5 5 5 5',
                        title: 'Listado de préstamos y descuentos'
                    },
                    {
                        xtype: 'edit_prestamo',
                        region:'east',
                        width: 350,
                        margin: '5 5 5 0'
                    }
                ]
            });
        });
    }
});