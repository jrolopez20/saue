Ext.define('Prestamo.view.Edit', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.edit_prestamo',
    layout: 'border',

    title: 'Adicionar préstamo',
    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                region: 'north',
                margin: '0 0 5 0',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%'
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 20',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idprestamo'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idpersona'
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        anchor: '100%',
                        items: [
                            {
                                name: 'numero',
                                fieldLabel: 'Numero',
                                flex: 1,
                                margins: '0 5 0 0',
                                readOnly: true
                            },
                            {
                                xtype: 'datefield',
                                name: 'fecha',
                                fieldLabel: 'Fecha',
                                flex: 1,
                                allowBlank: false,
                                value: new Date()
                            }
                        ]
                    },
                    {
                        xtype: 'trigger',
                        name: 'concepto',
                        fieldLabel: 'Concepto',
                        allowBlank: false,
                        //readOnly: true,
                        triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                        emptyText: 'Seleccione...'
                    },
                    {
                        xtype: 'trigger',
                        name: 'empleado',
                        fieldLabel: 'Empleado',
                        allowBlank: false,
                        triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                        emptyText: 'Seleccione...',
                        onTriggerClick: function () {
                            Ext.widget('search_persona', {
                                title: 'Buscar empleado',
                                height: 400,
                                width: 600,
                                layout: 'fit',
                                closeAction: 'destroy'
                            }).show();
                        }
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'numberfield',
                                name: 'monto',
                                fieldLabel: 'Monto',
                                flex: 1,
                                margins: '0 5 0 0',
                                allowBlank: false,
                                decimalSeparator: '.',
                                fieldStyle: 'text-align:right',
                                minValue: 0
                            },
                            {
                                xtype: 'numberfield',
                                name: 'dividendo',
                                fieldLabel: 'Dividendo',
                                flex: 1,
                                margins: '0 5 0 0',
                                allowBlank: false,
                                allowDecimals: false,
                                fieldStyle: 'text-align:right',
                                minValue: 1,
                                value: 1
                            },
                            {
                                xtype: 'button',
                                icon: perfil.dirImg + 'calcular.png',
                                iconCls: 'btn',
                                text: 'Dividendos',
                                tooltip: 'Calcular dividendos',
                                margin: '17 0 0 0',
                                action: 'calcular_dividendo'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'grid',
                region: 'center',
                title: 'Dividendos',
                itemdId: 'dividendos',
                selType: 'cellmodel',
                plugins: [
                    Ext.create('Ext.grid.plugin.CellEditing', {
                        clicksToEdit: 1
                    })
                ],
                store: Ext.create('Ext.data.Store', {
                    storeId: 'dividendosStore',
                    fields: ['fecha', 'valor']
                }),
                columns: [
                    Ext.create('Ext.grid.RowNumberer'),
                    {
                        text: 'Fecha vencimiento', dataIndex: 'fechavencimiento', flex: 1,
                        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        editor: {
                            xtype: 'datefield',
                            format: 'd/m/Y',
                            allowBlank: false
                        }
                    },
                    {
                        text: 'Valor', dataIndex: 'valor', flex: 1,
                        renderer: me.renderAlignRight,
                        editor: {
                            xtype: 'numberfield',
                            allowBlank: false,
                            decimalSeparator: '.',
                            fieldStyle: 'text-align:right',
                            minValue: 0
                        }
                    }
                ]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.hide
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    },
    renderAlignRight: function (v) {
        return '<div style="text-align:right">' + v + '</div>'
    }
});