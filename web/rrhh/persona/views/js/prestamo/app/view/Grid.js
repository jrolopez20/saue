Ext.define('Prestamo.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_prestamo',

    store: 'Prestamo.store.Prestamo',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.tbar = [
            {
                text: 'Adicionar',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: 'Eliminar',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            },
            {
                text: 'Imprimir',
                icon: perfil.dirImg + 'imprimir.png',
                iconCls: 'btn',
                disabled: true,
                action: 'imprimir'
            }
        ];

        me.columns = [
            {
                text: 'Número',
                dataIndex: 'numero',
                width: 100
            },
            {
                text: 'Fecha',
                dataIndex: 'fecha',
                width: 120
            },
            {
                text: 'Monto',
                dataIndex: 'monto',
                width: 100
            },
            {
                text: 'Empleado',
                dataIndex: 'empleado',
                flex: 1
            },
            {
                text: 'Concepto',
                dataIndex: 'concepto',
                flex: 1
            }
        ];

        this.callParent(arguments);
    }
});