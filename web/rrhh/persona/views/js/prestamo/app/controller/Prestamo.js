Ext.define('Prestamo.controller.Prestamo', {
    extend: 'Ext.app.Controller',

    views: [
        'Prestamo.view.Grid',
        'Prestamo.view.Edit',
        'InventarioPersona.view.persona.Grid',
        'InventarioPersona.view.persona.WinSearchPersona'
    ],
    stores: [
        'Prestamo.store.Prestamo',
        'InventarioPersona.store.Personas'
    ],
    models: [
        'Prestamo.model.Prestamo',
        'InventarioPersona.model.Persona'
    ],
    refs: [
        {ref: 'grid_prestamo', selector: 'grid_prestamo'},
        {ref: 'edit_prestamo', selector: 'edit_prestamo'},
        {ref: 'grid_persona', selector: 'grid_persona'},
        {ref: 'search_persona', selector: 'search_persona'}

    ],
    init: function () {
        var me = this;

        me.control({
            'grid_prestamo button[action=adicionar]': {
                click: me.adicionar
            },
            'grid_prestamo button[action=modificar]': {
                click: me.modificar
            },
            'grid_prestamo button[action=eliminar]': {
                click: me.eliminar
            },
            'grid_prestamo button[action=imprimir]': {
                click: me.imprimir
            },
            'grid_persona': {
                itemdblclick: me.onDblClickEmpleado
            },
            'edit_prestamo button[action=calcular_dividendo]': {
                click: me.calcularDividendos
            }
        });

    },

    onDblClickEmpleado: function (grid, record) {
        var name = record.get('nombre') + ' ' + record.get('apellidos');
        this.getEdit_prestamo().down('form').getForm().setValues({
            empleado: name,
            idpersona: record.get('idpersona')
        });
        grid.up('window').close();
    },

    calcularDividendos: function (btn) {
        var valuesForm = this.getEdit_prestamo().down('form').getForm().getValues();
        var valor = Ext.util.Format.round((valuesForm.monto / valuesForm.dividendo), 2);
        var stDividendo = this.getEdit_prestamo().down('grid').getStore();
        stDividendo.removeAll();
        while (valuesForm.dividendo > 0) {
            stDividendo.add({
                fecha: valuesForm.fecha,
                valor: valor
            });
            --valuesForm.dividendo;
        }
    },

    adicionar: function (btn) {
        this.getEdit_prestamo().show();
        this.getEdit_prestamo().setTitle('Adicionar préstamo');
        this.clearFormPrestamo();
        this.getEdit_prestamo().down('button[action=aplicar]').show();
    },
    
    modificar: function (btn) {
        this.getEdit_prestamo().show();
        this.getEdit_prestamo().setTitle('Modificar préstamo');
        this.clearFormPrestamo();
        var rec = this.getGrid_prestamo().getSelectionModel().getLastSelected();
        this.getEdit_prestamo().getForm().loadRecord(rec);
        this.getEdit_prestamo().down('button[action=aplicar]').hide();
    },
    
    eliminar: function (btn) {
    },

    clearFormPrestamo: function () {
        this.getEdit_prestamo().down('form').getForm().reset();
        this.getEdit_prestamo().down('grid').getStore().removeAll();
    }

});