Ext.define('Prestamo.model.Prestamo', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idprestamo', type: 'int'},
        {name: 'numero', type: 'int'},
        {name: 'idpersona', type: 'int'},
        {name: 'fecha', type: 'date'},
        {name: 'monto', type: 'float'}
    ]
});