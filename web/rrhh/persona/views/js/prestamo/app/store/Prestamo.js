Ext.define('Prestamo.store.Prestamo', {
    extend: 'Ext.data.Store',
    model: 'Prestamo.model.Prestamo',
    pageSize: 25,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'getPrestamos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});