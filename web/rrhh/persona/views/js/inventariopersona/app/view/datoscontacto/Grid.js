Ext.define('InventarioPersona.view.datoscontacto.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_contactos',

    store: 'InventarioPersona.store.DatosContacto',

    selType: 'rowmodel',
    //plugins: [
    //    Ext.create('Ext.grid.plugin.RowEditing', {
    //        pluginId: 'rowEditor_contacto',
    //        clicksToEdit: 2
    //    })
    //],
    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }
        ];

        me.columns = [
            {
                text: 'Medio de contacto',
                dataIndex: 'mediocontacto',
                width: 150
            },
            {
                text: 'Tipo de contacto',
                dataIndex: 'tipocontacto',
                width: 150
            },
            {
                text: 'Contacto',
                dataIndex: 'contacto',
                width: 170
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: 'Eliminar contacto',
                    handler: function (grid, rowIndex) {
                        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el contacto seleccionado?',
                            function (btn) {
                                if (btn == 'yes') {
                                    var myMask = new Ext.LoadMask(grid, {msg: 'Eliminando contacto de la persona...'});
                                    myMask.show();
                                    Ext.Ajax.request({
                                        url: 'eliminarContacto',
                                        method: 'POST',
                                        params: {
                                            idcontacto: grid.getStore().getAt(rowIndex).get('idcontacto')
                                        },
                                        callback: function (options, success, response) {
                                            myMask.hide();
                                            var rpse = Ext.decode(response.responseText);
                                            if (rpse.success) {
                                                grid.getStore().removeAt(rowIndex);
                                            }
                                        }
                                    });
                                }
                            }
                        );
                    }
                }]
            }
        ];

        this.callParent(arguments);
    }
});