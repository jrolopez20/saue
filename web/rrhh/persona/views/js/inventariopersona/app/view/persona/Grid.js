Ext.define('InventarioPersona.view.persona.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_persona',

    store: 'InventarioPersona.store.Personas',
    readOnly: true,
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.tbar = [
            {
                text: 'Adicionar',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                hidden: this.readOnly,
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                hidden: this.readOnly,
                disabled: true,
                action: 'modificar'
            },
            {
                text: 'Eliminar',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                hidden: this.readOnly,
                disabled: true,
                action: 'eliminar'
            },
            '->',
            {
                xtype: 'searchfield',
                store: 'InventarioPersona.store.Personas',
                emptyText: 'Filtrar por cédula, nombre o apellido',
                width: 250,
                maxLength: 50,
                vtype: 'alphanum',
                padding: '0 0 0 5',
                filterPropertysNames: ['text']
            }
        ];

        me.columns = [
            {
                text: 'Cédula',
                dataIndex: 'numid',
                width: 110
            },
            {
                text: 'Nombre',
                dataIndex: 'nombre',
                width: 150
            },
            {
                text: 'Apellidos',
                dataIndex: 'apellidos',
                width: 160
            },
            {
                text: 'Sexo',
                dataIndex: 'sexo',
                width: 50,
                renderer: this.showSexo
            },
            {
                text: 'Estado civil',
                dataIndex: 'estadocivil',
                width: 70
            }
        ];

        this.callParent(arguments);
        this.getStore().load();
    },
    showSexo: function (val) {
        if (val == 0) {
            return '<center><img data-qtip="<b>Femenino</b>" src="/rrhh//persona/views/images/female.png" /></center>';
        } else {
            return '<center><img data-qtip="<b>Maculino</b>" src="/rrhh/persona/views/images/male.png" /></center>';
        }
    }
});