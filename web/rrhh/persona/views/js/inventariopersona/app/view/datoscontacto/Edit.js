Ext.define('InventarioPersona.view.datoscontacto.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_contacto',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    modal: true,
    resizable: false,
    closeAction: 'hide',
    width: 300,
    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'form',
                url: 'saveContacto',
                region: 'center',
                margins: '5 5 5 5',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 70
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idcontacto'
                    },
                    {
                        xtype: 'hidden',
                        name: 'idpersona'
                    },
                    {
                        xtype: 'combobox',
                        name: 'idmediocontacto',
                        fieldLabel: 'Medio contacto',
                        allowBlank: false,
                        displayField: 'denominacion',
                        valueField: 'idmediocontacto',
                        queryMode: 'remote',
                        typeAhead: true,
                        forceSelection: true,
                        store: 'Maestro.store.MedioContacto'
                    },
                    {
                        xtype: 'combobox',
                        name: 'idtipocontacto',
                        fieldLabel: 'Tipo contacto',
                        allowBlank: false,
                        displayField: 'denominacion',
                        valueField: 'idtipocontacto',
                        queryMode: 'remote',
                        typeAhead: true,
                        forceSelection: true,
                        store: 'Maestro.store.TipoContacto'
                    },
                    {
                        xtype:'textfield',
                        name: 'contacto',
                        fieldLabel: 'Contacto',
                        allowBlank: false,
                        maxLength: 255
                    }
                ]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});