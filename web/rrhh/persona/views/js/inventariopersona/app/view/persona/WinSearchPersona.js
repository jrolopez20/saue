Ext.define('InventarioPersona.view.persona.WinSearchPersona', {
    extend: 'Ext.window.Window',
    alias: 'widget.search_persona',
    layout: 'fit',
    modal: true,
    width: 500,
    height: 400,
    initComponent: function () {
        var me = this;

        this.items = [{
            xtype: 'grid_persona'
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: this.close,
                scope: this
            }
        ];

        this.callParent(arguments);

    }
});