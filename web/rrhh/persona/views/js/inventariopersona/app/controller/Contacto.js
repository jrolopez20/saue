Ext.define('InventarioPersona.controller.Contacto', {
    extend: 'Ext.app.Controller',

    views: [
        'InventarioPersona.view.datoscontacto.Grid',
        'InventarioPersona.view.datoscontacto.Edit'
    ],
    stores: [
        'InventarioPersona.store.DatosContacto',
        'Maestro.store.MedioContacto',
        'Maestro.store.TipoContacto'
    ],
    models: [
        'InventarioPersona.model.Contacto',
        'Maestro.model.MedioContacto',
        'Maestro.model.TipoContacto'
    ],
    refs: [
        {ref: 'grid_contactos', selector: 'grid_contactos'},
        {ref: 'edit_contacto', selector: 'edit_contacto'}
    ],

    winEditContact: null,

    init: function () {
        var me = this;

        me.control({
            'grid_contactos': {
                activate: me.onActivateContacto,
                selectionchange: me.onContactoSelectionChange
            },
            'grid_contactos button[action=adicionar]': {
                click: me.adicionarContacto
            },
            'grid_contactos button[action=modificar]': {
                click: me.modificarContacto
            },
            'edit_contacto button[action=aceptar]': {
                click: me.saveContacto
            },
            'edit_contacto button[action=aplicar]': {
                click: me.saveContacto
            }
        });

    },

    onActivateContacto: function (grid) {
        grid.getStore().load({
            params: {
                idpersona: grid.up('window').getComponent('basic_data').getForm().findField('idpersona').getValue()
            }
        });
    },

    onContactoSelectionChange: function (sm) {
        var me = this;

        if (sm.hasSelection()) {
            me.getGrid_contactos().down('button[action=modificar]').enable();
        } else {
            me.getGrid_contactos().down('button[action=modificar]').disable();
        }
    },

    adicionarContacto: function (btn) {
        var me = this;
        if (!me.winEditContact) {
            me.winEditContact = Ext.widget('edit_contacto');
        }
        me.winEditContact.show();
        this._resetEditWindow();

        me.winEditContact.setTitle('Adicionar contacto');
        me.winEditContact.down('button[action=aplicar]').show();
        me.winEditContact.down('form').getForm().findField('idpersona').setValue(
            btn.up('window').getComponent('basic_data').getForm().findField('idpersona').getValue()
        );
    },

    modificarContacto: function (btn) {
        var me = this,
            smContacto = me.getGrid_contactos().getSelectionModel();

        if (smContacto.hasSelection()) {
            if (!me.winEditContact) {
                me.winEditContact = Ext.widget('edit_contacto');
            }
            me.winEditContact.show();
            this._resetEditWindow();

            me.winEditContact.setTitle('Modificar contacto');
            me.winEditContact.down('button[action=aplicar]').hide();

            if (me.getMaestroStoreMedioContactoStore().getTotalCount() == 0) {
                me.getMaestroStoreMedioContactoStore().load();
            }

            if (me.getMaestroStoreTipoContactoStore().getTotalCount() == 0) {
                me.getMaestroStoreTipoContactoStore().load();
            }

            var record = smContacto.getLastSelected();
            me.winEditContact.down('form').getForm().loadRecord(record);
        }
    },

    saveContacto: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando datos de contacto...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getGrid_contactos().getStore().reload();
                    }
                    else {
                        me._resetEditWindow();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    _resetEditWindow: function (win) {
        var form = this.winEditContact.down('form').getForm();
        form.reset();
        form.findField('idpersona').setValue(
            this.getGrid_contactos().up('window').getComponent('basic_data').getForm().findField('idpersona').getValue()
        );
    }
})
;