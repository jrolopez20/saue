Ext.define('InventarioPersona.controller.InventarioPersona', {
    extend: 'Ext.app.Controller',

    views: [
        'InventarioPersona.view.persona.Ficha',
        'InventarioPersona.view.persona.Grid',
        'InventarioPersona.view.persona.Edit',
        'InventarioPersona.view.datoscontacto.Grid',
        'InventarioPersona.view.datoscontacto.Edit'
    ],
    stores: [
        'InventarioPersona.store.Personas',
        'InventarioPersona.store.DatosContacto',
        'InventarioPersona.store.Nacionalidad',
        'Maestro.store.EstadoCivil',
        'Maestro.store.Cargo'
    ],
    models: [
        'InventarioPersona.model.Persona',
        'InventarioPersona.model.Contacto',
        'InventarioPersona.model.Nacionalidad',
        'Maestro.model.EstadoCivil',
        'Maestro.model.Cargo'
    ],
    refs: [
        {ref: 'ficha_persona', selector: 'ficha_persona'},
        {ref: 'grid_persona', selector: 'grid_persona'},
        {ref: 'edit_persona', selector: 'edit_persona'},
        {ref: 'grid_contactos', selector: 'grid_contactos'},
        {ref: 'edit_contacto', selector: 'edit_contacto'}
    ],

    winEditPersona: null,

    init: function () {
        var me = this;

        me.control({
            'grid_persona': {
                selectionchange: me.onPersonaSelectionChange
            },
            'grid_persona button[action=adicionar]': {
                click: me.adicionarPersona
            },
            'grid_persona button[action=modificar]': {
                click: me.modificarPersona
            },
            'grid_persona button[action=eliminar]': {
                click: me.eliminarPersona
            },
            'edit_persona combobox[name=tipodocidentidad]': {
                select: me.onSelectTipoDocId
            },
            'edit_persona button[action=guardar]': {
                click: me.guardarPersona
            },
            'edit_persona button[action=aceptar]': {
                click: me.guardarPersona
            },
            'edit_persona button[action=aplicar]': {
                click: me.guardarPersona
            }
        });

    },

    onSelectTipoDocId: function (cb, selection) {
        cb.up('window').down('textfield[name=numid]').setFieldLabel(selection[0].get('tipodocidentidad'));
    },

    onPersonaSelectionChange: function (sm) {
        var me = this,
            pFicha = me.getFicha_persona(),
            formFicha = pFicha.down('form').getForm(),
            foto = pFicha.down('image[name=foto]');

        me.getGrid_persona().down('button[action=modificar]').disable();
        me.getGrid_persona().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getGrid_persona().down('button[action=modificar]').enable();
            me.getGrid_persona().down('button[action=eliminar]').enable();
            var recPersona = sm.getLastSelected();
            formFicha.loadRecord(recPersona);
            if(recPersona.get('foto')){
                foto.setSrc(recPersona.get('foto'));
            }else{
                foto.setSrc(pFicha.DEFAULT_IMAGE);
            }
        } else {
            formFicha.reset();
            foto.setSrc(pFicha.DEFAULT_IMAGE);
        }
    },

    adicionarPersona: function () {
        var me = this;
        if (!me.winEditPersona) {
            me.winEditPersona = Ext.widget('edit_persona');
        }
        me.winEditPersona.show();


        me.winEditPersona.setTitle('Adicionar persona');
        me.winEditPersona.down('button[action=aplicar]').show();
        me.winEditPersona.down('button[action=guardar]').show();
        me.winEditPersona.down('tabpanel').setVisible(false);
        this._resetEditWindow();
    },

    modificarPersona: function () {
        var me = this,
            smPersona = me.getGrid_persona().getSelectionModel();

        if (smPersona.hasSelection()) {
            if (!me.winEditPersona) {
                me.winEditPersona = Ext.widget('edit_persona');
            }
            me.winEditPersona.show();
            me.winEditPersona.down('tabpanel').setVisible(true);
            me.winEditPersona.down('tabpanel').setActiveTab(0);
            me.winEditPersona.setTitle('Modificar persona');
            me.winEditPersona.down('button[action=aplicar]').hide();
            me.winEditPersona.down('button[action=guardar]').hide();
            this._resetEditWindow();

            if (me.getInventarioPersonaStoreNacionalidadStore().getTotalCount() == 0) {
                me.getInventarioPersonaStoreNacionalidadStore().load();
            }
            if (me.getMaestroStoreEstadoCivilStore().getTotalCount() == 0) {
                me.getMaestroStoreEstadoCivilStore().load();
            }

            var form = me.winEditPersona.down('form[itemId=basic_data]');
            var recPersona = this.getGrid_persona().getSelectionModel().getLastSelected();
            form.loadRecord(recPersona);
            if (recPersona.get('foto')) {
                me.winEditPersona.down('image[name=foto]').setSrc(recPersona.get('foto'));
            }
            this.getInventarioPersonaStoreDatosContactoStore().removeAll();
        }
    },

    guardarPersona: function (btn) {
        var me = this,
            win = btn.up('window'),
            formBasicData = win.getComponent('basic_data').getForm(),
            foto = null,
            datosBasicos = {},
            datosGenerales = {};

        if (formBasicData.isValid()) {
            //    //var myMask = new Ext.LoadMask(grid, {msg: 'Espere por favor ...'});
            //    //myMask.show();
            datosBasicos = formBasicData.getValues();

            //Solo guarda la foto si esta fue modificada
            if(me.getFicha_persona().DEFAULT_IMAGE != win.down('image[name=foto]').src){
                foto = win.down('image[name=foto]').src;
            }

            if (win.down('tabpanel').isVisible()) {
                datosGenerales = win.down('tabpanel').getComponent('general_data').getForm().getValues();
            }
            Ext.Ajax.request({
                url: 'savePersona',
                method: 'POST',
                params: {
                    foto: foto,
                    datosBasicos: Ext.encode(datosBasicos),
                    datosGenerales: Ext.encode(datosGenerales)
                },
                callback: function (options, success, response) {
                    //myMask.hide();
                    var responseData = Ext.decode(response.responseText);
                    formBasicData.findField('idpersona').setValue(responseData.idpersona);
                    win.down('tabpanel').setVisible(true);
                    win.down('button[action=guardar]').setVisible(false);
                }
            });
        }
    },

    eliminarPersona: function(btn){
        var me = this,
            grid = me.getGrid_persona();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar la persona seleccionada?', function (btn) {
                if (btn == 'yes') {
                    var rec = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando persona...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'eliminarPersona',
                        method: 'POST',
                        params: {
                            idpersona: rec.get('idpersona')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.getStore().reload();
                            }
                        }
                    });
                }
            });
        }
    },

    _resetEditWindow: function () {
        var win = this.getEdit_persona();
        win.down('image[name=foto]').setSrc(this.getFicha_persona().DEFAULT_IMAGE);
        win.getComponent('basic_data').getForm().reset();
        win.down('tabpanel').getComponent('general_data').getForm().reset();
    }
});