Ext.define('InventarioPersona.model.Persona', {
    extend: 'Ext.data.Model',

    fields: [
        //Datos basicos de la persona
        {name: 'idpersona', type: 'int'},
        {name: 'foto', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'apellidos', type: 'string'},
        {name: 'sexo', type: 'int'},
        {name: 'tipopersona', type: 'int'},
        {name: 'idestadocivil', type: 'int'},
        {name: 'estadocivil', mapping: 'NomEstadocivil.estadocivil', type: 'string', convert: null},

        //Documento de identificacion
        {name: 'iddocidentidad', mapping: 'DatDocidentidad[0].iddocidentidad', type: 'int'},
        {name: 'numid', mapping: 'DatDocidentidad[0].numid', type: 'string'},
        {name: 'tipodocidentidad', mapping: 'DatDocidentidad[0].tipodocidentidad', type: 'int'},

        //Datos de nacimiento
        {name: 'idnacimiento', mapping: 'DatNacimiento.idnacimiento', type: 'date', convert: null},
        {name: 'fechanacimiento', mapping: 'DatNacimiento.fechanacimiento', type: 'date', convert: null},
        {name: 'lugarnacimiento', mapping: 'DatNacimiento.lugarnacimiento', type: 'date', convert: null},

        //Nacionalidad
        {name: 'idnacionalidad', mapping: 'DatNacionalidad.idnacionalidad', type: 'int'},
        {name: 'nacionalidad', mapping: 'DatNacionalidad.NomPais.nacionalidad', type: 'string'}
    ]
});