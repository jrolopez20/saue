Ext.define('InventarioPersona.model.Contacto', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idcontacto', type: 'int'},
        {name: 'contacto', type: 'string'},
        {name: 'idpersona', type: 'int'},
        {name: 'idtipocontacto', type: 'int'},
        {name: 'tipocontacto', mapping: 'NomTipocontacto.tipocontacto', type: 'string'},
        {name: 'idmediocontacto', type: 'int'},
        {name: 'mediocontacto', mapping: 'NomMediocontacto.mediocontacto', type: 'string'},
    ]
});