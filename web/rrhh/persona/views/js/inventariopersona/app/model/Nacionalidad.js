Ext.define('InventarioPersona.model.Nacionalidad', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idpais', type: 'int'},
        {name: 'nombrepais', type: 'string'},
        {name: 'codigopais', type: 'string'},
        {name: 'siglas', type: 'string'},
        {name: 'nacionalidad', type: 'string'}
    ]
});