Ext.define('InventarioPersona.store.DatosContacto', {
    extend: 'Ext.data.Store',
    model: 'InventarioPersona.model.Contacto',

    proxy: {
        type: 'ajax',
        url: 'getDatosContacto',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});