Ext.define('InventarioPersona.store.Personas', {
    extend: 'Ext.data.Store',
    model: 'InventarioPersona.model.Persona',

    pageSize: 25,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: '/rrhh/persona/index.php/inventariopersona/getPersonas',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});