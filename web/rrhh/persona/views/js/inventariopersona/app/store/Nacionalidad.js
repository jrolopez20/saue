Ext.define('InventarioPersona.store.Nacionalidad', {
    extend: 'Ext.data.Store',
    model: 'InventarioPersona.model.Nacionalidad',

    proxy: {
        type: 'ajax',
        url: 'getNacionalidades',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});