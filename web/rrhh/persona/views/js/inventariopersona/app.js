var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'InventarioPersona',
    enableQuickTips: true,
    paths: {
        'InventarioPersona': '../../views/js/inventariopersona/app',
        'Maestro': '../../views/js/maestro/app'
    },
    controllers: ['InventarioPersona', 'Contacto'],
    launch: function () {
        UCID.portal.cargarEtiquetas('inventariopersona', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'west',
                        xtype: 'ficha_persona',
                        margin: '5 0 5 5',
                        gestionar: false,
                        width: 350,
                        collapsible: true,
                        split: true,
                        minWidth: 250,
                        maxWidth: 450
                    },
                    {
                        region: 'center',
                        xtype: 'grid_persona',
                        readOnly: false,
                        margin: '5 5 5 0'
                    }
                ]
            });
        });
    }
});