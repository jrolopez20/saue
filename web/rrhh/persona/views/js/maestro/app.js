var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Maestro',
    enableQuickTips: true,
    paths: {
        'Maestro': '../../views/js/maestro/app'
    },

    controllers: [
        'Maestro',
        'TipoContacto',
        'MedioContacto',
        'RelacionFamiliar',
        'EstadoCivil',
        'Cargo',
        'Departamento'
    ],

    launch: function () {
        UCID.portal.cargarEtiquetas('maestro', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'west',
                        xtype: 'main_tree',
                        margin: '5 5 5 5',
                        width: 350,
                        collapsible: true
                    },
                    {
                        region: 'center',
                        xtype: 'main_tabpanel',
                        margin: '5 5 5 0'
                    }
                ]
            });
        });
    }
});