Ext.define('Maestro.controller.Departamento', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.departamento.Grid'
    ],
    stores: [
        'Maestro.store.Departamento'
    ],
    models: [
        'Maestro.model.Departamento'
    ],
    refs: [
        {ref: 'grid_departamento', selector: 'grid_departamento'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_departamento button[action=add]': {
                click: me.onAddClick
            }
        });

    },
    onAddClick: function (btn) {
        var rec = new Maestro.model.Departamento({
            denominacion: '',
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_departamento');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});