Ext.define('Maestro.controller.Cargo', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.cargo.Grid'
    ],
    stores: [
        'Maestro.store.Cargo'
    ],
    models: [
        'Maestro.model.Cargo'
    ],
    refs: [
        {ref: 'grid_cargo', selector: 'grid_cargo'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_cargo button[action=add]': {
                click: me.onAddClick
            }
        });

    },
    onAddClick: function (btn) {
        var rec = new Maestro.model.Cargo({
            denominacion: '',
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_cargo');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});