Ext.define('Maestro.controller.MedioContacto', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.mediocontacto.Grid'
    ],
    stores: [
        'Maestro.store.MedioContacto'
    ],
    models: [
        'Maestro.model.MedioContacto'
    ],
    refs: [
        {ref: 'grid_mediocontacto', selector: 'grid_mediocontacto'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_mediocontacto button[action=add]': {
                click: me.onAddClick
            }
        });

    },
    onAddClick: function (btn) {
        var rec = new Maestro.model.MedioContacto({
            denominacion: '',
            numerico: 0,
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_mediocontacto');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});