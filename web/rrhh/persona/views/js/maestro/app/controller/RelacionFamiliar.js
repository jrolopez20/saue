Ext.define('Maestro.controller.RelacionFamiliar', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.relacionfamiliar.Grid'
    ],
    stores: [
        'Maestro.store.RelacionFamiliar'
    ],
    models: [
        'Maestro.model.RelacionFamiliar'
    ],
    refs: [
        {ref: 'grid_relacionfamiliar', selector: 'grid_relacionfamiliar'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_relacionfamiliar button[action=add]': {
                click: me.onAddClick
            }
        });

    },
    onAddClick: function (btn) {
        var rec = new Maestro.model.RelacionFamiliar({
            denominacion: '',
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_relacionfamiliar');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});