Ext.define('Maestro.controller.Maestro', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.main.Tree',
        'Maestro.view.main.TabPanel',
    ],
    stores: [
    ],
    models: [
    ],
    refs: [
        {ref: 'main_tree', selector: 'main_tree'},
        {ref: 'main_tabpanel', selector: 'main_tabpanel'}
    ],
    init: function () {
        var me = this;

        me.control({
            'main_tree': {
                selectionchange: me.onTreeSelectionChange
            }
        });

    },

    onTreeSelectionChange: function(sm, selected) {
        var me = this;
        if (sm.hasSelection()) {
            var tabPanel = me.getMain_tabpanel(),
                p = tabPanel.down(selected[0].raw.alias);
            if(p == null) {
                p = Ext.create(selected[0].raw.clsName, {
                    closable: true
                });
                tabPanel.add(p);
            }
            tabPanel.setActiveTab(p);
        }
    }
});