Ext.define('Maestro.controller.TipoContacto', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.tipocontacto.Grid'
    ],
    stores: [
        'Maestro.store.TipoContacto'
    ],
    models: [
        'Maestro.model.TipoContacto'
    ],
    refs: [
        {ref: 'grid_tipocontacto', selector: 'grid_tipocontacto'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_tipocontacto button[action=add]': {
                click: me.onAddClick
            }
        });

    },
    onAddClick: function (btn) {
        var rec = new Maestro.model.TipoContacto({
            denominacion: '',
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_tipocontacto');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});