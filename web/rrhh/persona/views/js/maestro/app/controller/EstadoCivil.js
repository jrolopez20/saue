Ext.define('Maestro.controller.EstadoCivil', {
    extend: 'Ext.app.Controller',

    views: [
        'Maestro.view.estadocivil.Grid'
    ],
    stores: [
        'Maestro.store.EstadoCivil'
    ],
    models: [
        'Maestro.model.EstadoCivil'
    ],
    refs: [
        {ref: 'grid_estadocivil', selector: 'grid_estadocivil'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_estadocivil button[action=add]': {
                click: me.onAddClick
            }
        });

    },
    onAddClick: function (btn) {
        var rec = new Maestro.model.EstadoCivil({
            denominacion: '',
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_estadocivil');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});