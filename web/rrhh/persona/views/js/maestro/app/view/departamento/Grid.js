Ext.define('Maestro.view.departamento.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_departamento',

    store: 'Maestro.store.Departamento',

    selType: 'rowmodel',
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            pluginId: 'rowEditor_departamento',
            clicksToEdit: 2
        })
    ],

    title: 'Departamentos de empleados',
    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar departamento',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'add'
            }, '->',
            {
                xtype: 'searchfield',
                store: 'Maestro.store.Departamento',
                emptyText: 'Buscar...',
                width: 250,
                filterPropertysNames: ['denominacion']
            }
        ];

        me.columns = [
            {
                header: 'Denominación',
                dataIndex: 'denominacion',
                flex: 1,
                field: {
                    xtype: 'textfield',
                    allowBlank: false,
                    maxLength: 255,
                    minLength: 3
                }
            },
            {
                header: 'Desde',
                dataIndex: 'desde',
                width: 110,
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                field: {
                    xtype: 'datefield',
                    format: 'd/m/Y',
                    allowBlank: false
                }
            },
            {
                header: 'Hasta',
                dataIndex: 'hasta',
                width: 110,
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                field: {
                    xtype: 'datefield',
                    format: 'd/m/Y'
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: 'Eliminar departamento',
                    handler: function (grid, rowIndex) {
                        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el departamento seleccionado?',
                            function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().removeAt(rowIndex);
                                }
                            }
                        );
                    }
                }]
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
        me.store.load();
    }
});