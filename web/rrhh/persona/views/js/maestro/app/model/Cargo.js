Ext.define('Maestro.model.Cargo', {
    extend: 'Ext.data.Model',
    idProperty: 'idcargo',
    fields: [
        {name: 'idcargo', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});