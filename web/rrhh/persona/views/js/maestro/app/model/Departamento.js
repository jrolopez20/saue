Ext.define('Maestro.model.Departamento', {
    extend: 'Ext.data.Model',
    idProperty: 'iddepartamento',
    fields: [
        {name: 'iddepartamento', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});