Ext.define('Maestro.model.TipoContacto', {
    extend: 'Ext.data.Model',
    idProperty: 'idtipocontacto',
    fields: [
        {name: 'idtipocontacto', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});