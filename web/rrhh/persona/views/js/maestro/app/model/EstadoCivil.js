Ext.define('Maestro.model.EstadoCivil', {
    extend: 'Ext.data.Model',
    idProperty: 'idestadocivil',
    fields: [
        {name: 'idestadocivil', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});