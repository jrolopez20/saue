Ext.define('Maestro.model.MedioContacto', {
    extend: 'Ext.data.Model',
    idProperty: 'idmediocontacto',
    fields: [
        {name: 'idmediocontacto', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'numerico', type: 'int'},
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});