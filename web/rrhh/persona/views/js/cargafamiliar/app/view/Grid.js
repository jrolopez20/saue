Ext.define('CargaFamiliar.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_cargafamiliar',

    store: 'CargaFamiliar.store.Familiar',

    selType: 'rowmodel',
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            pluginId: 'rowEditor_familiar',
            clicksToEdit: 2,
            errorSummary: false
        })
    ],

    title: 'Detalle de cargas familiares',
    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar carga familiar',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar',
                disabled: true
            }
        ];

        me.columns = [
            {
                header: 'Código',
                dataIndex: 'codigo',
                width: 90,
                field: {
                    xtype: 'textfield',
                    allowBlank: false,
                    maxLength: 255,
                    minLength: 3
                }
            },
            {
                header: 'Nombre',
                dataIndex: 'nombre',
                flex: 1,
                field: {
                    xtype: 'textfield',
                    allowBlank: false,
                    maxLength: 255,
                    minLength: 3
                }
            },
            {
                header: 'Sexo',
                dataIndex: 'sexo',
                width: 80,
                renderer: this.showSexo,
                field: {
                    xtype: 'combobox',
                    allowBlank: false,
                    displayField: 'sexo',
                    valueField: 'id',
                    queryMode: 'local',
                    typeAhead: true,
                    forceSelection: true,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['id', 'sexo'],
                        data: [
                            {id: 0, sexo: 'Femenino'},
                            {id: 1, sexo: 'Masculino'}
                        ]
                    })
                }
            },
            {
                header: 'Tipo',
                dataIndex: 'relacionfamiliar',
                width: 110,
                field: {
                    xtype: 'combobox',
                    allowBlank: false,
                    displayField: 'denominacion',
                    valueField: 'denominacion',
                    queryMode: 'remote',
                    typeAhead: true,
                    forceSelection: true,
                    store: 'Maestro.store.RelacionFamiliar',
                    listeners: {
                        select: function (cb, records) {
                            var recFamiliar = cb.up('grid').getSelectionModel().getLastSelected();
                            recFamiliar.set('idrelacionfamiliar', records[0].get('idrelacionfamiliar'));
                        }
                    }
                }
            },
            {
                header: 'Fecha nacimiento',
                dataIndex: 'fechanacimiento',
                width: 110,
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                field: {
                    xtype: 'datefield',
                    format: 'd/m/Y'
                }
            },
            {
                header: 'Estudia?',
                dataIndex: 'estudia',
                width: 70,
                stopSelection: false,
                field: {
                    xtype: 'combobox',
                    allowBlank: false,
                    displayField: 'estudia',
                    valueField: 'id',
                    queryMode: 'local',
                    typeAhead: true,
                    forceSelection: true,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['id', 'estudia'],
                        data: [
                            {id: 0, estudia: 'NO'},
                            {id: 1, estudia: 'SI'}
                        ]
                    })
                },
                renderer: function (val) {
                    if (val == 0) {
                        return 'NO'
                    } else {
                        return 'SI'
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: 'Eliminar carga familiar',
                    handler: function (grid, rowIndex) {
                        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el familiar seleccionado?',
                            function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().removeAt(rowIndex);
                                }
                            }
                        );
                    }
                }]
            }
        ];

        this.callParent(arguments);
    },
    showSexo: function (val) {
        if (val == 0) {
            return '<center><img data-qtip="<b>Femenino</b>" src="../../views/images/female.png" /></center>';
        } else {
            return '<center><img data-qtip="<b>Maculino</b>" src="../../views/images/male.png" /></center>';
        }
    }
});