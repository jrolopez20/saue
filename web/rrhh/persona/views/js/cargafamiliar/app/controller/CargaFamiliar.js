Ext.define('CargaFamiliar.controller.CargaFamiliar', {
    extend: 'Ext.app.Controller',

    views: [
        'CargaFamiliar.view.Grid',
        'InventarioPersona.view.persona.Grid'
    ],
    stores: [
        'InventarioPersona.store.Personas',
        'CargaFamiliar.store.Familiar',
        'Maestro.store.RelacionFamiliar'
    ],
    models: [
        'InventarioPersona.model.Persona',
        'CargaFamiliar.model.Familiar',
        'Maestro.model.RelacionFamiliar'
    ],
    refs: [
        {ref: 'grid_persona', selector: 'grid_persona'},
        {ref: 'grid_cargafamiliar', selector: 'grid_cargafamiliar'}

    ],
    init: function () {
        var me = this;

        me.control({
            'grid_persona': {
                selectionchange: me.onPersonaSelectionChange
            },
            'grid_cargafamiliar button[action=adicionar]': {
                click: me.onAddClick
            }
        });

    },

    onPersonaSelectionChange: function (sm, selected) {
        var gridFamiliar = this.getGrid_cargafamiliar()
            stFamiliar = gridFamiliar.getStore();
        if (sm.hasSelection()) {
            gridFamiliar.down('button[action=adicionar]').enable();
            stFamiliar.load({
                params: {
                    idpersona: sm.getLastSelected().get('idpersona')
                }
            });
        } else {
            gridFamiliar.down('button[action=adicionar]').disable();
            stFamiliar.removeAll();
        }
    },

    onAddClick: function (btn) {
        var sm = this.getGrid_persona().getSelectionModel();
        if (sm.hasSelection()) {
            var rec = new CargaFamiliar.model.Familiar({
                idpersona: sm.getLastSelected().get('idpersona'),
                codigo: '',
                nombre: '',
                fechanacimiento: null,
                sexo: '',
                estudia: 0,
                idrelacionfamiliar: null
            });
            var grid = btn.up('grid');
            var rowEditor = grid.getPlugin('rowEditor_familiar');
            rowEditor.cancelEdit();
            grid.getStore().insert(0, rec);
            rowEditor.startEdit(0, 0);
        }
    }

});