Ext.define('CargaFamiliar.model.Familiar', {
    extend: 'Ext.data.Model',
    idProperty: 'idfamiliar',
    fields: [
        {name: 'idfamiliar', type: 'int'},
        {name: 'idpersona', type: 'int'},
        {name: 'codigo', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'idrelacionfamiliar', type: 'int'},
        {name: 'relacionfamiliar', mapping: 'NomRelacionfamiliar.relacionfamiliar', type: 'string'},
        {name: 'sexo', type: 'string'},
        {name: 'fechanacimiento', type: 'date'},
        {name: 'estudia', type: 'int'}
    ],

    validations: [
        {type: 'presence', field: 'codigo'},
        {type: 'length', field: 'codigo', min: 3},
    ]
});