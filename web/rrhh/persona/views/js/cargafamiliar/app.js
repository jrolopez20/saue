var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'CargaFamiliar',
    enableQuickTips: true,
    paths: {
        'CargaFamiliar': '../../..views/js/cargafamiliar/app',
        'InventarioPersona': '../../views/js/inventariopersona/app',
        'Maestro': '../../views/js/maestro/app'
    },
    controllers: ['CargaFamiliar'],
    launch: function () {
        UCID.portal.cargarEtiquetas('cargafamiliar', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'center',
                        xtype: 'grid_persona',
                        margin: '5 0 5 5',
                        title:'Listado de empleados'
                    },
                    {
                        region: 'east',
                        xtype: 'grid_cargafamiliar',
                        title:'Detalles de Cargas familiares',
                        margin: '5 5 5 0',
                        //height: 360,
                        split: true,
                        width: '50%'
                    }
                ]
            });
        });
    }
});