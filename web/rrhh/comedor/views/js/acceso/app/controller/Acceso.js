Ext.define('Acceso.controller.Acceso', {
    extend: 'Ext.app.Controller',

    views: [
        'InventarioPersona.view.persona.Grid',
        'Acceso.view.Grid'
    ],
    stores: [
        'InventarioPersona.store.Personas',
        'Acceso.store.Acceso',
        'Evento.store.Evento'
    ],
    models: [
        'InventarioPersona.model.Persona',
        'Acceso.model.Acceso',
        'Evento.model.Evento'
    ],
    refs: [
        {ref: 'grid_persona', selector: 'grid_persona'},
        {ref: 'grid_accesocomedor', selector: 'grid_accesocomedor'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_persona': {
                selectionchange: me.onPersonaSelectionChange
            },
            'grid_accesocomedor button[action=adicionar]': {
                click: me.onAddClick
            }
        });

    },

    onPersonaSelectionChange: function (sm, selected) {
        var stAcceso = this.getGrid_accesocomedor().getStore();
        if (sm.hasSelection()) {
            stAcceso.load({
                params: {
                    idpersona: sm.getLastSelected().get('idpersona')
                }
            });
        } else {
            stAcceso.removeAll();
        }
    },

    onAddClick: function (btn) {
        var rec = new Acceso.model.Acceso({
            idaccesocomedor: null,
            idpersona: this.getGrid_persona().getSelectionModel().getLastSelected().get('idpersona'),
            ideventocomedor: null,
            desde: new Date(),
            hasta: null
        });
        var grid = btn.up('grid');
        var rowEditor = grid.getPlugin('rowEditor_accesocomedor');
        rowEditor.cancelEdit();
        grid.getStore().insert(0, rec);
        rowEditor.startEdit(0, 0);
    }

});