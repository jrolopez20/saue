Ext.define('Acceso.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_accesocomedor',

    store: 'Acceso.store.Acceso',

    selType: 'rowmodel',
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            pluginId: 'rowEditor_accesocomedor',
            clicksToEdit: 2
        })
    ],

    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar evento',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }
        ];

        me.columns = [
            {
                header: 'Denominación',
                dataIndex: 'denominacion',
                flex: 1,
                field: {
                    xtype: 'combobox',
                    allowBlank: false,
                    displayField: 'denominacion',
                    valueField: 'denominacion',
                    queryMode: 'remote',
                    typeAhead: true,
                    forceSelection: true,
                    store: 'Evento.store.Evento',
                    listeners: {
                        select: function (cb, records) {
                            var rec = cb.up('grid').getSelectionModel().getLastSelected();
                            rec.set('ideventocomedor', records[0].get('ideventocomedor'));
                        }
                    }
                }
            },
            {
                header: 'Desde',
                dataIndex: 'desde',
                width: 110,
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                field: {
                    xtype: 'datefield',
                    format: 'd/m/Y',
                    allowBlank: false
                }
            },
            {
                header: 'Hasta',
                dataIndex: 'hasta',
                width: 110,
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                field: {
                    xtype: 'datefield',
                    format: 'd/m/Y'
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: 'Eliminar acceso',
                    handler: function (grid, rowIndex) {
                        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el acceso seleccionado?',
                            function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().removeAt(rowIndex);
                                }
                            }
                        );
                    }
                }]
            }
        ];

        this.callParent(arguments);
    }
});