Ext.define('Acceso.model.Acceso', {
    extend: 'Ext.data.Model',
    idProperty: 'idaccesocomedor',
    fields: [
        {name: 'idaccesocomedor', type: 'int'},
        {name: 'idpersona', type: 'int'},
        {name: 'ideventocomedor', type: 'int'},
        {name: 'denominacion', mapping: 'NomEventocomedor.denominacion', type: 'string'},
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'presence', field: 'ideventocomedor'},
        {type: 'presence', field: 'desde'}
    ]
});