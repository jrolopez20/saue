var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Acceso',
    enableQuickTips: true,
    paths: {
        'Acceso': '../../views/js/acceso/app',
        'Evento': '../../views/js/evento/app',
        'InventarioPersona': '../../../persona/views/js/inventariopersona/app'
    },
    controllers: ['Acceso'],
    launch: function () {
        UCID.portal.cargarEtiquetas('accesocomedor', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'center',
                        xtype: 'grid_persona',
                        margin: '5 0 5 5',
                        title:'Listado de comedor'
                    },
                    {
                        region: 'east',
                        xtype: 'grid_accesocomedor',
                        title:'Eventos asignados',
                        margin: '5 5 5 0',
                        split: true,
                        width: '50%'
                    }
                ]
            });
        });
    }
});