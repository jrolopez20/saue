var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Evento',
    enableQuickTips: true,
    paths: {
        'Evento': '../../views/js/evento/app'
    },
    controllers: ['Evento'],
    launch: function () {
        UCID.portal.cargarEtiquetas('eventocomedor', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        region: 'center',
                        xtype: 'grid_eventocomedor',
                        margin: '5 0 5 5'
                    },
                    {
                        region: 'east',
                        xtype: 'edit_eventocomedor',
                        collapsible: true,
                        hidden: true,
                        margin: '5 5 5 5',
                        width: 350
                    }
                ]
            });
        });
    }
});