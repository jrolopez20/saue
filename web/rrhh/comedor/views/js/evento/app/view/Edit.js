Ext.define('Evento.view.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.edit_eventocomedor',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    url: 'saveEvento',
    fieldDefaults: {
        msgTarget: 'side',
        anchor: '100%',
        labelWidth: 70
    },
    frame: true,
    bodyPadding: '5 5 0',
    title: 'Adicionar evento',
    initComponent: function () {
        var me = this;

        this.items = [
            {
                xtype: 'hidden',
                name: 'ideventocomedor'
            },
            {
                xtype: 'textfield',
                name: 'denominacion',
                fieldLabel: 'Denominación',
                emptyText: 'Nombre del evento',
                labelAlign: 'top',
                maxLength: 275,
                allowBlank: false
            },
            {
                xtype: 'fieldset',
                title: 'Horario',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    //Lunes
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'lunes',
                                boxLabel: 'Lunes',
                                margin: '0 5 0 0',
                                checked: true,
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'lunes_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'lunes_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, allowBlank: false
                            },
                            {
                                xtype: 'button',
                                icon: perfil.dirImg + 'aplicar.png',
                                iconCls: 'btn',
                                action: 'aplicar_horario',
                                tooltip: 'Aplicar a todos'
                            }
                        ]
                    },
                    //Martes
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'martes',
                                boxLabel: 'Martes',
                                margin: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'martes_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, disabled: true, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'martes_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 27 0 0',
                                width: 95, disabled: true, allowBlank: false
                            }
                        ]
                    },
                    //Miercoles
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'miercoles',
                                boxLabel: 'Miércoles',
                                margin: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'miercoles_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, disabled: true, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'miercoles_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 27 0 0',
                                width: 95, disabled: true, allowBlank: false
                            }
                        ]
                    },
                    //Jueves
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'jueves',
                                boxLabel: 'Jueves',
                                margin: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'jueves_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, disabled: true, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'jueves_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 27 0 0',
                                width: 95, disabled: true, allowBlank: false
                            }
                        ]
                    },
                    //Viernes
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'viernes',
                                boxLabel: 'Viernes',
                                margin: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'viernes_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, disabled: true, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'viernes_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 27 0 0',
                                width: 95, disabled: true, allowBlank: false
                            }
                        ]
                    },
                    //Sabado
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'sabado',
                                boxLabel: 'Sábado',
                                margin: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'sabado_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, disabled: true, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'sabado_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 27 0 0',
                                width: 95, disabled: true, allowBlank: false
                            }
                        ]
                    },
                    //Domingo
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        items: [
                            {
                                xtype: 'checkbox', 'inputValue': 1,
                                name: 'domingo',
                                boxLabel: 'Domingo',
                                margin: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'domingo_inicio',
                                emptyText: 'Desde',
                                increment: 5,
                                margin: '0 5 0 0',
                                width: 95, disabled: true, allowBlank: false
                            },
                            {
                                xtype: 'timefield', format: 'H:i',
                                name: 'domingo_fin',
                                emptyText: 'Hasta',
                                increment: 5,
                                margin: '0 27 0 0',
                                width: 95, disabled: true, allowBlank: false
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.hide
            },
            {
                icon: perfil.dirImg + 'limpiar.png',
                iconCls: 'btn',
                text: 'Limpiar',
                action: 'limpiar'
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});