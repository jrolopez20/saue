Ext.define('Evento.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_eventocomedor',

    store: 'Evento.store.Evento',

    title: 'Eventos del comedor',

    initComponent: function () {
        var me = this;

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar evento',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                tooltip: 'Modificar evento',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'modificar',
                disabled: true
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar evento',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'eliminar',
                disabled: true
            }
        ];

        me.columns = [
            {
                header: 'Denominación',
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        this.callParent(arguments);
        this.getStore().load();
    }
});