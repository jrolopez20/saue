Ext.define('Evento.store.Evento', {
    extend: 'Ext.data.Store',
    model: 'Evento.model.Evento',

    pageSize: 25,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: '../eventocomedor/getEventosComedor',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },
        listeners: {
            exception: function (proxy, response, operation) {
                Ext.MessageBox.show({
                    title: 'REMOTE EXCEPTION',
                    msg: operation.getError(),
                    icon: Ext.MessageBox.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }
    }
});