Ext.define('Evento.model.Evento', {
    extend: 'Ext.data.Model',
    idProperty: 'ideventocomedor',
    fields: [
        {name: 'ideventocomedor', type: 'int'},
        {name: 'denominacion', type: 'string'},

        {name: 'lunes'},
        {name: 'lunes_inicio'},
        {name: 'lunes_fin'},

        {name: 'martes'},
        {name: 'martes_inicio'},
        {name: 'martes_fin'},

        {name: 'miercoles'},
        {name: 'miercoles_inicio'},
        {name: 'miercoles_fin'},

        {name: 'jueves'},
        {name: 'jueves_inicio'},
        {name: 'jueves_fin'},

        {name: 'viernes'},
        {name: 'viernes_inicio'},
        {name: 'viernes_fin'},

        {name: 'sabado'},
        {name: 'sabado_inicio'},
        {name: 'sabado_fin'},

        {name: 'domingo'},
        {name: 'domingo_inicio'},
        {name: 'domingo_fin'}
    ],

    validations: [
        {type: 'presence', field: 'denominacion'},
        {type: 'length', field: 'denominacion', min: 3},
    ]
});