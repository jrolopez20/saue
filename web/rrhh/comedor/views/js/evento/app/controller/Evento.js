Ext.define('Evento.controller.Evento', {
    extend: 'Ext.app.Controller',

    views: [
        'Evento.view.Grid',
        'Evento.view.Edit'
    ],
    stores: [
        'Evento.store.Evento'
    ],
    models: [
        'Evento.model.Evento'
    ],
    refs: [
        {ref: 'grid_eventocomedor', selector: 'grid_eventocomedor'},
        {ref: 'edit_eventocomedor', selector: 'edit_eventocomedor'}
    ],
    init: function () {
        var me = this;

        me.control({
            'grid_eventocomedor': {
                selectionchange: me.onEventoSelectionChange,
                itemdblclick: me.onModClick
            },
            'grid_eventocomedor button[action=adicionar]': {
                click: me.onAddClick
            },
            'grid_eventocomedor button[action=modificar]': {
                click: me.onModClick
            },
            'grid_eventocomedor button[action=eliminar]': {
                click: me.onDelClick
            },
            'edit_eventocomedor': {
                show: me.onShowEditForm
            },
            'edit_eventocomedor button[action=limpiar]': {
                click: me.clearForm
            },
            'edit_eventocomedor button[action=aplicar]': {
                click: me.saveEvento
            },
            'edit_eventocomedor button[action=aceptar]': {
                click: me.saveEvento
            },
            'edit_eventocomedor button[action=aplicar_horario]': {
                click: me.onAplicarHorarioClick
            },
            'edit_eventocomedor fieldset checkbox': {
                change: me.onChangeCheckDate
            }
        });

    },

    onEventoSelectionChange: function (sm) {
        var me = this;
        if (me.getEdit_eventocomedor().isVisible()) {
            me.getEdit_eventocomedor().hide();
        }
        if (sm.hasSelection()) {
            me.getGrid_eventocomedor().down('button[action=modificar]').enable();
            me.getGrid_eventocomedor().down('button[action=eliminar]').enable();
        } else {
            me.getGrid_eventocomedor().down('button[action=modificar]').disable();
            me.getGrid_eventocomedor().down('button[action=eliminar]').disable();
        }
    },

    onShowEditForm: function (p) {
        p.getForm().findField('denominacion').focus();
    },

    onAddClick: function (btn) {
        this.getEdit_eventocomedor().show();
        this.getEdit_eventocomedor().setTitle('Adicionar evento');
        this.clearForm();
        this.getEdit_eventocomedor().down('button[action=aplicar]').show();
    },

    onModClick: function () {
        this.getEdit_eventocomedor().show();
        this.getEdit_eventocomedor().setTitle('Modificar evento');
        this.clearForm();
        var rec = this.getGrid_eventocomedor().getSelectionModel().getLastSelected();
        this.getEdit_eventocomedor().getForm().loadRecord(rec);
        this.getEdit_eventocomedor().down('button[action=aplicar]').hide();
    },

    onDelClick: function(btn){
        var me = this,
            grid = me.getGrid_eventocomedor();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el evento seleccionado?', function (btn) {
                if (btn == 'yes') {
                    var rec = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando evento...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'eliminarEvento',
                        method: 'POST',
                        params: {
                            ideventocomedor: rec.get('ideventocomedor')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.getStore().reload();
                            }
                        }
                    });
                }
            });
        }
    },

    clearForm: function () {
        this.getEdit_eventocomedor().getForm().reset();
    },

    onAplicarHorarioClick: function (btn) {
        var form = btn.up('form'),
            inicio = form.getForm().findField('lunes_inicio').getValue(),
            fin = form.getForm().findField('lunes_fin').getValue();

        var timeFields = Ext.ComponentQuery.query('timefield', form);
        var checkBoxs = Ext.ComponentQuery.query('checkbox', form);

        Ext.each(checkBoxs, function (chkb) {
            chkb.setValue(true);
        });

        Ext.each(timeFields, function (timeField) {
            if (timeField.emptyText == 'Desde') {
                timeField.setValue(inicio);
            } else {
                timeField.setValue(fin);
            }
        });
    },

    onChangeCheckDate: function (checkbox, newValue, oldValue) {
        var timeFields = Ext.ComponentQuery.query('timefield', checkbox.up('fieldcontainer'));
        Ext.each(timeFields, function (timefield) {
            timefield.setDisabled(oldValue);
            timefield.reset();
        });
    },

    saveEvento: function (btn) {
        var me = this,
            form = btn.up('form'),
            basicForm = btn.up('form').getForm();

        if (basicForm.isValid()) {
            var myMask = new Ext.LoadMask(form.up('viewport'), {msg: 'Guardando datos del evento...'});
            myMask.show();
            basicForm.submit({
                submitEmptyText: false,
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        form.hide();
                        me.getGrid_eventocomedor().getStore().reload();
                    }
                    else {
                        me.clearForm();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    }
});