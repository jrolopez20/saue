////------------ Inicializo el singlenton QuickTips ------------////
Ext.QuickTips.init();

var gen, componente;
var cont = 0;
var stpreview, seleccionado;
var ventana = null, iddominio = null;
var FormIndex = null;
var newAccess = false;
var atrib = new Array();
var comp = new Array();
function cargarInterfaz() {
    var perfil = window.parent.UCID.portal.perfil;
    var entrar = Ext.create('Ext.Button', {
        text: '<b>Acceder al Sistema</b>',
        disabled: true,
         icon:  '../../lib/ExtJS/temas/personal4/icons/siguienteinactivo.png',
        iconCls: 'btn',
        handler: function () {
            entrarAlSistema('viejo');

        }
    });
var salir = Ext.create('Ext.Button', {
        text: '<b>Salir</b>',
         icon:  '../../lib/ExtJS/temas/personal4/icons/salir.png',
        iconCls: 'btn',
        handler: function () {
            entrarAlSistema('nuevo');

        }
    });

    ImageModel = Ext.define('ImageModel', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'name'},
            {name: 'fullName'},
            {name: 'url'},
            {name: 'size', type: 'float'},
            {name: 'lastmod', type: 'date', dateFormat: 'timestamp'}
        ]
    });
    stfacultades = Ext.create('Ext.data.Store', {//utilizado para cargar las imagenes
        model: 'ImageModel',
        proxy: {
            type: 'ajax',
            url: 'index.php/index/cargardominio',
            actionMethods: {

                read: 'POST'
            },
            reader: {
                type: 'json',
                root: 'images'
            }
        }

    });
    stfacultades.load({callback:function(records, operation, success){
            if(records.length == 1){
                iddominio = records[0].data.id;
                entrarAlSistema('viejo');
                }
            }
    });
    // stfacultades.load();
    var facultades = Ext.create('Ext.Panel', {//subpanel general
        id: 'images-view',
        frame: true,
        region: 'center',
        width: '65%',
        resizable: false,
        title: 'Estructuras',
        items: Ext.create('Ext.view.View', {
            store: stfacultades,
            tpl: [
                '<tpl for=".">',
                '<div class="dataview-multisort-item">',
                '<div class="thumb-wrap" id="{name}">',
                '<div class="thumb"><img src="{url}" title="{fullName}"></div>',
                '<span class="x-editable">{shortName}</span></div>',
                '</tpl>',
                '<div class="x-clear"></div>'
            ],
            multiSelect: false,
            trackOver: true,
            autoScroll :true,
            overflow:'auto',
            overItemCls: 'x-item-over',
            itemSelector: 'div.thumb-wrap',
            emptyText: 'No hay estructuras',
            renderTo: Ext.getBody(),

            prepareData: function (data) {
                Ext.apply(data, {
                    shortName: Ext.util.Format.ellipsis(data.name, 200),
                    sizeString: Ext.util.Format.fileSize(data.size),
                    dateString: Ext.util.Format.date(data.lastmod, "m/d/Y g:i a")
                });
                return data;
            },
            listeners: {
                onload:function(dv,nodes){
                    iddominio = nodes[0].data.id;
                    entrarAlSistema('viejo');
                },
                selectionchange: function (dv, nodes) {
                    var nombre = nodes[0].data.fullName;
                    iddominio = nodes[0].data.id;
                   // seleccionado = nombre;
                    entrar.enable();
                     // alert(seleccionado);
                    stpreview.load({
                        params: {
                            node: nombre
                        }
                    })

                }
            }
        })
    });
    stpreview = Ext.create('Ext.data.Store', {//utilizado para mostrar la imagen seleccionada
        model: 'ImageModel',
        proxy: {
            type: 'ajax',
            url: 'index.php/index/loadPreview',
            actionMethods: {
                read: 'POST'
            },
            reader: {
                type: 'json',
                root: 'images'
            }
        }
    });
    var preview = Ext.create('Ext.Panel', {//utilizado para mostrar la imagen seleccionada
        // title: 'Selector de interfaz',
        id: 'images-view1',
        frame: true,
        region: 'east',
        collapsible: true,
        collapsed: true,
		hidden:true,
        resizable: false,
        width: '25%',
        items: Ext.create('Ext.view.View', {
            store: stpreview,
            tpl: [
                '<tpl for=".">',
                '<div class="thumb-wrap1" id="{name}">',
                '<div class="thumb1"><img src="{url}" title="{name}"></div>',
                '<span class="x-editable">{name}</span></div>',
                '<div class="x-clear"></div>',
                '</tpl>'
            ],
            multiSelect: false,
            trackOver: true,
            overItemCls: 'x-item-over',
            itemSelector: 'div.thumb-wrap1',
            emptyText: 'No hay imagen',
            prepareData: function (data) {
                Ext.apply(data, {
                    shortName: Ext.util.Format.ellipsis(data.name, 200),
                    sizeString: Ext.util.Format.fileSize(data.size - 20),
                    dateString: Ext.util.Format.date(data.lastmod, "m/d/Y g:i a")
                });
                return data;
            }

        })
    });

    var panel = Ext.create('Ext.Window', {
        title: '<center>Sistema de la Universidad Ecotec</center>',
        layout: 'border',
        items: [facultades, preview],
        bbar: ['->', salir,entrar],
        closable: false,
		draggable: false,
		resizable: false,
        width: '30%',
        height: '40%'
    });
    panel.show();

    var wCfg = 'titlebar=yes,status=yes,resizable=yes,width=' + screen.width + ',height=' + screen.height + ',scrollbars=yes';
    if (document.getElementById('accesodirecto').value != '0') {
        verificarPerfil();
    }
    function entrarAlSistema(acs) {
        Ext.Ajax.request({
            url: 'index.php/index/entraralsistema',
            method: 'POST',
            waitMsg: 'Entrando al sistema Ecotec...',
            params: {
                dominio: iddominio,
                tipoacceso: acs

            },
            success: function (pResp, pOpt) {
                var objJson = Ext.decode(pResp.responseText);
                if (ventana != null) {
                    ventana.close();
                    ventana = null;
                }
                if (objJson.reload == false) {
                    window.location.reload();
                }
                else if (objJson.reload == true) {
                    verificarPerfil();
                }
                else if (objJson.codMsg) {
                    var msg = objJson.mensaje;
                    mensaje = objJson;
                    Ext.Msg.show({
                        title: 'ERROR',
                        msg: msg,
                        buttons: Ext.Msg.OKCANCEL,
                        animateTarget: 'elId',
                        icon: Ext.window.MessageBox.ERROR
                    });

                    //Ext.Msg.alert('ERROR', msg);
//                    mostrarMensaje(mensaje.codMsg, mensaje.mensaje);
                }
            }
        });

    }

    function verificarPerfil() {

        Ext.Ajax.request({
            url: 'index.php/portal/cargarperfil',
            method: 'POST',
            callback: function (options, success, response) {
                if (success) {

                    //Variable de configuracion con el perfil del usuario
                    var perfil = Ext.decode(response.responseText);
                    if (perfil.portal === 'comercial')
                        ventana = window.open('index.php/portal/portal', 'ERP_CUBA');
                    else
                        ventana = window.open('index.php/portal/portal', 'ERP_CUBA', wCfg);
                } else {
                    mostrarMensaje(3, response.responseText);
                }
            }
        });


    }
}
Ext.onReady(cargarInterfaz, this);