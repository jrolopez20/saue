Ext.define('Comprobante.store.Comprobante', {
    extend: 'Ext.data.Store',
    model: 'Comprobante.model.Comprobante',

    autoLoad: true,
    pageSize: 25,
    groupField: 'subsistema',
    sorters: ['subsistema', 'numerocomprobante'],
    proxy: {
        type: 'rest',
        api: {
            read: 'loadComprobantes'
        },
        actionMethods: {
            read: 'POST',
            update: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});