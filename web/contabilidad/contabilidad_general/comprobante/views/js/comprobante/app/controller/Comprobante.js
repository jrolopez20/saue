Ext.define('Comprobante.controller.Comprobante', {
    extend: 'Ext.app.Controller',
    views: [
        'Comprobante.view.Grid',
        'Comprobante.view.Edit',
        'Comprobante.view.PaseList',
        'Comprobante.view.WinCuenta',
        'Comprobante.view.ChartByState'
    ],
    stores: [
        'Comprobante.store.Comprobante',
        'Comprobante.store.Pase',
        'Comprobante.store.TipoDiario',
        'Comprobante.store.Modelo',
        'Comprobante.store.Chart'
    ],
    models: [
        'Comprobante.model.Comprobante',
        'Comprobante.model.Pase',
        'Comprobante.model.TipoDiario',
        'Comprobante.model.Modelo',
        'Comprobante.model.Chart'
    ],
    refs: [
        {ref: 'comprobante_grid', selector: 'comprobante_grid'},
        {ref: 'comprobante_edit', selector: 'comprobante_edit'},
        {ref: 'pase_list', selector: 'pase_list'},
        {ref: 'win_cuenta', selector: 'win_cuenta'},
        {ref: 'win_chartbystate', selector: 'win_chartbystate'}
    ],
    init: function() {
        var me = this;

        me.control({
            'comprobante_grid': {
                selectionchange: me.onComprobanteSelectionChange,
                render: me.onRenderComprobante,
                itemdblclick: me.onComprobanteDblClick
            },
            'comprobante_grid button[action=adicionar]': {
                click: me.addComprobante
            },
            'comprobante_grid button[action=modificar]': {
                click: me.modComprobante
            },
            'comprobante_grid button[action=asentar]': {
                click: me.asentar
            },
            'comprobante_grid button[action=duplicar]': {
                click: me.duplicarComprobante
            },
            'comprobante_grid button[action=anular]': {
                click: me.anularComprobante
            },
            'comprobante_grid button[action=pasarDia]': {
                click: me.pasarDia
            },
            'comprobante_grid button[action=preview]': {
                click: me.showPreview
            },
            'comprobante_grid button[action=showchart]': {
                click: me.showChart
            },
            'pase_list button[action=adicionar]': {
                click: me.addPase
            },
            'pase_list button[action=limpiar]': {
                click: me.delAllPases
            },
            'pase_list': {
                itemdblclick: me.onPaseDblClic
            },
            'comprobante_edit combobox[name=idtipodiario]': {
                select: me.onSelectTipoAsiento
            },
            'comprobante_edit combobox[name=idmodelocontable]': {
                select: me.onSelectModeloContable
            },
            'comprobante_edit button[action=aplicarpases]': {
                click: me.aplicarPases
            },
            'comprobante_edit button[action=aceptar]': {
                click: me.saveComprobante
            },
            'comprobante_edit button[action=aplicar]': {
                click: me.saveComprobante
            },
            'win_cuenta button[action=aceptar]': {
                click: me.onSelectCuenta
            },
            'win_cuenta button[action=aplicar]': {
                click: me.onSelectCuenta
            }
        });

        me.getComprobanteStoreModeloStore().on(
                {
                    beforeload: {fn: me.setearExtraParams, scope: this}
                }
        );

        me.getComprobanteStorePaseStore().on(
                {
                    /*add: {fn: me.onStorePaseChange, scope: this},
                     update: {fn: me.onStorePaseChange, scope: this},
                     remove: {fn: me.onStorePaseChange, scope: this}*/
                    update: {fn: me.onStorePaseChange, scope: this},
                    datachanged: {fn: me.onStorePaseChange, scope: this}
                }
        );

    },
    setearExtraParams: function(store) {
        var extraParams = {
            idtipodiario: this.getComprobante_edit().down('textfield[name=idtipodiario]').getValue()
        };
        store.getProxy().extraParams = extraParams;
    },
    onComprobanteSelectionChange: function(sm) {
        var me = this,
                grid = me.getComprobante_grid();

        grid.down('button[action=modificar]').setVisible(false);
        grid.down('button[action=asentar]').setVisible(false);
        grid.down('button[action=duplicar]').setVisible(false);
        grid.down('button[action=anular]').setVisible(false);
        grid.down('button[action=preview]').setVisible(false);

        if (sm.hasSelection()) {
            if (sm.getCount() == 1) {
                var record = sm.getLastSelected();
                grid.down('button[action=preview]').setVisible(true);

                //Verifica que si el comprobante para anular este ASENTADO  y sea de CONTABILIDAD
                if (record.get('idestadocomprobante') === 8032 && record.get('idsubsistema') === 4) {
                    grid.down('button[action=duplicar]').setVisible(true);
                    grid.down('button[action=anular]').setVisible(true);
                }
                //Verifica que si el comprobante no esta ASENTADO se pueda modificar
                if (record.get('idestadocomprobante') != 8032 && record.get('idsubsistema') == 4) {
                    grid.down('button[action=duplicar]').setVisible(true);
                    grid.down('button[action=modificar]').setVisible(true);
                }
                //Solo se habilita el boton de asentar cuando el comprobante esta SIN ERROR
                if (record.get('idestadocomprobante') == 8030) {
                    grid.down('button[action=asentar]').setVisible(true);
                }
            } else {
                var records = sm.getSelection();

                for (var i = 0; i < records.length; i++) {
                    if (records[i].get('idestadocomprobante') != 8030) {
                        grid.down('button[action=asentar]').setVisible(false);
                        return;
                    }
                }
                grid.down('button[action=asentar]').setVisible(true);
            }

        }
    },
    onRenderComprobante: function(cmp) {
        this.loadFecha(cmp);
    },
    loadFecha: function(cmp) {
        var me = this;
        var myMask = new Ext.LoadMask(me.getComprobante_grid(), {msg: perfil.etiquetas.msgLoadingFechaContable});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDataFecha',
            method: 'POST',
            callback: function(options, success, response) {
                var r = Ext.decode(response.responseText);
                myMask.hide();
                cmp.fechaContable = r.fecha.fecha;
                cmp.inicioPeriodo = r.periodo.inicio;
                cmp.finPeriodo = r.periodo.fin;
                cmp.idperiodo = r.periodo.idperiodo;

                var text = 'Período actual <b>' + r.periodo.nombre +
                        '</b>, desde <b>' + Ext.util.Format.date(r.periodo.inicio, 'd/m/Y') +
                        '</b> hasta <b>' + Ext.util.Format.date(r.periodo.fin, 'd/m/Y') +
                        '</b>.  Fecha contable<b> ' + Ext.util.Format.date(r.fecha.fecha, 'd/m/Y') + '</b>';
                cmp.tiFecha.setText(text);
            }
        });
    },
    onComprobanteDblClick: function(view, record) {
        this.modComprobante();
    },
    onStorePaseChange: function(st, record) {
        var records = st.getRange(),
                debito = 0,
                credito = 0;

        for (var i = 0; i < records.length; i++) {
            debito += records[i].get('debito');
            credito += records[i].get('credito');
        }
        var dif = Math.round(debito, 2) - Math.round(credito, 2);
        if (dif != 0) {
            dif = '<b style="color: red;">' + dif + '</b>';
        } else {
            dif = '<b>' + dif + '</b>';
        }
        this.getPase_list().difDebitoCredito.setText(dif);
    },
    addComprobante: function(btn) {
        var win = Ext.widget('comprobante_edit');
        grid = this.getComprobante_grid();
        win.setTitle('Adicionar comprobante');
        win.down('button[action=aplicar]').show();
        win.down('grid').getStore().removeAll();

        var dfFechaEmision = win.down('datefield[name=fechaemision]');
        dfFechaEmision.setValue(grid.fechaContable);
        dfFechaEmision.setMinValue(grid.inicioPeriodo);
        dfFechaEmision.setMaxValue(grid.finPeriodo);
    },
    modComprobante: function(btn) {
        var record = this.getComprobante_grid().getSelectionModel().getLastSelected();
        if (record && record.get('idestadocomprobante') != 8032 && record.get('idsubsistema') == 4) {
            var win = Ext.widget('comprobante_edit');
            var myMask = new Ext.LoadMask(win, {msg: perfil.etiquetas.msgLoadinPases});
            myMask.show();

            win.setTitle('Modificar comprobante');
            win.down('button[action=aplicar]').hide();
            var stPases = win.down('grid').getStore();

            var form = win.down('form');

            Ext.Ajax.request({
                url: 'loadDataComprobante',
                method: 'POST',
                params: {
                    idcomprobante: record.get('idcomprobante')
                },
                callback: function(options, success, response) {
                    var r = Ext.decode(response.responseText);

                    stPases.removeAll();
                    form.loadRecord(record);

                    var totalPases = r.datos.length;
                    if (totalPases > 0) {
                        for (var i = 0; i < totalPases; i++) {
                            stPases.add({
                                idpase: r.datos[i].idpase,
                                codigodocumento: r.datos[i].codigodocumento,
                                idcuenta: r.datos[i].idcuenta,
                                codigo: r.datos[i].concatcta,
                                cuenta: r.datos[i].denominacion,
                                debito: r.datos[i].importe < 0 ? Math.abs(r.datos[i].importe) : 0,
                                credito: r.datos[i].importe > 0 ? Math.abs(r.datos[i].importe) : 0
                            });
                        }
                    }

                    myMask.hide();
                }
            });
        }
    },
    asentar: function(btn) {
        var me = this;
        if (me.getComprobante_grid().getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmAsentar, function(btn) {
                if (btn == 'yes') {
                    var records = me.getComprobante_grid().getSelectionModel().getSelection();
                    var idComprobantes = new Array();
                    for (var i = 0; i < records.length; i++) {
                        //Verifica que el estado del comprobante sea SIN ERROR.
                        if (records[i].get('idestadocomprobante') == 8030) {
                            idComprobantes.push(records[i].get('idcomprobante'));
                        }
                    }

                    var myMask = new Ext.LoadMask(me.getComprobante_grid(), {msg: perfil.etiquetas.msgAsentandoComprobte});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'asentarComprobante',
                        method: 'POST',
                        params: {
                            idcomprobantes: Ext.encode(idComprobantes)
                        },
                        callback: function(options, success, response) {
                            var r = Ext.decode(response.responseText);
                            myMask.hide();
                            me.getComprobante_grid().getStore().reload();
                        }
                    });
                }
            });
        }
    },
    duplicarComprobante: function(btn) {
        var record = this.getComprobante_grid().getSelectionModel().getLastSelected();
        if (record && record.get('idsubsistema') === 4) {
            var win = Ext.widget('comprobante_edit');
            var myMask = new Ext.LoadMask(win, {msg: perfil.etiquetas.msgLoadinPases});
            myMask.show();

            win.setTitle('Duplicar comprobante');
            win.down('button[action=aplicar]').hide();
            var stPases = win.down('grid').getStore();

            var form = win.down('form');

            Ext.Ajax.request({
                url: 'loadDataComprobante',
                method: 'POST',
                params: {
                    idcomprobante: record.get('idcomprobante')
                },
                callback: function(options, success, response) {
                    var r = Ext.decode(response.responseText);

                    stPases.removeAll();
                    form.loadRecord(record);
                    form.items.items[0].setValue(null);

                    var totalPases = r.datos.length;
                    if (totalPases > 0) {
                        for (var i = 0; i < totalPases; i++) {
                            stPases.add({
                                idpase: r.datos[i].idpase,
                                codigodocumento: r.datos[i].codigodocumento,
                                idcuenta: r.datos[i].idcuenta,
                                codigo: r.datos[i].concatcta,
                                cuenta: r.datos[i].denominacion,
                                debito: r.datos[i].importe < 0 ? Math.abs(r.datos[i].importe) : 0,
                                credito: r.datos[i].importe > 0 ? Math.abs(r.datos[i].importe) : 0
                            });
                        }
                    }

                    myMask.hide();
                }
            });
        }
    },
    anularComprobante: function(btn) {
        var record = this.getComprobante_grid().getSelectionModel().getLastSelected();
        if (record && record.get('idsubsistema') === 4) {
            var win = Ext.widget('comprobante_edit');
            var myMask = new Ext.LoadMask(win, {msg: perfil.etiquetas.msgLoadinPases});
            myMask.show();

            win.setTitle('Anular comprobante');
            win.down('button[action=aplicar]').hide();
            var stPases = win.down('grid').getStore();

            var form = win.down('form');

            Ext.Ajax.request({
                url: 'loadDataComprobante',
                method: 'POST',
                params: {
                    idcomprobante: record.get('idcomprobante')
                },
                callback: function(options, success, response) {
                    var r = Ext.decode(response.responseText);

                    stPases.removeAll();
                    form.loadRecord(record);
                    form.items.items[0].setValue(null);

                    var totalPases = r.datos.length;
                    if (totalPases > 0) {
                        for (var i = 0; i < totalPases; i++) {
                            stPases.add({
                                idpase: r.datos[i].idpase,
                                codigodocumento: r.datos[i].codigodocumento,
                                idcuenta: r.datos[i].idcuenta,
                                codigo: r.datos[i].concatcta,
                                cuenta: r.datos[i].denominacion,
                                debito: r.datos[i].importe > 0 ? Math.abs(r.datos[i].importe) : 0, //aqui se invierte el pase ordinario
                                credito: r.datos[i].importe < 0 ? Math.abs(r.datos[i].importe) : 0//aqui se invierte el pase ordinario
                            });
                        }
                    }

                    myMask.hide();
                }
            });
        }
    },
    pasarDia: function(btn) {
        var me = this;

        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmPasarDia, function(btnConfirm) {
            if (btnConfirm == 'yes') {
                var myMask = new Ext.LoadMask(me.getComprobante_grid(), {msg: perfil.etiquetas.msgDiaSgte});
                myMask.show();
                Ext.Ajax.request({
                    url: 'nextDate',
                    method: 'POST',
                    callback: function(options, success, response) {
                        var r = Ext.decode(response.responseText);
                        myMask.hide();
                        me.loadFecha(btn.up('grid'));
                    }
                });
            }
        });
    },
    showPreview: function(btn) {
        var me = this,
                grid = me.getComprobante_grid();

        if (grid.getSelectionModel().hasSelection()) {
            var record = grid.getSelectionModel().getLastSelected();
            var myMask = new Ext.LoadMask(grid, {msg: perfil.etiquetas.msgLoadingDataReport});
            myMask.show();
            Ext.Ajax.request({
                url: 'vistaPreviaComprobante',
                method: 'POST',
                params: {
                    idcomprobante: record.get('idcomprobante')
                },
                callback: function(options, success, response) {
                    var r = Ext.decode(response.responseText);
                    var arrPost = {
                        datoGeneral: r.datoGeneral,
                        datoCuerpo: r.dataSources
                    };

                    myMask.hide();
                    var win = Ext.widget('printview', {
                        dataSources: arrPost
                    });
                }
            });
        }
    },
    showChart: function(btn) {
        var win = Ext.widget('win_chartbystate');
    },
    addPase: function(btn) {
        var win = Ext.widget('win_cuenta', {
            origen: 'add',
            checked: true
        });
        win.setTitle('Seleccione una cuenta para adicionarla como pase');
        win.down('button[action=aplicar]').show();
    },
    onPaseDblClic: function(view, record, item, index, e) {
        var win = Ext.widget('win_cuenta', {
            origen: 'mod',
            checked: false
        });
        win.setTitle('Seleccione una cuenta para modificar el pase');
        win.down('button[action=aplicar]').hide();
    },
    delAllPases: function(btn) {
        btn.up('grid').getStore().removeAll();
    },
    onSelectTipoAsiento: function(cb, records) {
        var cbModeloContable = cb.up('form').down('combobox[name=idmodelocontable]');
        cbModeloContable.enable();
        cbModeloContable.getStore().load({
            params: {
                idtipodiario: cb.getValue()
            }
        });
        cb.up('form').down('hidden[name=idsubsistema]').setValue(records[0].get('idsubsistema'));
        cb.up('form').down('button[action=aplicarpases]').disable();
        cb.up('form').down('textfield[name=codigodocumento]').disable();
    },
    onSelectModeloContable: function(cb, records) {
        cb.up('form').down('button[action=aplicarpases]').enable();
        cb.up('form').down('textfield[name=codigodocumento]').enable();
    },
    aplicarPases: function(btn) {
        var cbModelo = btn.up('form').down('combobox[name=idmodelocontable]'),
                stPases = btn.up('window').down('grid').getStore(),
                allPases = stPases.getRange();
        var record = cbModelo.getStore().findRecord('idmodelocontable', cbModelo.getValue()),
                newPases = record.get('pases');
        if (Ext.isArray(newPases)) {
            //Recorre los nuevos pases para adicionarlos al Store
            Ext.each(newPases, function(newPase) {
                var codDoc = btn.up('form').down('textfield[name=codigodocumento]').getValue();
                var totalPases = allPases.length;
                //Verifica la cuenta no exista con un mismo numero de documento
                for (var i = 0; i < totalPases; i++) {
                    if (newPase.idcuenta == allPases[i].get('idcuenta') && allPases[i].get('codigodocumento') == codDoc) {
                        return;
                    }
                }

                stPases.add({
                    idcuenta: newPase.idcuenta,
                    codigo: newPase.concatcta,
                    cuenta: newPase.denominacion,
                    debito: newPase.debitocredito == 0 ? 0.00 : null,
                    credito: newPase.debitocredito == 1 ? 0.00 : null,
                    codigodocumento: codDoc
                });
            });
        }

    },
    saveComprobante: function(btn) {
        var me = this,
                win = btn.up('window'),
                form = win.down('form').getForm(),
                stPases = win.down('grid').getStore(),
                gpComprobante = this.getComprobante_grid();

        if (form.isValid() && stPases.getCount() > 0) {
            var pases = new Array();
            var records = stPases.getRange();
            var flagError = false,
                    sumDebito = 0,
                    sumCredito = 0,
                    idestadocomprobante = '';

            var flagCodDoc = Ext.isEmpty(records[0].get('codigodocumento'));
            Ext.each(records, function(record) {
                if (record.get('debito') == 0 && record.get('credito') == 0) {
                    flagError = true;
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'Existen pases incompletos.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR
                    });
                    return;
                }

                //Valida que todos tengan codigo de documentos o todos lo tengan vacios
                if (flagCodDoc != Ext.isEmpty(record.get('codigodocumento'))) {
                    flagError = true;
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'Existen pases que no poseen No. de documento configurados.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR
                    });
                    return;
                }

                sumDebito += record.get('debito');
                sumCredito += record.get('credito');

                pases.push({
                    idpase: record.get('idpase'),
                    codigodocumento: record.get('codigodocumento'),
                    idcuenta: record.get('idcuenta'),
                    debito: record.get('debito'),
                    credito: record.get('credito')
                });
            });

            if (flagError) {
                return;
            }

            if (sumDebito == 0 || sumCredito == 0) {
                Ext.Msg.show({
                    title: 'Error',
                    msg: perfil.etiquetas.msgCompteNoAjustado,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
                return;
            }

            //Determina el estado del comprobante
            if (sumDebito == sumCredito) {
                //SIN ERROR
                idestadocomprobante = 8030;
            } else {
                //CON ERROR
                idestadocomprobante = 8033;
            }

            var myMask = new Ext.LoadMask(win, {msg: 'Guardando comprobante...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                params: {
                    idestadocomprobante: idestadocomprobante,
                    idperiodo: gpComprobante.idperiodo,
                    pases: Ext.encode(pases)
                },
                success: function(response) {
                    myMask.hide();
                    if (btn.action == 'aceptar') {
                        me.getComprobante_grid().getStore().load();
                        win.close();
                    }
                    else {
                        form.reset();
                        stPases.removeAll();
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    },
    onSelectCuenta: function(btn) {
        var me = this,
                win = btn.up('window'),
                tree = win.down('treepanel'),
                form = win.down('form').getForm(),
                formValues = form.getValues();

        if (win.origen == 'add') {
            var totalCuentas = tree.getChecked().length;
            if (totalCuentas > 0) {
                var records = tree.getChecked();

                for (var i = 0; i < totalCuentas; i++) {
                    //Verifica que la cuenta no exista en el listado de pases
                    if (me.checkCuentaOnPases(records[i].get('idcuenta'), formValues.codigodocumento) == false) {
                        this.getPase_list().getStore().insert(0, {
                            idcuenta: records[i].get('idcuenta'),
                            codigo: records[i].get('concatcta'),
                            cuenta: records[i].get('denominacion'),
                            codigodocumento: formValues.codigodocumento,
                            debito: formValues.debitocredito == 0 ? formValues.importe : 0,
                            credito: formValues.debitocredito == 1 ? formValues.importe : 0
                        });
                    }
                }
            }
        } else {
            if (tree.getSelectionModel().hasSelection()) {
                var selectedPase = this.getPase_list().getSelectionModel().getLastSelected();
                var cuenta = tree.getSelectionModel().getLastSelected();
                //Verifica que la cuenta no exista en el listado de pases
                //if (me.checkCuentaOnPases(record.get('idcuenta'), formValues.codigodocumento) == false) {
                if (me.checkCuentaOnPases(cuenta.get('idcuenta'), formValues.codigodocumento) == false) {
                    selectedPase.set('idcuenta', cuenta.get('idcuenta'));
                    selectedPase.set('codigo', cuenta.get('concatcta'));
                    selectedPase.set('cuenta', cuenta.get('denominacion'));
                    selectedPase.set('codigodocumento', formValues.codigodocumento);
                    selectedPase.set('debito', formValues.debitocredito == 0 ? formValues.importe : 0);
                    selectedPase.set('credito', formValues.debitocredito == 1 ? formValues.importe : 0);
                }
            }
        }

        this.getPase_list().getStore().sort('debito', 'DESC');

        if (btn.action === 'aceptar') {
            win.close();
        }
        else {
            tree.getSelectionModel().deselectAll();
            form.reset();
        }
    },
    /**
     * Verifica que si una cuenta existe en el listado de pases
     * @param idcuenta
     */
    checkCuentaOnPases: function(idcuenta, codigodocumento) {
        var allPases = this.getPase_list().getStore().getRange();
        var totalPases = allPases.length;
        for (var i = 0; i < totalPases; i++) {
            if (idcuenta == allPases[i].get('idcuenta') && codigodocumento == allPases[i].get('codigodocumento')) {
                Ext.Msg.show({
                    title: 'Alerta',
                    msg: 'La cuenta que desea agregar ya existe.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
                return true;
            }
        }
        return false;
    }

});