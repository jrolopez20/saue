Ext.define('Comprobante.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.comprobante_grid',
    store: 'Comprobante.store.Comprobante',
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel', {
            mode: 'MULTI'
        });

        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{columnName}: {name} ({rows.length} Comprobante{[values.rows.length > 1 ? "s" : ""]})',
            hideGroupedHeader: true
        });

        me.tiFecha = Ext.create('Ext.toolbar.TextItem', {
            text: ''
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdd,
                tooltip: perfil.etiquetas.ttpBtnAdd,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnMod,
                tooltip: perfil.etiquetas.ttpBtnMod,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                hidden: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnAsentar,
                tooltip: perfil.etiquetas.ttpBtnAsentar,
                icon: perfil.dirImg + 'asentar.png',
                iconCls: 'btn',
                hidden: true,
                action: 'asentar'
            },
            {
                text: perfil.etiquetas.lbBtnDuplicar,
                tooltip: perfil.etiquetas.ttpBtnDuplicar,
                icon: perfil.dirImg + 'duplicar.png',
                iconCls: 'btn',
                hidden: true,
                action: 'duplicar'
            },{
                text: perfil.etiquetas.lbBtnAnular,
                tooltip: perfil.etiquetas.ttpBtnAnular,
                icon: perfil.dirImg + 'anular.png',
                iconCls: 'btn',
                hidden: true,
                action: 'anular'
            },
            {
                text: perfil.etiquetas.lbBtnPreview,
                tooltip: perfil.etiquetas.ttpBtnPreview,
                icon: perfil.dirImg + 'ver.png',
                iconCls: 'btn',
                hidden: true,
                action: 'preview'
            },
            {
                text: perfil.etiquetas.lbBtnChart,
                tooltip: perfil.etiquetas.ttpBtnChart,
                icon: perfil.dirImg + 'ajustar.png',
                iconCls: 'btn',
                action: 'showchart'
            },
            {
                text: perfil.etiquetas.lbBtnPasarDia,
                tooltip: perfil.etiquetas.ttpBtnPasarDia,
                icon: perfil.dirImg + 'calendario.png',
                iconCls: 'btn',
                action: 'pasarDia'
            },
            '->', me.tiFecha, '-',
            {
                xtype: 'searchfield',
                store: 'Comprobante.store.Comprobante',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['numerocomprobante', 'referencia']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbNumero,
                tooltip: perfil.etiquetas.ttpColNoCompte,
                dataIndex: 'numerocomprobante',
                width: 80
            },
            {
                header: perfil.etiquetas.lbReferencia,
                tooltip: perfil.etiquetas.ttpColRefCompte,
                dataIndex: 'referencia',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbEstado,
                tooltip: perfil.etiquetas.ttpColEstado,
                dataIndex: 'estado',
                width: 100,
                renderer: me.showStatus
            },
            {
                header: perfil.etiquetas.lbFecha,
                tooltip: perfil.etiquetas.ttpColFechaEmision,
                dataIndex: 'fechaemision',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 90
            },
            {
                header: perfil.etiquetas.lbUsuario,
                tooltip: perfil.etiquetas.ttpColUsuarioCompte,
                dataIndex: 'usuario',
                width: 100
            },
            {
                header: perfil.etiquetas.lbDetalle,
                tooltip: perfil.etiquetas.ttpColDetalleCompte,
                dataIndex: 'detalle',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbSubsistema,
                dataIndex: 'subsistema'
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.plugins = [cellEditing];

        me.features = [groupingFeature];

        this.callParent(arguments);
    },

    showStatus: function (value, metaData, record) {
        if (record.get('idestadocomprobante') == 8033) {
            return '<b style="color: red;">' + record.get('estado') + '</b>';
        } else {
            return value;
        }
    }
});