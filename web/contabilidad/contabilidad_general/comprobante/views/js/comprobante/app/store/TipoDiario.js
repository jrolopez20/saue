Ext.define('Comprobante.store.TipoDiario', {
    extend: 'Ext.data.Store',
    model: 'Comprobante.model.TipoDiario',

    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'loadDataTipoAsiento'
        },
        actionMethods: {
            read: 'POST',
            update: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});