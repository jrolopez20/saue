Ext.define('Comprobante.model.Pase', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idpase', type: 'int'},
        {name: 'idcuenta', type: 'int'},
        {name: 'codigodocumento', type: 'string'},
        {name: 'codigo', type: 'string'},
        {name: 'cuenta', type: 'string'},
        {name: 'debito', type: 'float'},
        {name: 'credito', type: 'float'}
    ]
});