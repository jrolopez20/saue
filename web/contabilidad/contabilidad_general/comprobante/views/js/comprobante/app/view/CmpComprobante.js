Ext.define('CmpComprobante', {
    extend: 'Ext.window.Window',
    alias: 'widget.comprobante',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    constrain: true,
    width: 870,
    height: 580,
    readOnly: false,
    initComponent: function () {
        var me = this;

        this.stTipoDiario = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                api: {
                    read: '/contabilidad/contabilidad_general/comprobante/index.php/comprobante/loadDataTipoAsiento'
                },
                reader: {
                    root: 'datos',
                    totalProperty: 'total',
                    successProperty: 'success',
                    messageProperty: 'mensaje'
                },
                actionMethods: {
                    read: 'POST'
                }
            },
            fields: [
                {name: 'idtipodiario', type: 'int'},
                {name: 'denominacion', type: 'string'},
                {name: 'subsistema', type: 'string'},
                {name: 'idsubsistema', type: 'string'},
                {name: 'descripcion', type: 'string'}
            ]
        });

        this.stModelo = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                api: {
                    read: '/contabilidad/contabilidad_general/comprobante/index.php/comprobante/loadDataModelo'
                },
                reader: {
                    root: 'datos',
                    totalProperty: 'total',
                    successProperty: 'success',
                    messageProperty: 'mensaje'
                },
                actionMethods: {
                    read: 'POST'
                }
            },
            fields: [
                {name: 'idmodelocontable', type: 'int'},
                {name: 'denominacion', type: 'string'},
                {name: 'pases'}
            ]
        });

        this.stPases = Ext.create('Ext.data.Store', {
            fields: [
                {name: 'idpase', type: 'int'},
                {name: 'idcuenta', type: 'int'},
                {name: 'codigodocumento', type: 'string'},
                {name: 'codigo', type: 'string'},
                {name: 'concatcta', type: 'string'},
                {name: 'denominacion', type: 'string'},
                {name: 'debito', type: 'float'},
                {name: 'credito', type: 'float'},
                {name: 'readOnly', type: 'boolean'}
            ],
            sorters: [{property: 'debito',  direction: 'DESC' }],
            listeners: {
                update: function(st){
                    st.sort('debito', 'DESC');
                }
            }
        });

        this.cellEditingPase = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        this.items = [{
            xtype: 'form',
            region: 'north',
            margins: '5 5 0 5',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            frame: true,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 100
            },
            items: [
                {
                    xtype: 'hidden',
                    name: 'idsubsistema'
                },
                {
                    xtype: 'hidden',
                    name: 'idperiodo'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    items: [
                        {
                            name: 'referencia',
                            flex: 1,
                            fieldLabel: 'Referencia',
                            maxLength: 255
                        }, {
                            xtype: 'datefield',
                            width: 180,
                            name: 'fechaemision',
                            fieldLabel: 'Fecha',
                            readOnly: true,
                            margins: '0 0 0 5'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Detalle',
                    name: 'detalle'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'idtipodiario',
                            fieldLabel: 'Tipo de asiento',
                            store: this.stTipoDiario,
                            valueField: 'idtipodiario',
                            displayField: 'denominacion',
                            editable: false,
                            emptyText: 'Seleccione...',
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            flex: 1,
                            hidden: me.readOnly,
                            listeners: {
                                select: function (cb, records) {
                                    var cbModeloContable = cb.up('form').down('combobox[name=idmodelocontable]');
                                    cbModeloContable.enable();
                                    cbModeloContable.getStore().getProxy().extraParams = {
                                        idtipodiario: cb.getValue()
                                    };
                                    cbModeloContable.getStore().load();
                                    cb.up('form').down('hidden[name=idsubsistema]').setValue(records[0].get('idsubsistema'));
                                    cb.up('form').down('button[action=aplicarpases]').disable();
                                    cb.up('form').down('textfield[name=codigodocumento]').disable();
                                }
                            }
                        },
                        {
                            xtype: 'combobox',
                            name: 'idmodelocontable',
                            fieldLabel: 'Modelo',
                            margins: '0 0 0 5',
                            store: this.stModelo,
                            valueField: 'idmodelocontable',
                            displayField: 'denominacion',
                            flex: 1,
                            editable: false,
                            emptyText: 'Seleccione...',
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            disabled: true,
                            hidden: me.readOnly,
                            listeners: {
                                select: function (cb, records) {
                                    cb.up('form').down('button[action=aplicarpases]').enable();
                                    cb.up('form').down('textfield[name=codigodocumento]').enable();
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'codigodocumento',
                            fieldLabel: 'No. documento',
                            margins: '0 5 0 5',
                            width: 110,
                            hidden: me.readOnly,
                            disabled: true
                        },
                        {
                            xtype: 'button',
                            text: 'Aplicar',
                            icon: perfil.dirImg + 'aplicar.png',
                            iconCls: 'btn',
                            iconAlign: 'top',
                            disabled: true,
                            hidden: me.readOnly,
                            action: 'aplicarpases',
                            handler: me.aplicarPases
                        }
                    ]
                }
            ]
        }, {
            xtype: 'grid',
            itemId: 'grid_pases',
            region: 'center',
            margins: '5 5 5 5',
            tbar: [
                {
                    text: 'Adicionar',
                    tooltip: 'Adicionar pase',
                    icon: perfil.dirImg + 'adicionar.png',
                    iconCls: 'btn',
                    hidden: me.readOnly,
                    action: 'adicionar',
                    handler: function (btn) {
                        me.addPase(btn).bind(me);
                    }
                },
                {
                    text: 'Limpiar',
                    tooltip: 'Eliminar todos los pases',
                    icon: perfil.dirImg + 'limpiar.png',
                    iconCls: 'btn',
                    hidden: me.readOnly,
                    action: 'limpiar',
                    handler: this.delAllPases
                }
            ],
            store: this.stPases,
            columns: [
                {
                    header: 'Número documento',
                    //tooltip: perfil.etiquetas.ttpColNoDoc,
                    dataIndex: 'codigodocumento',
                    width: 120,
                    editor: {
                        readOnly: me.readOnly,
                        xtype: 'textfield',
                        maxLength: 255
                    }
                },
                {
                    header: 'Código',
                    //tooltip: perfil.etiquetas.ttpColCodigo,
                    dataIndex: 'concatcta',
                    width: 120
                },
                {
                    header: 'Cuenta',
                    //tooltip: perfil.etiquetas.ttpColCuenta,
                    dataIndex: 'denominacion',
                    flex: 1
                },
                {
                    header: 'Débito',
                    dataIndex: 'debito',
                    width: 90,
                    renderer: me.formatMoney,
                    summaryRenderer: me.formatSummaryMoney,
                    summaryType: 'sum',
                    sortable: true,
                    field: {
                        xtype: 'numberfield',
                        minValue: 0,
                        decimalSeparator: '.',
                        readOnly: me.readOnly,
                        listeners: {
                            change: function (f, newValue, oldValue, eOpts) {
                                var rec = f.up('grid').getSelectionModel().getLastSelected();
                                rec.set('credito', 0);
                            }
                        }
                    }
                },
                {
                    header: 'Crédito',
                    dataIndex: 'credito',
                    width: 90,
                    renderer: me.formatMoney,
                    summaryRenderer: me.formatSummaryMoney,
                    summaryType: 'sum',
                    sortable: true,
                    field: {
                        xtype: 'numberfield',
                        minValue: 0,
                        decimalSeparator: '.',
                        readOnly: me.readOnly,
                        listeners: {
                            change: function (f, newValue, oldValue, eOpts) {
                                var rec = f.up('grid').getSelectionModel().getLastSelected();
                                rec.set('debito', 0);
                            }
                        }
                    }
                },
                {
                    xtype: 'actioncolumn',
                    width: 30,
                    sortable: false,
                    menuDisabled: true,
                    hidden: me.readOnly,
                    hideable: false,
                    items: [{
                        icon: perfil.dirImg + 'eliminar.png',
                        iconCls: 'btn',
                        tooltip: 'Eliminar',
                        handler: function (grid, rowIndex, colIndex) {
                            grid.getStore().removeAt(rowIndex);
                        }
                    }]
                }
            ],
            features: [{
                ftype: 'summary'
            }],
            plugins: [this.cellEditingPase],
            height: 200,
            width: 400
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    },
    formatMoney: function (v) {
        if (isNaN(v)) {
            return '';
        } else {
            return '<div style="text-align: right;">' + formatoMoneda(v) + '</div>';
        }
    },
    formatSummaryMoney: function (v) {
        return '<div style="text-align: right; font-weight: bold;">' + formatoMoneda(v) + '</div>';
    },
    delAllPases: function (btn) {
        btn.up('grid').getStore().removeAll();
    },
    addPase: function (btn) {
        var winCuenta = Ext.widget('win_cuenta', {
            title: 'Seleccione una cuenta para adicionarla como pase',
            origen: 'add',
            checked: true
        });
        winCuenta.down('button[action=aplicar]').setHandler(this.onSelectCuenta, this);
        winCuenta.down('button[action=aceptar]').setHandler(this.onSelectCuenta, this);
    },
    onSelectCuenta: function (btn) {
        var win = btn.up('window'),
            tree = win.down('treepanel'),
            form = win.down('form').getForm(),
            formValues = form.getValues();

        var gridPases = Ext.ComponentQuery.query('grid[itemId=grid_pases]'),
            stPases = gridPases[0].getStore();

        if (win.origen == 'add') {
            var totalCuentas = tree.getChecked().length;
            if (totalCuentas > 0) {
                var records = tree.getChecked();

                for (var i = 0; i < totalCuentas; i++) {
                    //Verifica que la cuenta no exista en el listado de pases
                    if (this.checkCuentaOnPases(records[i].get('idcuenta'), formValues.codigodocumento) == false) {
                        stPases.insert(0, {
                            idcuenta: records[i].get('idcuenta'),
                            concatcta: records[i].get('concatcta'),
                            denominacion: records[i].get('denominacion'),
                            codigodocumento: formValues.codigodocumento,
                            debito: formValues.debitocredito == 0 ? formValues.importe : 0,
                            credito: formValues.debitocredito == 1 ? formValues.importe : 0
                        });
                    }
                }
            }
        } else {
            if (tree.getSelectionModel().hasSelection()) {
                //var selectedPase = this.getPase_list().getSelectionModel().getLastSelected();
                //var cuenta = tree.getSelectionModel().getLastSelected();
                ////Verifica que la cuenta no exista en el listado de pases
                //if (this.checkCuentaOnPases(record.get('idcuenta'), formValues.codigodocumento) == false) {
                //    selectedPase.set('idcuenta', cuenta.get('idcuenta'));
                //    selectedPase.set('codigo', cuenta.get('concatcta'));
                //    selectedPase.set('cuenta', cuenta.get('denominacion'));
                //    selectedPase.set('codigodocumento', formValues.codigodocumento);
                //    selectedPase.set('debito', formValues.debitocredito == 0 ? formValues.importe : 0);
                //    selectedPase.set('credito', formValues.debitocredito == 1 ? formValues.importe : 0);
                //}
            }
        }
        stPases.sort('debito', 'DESC');

        if (btn.action === 'aceptar') {
            win.close();
        }
        else {
            tree.getSelectionModel().deselectAll();
            form.reset();
        }
    },
    /**
     * Verifica que si una cuenta existe en el listado de pases
     * @param idcuenta
     */
    checkCuentaOnPases: function (idcuenta, codigodocumento) {
        var gridPases = Ext.ComponentQuery.query('grid[itemId=grid_pases]');
        var allPases = gridPases[0].getStore().getRange();
        var totalPases = allPases.length;
        for (var i = 0; i < totalPases; i++) {
            if (idcuenta == allPases[i].get('idcuenta') && codigodocumento == allPases[i].get('codigodocumento')) {
                Ext.Msg.show({
                    title: 'Alerta',
                    msg: 'La cuenta que desea agregar ya existe.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
                return true;
            }
        }
        return false;
    },
    aplicarPases: function (btn) {
        var cbModelo = btn.up('form').down('combobox[name=idmodelocontable]'),
            stPases = btn.up('window').stPases,
            allPases = stPases.getRange();
        var record = cbModelo.getStore().findRecord('idmodelocontable', cbModelo.getValue()),
            newPases = record.get('pases');
        if (Ext.isArray(newPases)) {
            //Recorre los nuevos pases para adicionarlos al Store
            Ext.each(newPases, function (newPase) {
                var codDoc = btn.up('form').down('textfield[name=codigodocumento]').getValue();
                var totalPases = allPases.length;
                //Verifica la cuenta no exista con un mismo numero de documento
                for (var i = 0; i < totalPases; i++) {
                    if (newPase.idcuenta == allPases[i].get('idcuenta') && allPases[i].get('codigodocumento') == codDoc) {
                        return;
                    }
                }
                stPases.add({
                    idcuenta: newPase.idcuenta,
                    concatcta: newPase.concatcta,
                    denominacion: newPase.denomcuenta,
                    debito: newPase.debitocredito == 0 ? 0.00 : null,
                    credito: newPase.debitocredito == 1 ? 0.00 : null,
                    codigodocumento: codDoc
                });
            });
            stPases.sort('debito', 'DESC');
        }

    }
});