Ext.define('CmpComprobantePreview', {
    extend: 'Ext.window.Window',
    alias: 'widget.comprobante_preview',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    constrain: true,
    width: 870,
    height: 580,
    initComponent: function () {
        var me = this;

        this.stPases = Ext.create('Ext.data.Store', {
            fields: [
                {name: 'idpase', type: 'int'},
                {name: 'idcuenta', type: 'int'},
                {name: 'codigodocumento', type: 'string'},
                {name: 'concatcta', type: 'string'},
                {name: 'denominacion', type: 'string'},
                {name: 'debito', type: 'float'},
                {name: 'credito', type: 'float'}
            ]
        });

        this.items = [{
            xtype: 'form',
            region: 'north',
            margins: '5 5 0 5',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            frame: true,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 100
            },
            items: [
                {
                    xtype: 'hidden',
                    name: 'idsubsistema'
                },
                {
                    xtype: 'hidden',
                    name: 'idperiodo'
                },
                {
                    xtype: 'hidden',
                    name: 'tipo'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    items: [
                        {
                            name: 'referencia',
                            flex: 1,
                            fieldLabel: 'Referencia',
                            maxLength: 255
                        }, {
                            xtype: 'datefield',
                            width: 180,
                            name: 'fechaemision',
                            fieldLabel: 'Fecha',
                            readOnly: true,
                            margins: '0 0 0 5'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Detalle',
                    name: 'detalle'
                }
            ]
        }, {
            xtype: 'grid',
            region: 'center',
            margins: '5 5 5 5',
            store: this.stPases,
            columns: [
                {
                    header: 'Número documento',
                    //tooltip: perfil.etiquetas.ttpColNoDoc,
                    dataIndex: 'codigodocumento',
                    width: 120
                },
                {
                    header: 'Código',
                    //tooltip: perfil.etiquetas.ttpColCodigo,
                    dataIndex: 'concatcta',
                    width: 120
                },
                {
                    header: 'Cuenta',
                    //tooltip: perfil.etiquetas.ttpColCuenta,
                    dataIndex: 'denominacion',
                    flex: 1
                },
                {
                    header: 'Débito',
                    dataIndex: 'debito',
                    width: 90,
                    renderer: me.formatMoney,
                    summaryRenderer: me.formatSummaryMoney,
                    summaryType: 'sum'
                },
                {
                    header: 'Crédito',
                    dataIndex: 'credito',
                    width: 90,
                    renderer: me.formatMoney,
                    summaryRenderer: me.formatSummaryMoney,
                    summaryType: 'sum'
                }
            ],
            features: [{
                ftype: 'summary'
            }],
            height: 200,
            width: 400
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    },
    formatMoney: function (val) {
        if (val > 0) {
            return '<div style="text-align: right;">' + Ext.util.Format.usMoney(val) + '</div>';
        } else {
            return null;
        }
    },
    formatSummaryMoney: function (val) {
        return '<div style="text-align: right; font-weight: bold;">' + Ext.util.Format.usMoney(val) + '</div>';
    }
});