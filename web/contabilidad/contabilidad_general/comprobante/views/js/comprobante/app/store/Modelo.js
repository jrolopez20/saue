Ext.define('Comprobante.store.Modelo', {
    extend: 'Ext.data.Store',
    model: 'Comprobante.model.Modelo',

    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'loadDataModelo'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});