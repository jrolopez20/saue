Ext.define('Comprobante.store.Chart', {
    extend: 'Ext.data.Store',
    model: 'Comprobante.model.Chart',

    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'graficarComprobantesPorEstado'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});