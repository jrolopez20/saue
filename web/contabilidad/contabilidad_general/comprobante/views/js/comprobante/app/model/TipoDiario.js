Ext.define('Comprobante.model.TipoDiario', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idtipodiario', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'subsistema', type: 'string'},
        {name: 'idsubsistema', type: 'string'},
        {name: 'descripcion', type: 'string'}
    ]
});