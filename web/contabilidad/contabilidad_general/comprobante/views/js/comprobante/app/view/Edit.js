Ext.define('Comprobante.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.comprobante_edit',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    constrain: true,
    width: 870,
    height: 580,
    initComponent: function () {

        this.items = [{
            xtype: 'form',
            url: 'updateComprobante',
            region: 'north',
            margins: '5 5 0 5',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            frame: true,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 100
            },
            items: [
                {
                    xtype:'hidden',
                    name: 'idcomprobante'
                },
                {
                    xtype:'hidden',
                    name: 'idsubsistema'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    items: [
                        {
                            name: 'referencia',
                            flex: 1,
                            fieldLabel: perfil.etiquetas.lbReferencia,
                            maxLength: 255,
                            allowBlank: false
                        }, {
                            xtype: 'datefield',
                            width: 180,
                            name: 'fechaemision',
                            fieldLabel: perfil.etiquetas.lbFecha,
                            allowBlank: false,
                            margins: '0 0 0 5'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: perfil.etiquetas.lbDetalle,
                    name: 'detalle'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'idtipodiario',
                            fieldLabel: perfil.etiquetas.lbTipoAsiento,
                            store: 'Comprobante.store.TipoDiario',
                            valueField: 'idtipodiario',
                            displayField: 'denominacion',
                            editable: false,
                            emptyText: 'Seleccione...',
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            flex: 1
                        },
                        {
                            xtype: 'combobox',
                            name: 'idmodelocontable',
                            fieldLabel: perfil.etiquetas.lbModelo,
                            margins: '0 0 0 5',
                            store: 'Comprobante.store.Modelo',
                            valueField: 'idmodelocontable',
                            displayField: 'denominacion',
                            flex: 1,
                            editable: false,
                            emptyText: 'Seleccione...',
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            disabled: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'codigodocumento',
                            fieldLabel: perfil.etiquetas.lbNumDoc,
                            margins: '0 5 0 5',
                            width: 110,
                            disabled: true
                        },
                        {
                            xtype: 'button',
                            text: 'Aplicar',
                            icon: perfil.dirImg + 'aplicar.png',
                            iconCls: 'btn',
                            iconAlign: 'top',
                            disabled: true,
                            action: 'aplicarpases'
                        }
                    ]
                }
            ]
        }, {
            xtype: 'pase_list',
            region: 'center',
            margins: '5 5 5 5'
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                tooltip: perfil.etiquetas.ttpBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAplicar,
                tooltip: perfil.etiquetas.ttpBtnAplicar,
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                tooltip: perfil.etiquetas.ttpBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});