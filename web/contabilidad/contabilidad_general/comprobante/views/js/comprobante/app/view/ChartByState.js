Ext.define('Comprobante.view.ChartByState', {
    extend: 'Ext.window.Window',
    alias: 'widget.win_chartbystate',
    layout: 'fit',
    modal: true,
    resizable: true,
    maximizable: true,
    autoShow: true,
    width: 750,
    height: 450,

    initComponent: function () {
        var me = this;
        this.title = perfil.etiquetas.lbCantCmpbteByState;

        this.chart = Ext.create('Ext.chart.Chart', {
            xtype: 'chart',
            animate: true,
            store: 'Comprobante.store.Chart',
            background: {
                fill: 'white'
            },
            shadow: true,
            legend: {
                position: 'right'
            },
            insetPadding: 30,
            theme: 'Base:gradients',
            series: [{
                type: 'pie',
                field: 'cantidad',
                showInLegend: true,
                donut: 20,
                tips: {
                    trackMouse: true,
                    width: 140,
                    height: 28,
                    renderer: function (storeItem, item) {
                        this.setTitle(storeItem.get('estado') + ': ' + storeItem.get('porciento') + '%');
                    }
                },
                highlight: {
                    segment: {
                        margin: 20
                    }
                },
                label: {
                    field: 'estado',
                    display: 'rotate',
                    contrast: true,
                    font: '12px Arial'
                }
            }]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCerrar,
                scope: this,
                handler: this.close
            }
        ];

        this.items = this.chart;
        this.callParent(arguments);
    }
});