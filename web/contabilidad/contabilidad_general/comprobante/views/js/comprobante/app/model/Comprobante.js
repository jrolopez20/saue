Ext.define('Comprobante.model.Comprobante', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idcomprobante', type: 'int'},
        {name: 'numerocomprobante', type: 'int'},
        {name: 'referencia', type: 'string'},
        {name: 'detalle', type: 'string'},
        {name: 'fechaemision', type: 'date'},
        {name: 'idestadocomprobante', type: 'int'},
        {name: 'idsubsistema', type: 'int'},
        {name: 'subsistema', type: 'string'},
        {name: 'idperiodo', type: 'int'},
        {name: 'idtipodiario', type: 'int'},
        {name: 'usuario', type: 'string'},
        {name: 'estado', type: 'string'}
    ]
});