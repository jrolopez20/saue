Ext.define('Comprobante.view.PaseList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pase_list',
    store: 'Comprobante.store.Pase',
    viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            dragGroup: 'firstGridDDGroup',
            dropGroup: 'firstGridDDGroup'
        },
        listeners: {
            drop: function (node, data, dropRec, dropPosition) {
                //var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('debito') : ' on empty view';
                //console.info('Dropped ' + data.records[0].get('debito') + ', ' + dropOn);
            }
        }
    },

    initComponent: function () {
        var me = this;

        me.difDebitoCredito = Ext.create('Ext.toolbar.TextItem', {
            text: '0'
        });

        me.selModel = Ext.create('Ext.selection.RowModel');

        this.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdd,
                tooltip: perfil.etiquetas.ttpBtnAddPase,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnClear,
                tooltip: perfil.etiquetas.ttpBtnClearPase,
                icon: perfil.dirImg + 'limpiar.png',
                iconCls: 'btn',
                action: 'limpiar'
            }
        ];

        me.features = [{
            ftype: 'summary'
        }];

        me.columns = [
            {
                header: perfil.etiquetas.lbNumDoc,
                tooltip: perfil.etiquetas.ttpColNoDoc,
                dataIndex: 'codigodocumento',
                width: 120,
                editor: {
                    xtype: 'textfield',
                    maxLength: 255
                }
            },
            {
                header: perfil.etiquetas.lbCodigo,
                tooltip: perfil.etiquetas.ttpColCodigo,
                dataIndex: 'codigo',
                width: 120
            },
            {
                header: perfil.etiquetas.lbCuenta,
                tooltip: perfil.etiquetas.ttpColCuenta,
                dataIndex: 'cuenta',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbDebito,
                dataIndex: 'debito',
                width: 90,
                renderer: me.formatMoney,
                summaryRenderer: me.formatSummaryMoney,
                summaryType: 'sum',
                field: {
                    xtype: 'numberfield',
                    minValue: 0,
                    decimalSeparator: '.',
                    listeners: {
                        change: function (f, newValue, oldValue, eOpts) {
                            var rec = f.up('grid').getSelectionModel().getLastSelected();
                            rec.set('credito', 0);
                        }
                    }
                }
            },
            {
                header: perfil.etiquetas.lbCredito,
                dataIndex: 'credito',
                width: 90,
                renderer: me.formatMoney,
                summaryRenderer: me.formatSummaryMoney,
                summaryType: 'sum',
                field: {
                    xtype: 'numberfield',
                    minValue: 0,
                    decimalSeparator: '.',
                    listeners: {
                        change: function (f, newValue, oldValue, eOpts) {
                            var rec = f.up('grid').getSelectionModel().getLastSelected();
                            rec.set('debito', 0);
                        }
                    }
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: perfil.etiquetas.ttpBtnDelPase,
                    handler: function (grid, rowIndex, colIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }
                }]
            }
        ];

        me.plugins = [this.cellEditing];

        me.bbar = [perfil.etiquetas.msgDragFila, '->', 'Diferencia:', me.difDebitoCredito];

        this.callParent(arguments);
    },
    formatMoney: function (val) {
        if (val > 0) {
            return '<div style="text-align: right;">' + Ext.util.Format.usMoney(val) + '</div>';
        } else {
            return null;
        }
    },
    formatSummaryMoney: function (val) {
        return '<div style="text-align: right; font-weight: bold;">' + Ext.util.Format.usMoney(val) + '</div>';
    }
});