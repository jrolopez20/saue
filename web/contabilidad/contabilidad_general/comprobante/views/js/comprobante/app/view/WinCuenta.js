Ext.define('Comprobante.view.WinCuenta', {
    extend: 'Ext.window.Window',
    alias: 'widget.win_cuenta',
    layout: 'border',
    modal: true,
    resizable: true,
    maximizable: true,
    autoShow: true,
    width: 800,
    height: 500,
    checked: false,
    initComponent: function () {

        this.items = [
            {
                xtype: 'cuenta_tree',
                checked: this.checked,
                region: 'center',
                anySelection: false,
                margins: '5 5 0 5'
            },
            {
                region: 'south',
                xtype: 'form',
                margins: '5 5 5 5',
                frame: true,
                bodyPadding: '5 5 0',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side'
                },
                items: [{
                    xtype: 'container',
                    anchor: '100%',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        width: 150,
                        layout: 'anchor',
                        items: [{
                            xtype: 'textfield',
                            fieldLabel: 'No. de documento',
                            name: 'codigodocumento',
                            maxLength: 255,
                            anchor: '95%'
                        }]
                    }, {
                        xtype: 'container',
                        width: 100,
                        layout: 'anchor',
                        items: [{
                            xtype: 'numberfield',
                            fieldLabel: 'Importe',
                            name: 'importe',
                            decimalSeparator: '.',
                            minValue: 0,
                            anchor: '95%'
                        }]
                    }, {
                        xtype: 'container',
                        width: 150,
                        layout: 'anchor',
                        items: [{
                            xtype: 'radiogroup',
                            fieldLabel: 'Afecta',
                            items: [
                                {
                                    boxLabel:'Débito',
                                    name: 'debitocredito',
                                    inputValue: '0',
                                    checked: true
                                },
                                {
                                    boxLabel: 'Crédito',
                                    name: 'debitocredito',
                                    inputValue: '1'
                                }
                            ]
                        }]
                    }]
                }]
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text:'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});