Ext.define('Comprobante.model.Chart', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'estado', type: 'string'},
        {name: 'cantidad', type: 'int'},
        {name: 'porciento', type: 'float'}
    ]
});