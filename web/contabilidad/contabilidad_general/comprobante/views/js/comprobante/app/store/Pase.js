Ext.define('Comprobante.store.Pase', {
    extend: 'Ext.data.Store',
    model: 'Comprobante.model.Pase',
    sorters: [{
        property: 'debito',
        direction: 'DESC'
    }],
    listeners: {
        update: function(st){
            st.sort('debito', 'DESC');
        }
    }
});