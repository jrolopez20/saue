var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Comprobante',

    enableQuickTips: true,

    paths: {
        'Comprobante': '../../views/js/comprobante/app'
    },

    controllers: ['Comprobante'],

    launch: function () {
        UCID.portal.cargarEtiquetas('comprobante', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'comprobante_grid',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });

    }
});