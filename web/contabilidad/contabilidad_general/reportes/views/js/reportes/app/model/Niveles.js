Ext.define('Reporte.model.Niveles', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idparteformato', type: 'int'
        },
        {
            name: 'idformato', type: 'int'
        },
        {
            name: 'nivel', type: 'int'
        },
        {
            name: 'longitud', type: 'int'
        },
        {
            name: 'nombre', type: 'string'
        },
        {
            name: 'abreviatura', type: 'string'
        },
        {
            name: 'vistap', type: 'string'
        },
        {
            name: 'descripcion', type: 'string'
        }
    ]
});