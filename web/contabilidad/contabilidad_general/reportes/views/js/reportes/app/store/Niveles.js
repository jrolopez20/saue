Ext.define('Reporte.store.Niveles', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Niveles',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadDataNiveles',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});