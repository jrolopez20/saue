Ext.define('Reporte.store.Cuenta', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Cuenta',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadDataCuentas',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});