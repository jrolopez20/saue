Ext.define('Reporte.store.Balance', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Balance',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadDataReport',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});