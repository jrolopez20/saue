Ext.define('Reporte.store.Ejercicio', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Ejercicio',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadDataEjercicios',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});