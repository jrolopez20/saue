Ext.define('Reporte.model.Cuenta', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idcuenta', type: 'int'
        },
        {
            name: 'text', type: 'string'
        },
        {
            name: 'codigo', type: 'string'
        },
        {
            name: 'concatcta', type: 'string'
        }
    ]
});