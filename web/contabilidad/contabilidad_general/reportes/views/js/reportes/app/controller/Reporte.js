Ext.apply(Ext.form.field.VTypes, {
    daterange: function(val, field) {
        var date = field.parseDate(val);

        if (!date) {
            return false;
        }
        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = field.up('form').down('#' + field.startDateField);
            start.setMaxValue(date);
            start.validate();
            this.dateRangeMax = date;
        }
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = field.up('form').down('#' + field.endDateField);
            end.setMinValue(date);
            end.validate();
            this.dateRangeMin = date;
        }
        /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
        return true;
    },
    daterangeText: 'La fecha de inicio debe ser menor que la fecha fin'
});

Ext.define('Reporte.controller.Reporte', {
    extend: 'Ext.app.Controller',
    views: [
        'Reporte.view.Grid',
        'Reporte.view.SearchForm'
    ],
    stores: [
        'Reporte.store.Reporte',
        'Reporte.store.Ejercicio',
        'Reporte.store.Periodo',
        'Reporte.store.TipoReporte',
        'Reporte.store.Cuenta',
        'Reporte.store.Niveles'
    ],
    models: [
        'Reporte.model.Reporte',
        'Reporte.model.Ejercicio',
        'Reporte.model.Periodo',
        'Reporte.model.TipoReporte',
        'Reporte.model.Cuenta',
        'Reporte.model.Niveles'
    ],
    refs: [
        {ref: 'reporte_grid', selector: 'reporte_grid'},
        {ref: 'search_form', selector: 'search_form'}
    ],
    init: function() {
        var me = this;
        me.busyRequest = {};

        me.control({
            'search_form': {
                render: me.onRenderSearchForm
            },
            'search_form combobox[name=idejercicio]': {
                select: me.onSelectEjercicio
            },
            'search_form combobox[name=idperiodo]': {
                select: me.onSelectPeriodo
            },
            'search_form combobox[name=tiporeporte]': {
                select: me.onSelectTipoReporte
            },
            'search_form button[action=search]': {
                click: me.onSearchClick
            },
            'search_form button[action=clear]': {
                click: me.onClearClick
            },
            'search_form button[action=preview]': {
                click: me.onPreviewClick
            }
        });

        me.getReporteStoreReporteStore().on(
                {
                    load: {fn: me.onReporteLoad, scope: this}
                }
        );
    },
    onSelectEjercicio: function(cb, records) {
        var form = cb.up('form');
        form.down('combobox[name=idperiodo]').reset();
        form.down('combobox[name=idperiodo]').getStore().load({
            params: {
                idejercicio: records[0].get('idejercicio')
            }
        });

        this.setRangeDate(records[0].get('inicio'), records[0].get('fin'));
    },
    onSelectPeriodo: function(cb, records) {
        this.setRangeDate(records[0].get('inicio'), records[0].get('fin'));
    },
    onSelectTipoReporte: function(cb, records) {
        /* limpiar el store */
        store = cb.up('viewport').down('grid').getStore();
        store.removeAll();

        var idreporte = cb.getValue();
        var panel = cb.up('form');
        this.addFilters(idreporte, panel);
    },
    /* Esto es lo nuevo */
    addFilters: function(idreporte, panel) {
        if (panel.items.items.length > 1) {
            for (var i = 1; i < panel.items.items.length; i++) {
                var cmp = panel.getComponent(i);
                if (cmp.items) {
                    this.delComponentCascade(panel.getComponent(i));
                } else {
                    panel.remove(i, true);
                }
            }
        }
        this.generateFilterComponentes(idreporte, panel);
    },
    delComponentCascade: function(component) {
        if (component.items.items.length > 0) {
            for (var i = component.items.length - 1; i >= 0; i--) {
                //component.remove(i, true);
                component.removeAll(true);
            }
        }
    },
    generateFilterComponentes: function(idReport, panel) {
        var arrayFilterComp = [];
        switch (idReport) {
            case 10001:
            case 10002:
                this.loadMask('Cargando filtros, espere por favor...');
                arrayFilterComp.push(this.buildAccountCb(idReport));
                break;
            case 10003:
                this.loadMask('Cargando filtros, espere por favor...');
                arrayFilterComp.push(this.buildLevelsCb(idReport));
                break;
            default :
                break;
        }
        if (arrayFilterComp.length) {
            this.renderFilters(arrayFilterComp, panel);
        }
    },
    renderFilters: function(cmpFilters, searchPanel) {
        if (cmpFilters.length > 0) {
            for (var i = 0; i < cmpFilters.length; i++) {
                searchPanel.add(cmpFilters[i]);
                searchPanel.doLayout(true);
            }
        }
    },
    buildAccountCb: function(idreporte) {
        var me = this;
        var cbCuenta = {
            xtype: 'combobox',
            name: 'cuenta',
            fieldLabel: (idreporte === 10001) ? perfil.etiquetas.lbCuenta : perfil.etiquetas.lbSubCuenta,
            store: 'Reporte.store.Cuenta',
            queryMode: 'local',
            displayField: 'concatcta',
            valueField: 'idcuenta',
            multiSelect: true,
            flex: 1,
            editable: false,
            allowBlank: false,
            listConfig: {
                getInnerTpl: function() {
                    return '<div data-qtip="{text}">{text}</div>';
                }
            },
            listeners: {
                change: function(field, newValue, oldValue) {
                    me.cambio = 1;
                }
            }
        };

        var store = me.getStore('Reporte.store.Cuenta');

        store.on({
            scope: me,
            beforeload: function(store, options) {
                this.busyRequest.Cuenta = true;
            },
            load: function(store, records, options) {
                delete this.busyRequest.Cuenta;
                if (!Object.keys(this.busyRequest).length) {
                    this.lmask.hide();
                }
            }
        });

        store.load({
            params: {
                idtiporeporte: idreporte
            }
        });

        return cbCuenta;
    },
    buildLevelsCb: function(idreporte) {
        var me = this;
        var cbLevels = {
            xtype: 'combobox',
            name: 'nivel',
            fieldLabel: 'Niveles:',
            store: 'Reporte.store.Niveles',
            queryMode: 'local',
            displayField: 'nombre',
            valueField: 'nivel',
            //multiSelect: true,
            flex: 1,
            editable: false,
            allowBlank: false,
            listConfig: {
                getInnerTpl: function() {
                    return '<div data-qtip="{nombre}">{nombre}</div>';
                }
            },
            listeners: {
                change: function(field, newValue, oldValue) {
                    me.cambio = 1;
                }
            }
        };

        var store = me.getStore('Reporte.store.Niveles');

        store.on({
            scope: me,
            beforeload: function(store, options) {
                this.busyRequest.Levels = true;
            },
            load: function(store, records, options) {
                delete this.busyRequest.Levels;
                if (!Object.keys(this.busyRequest).length) {
                    this.lmask.hide();
                }
            }
        });

        store.load();

        return cbLevels;
    },
    loadMask: function(mensaje) {
        this.lmask = new Ext.LoadMask(Ext.getBody(), {
            msg: mensaje
        });
        this.lmask.show();
    },
    /* Fin lo nuevo */

    setRangeDate: function(start, end) {
        var form = this.getSearch_form();

        var dfDesde = form.down('datefield[name=desde]');
        dfDesde.setMinValue(start);
        dfDesde.setValue(start);
        dfDesde.setMaxValue(end);

        var dfHasta = form.down('datefield[name=hasta]');
        dfHasta.setMinValue(start);
        dfHasta.setValue(end);
        dfHasta.setMaxValue(end);
    },
    onRenderSearchForm: function(form) {
        var me = this;
        var myMask = new Ext.LoadMask(form.up('viewport'), {msg: perfil.etiquetas.msgLoadingFecha});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDataFecha',
            method: 'POST',
            callback: function(options, success, response) {
                var r = Ext.decode(response.responseText);
                myMask.hide();
                var dfDesde = form.down('datefield[name=desde]');
                dfDesde.setValue(r.periodo.inicio);
                var dfHasta = form.down('datefield[name=hasta]');
                dfHasta.setValue(r.periodo.fin);
            }
        });
    },
    onSearchClick: function(btn) {
        var me = this,
                form = btn.up('form'),
                grid = btn.up('viewport').down('grid'),
                tiporeporte = form.down('combobox[name=tiporeporte]').getValue();

        /* Si es el balance reconfigurar las column del grid */

        this.reconfigureGrid(tiporeporte, grid);

        //grid.reconfigure(grid.getStore(), columns);

        var values = form.getForm().getValues();
        for (key in values) {
            if (Ext.isArray(values[key])) {
                values[key] = Ext.encode(values[key]);
            }
        }

        grid.getStore().load({
            params: values
        });
    },
    onPreviewClick: function(btn) {
        var dataSources = this.getReporte_grid().getStore().getProxy().getReader().jsonData.dataSources;
        var win = Ext.widget('printview', {
            dataSources: dataSources
        });
    },
    onClearClick: function(btn) {
        var form = btn.up('form');
        form.getForm().reset();
        var dfDesde = form.down('datefield[name=desde]');
        dfDesde.setMinValue(null);
        dfDesde.setMaxValue(null);

        var dfHasta = form.down('datefield[name=hasta]');
        dfHasta.setMinValue(null);
        dfHasta.setMaxValue(null);

        this.getReporte_grid().getStore().removeAll();
        form.down('button[action=preview]').disable();
    },
    onReporteLoad: function(st, records, successful) {

        if (records.length > 0) {
            this.getSearch_form().down('button[action=preview]').enable();
        }
    },
    reconfigureGrid: function(idReporte, grid) {
        var me = this;
        switch (idReporte) {
            case 10003:
                var store = this.getStore('Reporte.store.Balance');
                var columns = [
                    {
                        header: perfil.etiquetas.lbCodigo,
                        tooltip: perfil.etiquetas.ttpCodigo,
                        dataIndex: 'concatcuenta',
                        width: 100
                                //flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbDescripcion,
                        tooltip: perfil.etiquetas.ttpDescripcion,
                        dataIndex: 'descripcioncuenta',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbSaldoIni,
                        tooltip: perfil.etiquetas.lbSaldoIni,
                        dataIndex: 'saldoinicio',
                        align: 'right',
                        width: 110
                    },
                    {
                        header: perfil.etiquetas.lbParcial,
                        tooltip: perfil.etiquetas.lbParcial,
                        dataIndex: 'parcial',
                        align: 'right',
                        width: 110,
                        renderer: function(value, metadata, record) {
                            if (record.data.descripcioncuenta.toLowerCase().indexOf('<b>') !== -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>total</b>') === -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>subtotal</b>') === -1) {
                                return '';
                            } else {
                                return me.formatoMoneda(value);
                            }
                        }
                    },
                    {
                        header: perfil.etiquetas.lbDebe,
                        tooltip: perfil.etiquetas.lbDebe,
                        dataIndex: 'debe',
                        width: 110,
                        align: 'right',
                        renderer: function(value, metadata, record) {
                            if (record.data.descripcioncuenta.toLowerCase().indexOf('<b>') !== -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>total</b>') === -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>subtotal</b>') === -1) {
                                return '';
                            } else {
                                return me.formatoMoneda(value);
                            }
                        }
                    },
                    {
                        header: perfil.etiquetas.lbHaber,
                        tooltip: perfil.etiquetas.lbHaber,
                        dataIndex: 'haber',
                        width: 110,
                        align: 'right',
                        renderer: function(value, metadata, record) {
                            if (record.data.descripcioncuenta.indexOf('<b>') !== -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>total</b>') === -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>subtotal</b>') === -1) {
                                return '';
                            } else {
                                return me.formatoMoneda(value);
                            }
                        }
                    },
                    {
                        header: perfil.etiquetas.lbSaldoPeriod,
                        tooltip: perfil.etiquetas.ttpSaldoPeriod,
                        dataIndex: 'saldoperiodo',
                        width: 110,
                        align: 'right',
                        renderer: function(value, metadata, record) {
                            if (record.data.descripcioncuenta.toLowerCase().indexOf('<b>') !== -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>total</b>') === -1
                                    && record.data.descripcioncuenta.toLowerCase().indexOf('<b>subtotal</b>') === -1) {
                                return '';
                            } else {
                                return me.formatoMoneda(value);
                            }
                        }
                    }
                ];
                break;
            case 10006:
            case 10007:
                var store = this.getStore('Reporte.store.Comprobante');
                var columns = [
                    {
                        header: perfil.etiquetas.lbNumero,
                        dataIndex: 'numerocomprobante',
                        width: 80
                    },
                    {
                        header: perfil.etiquetas.lbReferencia,
                        dataIndex: 'referencia',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbEstado,
                        dataIndex: 'estado',
                        width: 100,
                        renderer: me.showStatus
                    },
                    {
                        header: perfil.etiquetas.lbFecha,
                        dataIndex: 'fechaemision',
                        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        width: 90
                    },
                    {
                        header: perfil.etiquetas.lbUsuario,
                        dataIndex: 'usuario',
                        width: 100
                    },
                    {
                        header: perfil.etiquetas.lbDetalle,
                        dataIndex: 'detalle',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbSubsistema,
                        dataIndex: 'subsistema'
                    }
                ];
                break;
            default:
                var store = this.getStore('Reporte.store.Reporte');
                var columns = [
                    {
                        header: perfil.etiquetas.lbCuenta,
                        tooltip: perfil.etiquetas.ttpColCuenta,
                        dataIndex: 'cuenta',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbFecha,
                        tooltip: perfil.etiquetas.ttpColFecha,
                        dataIndex: 'fechaemision',
                        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        width: 90
                    },
                    {
                        header: perfil.etiquetas.lbNoComprobante,
                        tooltip: perfil.etiquetas.ttpNoCompte,
                        dataIndex: 'numerocomprobante',
                        width: 110
                    },
                    {
                        header: perfil.etiquetas.lbReferencia,
                        tooltip: perfil.etiquetas.ttpColReferencia,
                        dataIndex: 'descripcion',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbDebe,
                        dataIndex: 'debe',
                        width: 110,
                        renderer: this.renderRightAlign
                    },
                    {
                        header: perfil.etiquetas.lbHaber,
                        dataIndex: 'haber',
                        width: 110,
                        renderer: this.renderRightAlign
                    },
                    {
                        header: perfil.etiquetas.lbSaldo,
                        dataIndex: 'saldo',
                        width: 110,
                        renderer: this.renderRightAlign
                    }
                ];
                break;
        }
        store.on({
            load: {fn: me.onReporteLoad, scope: this} // activar el btn vista previa si devuelve algo
        });

        grid.reconfigure(store, columns);
    },
    renderRightAlign: function(val) {
        return '<div style="text-align: right;">' + val + '</div>';
    },
    formatoMoneda: function(v) {
        if (v == 0 || Ext.isEmpty(v))
            return '0.00';
        v = (Math.round((v - 0) * 100)) / 100;
        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
        v = String(v);
        var ps = v.split(".");
        var whole = ps[0];
        var sub = ps[1] ? "." + ps[1] : ".00";
        var r = /(\d+)(\d{3})/;
        while (r.test(whole)) {
            whole = whole.replace(r, "$1" + "," + "$2");
        }
        v = whole + sub;
        if (v.charAt(0) == "-") {//caso saldo negativo
            return "(" + v.substr(1) + ")";
        }
        return v;
    }

})
        ;