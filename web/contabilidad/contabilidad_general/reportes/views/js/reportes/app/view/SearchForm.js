Ext.define('Reporte.view.SearchForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.search_form',

    bodyPadding: 5,
    collapsible: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    fieldDefaults: {
        labelAlign: 'top'
    },

    initComponent: function () {
        var me = this;
        me.title = perfil.etiquetas.lbSearchForm;

        this.items = [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaultType: 'textfield',

                fieldDefaults: {
                    labelAlign: 'top'
                },

                items: [
                    {
                        xtype: 'combobox',
                        name: 'idejercicio',
                        fieldLabel: 'Ejercicio',
                        store: 'Reporte.store.Ejercicio',
                        queryMode: 'local',
                        displayField: 'nombre',
                        valueField: 'idejercicio',
                        width: 150,
                        editable: false
                    },
                    {
                        xtype: 'combobox',
                        name: 'idperiodo',
                        fieldLabel: 'Periodo',
                        store: 'Reporte.store.Periodo',
                        queryMode: 'local',
                        displayField: 'nombre',
                        valueField: 'idperiodo',
                        width: 150,
                        editable: false,
                        margins: '0 0 0 5'
                    }, {
                        xtype: 'datefield',
                        name: 'desde',
                        fieldLabel: 'Desde',
                        width: 175,
                        allowBlank: false,
                        itemId: 'desde',
                        vtype: 'daterange',
                        endDateField: 'hasta',
                        margins: '0 0 0 5'
                    }, {
                        xtype: 'datefield',
                        name: 'hasta',
                        fieldLabel: 'Hasta',
                        width: 175,
                        allowBlank: false,
                        itemId: 'hasta',
                        vtype: 'daterange',
                        startDateField: 'desde',
                        margins: '0 0 0 5'
                    }, {
                        xtype: 'combobox',
                        name: 'tiporeporte',
                        fieldLabel: 'Tipo de reporte',
                        store: 'Reporte.store.TipoReporte',
                        queryMode: 'local',
                        displayField: 'nombre',
                        valueField: 'idtiporeporte',
                        flex: 1,
                        editable: false,
                        margins: '0 0 0 5',
                        allowBlank: false
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: perfil.etiquetas.lbBtnSearch,
                tooltip: perfil.etiquetas.ttpBtnSearch,
                icon: perfil.dirImg + 'buscar.png',
                iconCls: 'btn',
                formBind: true,
                disabled: true,
                action: 'search'
            },
            {
                text: perfil.etiquetas.lbBtnPreview,
                tooltip: perfil.etiquetas.ttpBtnPreview,
                icon: perfil.dirImg + 'ver.png',
                iconCls: 'btn',
                disabled: true,
                action: 'preview'
            },
            {
                text: perfil.etiquetas.lbBtnClear,
                tooltip: perfil.etiquetas.ttpBtnClear,
                icon: perfil.dirImg + 'limpiar.png',
                iconCls: 'btn',
                action: 'clear'
            }];

        this.callParent(arguments);
    }
});