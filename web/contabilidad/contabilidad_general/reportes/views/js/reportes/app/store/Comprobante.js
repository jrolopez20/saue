Ext.define('Reporte.store.Comprobante', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Comprobante',
    autoLoad: false,
    groupField: 'subsistema',
    sorters: ['subsistema', 'numerocomprobante'],
    proxy: {
        type: 'rest',
        url: 'loadDataReport',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});