Ext.define('Reporte.model.Reporte', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'idcuenta', type: 'int'
        },
        {
            name: 'cuenta', type: 'string'
        },
        {
            name: 'idcomprobante', type: 'int'
        },
        {
            name: 'descripcion', type: 'string'
        },
        {
            name: 'fechaemision', type: 'date'
        },
        {
            name: 'numerocomprobante'
        },
        {
            name: 'debe'
        },
        {
            name: 'haber'
        },
        {
            name: 'saldo'
        }
    ]
});