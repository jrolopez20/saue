Ext.define('Reporte.store.Periodo', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Periodo',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadDataPeriodos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});