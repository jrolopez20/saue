Ext.define('Estadosf.store.Periodo', {
    alias: 'widget.st_periodo',
    extend: 'Ext.data.Store',
    model: 'Estadosf.model.Periodo',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadDataPeriodos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});