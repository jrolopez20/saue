Ext.define('Estadosf.view.WinGestEF', {
    extend: 'Ext.window.Window',
    alias: 'widget.win_gestef',
    id:'winGestEF',
    width: 1024,
    height: 500,
    resizable: false,
    modal: true,
    plain: true,
    layout: 'fit',
    animateTarget: 'btnGestEF',
    initComponent: function(arguments) {
        this.items = [{
            xtype: 'fp_gestconf'
            }];
        this.buttons = [{
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            }, {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                hidden: true,
                action: 'aplicar'
            }, {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];
        this.callParent(arguments);
    }
});


