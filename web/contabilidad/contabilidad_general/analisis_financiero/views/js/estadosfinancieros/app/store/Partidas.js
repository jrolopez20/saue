Ext.define('Estadosf.store.Partidas', {
    alias: 'widget.st_partidas',
    extend: 'Ext.data.Store',
    model: 'Estadosf.model.Partidas',
    proxy: {
        type: 'memory',
        reader: {
            type: 'array'
        }
    },
    data: []
});