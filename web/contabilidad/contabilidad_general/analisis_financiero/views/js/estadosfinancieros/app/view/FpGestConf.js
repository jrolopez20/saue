Ext.define('Estadosf.view.FpGestConf', {
    extend: 'Ext.form.Panel',
    alias: 'widget.fp_gestconf',
    requires: ['Ext.form.*', 'Ext.layout.container.Column'],
    layout: 'form',
    initComponent: function() {
        Ext.apply(this, {
            fieldDefaults: {
                labelWidth: 90,
                anchor: '100%'
            },
            items: [
                {
                    id: 'fpDatosEF',
                    layout: 'column',
                    border: false,
                    items: {
                        columnWidth: 1,
                        margin: '3 10 0 10',
                        xtype: 'fieldset',
                        layout: 'column',
                        anchor: '100%',
                        defaultType: 'textfield',
                        items: [{
                                columnWidth: 0.25,
                                fieldLabel: 'C&oacute;digo',
                                labelAlign: 'left',
                                labelWidth: 50,
                                allowBlank: false,
                                maxLength: 30,
                                margin: '3 5 3 2',
                                id: 'tfCodigo',
                                name: 'codigoef',
                                regex: /^([a-zA-Z0-9\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+\,]+ ?[a-zA-Z0-9\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+\,]*)+$/
                            }, {
                                columnWidth: 0.55,
                                fieldLabel: 'Denominaci&oacute;n',
                                labelAlign: 'left',
                                allowBlank: false,
                                maxLength: 255,
                                margin: '3 5 3 0',
                                id: 'tfDenominacion',
                                name: 'denomef',
                                regex: /^([a-zA-Z0-9\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+\,]+ ?[a-zA-Z0-9\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+\,]*)+$/
                            }, {
                                labelAlign: 'left',
                                labelWidth: 45,
                                margin: '3 5 3 0',
                                xtype: 'combobox',
                                id: 'idmodestadofinanciero',
                                fieldLabel: 'Listado',
                                store: 'Estadosf.store.EstadosFinanc',
                                queryMode: 'local',
                                displayField: 'denominacion',
                                valueField: 'idestadofinanciero',
                                editable: false,
                                //hidden: true,
                                //disabled: true,
                                listConfig: {
                                    loadingText: 'Cargando...',
                                    emptyText: 'No se han encontrado resultados.'
                                    // getInnerTpl: function() {
                                        // var iconDel = perfil.dirImg + 'eliminar.png';
                                        // return '<span>{denominacion}</span><div align=right><img src=' + iconDel + ' onClick="Estadosf.getApplication().getEstadosFinancierosController().delEstadoF({idestadofinanciero});";></div>';
                                    // }
                                }
                            }, {
                                columnWidth: 0.15,
                                xtype: 'checkbox',
                                id: 'checkboxbModEF',
                                boxLabel: 'Modificar estado..',
                                anchor: '100%',
                                margins: '3 2 3 0',
                                hidden: true,
                                hideEmptyLabel: true,
                                handler: function(checkbox, checked) {
                                    Ext.getCmp('idmodestadofinanciero').setDisabled(!checked);
                                    if (!checked) {
                                        Ext.getCmp('tfCodigo').reset();
                                        Ext.getCmp('tfDenominacion').reset();
                                        Ext.getCmp('idmodestadofinanciero').reset();
                                        var gpPartidas = Ext.widget('gp_partidas');
                                        gpPartidas.getStore().removeAll();
                                    }
                                }
                            }
                        ]}

                }, {
                    layout: 'column',
                    border: false,
                    items: [{
                            columnWidth: 0.4,
                            margin: '0 10 0 10',
                            xtype: 'tp_cuentas',
                            height: 385
                        }, {
                            layout: 'form',
                            columnWidth: 0.03,
                            border: false,
                            items: [{
                                    tooltip: perfil.etiquetas.lbTtSumar,
                                    icon: perfil.dirImg + 'adicionar.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'plus'
                                }, {
                                    tooltip: perfil.etiquetas.lbTtRestar,
                                    icon: perfil.dirImg + 'restar.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'minus'
                                }, {
                                    tooltip: perfil.etiquetas.lbTtIgual,
                                    icon: perfil.dirImg + 'igual.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'equal'
                                }, {
                                    tooltip: perfil.etiquetas.lbTtMultiplicar,
                                    icon: perfil.dirImg + 'multiplicar.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'mult'
                                }, {
                                    tooltip: perfil.etiquetas.lbTtDividir,
                                    icon: perfil.dirImg + 'dividir.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'div'
                                }, {
                                    tooltip: perfil.etiquetas.lbTtSaltoLinea,
                                    icon: perfil.dirImg + 'incluir.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'breakline'
                                },{
                                    tooltip: perfil.etiquetas.lbTtDelete,
                                    icon: perfil.dirImg + 'eliminar.png',
                                    iconCls: 'btn',
                                    xtype: 'button',
                                    action: 'ctrlz'
                                }]
                        }, {
                            columnWidth: 0.57,
                            margin: '0 10 10 0',
                            height: 385,
                            xtype: 'gp_partidas'
                        }]
                }
            ]
        });
        this.callParent();
    }
});