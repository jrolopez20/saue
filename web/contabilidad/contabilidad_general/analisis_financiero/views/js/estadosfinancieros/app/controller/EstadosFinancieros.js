
Ext.define('Estadosf.controller.EstadosFinancieros', {
    id: 'controlef',
    extend: 'Ext.app.Controller',
    views: ['Estadosf.view.FpFilters', 'Estadosf.view.GpReporte', 'Estadosf.view.GpPartidas', 'Estadosf.view.FpGestConf', 'Estadosf.view.FpConfGrafica', 'Estadosf.view.TpCuentas', 'Estadosf.view.WinGestEF', 'Estadosf.view.WinConfGrafica'],
    stores: ['Estadosf.store.Ejercicio', 'Estadosf.store.Periodo', 'Estadosf.store.EstadosFinanc', 'Estadosf.store.CuentaAgrupada', 'Estadosf.store.Partidas', 'Estadosf.store.Dimension', 'Estadosf.store.Reporte'],
    models: ['Estadosf.model.Ejercicio', 'Estadosf.model.Periodo', 'Estadosf.model.EstadosFinanc', 'Estadosf.model.CuentaAgrupada', 'Estadosf.model.Partidas', 'Estadosf.model.Dimension', 'Estadosf.model.Reporte'],
    refs: [{ref: 'fp_gestconf', selector: 'fp_gestconf'}, {ref: 'fp_filters', selector: 'fp_filters'}, {ref: 'fp_confgrafica', selector: 'fp_confgrafica'}, {ref: 'gp_reporte', selector: 'gp_reporte'},
        {ref: 'gp_partidas', selector: 'gp_partidas'}, {ref: 'st_ejercicio', selector: 'st_ejercicio'}, {ref: 'st_periodo', selector: 'st_periodo'},
        {ref: 'st_estadosf', selector: 'st_estadosf'}, {ref: 'st_partidas', selector: 'st_partidas'}, {ref: 'st_ctagrupada', selector: 'st_ctagrupada'},
        {ref: 'cmp_cuenta', selector: 'cmp_cuenta'}, {ref: 'tp_cuentas', selector: 'tp_cuentas'}, {ref: 'win_gestef', selector: 'win_gestef'}, {ref: 'win_confgrafica', selector: 'win_confgrafica'}],
    stDimChart: new Object(),
    nodos: new Array(),
    arrayConcat: new Array(),
    arrayTipos: new Array(),
    arrayStr: new Array(),
    arrayID: new Array(),
    flagIgualdad: false,
    formulaStr: '',
    formulaID: '',
    formulaCta: '',
    titlef: '',
    modifiedEF: -1,
    init: function() {
        var me = this;
        me.control({
            'fp_gestconf button[action=plus]': {
                click: function() {
                    me.buildEstadoFinanciero(5, '+', '+');
                }
            },
            'fp_gestconf button[action=minus]': {
                click: function() {
                    me.buildEstadoFinanciero(5, '-', '-');
                }
            },
            'fp_gestconf button[action=equal]': {
                click: function() {
                    me.buildEstadoFinanciero(5, '=', '=');
                }
            },
            'fp_gestconf button[action=mult]': {
                click: function() {
                    me.buildEstadoFinanciero(5, '*', '*');
                }
            },
            'fp_gestconf button[action=div]': {
                click: function() {
                    me.buildEstadoFinanciero(5, '/', '/');
                }
            },
            'fp_gestconf button[action=breakline]': {
                click: function() {
                    me.buildEstadoFinanciero(6, '', 'breakline');
                }
            },
            'fp_gestconf button[action=ctrlz]': {
                click: function() {
                    me.buildEstadoFinanciero(7, '', 'ctrlz');
                }
            },
            'fp_filters combobox[name=idejercicio]': {
                select: me.onSelectEjercicio
            },
            'fp_filters combobox[name=idperiodo]': {
                select: me.onSelectPeriodo
            },
            'fp_filters combobox[name=idestadofinanciero]': {
                select: function(cmb, record) {
                    this.nodos = record[0].data.arraypostfija;
                    this.titlef = record[0].data.denominacion;
                    Ext.getCmp('btnDelEF').setDisabled(false);
                }
            },
            'fp_filters button[action=search]': {
                click: me.onSearchCalc
            },
            'fp_filters button[action=clear]': {
                click: me.onClearClick
            },
            'fp_filters button[action=gestef]': {
                click: me.gestEstadosFinancieros
            },
            'fp_filters button[action=delef]': {
                click: me.delEstadoF
            },
            'fp_filters button[action=preview]': {
                click: me.onPreviewClick
            },
            'st_ejercicio': {
                beforeload: me.loadMask(perfil.etiquetas.lbCargando),
                load: me.loadMask()
            },
            'st_periodo': {
                beforeload: me.loadMask(perfil.etiquetas.lbCargando),
                load: me.loadMask()
            },
            'win_gestef button[action=cancelar]': {
                click: me.clearWinGestEF
            },
            'win_gestef button[action=aplicar]': {
                click: me.regEstadoFinanciero
            },
            'win_gestef button[action=aceptar]': {
                click: me.regEstadoFinanciero
            },
            'win_gestef combobox[id=idmodestadofinanciero]': {
                select: function(combo, records, eOpts) {
                    this.modifiedEF = 1;
                    Estadosf.getApplication().getEstadosFinancierosController().clearEstadoFinanciero();
                    Ext.getCmp('tfCodigo').setValue(records[0].data.codigo);
                    Ext.getCmp('tfDenominacion').setValue(records[0].data.denominacion);
                    var arrayData = records[0].data.arrayinfija;
                    Ext.Object.each(arrayData, function(key, value, myself) {
                        Estadosf.getApplication().getEstadosFinancierosController().buildEstadoFinanciero(value.idtipo, value.descripcion, value.idtipoconcepto, value.concat, true);
                    });
                    var tpCtas = Ext.widget('tp_cuentas');
                    tpCtas.getStore().reload();
                }
            },
            'gp_reporte button[action=graficar]': {
                click: function() {
                    if (Ext.getCmp('gpReporte').getSelectionModel().getCount() > 0) {
                        var win = Ext.widget('win_confgrafica', {
                            title: 'Seleccionar la configuraci&oacute;n para mostrar la gr&aacute;fica'
                        });
                        win.show();
                        Ext.getCmp('cbxTipoGrafico').setValue('pie');
                        Ext.getCmp('cbxAlignLegend').enable();
                        Ext.getCmp('cbxAlignLegend').setValue('right');
                        Ext.getCmp('cbxDimGrafico').bindStore(this.stDimChart);
                    } else {
                        mostrarMensaje(1, 'Debe seleccionar las partidas que desea analizar.');
                    }

                }
            }
        });
    },
    onSelectEjercicio: function(cb, records) {
        this.loadMask('Cargando periodos...');
        var form = cb.up('form');
        form.down('combobox[name=idperiodo]').reset();
        form.down('combobox[name=idperiodo]').getStore().load({
            params: {
                idejercicio: records[0].get('idejercicio')
            }
        });
        this.setRangeDate(records[0].get('inicio'), records[0].get('fin'));
        this.loadMask();
    },
    onSelectPeriodo: function(cb, records) {
        this.setRangeDate(records[0].get('inicio'), records[0].get('fin'));
    },
    setRangeDate: function(start, end) {
        var form = this.getFp_filters();

        var dfDesde = form.down('datefield[name=desde]');
        dfDesde.setMinValue(start);
        dfDesde.setValue(start);
        dfDesde.setMaxValue(end);

        var dfHasta = form.down('datefield[name=hasta]');
        dfHasta.setMinValue(start);
        dfHasta.setValue(end);
        dfHasta.setMaxValue(end);
    },
    onPreviewClick: function(btn) {
        var dataSources = this.getGp_reporte().getStore().getProxy().getReader().jsonData.dataSources;
        var win = Ext.widget('printview', {
            dataSources: dataSources
        });
    },
    onClearClick: function(btn) {
        var form = btn.up('form');
        form.getForm().reset();

        var dfDesde = form.down('datefield[name=desde]');
        dfDesde.setMinValue(null);
        dfDesde.setMaxValue(null);

        var dfHasta = form.down('datefield[name=hasta]');
        dfHasta.setMinValue(null);
        dfHasta.setMaxValue(null);

        var cbxEstadosFinanc = form.down('combobox[name=idestadofinanciero]');
        cbxEstadosFinanc.reset();

        this.getGp_reporte().getStore().removeAll();
        form.down('button[action=preview]').disable();
    },
    loadMask: function(mensaje) {
        this.lmask = new Ext.LoadMask(Ext.getBody(), {
            msg: mensaje
        });
        mensaje ? this.lmask.show() : this.lmask.hide();
    },
    formatoMoneda: function(v) {
        if (v == 0 || Ext.isEmpty(v))
            return '0.00';
        v = (Math.round((v - 0) * 100)) / 100;
        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
        v = String(v);
        var ps = v.split(".");
        var whole = ps[0];
        var sub = ps[1] ? "." + ps[1] : ".00";
        var r = /(\d+)(\d{3})/;
        while (r.test(whole)) {
            whole = whole.replace(r, "$1" + "," + "$2");
        }
        v = whole + sub;
        if (v.charAt(0) == "-") {
            return "(" + v.substr(1) + ")";
        }
        return v;
    },
    gestEstadosFinancieros: function() {
        var win = Ext.widget('win_gestef', {
            title: perfil.etiquetas.lbTtlGestEF
        });
        win.show();
    },
    buildEstadoFinanciero: function(tipo, nodo, id, concat, addRecord) {
        var me = this;
        //var add = addRecord || true;
        var recordNodo = new Estadosf.model.Partidas({text: nodo});
        switch (tipo) {
            case '7':
            case 7:
                {
                    var gpPartidas = Ext.widget('gp_partidas');
                    if (!(gpPartidas.getStore().getCount() == 0)) {
//                        var selDel = Ext.getCmp('gpPartidas').getSelectionModel().getSelection();
//                        Ext.getCmp('gpPartidas').getStore().remove(Ext.getCmp('gpPartidas').getStore().last());
                        var selDel = Ext.getCmp('gpPartidas').getSelectionModel().getLastSelected();
                        var posSelDel = Ext.getCmp('gpPartidas').getStore().indexOf(selDel);
//                        console.info(posSelDel);
//                        Ext.getCmp('gpPartidas').getStore().remove(selDel);
//                        this.arrayTipos.pop();
//                        Ext.getCmp('gpPartidas').getStore().removeAll();
//                        console.info(this.arrayTipos[posSelDel]);
//                        console.info(this.arrayTipos.splice(posSelDel, 1));
                        delete this.arrayTipos[posSelDel];
                        var arrayData = this.arrayTipos;
//                        console.info(arrayData);
                        Estadosf.getApplication().getEstadosFinancierosController().clearEstadoFinanciero();
                        var mformulaID,mformulaStr,mformulaCta ;
                        var marrayTipos = new Array(), marrayConcat = new Array();
                        Ext.Object.each(arrayData, function(key, value, myself) {
//                            Estadosf.getApplication().getEstadosFinancierosController().buildEstadoFinanciero(value.idtipo, value.descripcion, value.idtipoconcepto, value.concat, true);
                            mformulaID += ' ' + value.idtipoconcepto;
                            mformulaStr += ' ' + value.descripcion;
                            mformulaCta += ' ' + concat;
                            marrayTipos.push({
                                "idtipoconcepto": value.idtipoconcepto,
                                "idtipo": value.idtipo,
                                "valor": value.descripcion,
                                "descripcion": value.descripcion,
                                "concat": value.concat
                            });
                            marrayConcat.push(value.concat);
                            var recordMod = new Estadosf.model.Partidas({text: value.descripcion});
                            Ext.getCmp('gpPartidas').getStore().add(recordMod);
                        });
                        this.formulaID = mformulaID;
                        this.formulaStr = mformulaStr;
                        this.formulaCta = mformulaCta;
                        this.arrayTipos = marrayTipos;
                        this.arrayConcat = marrayConcat;
                    }
                }
                break;
            case '5':
            case 5:
                {
                    var gpPartidas = Ext.widget('gp_partidas');
                    if (!(gpPartidas.getStore().getCount() == 0)) {
                        if (me.esOperador(gpPartidas.getStore().getAt(gpPartidas.getStore().getCount() - 1).data.text) == false) {
                            Ext.getCmp('gpPartidas').getStore().add(recordNodo);
                            this.formulaID += ' ' + id;
                            this.formulaStr += ' ' + nodo;
                            this.formulaCta += ' ' + nodo;
                            this.arrayTipos.push({
                                "idtipoconcepto": id,
                                "idtipo": tipo, //case '+' || '-' || '*' || '/' || '=' || '':
                                "valor": nodo,
                                "descripcion": nodo,
                                "concat": nodo
                            });
                            this.arrayConcat.push(nodo);
                        } else {
                            mostrarMensaje(1, "Este valor no es admisible.");
                        }
                    }
                }
                break;
            case '1':
            case '2':
            case '3':
            case 1:
            case 2:
            case 3:
                {
                    if (addRecord == true) {
                        Ext.getCmp('gpPartidas').getStore().add(recordNodo);
                    }
                    this.formulaID += ' ' + id;
                    this.formulaStr += ' ' + nodo;
                    this.formulaCta += ' ' + concat;
                    this.arrayTipos.push({
                        "idtipoconcepto": id, //1 agrupaciones de cuentas
                        "idtipo": tipo,
                        "valor": nodo,
                        "descripcion": nodo,
                        "concat": concat
                    });
                    this.arrayConcat.push(concat);
                }
                break;
            default:
                {
                    Ext.getCmp('gpPartidas').getStore().add(recordNodo);
                    this.arrayTipos.push({
                        "idtipoconcepto": id, //1 salto de linea
                        "idtipo": tipo,
                        "valor": nodo,
                        "descripcion": nodo,
                        "concat": concat
                    });
                }
                break;
        }
    },
    clearEstadoFinanciero: function() {
        Ext.getCmp('gpPartidas').getStore().removeAll();
        this.formulaID = '';
        this.formulaStr = '';
        this.formulaCta = '';
        this.arrayTipos = new Array();
        this.arrayConcat = new Array();
    },
    esOperador: function(oper) {
        if (oper == '+' || oper == '-' || oper == '*' || oper == '/' || oper == '=' || oper == '>' || oper == '<' || oper == '%' || oper == '^') {
            return true; //es operador
        } else {
            return false; //no es operador
        }
    },
    clearWinGestEF: function(btn) {
        Ext.getCmp('idmodestadofinanciero').reset();
        Ext.getCmp('tfDenominacion').reset();
        Ext.getCmp('tfCodigo').reset();
        this.modifiedEF = -1;
        Estadosf.getApplication().getEstadosFinancierosController().clearEstadoFinanciero();
    },
    regEstadoFinanciero: function(btn) {
        if (Ext.getCmp('tfCodigo').isValid() && Ext.getCmp('tfDenominacion').isValid()) {
            if (Ext.widget('gp_partidas').getStore().getCount() > 0) {
                var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Guargado estado financiero...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'updateEstadofinanciero',
                    params: {
                        formulamostrada: this.formulaCta,
                        modificado: this.modifiedEF,
                        idmodificado: (this.modifiedEF == 1) ? Ext.getCmp('idmodestadofinanciero').getValue() : 0,
                        nomformula: this.formulaID,
                        formulastr: this.formulaStr,
                        codigo: Ext.getCmp('tfCodigo').getValue(),
                        denominacion: Ext.getCmp('tfDenominacion').getValue(),
                        nodos: Ext.encode(this.arrayTipos),
                        concats: Ext.encode(this.arrayConcat),
                        equal: (this.formulaCta.toString().match(/\=/) != null) ? true : false
                    },
                    success: function(response) {
                        myMask.hide();
                        Estadosf.getApplication().getEstadosFinancierosController().clearWinGestEF();
                        Estadosf.getApplication().getEstadosFinancierosController().getEstadosfStoreEstadosFinancStore().reload();
                        Ext.widget('win_gestef').down('treepanel').getStore().load();
                        Ext.widget('win_gestef').down('button[action=cancelar]').handler.call();
                    }
                });
            } else {
                mostrarMensaje(1, 'Debe configurar el estado financiero.');

            }
        } else {
            mostrarMensaje(1, 'Debe teclear los datos obligatorios.');

        }

    },
    onSearchCalc: function(btn) {
        var me = this, form = btn.up('form'), grid = btn.up('viewport').down('grid'), estadofinanciero = form.down('combobox[name=idestadofinanciero]').getValue();
        var values = form.getForm().getValues();
        this.reconfigureGrid(grid, values['iddimension']);
        values['nodos'] = this.nodos;
        values['titlef'] = this.titlef;
        for (key in values) {
            if (Ext.isArray(values[key])) {
                values[key] = Ext.encode(values[key]);
            }
        }
        grid.getStore().load({
            params: values,
            callback: function(records, operation, success) {
                if (operation.resultSet.count == 0) {
                    mostrarMensaje(1, "No existen resultados para mostrar.");
                }
            }
        });

    },
    reconfigureGrid: function(grid, dimension) {
        var showSI = true;
        var showSP = true;
        var showSA = true;
        var showSD = true;
        var showSEA = true;
        var dimChart = new Array();
        for (iddim in dimension) {
            if (dimension[iddim] == 8030) {
                showSI = false;
                dimChart.push({"iddimension": dimension[iddim], "denominacion": 'Saldo inicial'});
            }
            if (dimension[iddim] == 8031) {
                showSP = false;
                dimChart.push({"iddimension": dimension[iddim], "denominacion": 'Saldo periodo'});
            }
            if (dimension[iddim] == 8032) {
                showSA = false;
                dimChart.push({"iddimension": dimension[iddim], "denominacion": 'Saldo acumulado'});
            }
            if (dimension[iddim] == 8033) {
                showSD = false;
                dimChart.push({"iddimension": dimension[iddim], "denominacion": 'Saldo diferencia'});
            }
            if (dimension[iddim] == 8034) {
                showSEA = false;
                dimChart.push({"iddimension": dimension[iddim], "denominacion": 'Saldo ejericio anterior'});
            }
        }
        this.stDimChart = Ext.create('Ext.data.Store', {
            fields: ['iddimension', 'denominacion'],
            data: dimChart
        });
        var me = this;
        var store = this.getStore('Estadosf.store.Reporte');
        store.on({
            load: {fn: function(st, records, successful) {
                    if (records.length > 0) {
                        this.getFp_filters().down('button[action=preview]').enable();
                    }
                }, scope: this}
        });
        var columns = [{
                text: 'C&oacute;digo',
                tooltip: 'C&oacute;digos de las cuentas contables',
                sortable: false,
                dataIndex: 'concat',
                hidden: true
            }, {
                text: 'Cuenta',
                tooltip: 'Cuentas contables',
                flex: 1,
                sortable: false,
                dataIndex: 'descripcionpartida'
            }, {
                text: 'Saldos',
                tooltip: 'Saldos contables',
                columns: [{
                        text: 'Inicial',
                        tooltip: 'Saldo inicial del per&iacute;odo actual o saldo final del per&iacute;odo anterior',
                        width: 120,
                        sortable: false,
                        dataIndex: 'sinicial',
                        hidden: showSI,
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Per&iacute;odo',
                        tooltip: 'Saldo per&iacute;odo actual',
                        width: 120,
                        sortable: false,
                        dataIndex: 'speriodo',
                        hidden: showSP,
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Acumulado',
                        tooltip: 'Saldo acumulado hasta el per&iacute;odo actual',
                        width: 120,
                        sortable: false,
                        dataIndex: 'sacumulado',
                        hidden: showSA,
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Diferencia',
                        tooltip: 'Saldo diferencia entre el per&iacute;odo actual y el anterior',
                        width: 120,
                        sortable: false,
                        dataIndex: 'sdiferencia',
                        hidden: showSD,
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Ejercicio anterior',
                        tooltip: 'Saldo del ejercicio anterior en el per&iacute;odo actual',
                        width: 120,
                        sortable: false,
                        hidden: showSEA,
                        dataIndex: 'sejercicioanterior',
                        renderer: me.renderRightAlign
                    }]
            }];
        grid.enable();
        grid.reconfigure(store, columns);
    },
    renderRightAlign: function(val) {
        if (val < 0)
            return '<div style="text-align: right;">(' + val * (-1) + ')</div>';
        else
            return '<div style="text-align: right;">' + val + '</div>';
    },
    delEstadoF: function() {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelete, function(btn) {
            if (btn == 'yes') {
                var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Eliminando estado financiero...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteEstadoFinanciero',
                    method: 'POST',
                    params: {
                        idestadof: Ext.getCmp('cbxEstadosFinanc').getValue()
                    },
                    callback: function(options, success, response) {
                        var responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        Ext.getCmp('cbxEstadosFinanc').reset();
                        Ext.getCmp('cbxEstadosFinanc').getStore().load();
                        Estadosf.getApplication().getEstadosFinancierosController().getEstadosfStoreEstadosFinancStore().reload();
                        Ext.widget('win_gestef').down('treepanel').getStore().load();
                    }
                });
            }
        });
    }
});
Ext.apply(Ext.form.field.VTypes, {
    daterange: function(val, field) {
        var date = field.parseDate(val);

        if (!date) {
            return false;
        }
        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = field.up('form').down('#' + field.startDateField);
            start.setMaxValue(date);
            start.validate();
            this.dateRangeMax = date;
        }
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = field.up('form').down('#' + field.endDateField);
            end.setMinValue(date);
            end.validate();
            this.dateRangeMin = date;
        }
        return true;
    },
    daterangeText: 'La fecha de inicio debe ser menor que la fecha fin'
});