Ext.define('Estadosf.view.TpCuentas', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.tp_cuentas',
    store: 'Estadosf.store.CuentaAgrupada',
    useArrows: true,
    rootVisible: false,
    allowDeselect: true,
    frame: false,
    id: 'tpCuentas',
    viewConfig: {
        plugins: {
            ptype: 'treeviewdragdrop',
            ddGroup: '-ddgroup',
            appendOnly: false,
            sortOnDrop: true,
            containerScroll: true
        },
        listeners: {
            drop: function(node, data, overModel, dropPosition) {
                var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('name') : ' on empty view';
            }
        }
    },
    initComponent: function() {
        this.callParent(arguments);
    }
});


