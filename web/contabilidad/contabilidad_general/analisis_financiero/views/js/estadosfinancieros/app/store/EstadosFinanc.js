Ext.define('Estadosf.store.EstadosFinanc', {
    alias: 'widget.st_estadosf',
    extend: 'Ext.data.Store',
    model: 'Estadosf.model.EstadosFinanc',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadDataEstadosFinanc',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});