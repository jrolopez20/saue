Ext.define('Estadosf.store.Ejercicio', {
    alias: 'widget.st_ejercicio',
    extend: 'Ext.data.Store',
    model: 'Estadosf.model.Ejercicio',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadDataEjercicios',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});