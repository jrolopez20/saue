Ext.define('Estadosf.model.Dimension', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'iddimension', type: 'int'
        }, {
            name: 'denominacion'
        }
    ]
});