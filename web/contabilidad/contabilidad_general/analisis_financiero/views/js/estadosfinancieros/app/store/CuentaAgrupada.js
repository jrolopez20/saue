Ext.define('Estadosf.store.CuentaAgrupada', {
    alias: 'widget.st_ctagrupada',
    extend: 'Ext.data.TreeStore',
    model: 'Estadosf.model.CuentaAgrupada',
    folderSort: true,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'loadCuentasAgrupadas',
        reader: {
            idProperty: 'idcuenta'
        },
        actionMethods: {
            read: 'POST'
        }
    }
});