Ext.define('Estadosf.store.Dimension', {
    alias: 'widget.st_dimension',
    extend: 'Ext.data.Store',
    model: 'Estadosf.model.Dimension',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadDimensiones',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});


