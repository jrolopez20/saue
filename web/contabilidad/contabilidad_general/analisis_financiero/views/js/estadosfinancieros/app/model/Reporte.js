Ext.define('Estadosf.model.Reporte', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'Entidad'
        },
        {
            name: 'idestructura'
        },
        {
            name: 'codigoestructura'
        },
        {
            name: 'descripcionestructura'
        },
        {
            name: 'descripcionpartida'
        },
        {
            name: 'concat'
        },
        {
            name: 'partida'
        },
        {
            name: 'idejercicio'
        },
        {
            name: 'ejercicio'
        },
        {
            name: 'idperiodo'
        },
        {
            name: 'periodo'
        },
        {
            name: 'type'
        },
        {
            name: 'sinicial'
        },
        {
            name: 'speriodo'
        },
        {
            name: 'sacumulado'
        },
        {
            name: 'sdiferencia'
        },
        {
            name: 'sejercicioanterior'
        }
    ]
});