Ext.define('Estadosf.view.WinConfGrafica', {
    extend: 'Ext.window.Window',
    alias: 'widget.win_confgrafica',
    id: 'winConfGrafica',
    require: ['Ext.data.*', 'Ext.chart.*', 'Ext.Window', 'Ext.fx.target.Sprite'],
    width: 700,
    height: 220,
    resizable: false,
    modal: true,
    layout: 'border',
    animateTarget: 'btnGraficar',
    initComponent: function(arguments) {
        me = this;
        me.items = [new Ext.form.Panel({
                frame: true,
                id: 'fpTipoGrafico',
                region: 'center',
                labelAlign: 'top',
                items: [new Ext.view.View({
                        id: 'dvImage',
                        style: 'margin:5 0 0 15',
                        store: me.setImageDataView('pie'),
                        tpl: new Ext.XTemplate(
                                '<tpl for=".">',
                                '<div class="thumb-wrap" id="{name}">',
                                '<div class="thumb"><img src="{url}" title="{name}"></div>',
                                '<span class="x-editable">{shortName}</span></div>',
                                '</tpl>',
                                '<div class="x-clear"></div>'
                                ),
                        autoHeight: true,
                        autoShow: true,
                        overClass: 'x-view-over',
                        itemSelector: 'div.thumb-wrap',
                        emptyText: 'No hay imagen para mostrar...'
                    })]
            }), {
                region: 'west',
                xtype: 'fp_confgrafica'
            }];
        me.buttons = [new Ext.Button({
                id: 'btnCancelar',
                text: 'Cancelar',
                tooltip: 'Cancelar la configuraci&oacute;n y cerrar ventana',
                iconCls: "cancelar",
                icon: perfil.dirImg + 'cancelar.png',
                scope: this,
                handler: this.close
            }), new Ext.Button({
                id: 'btnAceptar',
                text: 'Aceptar',
                tooltip: 'Aceptar la configuraci&oacute;n y mostrar la gr&aacute;fica',
                iconCls: "aceptar",
                icon: perfil.dirImg + 'aceptar.png',
                handler: function() {
                    var config = new Object();
                    if (Ext.getCmp('fpConfiguracion').getForm().isValid()) {
                        config.typeChart = Ext.getCmp('cbxTipoGrafico').getValue();
                        config.alignChart = Ext.getCmp('cbxAlignLegend').getValue();
                        config.dimChart = Ext.getCmp('cbxDimGrafico').getValue();
                        me.Graficar(config);
                    } else {
                        mostrarMensaje(1, 'Debe seleccionar la configuraci&oacute;n del gr&aacute;fico que desea ver.');
                    }
                }
            })];
        me.callParent(arguments);
    },
    setImageDataView: function(name) {
        return new Ext.data.Store({
            model: Ext.regModel('State', {fields: [{name: 'url'}, {name: 'name'}]}),
            data: [[perfil.dirImg + name + ".png", name]]
        });
    },
    Graficar: function(config) {
        var gSelections = Ext.getCmp('gpReporte').getSelectionModel().getSelection();
        var chart;
        var gData = new Array();
        var totalSaldo = 0, percentil = 0;
        switch (config.dimChart) {
            case 8030:
                {
                    Ext.each(gSelections, function(item, index) {
                        totalSaldo += item.data.sinicial < 0 ? parseFloat(item.data.sinicial) * -1 : parseFloat(item.data.sinicial);
                    });
                    Ext.each(gSelections, function(item, index) {
                        if (item.data.sinicial < 0)
                        {
                            percentil = ((parseFloat(item.data.sinicial) * -1) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sinicial * -1});
                        } else {
                            percentil = (parseFloat(item.data.sinicial) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sinicial});
                        }
                    });
                }
                break;
            case 8031:
                {
                    Ext.each(gSelections, function(item, index) {
                        totalSaldo += item.data.speriodo < 0 ? parseFloat(item.data.speriodo) * -1 : parseFloat(item.data.speriodo);
                    });
                    Ext.each(gSelections, function(item, index) {
                        if (item.data.speriodo < 0)
                        {
                            percentil = ((parseFloat(item.data.speriodo) * -1) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.speriodo * -1});
                        } else {
                            percentil = (parseFloat(item.data.speriodo) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.speriodo});
                        }
                    });
                }
                break;
            case 8032:
                {
                    Ext.each(gSelections, function(item, index) {
                        totalSaldo += item.data.sacumulado < 0 ? parseFloat(item.data.sacumulado) * -1 : parseFloat(item.data.sacumulado);
                    });
                    Ext.each(gSelections, function(item, index) {
                        if (item.data.sacumulado < 0)
                        {
                            percentil = ((parseFloat(item.data.sacumulado) * -1) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sacumulado * -1});
                        } else {
                            percentil = (parseFloat(item.data.sacumulado) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sacumulado});
                        }
                    });
                }
                break;
            case 8033:
                {
                    Ext.each(gSelections, function(item, index) {
                        totalSaldo += item.data.sdiferencia < 0 ? parseFloat(item.data.sdiferencia) * -1 : parseFloat(item.data.sdiferencia);
                    });
                    Ext.each(gSelections, function(item, index) {
                        if (item.data.sdiferencia < 0)
                        {
                            percentil = ((parseFloat(item.data.sdiferencia) * -1) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sdiferencia * -1});
                        } else {
                            percentil = (parseFloat(item.data.sdiferencia) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sdiferencia});
                        }
                    });
                }
                break;
            case 8034:
                {
                    Ext.each(gSelections, function(item, index) {
                        totalSaldo += item.data.sejercicioanterior < 0 ? parseFloat(item.data.sejercicioanterior) * -1 : parseFloat(item.data.sejercicioanterior);
                    });
                    Ext.each(gSelections, function(item, index) {
                        if (item.data.sejercicioanterior < 0)
                        {
                            percentil = ((parseFloat(item.data.sejercicioanterior) * -1) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sejercicioanterior * -1});
                        } else {
                            percentil = (parseFloat(item.data.sejercicioanterior) / totalSaldo) * 100;
                            gData.push({"name": item.data.partida, "y": percentil, "cant": item.data.sejercicioanterior});
                        }
                    });
                }
                break;
        }
        var storeChart = Ext.create('Ext.data.JsonStore', {
            fields: ['name', 'percentil', 'cant'],
            data: gData
        });
        switch (config.typeChart) {
            case 'pie':
                {
                    var donut = false;
                    chart = new Ext.chart.Chart({
                        xtype: 'chart',
                        animate: true,
                        store: storeChart,
                        shadow: true,
                        background: {
                            gradient: {
                                id: 'gradientId',
                                angle: 45,
                                stops: {
                                    0: {
                                        color: '#666'
                                    },
                                    100: {
                                        color: '#ddd'
                                    }
                                }
                            }
                        },
                        legend: {
                            position: config.alignChart || 'right'
                        },
                        insetPadding: 60,
                        theme: 'Base:gradients',
                        series: [{
                                type: 'pie',
                                field: 'cant',
                                showInLegend: true,
                                highlight: {
                                    segment: {
                                        margin: 20
                                    }
                                },
                                donut: donut,
                                tips: {
                                    trackMouse: true,
                                    width: 400,
                                    height: 65,
                                    renderer: function(storeItem, item) {
                                        var total = 0;
                                        storeChart.each(function(rec) {
                                            total += parseFloat(rec.get('cant'));
                                        });
                                        this.setTitle('<b>Cuenta:</b> ' + storeItem.get('name') + '<br><b>Saldo:</b> $' + parseFloat(storeItem.get('cant')) + '<br><b>Percentil:</b> ' + Math.round(parseFloat(storeItem.get('cant')) / total * 100) + '%');
                                    }
                                },
                                label: {
                                    field: 'name',
                                    display: 'none',
                                    contrast: true,
                                    font: '18px Arial'
                                }
                            }]
                    });
                }
                break;
            case 'column':
                {
                    chart = new Ext.chart.Chart({
                        style: 'background:#fff',
                        animate: true,
                        shadow: true,
                        store: storeChart,
                        background: {
                            gradient: {
                                id: 'gradientId',
                                angle: 45,
                                stops: {
                                    0: {
                                        color: '#999'
                                    },
                                    100: {
                                        color: '#ddd'
                                    }
                                }
                            }
                        },
                        axes: [{
                                type: 'Numeric',
                                position: 'left',
                                fields: ['cant'],
                                label: {
                                    renderer: Ext.util.Format.numberRenderer('0,0')
                                },
                                title: 'Saldos',
                                grid: true,
                                minimum: 0
                            }, {
                                type: 'Category',
                                position: 'bottom',
                                fields: ['name'],
                                title: 'Cuentas'
                            }],
                        series: [{
                                type: 'column',
                                axis: 'left',
                                highlight: true,
                                tips: {
                                    trackMouse: true,
                                    width: 140,
                                    height: 28,
                                    renderer: function(storeItem, item) {
                                        this.setTitle(storeItem.get('name') + ': $' + parseFloat(storeItem.get('cant')));
                                    }
                                },
                                label: {
                                    display: 'insideEnd',
                                    'text-anchor': 'middle',
                                    field: 'cant',
                                    renderer: Ext.util.Format.numberRenderer('0'),
                                    orientation: 'vertical',
                                    color: '#333'
                                },
                                xField: 'name',
                                yField: 'cant'
                            }]
                    });
                }
                break;
            case 'bar':
                {
                    chart = Ext.create('Ext.chart.Chart', {
                        animate: true,
                        store: storeChart,
                        axes: [{
                                type: 'Numeric',
                                position: 'bottom',
                                fields: ['cant'],
                                label: {
                                    renderer: Ext.util.Format.numberRenderer('0,0')
                                },
                                title: 'Saldos contables',
                                grid: true,
                                minimum: 0
                            }, {
                                type: 'Category',
                                position: 'left',
                                fields: ['name'],
                                title: 'Cuentas contables'
                            }],
                        series: [{
                                type: 'bar',
                                axis: 'bottom',
                                highlight: true,
                                tips: {
                                    trackMouse: true,
                                    width: 140,
                                    height: 28,
                                    renderer: function(storeItem, item) {
                                        this.setTitle(storeItem.get('name') + ': $' + parseFloat(storeItem.get('cant')));
                                    }
                                },
                                label: {
                                    display: 'insideEnd',
                                    field: 'cant',
                                    renderer: Ext.util.Format.numberRenderer('0'),
                                    orientation: 'horizontal',
                                    color: '#333',
                                    'text-anchor': 'middle'
                                },
                                xField: 'name',
                                yField: 'cant'
                            }]
                    });
                }
                break;
                /* case 'line':
                 {
                 var store = Ext.create('Ext.data.JsonStore', {
                 fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5'],
                 data: [
                 {'name': 'metric one', 'data1': 10, 'data2': 12, 'data3': 14, 'data4': 8, 'data5': 13},
                 {'name': 'metric two', 'data1': 7, 'data2': 8, 'data3': 16, 'data4': 10, 'data5': 3},
                 {'name': 'metric three', 'data1': 5, 'data2': 2, 'data3': 14, 'data4': 12, 'data5': 7},
                 {'name': 'metric four', 'data1': 2, 'data2': 14, 'data3': 6, 'data4': 1, 'data5': 23},
                 {'name': 'metric five', 'data1': 4, 'data2': 4, 'data3': 36, 'data4': 13, 'data5': 33}
                 ]
                 });
                 chart = Ext.create('Ext.chart.Chart', {
                 width: 500,
                 height: 300,
                 animate: true,
                 store: store,
                 axes: [{
                 type: 'Numeric',
                 position: 'left',
                 fields: ['data1', 'data2'],
                 label: {
                 renderer: Ext.util.Format.numberRenderer('0,0')
                 },
                 title: 'Sample Values',
                 grid: true,
                 minimum: 0
                 }, {
                 type: 'Category',
                 position: 'bottom',
                 fields: ['name'],
                 title: 'Sample Metrics'
                 }],
                 series: [{
                 type: 'line',
                 highlight: {
                 size: 7,
                 radius: 7
                 },
                 axis: 'left',
                 xField: 'name',
                 yField: 'data1',
                 markerConfig: {
                 type: 'cross',
                 size: 4,
                 radius: 4,
                 'stroke-width': 0
                 }
                 }, {
                 type: 'line',
                 highlight: {
                 size: 7,
                 radius: 7
                 },
                 axis: 'left',
                 fill: true,
                 xField: 'name',
                 yField: 'data2',
                 markerConfig: {
                 type: 'circle',
                 size: 4,
                 radius: 4,
                 'stroke-width': 0
                 }
                 }]
                 });
                 }
                 break;*/
        }
        var winGrafica = Ext.create('Ext.Window', {
            id: 'winGrafica',
            title: 'Representaci&oacute;n gr&aacute;fica de los datos seleccionados',
            closable: true,
            width: 1024,
            height: 580,
            layout: 'fit',
            modal: true,
            resizable: false,
            items: chart,
            buttons: [{
                    text: 'Cerrar',
                    icon: perfil.dirImg + 'cerrar.png',
                    iconCls: 'btn',
                    tooltip: 'Cerrar ventana',
                    handler: function() {
                        Ext.getCmp('winGrafica').close();
                    }
                }]
        }).show();
    }
});