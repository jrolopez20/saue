Ext.define('Estadosf.view.GpPartidas', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gp_partidas',
    selModel: Ext.create('Ext.selection.RowModel'),
    store: 'Estadosf.store.Partidas',
    id: 'gpPartidas',
    viewConfig: {
        stripeRows: true,
        enableTextSelection: true,
        plugins: {
            ptype: 'gridviewdragdrop',
            ddGroup: '-ddgroup',
            enableDrag: false
        },
        listeners: {
            drop: function(node, data, dropRec, dropPosition) {
                console.info(arguments);
                var gpPartidas = Ext.widget('gp_partidas');
                    var nodo = data.records[0].data;
                    Estadosf.getApplication().getEstadosFinancierosController().buildEstadoFinanciero(parseInt(nodo.type), nodo.text, nodo.id, nodo.concat, false);
                    var dropRec = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('text') : ' on empty view';
            }
        }
    },
    initComponent: function() {
        var me = this;
        me.columns = [{
                text: 'Partidas',
                flex: 1,
                sortable: false,
                dataIndex: 'text'
            }];
        this.callParent(arguments);
    }
});


