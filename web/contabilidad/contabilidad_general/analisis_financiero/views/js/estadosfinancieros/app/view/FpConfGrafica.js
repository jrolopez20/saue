Ext.define('Estadosf.view.FpConfGrafica', {
    extend: 'Ext.form.Panel',
    alias: 'widget.fp_confgrafica',
    requires: ['Ext.form.*'],
    layout: 'form',
    frame: true,
    id: 'fpConfiguracion',
    width: 510,
    labelAlign: 'top',
    initComponent: function() {
        var me = this;
        Ext.apply(this, {
            items: [{
                    xtype: 'fieldset',
                    title: 'Configuraci&oacute;n del gr&aacute;fico',
                    autoHeight: true,
                    autoWidth: true,
                    defaultType: 'combobox',
                    items: [{
                            id: 'cbxTipoGrafico',
                            fieldLabel: 'Tipo de gr&aacute;fico',
                            store: new Ext.data.Store({
                                model: Ext.regModel('State', {fields: [{name: 'id'}, {name: 'tipo'}]}),
                                data: [['bar', 'Barras'], ['pie', 'Pastel'], ['column', 'Columnas']/*, ['lines', 'Lineas']*/]
                            }),
                            valueField: 'id',
                            value: 'bar',
                            displayField: 'tipo',
                            hiddenName: 'id',
                            anchor: '95%',
                            tabIndex: 1,
                            typeAhead: true,
                            allowBlank: false,
                            queryMode: 'local',
                            emptyText: 'Seleccione...',
                            listeners: {
                                select: function(combo, record, index) {
                                    Ext.getCmp('dvImage').bindStore(me.setImageDataView(combo.getValue()));
                                    if(combo.getValue() == 'pie'){
                                        Ext.getCmp('cbxAlignLegend').enable();
                                    }else{
                                        Ext.getCmp('cbxAlignLegend').disable();
                                    }
                                }
                            }
                        }, {
                            id: 'cbxAlignLegend',
                            fieldLabel: 'Posici&oacute;n de la leyenda',
                            store: new Ext.data.Store({
                                model: Ext.regModel('State', {fields: [{name: 'id'}, {name: 'tipo'}]}),
                                data: [['right', 'Derecha'], ['left', 'Izquierda'], ['top', 'Arriba'], ['bottom', 'Abajo']]
                            }),
                            valueField: 'id',
                            displayField: 'tipo',
                            hiddenName: 'id',
                            anchor: '95%',
                            tabIndex: 2,
                            typeAhead: true,
                            allowBlank: false,
                            queryMode: 'local',
                            emptyText: 'Seleccione...',
                            disabled: true,
                            selectOnFocus: true
                        },{
                            id: 'cbxDimGrafico',
                            fieldLabel: 'Dimensiones',
                            store: 'Estadosf.store.Dimension',
                            valueField: 'iddimension',
                            displayField: 'denominacion',
                            hiddenName: 'iddimension',
                            anchor: '95%',
                            tabIndex: 3,
                            typeAhead: true,
                            allowBlank: false,
                            queryMode: 'local',
                            emptyText: 'Seleccione...'
                        }]
                }]
        });
        this.callParent();
    },
    setImageDataView: function(name) {
        return new Ext.data.Store({
            model: Ext.regModel('State', {fields: [{name: 'url'}, {name: 'name'}]}),
            data: [[perfil.dirImg + name + ".png", name]]
        });
    }
});