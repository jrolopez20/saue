Ext.define('Estadosf.model.EstadosFinanc', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'idestadofinanciero', type: 'int'
        },
        {
            name: 'codigo'
        },
        {
            name: 'denominacion'
        },
        {
            name: 'idestructura'
        },
        {
            name: 'formulaid'
        },
        {
            name: 'formulac'
        },
        {
            name: 'formulap'
        },
        {
            name: 'formulad'
        },
        {
            name: 'formulam'
        },
        {
            name: 'arraypostfija'
        },
        {
            name: 'arrayinfija'
        }
    ]
});