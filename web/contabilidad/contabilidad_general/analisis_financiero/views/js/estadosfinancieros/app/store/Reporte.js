Ext.define('Estadosf.store.Reporte', {
    alias: 'widget.st_reporte',
    extend: 'Ext.data.Store',
    model: 'Estadosf.model.Reporte',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'getReporteFinanciero',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});


