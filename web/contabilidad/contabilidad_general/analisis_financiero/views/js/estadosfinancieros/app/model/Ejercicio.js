Ext.define('Estadosf.model.Ejercicio', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idejercicio', type: 'int'
        },
        {
            name: 'nombre', type: 'string'
        },
        {
            name: 'inicio'
        },
        {
            name: 'fin'
        }
    ]
});