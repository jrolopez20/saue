Ext.define('Estadosf.view.GpReporte', {
    extend: 'Ext.grid.Panel',
    require: ['Ext.selection.Model'],
    id: 'gpReporte',
    alias: 'widget.gp_reporte',
    store: 'Estadosf.store.Reporte',
    columnLines: true,
    disabled: true,
    height: 350,
    title: 'Datos del estado financiero',
    viewConfig: {
        stripeRows: true
    },
    tbar: [{xtype: 'button', id: 'btnGraficar', text: 'Graficar', icon: perfil.dirImg + 'planificacion.png', action: 'graficar'}],
    initComponent: function() {
        var me = this;
        me.selModel = new Ext.selection.RowModel({mode: 'MULTI'});
        me.columns = [{
                text: 'C&oacute;digo',
                tooltip: 'C&oacute;digos de las cuentas contables',
                sortable: false,
                dataIndex: 'concat'
            }, {
                text: 'Cuentas',
                tooltip: 'Cuentas contables',
                flex: 1,
                sortable: false,
                dataIndex: 'descripcionpartida'
            }, {
                text: 'Saldos',
                tooltip: 'Saldos contables',
                columns: [{
                        text: 'Inicial',
                        tooltip: 'Saldo inicial del per&iacute;odo actual o saldo final del per&iacute;odo anterior',
                        width: 75,
                        sortable: false,
                        dataIndex: 'sinicial',
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Per&iacute;odo',
                        tooltip: 'Saldo per&iacute;odo actual',
                        width: 80,
                        sortable: false,
                        dataIndex: 'speriodo',
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Acumulado',
                        tooltip: 'Saldo acumulado hasta el per&iacute;odo actual',
                        width: 100,
                        sortable: false,
                        dataIndex: 'sacumulado',
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Diferencia',
                        tooltip: 'Saldo diferencia entre el per&iacute;odo actual y el anterior',
                        width: 100,
                        sortable: false,
                        dataIndex: 'sdiferencia',
                        renderer: me.renderRightAlign
                    }, {
                        text: 'Ejercicio anterior',
                        tooltip: 'Saldo del ejercicio anterior en el per&iacute;odo actual',
                        width: 100,
                        sortable: false,
                        dataIndex: 'sejercicioanterior',
                        renderer: me.renderRightAlign
                    }]
            }];

        me.callParent();
    },
    renderRightAlign: function(val) {
        if (val < 0)
            return '<div style="text-align: right;">(' + val * (-1)+ ')</div>';
        else
            return '<div style="text-align: right;">' + val + '</div>';
    }
});