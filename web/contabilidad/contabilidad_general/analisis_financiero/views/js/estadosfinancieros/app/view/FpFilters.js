Ext.define('Estadosf.view.FpFilters', {
    extend: 'Ext.form.Panel',
    alias: 'widget.fp_filters',
    bodyPadding: 5,
    collapsible: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    fieldDefaults: {
        labelAlign: 'top'
    },
    initComponent: function() {
        var me = this;
        me.title = perfil.etiquetas.lbFormSearch;
        this.items = [{
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaultType: 'textfield',
                fieldDefaults: {
                    labelAlign: 'top'
                },
                items: [
                    {
                        id: 'cbxEjercicio',
                        xtype: 'combobox',
                        name: 'idejercicio',
                        fieldLabel: 'Ejercicio',
                        store: 'Estadosf.store.Ejercicio',
                        queryMode: 'local',
                        displayField: 'nombre',
                        valueField: 'idejercicio',
                        width: 100,
                        editable: false
                    }, {
                        id: 'cbxPeriodo',
                        xtype: 'combobox',
                        name: 'idperiodo',
                        fieldLabel: 'Periodo',
                        store: 'Estadosf.store.Periodo',
                        queryMode: 'local',
                        displayField: 'nombre',
                        valueField: 'idperiodo',
                        width: 125,
                        editable: false,
                        margins: '0 0 0 5'
                    }, {
                        id: 'dfDesde',
                        xtype: 'datefield',
                        name: 'desde',
                        fieldLabel: 'Desde',
                        width: 125,
                        allowBlank: false,
                        itemId: 'desde',
                        vtype: 'daterange',
                        endDateField: 'hasta',
                        margins: '0 0 0 5'
                    }, {
                        id: 'dfHasta',
                        xtype: 'datefield',
                        name: 'hasta',
                        fieldLabel: 'Hasta',
                        width: 125,
                        allowBlank: false,
                        itemId: 'hasta',
                        vtype: 'daterange',
                        startDateField: 'desde',
                        margins: '0 0 0 5'
                    }, {
                        id: 'cbxDimension',
                        xtype: 'combobox',
                        name: 'iddimension',
                        fieldLabel: 'Saldos',
                        store: 'Estadosf.store.Dimension',
                        queryMode: 'local',
                        displayField: 'denominacion',
                        valueField: 'iddimension',
                        width: 200,
                        editable: false,
                        margins: '0 0 0 5',
                        allowBlank: false,
                        multiSelect: true
                    }, {
                        id: 'cbxEstadosFinanc',
                        xtype: 'combobox',
                        name: 'idestadofinanciero',
                        fieldLabel: 'Estados financieros',
                        store: 'Estadosf.store.EstadosFinanc',
                        queryMode: 'local',
                        displayField: 'denominacion',
                        valueField: 'idestadofinanciero',
                        flex: 1,
                        editable: false,
                        margins: '0 0 0 5',
                        allowBlank: false
                    }, {
//                        disabled: true,
                        xtype: 'button',
                        id: 'btnGestEF',
                        iconCls: 'btn',
                        action: 'gestef',
                        margins: '18 0 0 5',
                        tooltip: perfil.etiquetas.lbTtlGestEF,
                        icon: perfil.dirImg + 'generarconfiguracion.png'
                    }, {
                        disabled: true,
                        xtype: 'button',
                        id: 'btnDelEF',
                        iconCls: 'btn',
                        action: 'delef',
                        margins: '18 0 0 5',
                        icon: perfil.dirImg + 'eliminar.png',
                        tooltip: perfil.etiquetas.lbTtlDelEF
                    }
                ]
            }];
        this.buttons = [
            {
                text: perfil.etiquetas.lbBtnCalc,
                tooltip: perfil.etiquetas.lbTtCalc,
                icon: perfil.dirImg + 'buscar.png',
                iconCls: 'btn',
                formBind: true,
                disabled: true,
                action: 'search'
            },
            {
                text: perfil.etiquetas.lbBtnPreview,
                tooltip: perfil.etiquetas.lbTtPreview,
                icon: perfil.dirImg + 'ver.png',
                iconCls: 'btn',
                disabled: true,
                action: 'preview'
            },
            {
                text: perfil.etiquetas.lbBtnClear,
                tooltip: perfil.etiquetas.lbTtClear,
                icon: perfil.dirImg + 'limpiar.png',
                iconCls: 'btn',
                action: 'clear'
            }];

        this.callParent(arguments);
    }
});