var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();
Ext.application({
    name: 'Estadosf',
    enableQuickTips: true,
    paths: {
        'Estadosf': '../../views/js/estadosfinancieros/app'
    },
    controllers: ['EstadosFinancieros'],
    launch: function() {
        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Cargando...'});
        myMask.show();
        UCID.portal.cargarEtiquetas('estadosfinancieros', function() {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [{
                        xtype: 'fp_filters',
                        region: 'north',
                        margins: '5 5 0 5'
                    }, {
                        xtype: 'gp_reporte',
                        region: 'center'
                    }]
            });
        });
        Ext.Ajax.request({
            url: 'loadDefaultsValues',
            method: 'POST',
            callback: function(options, success, response) {
                var defaultValues = Ext.decode(response.responseText);
                myMask.hide();
                Ext.getCmp('dfDesde').setValue(defaultValues.periodo.inicio);
                Ext.getCmp('dfHasta').setValue(defaultValues.periodo.fin);
            }
        });
    }
});