var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'GrupoContable',

    enableQuickTips: true,

    paths: {
        'GrupoContable': '../../views/js/grupocontable/app'
    },

    controllers: ['GrupoContable'],

    launch: function () {
        UCID.portal.cargarEtiquetas('grupocontable', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'grupocontable_tree',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });

    }
});