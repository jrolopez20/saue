Ext.define('GrupoContable.controller.GrupoContable', {
    extend: 'Ext.app.Controller',
    views: [
        'GrupoContable.view.GrupoContableTree',
        'GrupoContable.view.GrupoContableEdit'
    ],
    stores: [
        'GrupoContable.store.GrupoContable',
        'GrupoContable.store.Formato',
        'GrupoContable.store.Naturaleza'
    ],
    models: [
        'GrupoContable.model.GrupoContable',
        'GrupoContable.store.Naturaleza'
    ],
    refs: [
        {ref: 'grupocontable_tree', selector: 'grupocontable_tree'},
        {ref: 'grupocontable_edit', selector: 'grupocontable_edit'}
    ],
    init: function() {
        var me = this;
        me.control({
            'grupocontable_tree': {
                selectionchange: me.onGrupoSelectionChange
            },
            'grupocontable_tree button[action=adicionar]': {
                click: me.addGrupo
            },
            'grupocontable_tree button[action=modificar]': {
                click: me.modGrupo
            },
            'grupocontable_tree button[action=eliminar]': {
                click: me.delGrupo
            },
            'grupocontable_tree button[action=reload]': {
                click: me.reloadGrupos
            },
            'grupocontable_edit combo[name=idformato]': {
                select: me.onSelectFormato
            },
            'grupocontable_edit button[action=aceptar]': {
                click: me.saveGrupo
            },
            'grupocontable_edit button[action=aplicar]': {
                click: me.saveGrupo
            }
        });

    },
    onGrupoSelectionChange: function(sm) {
        var me = this;
        me.getGrupocontable_tree().down('button[action=modificar]').disable();
        me.getGrupocontable_tree().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getGrupocontable_tree().down('button[action=modificar]').enable();
            if (sm.getLastSelected().get('leaf') == true) {
                me.getGrupocontable_tree().down('button[action=eliminar]').enable();
            }
        }

    },
    addGrupo: function() {
        var win = Ext.widget('grupocontable_edit');
        win.setTitle('Adicionar grupo contable');
        win.down('button[action=aplicar]').show();
        this.loadConfig(win);
    },
    modGrupo: function() {
        var win = Ext.widget('grupocontable_edit');
        win.setTitle('Modificar grupo contable');
        win.down('button[action=aplicar]').hide();
        var form = win.down('form');
        var record = this.getGrupocontable_tree().getSelectionModel().getLastSelected();
        form.loadRecord(record);
        form.getForm().findField('codigo').maxLength = record.get('longitud');
        form.getForm().findField('codigo').minLength = record.get('longitud');

        form.getForm().findField('codigo').setReadOnly(true);
        //form.getForm().findField('idnaturaleza').setReadOnly(true);
        //form.getForm().findField('descripcion').setReadOnly(true);
    },
    delGrupo: function(btn) {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelete, function(btn) {
            if (btn == 'yes') {
                var record = me.getGrupocontable_tree().getSelectionModel().getLastSelected();

                var myMask = new Ext.LoadMask(me.getGrupocontable_tree(), {msg: 'Eliminando grupo contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteGrupoContable',
                    method: 'POST',
                    params: {
                        idgrupo: record.get('idgrupo')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getGrupocontable_tree().down('button[action=modificar]').disable();
                        me.getGrupocontable_tree().down('button[action=eliminar]').disable();
                        me.getGrupocontable_tree().getStore().load();
                    }
                });
            }
        });
    },
    loadConfig: function(win) {
        var me = this,
                nivel = 1,
                node = null;
        var myMask = new Ext.LoadMask(win, {msg: 'Cargando...'});
        myMask.show();

        var form = win.down('form');
        var treeSM = this.getGrupocontable_tree().getSelectionModel();

        if (treeSM.hasSelection()) {
            var record = treeSM.getLastSelected();
            node = record.get('idgrupo');
            nivel = record.get('nivel') + 1;
            form.getForm().findField('idnaturaleza').setValue(record.get('idnaturaleza')); // seteando la naturaleza de grupo padre
        }

        Ext.Ajax.request({
            url: 'loadDataAddGrupo',
            method: 'POST',
            params: {
                node: node,
                nivel: nivel
            },
            callback: function(options, success, response) {
                var response = Ext.decode(response.responseText),
                        form = win.down('form').getForm();
                if ((response.formato && response.formato.length > 0) || (response.nivel && response.nivel.length > 0)) {
                    //Caso en que no se halla configurado todavia el formato para el sistema
                    if (response.formato) {
                        form.findField('idformato').setVisible(true);
                        form.findField('idformato').setDisabled(false);
                        form.findField('idformato').getStore().loadData(response.formato);

                        form.findField('codigo').disable();
                        form.findField('denominacion').disable();
                        form.findField('descripcion').disable();
                        win.down('button[action=aplicar]').hide();
                    } else {
                        if (response.nivel && response.nivel.length > 0) {
                            form.findField('nivel').setValue(response.nivel[0].nivel);
                            form.findField('separador').setValue(response.nivel[0].separador);
                            form.findField('idparteformato').setValue(response.nivel[0].idparteformato);

                            form.findField('codigo').maxLength = response.nivel[0].longitud;
                            form.findField('codigo').minLength = response.nivel[0].longitud;
                        }
                    }
                } else {
                    win.close();
                    Ext.MessageBox.show({
                        title: perfil.etiquetas.lbInformacion,
                        msg: perfil.etiquetas.lbNiveles,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                }
                myMask.hide();
            }
        });
    },
    onSelectFormato: function(cb, records) {
        var form = this.getGrupocontable_edit().down('form').getForm();
        form.findField('codigo').enable();
        form.findField('denominacion').enable();
        form.findField('descripcion').enable();

        form.findField('codigo').setValue('');
        form.findField('codigo').maxLength = records[0].get('nivel')[0].longitud;
        form.findField('codigo').minLength = records[0].get('nivel')[0].longitud;

        form.findField('nivel').setValue(records[0].get('nivel')[0].nivel);
        form.findField('separador').setValue(records[0].get('nivel')[0].separador);
        form.findField('idparteformato').setValue(records[0].get('nivel')[0].idparteformato);
    },
    saveGrupo: function(btn) {
        var me = this,
                concatgrupo = '',
                idgrupopadre = null;
        var form = btn.up('window').down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getGrupocontable_edit(), {msg: 'Guardando grupo...'});
            myMask.show();

            var selectedGrupo = me.getGrupocontable_tree().getSelectionModel().getLastSelected();
            //Caso de adicionar
            if (Ext.isEmpty(form.findField('idgrupo').getValue())) {
                if (selectedGrupo) {
                    concatgrupo = selectedGrupo.get('concatgrupo') + '' + form.findField('separador').getValue();
                    idgrupopadre = selectedGrupo.get('idgrupo');
                }
            } else {
                //Caso de modificar
                if (selectedGrupo.get('idgrupo') != selectedGrupo.get('idgrupopadre')) {
                    var grupoPadre = me.getGrupocontable_tree().getStore().getNodeById(selectedGrupo.get('idgrupopadre'));
                    idgrupopadre = selectedGrupo.get('idgrupopadre');
                    concatgrupo = grupoPadre.get('concatgrupo') + '' + grupoPadre.data.DatParteformato.DatFormato.separador;
                }
            }
            concatgrupo += form.findField('codigo').getValue();

            form.submit({
                clientValidation: true,
                url: 'updateGrupoContable',
                params: {
                    idgrupopadre: idgrupopadre,
                    concatgrupo: concatgrupo
                },
                success: function(response) {
                    myMask.hide();

                    if (btn.action === 'aceptar') {
                        me.getGrupocontable_edit().close();
                        me.getGrupocontable_tree().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    },
    reloadGrupos: function(btn) {
        this.getGrupocontable_tree().getStore().load();
    }

});