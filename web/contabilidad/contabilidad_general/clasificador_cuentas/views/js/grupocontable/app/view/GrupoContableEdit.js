Ext.define('GrupoContable.view.GrupoContableEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.grupocontable_edit',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 450,
    initComponent: function () {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'saveConcepto',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%'
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'combobox',
                    name: 'idformato',
                    fieldLabel: 'Seleccione el formato',
                    store: 'GrupoContable.store.Formato',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'idformato',
                    hidden: true,
                    disabled:true,
                    editable: false,
                    allowBlank: false
                },
                {
                    xtype: 'hidden',
                    name: 'idgrupo'
                },
                {
                    xtype: 'hidden',
                    name: 'nivel'
                },
                {
                    xtype: 'hidden',
                    name: 'separador'
                },
                {
                    xtype: 'hidden',
                    name: 'idparteformato'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [{
                        flex : 1,
                        xtype: 'textfield',
                        maskRe: /^[0-9]+$/,
                        regex: /^[0-9]+$/,
                        name: 'codigo',
                        fieldLabel: perfil.etiquetas.lbCodigo,
                        allowBlank: false
                    }, {
                        xtype: 'combobox',
                        flex : 2,
                        fieldLabel: perfil.etiquetas.lbNaturaleza,
                        name: 'idnaturaleza',
                        store: 'GrupoContable.store.Naturaleza',
                        editable: false,
                        emptyText: perfil.etiquetas.lbSeleccione,
                        triggerAction: 'all',
                        forceSelection: true,
                        allowBlank: false,
                        valueField: 'idnaturaleza',
                        displayField: 'naturaleza',
                        mode: 'local',
                        margins: '0 0 0 5'
                    }]
                },
                {
                    xtype: 'textfield',
                    name: 'denominacion',
                    fieldLabel: perfil.etiquetas.lbDenominacion,
                    maxLength: 255,
                    allowBlank: false
                },
                {
                    xtype: 'textarea',
                    name: 'descripcion',
                    fieldLabel: perfil.etiquetas.lbDescripcion,
                    maxLength: 255
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});