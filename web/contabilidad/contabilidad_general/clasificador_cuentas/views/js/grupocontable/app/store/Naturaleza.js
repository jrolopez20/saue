Ext.define('GrupoContable.store.Naturaleza', {
    extend: 'Ext.data.Store',
    model: 'GrupoContable.model.Naturaleza',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadNaturaleza',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});