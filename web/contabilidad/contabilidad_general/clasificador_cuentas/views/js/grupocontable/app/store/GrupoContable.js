Ext.define('GrupoContable.store.GrupoContable', {
    extend: 'Ext.data.TreeStore',
    model: 'GrupoContable.model.GrupoContable',

    autoLoad: true,
    folderSort: false,
    proxy: {
        type: 'ajax',
        url: 'loadGruposContables',
        actionMethods: {
            read: 'POST'
        }
    }

});