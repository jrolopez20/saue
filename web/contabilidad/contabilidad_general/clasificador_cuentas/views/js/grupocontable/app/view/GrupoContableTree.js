Ext.define('GrupoContable.view.GrupoContableTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.grupocontable_tree',

    store: 'GrupoContable.store.GrupoContable',

    useArrows: true,
    rootVisible: false,
    allowDeselect: true,
    plugins: [{
        ptype: 'treefilter',
        allowParentFolders: true
    }],
    initComponent: function () {
        var me = this;

        me.columns = [
            {
                xtype: 'treecolumn', //this is so we know which column will show the tree
                text: perfil.etiquetas.lbCodigo,
                width: 120,
                sortable: true,
                dataIndex: 'concatgrupo'
            }, {
                text: perfil.etiquetas.lbDenominacion,
                flex: 1,
                dataIndex: 'denominacion',
                sortable: true
            }, {
                text: perfil.etiquetas.lbNaturaleza,
                //flex: 1,
                width: 120,
                dataIndex: 'naturaleza',
                sortable: true
            }, {
                text: perfil.etiquetas.lbDescripcion,
                flex: 2,
                dataIndex: 'descripcion',
                sortable: true
            }
        ];

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdd,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnMod,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnDel,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            },
            {
                text: perfil.etiquetas.lbBtnReload,
                tooltip: perfil.etiquetas.ttpBtnReload,
                icon: perfil.dirImg + 'actualizar.png',
                iconCls: 'btn',
                action: 'reload'
            }, '->',
            {
                xtype: 'trigger',
                emptyText: 'Buscar...',
                width: 250,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function () {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function (f, e) {
                        var me = this,
                            tree = me.up('treepanel');
                        if (e.getKey() == e.ENTER) {
                            tree.getStore().load({
                                params: {
                                    filter: f.getValue()
                                }
                            });
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    },
                    change: function (field, newVal) {
                        var tree = field.up('treepanel');
                        tree.filter(newVal, 'concatgrupo');
                        if (newVal) {
                            this.triggerCell.item(0).setDisplayed(true);
                        } else {
                            this.triggerCell.item(0).setDisplayed(false);
                        }
                        this.updateLayout();
                    }, buffer: 250
                },

                onTrigger1Click: function () {
                    this.reset();
                    this.triggerCell.item(0).setDisplayed(false);
                    this.updateLayout();
                    this.focus();
                },

                onTrigger2Click: function () {
                    var newVal = this.getValue(),
                        tree = this.up('treepanel');

                    tree.filter(newVal, 'concatgrupo');
                    if (newVal) {
                        this.triggerCell.item(0).setDisplayed(true);
                    } else {
                        this.triggerCell.item(0).setDisplayed(false);
                    }
                    this.updateLayout();
                }
            }
        ];
        this.callParent(arguments);
    }
});