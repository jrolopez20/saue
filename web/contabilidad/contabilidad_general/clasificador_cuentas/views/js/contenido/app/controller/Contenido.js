Ext.define('Contenido.controller.Contenido', {
    extend: 'Ext.app.Controller',
    views: [
        'Contenido.view.Grid',
        'Contenido.view.ContenidoEdit'
    ],
    stores: [
        'Contenido.store.Contenido',
        'Contenido.store.Naturaleza',
        'Contenido.store.Grupo',
        'Contenido.store.Formato'
    ],
    models: [
        'Contenido.model.Contenido',
        'Contenido.model.Naturaleza',
        'Contenido.model.Grupo',
        'Contenido.model.Formato'
    ],
    refs: [
        {ref: 'contenido_grid', selector: 'contenido_grid'},
        {ref: 'contenido_edit', selector: 'contenido_edit'}
    ],
    init: function () {
        var me = this;
        me.control({
            'contenido_grid': {
                selectionchange: me.onContenidoSelectionChange,
                itemdblclick: me.onItemDblClick
            },
            'contenido_grid button[action=adicionar]': {
                click: me.addContenido
            },
            'contenido_grid button[action=modificar]': {
                click: me.modContenido
            },
            'contenido_grid button[action=eliminar]': {
                click: me.delContenido
            },
            'contenido_edit button[action=aceptar]': {
                click: me.saveContenido
            },
            'contenido_edit button[action=aplicar]': {
                click: me.saveContenido
            },
            'contenido_edit combo[name=idformato]': {
                select: me.onSelectFormato
            }
        });

    },

    onContenidoSelectionChange: function (sm) {
        var me = this;
        me.getContenido_grid().down('button[action=modificar]').disable();
        me.getContenido_grid().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getContenido_grid().down('button[action=modificar]').enable();
            me.getContenido_grid().down('button[action=eliminar]').enable();
        }
    },

    onItemDblClick: function (view, record, item, index, e) {
        this.getContenido_grid().down('button[action=modificar]').fireHandler();
    },

    addContenido: function () {
        var win = Ext.widget('contenido_edit');
        win.setTitle(perfil.etiquetas.lbWinAddContenido);
        win.down('button[action=aplicar]').show();
        this.loadConfig(win);
    },

    modContenido: function () {
        if (this.getContenido_grid().getSelectionModel().hasSelection()) {
            var win = Ext.widget('contenido_edit');
            win.setTitle(perfil.etiquetas.lbWinModContenido);
            win.down('button[action=aplicar]').hide();
            var form = win.down('form');
            var record = this.getContenido_grid().getSelectionModel().getLastSelected();
            form.loadRecord(record);

            form.getForm().findField('codigoinicio').maxLength = record.get('longitud');
            form.getForm().findField('codigoinicio').minLength = record.get('longitud');
            form.getForm().findField('codigofin').maxLength = record.get('longitud');
            form.getForm().findField('codigofin').minLength = record.get('longitud');
        }
    },

    delContenido: function (btn) {
        var me = this;
        if (this.getContenido_grid().getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelete, function (btn) {
                if (btn == 'yes') {
                    var record = me.getContenido_grid().getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(me.getContenido_grid(), {msg: 'Eliminando grupo contable...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteContenido',
                        method: 'POST',
                        params: {
                            idcontenido: record.get('idcontenido')
                        },
                        callback: function (options, success, response) {
                            responseData = Ext.decode(response.responseText);
                            myMask.hide();
                            me.getContenido_grid().down('button[action=modificar]').disable();
                            me.getContenido_grid().down('button[action=eliminar]').disable();
                            me.getContenido_grid().getStore().load();
                        }
                    });
                }
            });
        }
    },

    saveContenido: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando contenido...'});
            myMask.show();

            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getContenido_grid().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    loadConfig: function (win) {
        var me = this;
        var myMask = new Ext.LoadMask(win, {msg: 'Cargando...'});
        myMask.show();

        Ext.Ajax.request({
            url: 'loadDataAddContenido',
            method: 'POST',
            callback: function (options, success, response) {
                var response = Ext.decode(response.responseText),
                    form = win.down('form').getForm();

                //Caso en que no se halla configurado todavia el formato para el sistema
                if (response.formato) {
                    form.findField('idformato').setVisible(true);
                    form.findField('idformato').setDisabled(false);
                    form.findField('idformato').getStore().loadData(response.formato);

                    form.findField('codigoinicio').disable();
                    form.findField('codigofin').disable();
                    form.findField('denominacion').disable();
                    form.findField('idnaturaleza').disable();
                    form.findField('idgrupo').disable();
                    win.down('button[action=aplicar]').hide();
                } else {
                    if (response.nivel && response.nivel.length > 0) {
                        form.findField('idparteformato').setValue(response.nivel[0].idparteformato);

                        form.findField('codigoinicio').maxLength = response.nivel[0].longitud;
                        form.findField('codigoinicio').minLength = response.nivel[0].longitud;

                        form.findField('codigofin').maxLength = response.nivel[0].longitud;
                        form.findField('codigofin').minLength = response.nivel[0].longitud;
                    }
                }
                myMask.hide();
            }
        });
    },

    onSelectFormato: function (cb, records) {
        var form = this.getContenido_edit().down('form').getForm();
        form.findField('codigoinicio').enable();
        form.findField('codigofin').enable();
        form.findField('denominacion').enable();
        form.findField('idnaturaleza').enable();
        form.findField('idgrupo').enable();

        form.findField('codigoinicio').setValue('');
        form.findField('codigoinicio').maxLength = records[0].get('nivel')[0].longitud;
        form.findField('codigoinicio').minLength = records[0].get('nivel')[0].longitud;

        form.findField('codigofin').setValue('');
        form.findField('codigofin').maxLength = records[0].get('nivel')[0].longitud;
        form.findField('codigofin').minLength = records[0].get('nivel')[0].longitud;

        form.findField('idparteformato').setValue(records[0].get('nivel')[0].idparteformato);
    }

});