Ext.define('Contenido.store.Naturaleza', {
    extend: 'Ext.data.Store',
    model: 'Contenido.model.Naturaleza',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadNaturaleza',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});