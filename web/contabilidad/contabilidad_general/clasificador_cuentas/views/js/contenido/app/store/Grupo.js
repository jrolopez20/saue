Ext.define('Contenido.store.Grupo', {
    extend: 'Ext.data.TreeStore',
    model: 'Contenido.model.Grupo',

    autoLoad: true,
    folderSort: false,
    proxy: {
        type: 'ajax',
        url: 'loadGruposContables',
        actionMethods: {
            read: 'POST'
        }
    },

    root: {
        text: 'Root',
        id: '0'
    }

});