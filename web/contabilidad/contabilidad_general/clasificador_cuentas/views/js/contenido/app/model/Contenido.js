Ext.define('Contenido.model.Contenido', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idcontenido', type: 'int'},
        {name: 'codigoinicio', type: 'string'},
        {name: 'codigofin', type: 'string'},
        {name: 'denominacion', type: 'string'},
        {name: 'idgrupo', type: 'int'},
        {name: 'denomgrupo', type: 'string'},
        {name: 'idnaturaleza', type: 'int'},
        {name: 'naturaleza', type: 'string'},
        {name: 'idparteformato', type: 'int'},
        {name: 'longitud', type: 'int'}
    ]
});