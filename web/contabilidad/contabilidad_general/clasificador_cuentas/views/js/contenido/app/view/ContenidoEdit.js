Ext.define('Contenido.view.ContenidoEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.contenido_edit',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 450,
    initComponent: function () {

        this.cbGrupo = Ext.create('Ext.ux.TreeCombo', {
            fieldLabel: 'Grupo',
            name:'idgrupo',
            treeWidth: 400,
            rootVisible: false,
            store: 'Contenido.store.Grupo',
            selectChildren: false,
            canSelectFolders: false,
            allowBlank: false,
            displayField: 'denomgrupo',
            valueField: 'idgrupo',
            editable: false,
            treeColumns: [{
                xtype: 'treecolumn', //this is so we know which column will show the tree
                text: perfil.etiquetas.lbCodigo,
                width: 200,
                sortable: true,
                dataIndex: 'concatgrupo'
            }, {
                text: perfil.etiquetas.lbDenominacion,
                flex: 1,
                dataIndex: 'denomgrupo',
                sortable: true
            }]
        });

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'updateContenido',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'combobox',
                    name: 'idformato',
                    fieldLabel: 'Seleccione el formato',
                    store: 'Contenido.store.Formato',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'idformato',
                    hidden: true,
                    disabled:true,
                    editable: false,
                    allowBlank: false
                },
                {
                    xtype: 'hidden',
                    name: 'idcontenido'
                },
                {
                    xtype: 'hidden',
                    name: 'idparteformato'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [{
                        flex: 1,
                        name: 'codigoinicio',
                        fieldLabel: perfil.etiquetas.lbCodInicio,
                        maskRe: /^[0-9]+$/,
                        regex: /^[0-9]+$/,
                        allowBlank: false
                    }, {
                        flex: 1,
                        name: 'codigofin',
                        fieldLabel: perfil.etiquetas.lbCodFin,
                        maskRe: /^[0-9]+$/,
                        regex: /^[0-9]+$/,
                        allowBlank: false,
                        margins: '0 0 0 5'
                    }]
                },
                {
                    xtype: 'textfield',
                    name: 'denominacion',
                    fieldLabel: 'Denominación',
                    maxLength: 255,
                    allowBlank: false
                },
                {
                    xtype: 'combobox',
                    fieldLabel: perfil.etiquetas.lbNaturaleza,
                    name: 'idnaturaleza',
                    store: 'Contenido.store.Naturaleza',
                    editable: false,
                    emptyText: 'Seleccione...',
                    triggerAction: 'all',
                    forceSelection: true,
                    allowBlank: false,
                    valueField: 'idnaturaleza',
                    displayField: 'naturaleza',
                    mode: 'local'
                },
                this.cbGrupo
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});