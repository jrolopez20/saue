Ext.define('Contenido.model.Grupo', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idgrupo', type: 'int'
        },
        {
            name: 'codigo', type: 'string'
        },
        {
            name: 'denomgrupo', type: 'string'
        },
        {
            name: 'descripcion', type: 'string'
        },
        {
            name: 'concatgrupo', type: 'string'
        },
        {
            name: 'idgrupopadre', type: 'int'
        },
        {
            name: 'nivel', type: 'int'
        },
        {
            name: 'idarbol', type: 'int'
        },
        {
            name: 'idparteformato', type: 'int'
        },
        {
            name: 'leaf', type: 'bool'
        }
    ]
});