Ext.define('Contenido.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contenido_grid',
    store: 'Contenido.store.Contenido',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{columnName}: {name} ({rows.length} Contenido{[values.rows.length > 1 ? "s" : ""]})',
            hideGroupedHeader: true,
            startCollapsed: false
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.lbBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            },'->',
            {
                xtype: 'searchfield',
                store: 'Contenido.store.Contenido',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['codigoinicio','codigofin', 'denominacion']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbCodInicio,
                dataIndex: 'codigoinicio',
                width: 100
            },
            {
                header: perfil.etiquetas.lbCodFin,
                dataIndex: 'codigofin',
                width: 100
            },
            {
                header: perfil.etiquetas.lbDenominacion,
                dataIndex: 'denominacion',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbNaturaleza,
                dataIndex: 'naturaleza',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbGrupo,
                dataIndex: 'denomgrupo',
                flex: 1
            },
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.features = [groupingFeature];
        this.callParent(arguments);
    }
});