Ext.define('Contenido.store.Contenido', {
    extend: 'Ext.data.Store',
    model: 'Contenido.model.Contenido',

    autoLoad: true,
    pageSize: 25,

    groupField: 'denomgrupo',
    sorters: ['codigoinicio','denomgrupo','denominacion'],

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadContenidos'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});