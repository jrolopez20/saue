var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Contenido',

    enableQuickTips: true,

    paths: {
        'Contenido': '../../views/js/contenido/app'
    },

    controllers: ['Contenido'],

    launch: function () {
        UCID.portal.cargarEtiquetas('contenido', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'contenido_grid',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });

    }
});