Ext.define('Cuenta.store.Naturaleza', {
    extend: 'Ext.data.Store',
    model: 'Cuenta.model.Naturaleza',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadNaturaleza',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});