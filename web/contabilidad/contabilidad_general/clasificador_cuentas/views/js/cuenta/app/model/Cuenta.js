Ext.define('Cuenta.model.Cuenta', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idcuenta', type: 'int'
        },
        {
            name: 'codigo'
        },
        {
            name: 'concatcta'
        },
        {
            name: 'denominacion', type: 'string'
        },
        {
            name: 'privado', type: 'bool'
        },
        {
            name: 'nivel', type: 'int'
        },
        {
            name: 'idcontenido', type: 'int'
        },
        {
            name: 'idparteformato', type: 'int'
        },
        {
            name: 'denomcontenido', type: 'string'
        },
        {
            name: 'idcuentapadre', type: 'int'
        },
        {
            name: 'idnaturaleza', type: 'int'
        },
        {
            name: 'naturaleza', type: 'string'
        },
        {
            name: 'idestructura', type: 'int'
        },
        {
            name: 'leaf', type: 'bool'
        },
        {
            name: 'longitud', type: 'int'
        },
        {
            name: 'separador', type: 'string'
        },
        {
            name: 'activa'
        },
        {
            name: 'idgrupo', type: 'int'
        },
        /* Estos son los nuevos */
        {
            name: 'allowaction', type: 'int'
        },
        {
            name: 'children'
        },
        {
            name: 'expanded'
        },
        {
            name: 'qtip'
        },
        {
            name: 'nodetype', type: 'int'
        },
        {
            name: 'text', type: 'string'
        }
    ]
});