Ext.define('Cuenta.store.Cuenta', {
    extend: 'Ext.data.TreeStore',
    model: 'Cuenta.model.Cuenta',

    autoLoad: true,
    folderSort: false,
    storeId: 'stTree',
    proxy: {
        type: 'ajax',
        url: 'getTreeCuentas',
        //url: 'loadCuentas',
        reader: {
            idProperty:'idcuenta'
            //root: 'datos',
            //totalProperty: 'cant',
            //successProperty: 'success'
        },
        actionMethods: {
            read: 'POST'
        }
    }
});