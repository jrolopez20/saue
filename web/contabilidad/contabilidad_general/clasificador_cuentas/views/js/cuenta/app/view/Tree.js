Ext.define('Cuenta.view.Tree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.cuenta_tree',
    store: 'Cuenta.store.Cuenta',
    useArrows: true,
    rootVisible: false,
    allowDeselect: true,
    viewConfig: {
        getRowClass: function(record, rowIndex, rowParams, store) {
            return record.get("nodetype") ? "x-grid-group-hd" : "";
        }
    },
    initComponent: function() {
        var me = this;

        me.columns = [
            {
                xtype: 'treecolumn', //this is so we know which column will show the tree
                text: perfil.etiquetas.lbCodigo,
                //width: 200,
                flex: 1,
                sortable: true,
                //dataIndex: 'concatcta',
                dataIndex: 'text'
                        //tdCls: 'x-grid-group-hd'
            }, {
                text: perfil.etiquetas.lbNaturaleza,
                flex: 1,
                dataIndex: 'naturaleza',
                sortable: true
                        //tdCls: 'x-grid-group-hd'
            }, {
                header: 'Activa',
                dataIndex: 'activa',
                width: 55,
                renderer: function(val, metaData, record) {
                    if (record.get('nodetype')) {
                        return;
                    }
                    else if (val == 1) {
                        return '<img src="' + perfil.dirImg + 'banderaverde.png" />';
                    } else {
                        return '<img src="' + perfil.dirImg + 'banderaroja.png" />';
                    }
                }
            }];

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdd,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'adicionar'
            },
            {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnMod,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnDel,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            },
            {
                text: perfil.etiquetas.lbBtnActivar,
                tooltip: perfil.etiquetas.ttpBtnActivar,
                icon: perfil.dirImg + 'banderaverde.png',
                iconCls: 'btn',
                disabled: true,
                action: 'activar'
            },
            {
                text: perfil.etiquetas.lbBtnReload,
                tooltip: perfil.etiquetas.ttpBtnReload,
                icon: perfil.dirImg + 'actualizar.png',
                iconCls: 'btn',
                action: 'reload'
            },
            ,
            {
                text: perfil.etiquetas.lbBtnPreview,
                tooltip: perfil.etiquetas.ttpBtnPreview,
                icon: perfil.dirImg + 'ver.png',
                iconCls: 'btn',
                action: 'preview'
            }, '->',
            {
                xtype: 'triggerfield',
                emptyText: 'Buscar...',
                width: 250,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function() {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function(f, e) {
                        var me = this,
                                tree = me.up('treepanel');
                        if (e.getKey() == e.ENTER) {
                            tree.getStore().load({
                                params: {
                                    filter: f.getValue()
                                }
                            });
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    }
                },
                onTrigger1Click: function() {
                    var me = this,
                            tree = me.up('treepanel');
                    me.setValue('');
                    tree.getStore().load();
                    me.triggerCell.item(0).setDisplayed(false);
                    me.updateLayout();
                },
                onTrigger2Click: function() {
                    var me = this,
                            value = me.getValue(),
                            tree = me.up('treepanel');

                    if (value != null) {
                        tree.getStore().load({
                            params: {
                                filter: value
                            }
                        });
                        me.triggerCell.item(0).setDisplayed(true);
                        me.updateLayout();
                    }
                }
            }
        ];

        this.callParent(arguments);
    }
});