Ext.define('Cuenta.store.Grupo', {
    extend: 'Ext.data.Store',
    model: 'Cuenta.model.Grupo',

    autoLoad: true,
    folderSort: false,
    proxy: {
        type: 'rest',
        url: 'loadGrupos',
        actionMethods: {
            read: 'POST'
        }
    }

});