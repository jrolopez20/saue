Ext.define('Cuenta.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.cuenta_edit',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 500,
    initComponent: function () {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'updateCuentaContable',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%'
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'idcuenta'
                },
                {
                    xtype: 'hidden',
                    name: 'idparteformato'
                },
                //{
                //    xtype: 'hidden',
                //    name: 'idnaturaleza'
                //},
                {
                    xtype: 'hidden',
                    name: 'nivel'
                },
                {
                    xtype: 'combobox',
                    name: 'idformato',
                    fieldLabel: 'Seleccione el formato',
                    store: 'Cuenta.store.Formato',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'idformato',
                    hidden: true,
                    disabled: true,
                    editable: false,
                    allowBlank: false
                },
                //{
                //    xtype: 'combobox',
                //    name: 'idcontenido',
                //    fieldLabel: perfil.etiquetas.lbContenidoEconomico,
                //    store: 'Cuenta.store.Contenido',
                //    queryMode: 'local',
                //    displayField: 'denominacion',
                //    valueField: 'idcontenido',
                //    editable: false,
                //    allowBlank: false
                //},
                {
                    xtype: 'combobox',
                    fieldLabel: 'Grupo contable',
                    name: 'idgrupo',
                    store: 'Cuenta.store.Grupo',
                    editable: false,
                    emptyText: perfil.etiquetas.lbSeleccione,
                    triggerAction: 'all',
                    forceSelection: true,
                    allowBlank: false,
                    valueField: 'idgrupo',
                    displayField: 'text',
                    mode: 'local'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [{
                        flex: 1,
                        xtype: 'textfield',
                        maskRe: /^[0-9]+$/,
                        regex: /^[0-9]+$/,
                        name: 'codigo',
                        fieldLabel: perfil.etiquetas.lbCodigo,
                        allowBlank: false
                    }, {
                        xtype: 'combobox',
                        fieldLabel: perfil.etiquetas.lbNaturaleza,
                        name: 'idnaturaleza',
                        flex: 1,
                        margins: '0 0 0 5',
                        store: 'Cuenta.store.Naturaleza',
                        editable: false,
                        emptyText: perfil.etiquetas.lbSeleccione,
                        triggerAction: 'all',
                        forceSelection: true,
                        allowBlank: false,
                        valueField: 'idnaturaleza',
                        displayField: 'naturaleza',
                        mode: 'local'
                    }, /*{
                     flex: 2,
                     xtype: 'textfield',
                     name: 'naturaleza',
                     fieldLabel: perfil.etiquetas.lbNaturaleza,
                     readOnly: true,
                     margins: '0 0 0 5'
                     }*/]
                },
                {
                    xtype: 'textarea',
                    name: 'denominacion',
                    fieldLabel: perfil.etiquetas.lbDenominacion,
                    allowBlank: false,
                    maxLength: 255
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});