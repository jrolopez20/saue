Ext.define('CmpCuentaTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.cuenta_tree',
    useArrows: true,
    rootVisible: false,
    allowDeselect: true,
    checked: false,
    idnaturaleza: [8030, 8031, 8032],
    anySelection: true,
    onlyLeafSelection: false,
    saldo: false, //Permite mostrar en la respuesta el saldo de la cuenta o no
    initComponent: function() {
        var me = this;
        Ext.define('Cuenta', {
            extend: 'Ext.data.Model',
            fields: [
                {
                    name: 'idcuenta', type: 'int'
                },
                {
                    name: 'codigo'
                },
                {
                    name: 'concatcta'
                },
                {
                    name: 'denominacion', type: 'string'
                },
                {
                    name: 'privado', type: 'bool'
                },
                {
                    name: 'nivel', type: 'int'
                },
                {
                    name: 'idcontenido', type: 'int'
                },
                {
                    name: 'idparteformato', type: 'int'
                },
                {
                    name: 'denomcontenido', type: 'string'
                },
                {
                    name: 'idcuentapadre', type: 'int'
                },
                {
                    name: 'idnaturaleza', type: 'int'
                },
                {
                    name: 'naturaleza', type: 'string'
                },
                {
                    name: 'idestructura', type: 'int'
                },
                {
                    name: 'leaf', type: 'bool'
                },
                {
                    name: 'longitud', type: 'int'
                },
                {
                    name: 'separador', type: 'string'
                },
                {
                    name: 'activa'
                },
                {
                    name: 'idgrupo', type: 'int'
                },
                /* Estos son los nuevos */
                {
                    name: 'allowaction', type: 'int'
                },
                {
                    name: 'children'
                },
                {
                    name: 'expanded'
                },
                {
                    name: 'qtip'
                },
                {
                    name: 'nodetype', type: 'int'
                },
                {
                    name: 'text', type: 'string'
                },
                {
                    name: 'saldocuenta', type: 'float'
                }
            ]
        });

        me.store = Ext.create('Ext.data.TreeStore', {
            model: 'Cuenta',
            autoLoad: true,
            folderSort: false,
            proxy: {
                type: 'ajax',
                url: '/contabilidad/contabilidad_general/clasificador_cuentas/index.php/cuenta/getTreeCuentas',
                //url: 'loadCuentas',
                reader: {
                    idProperty: 'idcuenta'
                },
                actionMethods: {
                    read: 'POST'
                }
            },
            listeners: {
//                scope: me,
                beforeload: function(store, operation) {
                    if (me.checked) {
                        operation.params.checked = me.checked;
                    }
                    operation.params.saldo = me.saldo;
                    operation.params.idnaturaleza = Ext.encode(me.idnaturaleza);
                }
            }
        });

        me.columns = [
            {
                xtype: 'treecolumn', //this is so we know which column will show the tree
                text: perfil.etiquetas.lbCodigo,
                flex: 1,
                sortable: true,
                //dataIndex: 'concatcta',
                dataIndex: 'text'
                //tdCls: 'x-grid-group-hd'
            }, {
                text: 'Naturaleza',
                flex: 1,
                dataIndex: 'naturaleza',
                sortable: true
                //tdCls: 'x-grid-group-hd'
            }];

        me.tbar = [
            '->',
            {
                xtype: 'triggerfield',
                emptyText: 'Buscar...',
                width: 250,
                trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                enableKeyEvents: true,
                listeners: {
                    afterRender: function() {
                        this.triggerCell.item(0).setDisplayed(false);
                    },
                    keypress: function(f, e) {
                        var me = this,
                                tree = me.up('treepanel');
                        if (e.getKey() == e.ENTER) {
                            tree.getStore().load({
                                params: {
                                    filter: f.getValue()
                                }
                            });
                            me.triggerCell.item(0).setDisplayed(true);
                            me.updateLayout();
                        }
                    }
                },
                onTrigger1Click: function() {
                    var me = this,
                            tree = me.up('treepanel');
                    me.setValue('');
                    tree.getStore().load();
                    me.triggerCell.item(0).setDisplayed(false);
                    me.updateLayout();
                },
                onTrigger2Click: function() {
                    var me = this,
                            value = me.getValue(),
                            tree = me.up('treepanel');

                    if (value != null) {
                        tree.getStore().load({
                            params: {
                                filter: value
                            }
                        });
                        me.triggerCell.item(0).setDisplayed(true);
                        me.updateLayout();
                    }
                }
            }
        ];

        me.on({
            beforeselect: function(tree, record, index, eOpts) {
                /* Si es un grupo */
                if (!me.anySelection && record.get('nodetype')) {
                    return false;
                }

                if(me.onlyLeafSelection && !record.get('leaf')){
                    return false;
                }
            }
        });

        this.callParent(arguments);
    }
});