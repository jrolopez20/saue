Ext.define('Cuenta.model.Formato', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idformato', type: 'int'
        },
        {
            name: 'nombre', type: 'string'
        },
        {
            name: 'descripcion', type: 'string'
        },
        {
            name: 'separador', type: 'string'
        },
        {
            name: 'idestructuracomun', type: 'int'
        },
        {
            name: 'estandar', type: 'int'
        },
        {
            name: 'idsubsistema', type: 'int'
        },
        {
            name: 'nivel'
        }
    ]
});