Ext.define('Cuenta.store.Contenido', {
    extend: 'Ext.data.Store',
    model: 'Cuenta.model.Contenido',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        api: {
            read: 'loadContenidos'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});