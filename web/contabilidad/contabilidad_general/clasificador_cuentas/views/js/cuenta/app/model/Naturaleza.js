Ext.define('Cuenta.model.Naturaleza', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idnaturaleza', type: 'int'},
        {name: 'naturaleza', type: 'string'}
    ]
});