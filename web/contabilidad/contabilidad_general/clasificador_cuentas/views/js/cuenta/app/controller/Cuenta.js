Ext.define('Cuenta.controller.Cuenta', {
    extend: 'Ext.app.Controller',
    views: [
        'Cuenta.view.Tree',
        'Cuenta.view.Edit'
    ],
    stores: [
        'Cuenta.store.Cuenta',
        'Cuenta.store.Contenido',
        'Cuenta.store.Formato',
        'Cuenta.store.Naturaleza',
        'Cuenta.store.Grupo'
    ],
    models: [
        'Cuenta.model.Cuenta',
        'Cuenta.model.Contenido',
        'Cuenta.model.Formato',
        'Cuenta.model.Naturaleza',
        'Cuenta.model.Grupo'
    ],
    refs: [
        {ref: 'cuenta_tree', selector: 'cuenta_tree'},
        {ref: 'cuenta_edit', selector: 'cuenta_edit'}
    ],
    init: function() {
        var me = this;
        this.listen({
            component: {
                //'cuenta_tree': {
                //    beforeselect: me.onTreeBeforeSelect
                //},
                'cuenta_tree': {
                    selectionchange: me.onCuentaSelectionChange
                },
                'cuenta_tree button[action=adicionar]': {
                    click: me.addCuenta
                },
                'cuenta_tree button[action=modificar]': {
                    click: me.modCuenta
                },
                'cuenta_tree button[action=eliminar]': {
                    click: me.delCuenta
                },
                'cuenta_tree button[action=activar]': {
                    click: me.activeCuenta
                },
                'cuenta_tree button[action=reload]': {
                    click: me.reloadCuentas
                },
                'cuenta_tree button[action=preview]': {
                    click: me.previewPlanCuenta
                },
                'cuenta_edit button[action=aceptar]': {
                    click: me.saveCuenta
                },
                'cuenta_edit button[action=aplicar]': {
                    click: me.saveCuenta
                },
                //'cuenta_edit combo[name=idcontenido]': {
                //    select: me.onSelectContenido
                //},
                'cuenta_edit combo[name=idformato]': {
                    select: me.onSelectFormato
                }
                //'cuenta_tree': {
                //    beforerender: me.prueba
                //}
            }
        });

    },
    //prueba: function (tree, eOpts) {
    //    tree.getView().getRowClass = function (record, index, rowParams, store) {
    //        if (record.get('nodetype')) {
    //            return 'x-grid-group-hd';
    //        } else {
    //            return '';
    //        }
    //    };
    //},

    onCuentaSelectionChange: function(sm) {
        var me = this;
        me.getCuenta_tree().down('button[action=modificar]').disable();
        me.getCuenta_tree().down('button[action=eliminar]').disable();
        me.getCuenta_tree().down('button[action=activar]').disable();

        if (sm.hasSelection()) {
            var record = sm.getLastSelected();
            /* esta es nuevo */
            me.getCuenta_tree().down('button[action=adicionar]').setDisabled(!record.get('allowaction'));

            if (sm.getLastSelected().get('leaf') == true) {
                me.getCuenta_tree().down('button[action=modificar]').enable();
                me.getCuenta_tree().down('button[action=eliminar]').enable();
                var btnActivar = me.getCuenta_tree().down('button[action=activar]');
                btnActivar.enable();

                if (record.get('activa') == 1) {
                    btnActivar.setText(perfil.etiquetas.lbBtnDesactivar);
                    btnActivar.setTooltip(perfil.etiquetas.ttpBtnDesactivar);
                    btnActivar.setIcon(perfil.dirImg + 'banderaroja.png');
                } else {
                    btnActivar.setText(perfil.etiquetas.lbBtnActivar);
                    btnActivar.setTooltip(perfil.etiquetas.ttpBtnActivar);
                    btnActivar.setIcon(perfil.dirImg + 'banderaverde.png');
                }
            }
        }
    },
    //onCuentaSelectionChange: function (sm) {
    //    var me = this;
    //    me.getCuenta_tree().down('button[action=modificar]').disable();
    //    me.getCuenta_tree().down('button[action=eliminar]').disable();
    //    me.getCuenta_tree().down('button[action=activar]').disable();
    //
    //    if (sm.hasSelection()) {
    //        if (sm.getLastSelected().get('leaf') == true) {
    //            me.getCuenta_tree().down('button[action=modificar]').enable();
    //            me.getCuenta_tree().down('button[action=eliminar]').enable();
    //            var btnActivar = me.getCuenta_tree().down('button[action=activar]');
    //            btnActivar.enable();
    //
    //            var record = sm.getLastSelected();
    //            if (record.get('activa') == 1) {
    //                btnActivar.setText(perfil.etiquetas.lbBtnDesactivar);
    //                btnActivar.setTooltip(perfil.etiquetas.ttpBtnDesactivar);
    //                btnActivar.setIcon(perfil.dirImg + 'banderaroja.png');
    //            } else {
    //                btnActivar.setText(perfil.etiquetas.lbBtnActivar);
    //                btnActivar.setTooltip(perfil.etiquetas.ttpBtnActivar);
    //                btnActivar.setIcon(perfil.dirImg + 'banderaverde.png');
    //            }
    //        }
    //    }
    //},

    addCuenta: function() {
        var win = Ext.widget('cuenta_edit');
        win.setTitle('Adicionar cuenta contable');
        win.down('button[action=aplicar]').show();
        this.loadConfig(win);
    },
    modCuenta: function() {
        var win = Ext.widget('cuenta_edit');
        win.setTitle('Modificar cuenta contable');
        win.down('button[action=aplicar]').hide();
        var form = win.down('form');
        var record = this.getCuenta_tree().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },
    delCuenta: function(btn) {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelete, function(btn) {
            if (btn == 'yes') {
                var record = me.getCuenta_tree().getSelectionModel().getLastSelected();
                var cuentaPadre = me.getCuenta_tree().getStore().getNodeById(record.get('idcuenta'));

                var myMask = new Ext.LoadMask(me.getCuenta_tree(), {msg: 'Eliminando cuenta contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteCuentaContable',
                    method: 'POST',
                    params: {
                        idcuenta: record.get('idcuenta')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getCuenta_tree().down('button[action=modificar]').disable();
                        me.getCuenta_tree().down('button[action=eliminar]').disable();
                        me.getCuenta_tree().getStore().reload(cuentaPadre);
                        //me.getCuenta_tree().getStore().load();
                    }
                });
            }
        });
    },
    activeCuenta: function(btn) {
        var me = this;
        var record = me.getCuenta_tree().getSelectionModel().getLastSelected();
        var msConfirm = perfil.etiquetas.msgConfirmActivar;

        if (record.get('activa') == 1) {
            msConfirm = perfil.etiquetas.msgConfirmDesactivar;
        }

        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, msConfirm, function(btn) {
            if (btn == 'yes') {
                var myMask = new Ext.LoadMask(me.getCuenta_tree(), {msg: 'Guardando estado de la cuenta contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'activarCuenta',
                    method: 'POST',
                    params: {
                        idcuenta: record.get('idcuenta'),
                        activa: 1 - record.get('activa')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getCuenta_tree().down('button[action=modificar]').disable();
                        me.getCuenta_tree().down('button[action=eliminar]').disable();
                        me.getCuenta_tree().down('button[action=activar]').disable();
                        var cuentaPadre = me.getCuenta_tree().getStore().getNodeById(record.get('idcuenta'));
                        me.getCuenta_tree().getStore().reload(cuentaPadre);
                        //me.getCuenta_tree().getStore().load();
                    }
                });
            }
        });
    },
    reloadCuentas: function(btn) {
        this.getCuenta_tree().getStore().load();
    },
    previewPlanCuenta: function() {
        var me = this;
        var myMask = new Ext.LoadMask(me.getCuenta_tree(), {msg: perfil.etiquetas.msgLoadingDataReport});
        myMask.show();
        Ext.Ajax.request({
            url: 'vistaPreviaPlanCuenta',
            method: 'POST',
            callback: function(options, success, response) {
                var r = Ext.decode(response.responseText);
                var arrPost = {
                    datoGeneral: {reporte: 10008},
                    datoCuerpo: r.datos
                };

                myMask.hide();
                var win = Ext.widget('printview', {
//                    dataSources: arrPost
                    dataSources: r.dataSources[0]
                });
            }
        });
    },
    saveCuenta: function(btn) {
        var me = this,
                concatcta = '',
                idcuentapadre = null,
                win = btn.up('window'),
                form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando cuenta contable...'});
            myMask.show();

            var selectedCuenta = me.getCuenta_tree().getSelectionModel().getLastSelected();
            //Caso de adicionar
            if (Ext.isEmpty(form.findField('idcuenta').getValue())) {
                if (selectedCuenta) {
                    concatcta = selectedCuenta.get('concatcta') + '' + selectedCuenta.get('separador');
                    idcuentapadre = selectedCuenta.get('idcuenta');
                }
                var cuentaPadre = me.getCuenta_tree().getStore().getNodeById(idcuentapadre);
            } else {
                //Caso de modificar
                if (selectedCuenta.get('idcuenta') != selectedCuenta.get('idcuentapadre')) {
                    var cuentaPadre = me.getCuenta_tree().getStore().getNodeById(selectedCuenta.get('idcuentapadre'));
                    concatcta = cuentaPadre.get('concatcta') + '' + cuentaPadre.get('separador');
                } else {
                    /* si es un cuenta a primer nivel e hija de un subgrupo*/
                    var cuentaPadre = me.getCuenta_tree().getStore().getNodeById(selectedCuenta.get('idgrupo'));
                    concatcta = cuentaPadre.get('concatcta') + '' + cuentaPadre.get('separador');
                }
                idcuentapadre = selectedCuenta.get('idcuentapadre');
            }
            concatcta += form.findField('codigo').getValue();

            form.submit({
                submitEmptyText: false,
                params: {
                    idcuentapadre: idcuentapadre,
                    concatcta: concatcta
                },
                success: function(response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getCuenta_tree().getStore().reload(cuentaPadre);
                    }
                    else {
                        form.reset();
                        //Vuelve a resetear los valores por defecto del formato
                        form.findField('nivel').setValue(me.nivel);
                        form.findField('idparteformato').setValue(me.idparteformato);
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    },
    //onSelectContenido: function (cb, records) {
    //    var form = this.getCuenta_edit().down('form').getForm();
    //    //form.findField('naturaleza').setValue(records[0].get('naturaleza'));
    //    //form.findField('idnaturaleza').setValue(records[0].get('idnaturaleza'));
    //},

    onSelectFormato: function(cb, records) {
        var form = this.getCuenta_edit().down('form').getForm();
//        form.findField('idcontenido').enable();
        form.findField('codigo').enable();
        form.findField('naturaleza').enable();
        form.findField('denominacion').enable();

        form.findField('codigo').setValue('');
        form.findField('codigo').maxLength = records[0].get('nivel')[0].longitud;
        form.findField('codigo').minLength = records[0].get('nivel')[0].longitud;
        form.findField('idparteformato').setValue(records[0].get('nivel')[0].idparteformato);
    },
    loadConfig: function(win) {
        var me = this,
                nivel = 1,
                form = win.down('form');

        var myMask = new Ext.LoadMask(win, {msg: 'Cargando...'});
        myMask.show();

        var record = me.getCuenta_tree().getSelectionModel().getLastSelected();

        if (me.getCuenta_tree().getSelectionModel().hasSelection() && !record.get('nodetype')) {
            nivel = record.get('nivel') + 1;
        }
        win.down('form').getForm().findField('nivel').setValue(nivel);

        /*setenadole la naturaleza y el grupo del grupo selecionado */
        form.getForm().findField('idgrupo').setValue(record.get('idgrupo'));
        form.getForm().findField('idgrupo').setReadOnly(record.get('idgrupo')); // si tiene grupo
        form.getForm().findField('idnaturaleza').setValue(record.get('idnaturaleza'));

        Ext.Ajax.request({
            url: 'loadDataAddCuenta',
            method: 'POST',
            params: {
                nivel: nivel
            },
            callback: function(options, success, response) {
                var response = Ext.decode(response.responseText),
                        form = win.down('form').getForm();

                //Caso en que no se halla configurado todavia el formato para el sistema
                if (response.formato) {
                    form.findField('idformato').setVisible(true);
                    form.findField('idformato').setDisabled(false);
                    form.findField('idformato').getStore().loadData(response.formato);

//                    form.findField('idcontenido').disable();
                    form.findField('codigo').disable();
                    form.findField('denominacion').disable();
                    form.findField('naturaleza').disable();
                    win.down('button[action=aplicar]').hide();
                } else {
                    if (response.nivel && response.nivel.length > 0) {
                        me.nivel = response.nivel[0].nivel;
                        me.idparteformato = response.nivel[0].idparteformato;
                        form.findField('nivel').setValue(response.nivel[0].nivel);
                        form.findField('idparteformato').setValue(response.nivel[0].idparteformato);

                        form.findField('codigo').maxLength = response.nivel[0].longitud;
                        form.findField('codigo').minLength = response.nivel[0].longitud;
                    }
                }
                myMask.hide();
            }
        });
    }
});