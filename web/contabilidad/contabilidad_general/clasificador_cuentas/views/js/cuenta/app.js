var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();
Ext.application({
    name: 'Cuenta',
    enableQuickTips: true,
    paths: {
        'Cuenta': '../../views/js/cuenta/app'
    },
    controllers: ['Cuenta'],
    launch: function () {
        UCID.portal.cargarEtiquetas('cuenta', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'cuenta_tree',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});