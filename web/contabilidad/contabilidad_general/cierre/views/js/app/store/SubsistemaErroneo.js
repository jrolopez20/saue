Ext.define('Cierre.store.SubsistemaErroneo', {
    extend: 'Ext.data.Store',
    model: 'Cierre.model.SubsistemaErroneo',
    autoLoad: false,
    sorters: [{
        property: 'precendencia',
        direction: 'ASC'
    }, {
        property: 'denominacion',
        direction: 'ASC'
    }]
});