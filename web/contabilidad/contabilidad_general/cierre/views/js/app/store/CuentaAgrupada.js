Ext.define('Cierre.store.CuentaAgrupada', {
    extend: 'Ext.data.TreeStore',
    model: 'Cierre.model.CuentaAgrupada',
    folderSort: true,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'loadCuentasAgrupadas',
        actionMethods: {
            read: 'POST'
        }
    },
    root: {
        text: 'Cuentas',
        id: 'root',
        type: 'root',
        expanded: false
    },
    sorters: [{
        property: 'text',
        direction: 'ASC'
    }],
    listeners: {
        beforeload: function (store, operation) {
            operation.params.type = operation.node.raw.type;
        }
    }
});