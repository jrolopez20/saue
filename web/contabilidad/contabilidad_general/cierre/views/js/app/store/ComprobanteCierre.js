Ext.define('Cierre.store.ComprobanteCierre', {
    extend: 'Ext.data.Store',

    fields: ['id', 'generado', 'denominacion', 'numCmp'],
    proxy: {
        type: 'ajax',
        url: 'loadCfgComprobante',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});