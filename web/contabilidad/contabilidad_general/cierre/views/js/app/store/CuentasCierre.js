Ext.define('Cierre.store.CuentasCierre', {
    extend: 'Ext.data.Store',

    fields:['id', 'idconfcierrecontable', 'denominacion', 'tipo'],
    proxy: {
        type: 'ajax',
        url: 'loadCfgCierre',
        actionMethods: {
            read: 'POST'
        }
    }
});