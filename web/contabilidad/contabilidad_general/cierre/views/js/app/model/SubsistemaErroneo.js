Ext.define('Cierre.model.SubsistemaErroneo', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idsubsistema', type: 'int'},
        {name: 'denominacion', type: 'string'},
        {name: 'fecha', type: 'date'},
        {name: 'precedencia', type: 'int'}
    ]
});