Ext.define('Cierre.model.CuentaAgrupada', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'id'
        },
        {
            name: 'text', type: 'string'
        },
        {
            name: 'type'
        }
    ]
});