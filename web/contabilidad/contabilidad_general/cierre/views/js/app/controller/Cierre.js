Ext.define('Cierre.controller.Cierre', {
    extend: 'Ext.app.Controller',
    views: [
        'Cierre.view.MainPanel',
        'Cierre.view.ConfigCierre',
        'Cierre.view.SelectCuenta'
    ],

    stores: [
        'Cierre.store.CuentaAgrupada',
        'Cierre.store.ComprobanteErroneo',
        'Cierre.store.SubsistemaErroneo',
        'Cierre.store.CuentasCierre',
        'Cierre.store.ComprobanteCierre'
    ],

    models: [
        'Cierre.model.CuentaAgrupada',
        'Cierre.model.ComprobanteErroneo',
        'Cierre.model.SubsistemaErroneo'
    ],

    refs: [
        {ref: 'main_panel', selector: 'main_panel'},
        {ref: 'config_cierre', selector: 'config_cierre'},
        {ref: 'select_cuenta', selector: 'select_cuenta'},
        {ref: 'comprobante_preview', selector: 'comprobante_preview'}
    ],

    init: function () {
        var me = this;
        me.control({
            'main_panel': {
                afterrender: me.onRenderCierre
            },
            'main_panel button[action=cierreperiodo]': {
                click: me.cierrePeriodo
            },
            'main_panel button[action=cierreejercicio]': {
                click: me.cierreEjercicio
            },
            'main_panel button[action=configurarcierre]': {
                click: me.configurarCierre
            },
            'config_cierre button[action=config_cuenta_acreditar]': {
                toggle: me.onToggleConfigButton
            },
            'config_cierre button[action=config_cuenta_debitar]': {
                toggle: me.onToggleConfigButton
            },
            'config_cierre button[action=config_cuenta_cierre]': {
                toggle: me.onToggleConfigButton
            },
            'config_cierre button[action=config_cuenta_utilidades]': {
                toggle: me.onToggleConfigButton
            },
            'config_cierre button[action=config_cuenta_perdidas]': {
                toggle: me.onToggleConfigButton
            },
            'config_cierre button[action=load_comprobantes_cierre]': {
                toggle: me.showComprobantesCierre
            },
            'config_cierre button[action=add_cuenta_config]': {
                click: me.onClickAddCuentaConfig
            },
            'config_cierre button[action=del_cuenta_config]': {
                click: me.onClickDelCuentaConfig
            },
            'select_cuenta button[action=aceptar]': {
                click: me.saveConfigCierre
            },
            'select_cuenta button[action=aplicar]': {
                click: me.saveConfigCierre
            },
            'comprobante_preview button[action=aceptar]': {
                click: me.onAceptPreviewComprobante
            }
        });

        me.getCierreStoreCuentaAgrupadaStore().on(
            {
                beforeload: {fn: me.setearExtraParams, scope: this}
            }
        );
    },

    onRenderCierre: function (cmp) {
        this.loadFecha(cmp);
    },

    loadFecha: function(cmp) {
        var me = this,
            vp = cmp.up('viewport');

        var myMask = new Ext.LoadMask(cmp, {msg: 'Cargando datos de la fecha contable...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDataFecha',
            method: 'POST',
            callback: function (options, success, response) {
                var r = Ext.decode(response.responseText);
                myMask.hide();
                cmp.fechaContable = r.fecha.fecha;
                cmp.inicioPeriodo = r.periodo.inicio;
                cmp.finPeriodo = r.periodo.fin;
                cmp.idperiodo = r.periodo.idperiodo;

                var text = 'Período actual <b>' + r.periodo.nombre +
                    '</b>, desde <b>' + Ext.util.Format.date(r.periodo.inicio, 'd/m/Y') +
                    '</b> hasta <b>' + Ext.util.Format.date(r.periodo.fin, 'd/m/Y') +
                    '</b>.  Fecha contable<b> ' + Ext.util.Format.date(r.fecha.fecha, 'd/m/Y') + '</b>';
                cmp.tiFecha.setText(text);
            }
        });
    },

    setearExtraParams: function (store) {
        var extraParams = {
            action: this.configAction
        };
        store.getProxy().extraParams = extraParams;
    },

    cierrePeriodo: function (btn) {
        var me = this,
            vp = btn.up('viewport');

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea cerrar el período contable actual?', function (btnConfirm) {
            if (btnConfirm == 'yes') {
                var myMask = new Ext.LoadMask(vp, {msg: 'Cerrando período contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'cerrarPeriodo',
                    method: 'POST',
                    params: {
                        //idpases: Ext.encode(arrModelCuenta)
                    },
                    callback: function (options, success, response) {
                        var r = Ext.decode(response.responseText);
                        myMask.hide();
                        me._showErrorList(btn.up('panel'), r);
                    }
                });
            }
        });
    },

    cierreEjercicio: function (btn) {
        var me = this,
            vp = btn.up('viewport');
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea cerrar el ejercicio contable actual?', function (btnConfirm) {
            if (btnConfirm == 'yes') {
                var myMask = new Ext.LoadMask(vp, {msg: 'Cerrando período contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'cerrarEjercicio',
                    method: 'POST',
                    params: {
                        //idpases: Ext.encode(arrModelCuenta)
                    },
                    callback: function (options, success, response) {
                        var r = Ext.decode(response.responseText);
                        myMask.hide();
                        me._showErrorList(btn.up('panel'), r);
                    }
                });
            }
        });
    },

    configurarCierre: function (btn) {
        btn.up('viewport').down('config_cierre').toggleCollapse();
    },

    onToggleConfigButton: function (btn, pressed) {
        var me = this,
                btnAdd = btn.up('panel').down('button[action=add_cuenta_config]');
        btn.up('panel').getLayout().setActiveItem(0);
        
        if (pressed) {
            btn.up('panel').gpData.getStore().on('load', function (st, records) {
                if (btn.action == 'config_cuenta_cierre' || btn.action == 'config_cuenta_utilidades' ||
                    btn.action == 'config_cuenta_perdidas') {
                    if (records.length > 0) {
                        btnAdd.disable();
                    } else {
                        btnAdd.enable();
                    }
                } else {
                    btnAdd.enable();
                }
            });

            btn.up('panel').gpData.getStore().load({
                params: {
                    action: btn.action
                }
            });

            btnAdd.naturaleza = (btn.action == 'config_cuenta_acreditar') ? [8031] : 
                    (btn.action == 'config_cuenta_debitar') ? [8030] :
                    (btn.action == 'config_cuenta_cierre') ? [8032] : [8030,8031,8032];
                    
            //Setup the config option to load cuentas
            me.configAction = btn.action;
            me.configText = btn.getText();
        } else {
            btnAdd.disable();
            btn.up('panel').gpData.getStore().removeAll();
        }
    },

    onClickAddCuentaConfig: function (btn) {
        var win = Ext.widget('select_cuenta', {
            title: 'Seleccione cuenta(s) ' + this.configText.toLowerCase(),
            naturaleza : btn.naturaleza
        });
        //win.down('treepanel').getStore().load();
//        win.down('treepanel').idnaturaleza = [8031];
        win.show();
    },

    onClickDelCuentaConfig: function (btn) {
        var me = this,
            vp = btn.up('viewport'),
            grid = btn.up('panel').down('grid'),
            sm = grid.getSelectionModel();

        if (sm.hasSelection()) {
            var myMask = new Ext.LoadMask(vp, {msg: 'Eliminando elemento de configuración...'});
            myMask.show();
            Ext.Ajax.request({
                url: 'cerrarPeriodo',
                method: 'POST',
                params: {
                    id: sm.getLastSelected().get('')
                },
                callback: function (options, success, response) {
                    var rpta = Ext.decode(response.responseText);
                    myMask.hide();
                }
            });
        }
    },

    saveConfigCierre: function (btn) {
        var me = this,
            win = btn.up('window'),
            tree = win.down('treepanel'),
            sm = tree.getSelectionModel();

        if (sm.hasSelection()) {
            var node = sm.getLastSelected();
            //Verifica que no seleccione la raiz
            if (node.get('id') == 'root') {
                return false;
            }
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando configuración de cierre...'});
            myMask.show();
            Ext.Ajax.request({
                url: 'updateCfgCierre',
                method: 'POST',
                params: {
                    id: sm.getLastSelected().get('id'),
                    type: sm.getLastSelected().get('nodetype') ? '1' : '3',
//                    type: sm.getLastSelected().get('type'),
                    action: me.configAction
                },
                callback: function (options, success, response) {
                    var rpta = Ext.decode(response.responseText);
                    myMask.hide();
                    if (btn.action == 'aceptar') {
                        me.getConfig_cierre().down('grid').getStore().reload();
                        win.close();
                    }
                }
            });
        }
    },

    showComprobantesCierre: function (btn, pressed) {
        var me = this;
        if (pressed) {
            btn.up('panel').getLayout().setActiveItem(1);
            btn.up('panel').gpComprobante.getStore().load();
        } else {
            btn.up('panel').getLayout().setActiveItem(0);
        }
    },

    onAceptPreviewComprobante: function (btn) {
        var me = this,
            win = btn.up('window');
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea generar el comprobante?',
            function (btnConfirm) {
                if (btnConfirm == 'yes') {

                    var params = win.down('form').getForm().getValues();
                    var pases = new Array();
                    var recordsPases = win.down('grid').getStore().getRange();

                    var sumDebito = 0;
                    var sumCredito = 0;
                    Ext.each(recordsPases, function (pase) {
                        sumDebito += pase.get('debito');
                        sumCredito += pase.get('credito');

                        pases.push({
                            codigodocumento: pase.get('codigodocumento'),
                            concatcta: pase.get('concatcta'),
                            credito: pase.get('credito'),
                            debito: pase.get('debito'),
                            denominacion: pase.get('denominacion'),
                            idcuenta: pase.get('idcuenta'),
                            idpase: pase.get('idpase')
                        });
                    });
                    params.pases = Ext.encode(pases)

                    if ((sumDebito - sumCredito) != 0) {
                        Ext.Msg.show({
                            title: perfil.etiquetas.lnInfo,
                            msg: 'No se puede generar un comprobante con errores.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                        return;
                    }

                    var myMask = new Ext.LoadMask(win, {msg: 'Generando...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'saveComprobante',
                        method: 'POST',
                        params: params,
                        callback: function (options, success, response) {
                            responseData = Ext.decode(response.responseText);
                            myMask.hide();
                            me.getConfig_cierre().gpComprobante.getStore().reload();
                        }
                    });
                }
            }
        );
    },

    _showErrorList: function (mainPanel, rpse) {
        var me = this;
        var layout = mainPanel.getLayout();
        if (rpse.success == false) {
            if (rpse.status == 'subsistemas') {
                layout.setActiveItem(1);
                mainPanel.down('grid[name=subsistema]').getStore().loadData(rpse.datos);
            } else if (rpse.status == 'comprobante') {
                layout.setActiveItem(2);
                mainPanel.down('grid[name=comprobante]').getStore().loadData(rpse.datos);
            } else {
                Ext.Msg.show({
                    title: perfil.etiquetas.lnInfo,
                    msg: rpse.msg,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        } else {
            layout.setActiveItem(0);
            me.loadFecha(mainPanel);
        }
    }
});
