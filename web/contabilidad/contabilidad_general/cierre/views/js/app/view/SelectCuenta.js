Ext.define('Cierre.view.SelectCuenta', {
    extend: 'Ext.window.Window',
    alias: 'widget.select_cuenta',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: false,
    width: 580,
    height: 550,
    title: 'Seleccione',
    initComponent: function () {
        this.items = [{
            xtype: 'cuenta_tree',
            checked: false,
            idnaturaleza: this.naturaleza,
            anySelection: true,
            autoLoad: false,
            region: 'center',
            margins: '5 5 5 5'
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});