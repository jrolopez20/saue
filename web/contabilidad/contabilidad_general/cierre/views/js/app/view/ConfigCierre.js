Ext.define('Cierre.view.ConfigCierre', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.config_cierre',
    split: true,
    collapsible: true,
    //collapsed: true,
    width: 480,
    minWidth: 410,
    title: 'Configurar cierre de ejercicio contable',
    layout: 'card',
    activeItem: 0,
    initComponent: function () {
        var me = this;

        this.tbar = [
            {
                text: perfil.etiquetas.lbBtnAcreditar,
                iconCls: 'acreditar32x32',
                scale: 'large',
                iconAlign: 'top',
                enableToggle: true,
                toggleGroup: 'config',
                action: 'config_cuenta_acreditar'
            },
            {
                text: perfil.etiquetas.lbBtnDebitar,
                iconCls: 'debitar32x32',
                scale: 'large',
                iconAlign: 'top',
                enableToggle: true,
                toggleGroup: 'config',
                action: 'config_cuenta_debitar'
            },
            {
                text: perfil.etiquetas.lbBtnDeCierre,
                iconCls: 'cierre32x32',
                scale: 'large',
                iconAlign: 'top',
                enableToggle: true,
                toggleGroup: 'config',
                action: 'config_cuenta_cierre'
            },
            {
                text: perfil.etiquetas.lbBtnUtilides,
                iconCls: 'utilidades32x32',
                scale: 'large',
                iconAlign: 'top',
                enableToggle: true,
                toggleGroup: 'config',
                action: 'config_cuenta_utilidades'
            },
            {
                text: perfil.etiquetas.lbBtnPerdidas,
                iconCls: 'perdidas32x32',
                scale: 'large',
                iconAlign: 'top',
                enableToggle: true,
                toggleGroup: 'config',
                action: 'config_cuenta_perdidas'
            },
            {
                text: perfil.etiquetas.lbBtnComprobante,
                tooltip: 'Generar comprobantes',
                iconCls: 'comprobante32x32',
                scale: 'large',
                iconAlign: 'top',
                enableToggle: true,
                toggleGroup: 'config',
                action: 'load_comprobantes_cierre'
            }
        ];

        this.gpData = Ext.create('Ext.grid.Panel', {
            tbar: [{
                text: 'Adicionar',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'add_cuenta_config'
            }],
            store: 'Cierre.store.CuentasCierre',
            columns: [
                {text: 'Denominacion', dataIndex: 'denominacion', flex: 1},
                {text: 'Tipo', dataIndex: 'tipo'},
                {
                    xtype: 'actioncolumn',
                    width: 30,
                    sortable: false,
                    draggable: false,
                    hideable: false,
                    menuDisabled: true,
                    items: [{
                        icon: perfil.dirImg + 'eliminar.png',
                        tooltip: 'Eliminar cuenta',
                        handler: function (grid, rowIndex, colIndex, item, e, record) {
                            Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar la cuenta seleccionada?',
                                function (btn) {
                                    if (btn == 'yes') {
                                        var myMask = new Ext.LoadMask(grid, {msg: 'Eliminando...'});
                                        myMask.show();
                                        Ext.Ajax.request({
                                            url: 'deleteCfgCierre',
                                            method: 'POST',
                                            params: {
                                                idconfcierrecontable: record.get('idconfcierrecontable')
                                            },
                                            callback: function (options, success, response) {
                                                responseData = Ext.decode(response.responseText);
                                                myMask.hide();
                                                grid.getStore().removeAt(rowIndex);
                                                if (grid.getStore().getCount() == 0) {
                                                    me.gpData.down('button[action=add_cuenta_config]').enable();
                                                }
                                            }
                                        });
                                    }
                                }
                            );
                        }
                    }
                    ]
                }
            ]
        });

        this.gpComprobante = Ext.create('Ext.grid.Panel', {
            title: 'Comprobantes a generar',
            store: 'Cierre.store.ComprobanteCierre',
            columns: [
                {text: 'Denominacion', dataIndex: 'denominacion', flex: 1},
                {
                    text: 'Estado', dataIndex: 'generado', renderer: function (val) {
                    if (val) {
                        return '<span style="color: green;">Generado</span>';
                    }
                    return '<span style="color: brown;">Por generar</span>';
                }
                },
                {text: 'No', dataIndex: 'numCmp', width: 40},
                {
                    xtype: 'actioncolumn',
                    width: 30,
                    sortable: false,
                    draggable: false,
                    hideable: false,
                    menuDisabled: true,
                    items: [{
                        icon: perfil.dirImg + 'generardocumentos.png',
                        tooltip: 'Generar comprobante',
                        handler: function (grid, rowIndex, colIndex, item, e, record) {
                            if (record.get('generado')) {
                                Ext.Msg.show({
                                    title: perfil.etiquetas.lnInfo,
                                    msg: 'El comprobante ya fue generado.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.Msg.INFO
                                });
                            } else {
                                var myMask = new Ext.LoadMask(grid, {msg: 'Obteniendo datos del comprobante...'});
                                myMask.show();
                                Ext.Ajax.request({
                                    url: 'getComprobanteCierre',
                                    method: 'POST',
                                    params: {
                                        id: record.get('id')
                                    },
                                    callback: function (options, success, response) {
                                        var rpse = Ext.decode(response.responseText);
                                        myMask.hide();
                                        var mainPanel = Ext.ComponentQuery.query('main_panel')[0];
                                        var layout = mainPanel.getLayout();
                                        if (rpse.success == false) {
                                            if (rpse.status == 'subsistemas') {
                                                layout.setActiveItem(1);
                                                mainPanel.down('grid[name=subsistema]').getStore().loadData(rpse.datos);
                                            } else if (rpse.status == 'comprobante') {
                                                layout.setActiveItem(2);
                                                mainPanel.down('grid[name=comprobante]').getStore().loadData(rpse.datos);
                                            } else {
                                                Ext.Msg.show({
                                                    title: perfil.etiquetas.lnInfo,
                                                    msg: rpse.msg,
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.Msg.ERROR
                                                });
                                            }
                                        } else {
                                            layout.setActiveItem(0);
                                            var win = Ext.widget('comprobante_preview', {
                                                title: record.get('denominacion')
                                            });
                                            win.down('form').getForm().setValues(rpse);
                                            win.down('grid').getStore().loadData(rpse.pases);
                                        }
                                    }
                                });
                            }
                        }
                    }]
                }
            ]
        });

        this.items = [this.gpData, this.gpComprobante];

        this.callParent(arguments);
    }
})
;