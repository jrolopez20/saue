Ext.define('Cierre.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cierre_grid',
    store: 'Cierre.store.Cierre',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.lbBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbCodInicio,
                dataIndex: 'codigoinicio',
                width: 100
            },
            {
                header: perfil.etiquetas.lbCodFin,
                dataIndex: 'codigofin',
                width: 100
            },
            {
                header: perfil.etiquetas.lbDenominacion,
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        this.callParent(arguments);
    }
});