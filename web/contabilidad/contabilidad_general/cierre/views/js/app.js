var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Cierre',

    enableQuickTips: true,

    paths: {
        'Cierre': '../../views/js/app'
    },

    controllers: ['Cierre'],

    launch: function () {
        UCID.portal.cargarEtiquetas('cierrecontable', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'main_panel',
                        region:'center',
                        margin: '5 0 5 5'
                    },
                    {
                        xtype: 'config_cierre',
                        region:'east',
                        margin: '5 5 5 0'
                    }
                ]
            });
        });
    }
});