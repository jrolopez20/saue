Ext.define('Formato.model.Formato', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idformato', type: 'int'
        },
        {
            name: 'nombre', type: 'string'
        },
        {
            name: 'descripcion', type: 'string'
        },
        {
            name: 'longitud', type: 'int'
        },
        {
            name: 'separador', type: 'string'
        },
        {
            name: 'estructura', type: 'string'
        },
        {
            name: 'vistap', type: 'string'
        },
        {
            name: 'estandar', type: 'int'
        },
        {
            name: 'idsubsistema', type: 'int'
        },
        {
            name: 'subsistema', type: 'string'
        },
        {
            name: 'propietario', type: 'int'
        }
    ]
});