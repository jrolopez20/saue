Ext.define('Formato.model.NivelFormato', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idparteformato'
        },
        {
            name: 'nombre'
        },
        {
            name: 'abreviatura'
        },
        {
            name: 'nivel'
        },
        {
            name: 'longitud'
        },
        {
            name: 'idformato'
        }
    ]
});