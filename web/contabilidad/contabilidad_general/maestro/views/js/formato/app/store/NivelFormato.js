Ext.define('Formato.store.NivelFormato', {
    extend: 'Ext.data.Store',
    model: 'Formato.model.NivelFormato',

    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'listDataParteFormato',
            //create: 'insertarCursos',
            //update: 'modificarCursos',
            //destroy: 'eliminarCursos'
        },
        actionMethods: {
            read: 'POST',
            //create: 'POST',
            //update: 'POST',
            //destroy: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            idProperty: 'idparteformato',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});