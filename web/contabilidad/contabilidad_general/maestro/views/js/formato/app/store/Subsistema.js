Ext.define('Formato.store.Subsistema', {
    extend: 'Ext.data.Store',
    model: 'Formato.model.Subsistema',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadSubsistema',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});