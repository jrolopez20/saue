Ext.define('Formato.view.FormatoGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.formato_grid',

    title: 'Listado  de formatos',
    singleSelect: this.singleSelect || true,
    heredar: this.heredar || false,

    initComponent: function () {
        var me = this;

        me.store = Ext.create('Formato.store.Formato',{
            proxy: {
                type: 'ajax',
                url: me.heredar ? 'loadDataHeredar' : 'listDataFormato',
                actionMethods: {
                    read: 'POST'
                },
                reader: {
                    root: 'datos',
                    totalProperty: 'cantidad',
                    idProperty: 'idformato',
                    successProperty: 'success',
                    messageProperty: 'mensaje'
                }
            }
        });

        if(me.singleSelect){
            me.selModel = Ext.create('Ext.selection.RowModel');
        }else{
            me.selModel = Ext.create('Ext.selection.CheckboxModel');
        }

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar formato',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'addFormato'
            },
            {
                text: 'Modificar',
                tooltip: 'Modificar formato',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled:true,
                action: 'modFormato'
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar formato',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled:true,
                action: 'deleteFormato'
            },
            {
                text: 'Heredar',
                tooltip: 'Heredar de otro formato',
                icon: perfil.dirImg + 'heredar.png',
                iconCls: 'btn',
                action: 'heredarFormato'
            }
        ];



        me.columns = [
            Ext.create('Ext.grid.RowNumberer'),
            {
                header: 'Nombre',
                dataIndex: 'nombre',
                width: 200
            },
            {
                header: 'Descripción',
                dataIndex: 'descripcion',
                flex: 1
            },
            {
                header: 'Longitud',
                dataIndex: 'longitud',
                width: 70
            },
            {
                header: 'Separador',
                dataIndex: 'separador',
                width: 75
            },
            {
                header: 'Estructura',
                dataIndex: 'estructura',
                width: 150,
                sortable: true
            },
            {
                header: 'Vista previa',
                dataIndex: 'vistap',
                width: 130
            },
            {
                header: 'Subsistema',
                dataIndex: 'subsistema',
                width: 140,
                sortable: true
            },
            {
                header: 'Estándar',
                dataIndex: 'estandar',
                width: 100,
				renderer: function(value, metadata, record, rowIdex, colIndex, store, view){
					return (record.data.propietario) ? 'Si' : 'No';
				}
            }
        ];

        if(me.heredar == false){
            me.columns.push({
                header: 'Propietario',
                dataIndex: 'propietario',
                width: 100,
                renderer: function(value, metadata, record){
                    return (record.data.propietario) ? 'Si' : 'No';
                }
            });
        }

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});