Ext.define('Formato.controller.Formato', {
    extend: 'Ext.app.Controller',
    views: [
        'Formato.view.FormatoGrid',
        'Formato.view.FormatoEdit',
        'Formato.view.FormatoNivelEdit',
        'Formato.view.NivelFormatoGrid',
        'Formato.view.HeredarFormatoWin'
    ],
    stores: [
        'Formato.store.Formato',
        'Formato.store.Subsistema',
        'Formato.store.NivelFormato'
    ],
    models: [
        'Formato.model.Formato',
        'Formato.model.Subsistema',
        'Formato.model.NivelFormato'
    ],
    refs: [
        {ref: 'formato_grid', selector: 'formato_grid'},
        {ref: 'formato_edit', selector: 'formato_edit'},
        {ref: 'formatonivel_edit', selector: 'formatonivel_edit'},
        {ref: 'nivelformato_grid', selector: 'nivelformato_grid'},
        {ref: 'controller', selector: 'controller'}
    ],
    init: function() {
        var me = this;

        me.control({
            'formato_grid': {
                selectionchange: me.onFormatoSelectionChange
            },
            'formato_grid button[action=addFormato]': {
                click: me.addFormato
            },
            'formato_grid button[action=modFormato]': {
                click: me.modFormato
            },
            'formato_grid button[action=deleteFormato]': {
                click: me.deleteFormato
            },
            'formato_grid button[action=heredarFormato]': {
                click: me.heredarFormato
            },
            'formato_edit button[action=aplicar]': {
                click: me.saveFormato
            },
            'formato_edit button[action=aceptar]': {
                click: me.saveFormato
            },
            'nivelformato_grid': {
                selectionchange: me.onNivelFormatoSelectionChange
            },
            'nivelformato_grid button[action=addNivelFormato]': {
                click: me.addNivelFormato
            },
            'nivelformato_grid button[action=modNivelFormato]': {
                click: me.modNivelFormato
            },
            'nivelformato_grid button[action=deleteNivelFormato]': {
                click: me.deleteNivelFormato
            },
            'formatonivel_edit button[action=aplicar]': {
                click: me.saveNivelFormato
            },
            'formatonivel_edit button[action=aceptar]': {
                click: me.saveNivelFormato
            },
            'heredarformato_win button[action=aceptar]': {
                click: me.addFormatoHeredado
            }
        });

        me.getFormatoStoreNivelFormatoStore().on(
                {
                    beforeload: {fn: me.setearExtraParams, scope: this}
                }
        );
    },
    setearExtraParams: function(store) {
        var extraParams = {idformato: this.getFormato_grid().getSelectionModel().getLastSelected().get('idformato')};
        store.getProxy().extraParams = extraParams;
    },
    /**
     * Muestra la ventana de adicionar formato
     */
    addFormato: function() {
        var win = Ext.widget('formato_edit');
        win.down('button[action=aplicar]').show();
        win.setTitle('Adicionar formato');
    },
    /**
     * Muestra la ventana de modificar formato
     */
    modFormato: function() {
        var win = Ext.widget('formato_edit');
        win.down('button[action=aplicar]').hide();
        win.setTitle('Modificar formato');
        var form = win.down('form');
        var record = this.getFormato_grid().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },
    /**
     * Elimina un formato
     */
    deleteFormato: function() {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirmar, perfil.etiquetas.lbMsgConfirmar, function(btn) {
            if (btn == 'yes') {
                var record = me.getFormato_grid().getSelectionModel().getLastSelected();
                var myMask = new Ext.LoadMask(me.getFormato_grid(), {msg: perfil.etiquetas.lbEliminandoFormato});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteFormato',
                    method: 'POST',
                    params: {
                        idformato: record.get('idformato'),
                        propietario: record.get('propietario')
                    },
                    success: function(response) {
                        myMask.hide();
                        me.getFormato_grid().getStore().reload();
                    },
                    failure: function(response, opts) {
                        myMask.hide();
                    }
                });
            }
        });
    },
    heredarFormato: function(btn) {
        var win = Ext.widget('heredarformato_win');
    },
    /**
     * Muestra ventana para adicionar un nuevo nivel de formato
     */
    addNivelFormato: function() {
        var win = Ext.widget('formatonivel_edit');
        win.down('button[action=aplicar]').show();
        win.setTitle('Adicionar nivel de formato');
    },
    /**
     * Muestra ventana para modificar un nuevo nivel de formato
     */
    modNivelFormato: function() {
        var win = Ext.widget('formatonivel_edit');
        win.down('button[action=aplicar]').hide();
        win.setTitle('Modificar nivel de formato');
        var form = win.down('form');
        var record = this.getNivelformato_grid().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },
    deleteNivelFormato: function() {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirmar, perfil.etiquetas.lbMsgConfirmarParte, function(btn) {
            if (btn == 'yes') {
                var record = me.getNivelformato_grid().getSelectionModel().getLastSelected();
                var myMask = new Ext.LoadMask(me.getNivelformato_grid(), {msg: perfil.etiquetas.lbEliminandoNivel});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteParteFormato',
                    method: 'POST',
                    params: {
                        idformato: record.get('idformato'),
                        idparteformato: record.get('idparteformato')
                    },
                    success: function(response) {
                        myMask.hide();
                        me.getFormato_grid().getStore().reload();
                        me.getNivelformato_grid().getStore().load({
                            params: {
                                idformato: record.get('idformato')
                            }
                        });
                    },
                    failure: function(response, opts) {
                        myMask.hide();
                    }
                });
            }
        });
    },
    addFormatoHeredado: function(btn) {
        var me = this,
                win = btn.up('window'),
                grid = win.down('grid');

        if (grid.getSelectionModel().hasSelection()) {
            var records = grid.getSelectionModel().getSelection();
            var arrayIds = new Array();
            Ext.each(records, function(record) {
                arrayIds.push(record.get('idformato'));
            });

            var myMask = new Ext.LoadMask(grid, {msg: perfil.etiquetas.lbHeredandoFormato});
            myMask.show();
            Ext.Ajax.request({
                url: 'heredarFormato',
                params: {
                    idformato: Ext.encode(arrayIds)
                },
                success: function(response) {
                    myMask.hide();
                    var text = response.responseText;
                    me.getFormato_grid().getStore().reload();
                    win.close();
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        } else {
            Ext.MessageBox.show({
                title: perfil.etiquetas.lbAlerta,
                msg: perfil.etiquetas.lbSeleccionarFormato,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }
    },
    onFormatoSelectionChange: function(sm, arrSelectedRecord) {
        var me = this;
        var gridFormato = this.getFormato_grid();

        gridFormato.down('button[action=modFormato]').disable();
        gridFormato.down('button[action=deleteFormato]').disable();

        if (sm.hasSelection()) {
            var record = gridFormato.getSelectionModel().getLastSelected();
            if (record.get('propietario')) {
                gridFormato.down('button[action=modFormato]').enable();
            }

            gridFormato.down('button[action=deleteFormato]').enable();
            this.getNivelformato_grid().enable();
            this.getNivelformato_grid().getStore().load({
                params: {
                    idformato: arrSelectedRecord[0].get('idformato')
                }
            });
        } else {
            this.getNivelformato_grid().getStore().removeAll();
            this.getNivelformato_grid().disable();
        }
    },
    onNivelFormatoSelectionChange: function(sm, arrSelectedRecord) {
        var gridNivelFormato = this.getNivelformato_grid();
        if (sm.hasSelection()) {
            gridNivelFormato.down('button[action=modNivelFormato]').enable();
            gridNivelFormato.down('button[action=deleteNivelFormato]').enable();

        } else {
            gridNivelFormato.down('button[action=modNivelFormato]').disable();
            gridNivelFormato.down('button[action=deleteNivelFormato]').disable();
        }
    },
    saveFormato: function(btn) {
        var me = this,
                win = btn.up('window'),
                form = win.down('form'),
                gridFormato = me.getFormato_grid();

        if (form.getForm().isValid()) {
            var values = form.getForm().getValues();
            var url = values.idformato ? 'updateFormato' : 'addFormato';

            var myMask = new Ext.LoadMask(this.getFormato_grid(), {msg: perfil.etiquetas.lbGuardandoFormato});
            myMask.show();
            Ext.Ajax.request({
                url: url,
                params: values,
                success: function(response) {
                    myMask.hide();
                    var text = response.responseText;
                    gridFormato.getStore().reload();
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });

            if (btn.action === 'aceptar') {
                win.close();
            } else {
                form.reset();
            }
        }
    },
    saveNivelFormato: function(btn) {
        var me = this,
                win = btn.up('window'),
                form = win.down('form'),
                gridFormato = me.getFormato_grid(),
                gridNivelFormato = me.getNivelformato_grid();

        if (form.getForm().isValid()) {
            var values = form.getForm().getValues();
            values.idformato = gridFormato.getSelectionModel().getLastSelected().get('idformato');
            var url = values.idparteformato ? 'updateParteFormato' : 'addParteFormato';

            var myMask = new Ext.LoadMask(this.getNivelformato_grid(), {msg: perfil.etiquetas.lbGuardandoNivel});
            myMask.show();
            Ext.Ajax.request({
                url: url,
                params: values,
                success: function(response) {
                    myMask.hide();
                    var r = Ext.decode(response.responseText);
                    if (r.success) {
                        gridFormato.getStore().reload();
                        gridNivelFormato.getStore().load({
                            params: {
                                idformato: values.idformato
                            }
                        });
                    } 
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });

            if (btn.action === 'aceptar') {
                win.close();
            }
        }
    }
});