Ext.define('Formato.view.FormatoEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.formato_edit',

    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 500,

    initComponent: function () {
        this.items = [
            Ext.create('Ext.form.Panel',{
                xtype: 'form',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 60
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',

                items: [
                    {
                        xtype: 'hidden',
                        name: 'idformato'
                    },
                    {
                        xtype: 'container',
                        anchor: '100%',
                        layout: 'hbox',
                        items: [{
                            xtype: 'container',
                            flex: 2,
                            layout: 'anchor',
                            items: [{
                                xtype: 'textfield',
                                fieldLabel: 'Nombre',
                                allowBlank: false,
                                name: 'nombre',
                                anchor: '95%',
                                regex: /^([a-zA-Z\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+]+ ?[a-zA-Z\s]*)+$/,
                                maskRe: /^([a-zA-Z\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+]+ ?[a-zA-Z\s]*)+$/
                            }, {
                                xtype: 'combobox',
                                fieldLabel: 'Subsistema',
                                name: 'idsubsistema',
                                anchor: '95%',
                                store: 'Formato.store.Subsistema',
                                editable: false,
                                emptyText: 'Seleccione...',
                                triggerAction: 'all',
                                forceSelection: true,
                                allowBlank: false,
                                valueField: 'idsubsistema',
                                displayField: 'denominacion',
                                mode: 'local'
                            }]
                        }, {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            items: [{
                                xtype: 'combobox',
                                fieldLabel: 'Seperador',
                                name: 'separador',
                                anchor: '95%',
                                store: Ext.create('Ext.data.Store', {
                                    fields: ['separador'],
                                    data: [
                                        {separador:'*'},
                                        {separador:'/'},
                                        {separador:'-'},
                                        {separador:':'},
                                        {separador:'|'},
                                        {separador:'.'},
                                        {separador:';'},
                                        {separador:','}
                                    ]
                                }),
                                editable: false,
                                maxLength: 1,
                                emptyText: 'Seleccione...',
                                triggerAction: 'all',
                                forceSelection: true,
                                allowBlank: false,
                                valueField: 'separador',
                                displayField: 'separador',
                                mode: 'local'
                            }, {
                                xtype: 'checkbox',
                                fieldLabel: 'Est&aacute;ndar',
                                name: 'nivelpais',
                                anchor: '95%'
                            }]
                        }]
                    }, {
                        xtype: 'textarea',
                        fieldLabel: 'Descripción',
                        name: 'descripcion',
                        allowBlank: false,
                        maxLength: 255,
                        regex: /^([a-zA-Z0-9\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+]+ ?[a-zA-Z\s]*)+$/,
                        maskRe: /^([a-zA-Z0-9\xf3\xf1\xe1\xe9\xed\xfa\xd1\xc1\xc9\xcd\xd3\xda\xfc\s \"\-\_\/\(\)\.\#\+]+ ?[a-zA-Z\s]*)+$/
                    }
                ]
            })
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});