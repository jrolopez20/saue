Ext.define('Formato.view.NivelFormatoGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.nivelformato_grid',

    store: 'Formato.store.NivelFormato',

    title: 'Listado de niveles de formato',
    disabled:true,
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar nivel de formato',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'addNivelFormato'
            },
            {
                text: 'Modificar',
                tooltip: 'Modificar nivel de formato',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled:true,
                action: 'modNivelFormato'
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar nivel de formato',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled:true,
                action: 'deleteNivelFormato'
            }
        ];

        me.columns = [
            {
                header: 'Nombre',
                dataIndex: 'nombre',
                flex: 1
            },
            {
                header: 'Abreviatura',
                dataIndex: 'abreviatura',
                width: 120
            },
            {
                header: 'Nivel',
                dataIndex: 'nivel',
                width: 80
            },
            {
                header: 'Longitud',
                dataIndex: 'longitud',
                width: 80
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});