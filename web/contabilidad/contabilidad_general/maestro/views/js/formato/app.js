var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Formato',

    enableQuickTips: true,

    paths: {
        'Formato': '../../views/js/formato/app'
    },

    controllers: ['Formato'],

    launch: function () {
        UCID.portal.cargarEtiquetas('formato', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'formato_grid',
                        region: 'center',
                        margin: '5 5 0 5'
                    },
                    {
                        xtype: 'nivelformato_grid',
                        height: 300,
                        split: true,
                        margin: '0 5 5 5',
                        region: 'south'
                    }
                ]
            });
        });
    }
});