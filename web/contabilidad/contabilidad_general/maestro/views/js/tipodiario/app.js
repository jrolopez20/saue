var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'TipoDiario',

    enableQuickTips: true,

    paths: {
        'TipoDiario': '../../views/js/tipodiario/app'
    },

    controllers: ['TipoDiario'],

    launch: function () {
        UCID.portal.cargarEtiquetas('tipodiario', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'tipodiario_grid',
                        region: 'center',
                        margin: '5 0 5 5',
                        width: '65%'
                    },
                    {
                        xtype: 'modelo_grid',
                        region: 'east',
                        margin: '5 5 5 0',
                        collapsible:true,
                        split: true,
                        disabled: true,
                        width: '35%'
                    }
                ]
            });
        });
    }
});