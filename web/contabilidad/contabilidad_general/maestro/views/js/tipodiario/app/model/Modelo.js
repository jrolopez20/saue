Ext.define('TipoDiario.model.Modelo', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idmodelocontable', type: 'int'},
        {name: 'denominacion', type: 'string'}
    ]
});