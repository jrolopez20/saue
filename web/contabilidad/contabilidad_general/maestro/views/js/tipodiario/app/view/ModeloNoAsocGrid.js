Ext.define('TipoDiario.view.ModeloNoAsocGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.modelonoasoc_grid',

    title: 'Modelos no asociados',
    store: 'TipoDiario.store.ModeloNoAsoc',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
            mode: 'MULTI'
        });

        me.columns = [
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});