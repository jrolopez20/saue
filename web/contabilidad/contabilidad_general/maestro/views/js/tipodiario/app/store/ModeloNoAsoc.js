Ext.define('TipoDiario.store.ModeloNoAsoc', {
    extend: 'Ext.data.Store',
    model: 'TipoDiario.model.Modelo',

    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'loadModeloNoAsociado'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'menssssaje'
        }
    }
});