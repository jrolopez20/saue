Ext.define('TipoDiario.store.TipoDiario', {
    extend: 'Ext.data.Store',
    model: 'TipoDiario.model.TipoDiario',

    autoLoad: true,
    pageSize: 25,
    groupField: 'subsistema',
    sorters: ['subsistema', 'denominacion'],
    proxy: {
        type: 'rest',
        api: {
            read: 'listDataTipoDiario'
        },
        actionMethods: {
            read: 'POST',
            update: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});