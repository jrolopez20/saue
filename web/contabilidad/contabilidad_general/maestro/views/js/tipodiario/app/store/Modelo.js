Ext.define('TipoDiario.store.Modelo', {
    extend: 'Ext.data.Store',
    model: 'TipoDiario.model.Modelo',

    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'loadModeloAsociado'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'menssssaje'
        }
    }
});