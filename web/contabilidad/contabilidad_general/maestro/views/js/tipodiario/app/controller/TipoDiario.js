Ext.define('TipoDiario.controller.TipoDiario', {
    extend: 'Ext.app.Controller',

    views: [
        'TipoDiario.view.Grid',
        'TipoDiario.view.ModeloGrid',
        'TipoDiario.view.ModeloNoAsocGrid',
        'TipoDiario.view.TipoDiarioEdit',
        'TipoDiario.view.AsociarModeloWin'
    ],
    stores: [
        'TipoDiario.store.TipoDiario',
        'TipoDiario.store.Modelo',
        'TipoDiario.store.ModeloNoAsoc',
        'TipoDiario.store.Subsistema'
    ],

    models: [
        'TipoDiario.model.TipoDiario',
        'TipoDiario.model.Modelo',
        'TipoDiario.model.Subsistema'
    ],

    refs: [
        {ref: 'tipodiario_grid', selector: 'tipodiario_grid'},
        {ref: 'tipodiario_edit', selector: 'tipodiario_edit'},
        {ref: 'modelo_grid', selector: 'modelo_grid'},
        {ref: 'modelonoasoc_grid', selector: 'modelonoasoc_grid'},
        {ref: 'asociarmodelo_win', selector: 'asociarmodelo_win'}
    ],

    init: function () {
        var me = this;

        me.control({
            'tipodiario_grid': {
                selectionchange: me.onTipoDiarioSelectionChange
            },
            'modelo_grid': {
                selectionchange: me.onModeloSelectionChange
            },
            'tipodiario_grid button[action=adicionar]': {
                click: me.addTipoDiario
            },
            'tipodiario_grid button[action=modificar]': {
                click: me.modTipoDiario
            },
            'tipodiario_grid button[action=eliminar]': {
                click: me.delTipoDiario
            },
            'tipodiario_edit button[action=aceptar]': {
                click: me.saveTipoDiario
            },
            'tipodiario_edit button[action=aplicar]': {
                click: me.saveTipoDiario
            },
            'modelo_grid button[action=adicionar]': {
                click: me.showWinAsociarModelo
            },
            'modelo_grid button[action=eliminar]': {
                click: me.desasociarModelo
            },
            'asociarmodelo_win button[action=aceptar]': {
                click: me.asociarModelo
            },
            'asociarmodelo_win button[action=aplicar]': {
                click: me.asociarModelo
            }
        });

        me.getTipoDiarioStoreModeloStore().on(
            {
                beforeload: {fn: me.setearExtraParams, scope: this}
            }
        );
        me.getTipoDiarioStoreModeloNoAsocStore().on(
            {
                beforeload: {fn: me.setearExtraParams, scope: this}
            }
        );
    },

    setearExtraParams: function (store) {
        var extraParams = {idtipodiario: this.getTipodiario_grid().getSelectionModel().getLastSelected().get('idtipodiario')};
        store.getProxy().extraParams = extraParams;
    },

    onTipoDiarioSelectionChange: function (sm) {
        var me = this;

        if (sm.hasSelection()) {
            me.getTipodiario_grid().down('button[action=modificar]').enable();
            me.getTipodiario_grid().down('button[action=eliminar]').enable();
            me.getModelo_grid().enable();
            me.getModelo_grid().getStore().load({
                params: {
                    idtipodiario: sm.getLastSelected().get('idtipodiario'),
                    limit: 25,
                    start: 0
                }
            });

        } else {
            me.getTipodiario_grid().down('button[action=modificar]').disable();
            me.getTipodiario_grid().down('button[action=eliminar]').disable();

            me.getModelo_grid().disable();
            me.getModelo_grid().getStore().removeAll();
        }
    },

    onModeloSelectionChange: function (sm) {
        var me = this;

        if (sm.hasSelection()) {
            me.getModelo_grid().down('button[action=eliminar]').enable();
        } else {
            me.getModelo_grid().down('button[action=eliminar]').disable();
        }
    },

    addTipoDiario: function (btn) {
        var win = Ext.widget('tipodiario_edit');
        win.setTitle('Adicionar tipo de asiento');
        win.down('button[action=aplicar]').show();
    },

    modTipoDiario: function (btn) {
        var win = Ext.widget('tipodiario_edit');
        win.setTitle('Modificar tipo de asiento');
        win.down('button[action=aplicar]').hide();
        var form = win.down('form');
        var record = this.getTipodiario_grid().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },

    delTipoDiario: function (btn) {
        var me = this;
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el tipo de asiento seleccionado?', function (btn) {
            if (btn == 'yes') {
                var record = me.getTipodiario_grid().getSelectionModel().getLastSelected();

                var myMask = new Ext.LoadMask(me.getTipodiario_grid(), {msg: 'Eliminando tipo de asiento...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteTipoDiario',
                    method: 'POST',
                    params: {
                        idtipodiario: record.get('idtipodiario')
                    },
                    callback: function (options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getTipodiario_grid().getStore().reload();
                    }
                });
            }
        });
    },

    saveTipoDiario: function (btn) {
        var me = this;
        var form = me.getTipodiario_edit().down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getTipodiario_edit(), {msg: 'Guardando tipo de diario...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                url: 'updateTipoDiario',
                success: function (response) {
                    myMask.hide();

                    me.getTipodiario_grid().getStore().load();

                    if (btn.action === 'aceptar') {
                        me.getTipodiario_edit().close();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },

    showWinAsociarModelo: function (btn) {
        var me = this;
        if (me.getTipodiario_grid().getSelectionModel().hasSelection()) {
            var win = Ext.widget('asociarmodelo_win', {
                gestionar: false
            });
            var record = me.getTipodiario_grid().getSelectionModel().getLastSelected();
            win.down('grid').getStore().load({
                params: {
                    idtipodiario: record.get('idtipodiario'),
                    limit: 25,
                    start: 0
                }
            });
        }
    },

    asociarModelo: function (btn) {
        var me = this,
            grid = this.getAsociarmodelo_win().down('grid');

        if (grid.getSelectionModel().hasSelection()) {
            var myMask = new Ext.LoadMask(me.getAsociarmodelo_win(), {msg: 'Asociando modelos...'});
            myMask.show();
            var idtipodiario = me.getTipodiario_grid().getSelectionModel().getLastSelected().get('idtipodiario');
            var arrIdModelo = new Array();
            var records = grid.getSelectionModel().getSelection();
            Ext.each(records, function (record) {
                arrIdModelo.push(record.get('idmodelocontable'));
            });

            Ext.Ajax.request({
                url: 'asociarModelo',
                method: 'POST',
                params: {
                    idtipodiario: idtipodiario,
                    arridmodelo: Ext.encode(arrIdModelo)
                },
                callback: function (options, success, response) {
                    responseData = Ext.decode(response.responseText);
                    myMask.hide();

                    if (btn.action === 'aceptar') {
                        me.getModelo_grid().getStore().reload();
                        me.getAsociarmodelo_win().close();
                    } else {
                        grid.getSelectionModel().deselectAll();
                    }
                }
            });
        } else {
            Ext.MessageBox.show({
                title: 'Alerta',
                msg: 'Debe seleccionar al menos un modelo.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        }
    },

    desasociarModelo: function (btn) {
        var me = this,
            grid = btn.up('grid');

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el modelo seleccionado?', function (btn) {
            if (btn == 'yes') {
                if (grid.getSelectionModel().hasSelection()) {
                    var myMask = new Ext.LoadMask(grid, {msg: 'Eliminando modelos...'});
                    myMask.show();
                    var idtipodiario = me.getTipodiario_grid().getSelectionModel().getLastSelected().get('idtipodiario');
                    var arrIdModelo = new Array();
                    var records = grid.getSelectionModel().getSelection();
                    Ext.each(records, function (record) {
                        arrIdModelo.push(record.get('idmodelocontable'));
                    });

                    Ext.Ajax.request({
                        url: 'desasociarModelo',
                        method: 'POST',
                        params: {
                            idtipodiario: idtipodiario,
                            arridmodelo: Ext.encode(arrIdModelo)
                        },
                        callback: function (options, success, response) {
                            responseData = Ext.decode(response.responseText);
                            myMask.hide();
                            grid.getStore().reload();
                        }
                    });
                } else {
                    Ext.MessageBox.show({
                        title: 'Alerta',
                        msg: 'Debe seleccionar al menos un modelo.',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }
        });
    }

});