Ext.define('TipoDiario.model.Subsistema', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idsubsistema', type: 'int'
        },
        {
            name: 'denominacion', type: 'string'
        }
    ]
});