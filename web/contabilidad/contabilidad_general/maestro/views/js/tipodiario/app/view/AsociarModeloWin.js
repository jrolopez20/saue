Ext.define('TipoDiario.view.AsociarModeloWin', {
    extend: 'Ext.window.Window',
    alias: 'widget.asociarmodelo_win',

    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 800,
    height: 600,
    title:'Asociar modelo',
    initComponent: function () {
        this.items = [{
            xtype: 'modelonoasoc_grid',
            title:'Listado de modelos',
            gestionar: false,
            border: false
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});