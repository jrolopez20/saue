Ext.define('TipoDiario.view.TipoDiarioEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.tipodiario_edit',

    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 400,

    initComponent: function () {
        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'saveTipoDiario',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',

            items: [
                {
                    xtype: 'hidden',
                    name: 'idtipodiario'
                },
                {
                    xtype: 'textfield',
                    name: 'codigo',
                    fieldLabel: 'Código',
                    maxLength: 255,
                    allowBlank: false
                },
                {
                    xtype: 'textfield',
                    name: 'denominacion',
                    fieldLabel: 'Denominación',
                    maxLength: 255,
                    allowBlank: false
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Subsistema',
                    name: 'idsubsistema',
                    anchor: '100%',
                    store: 'TipoDiario.store.Subsistema',
                    editable: false,
                    emptyText: 'Seleccione...',
                    triggerAction: 'all',
                    forceSelection: true,
                    allowBlank: false,
                    valueField: 'idsubsistema',
                    displayField: 'denominacion',
                    mode: 'local'
                },
                {
                    xtype: 'textarea',
                    name: 'descripcion',
                    fieldLabel: 'Descripción',
                    maxLength: 255
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});