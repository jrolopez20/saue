Ext.define('TipoDiario.store.Subsistema', {
    extend: 'Ext.data.Store',
    model: 'TipoDiario.model.Subsistema',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadDataSubsistemas',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});