Ext.define('TipoDiario.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tipodiario_grid',

    store: 'TipoDiario.store.TipoDiario',

    title: 'Listado de tipo de asiento',
    autoScroll: true,
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{columnName}: {name} ({rows.length} Tipo{[values.rows.length > 1 ? "s" : ""]} de asiento)',
            hideGroupedHeader: true
        });

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar tipo de asiento',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Modificar',
                tooltip: 'Modificar tipo de asiento',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'modificar'
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar tipo de asiento',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: 'C&oacute;digo',
                dataIndex: 'codigo',
                width: 80
            },
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            },
            {
                header: 'Descripci&oacute;n',
                dataIndex: 'descripcion',
                flex: 2
            },
            {
                header: 'Subsistema',
                dataIndex: 'subsistema',
                width: 90
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.features = [groupingFeature];

        this.callParent(arguments);
    }
});