Ext.define('TipoDiario.model.TipoDiario', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idtipodiario', type: 'int'},
        {name: 'idtipodiarioestructura', type: 'int'},
        {name: 'idsubsistema', type: 'int'},
        {name: 'uso', type: 'bool'},
        {name: 'codigo', type: 'string'},
        {name: 'denominacion', type: 'string'},
        {name: 'subsistema', type: 'string'},
        {name: 'descripcion', type: 'string'}
    ]
});