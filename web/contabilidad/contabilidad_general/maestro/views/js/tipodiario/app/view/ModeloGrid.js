Ext.define('TipoDiario.view.ModeloGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.modelo_grid',

    title: 'Modelos asociados',
    store: 'TipoDiario.store.Modelo',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
            mode: 'MULTI'
        });

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar modelo al tipo de asiento',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar modelo del tipo de asiento',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});