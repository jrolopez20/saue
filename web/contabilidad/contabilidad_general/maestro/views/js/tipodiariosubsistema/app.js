var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'TipoDiarioSubsistema',

    enableQuickTips: true,

    paths: {
        'TipoDiarioSubsistema': '../../views/js/tipodiariosubsistema/app'
    },

    controllers: ['TipoDiarioSubsistema'],

    launch: function () {
        UCID.portal.cargarEtiquetas('tipodiariosubsistema', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'subsistema_grid',
                        region: 'center',
                        margin: '5 0 5 5',
                        width: '50%'
                    },
                    {
                        xtype: 'tipoasiento_grid',
                        region: 'east',
                        margin: '5 5 5 0',
                        split: true,
                        width: '50%'
                    }
                ]
            });
        });
    }
});