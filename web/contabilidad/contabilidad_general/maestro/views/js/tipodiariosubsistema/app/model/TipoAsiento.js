Ext.define('TipoDiarioSubsistema.model.TipoAsiento', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idtipodiario', type: 'int'},
        {name: 'uso', type: 'bool'},
        {name: 'codigo', type: 'string'},
        {name: 'denominacion', type: 'string'},
        {name: 'descripcion', type: 'string'}
    ]
});