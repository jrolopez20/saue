Ext.define('TipoDiarioSubsistema.store.Subsistema', {
    extend: 'Ext.data.Store',
    model: 'TipoDiarioSubsistema.model.Subsistema',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'listDataSubsistema',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});