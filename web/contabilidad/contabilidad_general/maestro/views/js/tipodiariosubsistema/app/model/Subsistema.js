Ext.define('TipoDiarioSubsistema.model.Subsistema', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idsubsistema', type: 'int'},
        {name: 'denominacion', type: 'string'}
    ]
});