Ext.define('TipoDiarioSubsistema.view.TipoAsientoGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.tipoasiento_grid',

    store: 'TipoDiarioSubsistema.store.TipoAsiento',

    title: 'Listado de tipos de asiento',
    initComponent: function () {
        var me = this;

        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.columns = [
            {
                xtype: 'checkcolumn',
                header: '¿Usar?',
                dataIndex: 'uso',
                width: 70,
                stopSelection: false
            }, {
                header: 'C&oacute;digo',
                dataIndex: 'codigo',
                width: 80
            },
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            },
            {
                header: 'Descripci&oacute;n',
                dataIndex: 'descripcion',
                flex: 2
            }
        ];

        me.tbar = [
            {
                text: 'Guardar',
                tooltip: 'Guardar configuraci&oacute;n',
                icon: perfil.dirImg + 'guardar.png',
                iconCls: 'btn',
                disabled:true,
                action: 'save'
            }
        ];

        me.plugins = [cellEditing];

        this.callParent(arguments);
    }
});