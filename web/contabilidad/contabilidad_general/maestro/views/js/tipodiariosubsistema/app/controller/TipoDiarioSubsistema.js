Ext.define('TipoDiarioSubsistema.controller.TipoDiarioSubsistema', {
    extend: 'Ext.app.Controller',

    views: [
        'TipoDiarioSubsistema.view.SubsistemaGrid',
        'TipoDiarioSubsistema.view.TipoAsientoGrid'

    ],
    stores: [
        'TipoDiarioSubsistema.store.Subsistema',
        'TipoDiarioSubsistema.store.TipoAsiento'
    ],

    models: [
        'TipoDiarioSubsistema.model.Subsistema',
        'TipoDiarioSubsistema.model.TipoAsiento'
    ],

    refs: [
        {ref: 'subsistema_grid', selector: 'subsistema_grid'},
        {ref: 'tipoasiento_grid', selector: 'tipoasiento_grid'},
    ],

    init: function () {
        var me = this;

        me.control({
            'subsistema_grid': {
                selectionchange: me.onSubsistemaSelectionChange
            },
            'tipoasiento_grid button[action=save]': {
                click: me.saveConfig
            }
        });
    },

    onSubsistemaSelectionChange: function (sm) {
        var me = this;

        if (sm.hasSelection()) {
            me.getTipoasiento_grid().down('button[action=save]').enable();
            me.getTipoasiento_grid().getStore().load({
                params: {
                    idsubsistema: sm.getLastSelected().get('idsubsistema')
                }
            });

        } else {
            me.getTipoasiento_grid().down('button[action=save]').disable();
            me.getTipoasiento_grid().getStore().removeAll();
        }
    },

    saveConfig: function(btn){
        var me = this,
            arrTipoAsiento = new Array();
        var records = me.getTipoasiento_grid().getStore().getRange();

        Ext.each(records, function(rec){
            if(rec.get('uso')){
                arrTipoAsiento.push(rec.get('idtipodiario'));
            }
        });
        var recSubsistema = me.getSubsistema_grid().getSelectionModel().getLastSelected();

        var myMask = new Ext.LoadMask(me.getTipoasiento_grid(), {msg: 'Guardando configuraci&oacute;n...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'updateTipoDiarioSubsistema',
            params: {
                idsubsistema: recSubsistema.get('idsubsistema'),
                arrTipoAsiento: Ext.encode(arrTipoAsiento)
            },
            success: function (response) {
                myMask.hide();
                var text = response.responseText;
            },
            failure: function (response, opts) {
                myMask.hide();
                Ext.MessageBox.show({
                    title: 'Error',
                    msg: 'No se ha podido guardar la configuraci&oacute;n.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        });
    }

});