Ext.define('TipoDiarioSubsistema.view.SubsistemaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.subsistema_grid',

    store: 'TipoDiarioSubsistema.store.Subsistema',
    title: 'Listado de subsistemas configurados',
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.columns = [
            Ext.create('Ext.grid.RowNumberer'),
            {
                header: 'Subsistema',
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        this.callParent(arguments);
    }
});