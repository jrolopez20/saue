Ext.define('TipoDiarioSubsistema.store.TipoAsiento', {
    extend: 'Ext.data.Store',
    model: 'TipoDiarioSubsistema.model.TipoAsiento',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'loadTipoDiario',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            successProperty: 'success'
        }
    }
});