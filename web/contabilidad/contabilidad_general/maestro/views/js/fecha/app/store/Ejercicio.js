Ext.define('Fecha.store.Ejercicio', {
    extend: 'Ext.data.Store',
    model: 'Fecha.model.Ejercicio',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadEjercicios',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success'
        }
    }
});