Ext.define('Fecha.controller.Fecha', {
    extend: 'Ext.app.Controller',
    views: [
        'Fecha.view.FechaGrid',
        'Fecha.view.FechaEdit'
    ],
    stores: [
        'Fecha.store.Fecha',
        'Fecha.store.Subsistema',
        'Fecha.store.Ejercicio',
        'Fecha.store.Periodo'
    ],
    models: [
        'Fecha.model.Fecha',
        'Fecha.model.Subsistema',
        'Fecha.model.Ejercicio',
        'Fecha.model.Periodo'
    ],
    refs: [
        {ref: 'fecha_grid', selector: 'fecha_grid'},
        {ref: 'fecha_edit', selector: 'fecha_edit'}
    ],
    init: function() {
        var me = this;
        me.control({
            'fecha_grid': {
                selectionchange: me.onFechaSelectionChange
            },
            'fecha_grid button[action=confFormato]': {
                click: me.configurarFecha
            },
            'fecha_grid button[action=modComportamiento]': {
                click: me.modificarComportamiento
            },
            'fecha_grid button[action=deleteFecha]': {
                click: me.deleteFecha
            },
            'fecha_grid menu[action=selectDatePicker]': {
                select: me.selectDatePicker
            },
            'fecha_edit combobox[name=idejercicio]': {
                select: me.onCbEjercicioSelected
            },
            'fecha_edit combobox[name=idperiodo]': {
                select: me.onCbPeriodoSelected
            },
            'fecha_edit button[action=aceptar]': {
                click: me.addFecha
            },
            'fecha_edit button[action=aplicar]': {
                click: me.addFecha
            }
        });

    },
    onFechaSelectionChange: function(sm) {
        var grid = this.getFecha_grid();

        grid.down('button[action=deleteFecha]').disable();
        grid.down('button[action=modComportamiento]').disable();
        grid.down('button[action=pasarDia]').disable();

        if (sm.hasSelection()) {
            grid.down('button[action=deleteFecha]').enable();
            grid.down('button[action=modComportamiento]').enable();
            grid.down('button[action=pasarDia]').enable();

            var record = sm.getLastSelected();
            grid.down('*[name=dateMenu]').picker.setValue(new Date(record.get('fecha')));
            grid.down('*[name=dateMenu]').picker.minDate = new Date(record.get('fecha'));
            grid.down('*[name=dateMenu]').picker.maxDate = new Date(record.get('finperiodo'));

        }
    },
    configurarFecha: function(btn) {
        var win = Ext.widget('fecha_edit');
        win.down('button[action=aplicar]').show();
    },
    deleteFecha: function(btn) {
        var me = this;
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirme, function(btn) {
            if (btn == 'yes') {
                var record = me.getFecha_grid().getSelectionModel().getLastSelected();

                var myMask = new Ext.LoadMask(me.getFecha_grid(), {msg: perfil.etiquetas.msgEliminando});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteDate',
                    method: 'POST',
                    params: {
                        idfecha: record.get('idfecha')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getFecha_grid().getStore().reload();
                    }
                });
            }
        });
    },
    modificarComportamiento: function(btn) {
        var me = this;
        var record = this.getFecha_grid().getSelectionModel().getLastSelected();

        var myMask = new Ext.LoadMask(this.getFecha_grid(), {msg: perfil.etiquetas.msgModificando});
        myMask.show();
        Ext.Ajax.request({
            url: 'updateDate',
            method: 'POST',
            params: {
                idfecha: record.get('idfecha'),
                incremento: record.get('incremento')
            },
            callback: function(options, success, response) {
                responseData = Ext.decode(response.responseText);
                myMask.hide();
                me.getFecha_grid().getStore().reload();
            }
        });
    },
    selectDatePicker: function(dp, date) {
        var me = this,
                grid = this.getFecha_grid();

        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmeCambioFecha, function(btn) {
            if (btn == 'yes') {
                if (grid.getSelectionModel().hasSelection()) {
                    var myMask = new Ext.LoadMask(me.getFecha_grid(), {msg: perfil.etiquetas.msgPasarDia});
                    myMask.show();

                    var record = grid.getSelectionModel().getLastSelected();
                    var newDate = Ext.Date.format(date, "d/m/Y");
                    Ext.Ajax.request({
                        url: 'nextDate',
                        method: 'POST',
                        params: {
                            idfecha: record.get('idfecha'),
                            fecha: newDate
                        },
                        callback: function(options, success, response) {
                            responseData = Ext.decode(response.responseText);
                            myMask.hide();
                            grid.getStore().reload();
                        }
                    });
                }
            }
        });
    },
    onCbEjercicioSelected: function(cb, records) {
        var me = this;
        var form = me.getFecha_edit().down('form');
        form.getForm().findField('idperiodo').enable();

        form.getForm().findField('idperiodo').getStore().load({
            params: {
                idejercicio: records[0].get('idejercicio')
            }
        });
    },
    onCbPeriodoSelected: function(cb, records) {
        var me = this;
        var form = me.getFecha_edit().down('form');
        var field = form.getForm().findField('fecha');

        field.enable();
        field.setMinValue(records[0].get('inicio'));
        field.setValue(records[0].get('inicio'));
        field.setMaxValue(records[0].get('fin'));
    },
    addFecha: function(btn) {
        var me = this;
        var form = me.getFecha_edit().down('form').getForm();
        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getFecha_edit(), {msg: perfil.etiquetas.msgAdicionando});
            myMask.show();

            Ext.Ajax.request({
                url: 'addDate',
                params: form.getValues(),
                success: function(response) {
                    myMask.hide();
                    me.getFecha_grid().getStore().load();

                    if (btn.action === 'aceptar') {
                        me.getFecha_edit().close();
                    } else {
                        form.reset();
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    }

});