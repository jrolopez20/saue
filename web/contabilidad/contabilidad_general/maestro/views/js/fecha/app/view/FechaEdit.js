Ext.define('Fecha.view.FechaEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.fecha_edit',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 500,
    title: 'Configurar fecha contable',
    initComponent: function() {
        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'addDate',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'idfecha'
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Subsistema',
                    name: 'idsubsistema',
                    anchor: '100%',
                    store: 'Fecha.store.Subsistema',
                    editable: false,
                    emptyText: 'Seleccione...',
                    triggerAction: 'all',
                    forceSelection: true,
                    allowBlank: false,
                    valueField: 'idsubsistema',
                    displayField: 'denominacion',
                    mode: 'local'
                },
                {
                    xtype: 'container',
                    anchor: '100%',
                    layout: 'hbox',
                    items: [{
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            items: [{
                                    xtype: 'combobox',
                                    fieldLabel: 'Ejercicio',
                                    name: 'idejercicio',
                                    anchor: '95%',
                                    store: 'Fecha.store.Ejercicio',
                                    editable: false,
                                    emptyText: 'Seleccione...',
                                    triggerAction: 'all',
                                    forceSelection: true,
                                    allowBlank: false,
                                    valueField: 'idejercicio',
                                    displayField: 'nombre',
                                    mode: 'local'
                                }, {
                                    xtype: 'datefield',
                                    allowBlank: false,
                                    disabled: true,
                                    minValue: new Date(),
                                    maskRe: /^()+$/,
                                    fieldLabel: 'Fecha',
                                    emptyText: 'Seleccione...',
                                    format: 'd/m/Y',
                                    name: 'fecha',
                                    anchor: '95%'
                                }]
                        }, {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            items: [{
                                    xtype: 'combobox',
                                    fieldLabel: 'Período',
                                    name: 'idperiodo',
                                    disabled: true,
                                    anchor: '100%',
                                    store: 'Fecha.store.Periodo',
                                    editable: false,
                                    emptyText: 'Seleccione...',
                                    forceSelection: true,
                                    allowBlank: false,
                                    valueField: 'idperiodo',
                                    displayField: 'nombre',
                                    queryMode: 'local'
                                }, {
                                    xtype: 'combobox',
                                    fieldLabel: 'Comportamiento',
                                    name: 'incremento',
                                    anchor: '100%',
                                    store: Ext.create('Ext.data.Store', {
                                        fields: ['incremento', 'denominacion'],
                                        data: [
                                            {incremento: '0', denominacion: 'Manual'},
                                            {incremento: '1', denominacion: 'Automático'}
                                        ]
                                    }),
                                    editable: false,
                                    emptyText: 'Seleccione...',
                                    triggerAction: 'all',
                                    forceSelection: true,
                                    allowBlank: false,
                                    valueField: 'incremento',
                                    displayField: 'denominacion',
                                    typeAhead: true,
                                    mode: 'local'
                                }]
                        }]
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});