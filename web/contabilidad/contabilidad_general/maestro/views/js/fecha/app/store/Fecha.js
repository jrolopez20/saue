Ext.define('Fecha.store.Fecha', {
    extend: 'Ext.data.Store',
    model: 'Fecha.model.Fecha',

    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'listDataFechas',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
    //------------------------------------------
    //model: 'Fecha.model.Fecha',
    //autoLoad: true,
    //autoSync: true,
    //proxy: {
    //    type: 'ajax',
    //    api: {
    //        read: 'listDataFechas',
    //        create: 'app.php/users/create',
    //        update: 'app.php/users/update',
    //        destroy: 'app.php/users/destroy'
    //    },
    //    reader: {
    //        type: 'json',
    //        successProperty: 'success',
    //        root: 'datos',
    //        totalProperty: 'cantidad',
    //        messageProperty: 'message'
    //    },
    //    writer: {
    //        type: 'json',
    //        writeAllFields: false,
    //        root: 'datos'
    //    },
    //    listeners: {
    //        exception: function(proxy, response, operation){
    //            Ext.MessageBox.show({
    //                title: 'REMOTE EXCEPTION',
    //                msg: operation.getError(),
    //                icon: Ext.MessageBox.ERROR,
    //                buttons: Ext.Msg.OK
    //            });
    //        }
    //    }
    //},
    //listeners: {
    //    write: function(proxy, operation){
    //        //if (operation.action == 'destroy') {
    //        //    main.child('#form').setActiveRecord(null);
    //        //}
    //        //Ext.example.msg(operation.action, operation.resultSet.message);
    //    }
    //}
});