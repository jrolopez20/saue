Ext.define('Fecha.model.Ejercicio', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idejercicio', type: 'int'
        }, {
            name: 'nombre', type: 'string'
        }
    ]
});