Ext.define('Fecha.store.Subsistema', {
    extend: 'Ext.data.Store',
    model: 'Fecha.model.Subsistema',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadSubsistemas',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});