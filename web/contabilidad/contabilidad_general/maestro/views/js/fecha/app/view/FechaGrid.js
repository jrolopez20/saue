Ext.define('Fecha.view.FechaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fecha_grid',

    store: 'Fecha.store.Fecha',
    frame: true,
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        this.dateMenu = Ext.create('Ext.menu.DatePicker', {
            name:'dateMenu',
            format:'d/m/Y',
            action: 'selectDatePicker'
        });

        me.pasarDiaMenu = Ext.create('Ext.menu.Menu', {
            items: [{
                text: 'Ir al día',
                iconCls: 'calendar',
                menu: this.dateMenu
            }]
        });

        me.tbar = [
            {
                text: 'Configurar fecha',
                tooltip: 'Configurar fecha contable',
                icon: perfil.dirImg + 'conciliar.png',
                iconCls: 'btn',
                action: 'confFormato'
            },
            {
                text: 'Modificar comportamiento',
                tooltip: 'Modificar comportamiento',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modComportamiento'
            },
            {
                text: 'Eliminar',
                tooltip: 'Eliminar formato',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'deleteFecha'
            },
            {
                text: 'Pasar día',
                icon: perfil.dirImg + 'cambiar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'pasarDia',
                menu: me.pasarDiaMenu
            }
        ];


        me.columns = [
            Ext.create('Ext.grid.RowNumberer'),
            {
                header: 'Subsistema',
                dataIndex: 'subsistema',
                flex: 1
            },
            {
                header: 'Comportamiento',
                dataIndex: 'incremento',
                width: 120,
                renderer: function (v) {
                    if (v == 0)
                        return "Manual";
                    return "Autom&aacute;tico";
                }
            },
            {
                header: 'Fecha',
                dataIndex: 'fecha',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 110
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});