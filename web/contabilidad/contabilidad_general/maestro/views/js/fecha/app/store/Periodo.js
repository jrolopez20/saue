Ext.define('Fecha.store.Periodo', {
    extend: 'Ext.data.Store',
    model: 'Fecha.model.Periodo',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadPeriodos',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});