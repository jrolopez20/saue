var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Fecha',

    enableQuickTips: true,

    paths: {
        'Fecha': '../../views/js/fecha/app'
    },

    controllers: ['Fecha'],

    launch: function () {
        UCID.portal.cargarEtiquetas('fecha', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'fecha_grid',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});