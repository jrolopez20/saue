Ext.define('Ejercicio.view.EjercicioGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ejercicio_grid',
    store: 'Ejercicio.store.Ejercicio',
    initComponent: function() {
        var me = this;
        me.title = perfil.etiquetas.lbTitleEjercicios;
        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip:'Adicionar ejercicio contable',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'addEjercicio'
            },
            {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip:'Eliminar ejercicio contable',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                action: 'deleteEjercicio'
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbNombre,
                dataIndex: 'nombre',
                width: 100
            },
            {
                header: perfil.etiquetas.lbFechainicio,
                width: 120,
                dataIndex: 'inicio',
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            }, {
                header: perfil.etiquetas.lbFechafin,
                width: 120,
                dataIndex: 'fin',
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            }
        ];

        this.callParent(arguments);
    }
});