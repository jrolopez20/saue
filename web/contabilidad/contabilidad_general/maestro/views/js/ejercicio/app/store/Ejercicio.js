Ext.define('Ejercicio.store.Ejercicio', {
    extend: 'Ext.data.Store',
    model: 'Ejercicio.model.Ejercicio',
    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: 'listDataEjercicio',
        reader: {
            type: 'json',
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }

});