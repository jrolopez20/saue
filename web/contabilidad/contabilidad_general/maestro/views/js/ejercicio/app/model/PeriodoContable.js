Ext.define('Ejercicio.model.PeriodoContable', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idperiodo', type: 'int'},
        {name: 'idejercicio', type: 'int'},
        {name: 'nombre', type: 'string'},
        {name: 'inicio', type: 'date'},
        {name: 'fin', type: 'date'}
    ]
});