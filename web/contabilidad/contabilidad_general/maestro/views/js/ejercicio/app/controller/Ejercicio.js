Ext.define('Ejercicio.controller.Ejercicio', {
    extend: 'Ext.app.Controller',
    views: [
        'Ejercicio.view.EjercicioGrid',
        'Ejercicio.view.PeriodoContableGrid'
    ],
    stores: [
        'Ejercicio.store.Ejercicio',
        'Ejercicio.store.PeriodoContable'
    ],
    models: [
        'Ejercicio.model.Ejercicio',
        'Ejercicio.model.PeriodoContable'
    ],
    refs: [
        {ref: 'ejercicio_grid', selector: 'ejercicio_grid'},
        {ref: 'periodocontable_grid', selector: 'periodocontable_grid'}
    ],
    init: function() {
        var me = this;

        me.control({
            'ejercicio_grid': {
                selectionchange: me.onEjercicioSelectionChange
            },
            'ejercicio_grid button[action=addEjercicio]': {
                click: me.addEjercicio
            },
            'ejercicio_grid button[action=deleteEjercicio]': {
                click: me.deleteEjercicio
            }
        });
    },
    addEjercicio: function(btn) {
        var myMask = new Ext.LoadMask(this.getEjercicio_grid(), {msg: perfil.etiquetas.lbAdicionando});
        var st = this.getEjercicio_grid().getStore();
        myMask.show();
        Ext.Ajax.request({
            url: 'addEjercicio',
            method: 'POST',
            callback: function(options, success, response) {
                var responseData = Ext.decode(response.responseText);
                myMask.hide();
                st.reload();
            }
        });
    },
    deleteEjercicio: function(btn) {
        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirmar, perfil.etiquetas.lbMsgConfirmar, function(btn) {
            if (btn == 'yes') {
                var rec = this.getEjercicio_grid().getSelectionModel().getSelection();
                var myMask = new Ext.LoadMask(this.getEjercicio_grid(), {msg: perfil.etiquetas.lbEliminando});
                myMask.show();
                var st = this.getEjercicio_grid().getStore();
                Ext.Ajax.request({
                    url: 'deleteEjercicio',
                    method: 'POST',
                    success: function(response) {
                        myMask.hide();
                        st.load();
                    },
                    failure: function(response, opts) {
                        myMask.hide();
                    }
                });
            }
        }, this);
    },
    onEjercicioSelectionChange: function(sm, arrSelectedRecord) {
        if (sm.hasSelection()) {
            this.getPeriodocontable_grid().getStore().load({
                params: {
                    idejercicio: arrSelectedRecord[0].get('idejercicio')
                }
            });
        } else {
            this.getPeriodocontable_grid().getStore().removeAll();
        }
    }
});