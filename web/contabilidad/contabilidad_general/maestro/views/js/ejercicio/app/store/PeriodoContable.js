Ext.define('Ejercicio.store.PeriodoContable', {
    extend: 'Ext.data.Store',
    model: 'Ejercicio.model.PeriodoContable',

    autoLoad: false,
    proxy: {
        type: 'rest',
        api: {
            read: 'listDataPeriodo'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});