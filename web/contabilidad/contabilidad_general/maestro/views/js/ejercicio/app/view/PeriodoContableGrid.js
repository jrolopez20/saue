Ext.define('Ejercicio.view.PeriodoContableGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.periodocontable_grid',
    store: 'Ejercicio.store.PeriodoContable',
    initComponent: function() {
        var me = this;
        me.title = perfil.etiquetas.lbTitlePeriodos;
        me.selModel = Ext.create('Ext.selection.RowModel');

        me.columns = [
            {
                header: perfil.etiquetas.lbNombre,
                dataIndex: 'nombre',
                width: 100
            },
            {
                header: perfil.etiquetas.lbFechainicio,
                width: 120,
                dataIndex: 'inicio',
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            }, {
                header: perfil.etiquetas.lbFechafin,
                width: 120,
                dataIndex: 'fin',
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            }
        ];

        this.callParent(arguments);
    }
});