var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Ejercicio',

    enableQuickTips: true,

    paths: {
        'Ejercicio': '../../views/js/ejercicio/app'
    },

    controllers: ['Ejercicio'],

    launch: function () {
        UCID.portal.cargarEtiquetas('ejercicio', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'ejercicio_grid',
                        region:'center',
                        width: '50%',
                        margin:'5 0 5 5'

                    },
                    {
                        xtype: 'periodocontable_grid',
                        region:'east',
                        split: true,
                        width: '50%',
                        margin:'5 5 5 0'
                    }
                ]
            });
        });
    }
});