Ext.define('Concepto.model.Concepto', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idconcepto', type: 'int'},
        {name: 'denominacion', type: 'string'}
    ]
});