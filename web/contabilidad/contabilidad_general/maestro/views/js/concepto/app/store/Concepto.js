Ext.define('Concepto.store.Concepto', {
    extend: 'Ext.data.Store',
    model: 'Concepto.model.Concepto',

    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'listDataConcepto'
            //update: 'saveConfTipoDiario',
            //delete: 'saveConfTipoDiario'
        },
        actionMethods: {
            read: 'POST'
            //update: 'POST',
            //delete: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});