Ext.define('Concepto.controller.Concepto', {
    extend: 'Ext.app.Controller',
    views: [
        'Concepto.view.Grid',
        'Concepto.view.ConceptoEdit'
    ],
    stores: [
        'Concepto.store.Concepto'
    ],
    models: [
        'Concepto.model.Concepto'
    ],
    refs: [
        {ref: 'concepto_grid', selector: 'concepto_grid'},
        {ref: 'concepto_edit', selector: 'concepto_edit'}
    ],
    init: function() {
        var me = this;

        me.control({
            'concepto_grid': {
                selectionchange: me.onConceptoSelectionChange
            },
            'concepto_grid button[action=adicionar]': {
                click: me.addConcept
            },
            'concepto_grid button[action=modificar]': {
                click: me.modConcept
            },
            'concepto_grid button[action=eliminar]': {
                click: me.delConcept
            },
            'concepto_edit button[action=aceptar]': {
                click: me.saveConcept
            },
            'concepto_edit button[action=aplicar]': {
                click: me.saveConcept
            }
        });
    },
    onConceptoSelectionChange: function(sm) {
        var me = this;
        me.getConcepto_grid().down('button[action=modificar]').disable();
        me.getConcepto_grid().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getConcepto_grid().down('button[action=modificar]').enable();
            me.getConcepto_grid().down('button[action=eliminar]').enable();
        }
    },
    addConcept: function(btn) {
        var win = Ext.widget('concepto_edit');
        win.setTitle('Adicionar concepto');
        win.down('button[action=aplicar]').show();
    },
    modConcept: function(btn) {
        var win = Ext.widget('concepto_edit');
        win.setTitle('Modificar concepto');
        win.down('button[action=aplicar]').hide();
        var form = win.down('form');
        var record = this.getConcepto_grid().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },
    delConcept: function(btn) {
        var me = this;
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el concepto contable seleccionado?', function(btn) {
            if (btn == 'yes') {
                var record = me.getConcepto_grid().getSelectionModel().getLastSelected();

                var myMask = new Ext.LoadMask(me.getConcepto_grid(), {msg: 'Eliminando concepto...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteConcepto',
                    method: 'POST',
                    params: {
                        idconcepto: record.get('idconcepto')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getConcepto_grid().getStore().reload();
                    }
                });
            }
        });
    },
    saveConcept: function(btn) {
        var me = this;
        var form = me.getConcepto_edit().down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getConcepto_edit(), {msg: 'Guardando concepto...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                url: 'saveConcepto',
                success: function(response) {
                    myMask.hide();

                    me.getConcepto_grid().getStore().load();

                    if (btn.action === 'aceptar') {
                        me.getConcepto_edit().close();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    }

});