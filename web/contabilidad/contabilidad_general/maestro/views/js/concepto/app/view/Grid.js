Ext.define('Concepto.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.concepto_grid',
    store: 'Concepto.store.Concepto',
    initComponent: function() {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar concepto contable',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: 'Modificar',
                tooltip: 'Modificar concepto contable',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: 'Eliminar',
                tooltip: 'Eliminar concepto contable',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.plugins = [cellEditing];

        this.callParent(arguments);
    }
});