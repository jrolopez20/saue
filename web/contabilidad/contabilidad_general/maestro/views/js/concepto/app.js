var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Concepto',
    enableQuickTips: true,
    paths: {
        'Concepto': '../../views/js/concepto/app'
    },
    controllers: ['Concepto'],
    launch: function() {
        UCID.portal.cargarEtiquetas('concepto', function() {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'concepto_grid'
                    }
                ]
            });
        });
    }
});