Ext.define('Modelo.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.modelo_grid',
    store: 'Modelo.store.Modelo',
    initComponent: function() {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar modelo contable',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: 'Modificar',
                tooltip: 'Modificar modelo contable',
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: 'Eliminar',
                tooltip: 'Eliminar modelo contable',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        me.plugins = [cellEditing];

        this.callParent(arguments);
    }
});