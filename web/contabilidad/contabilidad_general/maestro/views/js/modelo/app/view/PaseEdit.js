Ext.define('Modelo.view.PaseEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.pase_edit',
    layout: 'border',
    modal: true,
    resizable: true,
    maximizable: true,
    autoShow: true,
    width: 800,
    height: 500,
    initComponent: function () {

        this.items = [{
            xtype: 'form',
            region: 'north',
            autoHeight: true,
            layout: 'form',
            frame: true,
            bodyPadding: '5 5 0',
            margins: '5 5 0 5',
            items: [
                {
                    xtype: 'radiogroup',
                    labelWidth: 300,
                    fieldLabel: 'Seleccione por donde es afectada la cuenta',
                    allowBlank: false,
                    items: [
                        {boxLabel: 'Débito', name: 'debitocredito', inputValue: 0, checked: true},
                        {boxLabel: 'Crédito', name: 'debitocredito', inputValue: 1}
                    ]
                }
            ]
        },{
            xtype: 'cuenta_tree',
            checked: true,
            idnaturaleza: [8030,8031],
            anySelection: false,
            region: 'center',
            margins: '5 5 5 5'
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});