Ext.define('Modelo.model.Pase', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idmodelocontablecuenta', type: 'int'},
        {name: 'idmodelocontable', type: 'int'},
        {name: 'idcuenta', type: 'int'},
        {name: 'debitocredito', type: 'int'},
        {name: 'concatcta', type: 'string'},
        {name: 'denominacion', type: 'string'}
    ]
});