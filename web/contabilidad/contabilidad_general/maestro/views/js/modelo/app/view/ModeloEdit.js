Ext.define('Modelo.view.ModeloEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.modelo_edit',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 400,
    initComponent: function() {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'saveModelo',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'idmodelocontable'
                },
                {
                    xtype: 'textfield',
                    name: 'denominacion',
                    fieldLabel: 'Denominación',
                    maxLength: 255,
                    allowBlank: false
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});