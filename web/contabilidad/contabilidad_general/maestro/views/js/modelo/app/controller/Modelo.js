Ext.define('Modelo.controller.Modelo', {
    extend: 'Ext.app.Controller',
    views: [
        'Modelo.view.Grid',
        'Modelo.view.ModeloEdit',
        'Modelo.view.PaseEdit',
        'Modelo.view.PaseGrid'
    ],
    stores: [
        'Modelo.store.Modelo',
        'Modelo.store.Pase'
    ],
    models: [
        'Modelo.model.Modelo',
        'Modelo.model.Pase'
    ],
    refs: [
        {ref: 'modelo_grid', selector: 'modelo_grid'},
        {ref: 'modelo_edit', selector: 'modelo_edit'},
        {ref: 'pase_edit', selector: 'pase_edit'},
        {ref: 'pase_grid', selector: 'pase_grid'}
    ],
    init: function() {
        var me = this;

        me.control({
            'modelo_grid': {
                selectionchange: me.onModeloSelectionChange
            },
            'pase_grid': {
                selectionchange: me.onPaseSelectionChange
            },
            'modelo_grid button[action=adicionar]': {
                click: me.addConcept
            },
            'modelo_grid button[action=modificar]': {
                click: me.modConcept
            },
            'modelo_grid button[action=eliminar]': {
                click: me.delConcept
            },
            'modelo_edit button[action=aceptar]': {
                click: me.saveConcept
            },
            'modelo_edit button[action=aplicar]': {
                click: me.saveConcept
            },
            'pase_edit button[action=aceptar]': {
                click: me.savePase
            },
            'pase_edit button[action=aplicar]': {
                click: me.savePase
            },
            'pase_grid button[action=adicionar]': {
                click: me.addPase
            },
            'pase_grid button[action=eliminar]': {
                click: me.delPase
            }
        });
    },
    onModeloSelectionChange: function(sm) {
        var me = this;
        me.getModelo_grid().down('button[action=modificar]').disable();
        me.getModelo_grid().down('button[action=eliminar]').disable();
        me.getPase_grid().down('button[action=adicionar]').disable();

        if (sm.hasSelection()) {
            me.getModelo_grid().down('button[action=modificar]').enable();
            me.getModelo_grid().down('button[action=eliminar]').enable();
            me.getPase_grid().down('button[action=adicionar]').enable();
            me.getPase_grid().getStore().load({
                params: {
                    limit: 20,
                    start: 0,
                    idmodelocontable: sm.getLastSelected().get('idmodelocontable')
                }
            });
        } else {
            me.getPase_grid().getStore().removeAll();
        }
    },
    onPaseSelectionChange: function(sm) {
        var me = this;
        me.getPase_grid().down('button[action=eliminar]').disable();

        if (sm.hasSelection()) {
            me.getPase_grid().down('button[action=eliminar]').enable();
        }
    },
    addConcept: function(btn) {
        var win = Ext.widget('modelo_edit');
        win.setTitle('Adicionar modelo');
        win.down('button[action=aplicar]').show();
    },
    modConcept: function(btn) {
        var win = Ext.widget('modelo_edit');
        win.setTitle('Modificar modelo');
        win.down('button[action=aplicar]').hide();
        var form = win.down('form');
        var record = this.getModelo_grid().getSelectionModel().getLastSelected();
        form.loadRecord(record);
    },
    delConcept: function(btn) {
        var me = this;
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar el modelo contable seleccionado?', function(btn) {
            if (btn == 'yes') {
                var record = me.getModelo_grid().getSelectionModel().getLastSelected();

                var myMask = new Ext.LoadMask(me.getModelo_grid(), {msg: 'Eliminando modelo...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deleteModelo',
                    method: 'POST',
                    params: {
                        idmodelocontable: record.get('idmodelocontable')
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getModelo_grid().getStore().reload();
                    }
                });
            }
        });
    },
    delPase: function(btn) {
        var me = this;
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea eliminar los pases seleccionados?', function(btn) {
            if (btn == 'yes') {
                var records = me.getPase_grid().getSelectionModel().getSelection();
                var arrModelCuenta = new Array(),
                        totalRecords = records.length;

                for (var i = 0; i < totalRecords; i++) {
                    arrModelCuenta.push(records[i].get('idmodelocontablecuenta'));
                }

                var myMask = new Ext.LoadMask(me.getPase_grid(), {msg: 'Eliminando pase...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'deletePase',
                    method: 'POST',
                    params: {
                        idpases: Ext.encode(arrModelCuenta)
                    },
                    callback: function(options, success, response) {
                        responseData = Ext.decode(response.responseText);
                        myMask.hide();
                        me.getPase_grid().getStore().reload();
                    }
                });
            }
        });
    },
    saveConcept: function(btn) {
        var me = this;
        var form = me.getModelo_edit().down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getModelo_edit(), {msg: 'Guardando modelo...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                clientValidation: true,
                url: 'saveModelo',
                success: function(response) {
                    myMask.hide();

                    me.getModelo_grid().getStore().load();

                    if (btn.action === 'aceptar') {
                        me.getModelo_edit().close();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function(response, opts) {
                    myMask.hide();
                }
            });
        }
    },
    addPase: function(btn) {
        var win = Ext.widget('pase_edit');
        win.setTitle('Adicionar pase');
    },
    savePase: function(btn) {
        var me = this,
                form = me.getPase_edit().down('form').getForm(),
                tree = me.getPase_edit().down('treepanel');

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(me.getPase_edit(), {msg: 'Guardando modelo...'});
            myMask.show();
            var selectedRecords = tree.getChecked(),
                    totalRecords = selectedRecords.length,
                    idcuentas = new Array();

            for (var i = 0; i < totalRecords; i++) {
                idcuentas.push(selectedRecords[i].get('idcuenta'));
            }

            Ext.Ajax.request({
                url: 'savePase',
                params: {
                    idmodelocontable: me.getModelo_grid().getSelectionModel().getLastSelected().get('idmodelocontable'),
                    debitocredito: (form.findField('debitocredito').getValue()) ? '0' : '1',
                    idcuentas: Ext.encode(idcuentas)
                },
                success: function(response) {
                    var text = response.responseText;
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        me.getPase_grid().getStore().reload();
                        me.getPase_edit().close();
                    }
                    else {
                        form.reset();
                        tree.getSelectionModel().deselectAll();
                    }
                }
            });
        }
    },
    changeDebitoCredito: function(grid, rowIndex, colIndex, item, e, record) {
        var me = this,
                oldStatus = record.get('debitocredito'),
                newStatus = oldStatus == 1 ? 0 : 1;

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea cambiar el pase a: ' + (newStatus == 1 ? 'Crédito' : 'Débito') + '?',
                function(btn) {
                    if (btn == 'yes') {
                        var myMask = new Ext.LoadMask(me.getPase_grid(), {msg: 'Cambiando datos del pase...'});
                        myMask.show();
                        Ext.Ajax.request({
                            url: 'savePase',
                            method: 'POST',
                            params: {
                                idmodelocontablecuenta: record.get('idmodelocontablecuenta'),
                                debitocredito: newStatus
                            },
                            callback: function(options, success, response) {
                                responseData = Ext.decode(response.responseText);
                                myMask.hide();
                                me.getPase_grid().getStore().reload();
                            }
                        });
                    }
                }
        );
    }

});