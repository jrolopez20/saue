Ext.define('Modelo.view.PaseGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pase_grid',
    store: 'Modelo.store.Pase',
    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.CheckboxModel');

        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.tbar = [
            {
                text: 'Adicionar',
                tooltip: 'Adicionar pase',
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'adicionar'
            }, {
                text: 'Eliminar',
                tooltip: 'Eliminar pase',
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }
        ];

        me.columns = [
            {
                header: 'C&oacute;digo',
                dataIndex: 'concatcta',
                width: 140
            },
            {
                header: 'Denominaci&oacute;n',
                dataIndex: 'denominacion',
                flex: 1
            },
            {
                header: 'Débito o crédito',
                dataIndex: 'debitocredito',
                width: 120,
                renderer: function (val) {
                    var r = '<div style="font-weight: bold">';
                    r += val == 1 ? 'Crédito' : 'Débito';
                    r += '</div>';
                    return r;
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                draggable: false,
                hideable: false,
                menuDisabled: true,
                items: [{
                    icon: perfil.dirImg + 'aceptar.png',
                    tooltip: 'Cambia por donde se afecta la cuenta',
                    handler: function (grid, rowIndex, colIndex, item, e, record) {
                        var grid = this.up('grid'),
                            oldStatus = record.get('debitocredito'),
                            newStatus = oldStatus == 1 ? 0 : 1;

                        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea cambiar el pase a: ' + (newStatus == 1 ? 'Crédito' : 'Débito') + '?',
                            function (btn) {
                                if (btn == 'yes') {
                                    var myMask = new Ext.LoadMask(grid, {msg: 'Cambiando datos del pase...'});
                                    myMask.show();
                                    Ext.Ajax.request({
                                        url: 'savePase',
                                        method: 'POST',
                                        params: {
                                            idmodelocontablecuenta: record.get('idmodelocontablecuenta'),
                                            debitocredito: newStatus
                                        },
                                        callback: function (options, success, response) {
                                            responseData = Ext.decode(response.responseText);
                                            myMask.hide();
                                            grid.getStore().reload();
                                        }
                                    });
                                }
                            }
                        );
                    }
                }]
            }
        ];

        me.plugins = [cellEditing];

        this.callParent(arguments);
    }
});