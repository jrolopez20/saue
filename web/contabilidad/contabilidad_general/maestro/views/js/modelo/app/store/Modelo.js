Ext.define('Modelo.store.Modelo', {
    extend: 'Ext.data.Store',
    model: 'Modelo.model.Modelo',

    autoLoad: true,
    pageSize: 25,
    proxy: {
        type: 'rest',
        api: {
            read: 'listDataModelo'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});