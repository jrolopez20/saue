Ext.define('Modelo.store.Pase', {
    extend: 'Ext.data.Store',
    model: 'Modelo.model.Pase',

    autoLoad: false,
    pageSize: 25,
    sorters: ['debitocredito', 'denominacion'],
    proxy: {
        type: 'rest',
        api: {
            read: 'listDataPase'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cant',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});