var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Modelo',
    enableQuickTips: true,
    paths: {
        'Modelo': '../../views/js/modelo/app'
    },
    controllers: ['Modelo'],
    launch: function() {
        UCID.portal.cargarEtiquetas('modelo', function() {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'modelo_grid',
                        title:'Listado de modelos',
                        margins: '5 0 5 5',
                        region:'center',
                        width: '50%'
                    },
                    {
                        xtype: 'pase_grid',
                        title: 'Listado de pases',
                        margins: '5 5 5 0',
                        split: true,
                        region:'east',
                        width: '50%'
                    }
                ]
            });
        });
    }
});