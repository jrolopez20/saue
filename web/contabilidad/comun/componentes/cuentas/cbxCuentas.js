/**
 * Created by Team Delta on 22/11/2014.
 */
Ext.define('Ext.contabilidad.util.cbxCuentas', {
    extend:'Ext.form.field.ComboBox',
    alternateClassName: 'Ext.form.cbxCuentas',
    alias: 'widget.cbxCuentas',

    constructor: function(params) {
            /* DEFAULT CONFIG */
            this.listHeight = 150;
            this.singleSelection = true;
            this.fieldLabel = 'Cuentas';
            //this.perfil = window.parent.UCID.portal.perfil;
            this.allowBlank = false;
            this.forceSelection = true;
            this.typeAhead = true;
            this.mode = 'local';
            this.triggerAction = 'all';
            this.enableKeyEvents = true;
            this.anchor = '100%';
            this.onSelect = Ext.emptyFn;
            this.lazyRender = true;
            this.selectedClass = '';
            this.emptyText = 'Seleccione...';
            this.blankText = 'Este campo es obligatorio.';
            this.busyRequest = false;
            this.hideBbar = false;
            this.url = '';
            /* END DEFAULT CONFIG */

            this.addEvents(
                "beforeload",
                "load",
                "loadexception"
            );

            Ext.apply(this, params || {});

            this.editable = false;
            this.tpl = '<tpl for="."><div id = \"dataCbx\" style = \"height:' + params.listHeight + 'px; !important;\"></div></tpl>';

            this.store = new Ext.data.Store({
                fields: [],
                data: [[]]
            });

            this.initialice();
            this.initializeEvents();
            //this.initFunctions();
            return Ext.contabilidad.util.cbxCuentas.superclass.constructor.call(this);
    },
    initialice: function() {
        var _me = this;

        _me.tree = Ext.create('Ext.tree.Panel',{
            layout: 'fit',
            rootVisible: false,
            containerScroll: true,
            autoScroll: true,
            ddScroll: true,
            trackMouseOver: true,
            bodyStyle: 'background-color:#fff;',
            shadow: true,
            animate: true,
            enableDD: false,
            lines: false,
            store: _me.tStore = new Ext.data.TreeStore({
                proxy: {
                    type: 'ajax',
                    url: '/contabilidad/contabilidad_general/comprobante/index.php/comprobante/loadCuentas'
                },
                sorters: [{
                    property: 'codigo',
                    direction: 'ASC'
                }, {
                    property: 'denominacion',
                    direction: 'ASC'
                }]
            })
        });
    },
    initializeEvents: function() {
        var _me = this;

        this.on({
            scope: _me,
            expand: function(combo) {
                this.tree.setHeight(this.listHeight);
                //this.sfBuscar.setWidth(this.getSize().width + 10);
                if (!this.tree.rendered) {
                    this.tree.render('dataCbx');
                }
            }
        });
    }
});