Ext.define('Ext.ux.PrintView', {
    extend: 'Ext.window.Window',
    alias: 'widget.printview',
    layout: 'border',
    autoShow: true,
    height: 280,
    width: 400,
    resizable: false,
    modal: true,
    title: 'Configure el reporte',

    initComponent: function () {
        var me = this;
        me.componentPath = '/contabilidad/comun/SCFPrintView/index.php/scfprintview/';

        this.stSize = Ext.create('Ext.data.Store', {
            fields: ['id', 'denom'],
            data: [
                {"id": "A4", "denom": "A4"},
                {"id": "LETTER", "denom": "CARTA"},
                {"id": "FOLIO", "denom": "FOLIO"},
                {"id": "LEGAL", "denom": "LEGAL"}
            ]
        });

        this.stOrientation = Ext.create('Ext.data.Store', {
            fields: ['id', 'denom'],
            data: [
                {"id": "Horizontal", "denom": "Horizontal"},
                {"id": "Vertical", "denom": "Vertical"}
            ]
        });

        this.stFormat = Ext.create('Ext.data.Store', {
            fields: ['id', 'denom'],
            data: [
                {"id": "html", "denom": "HTML"},
                {"id": "pdf", "denom": "PDF"},
                {"id": "excel", "denom": "EXCEL"}
            ]
        });

        this.items = [
            {
                xtype: 'form',
                url: me.componentPath + 'savePost',
                region: 'west',
                margins: '5 0 5 5',
                width: '50%',
                frame: true,
                bodyPadding: '5 5 0',
                fieldDefaults: {
                    msgTarget: 'side',
                    labelAlign: 'top'
                },
                defaults: {
                    anchor: '100%'
                },
                title: 'Opciones',
                items: [
                    {
                        xtype: 'combobox',
                        name: 'size',
                        fieldLabel: 'Tamaño del papel',
                        store: this.stSize,
                        queryMode: 'local',
                        displayField: 'denom',
                        valueField: 'id',
                        editable: false,
                        value: 'A4',
                        listConfig: {
                            getInnerTpl: function () {
                                return '<div data-qtip="{denom}">{denom}</div>';
                            }
                        },
                        listeners: {
                            select: this.updatePreview
                        }
                    },
                    {
                        xtype: 'combobox',
                        name: 'orientation',
                        fieldLabel: 'Orientación',
                        store: this.stOrientation,
                        queryMode: 'local',
                        displayField: 'denom',
                        valueField: 'id',
                        editable: false,
                        value: 'Horizontal',
                        listConfig: {
                            getInnerTpl: function () {
                                return '<div data-qtip="{denom}">{denom}</div>';
                            }
                        },
                        listeners: {
                            select: this.updatePreview
                        }
                    },
                    {
                        xtype: 'combobox',
                        name: 'format',
                        fieldLabel: 'Formato',
                        store: this.stFormat,
                        queryMode: 'local',
                        displayField: 'denom',
                        valueField: 'id',
                        editable: false,
                        value: 'html',
                        listConfig: {
                            getInnerTpl: function () {
                                return '<div data-qtip="{denom}">{denom}</div>';
                            }
                        },
                        listeners: {
                            select: this.updatePreview
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                itemId: 'preview',
                region: 'center',
                margins: '5 5 5 5',
                bodyStyle: 'padding: 20 10px; text-align: center;',
                width: '50%',
                layout: 'fit',
                tpl: [
                    '<div class="details">',
                    '<tpl for=".">',
                    '<img src="/contabilidad/comun/printview/images/{image}" />',
                    '<br/>',
                    '<div class="details-info">',
                    '<b>Detalles: </b>',
                    '<span>{detail}</span>',
                    '</div>',
                    '</tpl>',
                    '</div>'
                ],
                html: '<div class="details">' +
                '<tpl for=".">' +
                '<img src="/contabilidad/comun/printview/images/Horizontalhtml.png" />'+
                '</tpl>' +
                '</div>',

                loadRecord: function (data) {
                    this.body.hide();
                    this.tpl.overwrite(this.body, data);
                    this.body.slideIn('l', {
                        duration: 250
                    });
                }
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar',
                handler: function (btn) {
                    var win = btn.up('window'),
                        form = win.down('form').getForm();

                    if (form.isValid()) {
                        var myMask = new Ext.LoadMask(win, {msg: 'Generando reporte'});
                        myMask.show();
                        form.submit({
                            clientValidation: true,
                            params: {
                                dataSources: Ext.encode(me.dataSources)
                            },
                            success: function (response) {
                                myMask.hide();
                                var gWindow = window.open(me.componentPath + "buildReport", "", "menubar = yes, scrollbars = yes, status = no, toolbar = no, location = yes, resizable = no, titlebar = no", true);

                                gWindow.onload = function () {
//                                    console.log('iframe loaded successful');
                                };
                                me.close();
                            },
                            failure: function (response, opts) {
                                myMask.hide();
                            }
                        });
                    }
                }
            }
        ];
        this.callParent(arguments);

    },

    updatePreview: function () {
        var values = this.up('form').getForm().getValues();
        var win = this.up('window');

        win.down('panel[itemId=preview]').loadRecord({
            image: values.orientation + values.format + '.png',
            detail: win.generatePagePropertys(values.size, values.orientation)
        });
    },

    generatePagePropertys: function (size, orientation) {
        var display;
        switch (size) {
            case 'LETTER' :
            {
                display = ( size == "Horizontal" ) ? "11 x 8.5" : "8.5 x 11";
            }
                break;
            case 'FOLIO' :
            {
                display = ( size == "Horizontal" ) ? "13 x 8.5" : "8.5 x 13";
            }
                break;
            case 'LEGAL' :
            {
                display = ( size == "Horizontal" ) ? "14 x 8.5" : "8.5 x 14";
            }
                break;
            default :
            {
                display = ( size == "Horizontal" ) ? "11.7 x 8.3" : "8.3 x 11.7";
            }
        }
        return display + " en \u2192 \u2191";
    }


});
