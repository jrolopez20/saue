Ext.define('Obligacion.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_obligacion',

    store: 'Obligacion.store.Obligacion',

    title: 'Listado de cuentas por cobrar y por pagar',
    //features: [{ftype: 'grouping'}],
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{name} ({rows.length} Cuenta{[values.rows.length > 1 ? "s" : ""]})',
        hideGroupedHeader: true,
        id: 'restaurantGrouping'
    }],

    initComponent: function () {
        var me = this;

        me.columns = [
            {
                header: 'Tipo',
                dataIndex: 'tipo',
                width: 95,
                renderer: me.showTipo
            },
            {
                header: 'No. Documento',
                dataIndex: 'numero',
                width: 95
            },
            {
                header: 'Tipo Documento',
                dataIndex: 'idtipodocumento',
                width: 100,
                renderer: me.showTipoDocumento
            },
            {
                header: 'Cliente / Proveedor',
                dataIndex: 'clienteproveedor',
                width: 150
            },
            {
                header: 'Fecha emisión',
                dataIndex: 'fechaemision',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            },
            {
                header: 'Importe',
                dataIndex: 'importe',
                width: 90,
                renderer: me.showImporte
            },
            {
                header: 'Liquidado',
                dataIndex: 'liquidado',
                width: 90,
                renderer: me.showImporte
            },
            {
                header: 'No. comprobante',
                dataIndex: 'nocomprobante',
                width: 110
            },
            {
                header: 'Término de pago',
                dataIndex: 'terminodepago',
                width: 130
            },
            {
                header: 'Estado',
                dataIndex: 'estado',
                width: 110,
                renderer: me.showEstado
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    },

    showTipo: function (val) {
        var t = '';
        if (t == 1) {
            tipo = 'Cuenta por Cobrar';
        } else {
            tipo = 'Cuenta por Pagar';
        }
        return tipo;
    },

    showTipoDocumento: function (val) {
        var tipo = 'Desconocido'
        if (val == 1) {
            tipo = 'Factura';
        }
        return tipo;
    },

    showImporte: function (v) {
        return '<div style="text-align: right;">' + v + '</div>';
    },

    showEstado: function (v) {
        var r = '';
        if (v == 1) {
            r = 'Pendiente';
        }
        else if (v == 2) {
            r = 'Liquidado Parcial';
        }
        else if (v == 3) {
            r = 'Liquidado Total';
        }
        else if (v == 4) {
            r = 'Cancelado';
        }
        else if (v == 5) {
            r = 'Temporal';
        }
        else if (v == 6) {
            r = 'Litigio';
        }
        else if (v == 6) {
            r = 'Litigio';
        }
        else if (v == 7) {
            r = 'Proceso Judicial';
        }
        else if (v == 8) {
            r = 'Incobrable';
        }
        return r;
    }
});