Ext.define('Obligacion.model.Obligacion', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idobligacion', type: 'int'},
        {name: 'numero', type: 'int'},
        {name: 'fechaemision'},
        {name: 'idclientesproveedores', type: 'int'},
        {name: 'clienteproveedor', type: 'string'},
        {name: 'comentario', type: 'string'},
        {name: 'estado', type: 'int'},
        {name: 'idtipodocumento', type: 'int'},
        {name: 'importe', type: 'float'},
        {name: 'liquidado', type: 'float'},
        {name: 'tipo'},
        {name: 'nocomprobante'},
        {name: 'terminodepago'}
    ]
});