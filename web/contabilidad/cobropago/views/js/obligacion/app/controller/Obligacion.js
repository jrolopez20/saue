Ext.define('Obligacion.controller.Obligacion', {
    extend: 'Ext.app.Controller',

    views: [
        'Obligacion.view.Grid'
    ],
    stores: [
        'Obligacion.store.Obligacion'
    ],
    models: [
        'Obligacion.model.Obligacion'
    ],
    refs: [
        {ref: 'grid_obligacion', selector: 'grid_obligacion'}
    ],

    init: function () {
        var me = this;

        me.control({
            'grid_obligacion': {
                selectionchange: me.onObligacionSelectionChange
            }
        });

    },

    onObligacionSelectionChange: function () {
        var me = this,
            grid = me.getGrid_obligacion();
        //grid.down('button[action=modificar]').disable();
        //grid.down('button[action=eliminar]').disable();

        if (grid.getSelectionModel().hasSelection()) {
            //grid.down('button[action=modificar]').enable();
            //grid.down('button[action=eliminar]').enable();

        }
    }

});