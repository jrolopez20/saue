var perfil = window.parent.UCID.portal.perfil;
Ext.application({
    name: 'Obligacion',
    enableQuickTips: true,
    paths: {
        'Obligacion': '../../views/js/obligacion/app'
    },
    controllers: ['Obligacion'],
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [{
                xtype: 'grid_obligacion',
                region: 'center',
                margin: '5 5 5 5'
            }]
        });
    }
});