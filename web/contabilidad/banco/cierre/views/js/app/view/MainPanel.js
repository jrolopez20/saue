Ext.define('Cierre.view.MainPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.main_panel',
    layout: 'card',
    activeItem: 0,
    initComponent: function () {
        var me = this;

        me.tiFecha = Ext.create('Ext.toolbar.TextItem', {
            text: ''
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnCierrePeriodo,
                tooltip: perfil.etiquetas.ttpBtnCierrePeriodo,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'cierreperiodo'
            },
            {
                text: perfil.etiquetas.lbBtnCierreEjercicio,
                tooltip: perfil.etiquetas.ttpBtnCierreEjercicio,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                action: 'cierreejercicio'
            }, '->',
            me.tiFecha
        ];

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{columnName}: {name} ({rows.length} Elemento{[values.rows.length > 1 ? "s" : ""]})',
            hideGroupedHeader: true
        });

        this.items = [
            {
                xtype: 'panel',
                layout: 'fit'
            },
            Ext.create('Ext.grid.Panel', {
                name: 'transacciones',
                title: '<span style="color: red;">Transacciones sin conciliar</span>',
                margins: '5 0 0 0',
                hidden: true,
                selModel: Ext.create('Ext.selection.RowModel'),
                store: 'Cierre.store.TransaccionesErroneo',
                columns: [
                    {
                        header: 'Transaccion',
                        dataIndex: 'tipotransaccion',
                        renderer: me.showTipoTransaccion,
                        width: 90
                    },
                    {
                        header: 'Numero',
                        dataIndex: 'numero',
                        width: 90
                    },
                    {
                        header: 'Saldo',
                        dataIndex: 'saldo',
                        width: 90
                    },
                    {
                        header: perfil.etiquetas.lbFecha,
                        dataIndex: 'fechaemision',
                        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        width: 110
                    },
                    {
                        header: 'Beneficiario',
                        dataIndex: 'beneficiario',
                        width: 120
                    },
                    {
                        header: 'Cuenta',
                        dataIndex: 'cuenta',
                        flex: 1
                    },
                    {
                        header: 'Estado',
                        dataIndex: 'estado',
                        width: 95
                    }
                ]
                //features: [groupingFeature]
            }),
            Ext.create('Ext.grid.Panel', {
                name: 'comprobante',
                title: '<span style="color: red;">' + perfil.etiquetas.lbComprobantesNoAsentados + '</span>',
                margins: '5 0 0 0',
                hidden: true,
                selModel: Ext.create('Ext.selection.RowModel'),
                store: 'Cierre.store.ComprobanteErroneo',
                columns: [
                    {
                        header: perfil.etiquetas.lbNumero,
                        dataIndex: 'numerocomprobante',
                        width: 80
                    },
                    {
                        header: perfil.etiquetas.lbReferencia,
                        dataIndex: 'referencia',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbEstado,
                        dataIndex: 'estado',
                        width: 90,
                        renderer: me.showStatus
                    },
                    {
                        header: perfil.etiquetas.lbFecha,
                        dataIndex: 'fechaemision',
                        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        width: 90
                    },
                    {
                        header: perfil.etiquetas.lbUsuario,
                        dataIndex: 'usuario',
                        width: 100
                    },
                    {
                        header: perfil.etiquetas.lbDetalle,
                        dataIndex: 'detalle',
                        flex: 1
                    },
                    {
                        header: perfil.etiquetas.lbSubsistema,
                        dataIndex: 'subsistema'
                    }
                ],

            })
        ];

        this.callParent(arguments);
    },
    showTipoTransaccion: function (val) {
        var str = '';
        if (val == 0) {
            str = 'Dépositos bancarios';
        } else if (val == 1) {
            str = 'Débitos bancarios';
        } else if (val == 2) {
            str = 'Créditos bancarios';
        } else {
            str = 'Emisión de cheques';
        }
        return str;
    }
});