Ext.define('Cierre.controller.Cierre', {
    extend: 'Ext.app.Controller',
    views: [
        'Cierre.view.MainPanel'
    ],

    stores: [
        'Cierre.store.ComprobanteErroneo',
        'Cierre.store.TransaccionesErroneo'
    ],

    models: [
        'Cierre.model.ComprobanteErroneo',
        'Cierre.model.TransaccionesErroneo'
    ],

    refs: [
        {ref: 'main_panel', selector: 'main_panel'},
        {ref: 'comprobante_preview', selector: 'comprobante_preview'}
    ],

    init: function () {
        var me = this;
        me.control({
            'main_panel': {
                afterrender: me.onRenderCierre
            },
            'main_panel button[action=cierreperiodo]': {
                click: me.cierrePeriodo
            },
            'main_panel button[action=cierreejercicio]': {
                click: me.cierreEjercicio
            },
            'comprobante_preview button[action=aceptar]': {
                click: me.onAceptPreviewComprobante
            }
        });
    },

    onRenderCierre: function (cmp) {
        this.loadFecha(cmp);
    },

    loadFecha: function(cmp) {
        var me = this,
            vp = cmp.up('viewport');

        var myMask = new Ext.LoadMask(cmp, {msg: 'Cargando datos de la fecha contable...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDataFecha',
            method: 'POST',
            callback: function (options, success, response) {
                var r = Ext.decode(response.responseText);
                myMask.hide();
                cmp.fechaContable = r.fecha.fecha;
                cmp.inicioPeriodo = r.periodo.inicio;
                cmp.finPeriodo = r.periodo.fin;
                cmp.idperiodo = r.periodo.idperiodo;

                var text = 'Período actual <b>' + r.periodo.nombre +
                    '</b>, desde <b>' + Ext.util.Format.date(r.periodo.inicio, 'd/m/Y') +
                    '</b> hasta <b>' + Ext.util.Format.date(r.periodo.fin, 'd/m/Y') +
                    '</b>.  Fecha contable<b> ' + Ext.util.Format.date(r.fecha.fecha, 'd/m/Y') + '</b>';
                cmp.tiFecha.setText(text);
            }
        });
    },

    setearExtraParams: function (store) {
        var extraParams = {
            action: this.configAction
        };
        store.getProxy().extraParams = extraParams;
    },

    cierrePeriodo: function (btn) {
        var me = this,
            vp = btn.up('viewport');

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea cerrar el período contable actual?', function (btnConfirm) {
            if (btnConfirm == 'yes') {
                var myMask = new Ext.LoadMask(vp, {msg: 'Cerrando período contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'cerrarPeriodo',
                    method: 'POST',
                    params: {
                        //idpases: Ext.encode(arrModelCuenta)
                    },
                    callback: function (options, success, response) {
                        var r = Ext.decode(response.responseText);
                        myMask.hide();
                        me._showErrorList(btn.up('panel'), r);
                    }
                });
            }
        });
    },

    cierreEjercicio: function (btn) {
        var me = this,
            vp = btn.up('viewport');
        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea cerrar el ejercicio contable actual?', function (btnConfirm) {
            if (btnConfirm == 'yes') {
                var myMask = new Ext.LoadMask(vp, {msg: 'Cerrando período contable...'});
                myMask.show();
                Ext.Ajax.request({
                    url: 'cerrarEjercicio',
                    method: 'POST',
                    params: {
                        //idpases: Ext.encode(arrModelCuenta)
                    },
                    callback: function (options, success, response) {
                        var r = Ext.decode(response.responseText);
                        myMask.hide();
                        me._showErrorList(btn.up('panel'), r);
                    }
                });
            }
        });
    },

    _showErrorList: function (mainPanel, rpse) {
        var me = this;
        var layout = mainPanel.getLayout();
        if (rpse.success == false) {
            if (rpse.status == 'transacciones') {
                layout.setActiveItem(1);
                mainPanel.down('grid[name=transacciones]').getStore().loadData(rpse.datos);
            } else if (rpse.status == 'comprobante') {
                layout.setActiveItem(2);
                mainPanel.down('grid[name=comprobante]').getStore().loadData(rpse.datos);
            } else {
                Ext.Msg.show({
                    title: perfil.etiquetas.lnInfo,
                    msg: rpse.msg,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        } else {
            layout.setActiveItem(0);
            me.loadFecha(mainPanel);
        }
    }
});
