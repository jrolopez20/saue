Ext.define('Cierre.model.TransaccionesErroneo', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idtransaccion', type: 'int'},
        {name: 'tipotransaccion', type: 'int'},
        {name: 'numero'},
        {name: 'saldo'},
        {name: 'fechaemision', type: 'date'},
        {name: 'beneficiario', type: 'string'},
        {name: 'cuenta', type: 'string'},
        {name: 'estado', type: 'string'}
    ]
});