Ext.define('Cierre.store.TransaccionesErroneo', {
    extend: 'Ext.data.Store',
    model: 'Cierre.model.TransaccionesErroneo',
    autoLoad: false,
    groupField: 'tipotransaccion',
    sorters: [{
        property: 'tipotransaccion',
        direction: 'ASC'
    }, {
        property: 'numero',
        direction: 'ASC'
    }]
});