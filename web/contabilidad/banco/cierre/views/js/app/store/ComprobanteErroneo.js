Ext.define('Cierre.store.ComprobanteErroneo', {
    extend: 'Ext.data.Store',
    model: 'Cierre.model.ComprobanteErroneo',
    autoLoad: false,
    groupField: 'subsistema',
    sorters: ['subsistema', 'numerocomprobante']
});