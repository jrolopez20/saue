var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Cierre',
    enableQuickTips: true,
    paths: {
        'Cierre': '../../views/js/app'
    },
    controllers: ['Cierre'],
    launch: function() {
        UCID.portal.cargarEtiquetas('cierrebanco', function() {
            Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [
                    {
                        xtype: 'main_panel',
                        margin: '5 0 5 5'
                    }
                ]
            });
        });
    }
});