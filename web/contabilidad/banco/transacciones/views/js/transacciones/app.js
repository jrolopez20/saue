var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Transacciones',

    enableQuickTips: true,

    paths: {
        'Transacciones': '../../views/js/transacciones/app'
    },

    controllers: ['Transacciones'],

    launch: function () {
        UCID.portal.cargarEtiquetas('transacciones', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'grid_transacciones',
                        title: 'Listado de transacciones bancarias',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});