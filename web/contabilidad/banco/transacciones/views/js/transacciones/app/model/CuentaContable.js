Ext.define('Transacciones.model.CuentaContable', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idcuenta', type: 'int'},
        {name: 'saldocuenta', type: 'float'},
        {name: 'text', type: 'string'}
    ]
});