Ext.define('Transacciones.model.InstrumentoPago', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'iddocumentoprimario'},
        {name: 'idinstrumentopago'},
        {name: 'codigo'},
        {name: 'denominacion'},
    ]
});