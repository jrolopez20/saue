Ext.define('Transacciones.model.Transacciones', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idtransaccion', type: 'int'},
        {name: 'tipotransaccion', type: 'int'},
        {name: 'numero', type: 'string'},
        {name: 'fechaemision'},
        {name: 'fecharecibo'},
        {name: 'idcuentabancaria', type: 'int'},
        {name: 'nombrebanco', type: 'string'},
        {name: 'saldo', type: 'float'},
        {name: 'idbeneficiario'},
        {name: 'beneficiario', type: 'string'},
        {name: 'recibido', type: 'string'},
        {name: 'concepto', type: 'string'},
        {name: 'observacion', type: 'string'},
        {name: 'idestado', type: 'int'},
        {name: 'nocomprobante'},
        {name: 'idestructura', type: 'int'},
        {name: 'cuenta', type: 'string'},
        {name: 'estado', type: 'string'},
        /* nuevos */
        {name: 'idobligacion', type: 'int'},
        {name: 'numerocheque', type: 'string'},
        {name: 'idinstrumentopago', type: 'int'},
        {name: 'liquidarcxc', type: 'int'}
    ]
});