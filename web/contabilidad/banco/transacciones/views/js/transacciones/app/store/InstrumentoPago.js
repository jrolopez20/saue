Ext.define('Transacciones.store.InstrumentoPago', {
    extend: 'Ext.data.Store',
    model: 'Transacciones.model.InstrumentoPago',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadInstrumentoPago'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});