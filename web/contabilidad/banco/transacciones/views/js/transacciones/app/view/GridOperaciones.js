Ext.define('Transacciones.view.GridOperaciones', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_operaciones',
    store: 'Transacciones.store.Operacion',

    initComponent: function () {
        var me = this;

        this.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAddOper,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbDenominacion,
                dataIndex: 'denominacion',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },
            {
                header: perfil.etiquetas.lbCtaContable,
                dataIndex: 'text',
                flex: 1,
                editor: {
                    xtype: 'combobox',
                    allowBlank: false,
                    store: 'Transacciones.store.CuentaContable',
                    queryMode: 'local',
                    displayField: 'text',
                    valueField: 'text',
                    editable: false,
                    listeners: {
                        select: function (combo, records) {
                            combo.up('grid').getSelectionModel().getLastSelected().set('idcuenta', records[0].get('idcuenta'));
                        }
                    }
                }
            },
            {
                xtype: 'numbercolumn',
                text: perfil.etiquetas.lbImporte,
                dataIndex: 'saldo',
                width: 120,
                editor: {
                    xtype: 'numberfield',
                    decimalSeparator: '.',
                    minValue: 0.01,
                    allowBlank: false
                },
                renderer: me.showSaldo
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: perfil.etiquetas.lbBtnEliminar,
                    handler: function (grid, rowIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }
                }]
            }
        ];

        this.plugins = [this.rowEditing];
        this.callParent(arguments);
    },
    showSaldo: function (v) {
        return '<div style="text-align: right">' + formatoMoneda(v) + '</div>';
    }
});