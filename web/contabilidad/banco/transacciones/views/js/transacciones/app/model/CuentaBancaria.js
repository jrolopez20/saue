Ext.define('Transacciones.model.CuentaBancaria', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idcuentabancaria', type: 'int'},
        {name: 'cuenta', type: 'string'},
        {name: 'nombrebanco', type: 'string'},
        {name: 'cuentas'} //Almacena las cuentas contables asociadas a la cuenta bancaria
    ]
});