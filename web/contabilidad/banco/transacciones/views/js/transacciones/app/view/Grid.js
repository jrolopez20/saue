Ext.define('Transacciones.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_transacciones',
    store: 'Transacciones.store.Transacciones',
    initComponent: function () {
        var me = this;

        this.selModel = Ext.create('Ext.selection.RowModel');

        this.tiFecha = Ext.create('Ext.toolbar.TextItem', {
            text: ''
        });

        this.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, {
                text: perfil.etiquetas.lbBtnAnular,
                tooltip: perfil.etiquetas.ttpBtnAnular,
                icon: perfil.dirImg + 'anular.png',
                iconCls: 'btn',
                disabled: true,
                action: 'anular'
            }, {
                text: perfil.etiquetas.lbBtnContabilizar,
                tooltip: perfil.etiquetas.ttpBtnContabilizar,
                icon: perfil.dirImg + 'contabilizar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'contabilizar'
            }, {
                text: perfil.etiquetas.lbBtnPasarDia,
                tooltip: perfil.etiquetas.ttpBtnPasarDia,
                icon: perfil.dirImg + 'calendario.png',
                iconCls: 'btn',
                action: 'pasarDia'
            }/*, {
             text: perfil.etiquetas.lbBtnPreview,
             tooltip: perfil.etiquetas.ttpBtnPreview,
             icon: perfil.dirImg + 'ver.png',
             iconCls: 'btn',
             disabled: true,
             action: 'preview'
             }*/, '->', this.tiFecha,
            {
                xtype: 'searchfield',
                store: 'Transacciones.store.Transacciones',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['numero']
            }
        ];

        this.columns = [
            {
                header: perfil.etiquetas.lbTransaccion,
                dataIndex: 'tipotransaccion',
                width: 130,
                renderer: me.showTipoTransaccion
            },
            {
                header: perfil.etiquetas.lbNumero,
                dataIndex: 'numero',
                width: 110
            },
            {
                text: perfil.etiquetas.lbFechaEmision,
                dataIndex: 'fechaemision',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 120
            },
            {
                text: perfil.etiquetas.lbCuentaBancaria,
                dataIndex: 'cuenta',
                flex: 1
            },
            {
                text: perfil.etiquetas.lbConcepto,
                dataIndex: 'concepto',
                width: 130
            },
            {
                text: perfil.etiquetas.lbSaldo,
                dataIndex: 'saldo',
                align: 'right',
                renderer: me.showSaldo,
                width: 90
            },
            {
                text: perfil.etiquetas.lbBeneficiario,
                dataIndex: 'beneficiario',
                flex: 1
            },
            {
                text: perfil.etiquetas.lbEstado,
                dataIndex: 'estado',
                width: 90
            },
            {
                text: perfil.etiquetas.lbNoComprobante,
                dataIndex: 'nocomprobante',
                width: 60
            }
        ];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: this.store
        });

        this.callParent(arguments);
    },
    showTipoTransaccion: function (val) {
        var str = '';
        if (val == 0) {
            str = 'Dépositos bancarios';
        } else if (val == 1) {
            str = 'Débitos bancarios';
        } else if (val == 2) {
            str = 'Créditos bancarios';
        } else {
            str = 'Emisión de cheques';
        }
        return str;
    },
    showSaldo: function (v) {
        return '<span style="text-align: right;">'+formatoMoneda(v)+'<span>';
    }
});
