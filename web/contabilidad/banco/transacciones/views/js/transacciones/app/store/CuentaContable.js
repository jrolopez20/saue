Ext.define('Transacciones.store.CuentaContable', {
    extend: 'Ext.data.Store',
    model: 'Transacciones.model.CuentaContable',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadCuentasContables'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});