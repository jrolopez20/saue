Ext.define('Transacciones.model.Beneficiario', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idclientesproveedores'},
        {name: 'nombre'},
        {name: 'foto'},
        {name: 'descripcion'}
    ]
});