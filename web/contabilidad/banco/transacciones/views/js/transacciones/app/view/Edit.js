Ext.define('Transacciones.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_transaccion',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 780,
    height: 310,
    constrainHeader: true,
    initComponent: function () {

        var stTipoTransaccion = Ext.create('Ext.data.Store', {
            fields: ['tipotransaccion', 'name'],
            data: [
                {"tipotransaccion": 0, "name": "Dépositos bancarios"}, //SUMA
                {"tipotransaccion": 1, "name": "Débitos bancarios"},   //RESTA
                {"tipotransaccion": 2, "name": "Créditos bancarios"},  //SUMA
                {"tipotransaccion": 3, "name": "Emisión de cheques"}   //RESTA
            ]
        });

        this.items = [Ext.create('Ext.form.Panel', {
            xtype: 'form',
            region: 'center',
            //region: 'north',
            url: 'updateTransaccion',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%'
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'idtransaccion'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'tipotransaccion',
                            fieldLabel: perfil.etiquetas.lbTransaccion,
                            store: stTipoTransaccion,
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'tipotransaccion',
                            editable: false,
                            allowBlank: false,
                            margins: '0 5 0 0',
                            width: 170
                        },
                        {
                            xtype: 'textfield',
                            name: 'numero',
                            vtype: 'alphanum',
                            fieldLabel: perfil.etiquetas.lbNumero,
                            maxLength: 50,
                            allowBlank: false,
                            readOnly: true,
                            margins: '0 5 0 0',
                            width: 110
                        },
                        {
                            xtype: 'datefield',
                            name: 'fechaemision',
                            fieldLabel: 'Fecha',
                            //fieldLabel: perfil.etiquetas.lbFechaEmision,
                            allowBlank: false,
                            margins: '0 5 0 0',
                            width: 90
                        },
                        {
                            xtype: 'datefield',
                            name: 'fecharecibo',
                            fieldLabel: 'Fecha cheque',
                            width: 90,
                            margins: '0 5 0 0',
                            hidden: true,
                            listeners: {
                                hide: function (cmp, eOpts) {
                                    cmp.reset();
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'concepto',
                            fieldLabel: perfil.etiquetas.lbConcepto,
                            allowBlank: false,
                            //margins: '0 5 0 0',
                            maxLength: 255,
                            flex: 1
                        }
                    ]
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'idcuentabancaria',
                            fieldLabel: perfil.etiquetas.lbCuentaBancaria,
                            store: 'Transacciones.store.CuentaBancaria',
                            queryMode: 'local',
                            displayField: 'cuenta',
                            valueField: 'idcuentabancaria',
                            editable: false,
                            allowBlank: false,
                            margins: '0 5 0 0',
                            flex: 2
                        },
                        {
                            xtype: 'textfield',
                            name: 'nombrebanco',
                            fieldLabel: perfil.etiquetas.lbBanco,
                            readOnly: true,
                            maxLength: 255,
                            flex: 1
                        }
                    ]
                },
                //{
                //    xtype: 'fieldcontainer',
                //    layout: 'hbox',
                //    fieldDefaults: {
                //        labelAlign: 'top'
                //    },
                //    items: [
                //        {
                //            xtype: 'checkbox',
                //            boxLabel: 'Liquidar Cuenta por Cobrar',
                //            name: 'liquidarcxc',
                //            //margins: '0 0 8 0',
                //            inputValue: 1,
                //            checked: false,
                //            hidden: true,
                //            disabled: true
                //        }
                //    ]
                //},
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    itemId: 'hiddebleContainer',
                    //hidden: true,
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [
                        {
                            xtype: 'checkbox',
                            boxLabel: 'Liquidar Cuenta por Cobrar',
                            name: 'liquidarcxc',
                            margins: '13 5 0 0',
                            inputValue: 1,
                            checked: false,
                            hidden: true,
                            disabled: true,
                            listeners: {
                                hide: function (cmp, eOpts) {
                                    cmp.setValue(false);
                                    cmp.setDisabled(true);
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'numerocheque',
                            vtype: 'alphanum',
                            fieldLabel: 'N&uacute;mero cheque',
                            maxLength: 50,
                            allowBlank: false,
                            disabled: true,
                            hidden: true,
                            margins: '0 5 0 0',
                            flex: 1,
                            listeners: {
                                hide: function (cmp, eOpts) {
                                    cmp.setDisabled(true);
                                    cmp.reset();
                                }
                            }
                        },
                        {
                            xtype: 'combobox',
                            name: 'idbeneficiario',
                            fieldLabel: 'Proveedor',
                            store: 'Transacciones.store.Beneficiario',
                            queryMode: 'remote',
                            //queryMode: 'local',
                            displayField: 'nombre',
                            valueField: 'idclientesproveedores',
                            listConfig: {
                                getInnerTpl: function () {
                                    return '<div data-qtip="{descripcion}">{nombre}</div>';
                                }
                            },
                            forceSelection: true,
                            pageSize: 15,
                            allowBlank: false,
                            margins: '0 5 0 0',
                            flex: 1,
                            disabled: true,
                            hidden: true,
                            listeners: {
                                hide: function (cmp, eOpts) {
                                    cmp.setDisabled(true);
                                }
                            }
                        },
                        //{
                        //    xtype: 'textfield',
                        //    name: 'beneficiario',
                        //    fieldLabel: perfil.etiquetas.lbBeneficiario,
                        //    allowBlank: false,
                        //    margins: '0 5 0 0',
                        //    flex: 1
                        //},
                        {
                            xtype: 'combobox',
                            name: 'idobligacion',
                            fieldLabel: 'Cuenta por pagar',
                            tipo: 2,
                            store: 'Transacciones.store.DerechouObligacion',
                            queryMode: 'remote',
                            //queryMode: 'local',
                            displayField: 'numero',
                            valueField: 'idobligacion',
                            //listConfig: {
                            //    getInnerTpl: function() {
                            //        return '<div data-qtip="{comentario}">{numero}</div>';
                            //    }
                            //},
                            forceSelection: true,
                            pageSize: 15,
                            allowBlank: false,
                            margins: '0 5 0 0',
                            flex: 1,
                            disabled: true,
                            hidden: true,
                            listeners: {
                                hide: function (cmp, eOpts) {
                                    cmp.setDisabled(true);
                                    cmp.reset();
                                },
                                disable: function (cmp, eOpts) {
                                    cmp.reset();
                                }
                            }
                            //validator: function(value){
                            //    var field = this,
                            //        form = field.up('form');
                            //    console.log(form.down('combobox[name=tipotransaccion]').getValue())
                            //    if(form.down('combobox[name=tipotransaccion]').getValue() == 3 && Ext.isEmpty(value)){
                            //        return 'Este campo es obligatorio.';
                            //    }
                            //    return true;
                            //}
                        },
                        {
                            xtype: 'textfield',
                            name: 'recibido',
                            fieldLabel: 'Girado a',
                            //fieldLabel: perfil.etiquetas.lbRecibido,
                            maxLength: 255,
                            flex: 1,
                            disabled: true,
                            hidden: true
                        }
                    ]
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            name: 'saldo',
                            align: 'right',
                            fieldLabel: perfil.etiquetas.lbImporte,
                            allowBlank: false,
                            decimalSeparator: '.',
                            //readOnly: true,
                            minValue: 0.01,
                            margins: '0 5 0 0',
                            width: 140
                        },
                        {
                            xtype: 'textfield',
                            name: 'importeletras',
                            fieldLabel: perfil.etiquetas.lbImporteLetras,
                            readOnly: true,
                            maxLength: 255,
                            flex: 1
                        }
                    ]
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            name: 'idinstrumentopago',
                            fieldLabel: 'Instrumento de pago',
                            store: 'Transacciones.store.InstrumentoPago',
                            //queryMode: 'remote',
                            queryMode: 'local',
                            displayField: 'denominacion',
                            valueField: 'idinstrumentopago',
                            forceSelection: true,
                            pageSize: 15,
                            allowBlank: false,
                            margins: '0 5 0 0',
                            flex: 1,
                            disabled: true,
                            hidden: true,
                            listeners: {
                                hide: function (cmp, eOpts) {
                                    cmp.setDisabled(true);
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'observacion',
                            fieldLabel: perfil.etiquetas.lbObservacion,
                            maxLength: 255,
                            flex: 2
                        }
                    ]
                }

            ]
        }),
            //{
            //    xtype: 'grid_operaciones',
            //    region: 'center',
            //    title: 'Operaciones',
            //    disabled: true
            //}
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                tooltip: perfil.etiquetas.ttpBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAplicar,
                tooltip: perfil.etiquetas.ttpBtnAplicar,
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                tooltip: perfil.etiquetas.ttpBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});