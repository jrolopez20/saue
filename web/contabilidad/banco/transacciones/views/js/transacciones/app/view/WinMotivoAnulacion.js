Ext.define('Transacciones.view.WinMotivoAnulacion', {
    extend: 'Ext.window.Window',
    alias: 'widget.motivo_anulacion',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 400,
    height: 200,
    constrainHeader:true,
    title: 'Motivo de anulación',
    initComponent: function () {

        this.items = [{
            xtype:'textarea',
            name: 'motivo',
            maxLength: 255
        }];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                tooltip: perfil.etiquetas.ttpBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                tooltip: perfil.etiquetas.ttpBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});