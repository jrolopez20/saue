Ext.define('Transacciones.store.DerechouObligacion', {
    extend: 'Ext.data.Store',
    model: 'Transacciones.model.DerechouObligacion',

    //autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadDerechouObligacion'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            //messageProperty: 'mensaje'
        }
    }
});