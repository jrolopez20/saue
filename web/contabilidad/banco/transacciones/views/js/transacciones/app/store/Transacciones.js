Ext.define('Transacciones.store.Transacciones', {
    extend: 'Ext.data.Store',
    model: 'Transacciones.model.Transacciones',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadTransacciones'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});