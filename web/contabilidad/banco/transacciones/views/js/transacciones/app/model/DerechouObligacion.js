Ext.define('Transacciones.model.DerechouObligacion', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idobligacion'},
        {name: 'numero'},
        {name: 'comentario'},
        {name: 'importe', type: 'float'},
    ]
});