Ext.define('Transacciones.controller.Transacciones', {
    extend: 'Ext.app.Controller',
    views: [
        'Transacciones.view.Grid',
        'Transacciones.view.GridOperaciones',
        'Transacciones.view.Edit',
        'Transacciones.view.WinMotivoAnulacion'
    ],
    stores: [
        'Transacciones.store.Transacciones',
        'Transacciones.store.CuentaBancaria',
        'Transacciones.store.CuentaContable',
        'Transacciones.store.Beneficiario',
        'Transacciones.store.Operacion',
        'Transacciones.store.DerechouObligacion',
        'Transacciones.store.InstrumentoPago'
    ],
    models: [
        'Transacciones.model.Transacciones',
        'Transacciones.model.CuentaBancaria',
        'Transacciones.model.CuentaContable',
        'Transacciones.model.Beneficiario',
        'Transacciones.model.Operacion',
        'Transacciones.model.DerechouObligacion',
        'Transacciones.model.InstrumentoPago'
    ],
    refs: [
        {ref: 'grid_transacciones', selector: 'grid_transacciones'},
        {ref: 'edit_transaccion', selector: 'edit_transaccion'},
        {ref: 'grid_operaciones', selector: 'grid_operaciones'},
        {ref: 'comprobante', selector: 'comprobante'},
        {ref: 'win_cuenta', selector: 'win_cuenta'},
        {ref: 'motivo_anulacion', selector: 'motivo_anulacion'}
    ],
    init: function () {
        var me = this;
        me.control({
            'grid_transacciones': {
                selectionchange: me.onGridSelectionChange,
                afterrender: me.onRenderTransacciones
            },
            'grid_transacciones button[action=adicionar]': {
                click: me.addTransaccion
            },
            'grid_transacciones button[action=modificar]': {
                click: me.modTransaccion
            },
            'grid_transacciones button[action=eliminar]': {
                click: me.delTransaccion
            },
            'grid_transacciones button[action=anular]': {
                click: me.anularTransaccion
            },
            'grid_transacciones button[action=contabilizar]': {
                click: me.contabilizarTransaccion
            },
            'grid_transacciones button[action=pasarDia]': {
                click: me.pasarDia
            },
            'grid_operaciones button[action=adicionar]': {
                click: me.addOperacion
            },
            'edit_transaccion combobox[name=idcuentabancaria]': {
                select: me.onSelectCuentaBancaria
            },
            'edit_transaccion button[action=aceptar]': {
                click: me.onUpdateTransaccion
            },
            'edit_transaccion button[action=aplicar]': {
                click: me.onUpdateTransaccion
            },
            'edit_transaccion combobox[name=idbeneficiario]': {
                //select: me.onSelectBeneficiario,
                change: me.onChangeBeneficiario,
                beforeshow: me.onBeforeShowBeneficiario
            },

            'edit_transaccion combobox[name=idobligacion]': {
                select: me.onSelectCuentaXP,
                hide: me.onHideCuentaXP
            },
            'edit_transaccion checkbox[name=liquidarcxc]': {
                change: me.onCheckLiquidarCxC
            },
            'edit_transaccion numberfield[name=saldo]': {
                blur: function (field) {
                    if (field.getValue() > 0) {
                        field.up('form').down('textfield[name=importeletras]').setValue(convertir_a_letras(field.getValue()));
                    }
                },
                change: function (field, newValue, oldValue, eOpts) {
                    if (field.getValue() > 0) {
                        field.up('form').down('textfield[name=importeletras]').setValue(convertir_a_letras(field.getValue()));
                    }else{
                        field.up('form').down('textfield[name=importeletras]').reset();
                    }
                }
            },
            'edit_transaccion combobox[name=tipotransaccion]': {
                change: me.onChangeTipoTransaccion
            },
            'comprobante button[action=aceptar]': {
                click: me.onAceptComprobante
            },
            'motivo_anulacion button[action=aceptar]': {
                click: me.onAceptAnularTransaccion
            }
        });

        this.getTransaccionesStoreOperacionStore().on({
            datachanged: {fn: me.onChangeOperacion, scope: this},
            update: {fn: me.onChangeOperacion, scope: this}
        });
    },

    onChangeBeneficiario: function (field, newValue, oldValue, eOpts) {
        var form = field.up('form'),
            cbxcppagarocobrar = form.down('combobox[name=idobligacion]');

        if (cbxcppagarocobrar) {
            cbxcppagarocobrar.getStore().getProxy().extraParams = {
                idclienteproveedor: newValue,
                tipo: cbxcppagarocobrar.tipo
            }
            cbxcppagarocobrar.getStore().load({
                callback: function () {
                    //cbxcppagarocobrar.setDisabled(false);
                }
            });
            cbxcppagarocobrar.setDisabled(false);
        }
    },

    //onSelectBeneficiario: function (combo, records, eOpts) {
    //    var form = combo.up('form'),
    //        cbxcppagarocobrar = form.down('combobox[name=idobligacion]');
    //
    //    if (cbxcppagarocobrar) {
    //        cbxcppagarocobrar.getStore().getProxy().extraParams = {
    //            idclienteproveedor: combo.getValue(),
    //            tipo: cbxcppagarocobrar.tipo
    //        }
    //        cbxcppagarocobrar.getStore().load({
    //            callback: function () {
    //                cbxcppagarocobrar.setDisabled(false);
    //            }
    //        });
    //    }
    //},

    /* cargar los clientes o proveedores antes de mostrarlos */
    onBeforeShowBeneficiario: function (combo, eOpts) {
        var form = combo.up('form'),
            alert
        (combo.tipo);

        combo.getStore().getProxy().extraParams = {
            tipo: combo.tipo
        }
        combo.getStore().load();
    },

    onSelectCuentaXP: function (combo, records, eOpts) {
        var form = combo.up('form'),
            nfieldImporte = form.down('numberfield[name=saldo]');
            nfieldImporteLetras = form.down('textfield[name=importeletras]');

        /* setearle el por defecto el importe de la cuenta por pagar y establecerlo como maxValue */
        if (nfieldImporte) {
            nfieldImporte.setValue(records[0].get('importe'));
            nfieldImporte.setMaxValue(records[0].get('importe'));
            nfieldImporteLetras.setValue(convertir_a_letras(nfieldImporte.getValue()));
        }
    },

    onHideCuentaXP: function (combo, eOpts) {
        var form = combo.up('form'),
            nfieldImporte = form.down('numberfield[name=saldo]');

        /* setearle el por defecto el importe de la cuenta por pagar y establecerlo como maxValue */
        if (nfieldImporte) {
            nfieldImporte.reset();
        }

    },

    onChangeTipoTransaccion: function (field, newValue, oldValue, eOpts) {
        var form = field.up('form'),
            cbxbeneficiario = form.down('combobox[name=idbeneficiario]'),
            cbxderuobligacion = form.down('combobox[name=idobligacion]'),
            txnum = form.down('textfield[name=numero]'),
            txnumcheque = form.down('textfield[name=numerocheque]'),
            cbxinstrumentos = form.down('combobox[name=idinstrumentopago]');

        /* cargando el consecutivo */
        if(!form.down('hidden[name=idtransaccion]').getValue()) {
            var myMask = new Ext.LoadMask(form.up('window'), {msg: 'Generando consecutivo'});
            myMask.show();
            Ext.Ajax.request({
                url: 'loadConsecutivo',
                method: 'POST',
                params: {
                    tipo: newValue
                },
                callback: function (options, success, response) {
                    var num = Ext.decode(response.responseText);
                    txnum.setValue(num);
                    myMask.hide();
                }
            });
        }

        if (newValue == 3) { // si es emision de cheque
            cbxbeneficiario.setFieldLabel('Proveedor o Beneficiario');
            cbxbeneficiario.tipo = 2; //  Proveedor
            cbxderuobligacion.setFieldLabel('Cuenta por pagar');
            cbxderuobligacion.tipo = 2; // cuentas por pagar

            txnum.setFieldLabel('Egreso');
        } else {
            txnum.setFieldLabel('N&uacute;mero');
            cbxbeneficiario.reset();
            cbxderuobligacion.reset();
        }


        cbxbeneficiario.setDisabled(newValue != 3);
        //form.down('combobox[name=idobligacion]').setDisabled(newValue != 3);
        form.down('textfield[name=recibido]').setDisabled(newValue != 3);
        txnumcheque.setDisabled(newValue != 3);
        cbxinstrumentos.setDisabled(newValue != 3 && newValue != 1);

        cbxbeneficiario.setVisible(newValue == 3);
        cbxderuobligacion.setVisible(newValue == 3);
        txnumcheque.setVisible(newValue == 3);
        cbxinstrumentos.setVisible(newValue == 3 || newValue == 1);
        form.down('textfield[name=recibido]').setVisible(newValue == 3);
        form.down('datefield[name=fecharecibo]').setVisible(newValue == 3);

        form.down('fieldcontainer[itemId=hiddebleContainer]').doLayout();

        /* Si es un credito bancario activar la opcion de liquidar cuentas por cobrar */
        form.down('checkbox[name=liquidarcxc]').setDisabled(newValue != 2);
        form.down('checkbox[name=liquidarcxc]').setVisible(newValue == 2);
    },

    onCheckLiquidarCxC: function (field, newValue, oldValue, eOpts) {
        var form = field.up('form');

        if (field.getValue()) {
            form.down('combobox[name=idbeneficiario]').setFieldLabel('Cliente');
            form.down('combobox[name=idbeneficiario]').tipo = 1; //  Cliente
            form.down('combobox[name=idobligacion]').setFieldLabel('Cuenta por cobrar');
            form.down('combobox[name=idobligacion]').tipo = 1; // cuentas por cobrar
        } else {
            form.down('combobox[name=idbeneficiario]').reset();
            form.down('combobox[name=idobligacion]').reset();
        }

        form.down('combobox[name=idbeneficiario]').setDisabled(!field.getValue());
        //form.down('textfield[name=recibido]').setDisabled(!field.getValue());

        form.down('combobox[name=idbeneficiario]').setVisible(field.getValue());
        form.down('combobox[name=idobligacion]').setVisible(field.getValue());
        //form.down('textfield[name=recibido]').setVisible(field.getValue());

        form.down('fieldcontainer[itemId=hiddebleContainer]').doLayout();
    },

    onChangeOperacion: function (st) {
        var records = st.getRange(),
            saldo = 0;
        Ext.each(records, function (record) {
            saldo += record.get('saldo');
        });
        var form = this.getEdit_transaccion().down('form').getForm();
        form.findField('saldo').setValue(saldo);

        if (saldo > 0) {
            form.findField('importeletras').setValue(convertir_a_letras(saldo));
        } else {
            form.findField('importeletras').setValue('');
        }
    },
    onGridSelectionChange: function (sm) {
        var grid = this.getGrid_transacciones();

        grid.down('button[action=modificar]').disable();
        grid.down('button[action=eliminar]').disable();
        grid.down('button[action=anular]').disable();
        grid.down('button[action=contabilizar]').disable();

        if (sm.hasSelection()) {
            var record = grid.getSelectionModel().getLastSelected();
            //Modificar estado: (1)guardado
            //Eliminar estado: (1)guardado
            //Contabilizar estado: (1)guardado
            //Anular estado: (1)guardado (2)contabilizado
            if (record.get('idestado') == 1) {
                grid.down('button[action=modificar]').enable();
                grid.down('button[action=eliminar]').enable();
                grid.down('button[action=anular]').enable();
                grid.down('button[action=contabilizar]').enable();
            } else if (record.get('idestado') == 2) {
                grid.down('button[action=anular]').enable();
            }
        }
    },
    addTransaccion: function () {
        var win = Ext.widget('edit_transaccion'),
            grid = this.getGrid_transacciones();
        win.setTitle(perfil.etiquetas.lbWinAddTransaccion);

        var dfFechaEmision = win.down('datefield[name=fechaemision]');
        dfFechaEmision.setValue(grid.fechaContable);
        dfFechaEmision.setMinValue(grid.inicioPeriodo);
        dfFechaEmision.setMaxValue(grid.finPeriodo);

        //win.down('grid_operaciones').getStore().removeAll();
    },
    modTransaccion: function () {
        var me = this,
            grid = this.getGrid_transacciones();
        if (grid.getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_transaccion');
            win.setTitle(perfil.etiquetas.lbWinModTransaccion);
            win.down('button[action=aplicar]').hide();
            //win.down('grid_operaciones').enable();
            var form = win.down('form');
            var record = grid.getSelectionModel().getLastSelected();
            form.getForm().loadRecord(record);

            //Carga el store de Cta. Contable con las que trae la Cta. Bancaria seleccionada
            var recCuentaBancaria = me.getTransaccionesStoreCuentaBancariaStore().findRecord('idcuentabancaria', record.get('idcuentabancaria'));
            me.getTransaccionesStoreCuentaContableStore().loadData(recCuentaBancaria.get('cuentas'));

            var dfFecha = win.down('datefield[name=fechaemision]');
            dfFecha.setValue(grid.fechaContable);
            dfFecha.setMinValue(grid.inicioPeriodo);
            dfFecha.setMaxValue(grid.finPeriodo);

            //var stOperaciones = win.down('grid_operaciones').getStore();
            //stOperaciones.removeAll();

            var myMask = new Ext.LoadMask(win, {msg: 'Cargando datos'});
            myMask.show();
            Ext.Ajax.request({
                url: 'loadDataUpdate',
                method: 'POST',
                params: {
                    idtransaccion: record.get('idtransaccion')
                },
                callback: function (options, success, response) {
                    var r = Ext.decode(response.responseText);

                    var total = r.operaciones.datos.length;
                    //if (total > 0) {
                    //    stOperaciones.loadData(r.operaciones.datos);
                    //}
                    myMask.hide();
                }
            });
        }
    },
    delTransaccion: function (btn) {
        var me = this,
            grid = me.getGrid_transacciones();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelT, function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando transacción...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteTransaccion',
                        method: 'POST',
                        params: {
                            idtransaccion: record.get('idtransaccion')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                //grid.down('button[action=modificar]').disable();
                                //grid.down('button[action=eliminar]').disable();
                                grid.getStore().reload();
                            }
                        }
                    });
                }
            });
        }
    },
    anularTransaccion: function (btn) {
        var me = this,
            grid = me.getGrid_transacciones();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmAnularT, function (btn) {
                if (btn == 'yes') {
                    var win = Ext.widget('motivo_anulacion');
                }
            });
        }
    },
    contabilizarTransaccion: function (btn) {
        var me = this,
            grid = me.getGrid_transacciones();
        if (grid.getSelectionModel().hasSelection()) {
            var record = grid.getSelectionModel().getLastSelected();
            var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: perfil.etiquetas.msgContabilizando});
            myMask.show();

            var arrIdTransaccion = [record.get('idtransaccion')];
            Ext.Ajax.request({
                url: 'loadDataContabilizar',
                method: 'POST',
                params: {
                    accion: 'contabilizar',
                    arridtransaccion: Ext.encode(arrIdTransaccion)
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);

                    var win = Ext.widget('comprobante', {
                        title: 'Contabilizar'
                    });
                    win.stTipoDiario.on('beforeload', function (st, operation, Opts) {
                        operation.params.idsubsistema = 12;
                    });
                    win.down('form').getForm().setValues(rpse);
                    win.down('grid').getStore().loadData(rpse.pases);
                }
            });
        }
    },
    pasarDia: function (btn) {
        var me = this;
        var grid = me.getGrid_transacciones();

        Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmPasarDia, function (btnConfirm) {
            if (btnConfirm == 'yes') {
                var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: perfil.etiquetas.msgDiaSgte});
                myMask.show();
                Ext.Ajax.request({
                    url: 'nextDate',
                    method: 'POST',
                    callback: function (options, success, response) {
                        myMask.hide();
                        me.loadFecha(btn.up('grid'));
                    }
                });
            }
        });
    },
    onSelectCuentaBancaria: function (cb, records) {
        cb.up('form').getForm().findField('nombrebanco').setValue(records[0].get('nombrebanco'));
        this.getTransaccionesStoreCuentaContableStore().loadData(records[0].get('cuentas'));
        //this.getGrid_operaciones().enable();
        //this.getGrid_operaciones().getStore().removeAll();
    },
    addOperacion: function (btn) {
        var grid = this.getGrid_operaciones();
        grid.rowEditing.cancelEdit();
        grid.getStore().insert(0, {
            denominacion: '',
            idcuenta: '',
            cuentacontable: '',
            saldo: 0.00
        });
        grid.rowEditing.startEdit(0, 0);
    },
    onUpdateTransaccion: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();


        if (form.isValid()) {
            var operaciones = new Array();
            /* Esto es lo viejo */
            //var stOperaciones = win.down('grid_operaciones').getStore();
            //var recordOperaciones = stOperaciones.getRange();
            //Ext.each(recordOperaciones, function (record) {
            //    operaciones.push(record.getData());
            //});

            var cbxcuenta = form.findField('idcuentabancaria'),
                record = cbxcuenta.findRecordByValue(cbxcuenta.getValue()).get('cuentas'),
                cuenta = record[0];

            operaciones.push({
                denominacion: form.findField('concepto').getValue(),
                idcuenta: cuenta.idcuenta,
                text: cuenta.text,
                saldo: form.findField('saldo').getValue()
            });

            //[{"denominacion":"Testing","idcuenta":991633,"text":"1.1.01.03.1000 BANCO MACHALA 1070695649","saldo":100}]

            var myMask = new Ext.LoadMask(win, {msg: 'Guardando datos...'});
            myMask.show();
            form.submit({
                submitEmptyText: false,
                params: {
                    operaciones: Ext.encode(operaciones)
                },
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                    }
                    else {
                        form.reset();
                        //stOperaciones.removeAll();
                    }
                    me.getGrid_transacciones().getStore().reload();
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    },
    onRenderTransacciones: function (cmp) {
        this.loadFecha(cmp);
    },
    loadFecha: function (cmp) {
        var myMask = new Ext.LoadMask(cmp, {msg: perfil.etiquetas.msgLoadingFechaContable});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDataFecha',
            method: 'POST',
            callback: function (options, success, response) {
                var r = Ext.decode(response.responseText);
                myMask.hide();
                cmp.fechaContable = r.fecha.fecha;
                cmp.inicioPeriodo = r.periodo.inicio;
                cmp.finPeriodo = r.periodo.fin;
                cmp.idperiodo = r.periodo.idperiodo;

                var text = 'Período actual <b>' + r.periodo.nombre +
                    '</b>, desde <b>' + Ext.util.Format.date(r.periodo.inicio, 'd/m/Y') +
                    '</b> hasta <b>' + Ext.util.Format.date(r.periodo.fin, 'd/m/Y') +
                    '</b>.  Fecha contable<b> ' + Ext.util.Format.date(r.fecha.fecha, 'd/m/Y') + '</b>';

                cmp.tiFecha.setText(text);
            }
        });
    },
    onAceptComprobante: function (btn) {
        var me = this,
            win = btn.up('window'),
            gridTrans = this.getGrid_transacciones();

        Ext.MessageBox.confirm('Confirme', '¿Está seguro que desea generar el comprobante?', function (btn) {
                if (btn == 'yes') {
                    var params = win.down('form').getForm().getValues();
                    params.idtransaccion = gridTrans.getSelectionModel().getLastSelected().get('idtransaccion');
                    var pases = new Array();
                    var recordsPases = win.down('grid').getStore().getRange();

                    var sumDebito = 0;
                    var sumCredito = 0;
                    Ext.each(recordsPases, function (pase) {
                        sumDebito += pase.get('debito');
                        sumCredito += pase.get('credito');

                        pases.push({
                            codigodocumento: pase.get('codigodocumento'),
                            concatcta: pase.get('concatcta'),
                            credito: pase.get('credito'),
                            debito: pase.get('debito'),
                            denominacion: pase.get('denominacion'),
                            idcuenta: pase.get('idcuenta'),
                            idpase: pase.get('idpase')
                        });
                    });
                    params.pases = Ext.encode(pases)

                    if ((sumDebito - sumCredito) != 0) {
                        Ext.Msg.show({
                            title: perfil.etiquetas.lnInfo,
                            msg: 'No se puede generar un comprobante con errores.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                        return;
                    }

                    if (win.extraParams) {
                        Ext.apply(params, win.extraParams);
                    }
                    var myMask = new Ext.LoadMask(win, {msg: 'Generando...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'saveComprobante',
                        method: 'POST',
                        params: params,
                        callback: function (options, success, response) {
                            var rpse = Ext.decode(response.responseText);
                            myMask.hide();
                            gridTrans.getStore().reload();
                            win.close();
                        }
                    });
                }
            }
        );
    },

    //idtransaccion, motivo, accion:'anular'

    onAceptAnularTransaccion: function (btn) {
        var grid = this.getGrid_transacciones(),
            win = btn.up('window');
        var record = grid.getSelectionModel().getLastSelected();

        var arrIdTransaccion = new Array()
        arrIdTransaccion.push(record.get('idtransaccion'));
        var motivo = win.down('textarea[name=motivo]').getValue();

        var myMask = new Ext.LoadMask(win, {msg: 'Anulando transacción...'});
        myMask.show();

        if (record.get('idestado') == '1') { //Si esta guardada la transaccion
            Ext.Ajax.request({
                url: 'anularTransaccion',
                method: 'POST',
                params: {
                    arridtransaccion: Ext.encode(arrIdTransaccion),
                    motivo: motivo,
                    accion: 'anular'
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);
                    if (rpse.success) {
                        grid.getStore().reload();
                    }
                    win.close();
                }
            });
        } else {
            //Si contabilizada
            Ext.Ajax.request({
                url: 'loadDataContabilizar',
                method: 'POST',
                params: {
                    accion: 'anular',
                    motivo: motivo,
                    arridtransaccion: Ext.encode(arrIdTransaccion)
                },
                callback: function (options, success, response) {
                    myMask.hide();
                    var rpse = Ext.decode(response.responseText);

                    var winComprobante = Ext.widget('comprobante', {
                        title: perfil.etiquetas.lbContabAnulacion,
                        readOnly: true,
                        extraParams: {
                            accion: 'anular',
                            motivo: motivo,
                            arridtransaccion: Ext.encode(arrIdTransaccion)
                        }
                    });
                    winComprobante.stTipoDiario.on('beforeload', function (st, operation, Opts) {
                        operation.params.idsubsistema = 12;
                    });
                    winComprobante.down('form').getForm().setValues(rpse);
                    winComprobante.down('grid').getStore().loadData(rpse.pases);
                    if (rpse.success) {
                        win.close();
                    }
                }
            });
        }
    }

});