Ext.define('Transacciones.model.Operacion', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'denominacion', type: 'string'},
        {name: 'idcuenta'},
        {name: 'text', type: 'string'}, //Cuenta contable
        {name: 'saldo', type: 'float'}
    ]
});