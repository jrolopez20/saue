Ext.define('Transacciones.store.Beneficiario', {
    extend: 'Ext.data.Store',
    model: 'Transacciones.model.Beneficiario',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadBeneficiarios' // este carga los dos tanto clientes como proveedores
            //read: 'loadClientesProveedores' // para que solo cargue de acuerdo al tipo descomentar esta linea
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});