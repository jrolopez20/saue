Ext.define('Conciliacion.model.Transaccion', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idtransaccion', type: 'int'},
        {name: 'tipotransaccion', type: 'int'},
        {name: 'numero', type: 'string'},
        {name: 'fechaemision'},
        {name: 'fecharecibo'},
        {name: 'idcuentabancaria', type: 'int'},
        {name: 'nombrebanco', type: 'string'},
        {name: 'saldo', type: 'float'},
        {name: 'conciliar', type: 'boolean', defaultValue: false},
    ]
});