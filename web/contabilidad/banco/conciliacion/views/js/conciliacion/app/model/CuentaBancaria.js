Ext.define('Conciliacion.model.CuentaBancaria', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idcuentabancaria', type: 'int'},
        {name: 'cuenta', type: 'string'},
        {name: 'nombrebanco', type: 'string'},
        {name: 'saldo', type: 'float'},
        {name: 'saldolibro', type: 'float'},
        {name: 'cuentas'} //Almacena las cuentas contables asociadas a la cuenta bancaria
    ]
});