Ext.define('Conciliacion.store.Conciliacion', {
    extend: 'Ext.data.Store',
    model: 'Conciliacion.model.Conciliacion',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadConciliaciones'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});