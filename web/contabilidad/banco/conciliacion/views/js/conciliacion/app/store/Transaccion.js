Ext.define('Conciliacion.store.Transaccion', {
    extend: 'Ext.data.Store',
    model: 'Conciliacion.model.Transaccion',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadTransaccionesNoConciliadas'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});