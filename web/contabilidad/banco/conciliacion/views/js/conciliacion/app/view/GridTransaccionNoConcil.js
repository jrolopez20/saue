Ext.define('Conciliacion.view.GridTransaccionNoConcil', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_transaccionnoconcil',
    store: 'Conciliacion.store.Transaccion',

    initComponent: function () {
        var me = this;

        me.columns = [
            {
                header: perfil.etiquetas.lbTipo,
                dataIndex: 'tipotransaccion',
                renderer: me.showTipoTransaccion,
                flex: 1
            },
            {
                header: perfil.etiquetas.lbNumero,
                dataIndex: 'numero',
                width: 130
            },
            {
                header: perfil.etiquetas.lbFecha,
                dataIndex: 'fechaemision',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 90
            },
            {
                header: perfil.etiquetas.lbSaldo,
                dataIndex: 'saldo',
                renderer: me.showSaldo,
                width: 130
            },
            {
                xtype: 'checkcolumn',
                header: perfil.etiquetas.lbConciliar,
                dataIndex: 'conciliar',
                width: 80,
                stopSelection: false
            }
        ];


        this.callParent(arguments);
    },
    showTipoTransaccion: function (val) {
        var str = '';
        if (val == 0) {
            str = 'Dépositos bancarios';
        } else if (val == 1) {
            str = 'Débitos bancarios';
        } else if (val == 2) {
            str = 'Créditos bancarios';
        } else {
            str = 'Emisión de cheques';
        }
        return str;
    },
    showSaldo: function (v) {
        return '<div style="text-align: right;">' + formatoMoneda(v) + '<div>';
    }
});