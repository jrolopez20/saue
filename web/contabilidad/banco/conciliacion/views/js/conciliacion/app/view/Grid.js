Ext.define('Conciliacion.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_conciliacion',
    store: 'Conciliacion.store.Conciliacion',

    initComponent: function () {
        var me = this;

        me.tiFecha = Ext.create('Ext.toolbar.TextItem', {
            text: ''
        });

        me.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnPreview,
                tooltip: perfil.etiquetas.ttpBtnPreview,
                icon: perfil.dirImg + 'ver.png',
                iconCls: 'btn',
                disabled: true,
                hidden: true,
                action: 'preview'
            }, '->', me.tiFecha, ' ',
            {
                xtype: 'searchfield',
                store: 'Conciliacion.store.Conciliacion',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['noestadocta']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbNoEstadoCta,
                dataIndex: 'noestadocta',
                width: 130
            },
            {
                header: perfil.etiquetas.lbFecha,
                dataIndex: 'fecha',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 110
            },
            {
                text: perfil.etiquetas.lbCtaBancaria,
                dataIndex: 'cuenta',
                flex: 1
            },
            {
                text: perfil.etiquetas.lbSaldo,
                dataIndex: 'saldoctabancaria',
                renderer: me.showSaldo,
                width: 110
            },
            {
                text: perfil.etiquetas.lbDiferencia,
                dataIndex: 'saldodiferencia',
                renderer: me.showSaldo,
                width: 130
            },
            {
                text: perfil.etiquetas.lbCorreccion,
                dataIndex: 'saldocorreccion',
                renderer: me.showSaldo,
                width: 90
            },
            {
                text: perfil.etiquetas.lbConciliado,
                dataIndex: 'conciliada',
                renderer: me.showConciliado,
                width: 90
            }
        ];

        me.plugins = [me.cellEditing];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    },
    showSaldo: function (v) {
        return '<div style="text-align: right">' + formatoMoneda(v) + '</div>';
    },
    showConciliado: function (v) {
        if (v) {
            return '<div style="color: green; text-align: center;">SI</div>';
        } else {
            return '<div style="color: green; text-align: center;">NO</div>';
        }
    }
});