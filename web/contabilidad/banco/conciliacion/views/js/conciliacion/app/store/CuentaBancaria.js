Ext.define('Conciliacion.store.CuentaBancaria', {
    extend: 'Ext.data.Store',
    model: 'Conciliacion.model.CuentaBancaria',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadCuentaBancaria'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});