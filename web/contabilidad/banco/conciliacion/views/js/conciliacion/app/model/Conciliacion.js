Ext.define('Conciliacion.model.Conciliacion', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idconciliacion', type: 'int'},
        {name: 'noestadocta'},
        {name: 'fecha'},
        {name: 'saldoestadocta', type: 'float'},
        {name: 'saldoctabancaria', type: 'float'},
        {name: 'conciliada', type: 'int'},
        {name: 'idcuentabancaria', type: 'int'},
        {name: 'cuenta', type: 'string'},
        {name: 'saldodiferencia', type: 'float'},
        {name: 'saldocorreccion', type: 'float'}
    ]
});