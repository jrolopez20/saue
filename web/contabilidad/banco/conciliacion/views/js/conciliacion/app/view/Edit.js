Ext.define('Conciliacion.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_conciliacion',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 800,
    height: 580,
    initComponent: function () {
        var me = this;

        me.items = [
            {
                xtype: 'form',
                itemId: 'mainForm',
                region: 'north',
                url: 'updateConciliacion',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 60
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',
                margins: '0 0 5 0',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idconciliacion'
                    },
                    {
                        xtype: 'hidden',
                        name: 'conciliada',
                        value: 0 //No conciliado
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldDefaults: {
                            labelAlign: 'top'
                        },
                        items: [
                            {
                                xtype: 'combobox',
                                name: 'idcuentabancaria',
                                fieldLabel: perfil.etiquetas.lbCtaBancaria,
                                store: 'Conciliacion.store.CuentaBancaria',
                                queryMode: 'local',
                                displayField: 'cuenta',
                                valueField: 'idcuentabancaria',
                                editable: false,
                                allowBlank: false,
                                margins: '0 5 0 0',
                                flex: 1
                            },
                            {
                                xtype: 'datefield',
                                name: 'fecha',
                                fieldLabel: perfil.etiquetas.lbFecha,
                                margins: '0 5 0 0',
                                width: 150,
                                allowBlank: false
                            }, {
                                xtype: 'textfield',
                                name: 'noestadocta',
                                fieldLabel: perfil.etiquetas.lbNoEstadoCta,
                                margins: '0 5 0 0',
                                width: 155,
                                allowBlank: false
                            }, {
                                xtype: 'numberfield',
                                name: 'saldoestadocta',
                                fieldLabel: perfil.etiquetas.lbSaldoEstadoCta,
                                decimalSeparator: '.',
                                minValue: 0.01,
                                width: 155,
                                fieldStyle: 'text-align:right',
                                allowBlank: false
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldDefaults: {
                            labelAlign: 'top'
                        },
                        items: [{
                            xtype: 'displayfield',
                            fieldLabel: '',
                            name: 'display_conciliado',
                            value: '<span style="color:red;">NO CONCILIADO</span>',
                            margins: '5 5 5 10',
                            fieldStyle: 'font-size:16px',
                            flex: 1
                        }, {
                            xtype: 'numberfield',
                            name: 'saldoctabancaria',
                            fieldLabel: perfil.etiquetas.lbSaldoCtaBancaria,
                            decimalSeparator: '.',
                            margins: '0 5 0 0',
                            readOnly: true,
                            fieldStyle: 'text-align:right',
                            width: 155
                        }, {
                            xtype: 'textfield',
                            name: 'saldolibro',
                            fieldLabel: perfil.etiquetas.lbSaldoLibro,
                            readOnly: true,
                            fieldStyle: 'text-align:right',
                            width: 155
                        }]
                    }
                ]
            },
            {
                xtype: 'grid_transaccionnoconcil',
                title: perfil.etiquetas.lbTransNoConcil,
                region: 'center'
            },
            {
                xtype: 'form',
                itemId: 'formDiferencia',
                region: 'south',
                title: 'Registrar/Corregir diferencia',
                margins: '5 0 0 0',
                height: 160,
                bodyPadding: '10 10 0',
                items: [{
                    xtype: 'container',
                    anchor: '100%',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        width: 310,
                        layout: 'anchor',
                        items: [
                            {
                                xtype: 'radiogroup',
                                listeners: {
                                    change: function(field, newValue, oldValue){
                                        if(newValue.tipodiferencia == "corregir"){
                                            me.down('textfield[name=diferenciaacumulada]').show();
                                        }else{
                                            me.down('textfield[name=diferenciaacumulada]').hide();
                                        }
                                    }
                                },
                                items: [
                                    {
                                        boxLabel: 'Registrar',
                                        name: 'tipodiferencia',
                                        inputValue: 'registrar',
                                        checked: true
                                    },
                                    {boxLabel: 'Corregir', name: 'tipodiferencia', inputValue: 'corregir'}
                                ]
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: perfil.etiquetas.lbDifAcumulada,
                                name: 'diferenciaacumulada',
                                readOnly: true,
                                hidden:true,
                                labelWidth: 140,
                                fieldStyle: 'text-align:right',
                                margins: '0 5 0 0'
                            }, {
                                xtype: 'numberfield',
                                fieldLabel: perfil.etiquetas.lbCorrDeb,
                                name: 'diferenciadebito',
                                decimalSeparator: '.',
                                labelWidth: 140,
                                enableKeyEvents: true,
                                fieldStyle: 'text-align:right',
                                margins: '0 5 0 0'
                            }, {
                                xtype: 'numberfield',
                                fieldLabel: perfil.etiquetas.lbCorrCred,
                                name: 'diferenciacredito',
                                decimalSeparator: '.',
                                labelWidth: 140,
                                enableKeyEvents: true,
                                fieldStyle: 'text-align:right',
                                margins: '0 5 0 0'
                            }]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        items: [{
                            xtype: 'textarea',
                            fieldLabel: perfil.etiquetas.lbDescripcion,
                            name: 'descripcion',
                            labelWidth: 80,
                            anchor: '100%'
                        }]
                    }]
                }]
            }
        ];

        me.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                tooltip: perfil.etiquetas.ttpBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                tooltip: perfil.etiquetas.ttpBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});