Ext.define('Conciliacion.controller.Conciliacion', {
    extend: 'Ext.app.Controller',
    views: [
        'Conciliacion.view.Grid',
        'Conciliacion.view.Edit',
        'Conciliacion.view.GridTransaccionNoConcil'
    ],
    stores: [
        'Conciliacion.store.Conciliacion',
        'Conciliacion.store.CuentaBancaria',
        'Conciliacion.store.Transaccion'
    ],
    models: [
        'Conciliacion.model.Conciliacion',
        'Conciliacion.model.CuentaBancaria',
        'Conciliacion.model.Transaccion'
    ],
    refs: [
        {ref: 'grid_conciliacion', selector: 'grid_conciliacion'},
        {ref: 'edit_conciliacion', selector: 'edit_conciliacion'},
        {ref: 'grid_transaccionnoconcil', selector: 'grid_transaccionnoconcil'}
    ],
    init: function () {
        var me = this;
        me.control({
            'grid_conciliacion': {
                selectionchange: me.onGridSelectionChange,
                afterrender: me.onRenderGridConciliacion
            },
            'grid_conciliacion button[action=adicionar]': {
                click: me.addConciliacion
            },
            'edit_conciliacion combobox[name=idcuentabancaria]': {
                select: me.onSelectCuentaBancaria
            },
            'grid_transaccionnoconcil checkcolumn[dataIndex=conciliar]': {
                checkchange: me.onCheckChangeConciliar
            },
            'edit_conciliacion numberfield[name=diferenciadebito]': {
                change: me.onChangeDifDebitoCredito
            },
            'edit_conciliacion numberfield[name=diferenciacredito]': {
                change: me.onChangeDifDebitoCredito
            },
            'edit_conciliacion button[action=aceptar]': {
                click: me.saveConciliacion
            },
            'edit_conciliacion numberfield[name=saldoestadocta]': {
                change: me.onChangeSaldoEstadoCuenta
            }
        });

    },

    onChangeSaldoEstadoCuenta: function(f, newValue){
        var form = f.up('form').getForm();
        var saldoctabancaria = form.findField('saldoctabancaria').getValue();
        var saldolibro = form.findField('saldolibro').getValue();

        var estadoConcil = '<span style="color:red;">NO CONCILIADO</span>';
        var statusConcil = 0;
        if(newValue == saldoctabancaria && newValue == saldolibro) {
            estadoConcil = '<span style="color:green;">CONCILIADO</span>';
            statusConcil = 1;
        }
        form.findField('display_conciliado').setValue(estadoConcil);
        form.findField('conciliada').setValue(statusConcil);
    },

    onGridSelectionChange: function (sm) {
        var grid = this.getGrid_conciliacion();
        if (sm.hasSelection()) {
            grid.down('button[action=preview]').enable();
        } else {
            grid.down('button[action=preview]').disable();
        }
    },

    onRenderGridConciliacion: function (cmp) {
        this.loadFecha(cmp);
    },

    addConciliacion: function () {
        var win = Ext.widget('edit_conciliacion'),
            grid = this.getGrid_conciliacion();
        win.setTitle(perfil.etiquetas.lbWinAddConciliacion);

        var dfFecha = win.down('datefield[name=fecha]');
        dfFecha.setValue(grid.fechaContable);
        dfFecha.setMinValue(grid.inicioPeriodo);
        dfFecha.setMaxValue(grid.finPeriodo);

        win.down('grid_transaccionnoconcil').getStore().removeAll();
    },

    onSelectCuentaBancaria: function (cb, records) {
        var win = cb.up('window');
        cb.up('form').getForm().findField('saldoctabancaria').setValue(records[0].get('saldo'));
        cb.up('form').getForm().findField('saldolibro').setValue(records[0].get('saldolibro'));
        this.getConciliacionStoreTransaccionStore().load({
            params: {
                idcuentabancaria: records[0].get('idcuentabancaria')
            }
        });

        var myMask = new Ext.LoadMask(win, {msg: 'Cargando diferencia acumulada de la cuenta bancaria...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDiferenciaAcumulada',
            method: 'POST',
            params: {
              idcuentabancaria: records[0].get('idcuentabancaria')
            },
            callback: function (options, success, response) {
                var diferencia = Ext.decode(response.responseText);
                win.down('textfield[name=diferenciaacumulada]').setValue(formatoMoneda(diferencia));
                myMask.hide();
            }
        });
    },

    onCheckChangeConciliar: function (checkColumn, rowIndex, checked) {
        this.calcularSaldoEnBanco();
    },

    onChangeDifDebitoCredito: function (field) {
        var form = field.up('form');
        if (field.name == 'diferenciadebito') {
            form.down('numberfield[name=diferenciacredito]').reset();
        }
        else if (field.name == 'diferenciacredito') {
            form.down('numberfield[name=diferenciadebito]').reset();
        }
        this.calcularSaldoEnBanco();
    },

    calcularSaldoEnBanco: function () {
        var win = this.getEdit_conciliacion();
        var idcuentabancaria = win.down('combobox[name=idcuentabancaria]').getValue();
        var recordCuentaBancaria = this.getConciliacionStoreCuentaBancariaStore().findRecord('idcuentabancaria', idcuentabancaria);
        var saldoCuentaBancaria = recordCuentaBancaria.get('saldo');

        //Calcula diferencias
        var diferenciaCredito = win.down('numberfield[name=diferenciacredito]').getValue();
        saldoCuentaBancaria += diferenciaCredito;

        var diferenciaDebito = win.down('numberfield[name=diferenciadebito]').getValue();
        saldoCuentaBancaria -= diferenciaDebito;

        //Calcula las transacciones
        var transacciones = this.getConciliacionStoreTransaccionStore().getRange();
        Ext.each(transacciones, function (recordTransaccion) {
            if (recordTransaccion.get('tipotransaccion') == 0 || recordTransaccion.get('tipotransaccion') == 2) {
                if (recordTransaccion.get('conciliar')) {
                    saldoCuentaBancaria += recordTransaccion.get('saldo');
                }
            } else if (recordTransaccion.get('tipotransaccion') == 1 || recordTransaccion.get('tipotransaccion') == 3) {
                //Transacciones que RESTAN (1 y 3)
                if (recordTransaccion.get('conciliar')) {
                    saldoCuentaBancaria -= recordTransaccion.get('saldo');
                }
            }
        });

        //Actualiza el saldo en el campo de Saldo cuenta bancaria
        win.down('numberfield[name=saldoctabancaria]').setValue(saldoCuentaBancaria);


        var estadoConcil = '<span style="color:red;">NO CONCILIADO</span>';
        var saldolibro = win.down('textfield[name=saldolibro]').getValue();
        var saldoestadocta = win.down('textfield[name=saldoestadocta]').getValue();
        var statusConcil = 0;
        if(saldoCuentaBancaria == saldolibro && saldolibro == saldoestadocta) {
            estadoConcil = '<span style="color:green;">CONCILIADO</span>';
            statusConcil = 1;
        }
        win.down('displayfield[name=display_conciliado]').setValue(estadoConcil);
        win.down('hidden[name=conciliada]').setValue(statusConcil);
    },

    saveConciliacion: function (btn) {
        var me = this,
            win = btn.up('window'),
            mainForm = win.down('form[itemId=mainForm]').getForm();

        if (mainForm.isValid()) {
            var recordsTransacciones = win.down('grid').getStore().getRange();
            var transacciones = new Array();
            Ext.each(recordsTransacciones, function (record) {
                if (record.get('conciliar')) {
                    transacciones.push(record.get('idtransaccion'));
                }
            });

            if (transacciones.length == 0) {
                Ext.Msg.show({
                    title: 'Alerta',
                    msg: 'Para conciliar debe seleccionar al menos una transacción.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
                return;
            }

            var formDiferencia = win.down('form[itemId=formDiferencia]').getForm();
            if (formDiferencia.isValid()) {
                var myMask = new Ext.LoadMask(win, {msg: 'Guardando datos...'});
                myMask.show();
                mainForm.submit({
                    submitEmptyText: false,
                    params: {
                        transacciones: Ext.encode(transacciones),
                        diferencias: Ext.encode(formDiferencia.getValues())
                    },
                    success: function (response) {
                        myMask.hide();
                        win.close();
                    },
                    failure: function (response, opts) {
                        myMask.hide();
                    }
                });
            }
        }
    },

    loadFecha: function (cmp) {
        var me = this,
            vp = cmp.up('viewport');

        var myMask = new Ext.LoadMask(cmp, {msg: 'Cargando datos de la fecha contable...'});
        myMask.show();
        Ext.Ajax.request({
            url: 'loadDataFecha',
            method: 'POST',
            callback: function (options, success, response) {
                var r = Ext.decode(response.responseText);
                myMask.hide();
                cmp.fechaContable = r.fecha.fecha;
                cmp.inicioPeriodo = r.periodo.inicio;
                cmp.finPeriodo = r.periodo.fin;
                cmp.idperiodo = r.periodo.idperiodo;

                var text = 'Período actual <b>' + r.periodo.nombre +
                    '</b>, desde <b>' + Ext.util.Format.date(r.periodo.inicio, 'd/m/Y') +
                    '</b> hasta <b>' + Ext.util.Format.date(r.periodo.fin, 'd/m/Y') +
                    '</b>.  Fecha contable<b> ' + Ext.util.Format.date(r.fecha.fecha, 'd/m/Y') + '</b>';
                cmp.tiFecha.setText(text);
            }
        });
    }

});