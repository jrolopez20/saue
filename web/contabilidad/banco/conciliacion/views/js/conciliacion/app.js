var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Conciliacion',

    enableQuickTips: true,

    paths: {
        'Conciliacion': '../../views/js/conciliacion/app'
    },

    controllers: ['Conciliacion'],

    launch: function () {
        UCID.portal.cargarEtiquetas('conciliacion', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'grid_conciliacion',
                        title: 'Listado de conciliaciones',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});