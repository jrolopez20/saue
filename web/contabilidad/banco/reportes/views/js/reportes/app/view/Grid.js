Ext.define('Reporte.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.reporte_grid',
    store: 'Reporte.store.Reporte',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        //var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
        //    groupHeaderTpl: '{columnName}: {name} ({rows.length} Contenido{[values.rows.length > 1 ? "s" : ""]})',
        //    hideGroupedHeader: true,
        //    startCollapsed: false
        //});

        me.columns = [
            {
                header: perfil.etiquetas.lbCuenta,
                tooltip: perfil.etiquetas.ttpColCuenta,
                dataIndex: 'cuenta',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbFecha,
                tooltip: perfil.etiquetas.ttpColFecha,
                dataIndex: 'fechaemision',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 90
            },
            {
                header: perfil.etiquetas.lbNoComprobante,
                tooltip: perfil.etiquetas.ttpNoCompte,
                dataIndex: 'numerocomprobante',
                width: 110
            },
            {
                header: perfil.etiquetas.lbReferencia,
                tooltip: perfil.etiquetas.ttpColReferencia,
                dataIndex: 'descripcion',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbDebe,
                dataIndex: 'debe',
                width: 110,
                renderer: me.renderRightAlign
            },
            {
                header: perfil.etiquetas.lbHaber,
                dataIndex: 'haber',
                width: 110,
                renderer: me.renderRightAlign
            },
            {
                header: perfil.etiquetas.lbSaldo,
                dataIndex: 'saldo',
                width: 110,
                renderer: me.renderRightAlign
            }
        ];

        //this.features = [groupingFeature];
        this.callParent(arguments);
    },
    renderRightAlign: function (val) {
        return '<div style="text-align: right;">' + val + '</div>';
    }
});