Ext.define('Reporte.store.TipoReporte', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.TipoReporte',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'loadDataTipoReporte',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    },
    sorters: {
        property: 'nombre'
    }
});