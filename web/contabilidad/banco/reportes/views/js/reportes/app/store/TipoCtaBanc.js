Ext.define('Reporte.store.TipoCtaBanc', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.TipoCtaBanc',

    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'getTipoCuentaBanc',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});