Ext.define('Reporte.store.Cuenta', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Cuenta',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'getCuentaBancaria',
        actionMethods: {
            read: 'POST'
        }
    }
});