Ext.define('Reporte.model.TipoCtaBanc', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idtipocuentabancaria', type: 'int'
        },
        {
            name: 'descripcion', type: 'string'
        },
        {
            name: 'codigo', type: 'string'
        }
    ]
});