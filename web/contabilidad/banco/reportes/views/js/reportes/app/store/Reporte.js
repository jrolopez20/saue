Ext.define('Reporte.store.Reporte', {
    extend: 'Ext.data.Store',
    model: 'Reporte.model.Reporte',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'loadDataReport',
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success'
        }
    }
});