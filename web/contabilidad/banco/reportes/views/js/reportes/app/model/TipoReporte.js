Ext.define('Reporte.model.TipoReporte', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'idtiporeporte', type: 'int'
        },
        {
            name: 'nombre', type: 'string'
        }
    ]
});