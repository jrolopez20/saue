Ext.define('Reporte.model.Periodo', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idperiodo', type: 'int'
        },
        {
            name: 'nombre', type: 'string'
        },
        {
            name: 'inicio'
        },
        {
            name: 'fin'
        }
    ]
});