Ext.define('Fecha.model.Fecha', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'idfecha', type: 'int'
        },
        {
            name: 'idestructurasubsist', type: 'int'
        },
        {
            name: 'incremento', type: 'int'
        },
        {
            name: 'fecha', type: 'date'
        },
        {
            name: 'idsubsistema', type: 'int'
        },
        {
            name: 'subsistema', type: 'string'
        },
        {
            name: 'iniperiodo', type: 'date'
        },
        {
            name: 'finperiodo', type: 'date'
        }
    ]
});