Ext.define('Reporte.model.Balance', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'idestructuraecon'},
        {name: 'codigoestructura'},
        {name: 'descripcionestructura'},
        {name: 'idcuenta'},
        {name: 'concatcuenta'},
        {name: 'descripcioncuenta'},
        {name: 'saldoinicio'},
        {name: 'debe'},
        {name: 'haber'},
        {name: 'fecha'},
        {name: 'saldoperiodo'},
        {name: 'parcial'},
    ]
});