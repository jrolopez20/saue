Ext.define('Reporte.model.Cuenta', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'idcuentabancaria', type: 'int'
        },
        {
            name: 'cuenta', type: 'string'
        }
    ]
});