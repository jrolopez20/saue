var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'Reporte',

    enableQuickTips: true,

    paths: {
        'Reporte': '../../views/js/reportes/app'
    },

    controllers: ['Reporte'],

    launch: function () {
        UCID.portal.cargarEtiquetas('reportes', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'search_form',
                        region: 'north',
                        margins: '5 5 0 5'
                    },
                    {
                        xtype: 'panel',
                        region: 'center',
                        margin: '5 5 5 5',
                        bodyStyle: {
                            background: '#fff'
                        }
                    }
                    //{
                    //    xtype: 'reporte_grid',
                    //    region: 'center',
                    //    margin: '5 5 5 5',
                    //    hidden: true
                    //}
                ]
            });
        });
    }
});