var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'TipoCuentaBancaria',

    enableQuickTips: true,

    paths: {
        'TipoCuentaBancaria': '../../views/js/tipocuentabancaria/app'
    },

    controllers: ['TipoCuentaBancaria'],

    launch: function () {
        UCID.portal.cargarEtiquetas('tipocuentabancaria', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'grid_tipocuentabancaria',
                        title: 'Listado de tipos de cuentas bancarias',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});