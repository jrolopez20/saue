Ext.define('TipoCuentaBancaria.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_tipocuentabancaria',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 600,
    initComponent: function () {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'updateTipoCuentaBancaria',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'idtipocuentabancaria'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [{
                        width: 110,
                        name: 'codigo',
                        fieldLabel: perfil.etiquetas.lbCodigo,
                        maxLength: 50,
                        allowBlank: false
                    }, {
                        flex: 1,
                        name: 'descripcion',
                        fieldLabel: perfil.etiquetas.lbDescripcion,
                        maxLength: 255,
                        allowBlank: false,
                        margins: '0 0 0 5'
                    }]
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});