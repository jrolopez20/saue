Ext.define('TipoCuentaBancaria.store.TipoCuentaBancaria', {
    extend: 'Ext.data.Store',
    model: 'TipoCuentaBancaria.model.TipoCuentaBancaria',

    autoLoad: true,
    pageSize: 25,
    groupField: 'estructura',
    sorters: ['estructura','descripcion'],

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadTipoCuentaBancaria'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});