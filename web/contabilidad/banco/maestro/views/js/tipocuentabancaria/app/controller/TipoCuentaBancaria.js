Ext.define('TipoCuentaBancaria.controller.TipoCuentaBancaria', {
    extend: 'Ext.app.Controller',
    views: [
        'TipoCuentaBancaria.view.Grid',
        'TipoCuentaBancaria.view.Edit'
    ],
    stores: [
        'TipoCuentaBancaria.store.TipoCuentaBancaria'
    ],
    models: [
        'TipoCuentaBancaria.model.TipoCuentaBancaria'
    ],
    refs: [
        {ref: 'grid_tipocuentabancaria', selector: 'grid_tipocuentabancaria'},
        {ref: 'edit_tipocuentabancaria', selector: 'edit_tipocuentabancaria'}
    ],
    init: function () {
        var me = this;
        me.control({
            'grid_tipocuentabancaria': {
                selectionchange: me.onGridSelectionChange,
                render: me.onRenderGrid
            },
            'grid_tipocuentabancaria button[action=adicionar]': {
                click: me.addTipoCuentaBancaria
            },
            'grid_tipocuentabancaria button[action=modificar]': {
                click: me.modTipoCuentaBancaria
            },
            'grid_tipocuentabancaria button[action=eliminar]': {
                click: me.delTipoCuentaBancaria
            },
            'edit_tipocuentabancaria button[action=aceptar]': {
                click: me.saveTipoCuentaBancaria
            },
            'edit_tipocuentabancaria button[action=aplicar]': {
                click: me.saveTipoCuentaBancaria
            }
        });

    },

    onRenderGrid: function(grid){
        grid.getStore().load();
    },

    onGridSelectionChange: function (sm) {
        var me = this;
        if (sm.hasSelection()) {
            me.getGrid_tipocuentabancaria().down('button[action=modificar]').enable();
            me.getGrid_tipocuentabancaria().down('button[action=eliminar]').enable();
        } else {
            me.getGrid_tipocuentabancaria().down('button[action=modificar]').disable();
            me.getGrid_tipocuentabancaria().down('button[action=eliminar]').disable();
        }
    },

    addTipoCuentaBancaria: function () {
        var win = Ext.widget('edit_tipocuentabancaria');
        win.setTitle(perfil.etiquetas.lbWinAddTCB);
        win.down('button[action=aplicar]').show();
    },

    modTipoCuentaBancaria: function () {
        if (this.getGrid_tipocuentabancaria().getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_tipocuentabancaria');
            win.setTitle(perfil.etiquetas.lbWinModTCB);
            win.down('button[action=aplicar]').hide();
            var form = win.down('form');
            var record = this.getGrid_tipocuentabancaria().getSelectionModel().getLastSelected();
            form.loadRecord(record);
        }
    },

    delTipoCuentaBancaria: function () {
        var me = this,
            grid = me.getGrid_tipocuentabancaria();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelTCB, function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando tipo de cuenta bancaria...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteTipoCuentaBancaria',
                        method: 'POST',
                        params: {
                            idtipocuentabancaria: record.get('idtipocuentabancaria')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.down('button[action=modificar]').disable();
                                grid.down('button[action=eliminar]').disable();
                                grid.getStore().load();
                            }
                        }
                    });
                }
            });
        }
    },

    saveTipoCuentaBancaria: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando tipo de cuenta bancaria...'});
            myMask.show();

            form.submit({
                clientValidation: true,
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getGrid_tipocuentabancaria().getStore().load();
                    }
                    else {
                        form.reset();
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    }

});