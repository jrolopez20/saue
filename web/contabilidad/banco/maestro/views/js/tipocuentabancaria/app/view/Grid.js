Ext.define('TipoCuentaBancaria.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_tipocuentabancaria',
    store: 'TipoCuentaBancaria.store.TipoCuentaBancaria',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{columnName}: {name} ({rows.length} Tipo{[values.rows.length > 1 ? "s" : ""]} de cuenta)',
            hideGroupedHeader: true
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, '->',
            {
                xtype: 'searchfield',
                store: 'TipoCuentaBancaria.store.TipoCuentaBancaria',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['codigo', 'descripcion']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbCodigo,
                dataIndex: 'codigo',
                width: 150
            },
            {
                header: perfil.etiquetas.lbDescripcion,
                dataIndex: 'descripcion',
                flex: 1
            },
            {
                text: 'Estructura',
                dataIndex: 'estructura',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.features = [groupingFeature];

        this.callParent(arguments);
    }
});