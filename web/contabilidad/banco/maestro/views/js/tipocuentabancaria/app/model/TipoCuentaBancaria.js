Ext.define('TipoCuentaBancaria.model.TipoCuentaBancaria', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idtipocuentabancaria', type: 'int'},
        {name: 'codigo', type: 'string'},
        {name: 'descripcion', type: 'string'},
        {name: 'idestructura', type: 'int'},
        {name: 'estructura', type: 'string'}
    ]
});