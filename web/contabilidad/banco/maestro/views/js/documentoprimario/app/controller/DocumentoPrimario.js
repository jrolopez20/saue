Ext.define('DocumentoPrimario.controller.DocumentoPrimario', {
    extend: 'Ext.app.Controller',
    views: [
        'DocumentoPrimario.view.Grid',
        'DocumentoPrimario.view.Edit'
    ],
    stores: [
        'DocumentoPrimario.store.DocumentoPrimario'
    ],
    models: [
        'DocumentoPrimario.model.DocumentoPrimario'
    ],
    refs: [
        {ref: 'grid_documentoprimario', selector: 'grid_documentoprimario'},
        {ref: 'edit_documentoprimario', selector: 'edit_documentoprimario'}
    ],
    init: function () {
        var me = this;
        me.control({
            'grid_documentoprimario': {
                selectionchange: me.onGridSelectionChange
            },
            'grid_documentoprimario button[action=adicionar]': {
                click: me.addDocumentoPrimario
            },
            'grid_documentoprimario button[action=modificar]': {
                click: me.modDocumentoPrimario
            },
            'grid_documentoprimario button[action=eliminar]': {
                click: me.delDocumentoPrimario
            },
            'edit_documentoprimario button[action=aceptar]': {
                click: me.saveDocumentoPrimario
            },
            'edit_documentoprimario button[action=aplicar]': {
                click: me.saveDocumentoPrimario
            }
        });

    },
    
    onGridSelectionChange: function (sm) {
        var me = this;
        if (sm.hasSelection()) {
            me.getGrid_documentoprimario().down('button[action=modificar]').enable();
            me.getGrid_documentoprimario().down('button[action=eliminar]').enable();
        } else {
            me.getGrid_documentoprimario().down('button[action=modificar]').disable();
            me.getGrid_documentoprimario().down('button[action=eliminar]').disable();
        }
    },
    
    addDocumentoPrimario: function () {
        var win = Ext.widget('edit_documentoprimario');
        win.setTitle(perfil.etiquetas.lbWinAddDB);
        win.down('button[action=aplicar]').show();
    },
    
    modDocumentoPrimario: function () {
        if (this.getGrid_documentoprimario().getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_documentoprimario');
            win.setTitle(perfil.etiquetas.lbWinModDB);
            win.down('button[action=aplicar]').hide();
            var form = win.down('form');
            var record = this.getGrid_documentoprimario().getSelectionModel().getLastSelected();
            form.loadRecord(record);
        }
    },

    delDocumentoPrimario: function () {
        var me = this,
            grid = me.getGrid_documentoprimario();
        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmDelDB, function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();
                    var myMask = new Ext.LoadMask(grid.up('viewport'), {msg: 'Eliminando documento bancario...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteDocumentoPrimario',
                        method: 'POST',
                        params: {
                            iddocumentoprimario: record.get('iddocumentoprimario')
                        },
                        callback: function (options, success, response) {
                            myMask.hide();
                            var rpse = Ext.decode(response.responseText);
                            if (rpse.success) {
                                grid.down('button[action=modificar]').disable();
                                grid.down('button[action=eliminar]').disable();
                                grid.getStore().load();
                            }
                        }
                    });
                }
            });
        }
    },

    saveDocumentoPrimario: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();

        if (form.isValid()) {
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando documento bancario...'});
            myMask.show();

            form.submit({
                clientValidation: true,
                success: function (form, response) {
                    myMask.hide();
                    if (response.result.success) {
                        if (btn.action === 'aceptar') {
                            win.close();
                            me.getGrid_documentoprimario().getStore().load();
                        }
                        else {
                            form.reset();
                        }
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });
        }
    }

});