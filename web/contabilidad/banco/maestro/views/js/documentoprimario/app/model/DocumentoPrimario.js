Ext.define('DocumentoPrimario.model.DocumentoPrimario', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'iddocumentoprimario', type: 'int'},
        {name: 'codigo', type: 'string'},
        {name: 'denominacion', type: 'string'},
        {name: 'sigla', type: 'string'},
        {name: 'idsubsistema', type: 'int'}
    ]
});