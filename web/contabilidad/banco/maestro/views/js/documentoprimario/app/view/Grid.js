Ext.define('DocumentoPrimario.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_documentoprimario',
    store: 'DocumentoPrimario.store.DocumentoPrimario',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, '->',
            {
                xtype: 'searchfield',
                store: 'DocumentoPrimario.store.DocumentoPrimario',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['codigo', 'denominacion', 'sigla']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbCodigo,
                dataIndex: 'codigo',
                width: 150
            },
            {
                header: perfil.etiquetas.lbSigla,
                dataIndex: 'sigla',
                width: 90
            },
            {
                header: perfil.etiquetas.lbDenominacion,
                dataIndex: 'denominacion',
                flex: 1
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.callParent(arguments);
    }
});