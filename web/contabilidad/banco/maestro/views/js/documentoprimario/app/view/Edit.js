Ext.define('DocumentoPrimario.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_documentoprimario',
    layout: 'fit',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 450,
    initComponent: function () {

        this.items = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            url: 'updateDocumentoPrimario',
            fieldDefaults: {
                labelAlign: 'top',
                msgTarget: 'side',
                anchor: '100%',
                labelWidth: 60
            },
            frame: true,
            border: false,
            bodyPadding: '5 5 0',
            items: [
                {
                    xtype: 'hidden',
                    name: 'iddocumentoprimario'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelAlign: 'top'
                    },
                    items: [{
                        flex: 1,
                        name: 'codigo',
                        fieldLabel: perfil.etiquetas.lbCodigo,
                        maxLength: 50,
                        allowBlank: false
                    }, {
                        flex: 1,
                        name: 'sigla',
                        fieldLabel: perfil.etiquetas.lbSigla,
                        maxLength: 20,
                        allowBlank: false,
                        margins: '0 0 0 5'
                    }]
                },
                {
                    xtype:'textfield',
                    flex: 1,
                    name: 'denominacion',
                    fieldLabel: perfil.etiquetas.lbDenominacion,
                    maxLength: 255,
                    allowBlank: false
                }
            ]
        });

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});