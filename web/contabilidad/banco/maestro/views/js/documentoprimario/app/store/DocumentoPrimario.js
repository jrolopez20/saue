Ext.define('DocumentoPrimario.store.DocumentoPrimario', {
    extend: 'Ext.data.Store',
    model: 'DocumentoPrimario.model.DocumentoPrimario',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadDocumentoPrimario'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});