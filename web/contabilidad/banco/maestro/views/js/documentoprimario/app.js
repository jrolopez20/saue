var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'DocumentoPrimario',

    enableQuickTips: true,

    paths: {
        'DocumentoPrimario': '../../views/js/documentoprimario/app'
    },

    controllers: ['DocumentoPrimario'],

    launch: function () {
        UCID.portal.cargarEtiquetas('documentoprimario', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'grid_documentoprimario',
                        title: 'Listado de tipos de documentos bancarios',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});