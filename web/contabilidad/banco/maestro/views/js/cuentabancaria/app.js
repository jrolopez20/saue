var perfil = window.parent.UCID.portal.perfil;
perfil.etiquetas = Object();

Ext.application({
    name: 'CuentaBancaria',

    enableQuickTips: true,

    paths: {
        'CuentaBancaria': '../../views/js/cuentabancaria/app',
        'TipoCuentaBancaria': '../../views/js/tipocuentabancaria/app'
    },

    controllers: ['CuentaBancaria'],

    launch: function () {
        UCID.portal.cargarEtiquetas('cuentabancaria', function () {
            Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [
                    {
                        xtype: 'grid_cuentabancaria',
                        title: 'Listado de cuentas bancarias',
                        region: 'center',
                        margin: '5 5 5 5'
                    }
                ]
            });
        });
    }
});