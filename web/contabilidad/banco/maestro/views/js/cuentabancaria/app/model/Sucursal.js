Ext.define('CuentaBancaria.model.Sucursal', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idsucursal', type: 'int'},
        {name: 'idbanco', type: 'int'},
        {name: 'numero', type: 'int'},
        {name: 'direccion', type: 'string'}
    ]
});