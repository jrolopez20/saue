Ext.define('CuentaBancaria.view.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.edit_cuentabancaria',
    layout: 'border',
    modal: true,
    resizable: false,
    autoShow: true,
    width: 700,
    height: 270,
    //height: 500,
    initComponent: function () {

        this.items = [
            Ext.create('Ext.form.Panel', {
                region: 'center',
                //region: 'north',
                margins: '5 5 5 5',
                xtype: 'form',
                url: 'updateCuentaBancaria',
                fieldDefaults: {
                    labelAlign: 'top',
                    msgTarget: 'side',
                    anchor: '100%',
                    labelWidth: 60
                },
                frame: true,
                border: false,
                bodyPadding: '5 5 0',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idcuentabancaria'
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        fieldDefaults: {
                            labelAlign: 'top'
                        },
                        items: [{
                            flex: 1,
                            name: 'numerocuenta',
                            fieldLabel: perfil.etiquetas.lbNumeroCuenta,
                            maxLength: 255,
                            allowBlank: false
                        }, {
                            flex: 2,
                            name: 'nombre',
                            fieldLabel: perfil.etiquetas.lbNombre,
                            maxLength: 255,
                            allowBlank: false,
                            margins: '0 0 0 5'
                        }]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        fieldDefaults: {
                            labelAlign: 'top'
                        },
                        items: [
                            {
                                xtype: 'combobox',
                                name: 'idbanco',
                                fieldLabel: perfil.etiquetas.lbBanco,
                                store: 'CuentaBancaria.store.Banco',
                                valueField: 'idbanco',
                                displayField: 'nombre',
                                editable: false,
                                allowBlank: false,
                                emptyText: 'Seleccione...',
                                forceSelection: true,
                                triggerAction: 'all',
                                mode: 'local',
                                flex: 1
                            },
                            {
                                xtype: 'combobox',
                                name: 'idsucursal',
                                fieldLabel: perfil.etiquetas.lbSucursal,
                                store: 'CuentaBancaria.store.Sucursal',
                                valueField: 'idsucursal',
                                displayField: 'direccion',
                                editable: false,
                                allowBlank: false,
                                emptyText: 'Seleccione...',
                                forceSelection: true,
                                triggerAction: 'all',
                                mode: 'local',
                                queryMode: 'local',
                                margins: '0 0 0 5',
                                flex: 1,
                                listConfig: {
                                    getInnerTpl: function () {
                                        return '<div data-qtip="{numero} ({direccion})">{numero} ({direccion})</div>';
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        fieldDefaults: {
                            labelAlign: 'top'
                        },
                        items: [
                            {
                                xtype: 'combobox',
                                name: 'idtipocuentabancaria',
                                fieldLabel: perfil.etiquetas.lbTipoCuenta,
                                store: 'TipoCuentaBancaria.store.TipoCuentaBancaria',
                                valueField: 'idtipocuentabancaria',
                                displayField: 'descripcion',
                                editable: false,
                                emptyText: 'Seleccione...',
                                forceSelection: true,
                                allowBlank: false,
                                triggerAction: 'all',
                                mode: 'local',
                                flex: 1
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: perfil.etiquetas.lbApertura,
                                name: 'fechaapertura',
                                width: 175,
                                value: new Date(),
                                allowBlank: false,
                                margins: '0 0 0 5'
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: perfil.etiquetas.lbSaldo,
                                name: 'saldo',
                                readOnly: true,
                                hideTrigger: true,
                                decimalSeparator: '.',
                                width: 140,
                                value: 0,
                                //allowBlank: false,
                                //style: 'text-align:right;',
                                margins: '0 0 0 5'
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaultType: 'textfield',
                        fieldDefaults: {
                            labelAlign: 'top'
                        },
                        items: [
                            {
                                xtype: 'hidden',
                                name: 'idcuenta'
                            }, {
                                xtype: 'trigger',
                                name: 'cuentacontable',
                                id: 'cuentatrigger',
                                fieldLabel: 'Cuenta',
                                editable: false,
                                triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                                emptyText: 'Seleccione una cuenta...',
                                flex: 1
                            }
                        ]
                    }

                ]
            }),
            /* Este es el grid viejo que habia con las cuentas  */
            //{
            //    xtype: 'grid_cuentacontable',
            //    title: 'Cuentas',
            //    region: 'center',
            //    margins: '0 5 5 5'
            //}
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: 'Cancelar',
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: 'Aplicar',
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: 'Aceptar',
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});