Ext.define('CuentaBancaria.store.CuentaContable', {
    extend: 'Ext.data.Store',
    fields: ['idcuentacontable', 'idcuenta', 'idcuentabancaria', 'concatcta', 'denominacion', 'saldocuenta']
});