Ext.define('CuentaBancaria.store.CuentaBancaria', {
    extend: 'Ext.data.Store',
    model: 'CuentaBancaria.model.CuentaBancaria',

    autoLoad: true,
    pageSize: 25,
    groupField: 'tipocuenta',
    sorters: ['tipocuenta','nombre'],

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadCuentaBancaria'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});