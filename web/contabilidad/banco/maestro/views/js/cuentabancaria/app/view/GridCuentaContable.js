Ext.define('CuentaBancaria.view.GridCuentaContable', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_cuentacontable',
    store: 'CuentaBancaria.store.CuentaContable',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbCodigo,
                dataIndex: 'concatcta',
                width: 130
            },
            {
                header: perfil.etiquetas.lbCuenta,
                dataIndex: 'denominacion',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbSaldo,
                dataIndex: 'saldocuenta',
                width: 90,
                renderer: me.showSaldo
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                hideable: false,
                items: [{
                    icon: perfil.dirImg + 'eliminar.png',
                    iconCls: 'btn',
                    tooltip: perfil.etiquetas.lbBtnEliminar,
                    handler: function (grid, rowIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }
                }]
            }
        ];

        this.callParent(arguments);
    },

    showSaldo: function (val) {
        var str = val;
        if (val < 0) {
            str = '(' + val + ')';
        }
        return '<div style="text-align: right">' + str + '</div>';
    }
});