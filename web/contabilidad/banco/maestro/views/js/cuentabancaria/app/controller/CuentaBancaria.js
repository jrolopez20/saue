Ext.define('CuentaBancaria.controller.CuentaBancaria', {
    extend: 'Ext.app.Controller',
    views: [
        'CuentaBancaria.view.Grid',
        'CuentaBancaria.view.Edit',
        'CuentaBancaria.view.GridCuentaContable',
        'CuentaBancaria.view.SelectCuenta'
    ],
    stores: [
        'CuentaBancaria.store.CuentaBancaria',
        'CuentaBancaria.store.CuentaContable',
        'TipoCuentaBancaria.store.TipoCuentaBancaria',
        'CuentaBancaria.store.Banco',
        'CuentaBancaria.store.Sucursal'
    ],
    models: [
        'CuentaBancaria.model.CuentaBancaria',
        'TipoCuentaBancaria.model.TipoCuentaBancaria',
        'CuentaBancaria.model.Banco',
        'CuentaBancaria.model.Sucursal'
    ],
    refs: [
        {ref: 'grid_cuentabancaria', selector: 'grid_cuentabancaria'},
        {ref: 'edit_cuentabancaria', selector: 'edit_cuentabancaria'},
        {ref: 'grid_cuentacontable', selector: 'grid_cuentacontable'},
        {ref: 'select_cuenta', selector: 'select_cuenta'}
    ],
    init: function () {
        var me = this;
        me.control({
            'grid_cuentabancaria': {
                selectionchange: me.onGridSelectionChange
            },
            'grid_cuentabancaria button[action=adicionar]': {
                click: me.addCuentaBancaria
            },
            'grid_cuentabancaria button[action=modificar]': {
                click: me.modCuentaBancaria
            },
            'grid_cuentabancaria button[action=eliminar]': {
                click: me.delCuentaBancaria
            },
            'edit_cuentabancaria combobox[name=idbanco]': {
                select: me.onSelectBanco
            },
            'edit_cuentabancaria button[action=aceptar], edit_cuentabancaria button[action=aplicar]': {
                click: me.onUpdateCuentaBancaria
            },
            'grid_cuentacontable button[action=adicionar]': {
                click: me.adicionarCuentaContable
            },
            'select_cuenta button[action=aceptar], select_cuenta button[action=aplicar]': {
                click: me.onAddCuentaContable
            },
            // este codigo es nuevo pa lo del triggerfield de la cuenta
            'edit_cuentabancaria form fieldcontainer trigger[xtype="trigger"]': {
                beforerender: me.setOnTriggerClick
            }
        });

    },

    setOnTriggerClick: function (triggerfield, eOpts) {
        var me = this
        triggerfield.onTriggerClick = function () {
            me.adicionarCuentaContable();
        };
    },

    onGridSelectionChange: function (sm) {
        var me = this;
        if (sm.hasSelection()) {
            me.getGrid_cuentabancaria().down('button[action=modificar]').enable();
            me.getGrid_cuentabancaria().down('button[action=eliminar]').enable();
        } else {
            me.getGrid_cuentabancaria().down('button[action=modificar]').disable();
            me.getGrid_cuentabancaria().down('button[action=eliminar]').disable();
        }
    },

    addCuentaBancaria: function () {
        var win = Ext.widget('edit_cuentabancaria');
        win.setTitle(perfil.etiquetas.lbWinAddCB);
        win.down('button[action=aplicar]').show();
        //win.down('grid').getStore().removeAll(); // se comento porque se quito el grid
    },

    modCuentaBancaria: function () {
        if (this.getGrid_cuentabancaria().getSelectionModel().hasSelection()) {
            var win = Ext.widget('edit_cuentabancaria'),
                form = win.down('form'),
                record = this.getGrid_cuentabancaria().getSelectionModel().getLastSelected(),
                basicForm = form.getForm();

            win.setTitle(perfil.etiquetas.lbWinModCB);
            win.down('button[action=aplicar]').hide();

            form.loadRecord(record);

            /* Esto es para cargarle la cuenta al triggerfield que se puso para la cuenta contable*/
            var hidden = basicForm.findField('idcuenta');
            if (hidden) {
                var cuenta = record.get('cuentas')[0]
                hidden.setValue(cuenta.idcuenta);
            }
            basicForm.findField('cuentatrigger').setValue(cuenta.text);

            /* esto es lo viejo para el grid d ecuentas que habia */
            /*if (Ext.isArray(record.get('cuentas')) && record.get('cuentas').length > 0) {
                win.down('grid').getStore().loadData(record.get('cuentas'));
            }*/

            if (record.get('uso')) {
                win.down('button[action=aceptar]').hide();
                win.down('button[action=aplicar]').hide();
                win.down('button[action=adicionar]').hide();
            } else {
                win.down('button[action=aceptar]').show();
                win.down('button[action=aplicar]').show();
                win.down('button[action=adicionar]').show();
            }
        }
    },

    //modCuentaBancaria: function () {
    //    if (this.getGrid_cuentabancaria().getSelectionModel().hasSelection()) {
    //        var win = Ext.widget('edit_cuentabancaria');
    //        win.setTitle(perfil.etiquetas.lbWinModCB);
    //        win.down('button[action=aplicar]').hide();
    //        var form = win.down('form');
    //        var record = this.getGrid_cuentabancaria().getSelectionModel().getLastSelected();
    //        form.loadRecord(record);
    //
    //        if (Ext.isArray(record.get('cuentas')) && record.get('cuentas').length > 0) {
    //            win.down('grid').getStore().loadData(record.get('cuentas'));
    //        }
    //
    //        if (record.get('uso')) {
    //            win.down('button[action=aceptar]').hide();
    //            win.down('button[action=aplicar]').hide();
    //            win.down('button[action=adicionar]').hide();
    //        } else {
    //            win.down('button[action=aceptar]').show();
    //            win.down('button[action=aplicar]').show();
    //            win.down('button[action=adicionar]').show();
    //        }
    //    }
    //},

    delCuentaBancaria: function () {
        var me = this,
            grid = me.getGrid_cuentabancaria();

        if (grid.getSelectionModel().hasSelection()) {
            Ext.MessageBox.confirm(perfil.etiquetas.lbConfirme, perfil.etiquetas.msgConfirmElimCuenta, function (btn) {
                if (btn == 'yes') {
                    var record = grid.getSelectionModel().getLastSelected();

                    var myMask = new Ext.LoadMask(grid, {msg: 'Eliminando cuenta contable...'});
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'deleteCuentaBancaria',
                        method: 'POST',
                        params: {
                            idcuentabancaria: record.get('idcuentabancaria')
                        },
                        callback: function (options, success, response) {
                            var responseData = Ext.decode(response.responseText);
                            myMask.hide();
                            grid.down('button[action=modificar]').disable();
                            grid.down('button[action=eliminar]').disable();
                            grid.getStore().load();
                        }
                    });
                }
            });
        }
    },

    onSelectBanco: function (cb, records) {
        var form = cb.up('form');
        form.down('combobox[name=idsucursal]').reset();
        form.down('combobox[name=idsucursal]').getStore().load({
            params: {
                idbanco: records[0].get('idbanco')
            }
        });
    },

    adicionarCuentaContable: function (btn) {
        var win = Ext.widget('select_cuenta', {
            title: perfil.etiquetas.lbTitleSelectCuenta
        });
    },

    onAddCuentaContable: function (btn) {
        var me = this,
            win = btn.up('window'),
            tree = win.down('treepanel'),
            selectedRecord = tree.getSelectionModel().getLastSelected(),
            editcuentaWindow = me.getEdit_cuentabancaria(),
            form = editcuentaWindow.down('form').getForm();
        /* lo nuevo */
        var hidden = form.findField('idcuenta');
        if (hidden) {
            hidden.setValue(selectedRecord.get('idcuenta'));
        }
        form.findField('cuentatrigger').setValue(selectedRecord.get('text'));

        editcuentaWindow.down('textfield[name=saldo]').setValue(selectedRecord.get('saldocuenta'));
        win.close();
    },

    //onAddCuentaContable: function (btn) {
    //    var me = this,
    //        win = btn.up('window'),
    //        tree = win.down('treepanel');
    //    var stCuentaContable = me.getGrid_cuentacontable().getStore();
    //    var cuentasExistentes = stCuentaContable.getRange(),
    //        totalRecordsExistentes = cuentasExistentes.length;
    //
    //    var selectedRecords = tree.getChecked(),
    //        totalRecordsSel = selectedRecords.length;
    //
    //    var saldoTotal = 0;
    //    for (var i = 0; i < totalRecordsSel; i++) {
    //        var flag = true;
    //        for (var k = 0; k < totalRecordsExistentes; k++) {
    //            //Verifica si existe la cuenta
    //            if(selectedRecords[i].get('idcuenta') == cuentasExistentes[k].get('idcuenta')) {
    //                flag = false;
    //                break;
    //            }
    //        }
    //
    //        if(flag){
    //            //Si no existe se adiciona la cuenta
    //            stCuentaContable.add({
    //                idcuenta: selectedRecords[i].get('idcuenta'),
    //                concatcta: selectedRecords[i].get('concatcta'),
    //                denominacion: selectedRecords[i].get('denominacion'),
    //                saldocuenta: selectedRecords[i].get('saldocuenta')
    //            });
    //            saldoTotal += parseFloat(selectedRecords[i].get('saldocuenta'));
    //        }
    //    }
    //
    //    //Actualiza el campo saldo con la suma del saldo todas las cuentas
    //    var field = me.getEdit_cuentabancaria().down('textfield[name=saldo]');
    //    field.setValue(field.getValue() + saldoTotal);
    //
    //    if (btn.action === 'aceptar') {
    //        win.close();
    //    }
    //    else {
    //        tree.getSelectionModel().deselectAll();
    //    }
    //},

    onUpdateCuentaBancaria: function (btn) {
        var me = this,
            win = btn.up('window'),
            form = win.down('form').getForm();
            //grid = win.down('grid');

        if (form.isValid()) {
            //var recordsCuentas = grid.getStore().getRange();
            //if (recordsCuentas.length > 0) {
            //    var cuentas = new Array();
            //    Ext.each(recordsCuentas, function (record) {
            //        cuentas.push(record.get('idcuenta'));
            //    });
            var myMask = new Ext.LoadMask(win, {msg: 'Guardando datos de la cuenta...'});
            myMask.show();
            form.submit({
                //params: {
                //    cuentas: Ext.encode(cuentas)
                //},
                success: function (response) {
                    myMask.hide();
                    if (btn.action === 'aceptar') {
                        win.close();
                        me.getGrid_cuentabancaria().getStore().load();
                    }
                    else {
                        form.reset();
                        //Vuelve a resetear los valores por defecto del formato
                        form.findField('nivel').setValue(me.nivel);
                        form.findField('idparteformato').setValue(me.idparteformato);
                    }
                },
                failure: function (response, opts) {
                    myMask.hide();
                }
            });

            //} else {
            //    Ext.Msg.show({
            //        title: 'Error',
            //        msg: 'Debe seleccionar al menos una cuenta contable.',
            //        buttons: Ext.Msg.OK,
            //        icon: Ext.Msg.ERROR
            //    });
            //}
        }
    }

});