Ext.define('CuentaBancaria.store.Sucursal', {
    extend: 'Ext.data.Store',
    model: 'CuentaBancaria.model.Sucursal',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadSucursales'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});