Ext.define('CuentaBancaria.store.Banco', {
    extend: 'Ext.data.Store',
    model: 'CuentaBancaria.model.Banco',

    autoLoad: true,
    pageSize: 25,

    proxy: {
        type: 'ajax',
        api: {
            read: 'loadBancos'
        },
        actionMethods: {
            read: 'POST'
        },
        reader: {
            root: 'datos',
            totalProperty: 'cantidad',
            successProperty: 'success',
            messageProperty: 'mensaje'
        }
    }
});