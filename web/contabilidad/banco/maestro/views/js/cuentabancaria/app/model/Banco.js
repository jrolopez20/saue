Ext.define('CuentaBancaria.model.Banco', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idbanco', type: 'int'},
        {name: 'codigo', type: 'string'},
        {name: 'abreviatura', type: 'string'},
        {name: 'nombre', type: 'string'}
    ]
});