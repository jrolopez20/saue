Ext.define('CuentaBancaria.view.SelectCuenta', {
    extend: 'Ext.window.Window',
    alias: 'widget.select_cuenta',
    layout: 'fit',
    modal: true,
    resizable: true,
    maximizable: true,
    autoShow: true,
    width: 800,
    height: 500,
    initComponent: function () {

        this.items = [
            {
                xtype: 'cuenta_tree',
                checked: false,
                saldo: true,
                anySelection: false,
                onlyLeafSelection: false
            }
        ];

        this.buttons = [
            {
                icon: perfil.dirImg + 'cancelar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnCancelar,
                tooltip: perfil.etiquetas.ttpBtnCancelar,
                action: 'cancelar',
                scope: this,
                handler: this.close
            },
            {
                icon: perfil.dirImg + 'aplicar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAplicar,
                tooltip: perfil.etiquetas.ttpBtnAplicar,
                action: 'aplicar'
            },
            {
                icon: perfil.dirImg + 'aceptar.png',
                iconCls: 'btn',
                text: perfil.etiquetas.lbBtnAceptar,
                tooltip: perfil.etiquetas.ttpBtnAceptar,
                action: 'aceptar'
            }
        ];

        this.callParent(arguments);
    }
});