Ext.define('CuentaBancaria.view.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid_cuentabancaria',
    store: 'CuentaBancaria.store.CuentaBancaria',

    initComponent: function () {
        var me = this;

        me.selModel = Ext.create('Ext.selection.RowModel');

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{columnName}: {name} ({rows.length} cuenta{[values.rows.length > 1 ? "s" : ""]})',
            hideGroupedHeader: true
        });

        me.tbar = [
            {
                text: perfil.etiquetas.lbBtnAdicionar,
                tooltip: perfil.etiquetas.ttpBtnAdicionar,
                icon: perfil.dirImg + 'adicionar.png',
                iconCls: 'btn',
                action: 'adicionar'
            }, {
                text: perfil.etiquetas.lbBtnModificar,
                tooltip: perfil.etiquetas.ttpBtnModificar,
                icon: perfil.dirImg + 'modificar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'modificar'
            }, {
                text: perfil.etiquetas.lbBtnEliminar,
                tooltip: perfil.etiquetas.ttpBtnEliminar,
                icon: perfil.dirImg + 'eliminar.png',
                iconCls: 'btn',
                disabled: true,
                action: 'eliminar'
            }, '->',
            {
                xtype: 'searchfield',
                store: 'CuentaBancaria.store.CuentaBancaria',
                emptyText: perfil.etiquetas.msgEmptyTextSearch,
                width: 250,
                filterPropertysNames: ['numerocuenta', 'nombre']
            }
        ];

        me.columns = [
            {
                header: perfil.etiquetas.lbNumeroCuenta,
                dataIndex: 'numerocuenta',
                width: 190
            },
            {
                header: perfil.etiquetas.lbNombre,
                dataIndex: 'nombre',
                flex: 1
            },
            {
                header: perfil.etiquetas.lbSaldo,
                dataIndex: 'saldo',
                width: 80,
                renderer: me.showSaldo
            },
            {
                header: perfil.etiquetas.lbApertura,
                dataIndex: 'fechaapertura',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                width: 80
            },
            {
                header: perfil.etiquetas.lbBanco,
                dataIndex: 'nombrebanco',
                width: 150
            },
            {
                header: perfil.etiquetas.lbSucursal,
                dataIndex: 'numerosucursal',
                width: 72
            },
            {
                header: perfil.etiquetas.lbDirSucursal,
                dataIndex: 'direccion',
                width: 200
            },
            {
                header: perfil.etiquetas.lbTipoCuenta,
                dataIndex: 'tipocuenta',
                width: 110
            },
            {
                header: perfil.etiquetas.lbEnUso,
                dataIndex: 'uso',
                width: 70,
                renderer: me.showUsage
            }
        ];

        me.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            store: me.store
        });

        this.features = [groupingFeature];

        this.callParent(arguments);
    },
    showUsage: function(val){
        if(val){
            return '<b style="color: green">En uso</b>';
        }
        return '';
    },
    showSaldo: function (val) {
        var str = val;
        if (val < 0) {
            str = '(' + val + ')';
        }
        return '<div style="text-align: right">' + str + '</div>';
    }
});