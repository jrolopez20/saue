Ext.define('CuentaBancaria.model.CuentaBancaria', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idcuentabancaria', type: 'int'},
        {name: 'numerocuenta', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'fechaapertura'},
        {name: 'idsucursal', type: 'int'},
        {name: 'idbanco', type: 'int'},
        {name: 'idtipocuentabancaria', type: 'int'},
        {name: 'idestructura', type: 'int'},
        {name: 'estructura', type: 'string'},
        {name: 'saldo', type: 'float'},
        {name: 'saldoinicial', type: 'float'},
        {name: 'tipocuenta', type: 'string'},
        {name: 'nombrebanco', type: 'string'},
        {name: 'numerosucursal'},
        {name: 'direccion', type: 'string'},
        {name: 'uso', type: 'boolean'}, //Indica si la cuenta se encuentra en uso
        {name: 'cuentas'}
    ]
});